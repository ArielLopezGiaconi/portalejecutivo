@extends('back.master.masterpage')
@section('contenido')

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="zmdi zmdi-lamp text-inverse f-46"></i>
                        </div>
                        <div class="col-sm-6">
                            <h3>Focos (Cotización automatica)</h3>
                            <h6 style="color: burlywood">Permite generar cotizaciones inteligentes de las lineas sin ventas de los últimos 6 meses</h6>
                            <span class="f-16">Focos | <i class="zmdi zmdi-account f-22 middle m-r-5"
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        {{-- <div class="col-sm-4">
                            <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button>
                        </div> --}}
                    </div>
                    {{-- <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div> --}}
                </div>
                <div class="card-block">
                    <div class="x_content">
                        
                        {{-- <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <label>Periodo</label>
                                    <select name="Periodo" id="Periodo" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div> --}}

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="elPotencial" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">El Potencial</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Cálculo matemático que define el máximo de ventas que puede tener un cliente</h3>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card-block">
                <div class="table-responsive">
                    <div id="detalle">
                        <table id="tablaKarim" class="table table-hover display nowrap table-bordered table-styling sortable">
                            {{-- <thead>            table-custom table-hover table-bordered table-styling
                                <tr>
                                    <th colspan="5"></th>
                                    <th colspan="2">Librería</th>
                                    <th colspan="2">Tissue</th>
                                    <th colspan="2">Papel</th>
                                    <th colspan="2">Aseo</th>
                                    <th colspan="2">Cafetería</th>
                                    <th colspan="2">Tegnología</th>
                                    <th colspan="2">Seguridad</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead> --}}
                            <thead>
                                <tr class="table-inverse">
                                    <th colspan="6"></th>
                                    <th colspan="1">Librería</th>
                                    <th colspan="1">Tissue</th>
                                    <th colspan="1">Papel</th>
                                    <th colspan="1">Aseo</th>
                                    <th colspan="1">Cafetería</th>
                                    <th colspan="1">Tecnología</th>
                                    <th colspan="1">Seguridad</th>
                                    <th colspan="1">Total</th>
                                </tr>
                                <tr class="table-inverse">
                                    <th >Acc</th>
                                    <th >Rut</th>
                                    <th >Razon Social</th>
                                    {{-- <th >Segmento <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th> --}}
                                    <th >Credito</th>
                                    <th >Status Credito</th>
                                    <th id="potencial">Potencial <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial"></i></th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th >Ventas 6 meses antes</th>
                                    <th class="ocultar">Venta Mes Actual</th>
                                    <th class="ocultar">califica_aseo</th>
                                    <th class="ocultar">califica_cafeteria</th>
                                    <th class="ocultar">califica_libreria</th>
                                    <th class="ocultar">califica_papel</th>
                                    <th class="ocultar">califica_seguridad</th>
                                    <th class="ocultar">califica_tecnologia</th>
                                    <th class="ocultar">califica_tissue</th>
                                </tr>
                            </thead>
                            <tbody id="cuadro1.1">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "zmdi zmdi-lamp";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Focos";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

            //getPeriodosComisiones();
            getDerechos();
            Buscar2();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        $('#campana').on('change', function() {
            Buscar2();
        });

        // function getPeriodosComisiones(rutcli) {
        //     var token = '{{ csrf_token() }}';
        //     var parametros = {
        //         _token: token,
        //         rutcli: rutcli
        //     };
        //     var text = '';
        //     $.ajax({
        //         url: '{{ url('/pricing/getPeriodosComisiones') }}',
        //         data: parametros,
        //         type: 'POST',
        //         cache: false,
        //         datatype: 'json',
        //         async: true,
        //         success: function (json) {
        //             if (Object.keys(json).length > 0) {
        //                 var i;
        //                 for (i = 0; i < json.response.length; i++) {
        //                     text += '<option value="' + json.response[i].periodo + '">' + json.response[i].periodo + '</option>';
        //                 }
        //                 document.getElementById("Periodo").innerHTML = text;
        //             } else {
        //                 alert("Sin información");
        //                 $("#spinner").hide();
        //             }
        //         }
        //     });
        // }

        function FichaCliente(rut){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=20;
            var actionid=50;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("tablaKarim");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                //console.log(window.location.href.replace('/focos','') + "/fichacliente/" + cells[1].innerHTML);
                                location.href = window.location.href.replace('/focos','') + "/fichacliente/" + cells[1].innerHTML;
                            };

            };
        } 

        function generarCotizac(rut){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=20;
            var actionid=51;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("tablaKarim");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                //console.log(window.location.href.replace('/focos','') + "/fichacliente/" + cells[1].innerHTML);
                                location.href = window.location.href.replace('/focos','') + "/autocoti/" + cells[1].innerHTML + "/portada";
                            };

            };
        }

        function Buscar2() {
            var text1 = '';
            var date = new Date();
            var token = '{{ csrf_token() }}';
            // var lafecha = document.getElementById("Periodo").value;
            // if (lafecha === '') {
            //     lafecha = date.getFullYear() + '-' +
            //         (date.getMonth()+1) + '-01';
            // }else{
            //     lafecha = lafecha.substring(6,10) + '-' + lafecha.substring(3,5) + '-' + lafecha.substring(0,2);
            // }
            var parametros = {
                _token: token,
                valor: ''
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/Focos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#tablaKarim").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                            '   <button type="button"\n' +
                                            '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                            '       class="icofont icofont-ui-user f-16"></span>\n' +
                                            '   </button>\n' +
                                            '   <button type="button"\n' +
                                            '       class="tabledit-delete-button btn btn-primary waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '       title="Generar cotización" onclick="generarCotizac('+ data +');"><span\n' +
                                            '       class="fa fa-magic f-18"></span>\n' +
                                            '   </button>\n' +
                                            '</div>\n';}},
                                {data: "rutcli"},
                                {data: "razons",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "credito",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "status_credito",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "potencial",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_libreria",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_libreria",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_tissue",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_tissue",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_papel",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_papel",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_aseo",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_aseo",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_cafeteria",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_cafeteria",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_tecnologia",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_tecnologia",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_seguridad",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_seguridad",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_6_meses_total",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_meses_actual_total"},
                                {data: "califica_aseo"},
                                {data: "califica_cafeteria"},
                                {data: "califica_libreria"},
                                {data: "califica_papel"},
                                {data: "califica_seguridad"},
                                {data: "califica_tecnologia"},
                                {data: "califica_tissue"}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 10000,
                            "bPaginate": false, 
                            "bAutoWidth": false,
                            destroy: true,
                            dom: 'Bfrtip',
                            buttons: []
                            ,
                            language: {
                                buttons: {
                                },
                                "search": "Buscar:",
                                "info": ''
                            }
                        });
                        
                        var table, tr, td, i, text;
                        table = document.getElementById("tablaKarim");
                        tr = table.getElementsByTagName("tr");

                        // Loop through all table rows, and hide those who don't match the search query
                        for (i = 0; i < tr.length; i++) {
                            //'$' + number_format(row.cells[9].innerHTML,0)
                            td = tr[i].getElementsByTagName("td")[3];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[5];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[6];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[8];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[10];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[12];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[14];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[16];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[18];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[20];
                            text = $(td).text();
                            $(td).text('$' + text);
                            $(td).addClass('text-right');

                            td = tr[i].getElementsByTagName("td")[7];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[9];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[11];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[13];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[15];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[17];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[19];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[21];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[22];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[23];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[24];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[25];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[26];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[27];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[28];
                            $(td).addClass('ocultar');
                            td = tr[i].getElementsByTagName("td")[29];
                            $(td).addClass('ocultar');
                            
                            $(".ocultar").hide();

                            //LIBRERIA
                            if($(tr[i].getElementsByTagName("td")[24]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[6];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[7]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[7];
                                    $(td).css('background','#32FF00');
                                }
                            }

                            //TISSUE
                            if($(tr[i].getElementsByTagName("td")[28]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[8];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[9]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[9];
                                    $(td).css('background','#32FF00');
                                }
                            }
                            
                            //PAPEL
                            if($(tr[i].getElementsByTagName("td")[25]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[10];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[11]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[11];
                                    $(td).css('background','#32FF00');
                                }
                            }
                            //ASEO
                            if($(tr[i].getElementsByTagName("td")[22]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[12];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[13]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[13];
                                    $(td).css('background','#32FF00');
                                }
                            }
                            //CAFETERIA
                            if($(tr[i].getElementsByTagName("td")[23]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[14];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[15]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[15];
                                    $(td).css('background','#32FF00');
                                }
                            }
                            //TEGNOLOGIA
                            if($(tr[i].getElementsByTagName("td")[27]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[16];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[17]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[17];
                                    $(td).css('background','#32FF00');
                                }
                            }
                            //SEGURIDAD
                            if($(tr[i].getElementsByTagName("td")[26]).text() == '1') {
                                td = tr[i].getElementsByTagName("td")[18];
                                $(td).css('background','#FF0000');
                                if(parseInt($(tr[i].getElementsByTagName("td")[19]).text()) > 0){
                                    td = tr[i].getElementsByTagName("td")[19];
                                    $(td).css('background','#32FF00');
                                }
                            }
                        }
                        document.getElementById("potencial").click();
                        document.getElementById("potencial").click();
                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection