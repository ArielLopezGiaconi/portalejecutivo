<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 11/08/2018
 * Time: 1:20
 */

namespace App\PDO\Oracle\DMVentas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QvLineaPrincipalPDO extends Model
{
    public static function getPrincipalLines() {
        $sql = "SELECT DISTINCT linea_principal as linea, codlin_principal as codigo "
            . " FROM qv_linea_principal"
            . " ORDER BY codlin_principal ASC";

        $resultado = DB::connection('oracle_dmventas')->select($sql);
        return $resultado;
    }
}
