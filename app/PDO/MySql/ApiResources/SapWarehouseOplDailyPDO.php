<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 12:31
 */

namespace App\PDO\MySql\ApiResources;

use App\Entities\MySql\Cerberus\SapWarehouseOplDaily;
use App\PDO\Oracle\DMVentas\MaProductPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class SapWarehouseOplDailyPDO extends Model
{
    public static function existsSapWarehouse(SapWarehouseOplDaily $in_sap)
    {
        $sql = 'SELECT COUNT(*) AS cantidad FROM sap_warehouse_opl_daily a'
            . ' WHERE sap_material = :in_material'
            . ' AND sap_centro = :in_centro'
            . ' AND sap_almacen = :in_almacen'
            . ' AND DATE_FORMAT(a.fecha_registro, \'%Y-%m-%d\') = :now';

        $resultado = DB::connection('mysql_api_resources')->select($sql, [
            'in_material' => $in_sap->getSapMaterial(),
            'in_centro' => $in_sap->getSapCentro(),
            'in_almacen' => $in_sap->getSapAlmacen(),
            'now' => Carbon::now()->format('Y-m-d')
        ]);

        return ($resultado && count($resultado) > 0 && $resultado[0]->cantidad > 0) ? true : false;
    }

    public static function getSapWarehouseByCodpro($in_codpro)
    {
        $sql = 'SELECT * FROM sap_warehouse_opl_daily a'
            . ' WHERE codpro = :cod_pro'
            . ' AND DATE_FORMAT(a.fecha_registro, \'%Y-%m-%d\') = :now';

        $resultado = DB::connection('mysql_api_resources')->select($sql, [
            'cod_pro' => $in_codpro,
            'now' => Carbon::now()->format('Y-m-d')
        ]);

        return SapWarehouseOplDailyPDO::returnWarehouseFormat($resultado, true);
    }

    public static function insertSapWarehouse(SapWarehouseOplDaily $in_sap)
    {
        $sql = 'INSERT INTO sap_warehouse_opl_daily'
            . ' (sap_material, codpro, sap_centro, sap_almacen, stock, stock_compr, costo_prom, fecha_registro)'
            . ' VALUES ('
            . ' :sap_material, :codpro, :sap_centro, :sap_almacen, :stock, :stock_compr, :costo_prom, :fecha_registro)';

        try {
            DB::connection('mysql_api_resources')->select($sql, [
                'sap_material' => $in_sap->getSapMaterial(),
                'codpro' => $in_sap->getCodpro(),
                'sap_centro' => $in_sap->getSapCentro(),
                'sap_almacen' => $in_sap->getSapAlmacen(),
                'stock' => intval($in_sap->getStock()),
                'stock_compr' => intval($in_sap->getStockCompr()),
                'costo_prom' => intval($in_sap->getCostoProm()),
                'fecha_registro' => Carbon::now()->toDateTimeString(),
            ]);
        } catch (\PDOException $e) {
            return false;
        }

        return true;
    }

    public static function updateSapWarehouse(SapWarehouseOplDaily $in_sap)
    {
        $sql = '';
    }

    private static function returnWarehouseFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $sap = new SapWarehouseOplDaily($registro);
            if ($first_row) {
                return $sap;
            }
            $arrayReturn[] = $sap;
        }

        return $arrayReturn;
    }
}