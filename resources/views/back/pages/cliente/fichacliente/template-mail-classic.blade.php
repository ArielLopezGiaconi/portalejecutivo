Se envía adjunta cotización solicitada, N° cotización: [ {{ $numcot }} ].

Para más información contáctese directamente con su ejecutivo(a) de ventas, cuyos datos son los siguientes:

-Ejecutivo(a): {{ $ejecutivo }}
-Correo: {{ $correoejecutivo }}

Saludos cordiales,

DIMERC S.A.

Gracias por su preferencia.

(Favor no responder a esta dirección de email. Contacte a su ejecutivo.)