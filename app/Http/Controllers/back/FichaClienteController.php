<?php

namespace App\Http\Controllers\back;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\Controller;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\Oracle\DMVentas\DmVentasPDO; 
use App\PDO\Oracle\DMVentas\EnClientePDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mail;
use View;
use Swift_Mailer;
use Swift_Attachment;
use Swift_SmtpTransport;
use Swift_Message;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class FichaClienteController extends Controller
{
    public function getMesesConsulta($ano, $mes){
        $meses = array();
        if($mes == 1){
            $ano_consulta = $ano;
            $mes_consulta = 'ENERO';
            $nombre_mes1 = 'Octubre';
            $nombre_mes2 = 'Noviembre';
            $nombre_mes3 = 'Diciembre';
            $mes1 = 10;
            $mes2 = 11;
            $mes3 = 12;
            $ano1 = $ano-1;
            $ano2 = $ano-1;
            $ano3 = $ano-1;
        }elseif($mes == 2){
            $ano_consulta = $ano;
            $mes_consulta = 'FEBRERO';
            $nombre_mes1 = 'Noviembre';
            $nombre_mes2 = 'Diciembre';
            $nombre_mes3 = 'Enero';
            $mes1 = 11;
            $mes2 = 12;
            $mes3 = 1;
            $ano1 = $ano;
            $ano2 = $ano-1;
            $ano3 = $ano-1;
        }elseif($mes == 3){
            $ano_consulta = $ano;
            $mes_consulta = 'MARZO';
            $nombre_mes1 = 'Diciembre';
            $nombre_mes2 = 'Enero';
            $nombre_mes3 = 'Febrero';
            $mes1 = 12;
            $mes2 = 1;
            $mes3 = 2;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano-1;
        }elseif($mes == 4){
            $ano_consulta = $ano;
            $mes_consulta = 'ABRIL';
            $nombre_mes1 = 'Enero';
            $nombre_mes2 = 'Febrero';
            $nombre_mes3 = 'Marzo';
            $mes1 = 1;
            $mes2 = 2;
            $mes3 = 3;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 5){
            $ano_consulta = $ano;
            $mes_consulta = 'MAYO';
            $nombre_mes1 = 'Febrero';
            $nombre_mes2 = 'Marzo';
            $nombre_mes3 = 'Abril';
            $mes1 = 2;
            $mes2 = 3;
            $mes3 = 4;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 6){
            $ano_consulta = $ano;
            $mes_consulta = 'JUNIO';
            $nombre_mes1 = 'Marzo';
            $nombre_mes2 = 'Abril';
            $nombre_mes3 = 'Mayo';
            $mes1 = 3;
            $mes2 = 4;
            $mes3 = 5;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 7){
            $ano_consulta = $ano;
            $mes_consulta = 'JULIO';
            $nombre_mes1 = 'Abril';
            $nombre_mes2 = 'Mayo';
            $nombre_mes3 = 'Junio';
            $mes1 = 4;
            $mes2 = 5;
            $mes3 = 6;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 8){
            $ano_consulta = $ano;
            $mes_consulta = 'AGOSTO';
            $nombre_mes1 = 'Mayo';
            $nombre_mes2 = 'Junio';
            $nombre_mes3 = 'Julio';
            $mes1 = 5;
            $mes2 = 6;
            $mes3 = 7;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 9){
            $ano_consulta = $ano;
            $mes_consulta = 'SEPTIEMBRE';
            $nombre_mes1 = 'Junio';
            $nombre_mes2 = 'Julio';
            $nombre_mes3 = 'Agosto';
            $mes1 = 6;
            $mes2 = 7;
            $mes3 = 8;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 10){
            $ano_consulta = $ano;
            $mes_consulta = 'OCTUBRE';
            $nombre_mes1 = 'Julio';
            $nombre_mes2 = 'Agosto';
            $nombre_mes3 = 'Septiembre';
            $mes1 = 7;
            $mes2 = 8;
            $mes3 = 9;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 11){
            $ano_consulta = $ano;
            $mes_consulta = 'NOVIEMBRE';
            $nombre_mes1 = 'Agosto';
            $nombre_mes2 = 'Septiembre';
            $nombre_mes3 = 'Octubre';
            $mes1 = 8;
            $mes2 = 9;
            $mes3 = 10;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 12){
            $ano_consulta = $ano;
            $mes_consulta = 'DICIEMBRE';
            $nombre_mes1 = 'Septiembre';
            $nombre_mes2 = 'Octubre';
            $nombre_mes3 = 'Noviembre';
            $mes1 = 9;
            $mes2 = 10;
            $mes3 = 11;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }

        $meses[0]["ano_consulta"] = $ano_consulta;
        $meses[0]["mes_consulta"] = $mes_consulta;
        $meses[0]["nombre_mes1"] = $nombre_mes1;
        $meses[0]["nombre_mes2"] = $nombre_mes2;
        $meses[0]["nombre_mes3"] = $nombre_mes3;
        $meses[0]["mes1"] = $mes1;
        $meses[0]["mes2"] = $mes2;
        $meses[0]["mes3"] = $mes3;
        $meses[0]["ano1"] = $ano1;
        $meses[0]["ano2"] = $ano2;
        $meses[0]["ano3"] = $ano3;
        
        return $meses;
    }

    public function EnviarCartolaCorp(request $request){
        $usuario = Session::get('usuario');
        $ejecutivo = $usuario->get('nombre') . ' ' . $usuario->get('apepat') . ' ' . $usuario->get('apemat');
        $mail_ejecutivo = $usuario->get('mail01');

        $comentarioejecutivo = 'prueba Ariel';//$request->get('comentarioejecutivo', null);
		$correo_destinatario = $request->get('mail', null);
        $comentario = $request->get('comentario');
        $tipo = $request->get('tipo');
        $rut = $request->get('rut');
        $ano = $request->get('ano');
        $mes = $request->get('mes');

		//insertamos coti a link y generamos hash
		//$hashCoti = EnCotizacPDO::insertCotiLink($numcot, $correo_destinatario);

        //$data = array('linkcoti'=>$hashCoti, 'numcot'=>$numcot, 'comentarioejecutivo' => $comentarioejecutivo);

        // Path or name to the blade template to be rendered
        //$template_path = 'back.pages.cliente.fichacliente.template-mail';
        $template_path = 'back.pages.FormatoPDF.Cartola';

        //Contenido email
        $asunto = 'Reportes Corp';
		
		//generamos transporte con datos de conexion de office 365
		$transport = new Swift_SmtpTransport('smtp.office365.com', 587, 'tls');
		$transport->setUsername(ENV('MAIL_USERNAME'));
		$transport->setPassword(ENV('MAIL_PASSWORD'));
        // $transport->setUsername('cotizaciones@dimerc.cl');
		// $transport->setPassword('D1m3rc*.*-1220.,');		
		//generamos mailer con el transporte
		$swift_mailer = new Swift_Mailer($transport);
		
        $user = 'consultaDimerc';
        $pass = '3st4E5lAC0ntr4s3ña.';

        //$user, $pass, $tipo, $ano, $mes, $rut
        $cabecera = oraclePDO::getCabeceraReportesCorp($tipo, $rut, $mes);
        $meses = $this->getMesesConsulta($ano, $mes);
        $reporte1 = $this->getReporteCliente1($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte2vista1 = $this->getReporteCliente2Vista1($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte2 = $this->getReporteCliente2($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte3 = $this->getReporteCliente3($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte4 = $this->getReporteCliente4($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte5 = $this->getReporteCliente5($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte6 = $this->getReporteCliente6($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte7 = $this->getReporteCliente7($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte8 = $this->getReporteCliente8($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte9 = $this->getReporteCliente9($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte10 = $this->getReporteCliente10($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte11 = $this->getReporteCliente11($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte12 = $this->getReporteCliente12($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte13 = $this->getReporteCliente13($user, $pass, $tipo,  $ano, $mes, $rut);
        $reporte14 = $this->getReporteCliente14($user, $pass, $tipo,  $ano, $mes, $rut);
        
        $data = ['meses' => $meses,
        'cabecera' => $cabecera,
        'reporte1' => $reporte1,
        'reporte2vista1' => json_decode($reporte2vista1),
        'reporte2' => $reporte2,
        'reporte3' => json_decode($reporte3),
        'reporte4' => json_decode($reporte4),
        'reporte5' => json_decode($reporte5),
        'reporte6' => json_decode($reporte6),
        'reporte7' => json_decode($reporte7),
        'reporte8' => json_decode($reporte8),
        'reporte9' => json_decode($reporte9),
        'reporte10' => json_decode($reporte10),
        'reporte11' => json_decode($reporte11),
        'reporte12' => json_decode($reporte12),
        'reporte13' => json_decode($reporte13),
        'reporte14' => json_decode($reporte14)];
        //dd($data);
		//cargamos y rendereamos vista a HTML
		$view = View::make($template_path, $data);
        $html = $view->render();
        
        $file = fopen("Reporte_".$rut.".html","w");
        echo fwrite($file,$html);
        fclose($file);
        
 		$respuesta = new ResponseFormatt();
        try{
			//instanciamos mensaje y seteamos valores necesarios
			$message = new Swift_Message();
            
        	$message->setFrom($mail_ejecutivo)
            	->setTo([$correo_destinatario => $correo_destinatario])
            	->setSubject($asunto)
            	->setBody($comentario)
                //->attach("/test.html");
				 ->attach(Swift_Attachment::fromPath('Reporte_'.$rut.'.html', 'text/html'));
            
			//enviamos mail
			$cantMails = $swift_mailer->send($message);
			$respuesta->setCode(200)
            		  ->setResponse('Correo enviado exitosamente');
		}
		catch(\Exception $e){
			$respuesta->setCode(201)
            		  ->setResponse('Error en la autenticación, ¿es su correo '.$mail_ejecutivo.'? Puede actualizarlo en el cuadro inferior');
		}
		finally{
			return $respuesta->returnToJson();
		}
	}

    public function imprimir(Request $request){

        $datos = array();
        $rut = $_GET['valor'];

        $resultado1 = oraclePDO::getReporteCobranzaDatos($rut);
        $resultado2 = oraclePDO::getReporteDeudaVencida($rut);
        $resultado3 = oraclePDO::getReporteDeudaVencidaTotal($rut);

        $datos[0]["encabezado"] = $resultado1;
        $datos[0]["detalle"] = $resultado2;
        $datos[0]["total"] = $resultado3;

        //$view = View::make()->render();
        $urlFont=resource_path().'\\assets\\fonts\\Abel-Regular.ttf';
        $urlHeader = asset('assets/1/img/header-cobranza.jpg');
        $urlFooter = asset('assets/1/img/footer-cobranza.jpg');
        $pdf = PDF::loadView('back.pages.FormatoPDF.prueba' ,  ['datos' => $datos,
                                                                'urlFont' => $urlFont,
                                                                'header' => $urlHeader,
                                                                'footer' => $urlFooter]);
        return $pdf->download('cartola_cobranza_' . $rut . '.pdf');
        return view('back.pages.FormatoPDF.prueba',['datos' => $datos,
                                                    'urlFont' => $urlFont,
                                                    'header' => $urlHeader,
                                                    'footer' => $urlFooter]);
        
    }

    public function setFichaCliente($rutcli)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        if (!isset($rutcli)) {
            Session::get('auth', false);
            return redirect()->to('login');
        }
        if (!Session::get('auth')) {
            return redirect()->to('login');
        }
        if (!DeClientePDO::canSeeCliente($rutcli, $rutusu)) {
            Session::flash('err_busquedacliente', 'Rut no existe o no tiene permiso para visualizar el cliente buscado.');
            $rutcli_actual = Session::get('searchclient');
            Session::put('searchclient', $rutcli_actual);
            //return redirect()->to('login');
        } else {
            Session::put('searchclient', $rutcli);
        }

        return redirect()->to('fichacliente');

        /*
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $carteraData = PortalEjUsuariosPDO::getCarteraEjecutivo($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($carteraData);
        return $respuesta->returnToJson();*/
    }

    public function getDatosCliente(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = PortalEjUsuariosPDO::getDatosCliente($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCentrosCostos(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::getCentrosCostos($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getGestiones(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::getGestiones($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCobrador(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');
        $cencos = $request->get('cencos');

        $resultado = oraclePDO::getCobrador($rutcliente, $cencos);

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCredito(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');
        $cencos = $request->get('cencos');
        $idsap = '';

        $idsap = oraclePDO::getIdCliente($rutcliente, $cencos);

        $datos = $this->ConsultaCreditoSap($rutcliente, $cencos, $idsap, 2000);

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($datos);
        return $respuesta->returnToJson();
    }

    public function ConsultaCreditoSap($rut, $cencos, $idsap, $sociedad) {

        try {
            $user = 'popws';
            $pass = 'D1w3rc2020';
            $url = 'http://dimpoprdci.dimerc.cl:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_TELEMARK&receiverParty=&receiverService=&interface=SI_FIRM007_2_SYN_OUT&interfaceNamespace=urn%3Aswap%3Atelemarke%3As4h%3Adatos_credito_light%3AFIRM007_2';
            $body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:swap:telemarke:s4h:datos_credito_light:FIRM007_2">
                        <soapenv:Header/>
                        <soapenv:Body>
                        <urn:MT_FIRM007_2_REQ_OUT>
                            <COD_CLIENTE>' . $idsap . '</COD_CLIENTE>
                            <SOCIEDAD>' . $sociedad . '</SOCIEDAD>
                        </urn:MT_FIRM007_2_REQ_OUT>
                        </soapenv:Body>
                    </soapenv:Envelope>';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $pass);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cache-Control: no-cache',
                'Content-Type: application/xml'
            ));
            $response = curl_exec($ch);

            curl_close($ch);

            $dom = new \DOMDocument;
            $dom->loadXML($response);
            dd($dom->loadXML($response));
            //CUPO_CREDITO
            $dom->getElementsByTagName('CUPO_CREDITO');
            $credito = $dom->getElementsByTagName('CUPO_CREDITO')->item(0)->nodeValue;
            $comprometido = $dom->getElementsByTagName('COMPROMETIDO')->item(0)->nodeValue;

            //PortalEjUsuariosPDO::ActualizaCreditoMysql($rut, $credito, $disponible, 'mysql_portal');
            $datos = array();
            $datos[0]['id'] = $idsap;
            $datos[0]['rut'] = $rut;
            $datos[0]['cencos'] = $cencos;
            $datos[0]['credito'] = intval($credito);
            $datos[0]['comprometido'] = intval($comprometido);

            return $datos;

        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }
    
    public function getCredito2(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');
        $cencos = $request->get('cencos');
        $idsap = '';

        $idsap = oraclePDO::getIdCliente($rutcliente, $cencos);

        $datos = $this->ConsultaDeudaSap($rutcliente, $cencos, $idsap, 2000);
      
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($datos);
        return $respuesta->returnToJson();
    }

    public function ConsultaDeudaSap($rut, $cencos, $idsap, $sociedad) {

        try {
            $user = 'popws';
            $pass = 'D1w3rc2020';
            $url = 'http://dimpoprdci.dimerc.cl:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_TELEMARK&receiverParty=&receiverService=&interface=SI_FIRM007_SYN_OUT&interfaceNamespace=urn:swap:telemarke:s4h:asignacion_de_credio:FIRM007';
            $body = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:swap:telemark:s4h:asignacion_credito:FIRM007'>" .
            "<soapenv:Header/>" .
                "<soapenv:Body>" .
                    "<urn:MT_FIRM007_REQ_OUT>" .
                    "<CLIENTE>" . $idsap . "</CLIENTE> ".
                    "<SOCIEDAD>2000</SOCIEDAD>" .
                    "<CLASE_DOC>1</CLASE_DOC>" .
                    "<YEAR>2021</YEAR>" .
                    "<DATE>22/06/2021</DATE>" .
                    "</urn:MT_FIRM007_REQ_OUT>" .
                "</soapenv:Body>" . 
                "</soapenv:Envelope>";
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $pass);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cache-Control: no-cache',
                'Content-Type: application/xml'
            ));
            $response = curl_exec($ch);

            curl_close($ch);

            if (empty( $response)) {
                return 'vacio';
            }

            $dom = new \DOMDocument;
            $dom->loadXML($response);
            
            $i = 0;
            $datos = array();

            foreach ($dom->getElementsByTagName('T_PARTIDAS') AS $item) {
                $datos[$i]['COD_SOCIEDAD'] = $item->getElementsByTagName('COD_SOCIEDAD')->item(0)->nodeValue;
                $datos[$i]['DOC_FINANCIERO'] = $item->getElementsByTagName('DOC_FINANCIERO')->item(0)->nodeValue;
                $datos[$i]['CLASE_DOC'] = $item->getElementsByTagName('CLASE_DOC')->item(0)->nodeValue;
                $datos[$i]['DOC_REFERENCIA'] = $item->getElementsByTagName('DOC_REFERENCIA')->item(0)->nodeValue;
                $datos[$i]['IMPORTE_MONEDA_LOC'] = $item->getElementsByTagName('IMPORTE_MONEDA_LOC')->item(0)->nodeValue;
                $datos[$i]['COD_SOLICITANTE'] = $item->getElementsByTagName('COD_SOLICITANTE')->item(0)->nodeValue;
                $datos[$i]['FECHA_DOC'] = $item->getElementsByTagName('FECHA_DOC')->item(0)->nodeValue;
                $datos[$i]['FECHA_DOC'] = substr($datos[$i]['FECHA_DOC'], 6, 2) . '/' . substr($datos[$i]['FECHA_DOC'], 4, 2) . '/' . substr($datos[$i]['FECHA_DOC'], 0, 4);
                $datos[$i]['CONDICION_PAGO'] = $item->getElementsByTagName('CONDICION_PAGO')->item(0)->nodeValue;
                $datos[$i]['DIAS_PLAZO'] = $item->getElementsByTagName('DIAS_PLAZO')->item(0)->nodeValue;
                $datos[$i]['FECHA_VENCIMIENTO'] = $item->getElementsByTagName('FECHA_VENCIMIENTO')->item(0)->nodeValue;
                $datos[$i]['FECHA_VENCIMIENTO'] = substr($datos[$i]['FECHA_VENCIMIENTO'], 6, 2) . '/' . substr($datos[$i]['FECHA_VENCIMIENTO'], 4, 2) . '/' . substr($datos[$i]['FECHA_VENCIMIENTO'], 0, 4);
                $datos[$i]['EJERCICIO'] = $item->getElementsByTagName('EJERCICIO')->item(0)->nodeValue;
                $datos[$i]['CON_PROMESA_PAGO'] = $item->getElementsByTagName('CON_PROMESA_PAGO')->item(0)->nodeValue;
                $datos[$i]['FECHA_VENC_PROMESA_PAGO'] = $item->getElementsByTagName('FECHA_VENC_PROMESA_PAGO')->item(0)->nodeValue;
                if ($datos[$i]['FECHA_VENC_PROMESA_PAGO'] != "null") {
                    $datos[$i]['FECHA_VENC_PROMESA_PAGO'] = substr($datos[$i]['FECHA_VENC_PROMESA_PAGO'], 6, 2) . '/' . substr($datos[$i]['FECHA_VENC_PROMESA_PAGO'], 4, 2) . '/' . substr($datos[$i]['FECHA_VENC_PROMESA_PAGO'], 0, 4);
                }
                $i++;
            }

            oraclePDO::ActualizaMa_partidas_abiertas($datos, $idsap);
            
            return $datos;

        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function validaConvenio(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::validaConvenio($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }
    
    public function observacionCliente(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::observacionCliente($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function grabaObserv(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rut = $request->get('rutcli');
        $observacion = $request->get('observacion');
        
        $resultado = oraclePDO::observacionCliente($rut);
        $cencos = $resultado[0]->cencos;
        
        $resultado = oraclePDO::grabaObserv($rut, $observacion, $cencos);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function descargaConvenio(request $request, $rutcli)
    {
        $respuesta = oraclePDO::Convenio($rutcli);

        $arrayData = array();
        $i = 1;
        $arrayData[0]['numcot'] = "Convenio";
        $arrayData[0]['codpro'] = "Codigo";
        $arrayData[0]['marca'] = "Marca";
        $arrayData[0]['descripcion'] = "Descripcion";
        $arrayData[0]['precio'] = "Precio";
        $arrayData[0]['fecemi'] = "fecha inicio";
        $arrayData[0]['fecven'] = "fecha fin";
        foreach ($respuesta as $data) {
            $arrayData[$i]['numcot'] = $data->numcot;
            $arrayData[$i]['codpro'] = $data->codpro;
            $arrayData[$i]['marca'] = $data->marca;
            $arrayData[$i]['descripcion'] = $data->descripcion;
            $arrayData[$i]['precio'] = $data->precio;
            $arrayData[$i]['fecemi'] = $data->fecemi;
            $arrayData[$i]['fecven'] = $data->fecven;
            $i++;
        }

        Excel::create('Convenio_' . $arrayData[1]['numcot'], function ($excel) use ($arrayData) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Convenio ' . $arrayData[1]['numcot']);
            $excel->setCreator('Dimerc')->setCompany('Dimerc S.A');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($arrayData) {
                $sheet->fromArray($arrayData, null, 'A1', false, false);
            });
        })->download('xlsx');

        return 'ok';
    }

    public function getContactos(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::getContactos($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getRelaciones(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = oraclePDO::getRelaciones($rutcliente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getfocosCliente(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

        $resultado = PortalEjUsuariosPDO::getfocosCliente($rutcliente, $rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getDetalleFichaClienteOracle(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcliente = $request->get('rutcli');

//        if(!isset($rutcli)){
//            $rutcliente = Session::get('searchclient');
//        }
//        else{
//            $rutcliente=$rutcli;
//
//            if($rutcli!=null&&$rutcli!=''){
//                Session::put('searchclient', $rutcli);
//            }
//        }

//        print_r($rutcliente);
        if (isset($rutcliente)) {
            if (!DeClientePDO::canSeeCliente($rutcliente, $rutusu)) {
                $msg = 'Rut no existe o no tiene permiso para visualizar el cliente buscado.';
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)
                    ->setResponse($msg);
                return $msg;
            }
            else{
                $cliente = EnClientePDO::getDetalleFichaCliente($rutcliente);
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(200)
                    ->setResponse($cliente);
                return $respuesta->returnToJson();
            }

        } else {
            return false;
        }
    }

    public function getDetalleFichaClienteMysql(Request $request)
    {
        $rutcliente = $request->get('rutcli');
        //$rutcliente = Session::get('searchclient');
        if (isset($rutcliente)) {

            $cliente = PortalEjUsuariosPDO::getDetalleFichaCliente($rutcliente);
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(200)
                ->setResponse($cliente);
            return $respuesta->returnToJson();
        } else {
            return false;
        }
    }

    public function getReporteCobranza(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $rutcli = $request->get('rutcli');
        
        if (!DeClientePDO::canSeeCliente($rutcli, $rutusu)) {
            $msg = false;
            return json_encode($msg);
        }
        else{
            $cliente = oraclePDO::getReporteCobranza($rutcli);
        }
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($cliente);
        return $respuesta->returnToJson();
    }

    public function getReporteCobranzaDatos(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $rutcli = $request->get('rutcli');
        
        if (!DeClientePDO::canSeeCliente($rutcli, $rutusu)) {
            $msg = false;
            return json_encode($msg);
        }
        else{
            $cliente = oraclePDO::getReporteCobranzaDatos($rutcli);
        }
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($cliente);
        return $respuesta->returnToJson();
    }

    public function getContactosAnotaciones($rutcli)
    {
        if (!isset($rutcli)) {
            return false;
        }
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $contactos = DeClientePDO::getContactsByRutcli($rutcli, $rutusu);
        $contact = array();
        $i = 0;
        foreach ($contactos as $contacto) {
            $contact[$i]['contacto'] = $contacto->contacto;
            $i++;
        }
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($contact);
        return $respuesta->returnToJson();
    }

    public function saveAnotaciones($rutcli, Request $request)
    {
        if (!isset($rutcli)) {
            return redirect()->to('login');
        }
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $inputs = $request->all();
        if (!DmVentasPDO::saveAnotacion($inputs, $codusu, $rutcli)) {
            Session::flash('error', 'Ocurrió un error al grabar la anotación, vuelva a intentar');
        } else {
            if (!PortalEjUsuariosPDO::saveAnotacion($inputs, $rutcli)) {
                Session::flash('error', 'Ocurrió un error al grabar la anotación, vuelva a intentar');
            } else {
                Session::flash('success', 'Anotación guardada con éxito');
            }
        }
        return redirect()->to('fichacliente/' . $rutcli);
    }

    public function getGraficoFicha(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $rutcli = $request->input('_rutcli', null);
        if (is_null($rutcli)) {
            return;
        }
        if (!DeClientePDO::canSeeCliente($rutcli, $rutusu)) {
            return;
        }
        else{
            $data = PortalEjUsuariosPDO::getGraficoFicha($rutcli);
            if (!$data) {
                return;
            }
            $array_data = array();
            $i = 0;
            foreach ($data as $d) {
                $array_data[$i]['date'] = $d->periodo;
                $array_data[$i]['Venta'] = $d->venta;
                $array_data[$i]['Brecha'] = $d->brecha;
                $array_data[$i]['Potencial'] = $d->potencial;
                $array_data[$i]['Margen'] = $d->margen;
                $i++;
            }
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(200)
                ->setResponse($array_data);
            return $respuesta->returnToJson();
        }
    }

    public function getDatosReportesCorp($tipo, $rut, $ano, $mes){
        try {

            $user = 'consultaDimerc';
            $pass = '3st4E5lAC0ntr4s3ña.';

            //$user, $pass, $tipo, $ano, $mes, $rut
            $cabecera = oraclePDO::getCabeceraReportesCorp($tipo, $rut, $mes);
            
            $reporte1 = $this->getReporteCliente1($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte2 = $this->getReporteCliente2($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte3 = $this->getReporteCliente3($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte4 = $this->getReporteCliente4($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte5 = $this->getReporteCliente5($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte6 = $this->getReporteCliente6($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte7 = $this->getReporteCliente7($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte8 = $this->getReporteCliente8($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte9 = $this->getReporteCliente9($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte10 = $this->getReporteCliente10($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte11 = $this->getReporteCliente11($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte12 = $this->getReporteCliente12($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte13 = $this->getReporteCliente13($user, $pass, $tipo,  $ano, $mes, $rut);
            $reporte14 = $this->getReporteCliente14($user, $pass, $tipo,  $ano, $mes, $rut);
            
            return View::make('back.pages.FormatoPDF.Cartola', ['cabecera' => $cabecera,
                                                            'reporte1' => $reporte1,
                                                            'reporte2' => $reporte2,
                                                            'reporte3' => json_decode($reporte3),
                                                            'reporte4' => json_decode($reporte4),
                                                            'reporte5' => json_decode($reporte5),
                                                            'reporte6' => json_decode($reporte6),
                                                            'reporte7' => json_decode($reporte7),
                                                            'reporte8' => json_decode($reporte8),
                                                            'reporte9' => json_decode($reporte9),
                                                            'reporte10' => json_decode($reporte10),
                                                            'reporte11' => json_decode($reporte11),
                                                            'reporte12' => json_decode($reporte12),
                                                            'reporte13' => json_decode($reporte13),
                                                            'reporte14' => json_decode($reporte14)]);

        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente1($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 1,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente2($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 2,
                    "vista": 2,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente3($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 3,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente4($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 4,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente5($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 5,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente6($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 6,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente7($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 7,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente8($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 8,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente9($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 9,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente10($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 10,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente11($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 11,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente12($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 12,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente13($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 13,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente14($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 14,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente2Vista1($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 2,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

}