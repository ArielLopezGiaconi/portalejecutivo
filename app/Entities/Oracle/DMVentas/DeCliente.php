<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 16:31
 */

namespace App\Entities\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class DeCliente implements \JsonSerializable {

    private $rutcli;
    private $cencos;
    private $descco;
    private $direcc;
    private $comuna;
    private $ciudad;
    private $fono01;
    private $numfax;
    private $contac;
    private $carcon;
    private $feclla;
    private $fecprx;
    private $feulco;
    private $moulco;
    private $lincre;
    private $credis;
    private $forpag;
    private $codest;
    private $codcob;
    private $telcob;
    private $descli;
    private $observ;
    private $rutven;
    private $concob;
    private $tipest;
    private $usrweb;
    private $obscob;
    private $dirdes;
    private $fecing;
    private $coding;
    private $plapag;
    private $creocu;
    private $fecblq;
    private $emails;
    private $faxcob;
    private $obscco;
    private $usr_creac;
    private $fecha_creac;
    private $mail_cob;
    private $fecactcob;
    private $dirret;
    private $comret;
    private $ciuret;
    private $recaudador;
    private $codret;
    private $mail_elec;
    private $dir_retiro;
    private $docfut;
    private $codemp;
    private $unidad_pago;
    private $id_cliente_sap;
    private $latitud;
    private $longitud;
    private $jaula_sucursal;
    private $tipo_via;
    private $nombre_via;
    private $numero;
    private $piso;
    private $depto;
    private $codigo_postal;
    private $validado_formulario;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->rutcli = isset($registro->rutcli) ? $registro->rutcli : null;
            $this->cencos = isset($registro->cencos) ? $registro->cencos : null;
            $this->descco = isset($registro->descco) ? $registro->descco : null;
            $this->direcc = isset($registro->direcc) ? $registro->direcc : null;
            $this->comuna = isset($registro->comuna) ? $registro->comuna : null;
            $this->ciudad = isset($registro->ciudad) ? $registro->ciudad : null;
            $this->fono01 = isset($registro->fono01) ? $registro->fono01 : null;
            $this->numfax = isset($registro->numfax) ? $registro->numfax : null;
            $this->contac = isset($registro->contac) ? $registro->contac : null;
            $this->carcon = isset($registro->carcon) ? $registro->carcon : null;
            $this->feclla = isset($registro->feclla) ? $registro->feclla : null;
            $this->fecprx = isset($registro->fecprx) ? $registro->fecprx : null;
            $this->feulco = isset($registro->feulco) ? $registro->feulco : null;
            $this->moulco = isset($registro->moulco) ? $registro->moulco : null;
            $this->lincre = isset($registro->lincre) ? $registro->lincre : null;
            $this->credis = isset($registro->credis) ? $registro->credis : null;
            $this->forpag = isset($registro->forpag) ? $registro->forpag : null;
            $this->codest = isset($registro->codest) ? $registro->codest : null;
            $this->codcob = isset($registro->codcob) ? $registro->codcob : null;
            $this->telcob = isset($registro->telcob) ? $registro->telcob : null;
            $this->descli = isset($registro->descli) ? $registro->descli : null;
            $this->observ = isset($registro->observ) ? $registro->observ : null;
            $this->rutven = isset($registro->rutven) ? $registro->rutven : null;
            $this->concob = isset($registro->concob) ? $registro->concob : null;
            $this->tipest = isset($registro->tipest) ? $registro->tipest : null;
            $this->usrweb = isset($registro->usrweb) ? $registro->usrweb : null;
            $this->obscob = isset($registro->obscob) ? $registro->obscob : null;
            $this->dirdes = isset($registro->dirdes) ? $registro->dirdes : null;
            $this->fecing = isset($registro->fecing) ? $registro->fecing : null;
            $this->coding = isset($registro->coding) ? $registro->coding : null;
            $this->plapag = isset($registro->plapag) ? $registro->plapag : null;
            $this->creocu = isset($registro->creocu) ? $registro->creocu : null;
            $this->fecblq = isset($registro->fecblq) ? $registro->fecblq : null;
            $this->emails = isset($registro->emails) ? $registro->emails : null;
            $this->faxcob = isset($registro->faxcob) ? $registro->faxcob : null;
            $this->obscco = isset($registro->obscco) ? $registro->obscco : null;
            $this->usr_creac = isset($registro->usr_creac) ? $registro->usr_creac : null;
            $this->fecha_creac = isset($registro->fecha_creac) ? $registro->fecha_creac : null;
            $this->mail_cob = isset($registro->mail_cob) ? $registro->mail_cob : null;
            $this->fecactcob = isset($registro->fecactcob) ? $registro->fecactcob : null;
            $this->dirret = isset($registro->dirret) ? $registro->dirret : null;
            $this->comret = isset($registro->comret) ? $registro->comret : null;
            $this->ciuret = isset($registro->ciuret) ? $registro->ciuret : null;
            $this->recaudador = isset($registro->recaudador) ? $registro->recaudador : null;
            $this->codret = isset($registro->codret) ? $registro->codret : null;
            $this->mail_elec = isset($registro->mail_elec) ? $registro->mail_elec : null;
            $this->dir_retiro = isset($registro->dir_retiro) ? $registro->dir_retiro : null;
            $this->docfut = isset($registro->docfut) ? $registro->docfut : null;
            $this->codemp = isset($registro->codemp) ? $registro->codemp : null;
            $this->unidad_pago = isset($registro->unidad_pago) ? $registro->unidad_pago : null;
            $this->id_cliente_sap = isset($registro->id_cliente_sap) ? $registro->id_cliente_sap : null;
            $this->latitud = isset($registro->latitud) ? $registro->latitud : null;
            $this->longitud = isset($registro->longitud) ? $registro->longitud : null;
            $this->jaula_sucursal = isset($registro->jaula_sucursal) ? $registro->jaula_sucursal : null;
            $this->tipo_via = isset($registro->tipo_via) ? $registro->tipo_via : null;
            $this->nombre_via = isset($registro->nombre_via) ? $registro->nombre_via : null;
            $this->numero = isset($registro->numero) ? $registro->numero : null;
            $this->piso = isset($registro->piso) ? $registro->piso : null;
            $this->depto = isset($registro->depto) ? $registro->depto : null;
            $this->codigo_postal = isset($registro->codigo_postal) ? $registro->codigo_postal : null;
            $this->validado_formulario = isset($registro->validado_formulario) ? $registro->validado_formulario : null;
        }
    }

    public function getRutcli()
    {
        return $this->rutcli;
    }

    public function setRutcli($rutcli)
    {
        $this->rutcli = $rutcli;
    }

    public function getCencos()
    {
        return $this->cencos;
    }

    public function setCencos($cencos)
    {
        $this->cencos = $cencos;
    }

    public function getDescco()
    {
        return $this->descco;
    }

    public function setDescco($descco)
    {
        $this->descco = $descco;
    }

    public function getDirecc()
    {
        return $this->direcc;
    }

    public function setDirecc($direcc)
    {
        $this->direcc = $direcc;
    }

    public function getComuna()
    {
        return $this->comuna;
    }

    public function setComuna($comuna)
    {
        $this->comuna = $comuna;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    public function getFono01()
    {
        return $this->fono01;
    }

    public function setFono01($fono01)
    {
        $this->fono01 = $fono01;
    }

    public function getNumfax()
    {
        return $this->numfax;
    }

    public function setNumfax($numfax)
    {
        $this->numfax = $numfax;
    }

    public function getContac()
    {
        return $this->contac;
    }

    public function setContac($contac)
    {
        $this->contac = $contac;
    }

    public function getCarcon()
    {
        return $this->carcon;
    }

    public function setCarcon($carcon)
    {
        $this->carcon = $carcon;
    }

    public function getFeclla()
    {
        return $this->feclla;
    }

    public function setFeclla($feclla)
    {
        $this->feclla = $feclla;
    }

    public function getFecprx()
    {
        return $this->fecprx;
    }

    public function setFecprx($fecprx)
    {
        $this->fecprx = $fecprx;
    }

    public function getFeulco()
    {
        return $this->feulco;
    }

    public function setFeulco($feulco)
    {
        $this->feulco = $feulco;
    }

    public function getMoulco()
    {
        return $this->moulco;
    }

    public function setMoulco($moulco)
    {
        $this->moulco = $moulco;
    }

    public function getLincre()
    {
        return $this->lincre;
    }

    public function setLincre($lincre)
    {
        $this->lincre = $lincre;
    }

    public function getCredis()
    {
        return $this->credis;
    }

    public function setCredis($credis)
    {
        $this->credis = $credis;
    }

    public function getForpag()
    {
        return $this->forpag;
    }

    public function setForpag($forpag)
    {
        $this->forpag = $forpag;
    }

    public function getCodest()
    {
        return $this->codest;
    }

    public function setCodest($codest)
    {
        $this->codest = $codest;
    }

    public function getCodcob()
    {
        return $this->codcob;
    }

    public function setCodcob($codcob)
    {
        $this->codcob = $codcob;
    }

    public function getTelcob()
    {
        return $this->telcob;
    }

    public function setTelcob($telcob)
    {
        $this->telcob = $telcob;
    }

    public function getDescli()
    {
        return $this->descli;
    }

    public function setDescli($descli)
    {
        $this->descli = $descli;
    }

    public function getObserv()
    {
        return $this->observ;
    }

    public function setObserv($observ)
    {
        $this->observ = $observ;
    }

    public function getRutven()
    {
        return $this->rutven;
    }

    public function setRutven($rutven)
    {
        $this->rutven = $rutven;
    }

    public function getConcob()
    {
        return $this->concob;
    }

    public function setConcob($concob)
    {
        $this->concob = $concob;
    }

    public function getTipest()
    {
        return $this->tipest;
    }

    public function setTipest($tipest)
    {
        $this->tipest = $tipest;
    }

    public function getUsrweb()
    {
        return $this->usrweb;
    }

    public function setUsrweb($usrweb)
    {
        $this->usrweb = $usrweb;
    }

    public function getObscob()
    {
        return $this->obscob;
    }

    public function setObscob($obscob)
    {
        $this->obscob = $obscob;
    }

    public function getDirdes()
    {
        return $this->dirdes;
    }

    public function setDirdes($dirdes)
    {
        $this->dirdes = $dirdes;
    }

    public function getFecing()
    {
        return $this->fecing;
    }

    public function setFecing($fecing)
    {
        $this->fecing = $fecing;
    }

    public function getCoding()
    {
        return $this->coding;
    }

    public function setCoding($coding)
    {
        $this->coding = $coding;
    }

    public function getPlapag()
    {
        return $this->plapag;
    }

    public function setPlapag($plapag)
    {
        $this->plapag = $plapag;
    }

    public function getCreocu()
    {
        return $this->creocu;
    }

    public function setCreocu($creocu)
    {
        $this->creocu = $creocu;
    }

    public function getFecblq()
    {
        return $this->fecblq;
    }

    public function setFecblq($fecblq)
    {
        $this->fecblq = $fecblq;
    }

    public function getEmails()
    {
        return $this->emails;
    }

    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    public function getFaxcob()
    {
        return $this->faxcob;
    }

    public function setFaxcob($faxcob)
    {
        $this->faxcob = $faxcob;
    }

    public function getObscco()
    {
        return $this->obscco;
    }

    public function setObscco($obscco)
    {
        $this->obscco = $obscco;
    }

    public function getUsrCreac()
    {
        return $this->usr_creac;
    }

    public function setUsrCreac($usr_creac)
    {
        $this->usr_creac = $usr_creac;
    }

    public function getFechaCreac()
    {
        return $this->fecha_creac;
    }

    public function setFechaCreac($fecha_creac)
    {
        $this->fecha_creac = $fecha_creac;
    }

    public function getMailCob()
    {
        return $this->mail_cob;
    }

    public function setMailCob($mail_cob)
    {
        $this->mail_cob = $mail_cob;
    }

    public function getFecactcob()
    {
        return $this->fecactcob;
    }

    public function setFecactcob($fecactcob)
    {
        $this->fecactcob = $fecactcob;
    }

    public function getDirret()
    {
        return $this->dirret;
    }

    public function setDirret($dirret)
    {
        $this->dirret = $dirret;
    }

    public function getComret()
    {
        return $this->comret;
    }

    public function setComret($comret)
    {
        $this->comret = $comret;
    }

    public function getCiuret()
    {
        return $this->ciuret;
    }

    public function setCiuret($ciuret)
    {
        $this->ciuret = $ciuret;
    }

    public function getRecaudador()
    {
        return $this->recaudador;
    }

    public function setRecaudador($recaudador)
    {
        $this->recaudador = $recaudador;
    }

    public function getCodret()
    {
        return $this->codret;
    }

    public function setCodret($codret)
    {
        $this->codret = $codret;
    }

    public function getMailElec()
    {
        return $this->mail_elec;
    }

    public function setMailElec($mail_elec)
    {
        $this->mail_elec = $mail_elec;
    }

    public function getDirRetiro()
    {
        return $this->dir_retiro;
    }

    public function setDirRetiro($dir_retiro)
    {
        $this->dir_retiro = $dir_retiro;
    }

    public function getDocfut()
    {
        return $this->docfut;
    }

    public function setDocfut($docfut)
    {
        $this->docfut = $docfut;
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getUnidadPago()
    {
        return $this->unidad_pago;
    }

    public function setUnidadPago($unidad_pago)
    {
        $this->unidad_pago = $unidad_pago;
    }

    public function getIdClienteSap()
    {
        return $this->id_cliente_sap;
    }

    public function setIdClienteSap($id_cliente_sap)
    {
        $this->id_cliente_sap = $id_cliente_sap;
    }

    public function getLatitud()
    {
        return $this->latitud;
    }

    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    public function getLongitud()
    {
        return $this->longitud;
    }

    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    public function getJaulaSucursal()
    {
        return $this->jaula_sucursal;
    }

    public function setJaulaSucursal($jaula_sucursal)
    {
        $this->jaula_sucursal = $jaula_sucursal;
    }

    public function getTipoVia()
    {
        return $this->tipo_via;
    }

    public function setTipoVia($tipo_via)
    {
        $this->tipo_via = $tipo_via;
    }

    public function getNombreVia()
    {
        return $this->nombre_via;
    }

    public function setNombreVia($nombre_via)
    {
        $this->nombre_via = $nombre_via;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    public function getPiso()
    {
        return $this->piso;
    }

    public function setPiso($piso)
    {
        $this->piso = $piso;
    }

    public function getDepto()
    {
        return $this->depto;
    }

    public function setDepto($depto)
    {
        $this->depto = $depto;
    }

    public function getCodigoPostal()
    {
        return $this->codigo_postal;
    }

    public function setCodigoPostal($codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    public function getValidadoFormulario()
    {
        return $this->validado_formulario;
    }

    public function setValidadoFormulario($validado_formulario)
    {
        $this->validado_formulario = $validado_formulario;
    }

    public function jsonSerialize()
    {
        return [
            'rutcli' => $this->rutcli,
            'cencos' => $this->cencos,
            'descco' => $this->descco,
            'direcc' => $this->direcc,
            'comuna' => $this->comuna,
            'ciudad' => $this->ciudad,
            'fono01' => $this->fono01,
            'numfax' => $this->numfax,
            'contac' => $this->contac,
            'carcon' => $this->carcon,
            'feclla' => $this->feclla,
            'fecprx' => $this->fecprx,
            'feulco' => $this->feulco,
            'moulco' => $this->moulco,
            'lincre' => $this->lincre,
            'credis' => $this->credis,
            'forpag' => $this->forpag,
            'codest' => $this->codest,
            'codcob' => $this->codcob,
            'telcob' => $this->telcob,
            'descli' => $this->descli,
            'observ' => $this->observ,
            'rutven' => $this->rutven,
            'concob' => $this->concob,
            'tipest' => $this->tipest,
            'usrweb' => $this->usrweb,
            'obscob' => $this->obscob,
            'dirdes' => $this->dirdes,
            'fecing' => $this->fecing,
            'coding' => $this->coding,
            'plapag' => $this->plapag,
            'creocu' => $this->creocu,
            'fecblq' => $this->fecblq,
            'emails' => $this->emails,
            'faxcob' => $this->faxcob,
            'obscco' => $this->obscco,
            'usr_creac' => $this->usr_creac,
            'fecha_creac' => $this->fecha_creac,
            'mail_cob' => $this->mail_cob,
            'fecactcob' => $this->fecactcob,
            'dirret' => $this->dirret,
            'comret' => $this->comret,
            'ciuret' => $this->ciuret,
            'recaudador' => $this->recaudador,
            'codret' => $this->codret,
            'mail_elec' => $this->mail_elec,
            'dir_retiro' => $this->dir_retiro,
            'docfut' => $this->docfut,
            'codemp' => $this->codemp,
            'unidad_pago' => $this->unidad_pago,
            'id_cliente_sap' => $this->id_cliente_sap,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud,
            'jaula_sucursal' => $this->jaula_sucursal,
            'tipo_via' => $this->tipo_via,
            'nombre_via' => $this->nombre_via,
            'numero' => $this->numero,
            'piso' => $this->piso,
            'depto' => $this->depto,
            'codigo_postal' => $this->codigo_postal,
            'validado_formulario' => $this->validado_formulario,
        ];
    }

}