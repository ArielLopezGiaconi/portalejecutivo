<?php


namespace App\Http\Controllers\back;
use App\EPDO\MySQL\PricingServer\PortalEjecutivo\PortalEjUsuariosEPDO;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use App\PDO\Oracle\DMVentas\EnCotizacPDO;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PDF;
use Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Mail;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use View;
use Swift_Attachment;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CotizacionController
{
    public function generarPdfCotizacion(Request $request){
        Session::pull('productos_coti');
        $productos = $request->get('productos');
        $totales = $request->get('totales');
        $numcot = $request->get('numcot');

        foreach ($productos as $pro){
            Session::push('productos_coti', $pro);
        }

        Session::put('totales_coti', $totales[0]);
    }

    public function getPdfCotizacionSimple($numcot){
        $curl = curl_init(); //obtenemos la cotización mediante API
        $jsonBody = '{"numcot" : '.$numcot.'}';
        curl_setopt_array($curl, array(
            CURLOPT_URL => env("URL_API")."/api/cotizacion/exportar-cotizacion",
            CURLOPT_USERPWD => "inteligenciadimerc:iNt3l1g3nt3",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonBody,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        $result = json_decode($response);
        if($result->code != 200){
            return $result->response;
        }
        else{
            //pasamos los parámetros de cotización en base a lo enviado por la API
            $productos =array();
            foreach($result->response->detalle_coti as $prod){
                array_push($productos, get_object_vars($prod));
            }
            $totales = get_object_vars($result->response->encabezado_coti[0]);
            //construimos HTML
            $html = $this->construyeHtmlCotizacion($productos, $totales);
            //cargamos html a PDF
            $pdf = PDF::loadHTML($html);
            $nombrearchivo = 'cotizacion_'.$totales['numcot'].'.pdf';
            //devolvemos el binario
            return $pdf->download($nombrearchivo);
        }
    }

    public function getExcelCotizacionSimple($numcot){
        $curl = curl_init(); //obtenemos la cotización mediante API
        $jsonBody = '{"numcot" : '.$numcot.'}';
        curl_setopt_array($curl, array(
            CURLOPT_URL => env("URL_API")."/api/cotizacion/exportar-cotizacion",
            CURLOPT_USERPWD => "inteligenciadimerc:iNt3l1g3nt3",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonBody,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        $result = json_decode($response);
        //pasamos los parámetros de cotización en base a lo enviado por la API
        $productos =array();
        foreach($result->response->detalle_coti as $prod){
            array_push($productos, array($prod->codpro, $prod->precio, $prod->cantid));
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'codigo/sku');
        $sheet->setCellValue('B1', 'precio');
        $sheet->setCellValue('C1', 'cantidad');

        $spreadsheet->getActiveSheet()
        ->fromArray($productos, NULL, 'A2');

        $writer = new Xlsx($spreadsheet);

        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="cotizacion_'.$numcot.'.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');
        return $response;
    }

    public function moneda_chilena2($numero){
        $numero = (string)$numero;
        $puntos = floor((strlen($numero)-1)/3);
        $tmp = "";
        $pos = 1;
        for($i=strlen($numero)-1; $i>=0; $i--){
            $tmp = $tmp.substr($numero, $i, 1);
            if($pos%3==0 && $pos!=strlen($numero))
                $tmp = $tmp.".";
            $pos = $pos + 1;
        }
        $formateado = "$ ".strrev($tmp);
        return $formateado;
    } 

    public function getCotizaciones(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $busqueda = $request->get('busqueda');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $cartera = $request->get('cartera');
        $tipo = $request->get('tipo');
        $status = $request->get('status');
        
        $resultado = PortalEjUsuariosPDO::getCotizaciones($rutusu, $busqueda, $fechaDesde, $fechaHasta, $tipo, $cartera, $status);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCovidComuna(Request $request) {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');
        
        $resultado = PortalEjUsuariosPDO::getCovidComuna($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function EliminaNotaWeb(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $observ = $request->get('observ');
        $numord = $request->get('numord');
        $rutcli = $request->get('rutcli');
        $fecha = $request->get('fecha');
        
        $resultado = oraclePDO::EliminaNotaWeb($codusu, $observ, $numord, $rutcli);
        $resultado = oraclePDO::RegistroAccion($fecha, $codusu, $observ, $numord, $rutcli);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getStatusNota(Request $request) {
        $usuario = Session::get('usuario');

        $nota = $request->get('numnvt');
        
        $resultado = oraclePDO::getStatusNota($nota);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getInfoEstadoNota1(Request $request) {
        $usuario = Session::get('usuario');

        $nota = $request->get('numnvt');
        
        $resultado = oraclePDO::getInfoStatus($nota, 1);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getInfoEstadoNota2(Request $request) {
        $usuario = Session::get('usuario');

        $nota = $request->get('numnvt');
        
        $resultado = oraclePDO::getInfoStatus($nota, 2);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getInfoEstadoNota3(Request $request) {
        $usuario = Session::get('usuario');

        $nota = $request->get('numnvt');
        
        $resultado = oraclePDO::getInfoStatus($nota, 3);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getInfoEstadoNota4(Request $request) {
        $usuario = Session::get('usuario');

        $nota = $request->get('numnvt');
        $tipo = $request->get('tipo');
        
        $resultado = oraclePDO::getInfoStatus($nota, 4);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getComisionesPortal(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $fecha = $request->get('fecha');
        
        $resultado = PortalEjUsuariosPDO::getComisionesPortal($rutusu, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getComisiones1(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $fecha = $request->get('fecha');
        
        $resultado = PortalEjUsuariosPDO::getComisiones1($rutusu, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getComisiones2(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $fecha = $request->get('fecha');
        
        $resultado = PortalEjUsuariosPDO::getComisiones2($rutusu, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCotizacionesRut(Request $request) {
        $usuario = Session::get('usuario');
        $rut = $request->get('rut');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $status = $request->get('status');
        
        $resultado = oraclePDO::getCotizacionesRut($rut, $fechaDesde, $fechaHasta, $status);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasRut(Request $request) {
        $usuario = Session::get('usuario');
        $rut = $request->get('rut');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $status = $request->get('status');
        
        $resultado = oraclePDO::getNotasRut($rut, $fechaDesde, $fechaHasta, $status);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    } 

    public function getPeriodosComisiones(Request $request) {
        $usuario = Session::get('usuario');
        
        $resultado = PortalEjUsuariosPDO::getPeriodosComisiones();
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function ingresosWeb(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $rutusu = $usuario->get('rutusu');
        $cartera = $request->get('cartera');
        
        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 2, $cartera);

        $resultado = PortalEjUsuariosPDO::ingresosWeb($resultado);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function ingresosWebQ() {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        
        $resultado = PortalEjUsuariosPDO::ingresosWebQ($codusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasWeb(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');
        $cartera = $request->get('cartera');

        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 1, $cartera);
        
        $resultado = oraclePDO::getNotasWeb($resultado, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasWebHist(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');
        $cartera = $request->get('cartera');
        $rutcli = $request->get('rut');

        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 1, $cartera);
        
        $resultado = oraclePDO::getNotasWebHist($resultado, $fecha, $rutcli);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasWebQ(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');
        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 1, 1);
        
        $resultado = oraclePDO::getNotasWebQ($resultado, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotas(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $busqueda = $request->get('busqueda');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $cartera = $request->get('cartera');
        $tipo = $request->get('tipo');
        $status = $request->get('status');

    
        $resultado = PortalEjUsuariosPDO::getNotas($rutusu, $busqueda, $fechaDesde, $fechaHasta, $tipo, $cartera, $status);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasSinDocumentar(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $busqueda = $request->get('busqueda');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $cartera = $request->get('cartera');
        $tipo = $request->get('tipo');
        $status = $request->get('status');

    
        $resultado = oraclePDO::getNotasSinDocumentar($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getNotasSinDocumentarQ(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $busqueda = $request->get('busqueda');
        $fechaDesde = $request->get('fechaDesde');
        $fechaHasta = $request->get('fechaHasta');
        $cartera = $request->get('cartera');
        $tipo = $request->get('tipo');
        $status = $request->get('status');

    
        $resultado = oraclePDO::getNotasSinDocumentarQ($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getContactosAnotaciones(Request $request)
    {
        $rutcli = $request->get('rutcli');
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $contactos = DeClientePDO::getMailsByRutcli($rutcli, $rutusu);
        
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($contactos);
        return $respuesta->returnToJson();
    }

    public function construyeHtmlCotizacion($productos = false, $totales = false){
        if($productos == false || $totales == false){
            $productos = Session::get('productos_coti');
            $totales = Session::get('totales_coti');
        }
        $cantidad_prod = count($productos);
        $limiteProdsPorPagina = 29;
        $contador_item=1;
        $first_item=false;
        $contador=0;
        $limite=29;
        $cantidad_pag=0;
        $cantidad_prod_pag=[];
        $productos_pag=[];
        $cobrologistico=0;

        if($cantidad_prod>$limiteProdsPorPagina){

            foreach($productos as $pro){
                if($pro['codpro']=="WW00008"){
                    $cobrologistico=intval($pro['precio']);
                }
                else{
                    $contador++;
                    if($contador_item==1)
                    {
                        $first_item=true;

                        //Calcula items por pag************************
                        $init=($limite-$limiteProdsPorPagina);
                        $items=0;
                        if($init==0){
                            $items=$limiteProdsPorPagina;
                        }
                        else{
                            $saldo = $cantidad_prod-$init;
                            if($saldo>$limiteProdsPorPagina){
                                $items=$limiteProdsPorPagina;
                            }
                            else{
                                $items=$saldo;
                            }
                        }
                        array_push($cantidad_prod_pag, $items);
                        //**********************************************
                    }
                    else{
                        $first_item=false;
                    }

                    if($contador<=$limite){
                        if($first_item){
                            $cantidad_pag++;
                        }
                        $contador_item++;
                    }
                    if($contador==$limite){
                        $contador_item=1;
                        $limite=$limite+$limiteProdsPorPagina;
                    }
                }
            }

            $prod=[];
            $cuenta_prod=0;
            $contador_pag=0;
            $count=0;

            foreach ($productos as $pro){
                $count++;
                $cuenta_prod++;

                $limit_pag = $cantidad_prod_pag[($contador_pag)];

                if($cuenta_prod<=$limit_pag){
                    array_push($prod, $pro);

                    if($cuenta_prod==$limit_pag){

                        array_push($productos_pag, $prod);
                    }
                }
                if($cuenta_prod==$limit_pag){
                    $contador_pag++;
                    $cuenta_prod=0;
                    $prod=[];
                }
            }
        }
        else{
            foreach($productos as $pro){
                if($pro['codpro']=="WW00008"){
                    $cobrologistico=intval($pro['precio']);
                }
            }

            $cantidad_pag=1;
            array_push($productos_pag, $productos);
        }

        //print_r($cantidad_prod_pag);

        $contador_pag=1;
        $encabezado='';
        $footer='';
        $html='';
        $imagen1='';
        $imagen2='';
        $table='';
        $margin1=-50;
        $margin2=40;
        $margin3=110;
        $margin4=100;
        $margin5=80;
        $urlFont=resource_path().'\\assets\\fonts\\Abel-Regular.ttf';
        $encabezado = '<!DOCTYPE html>
                <html>
                <head>
                    <style>
                        @font-face {
                            font-family: \'Abel\';
                            src: url("'.$urlFont.'") format("truetype");
                            font-style: normal; // use the matching font-style here
                        }
                        @font-face {
                            font-family: \'Abel\';
                            src: url("'.$urlFont.'") format("truetype");
                            font-style: bold; // use the matching font-style here
                        }
                        @font-face {
                            font-family: \'Abel\';
                            src: url("'.$urlFont.'") format("truetype");
                            font-style: italic; // use the matching font-style here
                        }
                        @font-face {
                            font-family: \'Abel\';
                            src: url("'.$urlFont.'") format("truetype");
                            font-weight: bold; // use the matching font-style here
                            font-style: oblique;
                        }
						#tablazo th {
						  text-align: left;
                          font-family: \'Abel\', normal;
						  background-color: #f2f2f2;
						}
						#tablazo tr:nth-child(even){background-color: #f2f2f2; font-family: \'Abel\', normal;}
                        body {
                          background-image: url("'. asset('assets/1/img/bg_cotizacion_.png') .'");
                          background-repeat: repeat-y;
                          font-family: \'Abel\', normal;

                        }
                        .bg{

                            background: url('. asset('assets/1/img/bg_cotizacion.png') .') no-repeat center center fixed;
                            -webkit-background-size: cover;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;
                        }
                        .page-break {
                            page-break-after: always;
                        }
                    </style>
                </head>
                <body>
                <div>
                    <div style="float: left;margin-top:'.$margin2.'px;margin-left:280px;font-size:20px;font-family: \'Abel\', normal;display:block;position:absolute;z-index:2;">
                        <span><b style="font-family: \'Abel\', normal;">N° '. $totales['numcot'] .'</b></span>
                    </div>
                    <div style="margin-top:60px;float:left;left: 10px; display: block;clear: both;position:absolute;z-index:3;font-family: \'Abel\', normal;">
                        <div style="float: left">
                            <table style="font-size: 12px;font-family: \'Abel\', normal;float:left;">
                                <tbody>
                                <tr>
                                    <td><b style="font-family: \'Abel\', normal;">CLIENTE:</b></td>
                                    <td>'. $totales['razons'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>RUT:</b></td>
                                    <td>'. $totales['rutcli'] .'-'. $totales['digcli'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>C.COSTO:</b></td>
                                    <td>'. $totales['desc_ceco'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>DIRECCIÓN:</b></td>
                                    <td>'. $totales['direc_ceco'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>COMUNA:</b></td>
                                    <td>'. $totales['comuna'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>CONDICIÓN:</b></td>
                                    <td>Cotización válida hasta el '. $totales['fecven'] .' o hasta agotar stock</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="float: left;font-family: \'Abel\', normal;">
                            <table style="font-size:12px;font-family: \'Abel\', normal;float: left;margin-left:400px;">
                                <tbody>
                                <tr>
                                    <td><b>F. emisión:</b></td>
                                    <td>'. $totales['fecemi'] .'</td>
                                </tr>
                                <tr>
                                    <td><b>F. vencimiento:</b></td>
                                    <td>'. $totales['fecven'] .'</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <p style="margin-top:'.$margin4.'px;">_______________________________________________________________________________________</p>
                        <div style="float: left">
                ';


        $footer='<p>_______________________________________________________________________________________</p>
                            <div style="float: left;clear:both;">
                                <table style="font-size:12px;font-family: \'Abel\', normal;float: left;margin-left:440px;">
                                    <tbody>                                    
                                    <tr>
                                        <td><b>Neto:</b></td>
                                        <td>'. $this->moneda_chilena2(($totales['neto']-$cobrologistico)) .'</td>
                                    </tr>
                                    <tr>
                                        <td><b>Cobro logístico:</b></td>
                                        <td>'. $this->moneda_chilena2($cobrologistico) .'</td>
                                    </tr>
                                    <tr>
                                        <td><b>Subtotal:</b></td>
                                        <td>'. $this->moneda_chilena2($totales['subtotal']) .'</td>
                                    </tr>
                                    <tr>
                                        <td><b>Iva:</b></td>
                                        <td>'. $this->moneda_chilena2($totales['iva']) .'</td>
                                    </tr>
                                    <tr>
                                        <td><b>Total:</b></td>
                                        <td>'. $this->moneda_chilena2($totales['total']) .'</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="float:left;margin-top:'.$margin5.'px;bottom:0;">
                                <p style="font-size:12px;font-family: \'Abel\', normal;">Saludos cordiales,</p>

                                <table style="font-size:12px;font-family: \'Abel\', normal;float: left;margin-left:400px;padding: 0px 0px 0px 0px;">
                                    <tbody>
                                    <tr><td><b style="font-family: \'Abel\', normal;">Ejecutiv@ Comercial</b></td></tr>
                                    <tr><td><b style="font-family: \'Abel\', normal;">'. ucwords(strtolower(Session::get('usuario')->get('nombre').' '.Session::get('usuario')->get('apepat').' '.Session::get('usuario')->get('apemat'))) .'</b></td></tr>
                                    <tr><td><b style="font-family: \'Abel\', normal;">Fono directo: +56 22 385 '. Session::get('usuario')->get('anexos') .'</b></td></tr>
                                    <tr><td><b style="font-family: \'Abel\', normal;">'. Session::get('usuario')->get('mail01') .'</b></td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                </body>
                </html>';

        while($contador_pag<=$cantidad_pag){
            $prod_recorrer = $productos_pag[($contador_pag-1)];

            $rows_table='';

            foreach($prod_recorrer as $p){
                if($p['codpro']!='WW00008'){
                    $rows_table = $rows_table . '<tr>
                                <td>'.$p['codpro'].'</td>
                                <td>'.$p['despro'].'</td>
                                <td>'.$p['desmar'].'</td>
                                <td>'.$p['und'].'</td>
                                <td>'.$p['cantid'].'</td>
                                <td>'.$this->moneda_chilena2($p['precio']).'</td>
                                <td>'.$this->moneda_chilena2($p['totnet']).'</td>
                            </tr>';
                }
            }

            if($contador_pag==1){
                if($contador_pag==$cantidad_pag){
                    $table = $table . '
                            <table id="tablazo" style="font-size: 11px;font-family: \'Abel\', normal;width:730px;margin-top:0px;">
                                <thead>
                                <tr>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Código</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Descripción</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Marca</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Und</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Cantidad</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Precio</th>
                                    <th style="text-align: left;font-family: \'Abel\', normal;">Total</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_coti_pdf">
                                '.$rows_table.'
                                </tbody>
                            </table>
                            '.$footer.'
                           ';
                }
                else{
                    $table = $table . '
                            <table style="font-size: 11px;font-family: \'Abel\', normal;width:730px;margin-top:0px;">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Descripción</th>
                                    <th>Marca</th>
                                    <th>Und</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_coti_pdf">
                                '.$rows_table.'
                                </tbody>
                            </table>
                            <div class="page-break"></div>
                           ';
                }

            }
            else{
                if($contador_pag==$cantidad_pag){
                    $table = $table . '
                            <div style="float: left;margin-top:'.$margin2.'px;margin-left:280px;font-size:20px;font-family: \'Abel\', normal;display:block;position:absolute;z-index:2;">
                                <span><b>N° '. $totales['numcot'] .'</b></span>
                            </div>
                            <table style="font-size: 11px;width:730px;margin-top:100px;font-family: \'Abel\', normal;">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Descripción</th>
                                    <th>Marca</th>
                                    <th>Und</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_coti_pdf" style="font-family: \'Abel\', normal;">
                                '.$rows_table.'

                                </tbody>
                            </table>
                            '.$footer.'
                           ';
                }
                else{
                    $table = $table . '
                            <table style="font-size: 11px;font-family: \'Abel\', normal;width:730px;margin-top:100px;">
                                <thead>
                                <tr">
                                    <th>Código</th>
                                    <th>Descripción</th>
                                    <th>Marca</th>
                                    <th>Und</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_coti_pdf" style="font-family: \'Abel\', normal;">
                                '.$rows_table.'
                                </tbody>
                            </table>
                            <div class="page-break"></div>
                           ';
                }
            }

            $contador_pag++;
        }
        $html = $encabezado . $table;
        return $html;
    }

    public function descargaPdfCotizacion($numcot){
        $html = $this->construyeHtmlCotizacion();
        $pdf = PDF::loadHTML($html);
		//echo($html);die();
        return $pdf->download('cotizacion_'.$numcot.'.pdf');
    }

    public function guardaPdfCotizacion($numcot){

        $html = $this->construyeHtmlCotizacion();
        $pdf = PDF::loadHTML($html);
        $nombrearchivo = 'cotizacion_'.$numcot.'.pdf';
        $r1 = Storage::disk('pdf')->put($nombrearchivo, $pdf);
        $pdf->save(storage_path().'/archivos/pdf/'.$nombrearchivo);

        $rutaarchivo = storage_path('archivos/pdf/');

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse(array(
                'rutaarchivo' => $rutaarchivo,
                'nombrearchivo' => $nombrearchivo
            ));
        return $respuesta->returnToJson();
    }

    public function generarPdfCotizacionAndSave(Request $request){
        Session::pull('productos_coti');
        $productos = $request->get('productos');
        $totales = $request->get('totales');

        /*foreach ($productos as $pro){
            Session::push('productos_coti', $pro);
        }

        Session::put('totales_coti', $totales[0]);*/

        $numcoti = $totales[0]['numcot'];

        //Genera y guarda archivo pdf
//        $pdf = PDF::loadView('back.pages.cliente.fichacliente.cotizacionpdf');
        $html = $this->construyeHtmlCotizacion($productos, $totales[0]);
        $pdf = PDF::loadHTML($html);
        $nombrearchivo = 'cotizacion_'.$numcoti.'.pdf';
        $r1 = Storage::disk('pdf')->put($nombrearchivo, $pdf);
        $pdf->save(storage_path().'/archivos/pdf/'.$nombrearchivo);

        $rutaarchivo = storage_path('archivos/pdf/');
        //return $rutaarchivo;

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse(array(
                'rutaarchivo' => $rutaarchivo,
                'nombrearchivo' => $nombrearchivo
            ));
        return $respuesta->returnToJson();
    }

    public function importExcelCotizacion(Request $request){
		$cotizacion=array();
		$autoCoti = "";
		$coddir = "";
		if(array_key_exists('autoCoti', $request->all())){
			$autoCoti = $request->get('autoCoti', null);
			$coddir = $request->get('coddir', null);
			if($autoCoti == null){
				$autoCoti = "";
			}
		}
		if($autoCoti!=""){
			
			$prods = EnCotizacPDO::getAutoCoti($autoCoti);
			
			foreach($prods as $prod){
				$datos[0] = $prod["codpro"];
				$datos[1] = 1;
				$datos[2] = 1;
				array_push($cotizacion, $datos);
			}
		}
		else{
			$archivo = $request->file('file');
            	Excel::selectSheetsByIndex(0)->load($archivo, function($hoja){

                	Session::pull('cotizacion');
                	$hoja->each(function($fila){
                    	$codigo = $fila->codigosku;
                    	$precio = $fila->precio;
                    	$cantidad =  $fila->cantidad;

                    	$datos[0]=$codigo;
                    	$datos[1]=$precio;
                    	$datos[2]=$cantidad;
                    	Session::push('cotizacion', $datos);
                	});
            	});

            	foreach (Session::get('cotizacion') as $j){
                	$a[0]=$j[0];
                	$a[1]=$j[1];
                	$a[2]=$j[2];
                	array_push($cotizacion, $a);
            	}
		}
		$respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
                   ->setResponse($cotizacion);
        return $respuesta->returnToJson();
    }

    public function getListadoCotizaciones(Request $request){
        $vendedor = $request->get('ejecutivo', null);
        if (!isset($vendedor)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [ejecutivo]");
            return $respuesta->returnToJson();
        }

        $rutcli = $request->get('rutcli', null);
        if (!isset($rutcli)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [rutcli]");
            return $respuesta->returnToJson();
        }

        if(isset($vendedor) && isset($rutcli)){
            $listado = EnCotizacPDO::getListadoCotizaciones2($rutcli, $vendedor);
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(200)
                ->setResponse($listado);
            return $respuesta->returnToJson();
        }
    }

    public function sendMailCotizacion(Request $request){

        $numcot = $request->get('numcot', null);
        $nombrearchivo = $request->get('nombrearchivo', null);
        $rutaarchivo = $request->get('rutaarchivo', null) . $nombrearchivo;
        $ejecutivo = $request->get('ejecutivo', null);
        $comentarioejecutivo = $request->get('comentarioejecutivo', null);
		$correo_destinatario = $request->get('destinatario', null);
		
		$usuario = MaUsuarioPDO::getUserByUseridOnly(Session::get('usuario')->get('userid'));
		$mail_ejecutivo = $usuario->get('mail01', "");
		$password_mail = $usuario->get('psw_mail', "");
		
		if($mail_ejecutivo != ""){
			return $this->sendMailCotizacionNew($numcot, $nombrearchivo, $rutaarchivo, $ejecutivo, $comentarioejecutivo, $correo_destinatario, $mail_ejecutivo);
		}
		else{
			return $this->sendMailCotizacionClasico($numcot, $nombrearchivo, $rutaarchivo, $ejecutivo, $comentarioejecutivo, $correo_destinatario);
		}
    }
	
	public function sendMailCotizacionClasico($numcot, $nombrearchivo, $rutaarchivo, $ejecutivo, $comentarioejecutivo, $correo_destinatario){
		//Direcciones de correo
        $correo_ejecutivo = Session::get('usuario')->get('mail01');

        $data = array('ejecutivo'=>$ejecutivo, 'correoejecutivo'=>$correo_ejecutivo, 'numcot'=>$numcot, 'comentarioejecutivo' => $comentarioejecutivo);

        // Path or name to the blade template to be rendered
        $template_path = 'back.pages.cliente.fichacliente.template-mail-classic';

        //Contenido email
        $asunto = 'DIMERC | Cotización n° '. $numcot;

        $emailto = $correo_destinatario;
        $emailcc = $correo_ejecutivo;

        Mail::send(['text'=> $template_path ], $data, function($message) use($asunto, $rutaarchivo, $nombrearchivo, $emailto, $emailcc) {

            // Set the receiver and subject of the mail.
            $message->to($emailto)
                ->subject($asunto)
                ->attach($rutaarchivo, [
                    'as' => $nombrearchivo,
                    'mime' => 'application/pdf',
                ]);
            $message->cc($emailcc);
            // Set the sender
            $message->from('cotizaciones@dimerc.cl','Cotizaciones Dimerc');
        });

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse('Correo enviado exitosamente');
        return $respuesta->returnToJson();
	}
	
	public function sendMailCotizacionNew($numcot, $nombrearchivo, $rutaarchivo, $ejecutivo, $comentarioejecutivo, $correo_destinatario, $mail_ejecutivo){
		//insertamos coti a link y generamos hash
		$hashCoti = EnCotizacPDO::insertCotiLink($numcot, $correo_destinatario);

        $data = array('linkcoti'=>$hashCoti, 'numcot'=>$numcot, 'comentarioejecutivo' => $comentarioejecutivo);

        // Path or name to the blade template to be rendered
        $template_path = 'back.pages.cliente.fichacliente.template-mail';

        //Contenido email
        $asunto = 'DIMERC | Cotización n° '. $numcot;
		
		//generamos transporte con datos de conexion de office 365
		$transport = new Swift_SmtpTransport('smtp.office365.com', 587, 'tls');
		$transport->setUsername(ENV('MAIL_USERNAME'));
		$transport->setPassword(ENV('MAIL_PASSWORD'));		
		//generamos mailer con el transporte
		$swift_mailer = new Swift_Mailer($transport);
		
		//cargamos y rendereamos vista a HTML
		$view = View::make($template_path, $data);
        $html = $view->render();
		$respuesta = new ResponseFormatt();
		try{
			//instanciamos mensaje y seteamos valores necesarios
			$message = new Swift_Message();
        	$message->setFrom($mail_ejecutivo)
            	->setTo([$correo_destinatario => $correo_destinatario])
				->setBcc($mail_ejecutivo)
            	->setSubject($asunto)
            	->setBody($html, 'text/html')
				->attach(Swift_Attachment::fromPath($rutaarchivo, 'application/pdf'));
		
		
			//enviamos mail
			$cantMails = $swift_mailer->send($message);
			$respuesta->setCode(200)
            		  ->setResponse('Correo enviado exitosamente');
		}
		catch(\Exception $e){
			$respuesta->setCode(201)
            		  ->setResponse('Error en la autenticación, ¿es su correo '.$mail_ejecutivo.'? Puede actualizarlo en el cuadro inferior');
		}
		finally{
			return $respuesta->returnToJson();
		}
		
		/*var_dump($cantMails);die();
        if($swift_mailer->send($message)){
        	$respuesta->setCode(200)
            		  ->setResponse('Correo enviado exitosamente');
        }
		else{
			$respuesta->setCode(201)
            		  ->setResponse('Correo no enviado');
		}*/
	}

	public function updateMail(Request $request){
		$respuesta = new ResponseFormatt();
		try{
			$mailEjecutivo = $request->get('mail', false);
			$userid = $numcot = $request->get('userid', false);
		
			if($mailEjecutivo == false || $userid == false){
				$respuesta->setCode(200)->setResponse("Faltan datos, recuerde ingresar mail y usuario");
			}
			MaUsuarioPDO::updateMail($userid, $mailEjecutivo);
		
        	$respuesta->setCode(200)->setResponse("Mail actualizado");
		}
		catch(\Exception $e){
			$respuesta->setCode(201)->setResponse("Problema con BD: ".$e->getMessage());
		}
		finally{
			return $respuesta->returnToJson();
		}
		
        
	}

    public function convertCotiToNtaVta($numcot, $coddir){
        //validación vta anto, opl, pedido/stock mezclados o spot
        $validaProductos= EnCotizacPDO::getValidaProds($numcot);
        $validaCoti= EnCotizacPDO::getValidaCoti($numcot, $coddir);

        //valida vigencia y dirección de despacho sea solo santiago por ahora
        if ($validaCoti["VIGENTE"] == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("Cotización no vigente, actualizar");
            return $respuesta->returnToJson();
        }
        if ($validaCoti["SUCURSAL"] != 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("Cotizaciones Antofagasta por implementar");
            return $respuesta->returnToJson();
        }

        //valida que no haya mezclas de productos
        $cotiOK = true;
        $respuesta = "";
        $pedsto = "Y";
        $tipoprod = "N";
        foreach ($validaProductos as $prod) {
            //pedsto
            if ($pedsto == "Y" && $prod["CODPRO"] != "WW00008") {
                $pedsto = $prod["PEDSTO"];
            }
            if ($pedsto != $prod["PEDSTO"] && $prod["CODPRO"] != "WW00008") {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(203)->setResponse("Cotizaciones Stock/Pedido por implementar");
                return $respuesta->returnToJson();
            }
            if ($prod["CODPRO"] != "WW00008") {
                $pedsto = $prod["PEDSTO"];
            }
            //opl
            if ($tipoprod == "N" && $prod["CODPRO"] != "WW00008") {
                $tipoprod = $prod["TIPOPROD"];
            }
            if ($tipoprod != $prod["TIPOPROD"] && $prod["CODPRO"] != "WW00008") {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(204)->setResponse("Cotizaciones OPL/normal por implementar");
                return $respuesta->returnToJson();
            }
            if ($prod["CODPRO"] != "WW00008") {
                $tipoprod = $prod["TIPOPROD"];
            }
            //venta spot
            if ($prod["ESSPOT"] == 1 && $prod["QSPOT"] <= $prod["CANTID"]) {
                $solAprobada = EnCotizacPDO::solicitudAprobada($prod['CODPRO'], $validaCoti['RUTCLI'], $prod['CANTID']);

                EnCotizacPDO::deleteSolicitudVtaSpot($validaCoti["RUTCLI"], $prod["CODPRO"]);

                if (count($solAprobada) == 0) {
                    EnCotizacPDO::saveSolicitudVtaSpot($validaCoti["RUTCLI"], $validaCoti["CODVEN"], $prod["CODPRO"], $prod["CANTID"]);
                }

                $respuesta = new ResponseFormatt();
                $respuesta->setCode(205)->setResponse("Productos con venta Spot, solicitudes enviadas");
                return $respuesta->returnToJson();
            }
        }

        //saca número de nota
        $numnvt = EnCotizacPDO::getNextNtaVta();

        //saca promesa de venta
        $diasPromesa = EnCotizacPDO::getPromesaVenta($validaCoti["CODCOM"], $validaCoti["CODCIU"], $validaCoti["CODLOC"]);

        if (count($diasPromesa) > 0) {
            $diasPromesa = $diasPromesa[0]["PROMESA"];
        } else {
            $diasPromesa = 1;
        }

    }

    public function ConsultaApi(Request $request) {

        try {
            $nota = $request->get('numnvt');

            $user = 'api_dimerc';
            $pass = 'D1m3rcl4bs';
            $url = 'apis.dimerc.cl/external_dev/public/magento-integraciones/listado-pedidos/detallev2';
            $body = '{ "numdoc": ' . $nota . ' }';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $pass);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            //curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
            ));
            $response = curl_exec($ch);

            curl_close($ch);


            // $dom = new \DOMDocument;
            // $dom->loadXML($response);
            $array = json_decode($response);
            // var_dump($array);
            //die;

            //CUPO_CREDITO
            // $dom->getElementsByTagName('CUPO_CREDITO');
            // $credito = $dom->getElementsByTagName('CUPO_CREDITO')->item(0)->nodeValue;
            // $comprometido = $dom->getElementsByTagName('COMPROMETIDO')->item(0)->nodeValue;

            // //PortalEjUsuariosPDO::ActualizaCreditoMysql($rut, $credito, $disponible, 'mysql_portal');
            // $datos = array();
            // $datos[0]['id'] = $idsap;
            // $datos[0]['rut'] = $rut;
            // $datos[0]['cencos'] = $cencos;
            // $datos[0]['credito'] = intval($credito);
            // $datos[0]['comprometido'] = intval($comprometido);

            $respuesta = new ResponseFormatt();
            $respuesta->setCode(200)
                ->setResponse($array);
            return $respuesta->returnToJson();

        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

}