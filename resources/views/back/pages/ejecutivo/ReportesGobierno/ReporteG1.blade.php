@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-usd text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Reporte 1</h3>
                            <span class="f-16">Reporte 1 | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                        {{-- <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul> --}}
                    </div>
                </div>
                <a id="ruta"></a>
                <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-1 form-group">
                                    <label>Año</label>
                                    <div class='input-group'>
                                        <select name="year" id="year" class="form-control">
                                            <option value="2021">2021</option>
                                            <option value="2020">2020</option>
                                            <option value="2019">2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label>Mes</label>
                                    <div class='input-group'>
                                        <select name="month" id="month" class="form-control">
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label>Tipo</label>
                                    <div class='input-group'>
                                        <select name="tipo" id="tipo" class="form-control">
                                            <option value="1">Cliente</option>
                                            <option value="2">Holding</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 ">
                                    <label>Rut</label>
                                    <div class='input-group'>
                                        <input type="number" class="form-control pull-right col-md-12" placeholder="ingrese rut" id="rut">
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <div class='input-group' style="margin-top: 25px">
                                        <button id="Buscar" class="btn btn-danger" onclick="imprimir()" style="height: 40px; margin-left: 5px">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="card">
                
                <div class="card table-responsive col-xl-5">
                    <div>
                        <h4>Comisión por Venta ABC</h4>
                    </div>

                    {{-- <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                        <thead>
                            <tr class="table-inverse">
                                <th>Categoria</th>
                                <th>Venta</th>
                                <th>Comisión ABC</th>
                            </tr>
                        </thead>
                        <tbody id="cuadro2.1">
                        </tbody>
                    </table> --}}

                    <canvas id="barChart">

                    </canvas>
                </div>
            </div>
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-usd";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Mis Comisiones";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            Buscar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function exportar(){
            var fecha = document.getElementById('Periodo').value;
            fecha = fecha.substring(0,2) + fecha.substring(3,5) + fecha.substring(6,10);
            //console.log(fecha);
            window.open("{{url('descarga/comisiones')}}/" + fecha);
            //descargaComisiones();
        }

        function Buscar(){
            // var userid='{{ Session::get('usuario')->get('codusu') }}';
            // var moduleid=19;
            // var actionid=49;
            // var oldvalue='';
            // var newvalue='';
            // registro(userid, moduleid, actionid, oldvalue, newvalue);
            getDatos();
        }

        function imprimir(){
            var tipo = document.getElementById("tipo").value;
            var rut = document.getElementById("rut").value;
            var ano = document.getElementById("year").value;
            var mes = document.getElementById("month").value;
            document.getElementById("ruta").href = 'imprimirCartola/?datos='+tipo+'/'+rut+'/'+ano+'/'+mes;
            document.getElementById("ruta").click();
            //location.href = window.location.href.replace('Gobierno/' ,'imprimirCartola/?tipo='+tipo+'/?rut='+rut+'/?ano='+ano+'/?mes='+mes);
        }

        function getDatos() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                tipo: document.getElementById("tipo").value,
                rut: document.getElementById("rut").value,
                ano: document.getElementById("year").value,
                mes: document.getElementById("month").value
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/GobiernoDatos')}}',
                type: 'GET',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    if (json.response.length > 0) {
                        console.log(json);
                        
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection