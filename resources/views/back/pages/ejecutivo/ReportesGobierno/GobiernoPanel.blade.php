@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-table text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo" >Indicadores</h3>
                            <span class="f-16">Indicadores | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                             style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                    </div>
                </div>
                <div class="card-block" style="">
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
           
            <div class="card-block">
                <div class="row" id="draggablePanelList">
                    <div id="reporte1" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-panel text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('gobiernoReporte1') }}">1.- Consumo por linea mensual</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte2" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="fa fa-bar-chart-o text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('estadisticasMes') }}"> 2.- Consumo por linea ultimo trimestre</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte3" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-wallet text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('carteraMes') }}">3.- top 10 sku con mas cosumo mes en curso</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte4" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-notebook text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('SeguimientoNota') }}">4.- top 10 sku con mas consumo trimestral</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte5" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-notepad text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('anotacionesEjecutivo') }}">5.- top 10 cc con mas pedidos mes en curso</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte6" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-panel text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('estadisticasDia') }}">6.- top 10 cc con mas consumo mes en curso</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte7" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="fa fa-bar-chart-o text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('estadisticasMes') }}">7.- top 5 sku con mas consumo de los 10 cc con mas compra</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte8" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-wallet text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('carteraMes') }}">8.- consumo marca propia ultimo trimestre</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte9" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-notebook text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('SeguimientoNota') }}">9.- Venta Convenio</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte10" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-notepad text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('anotacionesEjecutivo') }}">10.- sku agregados a convenio</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte11" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-panel text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('estadisticasDia') }}">11.- sku de convenio sin consumo ultimo trimestre</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte12" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="fa fa-bar-chart-o text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('estadisticasMes') }}">12.- venta generada por la web</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte13" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-wallet text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('carteraMes') }}">13.- Cobranza deuda</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="draggablePanelList">
                    <div id="reporte14" class="col-lg-12 col-xl-8">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-notebook text-inverse f-40"></i>
                                <a class="f-22" target="_blank" href="{{ url('SeguimientoNota') }}">14.- Cobranza notas de credito</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-hand-rock-o";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Acciones Agendadas";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
            //perfil();
            loadData();
        });

        function perfil() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('getDerechos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json);
                    
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection