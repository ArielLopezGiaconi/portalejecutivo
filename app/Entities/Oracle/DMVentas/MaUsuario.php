<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 08-05-2019
 * Time: 18:13
 */

namespace App\Entities\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class MaUsuario implements \JsonSerializable
{
    private $rutusu;
    private $digusu;
    private $apepat;
    private $apemat;
    private $nombre;
    private $userid;
    private $fecing;
    private $fecexp;
    private $tipusr;
    private $tipven;
    private $codcnl;
    private $actfpr;
    private $cnlven;
    private $nomprt;
    private $cnlcom;
    private $gruvta;
    private $impres;
    private $conec;
    private $codest;
    private $margen;
    private $vermsg;
    private $modmov;
    private $accrec;
    private $accfac;
    private $nuldoc;
    private $usrnul;
    private $movent;
    private $movsal;
    private $asignado;
    private $notavta_internet_a_bodega;
    private $margvt;
    private $vercnl;
    private $accdoc;
    private $accsis;
    private $accexi;
    private $anexos;
    private $transp;
    private $accrev;
    private $porrev;
    private $numcli;
    private $acccta;
    private $problq;
    private $addcli;
    private $dpesos;
    private $vermar;
    private $verped;
    private $grucob;
    private $modmer;
    private $aprueb;
    private $depart;
    private $grupar;
    private $empres;
    private $acctls;
    private $tipfax;
    private $modfic;
    private $canal;
    private $subcnl;
    private $codusu;
    private $codval;
    private $mail01;
    private $codcom;
    private $numfax;
    private $prtele;
    private $segmento;
    private $region;
    private $clave;
    private $numver;
    private $clave2;
    private $codemp;
    private $es_horeca;
    private $tipcom;
    private $psw_mail;
    private $id_cliente_sap;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->rutusu = isset($registro->rutusu) ? $registro->rutusu : null;
            $this->digusu = isset($registro->digusu) ? $registro->digusu : null;
            $this->apepat = isset($registro->apepat) ? $registro->apepat : null;
            $this->apemat = isset($registro->apemat) ? $registro->apemat : null;
            $this->nombre = isset($registro->nombre) ? $registro->nombre : null;
            $this->userid = isset($registro->userid) ? $registro->userid : null;
            $this->fecing = isset($registro->fecing) ? $registro->fecing : null;
            $this->fecexp = isset($registro->fecexp) ? $registro->fecexp : null;
            $this->tipusr = isset($registro->tipusr) ? $registro->tipusr : null;
            $this->tipven = isset($registro->tipven) ? $registro->tipven : null;
            $this->codcnl = isset($registro->codcnl) ? $registro->codcnl : null;
            $this->actfpr = isset($registro->actfpr) ? $registro->actfpr : null;
            $this->cnlven = isset($registro->cnlven) ? $registro->cnlven : null;
            $this->nomprt = isset($registro->nomprt) ? $registro->nomprt : null;
            $this->cnlcom = isset($registro->cnlcom) ? $registro->cnlcom : null;
            $this->gruvta = isset($registro->gruvta) ? $registro->gruvta : null;
            $this->impres = isset($registro->impres) ? $registro->impres : null;
            $this->conec = isset($registro->conec) ? $registro->conec : null;
            $this->codest = isset($registro->codest) ? $registro->codest : null;
            $this->margen = isset($registro->margen) ? $registro->margen : null;
            $this->vermsg = isset($registro->vermsg) ? $registro->vermsg : null;
            $this->modmov = isset($registro->modmov) ? $registro->modmov : null;
            $this->accrec = isset($registro->accrec) ? $registro->accrec : null;
            $this->accfac = isset($registro->accfac) ? $registro->accfac : null;
            $this->nuldoc = isset($registro->nuldoc) ? $registro->nuldoc : null;
            $this->usrnul = isset($registro->usrnul) ? $registro->usrnul : null;
            $this->movent = isset($registro->movent) ? $registro->movent : null;
            $this->movsal = isset($registro->movsal) ? $registro->movsal : null;
            $this->asignado = isset($registro->asignado) ? $registro->asignado : null;
            $this->notavta_internet_a_bodega = isset($registro->notavta_internet_a_bodega) ? $registro->notavta_internet_a_bodega : null;
            $this->margvt = isset($registro->margvt) ? $registro->margvt : null;
            $this->vercnl = isset($registro->vercnl) ? $registro->vercnl : null;
            $this->accdoc = isset($registro->accdoc) ? $registro->accdoc : null;
            $this->accsis = isset($registro->accsis) ? $registro->accsis : null;
            $this->accexi = isset($registro->accexi) ? $registro->accexi : null;
            $this->anexos = isset($registro->anexos) ? $registro->anexos : null;
            $this->transp = isset($registro->transp) ? $registro->transp : null;
            $this->accrev = isset($registro->accrev) ? $registro->accrev : null;
            $this->porrev = isset($registro->porrev) ? $registro->porrev : null;
            $this->numcli = isset($registro->numcli) ? $registro->numcli : null;
            $this->acccta = isset($registro->acccta) ? $registro->acccta : null;
            $this->problq = isset($registro->problq) ? $registro->problq : null;
            $this->addcli = isset($registro->addcli) ? $registro->addcli : null;
            $this->dpesos = isset($registro->dpesos) ? $registro->dpesos : null;
            $this->vermar = isset($registro->vermar) ? $registro->vermar : null;
            $this->verped = isset($registro->verped) ? $registro->verped : null;
            $this->grucob = isset($registro->grucob) ? $registro->grucob : null;
            $this->modmer = isset($registro->modmer) ? $registro->modmer : null;
            $this->aprueb = isset($registro->aprueb) ? $registro->aprueb : null;
            $this->depart = isset($registro->depart) ? $registro->depart : null;
            $this->grupar = isset($registro->grupar) ? $registro->grupar : null;
            $this->empres = isset($registro->empres) ? $registro->empres : null;
            $this->acctls = isset($registro->acctls) ? $registro->acctls : null;
            $this->tipfax = isset($registro->tipfax) ? $registro->tipfax : null;
            $this->modfic = isset($registro->modfic) ? $registro->modfic : null;
            $this->canal = isset($registro->canal) ? $registro->canal : null;
            $this->subcnl = isset($registro->subcnl) ? $registro->subcnl : null;
            $this->codusu = isset($registro->codusu) ? $registro->codusu : null;
            $this->codval = isset($registro->codval) ? $registro->codval : null;
            $this->mail01 = isset($registro->mail01) ? $registro->mail01 : null;
            $this->codcom = isset($registro->codcom) ? $registro->codcom : null;
            $this->numfax = isset($registro->numfax) ? $registro->numfax : null;
            $this->prtele = isset($registro->prtele) ? $registro->prtele : null;
            $this->segmento = isset($registro->segmento) ? $registro->segmento : null;
            $this->region = isset($registro->region) ? $registro->region : null;
            $this->clave = isset($registro->clave) ? $registro->clave : null;
            $this->numver = isset($registro->numver) ? $registro->numver : null;
            $this->clave2 = isset($registro->clave2) ? $registro->clave2 : null;
            $this->codemp = isset($registro->codemp) ? $registro->codemp : null;
            $this->es_horeca = isset($registro->es_horeca) ? $registro->es_horeca : null;
            $this->tipcom = isset($registro->tipcom) ? $registro->tipcom : null;
            $this->psw_mail = isset($registro->psw_mail) ? $registro->psw_mail : null;
            $this->id_cliente_sap = isset($registro->id_cliente_sap) ? $registro->id_cliente_sap : null;
        }
    }

    public function getRutusu()
    {
        return $this->rutusu;
    }

    public function setRutusu($rutusu)
    {
        $this->rutusu = $rutusu;
    }

    public function getDigusu()
    {
        return $this->digusu;
    }

    public function setDigusu($digusu)
    {
        $this->digusu = $digusu;
    }

    public function getApepat()
    {
        return $this->apepat;
    }

    public function setApepat($apepat)
    {
        $this->apepat = $apepat;
    }

    public function getApemat()
    {
        return $this->apemat;
    }

    public function setApemat($apemat)
    {
        $this->apemat = $apemat;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function getFecing()
    {
        return $this->fecing;
    }

    public function setFecing($fecing)
    {
        $this->fecing = $fecing;
    }

    public function getFecexp()
    {
        return $this->fecexp;
    }

    public function setFecexp($fecexp)
    {
        $this->fecexp = $fecexp;
    }

    public function getTipusr()
    {
        return $this->tipusr;
    }

    public function setTipusr($tipusr)
    {
        $this->tipusr = $tipusr;
    }

    public function getTipven()
    {
        return $this->tipven;
    }

    public function setTipven($tipven)
    {
        $this->tipven = $tipven;
    }

    public function getCodcnl()
    {
        return $this->codcnl;
    }

    public function setCodcnl($codcnl)
    {
        $this->codcnl = $codcnl;
    }

    public function getActfpr()
    {
        return $this->actfpr;
    }

    public function setActfpr($actfpr)
    {
        $this->actfpr = $actfpr;
    }

    public function getCnlven()
    {
        return $this->cnlven;
    }

    public function setCnlven($cnlven)
    {
        $this->cnlven = $cnlven;
    }

    public function getNomprt()
    {
        return $this->nomprt;
    }

    public function setNomprt($nomprt)
    {
        $this->nomprt = $nomprt;
    }

    public function getCnlcom()
    {
        return $this->cnlcom;
    }

    public function setCnlcom($cnlcom)
    {
        $this->cnlcom = $cnlcom;
    }

    public function getGruvta()
    {
        return $this->gruvta;
    }

    public function setGruvta($gruvta)
    {
        $this->gruvta = $gruvta;
    }

    public function getImpres()
    {
        return $this->impres;
    }

    public function setImpres($impres)
    {
        $this->impres = $impres;
    }

    public function getConec()
    {
        return $this->conec;
    }

    public function setConec($conec)
    {
        $this->conec = $conec;
    }

    public function getCodest()
    {
        return $this->codest;
    }

    public function setCodest($codest)
    {
        $this->codest = $codest;
    }

    public function getMargen()
    {
        return $this->margen;
    }

    public function setMargen($margen)
    {
        $this->margen = $margen;
    }

    public function getVermsg()
    {
        return $this->vermsg;
    }

    public function setVermsg($vermsg)
    {
        $this->vermsg = $vermsg;
    }

    public function getModmov()
    {
        return $this->modmov;
    }

    public function setModmov($modmov)
    {
        $this->modmov = $modmov;
    }

    public function getAccrec()
    {
        return $this->accrec;
    }

    public function setAccrec($accrec)
    {
        $this->accrec = $accrec;
    }

    public function getAccfac()
    {
        return $this->accfac;
    }

    public function setAccfac($accfac)
    {
        $this->accfac = $accfac;
    }

    public function getNuldoc()
    {
        return $this->nuldoc;
    }

    public function setNuldoc($nuldoc)
    {
        $this->nuldoc = $nuldoc;
    }

    public function getUsrnul()
    {
        return $this->usrnul;
    }

    public function setUsrnul($usrnul)
    {
        $this->usrnul = $usrnul;
    }

    public function getMovent()
    {
        return $this->movent;
    }

    public function setMovent($movent)
    {
        $this->movent = $movent;
    }

    public function getMovsal()
    {
        return $this->movsal;
    }

    public function setMovsal($movsal)
    {
        $this->movsal = $movsal;
    }

    public function getAsignado()
    {
        return $this->asignado;
    }

    public function setAsignado($asignado)
    {
        $this->asignado = $asignado;
    }

    public function getNotavtaInternetABodega()
    {
        return $this->notavta_internet_a_bodega;
    }

    public function setNotavtaInternetABodega($notavta_internet_a_bodega)
    {
        $this->notavta_internet_a_bodega = $notavta_internet_a_bodega;
    }

    public function getMargvt()
    {
        return $this->margvt;
    }

    public function setMargvt($margvt)
    {
        $this->margvt = $margvt;
    }

    public function getVercnl()
    {
        return $this->vercnl;
    }

    public function setVercnl($vercnl)
    {
        $this->vercnl = $vercnl;
    }

    public function getAccdoc()
    {
        return $this->accdoc;
    }

    public function setAccdoc($accdoc)
    {
        $this->accdoc = $accdoc;
    }

    public function getAccsis()
    {
        return $this->accsis;
    }

    public function setAccsis($accsis)
    {
        $this->accsis = $accsis;
    }

    public function getAccexi()
    {
        return $this->accexi;
    }

    public function setAccexi($accexi)
    {
        $this->accexi = $accexi;
    }

    public function getAnexos()
    {
        return $this->anexos;
    }

    public function setAnexos($anexos)
    {
        $this->anexos = $anexos;
    }

    public function getTransp()
    {
        return $this->transp;
    }

    public function setTransp($transp)
    {
        $this->transp = $transp;
    }

    public function getAccrev()
    {
        return $this->accrev;
    }

    public function setAccrev($accrev)
    {
        $this->accrev = $accrev;
    }

    public function getPorrev()
    {
        return $this->porrev;
    }

    public function setPorrev($porrev)
    {
        $this->porrev = $porrev;
    }

    public function getNumcli()
    {
        return $this->numcli;
    }

    public function setNumcli($numcli)
    {
        $this->numcli = $numcli;
    }

    public function getAcccta()
    {
        return $this->acccta;
    }

    public function setAcccta($acccta)
    {
        $this->acccta = $acccta;
    }

    public function getProblq()
    {
        return $this->problq;
    }

    public function setProblq($problq)
    {
        $this->problq = $problq;
    }

    public function getAddcli()
    {
        return $this->addcli;
    }

    public function setAddcli($addcli)
    {
        $this->addcli = $addcli;
    }

    public function getDpesos()
    {
        return $this->dpesos;
    }

    public function setDpesos($dpesos)
    {
        $this->dpesos = $dpesos;
    }

    public function getVermar()
    {
        return $this->vermar;
    }

    public function setVermar($vermar)
    {
        $this->vermar = $vermar;
    }

    public function getVerped()
    {
        return $this->verped;
    }

    public function setVerped($verped)
    {
        $this->verped = $verped;
    }

    public function getGrucob()
    {
        return $this->grucob;
    }

    public function setGrucob($grucob)
    {
        $this->grucob = $grucob;
    }

    public function getModmer()
    {
        return $this->modmer;
    }

    public function setModmer($modmer)
    {
        $this->modmer = $modmer;
    }

    public function getAprueb()
    {
        return $this->aprueb;
    }

    public function setAprueb($aprueb)
    {
        $this->aprueb = $aprueb;
    }

    public function getDepart()
    {
        return $this->depart;
    }

    public function setDepart($depart)
    {
        $this->depart = $depart;
    }

    public function getGrupar()
    {
        return $this->grupar;
    }

    public function setGrupar($grupar)
    {
        $this->grupar = $grupar;
    }

    public function getEmpres()
    {
        return $this->empres;
    }

    public function setEmpres($empres)
    {
        $this->empres = $empres;
    }

    public function getAcctls()
    {
        return $this->acctls;
    }

    public function setAcctls($acctls)
    {
        $this->acctls = $acctls;
    }

    public function getTipfax()
    {
        return $this->tipfax;
    }

    public function setTipfax($tipfax)
    {
        $this->tipfax = $tipfax;
    }

    public function getModfic()
    {
        return $this->modfic;
    }

    public function setModfic($modfic)
    {
        $this->modfic = $modfic;
    }

    public function getCanal()
    {
        return $this->canal;
    }

    public function setCanal($canal)
    {
        $this->canal = $canal;
    }

    public function getSubcnl()
    {
        return $this->subcnl;
    }

    public function setSubcnl($subcnl)
    {
        $this->subcnl = $subcnl;
    }

    public function getCodusu()
    {
        return $this->codusu;
    }

    public function setCodusu($codusu)
    {
        $this->codusu = $codusu;
    }

    public function getCodval()
    {
        return $this->codval;
    }

    public function setCodval($codval)
    {
        $this->codval = $codval;
    }

    public function getMail01()
    {
        return $this->mail01;
    }

    public function setMail01($mail01)
    {
        $this->mail01 = $mail01;
    }

    public function getCodcom()
    {
        return $this->codcom;
    }

    public function setCodcom($codcom)
    {
        $this->codcom = $codcom;
    }

    public function getNumfax()
    {
        return $this->numfax;
    }

    public function setNumfax($numfax)
    {
        $this->numfax = $numfax;
    }

    public function getPrtele()
    {
        return $this->prtele;
    }

    public function setPrtele($prtele)
    {
        $this->prtele = $prtele;
    }

    public function getSegmento()
    {
        return $this->segmento;
    }

    public function setSegmento($segmento)
    {
        $this->segmento = $segmento;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function setClave($clave)
    {
        $this->clave = $clave;
    }

    public function getNumver()
    {
        return $this->numver;
    }

    public function setNumver($numver)
    {
        $this->numver = $numver;
    }

    public function getClave2()
    {
        return $this->clave2;
    }

    public function setClave2($clave2)
    {
        $this->clave2 = $clave2;
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getEsHoreca()
    {
        return $this->es_horeca;
    }

    public function getTipcom()
    {
        return $this->tipcom;
    }

    public function setTipcom($tipcom)
    {
        $this->tipcom = $tipcom;
    }

    public function getPswMail()
    {
        return $this->psw_mail;
    }

    public function setPswMail($psw_mail)
    {
        $this->psw_mail = $psw_mail;
    }

    public function getIdClienteSap()
    {
        return $this->id_cliente_sap;
    }

    public function setIdClienteSap($id_cliente_sap)
    {
        $this->id_cliente_sap = $id_cliente_sap;
    }

    public function jsonSerialize()
    {
        return [
            'rutusu' => $this->rutusu,
            'digusu' => $this->digusu,
            'apepat' => $this->apepat,
            'apemat' => $this->apemat,
            'nombre' => $this->nombre,
            'userid' => $this->userid,
            'fecing' => $this->fecing,
            'fecexp' => $this->fecexp,
            'tipusr' => $this->tipusr,
            'tipven' => $this->tipven,
            'codcnl' => $this->codcnl,
            'actfpr' => $this->actfpr,
            'cnlven' => $this->cnlven,
            'nomprt' => $this->nomprt,
            'cnlcom' => $this->cnlcom,
            'gruvta' => $this->gruvta,
            'impres' => $this->impres,
            'conec' => $this->conec,
            'codest' => $this->codest,
            'margen' => $this->margen,
            'vermsg' => $this->vermsg,
            'modmov' => $this->modmov,
            'accrec' => $this->accrec,
            'accfac' => $this->accfac,
            'nuldoc' => $this->nuldoc,
            'usrnul' => $this->usrnul,
            'movent' => $this->movent,
            'movsal' => $this->movsal,
            'asignado' => $this->asignado,
            'notavta_internet_a_bodega' => $this->notavta_internet_a_bodega,
            'margvt' => $this->margvt,
            'vercnl' => $this->vercnl,
            'accdoc' => $this->accdoc,
            'accsis' => $this->accsis,
            'accexi' => $this->accexi,
            'anexos' => $this->anexos,
            'transp' => $this->transp,
            'accrev' => $this->accrev,
            'porrev' => $this->porrev,
            'numcli' => $this->numcli,
            'acccta' => $this->acccta,
            'problq' => $this->problq,
            'addcli' => $this->addcli,
            'dpesos' => $this->dpesos,
            'vermar' => $this->vermar,
            'verped' => $this->verped,
            'grucob' => $this->grucob,
            'modmer' => $this->modmer,
            'aprueb' => $this->aprueb,
            'depart' => $this->depart,
            'grupar' => $this->grupar,
            'empres' => $this->empres,
            'acctls' => $this->acctls,
            'tipfax' => $this->tipfax,
            'modfic' => $this->modfic,
            'canal' => $this->canal,
            'subcnl' => $this->subcnl,
            'codusu' => $this->codusu,
            'codval' => $this->codval,
            'mail01' => $this->mail01,
            'codcom' => $this->codcom,
            'numfax' => $this->numfax,
            'prtele' => $this->prtele,
            'segmento' => $this->segmento,
            'region' => $this->region,
            'clave' => $this->clave,
            'numver' => $this->numver,
            'clave2' => $this->clave2,
            'codemp' => $this->codemp,
            'es_horeca' => $this->es_horeca,
            'tipcom' => $this->tipcom,
            'psw_mail' => $this->psw_mail,
            'id_cliente_sap' => $this->id_cliente_sap,
        ];
    }
}