<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use \pdo_DB;

class ReClienteListaLineaPDO extends Model
{
    public static function getCodCnl($rutcli, $codpro, $codemp = 3)
    {
        $sql = "select getlinealista(:codemp,:rutcli,:codpro) as codcnl from dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codemp' => $codemp,
            'rutcli' => $rutcli,
            'codpro' => $codpro
        ]);
        return $resultado[0]->codcnl;
    }
}
