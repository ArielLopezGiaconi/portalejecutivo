<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 13:31
 */

namespace App\Http\Helpers;

use App\Helpers\GeneralHelper;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\Reportes\ConsumosController;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\MaEmpresaPDO;
use Illuminate\Http\Request;
use App\EPDO\MySQL\PricingServer\PortalEjecutivo\PortalEjUsuariosEPDO;

class ClientesHelper
{
    public function buscarPorRazonSocial(Request $formulario)
    {
        // 0.- Validar que venga campo vendedor
        $vendedor = $formulario->get('vendedor', null);
        if (!isset($vendedor)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [vendedor]");
            return $respuesta->returnToJson();
        }

        $usuario = PortalEjUsuariosEPDO::where('codusu', '=', $vendedor)
            ->where('estado', '=', 1)
            ->first();

        if (count($usuario) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró vendedor en BBDD");
            return $respuesta->returnToJson();
        }

        // 1.- Validar que venga campo
        $razon_social = $formulario->get('razon_social', null);
        if (!isset($razon_social)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [razon_social]");
            return $respuesta->returnToJson();
        }

        // 2.- Verificación existencia codemp
        $codemp = $formulario->get('cod_emp', false) ? $formulario->get('cod_emp') : 3;
        if (!MaEmpresaPDO::existsCompanyByCod($codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con código [" . $codemp . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        // IR AL CONTROLADOR
        $controlador = new ClientesController();
        return $controlador->buscarPorRazonSocial($razon_social, $codemp, $usuario);
    }

    public function buscarEspecificoPorRut(Request $formulario)
    {
        // 0.- Validar que venga campo vendedor
        $vendedor = $formulario->get('vendedor', null);
        if (!isset($vendedor)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [vendedor]");
            return $respuesta->returnToJson();
        }

        $usuario = PortalEjUsuariosEPDO::where('codusu', '=', $vendedor)
            ->where('estado', '=', 1)
            ->first();

        if (count($usuario) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró vendedor en BBDD");
            return $respuesta->returnToJson();
        }

        // 1.- Validar que venga campo
        $rut = $formulario->get('rut', null);
        if (!isset($rut)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [rut]");
            return $respuesta->returnToJson();
        }

        // 2.- Verificación existencia codemp
        $codemp = $formulario->get('cod_emp', false) ? $formulario->get('cod_emp') : 3;
        if (!MaEmpresaPDO::existsCompanyByCod($codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con código [" . $codemp . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        // IR AL CONTROLADOR
        $controlador = new ClientesController();
        return $controlador->buscarEspecificoPorRut($rut, $codemp, $usuario);
    }
}