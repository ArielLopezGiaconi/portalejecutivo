<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 10/08/2018
 * Time: 19:41
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\EnCliente;
use App\Entities\Oracle\DMVentas\QvControlMargen;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class QvControlMargenPDO extends Model
{
    /* ********* 1 ******** */
    public static function getReportByClientDates(Collection $in_rutsUltramar, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " (venta_tlmk + venta_web) as ventas, "
            . " (venta_nc) as notas_credito, "
            . " (venta_tlmk + venta_web - venta_nc) as total, "
            . " cantidad_ventas, cantidad_lineas, cantidad_productos "
            . " FROM "
            . " ( "
            . "     SELECT "
            . "     a.rutcli, a.codemp, "
            . "     SUM(CASE WHEN a.tipo IN ('F', 'G') AND a.internet = 0 THEN a.neto ELSE 0 END) AS VENTA_TLMK, "
            . "     SUM(CASE WHEN (a.tipo = 'F' AND a.internet = 1) OR (a.tipo = 'G' AND a.internet = 2) THEN a.neto ELSE 0 END) VENTA_WEB, "
            . "     SUM(CASE WHEN a.tipo IN ('N') THEN a.neto ELSE 0 END) VENTA_NC, "
            . "     COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "     COUNT(a.codpro) AS cantidad_lineas, "
            . "     SUM(a.cantid) AS cantidad_productos"
            . "     FROM "
            . "     qv_control_margen a "
            . "     WHERE "
            . "     a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY')"
            . "     AND a.internet in ('0', '1', '2') "
            . "     AND a.rutcli IN ('" . $in_rutsUltramar->implode("', '") . "')"
            . "     GROUP BY "
            . "     a.rutcli, a.codemp "
            . " ) z";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y')
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportClientFormat($resultado);
        return $arrayReturn;
    }

    public static function getReportBySpecificClientDates(EnCliente $in_client, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " (venta_tlmk + venta_web) as ventas, "
            . " (venta_nc) as notas_credito, "
            . " (venta_tlmk + venta_web - venta_nc) as total, "
            . " cantidad_ventas, cantidad_lineas, cantidad_productos "
            . " FROM "
            . " ( "
            . "     SELECT "
            . "     a.rutcli, a.codemp, "
            . "     SUM(CASE WHEN a.tipo IN ('F', 'G') AND a.internet = 0 THEN a.neto ELSE 0 END) AS VENTA_TLMK, "
            . "     SUM(CASE WHEN (a.tipo = 'F' AND a.internet = 1) OR (a.tipo = 'G' AND a.internet = 2) THEN a.neto ELSE 0 END) VENTA_WEB, "
            . "     SUM(CASE WHEN a.tipo IN ('N') THEN a.neto ELSE 0 END) VENTA_NC, "
            . "     COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "     COUNT(a.codpro) AS cantidad_lineas, "
            . "     SUM(a.cantid) AS cantidad_productos"
            . "     FROM "
            . "     qv_control_margen a "
            . "     WHERE "
            . "     a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY')"
            . "     AND a.internet in ('0', '1', '2') "
            . "     AND a.rutcli = :client_rut"
            . "     GROUP BY "
            . "     a.rutcli, a.codemp "
            . " ) z";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y'),
            'client_rut' => $in_client->getRutcli()
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportClientFormat($resultado);
        return $arrayReturn;
    }


    /* ********* 2 ******** */
    public static function getReporByLineClientDates(Collection $in_rutsUltramar, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " z.linea, "
            . " (venta_tlmk + venta_web) as ventas, "
            . " (venta_nc) as notas_credito, "
            . " (venta_tlmk + venta_web - venta_nc) as total, "
            . " cantidad_ventas, cantidad_lineas, cantidad_productos "
            . " FROM "
            . " ( "
            . "     SELECT "
            . "     a.rutcli, a.codemp, a.linea, "
            . "     SUM(CASE WHEN a.tipo IN ('F', 'G') AND a.internet = 0 THEN a.neto ELSE 0 END) AS VENTA_TLMK, "
            . "     SUM(CASE WHEN (a.tipo = 'F' AND a.internet = 1) OR (a.tipo = 'G' AND a.internet = 2) THEN a.neto ELSE 0 END) VENTA_WEB, "
            . "     SUM(CASE WHEN a.tipo IN ('N') THEN a.neto ELSE 0 END) VENTA_NC, "
            . "     COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "     COUNT(a.codpro) AS cantidad_lineas, "
            . "     SUM(a.cantid) AS cantidad_productos"
            . "     FROM "
            . "     qv_control_margen a "
            . "     WHERE "
            . "     a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY')"
            . "     AND a.internet in ('0', '1', '2') "
            . "     AND a.rutcli IN ('" . $in_rutsUltramar->implode("', '") . "')"
            . "     GROUP BY a.rutcli, a.codemp, a.linea "
            . " ) z "
            . " ORDER BY rutcli, linea";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y')
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportLineaFormat($resultado);
        return $arrayReturn;
    }

    public static function getReporByLineClientSpecificDates(EnCliente $in_client, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " z.linea, "
            . " (venta_tlmk + venta_web) as ventas, "
            . " (venta_nc) as notas_credito, "
            . " (venta_tlmk + venta_web - venta_nc) as total, "
            . " cantidad_ventas, cantidad_lineas, cantidad_productos "
            . " FROM "
            . " ( "
            . "     SELECT "
            . "     a.rutcli, a.codemp, a.linea, "
            . "     SUM(CASE WHEN a.tipo IN ('F', 'G') AND a.internet = 0 THEN a.neto ELSE 0 END) AS VENTA_TLMK, "
            . "     SUM(CASE WHEN (a.tipo = 'F' AND a.internet = 1) OR (a.tipo = 'G' AND a.internet = 2) THEN a.neto ELSE 0 END) VENTA_WEB, "
            . "     SUM(CASE WHEN a.tipo IN ('N') THEN a.neto ELSE 0 END) VENTA_NC, "
            . "     COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "     COUNT(a.codpro) AS cantidad_lineas, "
            . "     SUM(a.cantid) AS cantidad_productos"
            . "     FROM "
            . "     qv_control_margen a "
            . "     WHERE "
            . "     a.rutcli = :client_rut"
            . "     AND a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY')"
            . "     AND a.internet in ('0', '1', '2') "
            . "     GROUP BY a.rutcli, a.codemp, a.linea "
            . " ) z "
            . " ORDER BY rutcli, linea";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y'),
            'client_rut' => $in_client->getRutcli()
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportLineaFormat($resultado);
        return $arrayReturn;
    }

    /* ********************* 3 ********************* */
    public static function getReporByProdClientDates(Collection $in_rutsUltramar, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " z.codpro, "
            . " GETMARCA(z.codpro) AS marca, "
            . " z.linea, "
            . " (ventas) as total, "
            . " cantidad_ventas, cantidad_productos, "
            . " promedio_precio, promedio_costo "
            . " FROM "
            . " ( "
            . "    SELECT "
            . "    a.rutcli, a.codemp, a.codpro, a.linea, "
            . "    SUM(a.neto) AS ventas, "
            . "    COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "    SUM(a.cantid) AS cantidad_productos, "
            . "    ROUND(AVG(a.neto / a.cantid)) AS promedio_precio, "
            . "    ROUND(AVG(a.costo / a.cantid)) AS promedio_costo "
            . "    FROM "
            . "    qv_control_margen a "
            . "    WHERE a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY') "
            . "    AND a.tipo IN ('F', 'G') "
            . "     AND a.rutcli IN ('" . $in_rutsUltramar->implode("', '") . "')"
            . "    GROUP BY a.rutcli, a.codemp, a.codpro, a.linea "
            . ") z "
            . " ORDER BY rutcli, codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y')
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportProdFormat($resultado);
        return $arrayReturn;
    }

    public static function getReporByProdSpecificClientDates(EnCliente $in_client, Carbon $in_datestart, Carbon $in_dateend)
    {
        $sql = "SELECT z.rutcli, "
            . " GETRAZONSOCIAL(z.codemp, z.rutcli) as razon_social, "
            . " z.codpro, "
            . " GETMARCA(z.codpro) AS marca, "
            . " z.linea, "
            . " (ventas) as total, "
            . " cantidad_ventas, cantidad_productos, "
            . " promedio_precio, promedio_costo "
            . " FROM "
            . " ( "
            . "    SELECT "
            . "    a.rutcli, a.codemp, a.codpro, a.linea, "
            . "    SUM(a.neto) AS ventas, "
            . "    COUNT(DISTINCT a.numnvt) as cantidad_ventas, "
            . "    SUM(a.cantid) AS cantidad_productos, "
            . "    ROUND(AVG(a.neto / a.cantid)) AS promedio_precio, "
            . "    ROUND(AVG(a.costo / a.cantid)) AS promedio_costo "
            . "    FROM "
            . "    qv_control_margen a "
            . "    WHERE a.rutcli = :client_rut"
            . "    AND a.fecha BETWEEN TO_DATE(:start_date, 'DD/MM/YYYY') AND TO_DATE(:end_date, 'DD/MM/YYYY') "
            . "    AND a.tipo IN ('F', 'G') "
            . "    GROUP BY a.rutcli, a.codemp, a.codpro, a.linea "
            . ") z "
            . " ORDER BY rutcli, codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'start_date' => $in_datestart->format('d/m/Y'),
            'end_date' => $in_dateend->format('d/m/Y'),
            'client_rut' => $in_client->getRutcli()
        ]);

        $arrayReturn = QvControlMargenPDO::returnReportProdFormat($resultado);
        return $arrayReturn;
    }


    /* FORMATOS DE RESPUESTA */
    private static function returnReportClientFormat($registros)
    {
        $arrayReturn = null;
        $suma = 0;
        foreach ($registros as $registro) {
            // Sumatoria de totales
            $suma += (int)$registro->total;

            // Guardar registros
            $arrayReturn['detalle'][] = array(
                'rutcli' => (int)$registro->rutcli,
                'razon_social' => $registro->razon_social,

                'notas_credito' => (int)$registro->notas_credito,
                'ventas' => (int)$registro->ventas,
                'total' => (int)$registro->total,
                'total_format' => '$' . number_format((int)$registro->total, 0, ',', '.'),
                // 'total_format' => ((int)$registro->total >= 0 ? '$': '-$').number_format((int)$registro->total, 0, ',', '.'),

                'cantidad_lineas' => (int)$registro->cantidad_lineas,
                'cantidad_ventas' => (int)$registro->cantidad_ventas,
                'cantidad_productos' => (int)$registro->cantidad_productos
            );
        }

        // Guardar sumatoria totales
        $arrayReturn['suma_total'] = $suma;
        $arrayReturn['suma_total_format'] = '$' . number_format((int)$suma, 0, ',', '.');

        // Retornar respuesta final
        return $arrayReturn;
    }

    private static function returnReportLineaFormat($registros)
    {
        $arrayReturn = null;
        $collectionReturn = null;
        $suma = 0;

        // Generar detalle específico
        foreach ($registros as $registro) {
            // Sumatoria de totales
            $suma += (int)$registro->total;

            // Guardar registros
            $arrayReturn['detalle_especifico'][] = array(
                'rutcli' => (int)$registro->rutcli,
                'razon_social' => $registro->razon_social,

                'linea' => $registro->linea,
                'notas_credito' => (int)$registro->notas_credito,
                'ventas' => (int)$registro->ventas,
                'total' => (int)$registro->total,
                'total_format' => '$' . number_format((int)$registro->total, 0, ',', '.'),
                // 'total_format' => ((int)$registro->total >= 0 ? '$': '-$').number_format((int)$registro->total, 0, ',', '.'),

                'cantidad_lineas' => (int)$registro->cantidad_lineas,
                'cantidad_ventas' => (int)$registro->cantidad_ventas,
                'cantidad_productos' => (int)$registro->cantidad_productos
            );
        }

        // Generar colección
        $coleccion = collect($arrayReturn['detalle_especifico']);
        $coleccionPorRut = $coleccion->groupBy('rutcli');
        foreach ($coleccionPorRut as $registroRut => $registroRutDetalle) {
            $sumaRut = 0;
            foreach ($registroRutDetalle->pluck('total', 'linea') as $registroRutLinea => $registroRutLineaMonto) {
                $arrayReturn['detalle_personalizado'][$registroRut][$registroRutLinea] = $registroRutLineaMonto;
                $sumaRut += $registroRutLineaMonto;
            }

            $primero = $coleccion->where('rutcli', '=',''.$registroRut)->first();
            $arrayReturn['detalle_personalizado'][$registroRut]['razon_social'] = ($primero) ? $primero['razon_social'] : 'S/N' ;
            $arrayReturn['detalle_personalizado'][$registroRut]['suma_total'] = $sumaRut;
            $arrayReturn['detalle_personalizado'][$registroRut]['suma_total_format'] = '$' . number_format((int)$sumaRut, 0, ',', '.');
        }

        // Guardar sumatoria totales
        $arrayReturn['suma_total'] = $suma;
        $arrayReturn['suma_total_format'] = '$' . number_format((int)$suma, 0, ',', '.');

        // Retornar respuesta final
        return $arrayReturn;
    }

    private static function returnReportProdFormat($registros)
    {
        $arrayReturn = null;
        $suma = 0;
        foreach ($registros as $registro) {
            // Sumatoria de totales
            $suma += (int)$registro->total;

            // Guardar registros
            $arrayReturn['detalle'][] = array(
                'rutcli' => (int)$registro->rutcli,
                'razon_social' => $registro->razon_social,

                'codpro' => $registro->codpro,
                'marca' => $registro->marca,
                'linea' => $registro->linea,

                'total' => (int)$registro->total,

                'cantidad_ventas' => (int)$registro->cantidad_ventas,
                'cantidad_productos' => (int)$registro->cantidad_productos,
                'promedio_precio' => (int)$registro->promedio_precio,
                'promedio_costo' => (int)$registro->promedio_costo,
            );
        }

        // Guardar sumatoria totales
        $arrayReturn['suma_total'] = $suma;
        $arrayReturn['suma_total_format'] = '$' . number_format($suma, 0, ',', '.');

        // Retornar respuesta final
        return $arrayReturn;
    }

    // note: if a product exists in ma_product but there is no records of it for a certain customer cost center,
    // the array will be null. The same happens when the date1 its greater than f2
    public static function consultaMediaProducto($rut_cli, $centro_costo, $str_prods, $fecha_inic, $fecha_term)
    {
        $sql = "SELECT codpro, ROUND(AVG(cantid)) AS media"
            . " FROM qv_control_margen "
            . " WHERE (fecha BETWEEN TO_DATE(:fecha_in, 'DD-MM-YYYY') AND TO_DATE(:fecha_out, 'DD-MM-YYYY')) "
            . " AND rutcli = :rut_cli"
            . " AND cencos = :centro_costo"
            . " AND codpro IN ($str_prods)"
            . " GROUP BY codpro"
        ;

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'fecha_in' => $fecha_inic,
            'fecha_out' => $fecha_term,
            'rut_cli' => $rut_cli,
            'centro_costo' => $centro_costo
        ]);

        return ($resultado != null)
            ? $resultado : false;
    }

    private static function returnStandardFormat($registros)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $controlMargen = new QvControlMargen();
            $controlMargen->setCantid($registro->cantid);
            $controlMargen->setCencos($registro->cencos);
            $controlMargen->setClieconvenio($registro->clieconvenio);
            $controlMargen->setCodcom($registro->codcom);
            $controlMargen->setCodemp($registro->codemp);
            $controlMargen->setCodlis($registro->codlis);
            $controlMargen->setCodpro($registro->codpro);
            $controlMargen->setCodven($registro->codven);
            $controlMargen->setCosprom($registro->cosprom);
            $controlMargen->setCosto($registro->costo);
            $controlMargen->setCtocte($registro->ctocte);
            $controlMargen->setExento($registro->exento);
            $controlMargen->setFecha($registro->fecha);
            $controlMargen->setFoliodoc($registro->foliodoc);
            $controlMargen->setInternet($registro->internet);
            $controlMargen->setLinea($registro->linea);
            $controlMargen->setMpropiaHst($registro->mpropia_hst);
            $controlMargen->setNeto($registro->neto);
            $controlMargen->setNomreg($registro->nomreg);
            $controlMargen->setNumcot($registro->numcot);
            $controlMargen->setNumnvt($registro->numnvt);
            $controlMargen->setOrdweb($registro->ordweb);
            $controlMargen->setProdconvenio($registro->prodconvenio);
            $controlMargen->setRutcli($registro->rutcli);
            $controlMargen->setSucDestino($registro->suc_destino);
            $controlMargen->setSucOri($registro->suc_ori);
            $controlMargen->setSupervisor($registro->supervisor);
            $controlMargen->setTipCos($registro->tip_cos);
            $controlMargen->setTipo($registro->tipo);
            $controlMargen->setTipweb($registro->tipweb);
            $arrayReturn[] = $controlMargen;
        }

        return $arrayReturn;
    }
}