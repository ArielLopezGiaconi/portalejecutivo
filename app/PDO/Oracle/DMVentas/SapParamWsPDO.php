<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 13:47
 */

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SapParamWsPDO extends Model
{
    public static function getCredentials()
    {
        $sql = "SELECT usuario, passwd FROM sap_param_ws"
            . " WHERE estado = :status "
            // . " AND id = :identifier"
        ;

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            // 'identifier' => 2,
            'status' => 1
        ]);

        return $resultado[0];
    }
}
