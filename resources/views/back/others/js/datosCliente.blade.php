<script>
    var meses = [];
    meses['01'] = 'Enero';
    meses['02'] = 'Febrero';
    meses['03'] = 'Marzo';
    meses['04'] = 'Abril';
    meses['05'] = 'Mayo';
    meses['06'] = 'Junio';
    meses['07'] = 'Julio';
    meses['08'] = 'Agosto';
    meses['09'] = 'Septiembre';
    meses['10'] = 'Octubre';
    meses['11'] = 'Noviembre';
    meses['12'] = 'Diciembre';

    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var moduleid_global = 6;

    /* inicio:Ficha */
    function buscaCliente() {
        var rutcli = $('#tx_rut_cliente').val();
        compruebaExisteRut(rutcli);
    }

    function buscaCliente(rutcli) {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            url: '{{ url('getDatosCliente') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    console.log(json);
                    document.getElementById("rut").innerHTML = rutcli;
                    
                }
                else{
                    $('#tx_rut_cliente').val('');

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json;

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
            }
        });
    }
</script>