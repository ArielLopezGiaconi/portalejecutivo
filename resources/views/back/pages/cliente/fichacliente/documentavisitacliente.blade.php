@extends('back.master.masterpage')
@section('contenido')
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><i class="icofont icofont-foot-print text-inverse m-r-10 f-40 m-t-15-neg" style="box-shadow: none;-moz-box-shadow: none;"></i>Visitas a cliente</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icofont icofont-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ url('visitascliente') }}">Visitas a cliente</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <div class="page-body">
                    <span>Sin información</span>
                </div>

            </div>
        </div>

    </div>
    @include('back.others.js.portal')
@endsection
@section('custom-includes')
@endsection
