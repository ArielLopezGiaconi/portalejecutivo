<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 31-05-2019
 * Time: 13:27
 */

namespace App\EPDO\MySQL\PricingServer\PortalEjecutivo;

use Illuminate\Database\Eloquent\Model;
use DB;
use Mockery\Exception;

class PortalEjUsuariosEPDO extends Model
{
    /* ELOQUENT PART */
    protected $connection = 'mysql_portal_ejecutivo';

    protected $table = 'portal_ej_usuarios';

    protected $primaryKey = 'codusu';
    public $incrementing = false;

    protected $fillable = array(
        'codusu',
        'rutusu',
        'email',
        'password',
        'avatar',
        'nombre',
        'perfil',
        'estado',
        'superadmin',
    );

    protected $dates = array(
        'fecha_creacion',
        'fecha_modificacion',
        'fecha_eliminacion',
    );

    public $timestamps = true;

    /*public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }*/
}