<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 13:50
 */


namespace App\PDO\MySql\Dimerc;

use App\Entities\MySql\Dimerc\DimProdOpl;
use Illuminate\Database\Eloquent\Model;
use DB;

class DimProdOplPDO extends Model
{
    public static function getRutProd($in_codpro)
    {
        $sql = "SELECT rut FROM dim_prod_opl "
            . " WHERE sku = :cod_pro "
            . " ORDER BY id ASC LIMIT 0,1";

        $resultado = DB::connection('mysql_magentodimerc')->select($sql, [
            'cod_pro' => $in_codpro
        ]);

        return DimProdOplPDO::returnDimProdFormat($resultado, true);
    }

    private static function returnDimProdFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $prod = new DimProdOpl();
            // $prod->setId($registro->id);
            $prod->setRut($registro->rut);
            // $prod->setSku($registro->sku);
            if ($first_row) {
                return $prod;
            }

            $arrayReturn[] = $prod;
        }

        return $arrayReturn;
    }
}