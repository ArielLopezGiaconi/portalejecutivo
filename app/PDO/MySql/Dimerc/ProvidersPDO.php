<?php
/**
 * Created by PhpStorm.
 * User: fburgos
 * Date: 2019-01-16
 * Time: 12:20
 */

namespace App\PDO\MySql\Dimerc;

use App\Entities\MySql\Magento\TelemarketingOrderEntity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;


class ProvidersPDO extends Model
{
    public static function getSpecificCodsapProvider($in_rutprov, $in_firstrow = true)
    {
        $sql = "SELECT id_sap FROM "
            . " providers"
            . " WHERE rut = :rut_prv";

        $resultado = DB::connection('mysql_magentodimerc')->select($sql, [
            'rut_prv' => $in_rutprov
        ]);

        return (isset($resultado) && count($resultado) > 0) ? $resultado[0]->id_sap : '100007';
    }
}