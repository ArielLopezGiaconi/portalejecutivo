<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 05-02-2019
 * Time: 16:55
 */

namespace App\Entities\Local;

class InsertProductWS
{
    protected $codsap;
    protected $material;
    protected $almacen;
    protected $centro;
    protected $stock_fisico;
    protected $stock_compro;
    protected $stock_dispon;
    protected $costo_promedio;

    public function __construct()
    {

    }

    public function getCodsap()
    {
        return $this->codsap;
    }

    public function setCodsap($codsap)
    {
        $this->codsap = $codsap;
    }

    public function getMaterial()
    {
        return $this->material;
    }

    public function setMaterial($material)
    {
        $this->material = $material;
    }

    public function getAlmacen()
    {
        return $this->almacen;
    }

    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;
    }

    public function getCentro()
    {
        return $this->centro;
    }

    public function setCentro($centro)
    {
        $this->centro = $centro;
    }

    public function getStockFisico()
    {
        return $this->stock_fisico;
    }

    public function setStockFisico($stock_fisico)
    {
        $this->stock_fisico = $stock_fisico;
    }

    public function getStockCompro()
    {
        return $this->stock_compro;
    }

    public function setStockCompro($stock_compro)
    {
        $this->stock_compro = $stock_compro;
    }

    public function getStockDispon()
    {
        return $this->stock_dispon;
    }

    public function setStockDispon($stock_dispon)
    {
        $this->stock_dispon = $stock_dispon;
    }

    public function getCostoPromedio()
    {
        return $this->costo_promedio;
    }

    public function setCostoPromedio($costo_promedio)
    {
        $this->costo_promedio = $costo_promedio;
    }

}
