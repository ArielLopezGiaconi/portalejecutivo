<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 9:19
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\MaBodegas;
use \DB;
use Illuminate\Database\Eloquent\Model;

class MaBodegasPDO extends Model
{
    public static function existsActiveWharehouseByCod($in_codbod, $in_codemp)
    {
        $sql = "SELECT COUNT(*) AS existe FROM ma_bodegas"
            . " WHERE activa = :status"
            . " AND codbod = :cod_bod"
            . " AND codemp = :cod_emp";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'status' => 'S',
            'cod_bod' => $in_codbod,
            'cod_emp' => $in_codemp
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getBodegasByCodbod($in_codbod, $in_codemp, $first_row = false) {
        $conexion = ($in_codemp == 3) ? 'oracle_dmventas' : 'oracle_unificado';
        $sql = "SELECT * FROM ma_bodegas "
            . " WHERE codemp = :cod_emp "
            . " AND codbod = :cod_bod"
            . " AND activa = :status";

        $resultado = DB::connection($conexion)->select($sql, [
            'status' => 'S',
            'cod_bod' => $in_codbod,
            'cod_emp' => $in_codemp
        ]);

        return MaBodegasPDO::returnBodegaFormat($resultado, $first_row);
    }

    public static function getAllSapBodegas() {
        $sql = "SELECT * FROM ma_bodegas "
            . " WHERE sap_sociedad IS NOT NULL"
            . " AND sap_centro IS NOT NULL"
            . " AND sap_almacen IS NOT NULL"
        ;

        $resultado = DB::connection('oracle_dmventas')->select($sql);

        return MaBodegasPDO::returnBodegaFormat($resultado);
    }

    public static function getBodegaBySapCodes($in_centro, $in_almacen, $first_row = false) {
        $sql = "SELECT * FROM ma_bodegas "
            . " WHERE sap_centro = :sap_centro "
            . " AND sap_almacen = :sap_almacen"
        ;

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'sap_centro' => $in_centro,
            'sap_almacen' => $in_almacen
        ]);

        return MaBodegasPDO::returnBodegaFormat($resultado, $first_row);
    }

    public static function getConconcessionedWharehouse($client_rut) {
        $sql = "SELECT * FROM ma_bodegas"
            . " WHERE codbod = GETRUTCONCESION_NEW(:cod_emp, :rut_client, :cen_cos)"
            . " AND activa = :status"
            . " AND codemp = :cod_emp2";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_emp' => 3,
            'rut_client' => $client_rut,
            'cen_cos' => 0,
            'status' => 'S',
            'cod_emp2' => 3
        ]);

        return MaBodegasPDO::returnBodegaFormat($resultado, true);
    }

    public static function getSapCentroWareHouseOpl() {
        $sql = 'SELECT DISTINCT sap_centro FROM ma_bodegas'
            . ' WHERE rutcli IS NOT NULL'
            . ' AND SAP_CENTRO IS NOT NULL'
            . ' AND SAP_ALMACEN IS NOT NULL';

        $resultado = DB::connection('oracle_dmventas')->select($sql);

        return $resultado;
    }

    private static function returnBodegaFormat($registros, $first_row = false) {
        $arrayReturn = null;
        foreach($registros as $registro) {
            $bodega = new MaBodegas($registro);
            if($first_row == true) {
                return $bodega;
            }
            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }
}
