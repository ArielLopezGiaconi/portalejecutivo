<script>
    var meses = [];
    meses['01'] ='Enero';
    meses['02'] = 'Febrero';
    meses['03'] = 'Marzo';
    meses['04'] = 'Abril';
    meses['05'] = 'Mayo';
    meses['06'] = 'Junio';
    meses['07'] = 'Julio';
    meses['08'] = 'Agosto';
    meses['09'] = 'Septiembre';
    meses['10'] = 'Octubre';
    meses['11'] = 'Noviembre';
    meses['12'] = 'Diciembre';

	var lala;
    var rut_cliente='{{ $rutcli }}';
    var from_coti = '{{ $numcot or ""}}';
	var from_nota = '{{ $numnvt or ""}}';
	var from_autocoti = '{{ $autocoti or "" }}';
	var from_otra_coti = '{{ $from_otra_coti or 0 }}';
    var rut_cliente_bloqueado=false;
    var num_coti_guardada=0;
    var productos_seleccionados = [];
    var bolsa_cotizacion = [];
    var bolsa_cotizacion_completa = [];
    var count_carrusel=0;
    var count_carrusel_alter=0;
    var count_carrusel_ciclo=0;
    var set_masivo =false;
    var cliente_antofa = 0;
    var recom = 0;
    var cencos=[];
    var guardaCotiFromNuevaCoti = false;
    var setmasivoimport=false;
    var setmasivoimportmsg=false;
    var actualizapreciominimo=false;
    var oldceco='0';
    var olddirection=0;
    var moduleid_global = 11;
    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var prod_ref_alter='';
    var first_prod=0;
    var last_cell_selection=[ 0, 0 ];
    //var cobro_logistico=0;
    var tblCotizacionWindow;
	var notaGrabada = false;
	var objSubtotales;
	var numordWindow = 0;
	var mgWeb = 0;
	var yaValidada = false;
	var cobroLogisticoWeb = 0;
	var audioParado = false;
	var faltaDireccion = false;
	var fechaCajas = "";
	var confirmaAnto = false;
	var despacharDesdeStgo = false;
	var numnvtWindow = 0;
	var importarPrecio = true;
	var importarCantidad = true;
    var diasSemana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ];
    var diasSemanaCorto = ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"];
    var mesesAgno = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
                    "Septiembre","Octubre", "Noviembre", "Diciembre"];
	
	@if(env('ENV_DATABASE')!='prod')
	//archivos de audio
	var snd1  = new Audio();
	var src1  = document.createElement("source");
	src1.type = "audio/ogg";
	src1.src  = "{{ asset('assets/audio/askVoice.ogg') }}";
	snd1.appendChild(src1);
		
	var snd2  = new Audio();
	var src2  = document.createElement("source");
	src2.type = "audio/ogg";
	src2.src  = "{{ asset('assets/audio/ask.ogg') }}";
	snd2.appendChild(src2);
		
	var snd3 = new Audio();
	var src3 = document.createElement("source");
	src3.type = "audio/ogg";
	src3.src  = "{{ asset('assets/audio/loopRequest.ogg') }}";
	snd3.appendChild(src3);
	snd3.loop = true;

	var snd4 = new Audio();
	var src4 = document.createElement("source");
	src4.type = "audio/ogg";
	src4.src  = "{{ asset('assets/audio/goodRequest.ogg') }}";
	snd4.appendChild(src4);
	
	var snd5 = new Audio();
	var src5 = document.createElement("source");
	src5.type = "audio/ogg";
	src5.src  = "{{ asset('assets/audio/badRequest.ogg') }}";
	snd5.appendChild(src5);

	var snd6 = new Audio();
	var src6 = document.createElement("source");
	src6.type = "audio/ogg";
	src6.src  = "{{ asset('assets/audio/goodRequestVoice.ogg') }}";
	snd6.appendChild(src6);
	//fin archivos de audio
	@endif

	function getBolsa(){
		if(localStorage.getItem('bolsa') == null){
			return "";
		}
		if(localStorage.getItem('bolsa') == ""){
			return "";
		}
		var productos = JSON.parse(localStorage.getItem('bolsa'));
		var bolsa = [];
		
		productos.forEach(function(pro){
			if(pro.codpro!='WW00008' && pro[0] != null){
            	var precio = pro[3];
            	var cantidad = pro[4];
            	var total = pro[5];
            	var stock = pro[9];
                bolsa.push({ "Codigo": pro[0], "Descripcion": pro[1], "Marca": pro[2], "Precio": precio, "Cantidad": cantidad, "Total": total, "Margen": pro[6], "Comision": pro[7], "Condicion": pro[8], "Stock": stock, "Costo": pro[10], "Suc": pro[11], "Convenio": pro[12], "Recomendado": pro[13], "Transporte": "$0" });
            }

        });
		bolsa.push({ "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" });
		return bolsa;
	}
	
	function setBolsa(bolsa){
		if(bolsa[0][0]!=null){
			localStorage.setItem('bolsa', JSON.stringify(bolsa));
		}
	}
	
	function cleanBolsa(){
		localStorage.setItem('bolsa', "");
	}
	
    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    function redirecturl(id){
        switch (id) {
            case 'btn_editcoti':
                window.location = "{{ url('editacotizacion') }}/" + rut_cliente + "/generacotizacion";
                break;
            case 'btn_listcoti':
                invocaRegistroBitacora('vercotis');
				//var win = window.open("{{ url('cotizaciones') }}/" + rut_cliente, '_blank');
  				//win.focus();
				window.location = "{{ url('cotizaciones') }}/" + rut_cliente;
                break;
            case 'btn_fichacliente':
                invocaRegistroBitacora('fichacliente');
                window.location = "{{ url('fichacliente') }}/" + rut_cliente;

                break;

        }
    }
    function refreshPage(){
        invocaRegistroBitacora('refresh');
        location.reload();

    }
    function confirmaRedireccion(id){
        if(bolsa_cotizacion.length > 0 && num_coti_guardada == 0){
            swal({
                title: "驴Quieres guardar antes la cotizaci贸n actual?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                    cancel: "Cancelar",
                    catch: {
                        text: "No guardar",
                        value: "nosave",
                    },
                    defeat: {
                        text:"Guardar",
                        value:"save"
                    },
                }
            })
                .then((value) => {
                    switch (value) {

                        case "cancel":

                            break;

                        case "save":
                            document.getElementById('btn_guarda_coti').click();
                            guardaCotiFromNuevaCoti = true;
                            break;

                        case "nosave":
                            swal("No se guardaron los cambios.", {
                                icon: ""
                            }).then((ok) => {
                                if (ok) {
                                    redirecturl(id);
                                }
                            });
                    }
                });
        }
        else{
            redirecturl(id);
        }
    }
    function asignarUrlPreviousPage(){
        var nombrepagina='';
        var idbtn='';
        var previouspage='{{ $previouspage }}';

        switch (previouspage) {
            case 'listacotizaciones':
                nombrepagina='Lista cotizaciones';
                idbtn='btn_listcoti';
                break;
            case 'editacotizacion':
                nombrepagina='Editar cotizaci贸n';
                idbtn='btn_editcoti';
                break;
            case 'fichacliente':
                nombrepagina='Ficha cliente';
                idbtn='btn_fichacliente';
                break;
            case '':
                nombrepagina='Ficha cliente';
                idbtn='btn_fichacliente';
                break;
        }

        document.getElementById('btn_volver').setAttribute('data-original-title', 'Volver a ' + nombrepagina);
        document.getElementById('btn_volver').setAttribute('onclick', 'javascript:confirmaRedireccion("' + idbtn + '");');
    }

    function initializeGeneraCotizacion(){
        var rutcli = '{{ $rutcli }}';
        getInfoClienteRut(rutcli);
        makeInfoTotalesCotizacion();
    }
    function pantallaCompleta(vista){
        if(vista == 'fichacliente'){
            var sc = rut_cliente;
            if(sc == '') {
                $('#btn_buscar_cliente').click();
                document.getElementById('p_body').className = 'd-none';
                document.getElementById('name_client').innerHTML = 'No ha seleccionado ning煤n cliente';
            }
            else{
                $('#mobile-collapse').click();
                document.getElementById('p_body').className = 'd-block';
                initializeFichaCliente();
                // console.log($('#potencial').val());
            }
        }
        if(vista == 'generacoti') {
            $('#mobile-collapse').click();
            $('#mobile-collapse').click(function () {
                var boton = document.getElementById('mobile-collapse');
                var icono = boton.getElementsByTagName('i')[0];

                if (icono.className == 'feather icon-menu icon-toggle-right') {
                    document.getElementById('tbl_cotizacion').setAttribute('style', "height: 260px; overflow: hidden; width: 1050px;");
                    document.getElementById('tbl_cotizacion').setAttribute('data-originalstyle', "height: 260px; overflow: hidden; width: 1050px;");
                    document.getElementsByClassName('wtHolder')[0].setAttribute('style', "position: relative; height: 260px; width: 1050px;");
                    document.getElementsByClassName('wtHider')[0].setAttribute('style', "width: 1030px; height: 533px;");
                } else if (icono.className == 'feather icon-menu icon-toggle-left') {
                    document.getElementById('tbl_cotizacion').setAttribute('style', "height: 260px; overflow: hidden; width: 1300px;");
                    document.getElementById('tbl_cotizacion').setAttribute('data-originalstyle', "height: 260px; overflow: hidden; width: 1300px;");
                    document.getElementsByClassName('wtHolder')[0].setAttribute('style', "position: relative; height: 260px; width: 1300px;");
                    document.getElementsByClassName('wtHider')[0].setAttribute('style', "width: 1283px; height: 533px;");
                }
            });

        }
    }
    function getInfoClienteRut(rutcli) {
        var rutcliente='';

        if(rutcli==null||rutcli==""){
            rutcliente = rut_cliente;
        }
        else{
            rutcliente=rutcli;
        }

        if(rutcliente!=""&&rutcliente!=null){
            var vendedor = ejecutivo;

            var parametros = {
                "vendedor": vendedor,
                "rut": rutcliente,
                "codemp": 3
            };
            $.ajax({
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                },
                data: parametros,
                url: '{{ env("URL_API") }}/api/clientes/buscar-especifico',
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                beforeSend: function () {
                    document.getElementById("pre_loader").className = "loader animation-start";
                },
                success: function (json) {
                    if (json.code == 200) {

                        makeInfoClientInputsModal(json.response);

                        if(rut_cliente==''){ //esta l韓ea es medio rara :c
                            rut_cliente = rutcliente;
                            document.getElementById('btn_lista_cotizacion').href='{{ url('listacotizacion') }}/' + rut_cliente;
                        }

                        //Llenar popover
                        var ceco = document.getElementsByName('select_ceco')[0].value;

                        //llenarPopoverCliente(rutcliente, ceco, json.response.forpag, json.response.plapag, json.response.estado_cliente);

                        //Si generamos una coti a partir de otra entonces trae los datos de la coti
                        if(from_coti!="" || from_autocoti != ""){
							if(from_coti.substr(0,1)=="W"){ //Si es nota web, busco nota web
								from_coti = from_coti.substr(1);
								obtieneDetalleCotizacion(0,from_coti);
							}
							else{
								if(from_autocoti != ""){ //si viene de cotizacion automatica
									obtieneDetalleCotizacion(0,0,0, json.response.cencos[0].dir_cod, json.response.cencos[0].cen_cos);
								}
								else{
									obtieneDetalleCotizacion(from_coti); //Si genero nota desde coti
								}
							}
                        }
                        //Si no viene de nota de venta genera una coti desde cero
                        else{
							if(from_nota!=""){
								obtieneDetalleCotizacion(0,0,from_nota);
							}
							else{
								document.getElementById('dv_buscar_numcot').className = 'float-right m-r-350 m-t-5-neg width-350-p d-block';
								if(getBolsa()!=""){ //si hay data de autorrecuperacion
									swal({
                						title: "Recuperar cotizaci贸n",
										text: "驴Desea recuperar la ultima cotizaci贸n no guardada?",
                						icon: "info",
                						buttons: true,
                						dangerMode: true,
                						buttons: {
										defeat: {
                        					text:"Si",
                        					value:"regen"
                    						},
                    					cancel: "No",
										deldata: {
                        					text:"Borrar informaci贸n de autorrecuperaci贸n",
                        					value:"delete"
                    						}
                						},
										closeOnCancel: false
            						})
                					.then((value) => {
										if(value != null){
											if(value == "regen"){ //si elige regenerar
												set_masivo = true;
												makeTablaCotizacion(getBolsa());
												set_masivo = false;
											}
											if(value == "delete"){ //si elige borrar data
												cleanBolsa();
												makeTablaCotizacion();
											}
										}
										else{ //si no le interesa
											makeTablaCotizacion();
										}
                					});
								}
								else{
									makeTablaCotizacion();
								}
							} 
                        }
                    }
                    else{
                        rut_cliente_bloqueado=true;

                        var nFrom = "top";
                        var nAlign = "center";
                        var nIcons = "fa fa-times";
                        var nType = "danger";
                        var nAnimIn = "animated bounceInRight";
                        var nAnimOut = "animated bounceOutRight";
                        var title = "";
                        var message = json.response;

                        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                    }

                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    registroError(ejecutivo, moduleid_global, 'buscar cliente por rut', code, text);
                }
            });
        }
    }
    function llenarPopoverCliente(rutcliente, ceco, forpag, plapag, estado, direccion){
        var data=[];
        data.push({ cencos: ceco, formapago: forpag, plazopago: plapag, estado: estado, direccion: direccion });
        getInfoClienteOracleForCotizacion(rutcliente, data);
    }
    function makeInfoClientInputsModal(obj){

        var razons=obj.razons;
        var rut=obj.rutcli + '-' +obj.digcli;
        var negocio=obj.negocio_eerr;
        var segmento=obj.segmento;
        cencos=obj.cencos;
        var formapago=obj.forpag;
        var plazopago=obj.plapag;
        var estado=obj.estado_cliente;
		var docfut = obj.tipdoc;

        // console.log(cencos);
        var cencos_onlynumber = [];

        cencos.forEach(function(cc){
            cencos_onlynumber.push([parseInt(cc.cen_cos)]);
        });

        cencos_onlynumber=cencos_onlynumber.sort(function(a, b){return a-b});
        //console.log(cencos_onlynumber);

        document.getElementById('cli_input_razons').value = razons;
        document.getElementById('cli_input_rut').value = rut;
        document.getElementById('cli_input_negocio').value = negocio;
        document.getElementById('cli_input_segmento').value = segmento;
        document.getElementById('cli_input_formapago').value = formapago;
        document.getElementById('cli_input_plazopago').value = plazopago;
        document.getElementById('cli_input_estado').value = estado;
		document.getElementsByName('select_docfut')[0].value = docfut;

        var options_select_ceco = '';
        var descco='';
        options_select_ceco = '<option value="--">Seleccione CeCo</option>\n';

        cencos_onlynumber.forEach(function(cc){

            var descripcion='';
            cencos.forEach(function(ceco){

                if(ceco.cen_cos==cc.toString()){
                    descripcion=ceco.des_cco;
                }
            });

            options_select_ceco = options_select_ceco + '<option value="' + cc + '">' + cc + ' - ' + descripcion + '</option>\n';

        });

        cencos.forEach(function(ceco){

            if(ceco.cen_cos=="0"){
                descco=ceco.des_cco;
                document.getElementById('cli_input_desceco').value = descco;
                document.getElementById('cli_input_desceco').title = descco;
            }
        });

        document.getElementsByName('select_ceco')[0].innerHTML = options_select_ceco;
        document.getElementsByName('select_ceco')[0].value = "0";
        buscarDireccionesDespacho();
    }
    function seleccionaCeCo(){

        var ceco = document.getElementsByName("select_ceco")[0].value;
        var descripcion='';

        if(ceco!="--"){
            cencos.forEach(function(cc){
                if(cc.cen_cos==ceco){
                    descripcion=cc.des_cco;
                }
            });
            document.getElementById('cli_input_desceco').value = descripcion;
            document.getElementById('cli_input_desceco').title = descripcion;

            //Llenar popover
            var rutcliente=document.getElementById('cli_input_rut').value.substr(0, 8);
            // console.log(rutcliente);
            var ceco = document.getElementsByName('select_ceco')[0].value;
            var forpag=document.getElementById('cli_input_formapago').value;
            var plapag=document.getElementById('cli_input_plazopago').value;
            var estado=document.getElementById('cli_input_estado').value;
            //llenarPopoverCliente(rutcliente, ceco, forpag, plapag, estado);

            //console.log(ceco);

            buscarDireccionesDespacho();
            invocaRegistroBitacora('cambiarceco', oldceco, ceco, '');
            oldceco=ceco;
        }
    }
    function getInfoClienteOracleForCotizacion(rutcli, data) {
        // console.log(data);
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            //headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            url: '{{ url('getdetalleclienteoracle') }}',
            data: parametros,
            type: 'POST',
            cache: true,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json) {
                if (json.code == 200) {
                    makeInfoClienteCotizacion('oracle', json.response[0], data);
                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                }
                else{
                    // console.log(json.response);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut', code, text);

                /*swal({ //descomentar!!
                    title: "Ha ocurrido un error en el servidor.",
                    text: "Favor vuelva a iniciar sesi贸n.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    buttons: {
                        ok: "Ok"
                    }
                })
                    .then((value) => {
                        switch (value) {

                            case "ok":
                                location.reload();
                                break;
                        }
                    });*/
            }
        });
    }
    function makeInfoClienteCotizacion(tipo_db, obj, data){
        // console.log(data);
        if(tipo_db=='oracle'){
            document.getElementById('tx_nameclient').innerHTML = obj.razons;

            /* Variables panel */
            var cl_razons = obj.razons;
            var cl_rutempresa = obj.rutcli + '-' + obj.digcli;
            var cl_negocio = obj.negocio;
            var cl_segmento = obj.segmento;
            var cl_relacion = obj.relacion;
            var cl_centro_costo = data[0].cencos;
            var cl_forma_pago = data[0].formapago;
            var cl_plazo_pago = data[0].plazopago;
            var cl_estado_cliente = data[0].estado;
            var cl_direc_despacho = data[0].direccion;

            var datatitle = '<span>Informaci贸n cliente</span>';

            var datacontent = '<div class="row" style="width:600px;">\n' +
                '                <div class="col-sm-12">' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Raz贸n social :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_razons">' + cl_razons + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-sm-7">\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Rut :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_rut">' + cl_rutempresa + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Negocio :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_udn">' + cl_negocio + '</p>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Segmento :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_segmento">' + cl_segmento + '</p>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Relaci贸n :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_relacion">' + cl_relacion + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-sm-5">\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Centro costo :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_cencos">' + cl_centro_costo + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Forma pago :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_formapago">' + cl_forma_pago + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Plazo pago :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_plazopago">' + cl_plazo_pago + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Estado cliente :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_estado">' + cl_estado_cliente + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="divider m-t-5 m-b-5"></div>' +
                '                <div class="col-sm-12">\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-3"><p class="text-muted m-b-5">Direcci贸n despacho :</p></div>\n' +
                '                        <div class="col-9"><p class="m-b-5 f-w-400" id="tb_infoclient_direc_despacho">' + cl_direc_despacho + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            document.getElementById('btn_infoclient').setAttribute('data-original-title', datatitle);
            document.getElementById('btn_infoclient').setAttribute('data-content', datacontent);
        }
    }
    function makeInfoTotalesCotizacion(obj, cobro=0){
        if(obj!=null) {
			objSubtotales = obj;
            /* Variables panel */
            var subtotal = '$' + obj[0];
            var iva = '$' + obj[1];
            var total = '$' + obj[2];
            var margen = obj[3] + '%';
            var costotransp = "$" + formatMoney(obj[4], 0, ',', '.');
            var cobrologistico = '$' + cobro;

            var datatitle = '<span>Totales</span>';

            var datacontent = '<div class="row" style="width:300px;">\n' +
                '                <div class="col-sm-12">\n' +
                (tieneDerecho(40001)?('                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Margen :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_margen">' + margen + '</p></div>\n' +
                '                    </div>\n'):'') +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Subtotal :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_subtotal">' + subtotal + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Iva :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_iva">' + iva + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Costo transporte :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_costo_transp">' + costotransp + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="divider m-t-5 m-b-5"></div>' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Total :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_total">' + total + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="divider m-t-5 m-b-10"></div>' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">* Cobro log铆stico :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_cobro_logis">' + cobrologistico + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-12"><p class="text-muted m-b-5">(Este valor ya se encuentra reflejado en el subtotal)</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            document.getElementById('btn_subtotal').setAttribute('data-original-title', datatitle);
            document.getElementById('btn_subtotal').setAttribute('data-content', datacontent);
            document.getElementById('tx_subtotal').innerHTML = 'Subtotal: <b>' + subtotal + '</b> |'+ (tieneDerecho(40001)?(' Margen: <b>' + margen + '</b> |'):'') + ' Costo transp: <b>' + costotransp + '</b>';
        }
        else{
            /* Variables panel */
            var subtotal = '$0';
            var iva = '$0';
            var total = '$0';
            var margen = '0,0%';
            var costotransp = "$0";
            var cobrologistico = '$0';

            var datatitle = '<span>Totales</span>';

            var datacontent = '<div class="row" style="width:300px;">\n' +
                '                <div class="col-sm-12">\n' +
                (tieneDerecho(40001)?('                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Margen :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_margen">' + margen + '</p></div>\n' +
                '                    </div>\n'): '') +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Subtotal :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_subtotal">' + subtotal + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Iva :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_iva">' + iva + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Costo transporte :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_costo_transp">' + costotransp + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="divider m-t-5 m-b-5"></div>' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Total :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_total">' + total + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="divider m-t-5 m-b-10"></div>' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">* Cobro log铆stico :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_total_cobro_logis">' + cobrologistico + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-12"><p class="text-muted m-b-5">(Este valor ya se encuentra reflejado en el subtotal)</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            document.getElementById('btn_subtotal').setAttribute('data-original-title', datatitle);
            document.getElementById('btn_subtotal').setAttribute('data-content', datacontent);
            document.getElementById('tx_subtotal').innerHTML = 'Subtotal: <b>$0</b> | ' + (tieneDerecho(40001)?'Margen: <b>0,0%</b> |':'') + ' Costo transp: <b>$0</b>';
        }
    }
    function makeTablaCotizacion(data=""){
        console.log(data);

        var dataObject = [
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" },
            { "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" }
        ];

        if(data!=""){
            dataObject = data;
        }

        var currencyCodes = ['A', 'B', 'C'];
        var flagRenderer = function (instance, td, row, col, prop, value, cellProperties) {

            var currencyCode = value;
            while (td.firstChild) {
                td.removeChild(td.firstChild);
            }
            if (currencyCodes.indexOf(currencyCode) > -1) {
                var flagElement = document.createElement('DIV');
                flagElement.className = 'flag cat-' + currencyCode.toLowerCase();
                td.appendChild(flagElement);
            } else {
                var textNode = document.createTextNode(value === null ? '' : value);

                td.appendChild(textNode);
            }
        };

        var convenioCodes = ['S', 'N'];
        var convenioRenderer = function (instance, td, row, col, prop, value, cellProperties) {

            var convenioCode = value;
            while (td.firstChild) {
                td.removeChild(td.firstChild);
            }
            if (convenioCodes.indexOf(convenioCode) > -1) {
                var flagElement = document.createElement('DIV');
                flagElement.className = 'conv ' + convenioCode.toLowerCase();
                td.appendChild(flagElement);
            } else {
                var textNode = document.createTextNode(value === null ? '' : value);

                td.appendChild(textNode);
            }
        };
		var precioRenderer = function(instance, td, row, col, prop, value, cellProperties){
			var precioCaptura = instance.getDataAtCell(row, 15);
			if(precioCaptura == 910){
				td.style.animation = "glowpiola 1s infinite alternate";
            	td.style.background = '#c3e6cb';
			}
			else{
				td.style.animation = "";
            	td.style.background = '';
			}
			Handsontable.renderers.TextRenderer.apply(this, arguments);
        };

        var debounceFn = Handsontable.helper.debounce(function (colIndex, event) {
          var filtersPlugin = tblCotizacion.getPlugin('filters');

          filtersPlugin.removeConditions(colIndex);
          filtersPlugin.addCondition(colIndex, 'contains', [event.target.value]);
          filtersPlugin.filter();
        }, 0);

        var addEventListeners = function (input, colIndex) {
          input.addEventListener('keydown', function(event) {
            debounceFn(colIndex, event);
          });
        };

        // Build elements which will be displayed in header.
        var getInitializedElements = function(colIndex) {
          var icon = document.createElement('i');
          icon.className = "fa fa-search";
          var button = document.createElement('button');
          button.className = "btn btn-default";
          button.appendChild(icon);
          return button;
        };
        var getTextBox = function(colIndex, idElement){
            var txtFiltrar = document.createElement('input');
            //txtFiltrar.className = "form-control form-control-sm";
            txtFiltrar.setAttribute("type", "text");
            txtFiltrar.id = idElement;
            return txtFiltrar;
        }

        // Add elements to header on `afterGetColHeader` hook.
        var addButton = function(col, TH) {
          // Hooks can return value other than number (for example `columnSorting` plugin use this).
          if (typeof col !== 'number') {
            return col;
          }

          if ((col == 0 || col == 1 || col == 2) && TH.childElementCount < 2 && TH.childNodes[0].childElementCount == 1) {
            TH.childNodes[0].appendChild(getInitializedElements(col));
            /*if(col == 0){
                TH.appendChild(getTextBox(col, 'filtroCodpro'));
            }
            if(col == 1){
                TH.appendChild(getTextBox(col, 'filtroDespro'));
            }
            if(col == 2){
                TH.appendChild(getTextBox(col, 'filtroMarca'));
            }*/
          }
        };

        // Deselect column after click on input.
        var doNotSelectColumn = function (event, coords) {
            if(coords.row === -1 && (event.target.className == 'fa fa-search' || event.target.className == 'btn btn-default')){
                var elementoBuscaCodpro = document.getElementById('filtroCodpro');
                var elementoBuscaDespro = document.getElementById('filtroDespro');
                var elementoBuscaMarca = document.getElementById('filtroMarca');
                event.stopImmediatePropagation();
                this.deselectCell();
                if(coords.col == 0){
                    elementoBuscaCodpro.removeAttribute("hidden");
                    elementoBuscaDespro.setAttribute("hidden","true");
                    elementoBuscaMarca.setAttribute("hidden","true");
                }
                if(coords.col == 1){
                    elementoBuscaCodpro.setAttribute("hidden","true");
                    elementoBuscaDespro.removeAttribute("hidden");
                    elementoBuscaMarca.setAttribute("hidden","true");
                }
                if(coords.col == 2){
                    elementoBuscaCodpro.setAttribute("hidden","true");
                    elementoBuscaDespro.setAttribute("hidden","true");
                    elementoBuscaMarca.removeAttribute("hidden");
                }
            }
            /*if (coords.row === -1 && event.target.nodeName === 'INPUT') {
            event.stopImmediatePropagation();
            this.deselectCell();
            }*/
        };

        var configuraciones = {
            data: dataObject,
            colHeaders: [
                'C贸digo',
                'Descripci贸n',
                'Marca',
                'Precio',
                'Cantidad',
                'Total',
                'Margen',
                'C',
                'S/P',
                'Stock',
                'Costo',
                'Suc',
                'Conv',
                'Recomendado',
                '&nbsp&nbsp&nbsp<i class="fa fa-truck f-20"></i> &nbsp&nbsp&nbsp Costo transporte'
            ],
            columns: [
                { data: "Codigo", type: "text", width:70 },
                { data: "Descripcion", type: "text", width:350 },
                { data: "Marca", type: "text", readOnly: true, width:80 },
                { data: "Precio", type: "text", width:80, renderer: precioRenderer },//renderer: "html" },
                { data: "Cantidad", type: "text", width:70 },
                { data: "Total", type: "text", readOnly: true, width:80 },
                { data: "Margen", type: "text", readOnly: true, width:60 },
                { data: "Comision", renderer: flagRenderer, type: "text", readOnly: true, width:30 },
                { data: "Condicion", type: "text", readOnly: true, width:80 },
                { data: "Stock", type: 'handsontable', readOnly: true, width:60 },
                { data: "Costo" },
                { data: "Suc", readOnly: true, width:30 },
                { data: "Convenio", renderer: convenioRenderer, readOnly: true, width:30 },
                { data: "Recomendado", width:40, readOnly: true },
                { data: "Transporte", width:30, readOnly: true },
				{ data: "Lista", width:30, readOnly: true }
            ],
            filters: true,
            afterGetColHeader: addButton,
            beforeOnCellMouseDown: doNotSelectColumn,
            headerTooltips: true,
            hiddenColumns: {
                columns: [10, 13, 15, tieneDerecho(40001)?undefined:6],
                indicators: true
            },
            overflow: 'hidden',
            stretchH: 'all',
            width: '100%',
            autoWrapRow: true,
            height: 260,
            maxRows: 1000,
            rowHeaders: true,
            licenseKey: 'non-commercial-and-evaluation',
            columnSorting: {
                indicator: true
            },
            enterMoves: {row: 0, col: 0},
            contextMenu: {
                items: {
                    'row_above': {},
                    'row_below': {},
                    'remove_row': {},
                    'separator': Handsontable.plugins.ContextMenu.SEPARATOR,
                    'precios_anteriores': {
                        name: '$ 脷ltimos precios',
                        callback: function(btn, obj) {
                            var row = obj[0].start.row;
                            var col = obj[0].start.col;
                            var data = tblCotizacion.getDataAtRow(row);

                            if(data[0] != null && data[0] != ""){

                                var parametros = {
                                    "rutcli": "{{ $rutcli }}",
                                    "cod_producto": data[0]
                                };

                                $.ajax({
                                    xhrFields: {
                                        withCredentials: true
                                    },
                                    headers: {
                                        'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                                    },
                                    data: parametros,
                                    url: '{{ env("URL_API") }}/api/productos/listar-ultimos-precios',
                                    type: 'POST',
                                    // cache: true,
                                    datatype: 'json',
                                    // async: true,
                                    beforeSend: function () {
                                        document.getElementById("pre_loader").className = "loader animation-start";
                                    },
                                    success: function (json) {

                                        if (json.code == 200) {

                                            if(json.response.productos_cotizados_rut.length>0 || json.response.productos_vendidos_rut.length>0){
                                                var pro_cot_rut = json.response.productos_cotizados_rut;
                                                var pro_ven_rut = json.response.productos_vendidos_rut;

                                                var rows_cot_rut = '';
                                                pro_cot_rut.forEach(function(pro){
                                                    rows_cot_rut = rows_cot_rut +
                                                        '                                            <tr>\n' +
                                                        '                                                <td>' + pro.numcot + '</td>\n' +
                                                        '                                                <td>' + moment(pro.fecemi).format('L') + '</td>\n' +
                                                        '                                                <td align="center">' + pro.cantid + '</td>\n' +
                                                        '                                                <td>$' + formatMoney(pro.precio, 0, ',', '.') + '</td>\n' +
                                                        '                                            </tr>\n';
                                                });

                                                var rows_ven_rut = '';
                                                pro_ven_rut.forEach(function(pro){
                                                    rows_ven_rut = rows_ven_rut +
                                                        '                                            <tr>\n' +
                                                        '                                                <td>' + pro.numnvt + '</td>\n' +
                                                        '                                                <td>' + moment(pro.fecha_creac).format('L') + '</td>\n' +
                                                        '                                                <td align="center">' + pro.cantid + '</td>\n' +
                                                        '                                                <td>$' + formatMoney(pro.precio, 0, ',', '.') + '</td>\n' +
                                                        '                                            </tr>\n';
                                                });

                                                var cont_pre_relacionados='';

                                                if(json.response.productos_cotizados_relacion.length>0 && json.response.productos_vendidos_relacion.length>0){

                                                    var pro_cot_relac = json.response.productos_cotizados_relacion;
                                                    var pro_ven_relac = json.response.productos_vendidos_relacion;

                                                    var rows_cot_relac = '';
                                                    pro_cot_relac.forEach(function(pro){
                                                        rows_cot_relac = rows_cot_relac +
                                                            '                                            <tr title="Rut: ' + pro.rutcli + '">\n' +
                                                            '                                                <td>' + pro.numcot + '</td>\n' +
                                                            '                                                <td>' + moment(pro.fecemi).format('L') + '</td>\n' +
                                                            '                                                <td align="center">' + pro.cantid + '</td>\n' +
                                                            '                                                <td>$' + formatMoney(pro.precio, 0, ',', '.') + '</td>\n' +
                                                            '                                            </tr>\n';
                                                    });

                                                    var rows_ven_relac = '';
                                                    pro_ven_relac.forEach(function(pro){
                                                        rows_ven_relac = rows_ven_relac +
                                                            '                                            <tr title="Rut: ' + pro.rutcli + '">\n' +
                                                            '                                                <td>' + pro.numnvt + '</td>\n' +
                                                            '                                                <td>' + moment(pro.fecha_creac).format('L') + '</td>\n' +
                                                            '                                                <td align="center">' + pro.cantid + '</td>\n' +
                                                            '                                                <td>$' + formatMoney(pro.precio, 0, ',', '.') + '</td>\n' +
                                                            '                                            </tr>\n';
                                                    });

                                                    cont_pre_relacionados = '                                <h6 class="text-uppercase">Relacionados</h6>\n' +
                                                        '                                <div class="row">\n' +
                                                        '                                    <div class="col-xl-6 m-r-15-neg">\n' +
                                                        '                                        <table class="table table-hover no-padding">\n' +
                                                        '                                            <thead>\n' +
                                                        '                                            <tr>\n' +
                                                        '                                                <th colspan="4" class="text-center"><label class="label label-danger">Cotizados</label></th>\n' +
                                                        '                                            </tr>\n' +
                                                        '                                            <tr>\n' +
                                                        '                                                <th>N掳 coti</th>\n' +
                                                        '                                                <th>Fecha</th>\n' +
                                                        '                                                <th>Cant</th>\n' +
                                                        '                                                <th>Precio</th>\n' +
                                                        '                                            </tr>\n' +
                                                        '                                            </thead>\n' +
                                                        '                                            <tbody>\n' +
                                                        rows_cot_relac +
                                                        '                                            </tbody>\n' +
                                                        '                                        </table>\n' +
                                                        '                                    </div>\n' +
                                                        '                                    <div class="col-xl-6 m-r-15-neg">\n' +
                                                        '                                        <table class="table table-hover no-padding">\n' +
                                                        '                                            <thead>\n' +
                                                        '                                            <tr>\n' +
                                                        '                                                <th colspan="4" class="text-center"><label class="label label-success">Vendidos</label></th>\n' +
                                                        '                                            </tr>\n' +
                                                        '                                            <tr>\n' +
                                                        '                                                <th>Nta vta</th>\n' +
                                                        '                                                <th>Fecha</th>\n' +
                                                        '                                                <th>Cant</th>\n' +
                                                        '                                                <th>Precio</th>\n' +
                                                        '                                            </tr>\n' +
                                                        '                                            </thead>\n' +
                                                        '                                            <tbody>\n' +
                                                        rows_ven_relac +
                                                        '                                            </tbody>\n' +
                                                        '                                        </table>\n' +
                                                        '                                    </div>\n' +
                                                        '                                </div>\n';
                                                }

                                                var cont_precios_rut = '<p class="d-inline"><b>C贸d:</b> <code>' + data[0] + '</code></p>\n' +
                                                    '                            <p><b>Des:</b> <code>' + data[1] + '</code></p>\n' +
                                                    '                            <div class="">\n' +
                                                    '                                <h6 class="text-uppercase">Precios Rut: <code>' + rut_cliente + '</code></h6>\n' +
                                                    '                                <div class="row">\n' +
                                                    '                                    <div class="col-xl-6 m-r-15-neg">\n' +
                                                    '                                        <table class="table table-hover no-padding">\n' +
                                                    '                                            <thead>\n' +
                                                    '                                            <tr>\n' +
                                                    '                                                <th colspan="4" class="text-center"><label class="label label-danger">Cotizados</label></th>\n' +
                                                    '                                            </tr>\n' +
                                                    '                                            <tr>\n' +
                                                    '                                                <th>N掳 coti</th>\n' +
                                                    '                                                <th>Fecha</th>\n' +
                                                    '                                                <th>Cant</th>\n' +
                                                    '                                                <th>Precio</th>\n' +
                                                    '                                            </tr>\n' +
                                                    '                                            </thead>\n' +
                                                    '                                            <tbody>\n' +
                                                    rows_cot_rut +
                                                    '                                            </tbody>\n' +
                                                    '                                        </table>\n' +
                                                    '                                    </div>\n' +
                                                    '                                    <div class="col-xl-6 m-r-15-neg">\n' +
                                                    '                                        <table class="table table-hover no-padding">\n' +
                                                    '                                            <thead>\n' +
                                                    '                                            <tr>\n' +
                                                    '                                                <th colspan="4" class="text-center"><label class="label label-success">Vendidos</label></th>\n' +
                                                    '                                            </tr>\n' +
                                                    '                                            <tr>\n' +
                                                    '                                                <th>Nta vta</th>\n' +
                                                    '                                                <th>Fecha</th>\n' +
                                                    '                                                <th>Cant</th>\n' +
                                                    '                                                <th>Precio</th>\n' +
                                                    '                                            </tr>\n' +
                                                    '                                            </thead>\n' +
                                                    '                                            <tbody>\n' +
                                                    rows_ven_rut +
                                                    '                                            </tbody>\n' +
                                                    '                                        </table>\n' +
                                                    '                                    </div>\n' +
                                                    '                                </div>\n' +
                                                    cont_pre_relacionados +
                                                    '                            </div>';
                                                document.getElementById('m_body_precios').innerHTML = cont_precios_rut;

                                                $('#btn_precios').click();
                                            }
                                            else{
                                                var nFrom = "top";
                                                var nAlign = "center";
                                                var nIcons = "fa fa-warning";
                                                var nType = "warning";
                                                var nAnimIn = "animated bounceInRight";
                                                var nAnimOut = "animated bounceOutRight";
                                                var title = "";
                                                var message = "No hay precios anteriores para mostrar";

                                                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                                            }

                                        }
                                        else {
                                            var nFrom = "top";
                                            var nAlign = "center";
                                            var nIcons = "fa fa-times";
                                            var nType = "danger";
                                            var nAnimIn = "animated bounceInRight";
                                            var nAnimOut = "animated bounceOutRight";
                                            var title = "";
                                            var message = json.response;

                                            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                                        }
                                        document.getElementById("pre_loader").className = "loader animation-start d-none";
                                    },
                                    error: function(e){
                                        var code = e.status;
                                        var text = e.statusText;
                                        registroError(ejecutivo, moduleid_global, 'listar ultimos precios', code, text);
                                    }
                                })
                            }
                        }
                    },
                    'separator2': Handsontable.plugins.ContextMenu.SEPARATOR,
					'ficha_tecnica': {
                        name: 'Ficha t茅cnica producto',
                        callback: function(btn, obj) {
                            var row = obj[0].start.row;
                            var data = tblCotizacion.getDataAtRow(row);
                            var codpro = data[0];
							if(codpro != null && codpro != ""){
								var parametros = {codpro: codpro};
								$.ajax({
            						xhrFields: {
                						withCredentials: true
            						},
            						headers: {
                						'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            						},
            						data:parametros,
            						url: '{{ env("URL_API") }}/api/productos/get-datos-adicionales',
            						type: 'POST',
            						cache: false,
            						datatype: 'json',
            						async: true,
            						success: function (json) {
                						//console.log(json);
                						if(json.code==200){
											$("#modal_datosprod_img").attr("src", getUrlFotoProd(json.response.codpro));
											$("#modal_datosprod_codpro").text(json.response.codpro);
											$("#modal_datosprod_despro").text(json.response.despro);
											$("#modal_datosprod_embalaje").text(json.response.embalaje);
											$("#modal_datosprod_subembalaje").text(json.response.subembalaje);
											$("#modal_datosprod_peso").text(json.response.peso);
											$("#modal_datosprod_volumen").text(json.response.volumen);
                    						$("#modal_datosprod").modal("show");
                						}
            						},
            						error: function(e){
                						var code = e.status;
                						var text = e.statusText;
                						registroError(ejecutivo, moduleid_global, 'lista precio recomendado', code, text);
            						}
        						});
							}						
                        }
                    },
                    'separator4': Handsontable.plugins.ContextMenu.SEPARATOR,
                    'precio_sugerido': {
                        name: '* Precio sugerido',
                        callback: function(btn, obj) {
                            var row = obj[0].start.row;
                            var data = tblCotizacion.getDataAtRow(row);
                            var codpro = data[0];

                            if(codpro != null && codpro != ""){
                                //Obtiene y muestra precio sugerido si corresponde
                                getPrecioSugerido(codpro, true);
                            }
                        }
                    },
                    'separator5': Handsontable.plugins.ContextMenu.SEPARATOR,
                    'alternativos': {
                        name: '* Productos alternativos',
                        callback: function(btn, obj){
                            var row = obj[0].start.row;
                            var data = tblCotizacion.getDataAtRow(row);
                            var codpro = data[0];

                            if(codpro != null && codpro != ""){
                                //Busca productos alternativos
                                $('#btn-alter').click();
                                buscaProductosAlternativos(codpro, tblCotizacion);
                            }
                        }
                    }
                }
            },
            afterRemoveRow: function(){
                var tabla = tblCotizacion.getData();
                actualizaBolsaCotizacion(tabla, tblCotizacion);
            },
            language: 'es-MX',
            manualRowMove: true,
            beforeChange: function (changes, source) {
                for (var i = changes.length - 1; i >= 0; i--) {

                    if(changes[i][1] == 'Codigo'){
						if(changes[i][3] != null){
							changes[i][3] = changes[i][3].toUpperCase();
						}
                    }

                    else{
                        var celda_codigo = tblCotizacion.getDataAtRow(changes[i][0]);

                        if (celda_codigo[0] != "" && celda_codigo[0] != null) {

                            if(changes[i][1] == 'Precio'){

                                var obj_numeric = changes[i][3].replace('$', '');
                                obj_numeric = obj_numeric.replace(',', '');
                                obj_numeric = obj_numeric.replace('.', '');
                                changes[i][3] = '$' + formatMoney(obj_numeric.trim(), 0, ',', '.');
                            }
                            else if(changes[i][1] == 'Cantidad'){

                                var obj_numeric = changes[i][3].replace(',', '');
                                obj_numeric = obj_numeric.replace('.', '');
                                changes[i][3] = formatMoney(obj_numeric.trim(), 0, ',', '.');
                            }
                        }
                    }
                }
            },
            afterSelection: function(row, col){
                // console.log(row);
                // console.log(col);
                last_cell_selection =[{ row, col }];

                var valor = tblCotizacion.getDataAtRow(row, col);

                var codpro=valor[0];
                var despro=valor[1];

                if(codpro!="" && despro!=""){
                    if(prod_ref_alter!="" && prod_ref_alter!=codpro){
                        //Busca productos alternativos
                        var btnAlter = document.getElementById('btn-alter');
                        if(btnAlter.className.includes('active')){
                            buscaProductosAlternativos(codpro, tblCotizacion);
                        }
                    }
                }
            }
        };

        var elemento = document.getElementById('tbl_cotizacion');
        var tblCotizacion = new Handsontable(elemento, configuraciones);
        tblCotizacionWindow = tblCotizacion;
        var elementoBuscaCodpro = document.getElementById('filtroCodpro');
        addEventListeners(elementoBuscaCodpro, 0);
		var elementoBuscaDespro = document.getElementById('filtroDespro');
        addEventListeners(elementoBuscaDespro, 1);
        var elementoBuscaMarca = document.getElementById('filtroMarca');
        addEventListeners(elementoBuscaMarca, 2);

		if(bolsa_cotizacion.length == 0){
			getRecomendados(rut_cliente, [], tblCotizacion);
		} //si algo se cae descomentar!

        var firstcellchange = '';

        const hiddenColumnsPlugin = tblCotizacion.getPlugin('hiddenColumns');

        var btnAgregarCotizacion = document.getElementById('btn_agregar_coti');

        Handsontable.dom.addEvent(btnAgregarCotizacion, 'click', function () {

            /* Recorrer table temporal */
            var tabla_temporal = document.getElementById('tbl_temporal');
            var rows_tbl = tabla_temporal.getElementsByTagName('tr');

            var cantProducts = rows_tbl.length;
            var contarfila=0;
            var contarprod=0;

            if(cantProducts>0) {

                var tabla = tblCotizacionWindow.getData();

                var count_filas_vacias = 0;
                var ultima_fila = 0;

                tabla.forEach(function (row) {
                    row[0] = row[0] == null ? "" : row[0];
                    row[1] = row[1] == null ? "" : row[1];
                    row[2] = row[2] == null ? "" : row[2];
                    row[3] = row[3] == null ? "" : row[3];
                    row[4] = row[4] == null ? "" : row[4];
                    row[5] = row[5] == null ? "" : row[5];
                    row[6] = row[6] == null ? "" : row[6];
                    row[7] = row[7] == null ? "" : row[7];
                    row[8] = row[8] == null ? "" : row[8];
                    row[9] = row[9] == null ? "" : row[9];
                    row[10] = row[10] == null ? "" : row[10];
                    row[11] = row[11] == null ? "" : row[11];
                    row[12] = row[12] == null ? "" : row[12];
                    row[13] = row[13] == null ? "" : row[13];
                    row[14] = row[14] == null ? "" : row[14];
					row[15] = row[15] == null ? "" : row[15];
                    if (row[0] == "") {
                        count_filas_vacias++;
                    } else {
                        ultima_fila++;
                    }
                });

                /* Crear filas si las filas a agregar son m谩s que las filas vacias */
                var filas_crear = cantProducts > 0 ? cantProducts - count_filas_vacias : 0;

                if (filas_crear > 0) {
                    tblCotizacionWindow.alter('insert_row', ultima_fila + 1, filas_crear + 1);
                    tabla = tblCotizacionWindow.getData();
                }

                if(cantProducts==count_filas_vacias){
                    tblCotizacionWindow.alter('insert_row', ultima_fila + 1, 1);
                    tabla = tblCotizacionWindow.getData();
                }

                tabla.forEach(function (row) {
                    row[0] = row[0] == null ? "" : row[0];
                    row[1] = row[1] == null ? "" : row[1];
                    row[2] = row[2] == null ? "" : row[2];
                    row[3] = row[3] == null ? "" : row[3];
                    row[4] = row[4] == null ? "" : row[4];
                    row[5] = row[5] == null ? "" : row[5];
                    row[6] = row[6] == null ? "" : row[6];
                    row[7] = row[7] == null ? "" : row[7];
                    row[8] = row[8] == null ? "" : row[8];
                    row[9] = row[9] == null ? "" : row[9];
                    row[10] = row[10] == null ? "" : row[10];
                    row[11] = row[11] == null ? "" : row[11];
                    row[12] = row[12] == null ? "" : row[12];
                    row[13] = row[13] == null ? "" : row[13];
                    row[14] = row[14] == null ? "" : row[14];
					row[15] = row[15] == null ? "" : row[15];
                });

                set_masivo = true;
                tabla.forEach(function (row) {

                    if (cantProducts>0) {

                        if (row[0] == "") {

                            if(setmasivoimport) {
                                setmasivoimportmsg=true;
                                tblCotizacionWindow.setDataAtCell(contarfila, 0, rows_tbl[0].getElementsByTagName('td')[0].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 1, rows_tbl[0].getElementsByTagName('td')[1].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 2, rows_tbl[0].getElementsByTagName('td')[2].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 3, rows_tbl[0].getElementsByTagName('td')[3].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 4, rows_tbl[0].getElementsByTagName('td')[11].innerHTML);

                                var precio = rows_tbl[0].getElementsByTagName('td')[3].innerHTML.replace('$', '').replace('.', '');
                                var cantidad = rows_tbl[0].getElementsByTagName('td')[11].innerHTML;
                                var total = precio * cantidad;

                                var costo = rows_tbl[0].getElementsByTagName('td')[8].innerHTML;
                                var margen = precio > 0 ? (((precio - costo) / precio) * 100).toFixed(2).toString() + '%' : '0';

                                tblCotizacionWindow.setDataAtCell(contarfila, 5, '$' + formatMoney(total, 0, ',', '.'));
                                //tblCotizacion.setDataAtCell(contarfila, 6, rows_tbl[0].getElementsByTagName('td')[6].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 6, margen);
                                tblCotizacionWindow.setDataAtCell(contarfila, 7, rows_tbl[0].getElementsByTagName('td')[4].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 8, rows_tbl[0].getElementsByTagName('td')[5].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 9, rows_tbl[0].getElementsByTagName('td')[7].innerHTML);

                                //getPrecioSugerido(rows_tbl[0].getElementsByTagName('td')[0].innerHTML);

                                if (cliente_antofa == 1) {
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'readOnly', false);
                                    //Stock
                                    var stock = [
                                        {nombre: 'Stgo', valor: rows_tbl[0].getElementsByTagName('td')[7].innerHTML},
                                        {
                                            nombre: 'Anto',
                                            valor: formatMoney(rows_tbl[0].getElementsByTagName('td')[9].innerHTML, 0, ',', '.')
                                        }
                                    ];
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'handsontable', {
                                        data: stock, autoColumnSize: true,
                                        getValue: function () {
                                            var selection = this.getSelectedLast();
                                            return this.getSourceDataAtRow(selection[0]).valor;
                                        }
                                    });
                                } else {
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'readOnly', true);
                                }

                                tblCotizacionWindow.setDataAtCell(contarfila, 10, rows_tbl[0].getElementsByTagName('td')[8].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 11, "STGO");
                                tblCotizacionWindow.setDataAtCell(contarfila, 12, rows_tbl[0].getElementsByTagName('td')[10].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 13, recom);
                                tblCotizacionWindow.setDataAtCell(contarfila, 14, '$0');
                            }
                            else {
                                setmasivoimportmsg=false;
                                tblCotizacionWindow.setDataAtCell(contarfila, 0, rows_tbl[0].getElementsByTagName('td')[0].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 1, rows_tbl[0].getElementsByTagName('td')[1].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 2, rows_tbl[0].getElementsByTagName('td')[2].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 3, rows_tbl[0].getElementsByTagName('td')[3].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 4, '1');
                                tblCotizacionWindow.setDataAtCell(contarfila, 5, '$' + formatMoney(rows_tbl[0].getElementsByTagName('td')[3].innerHTML, 0, ',', '.'));//'$' + formatMoney(rows_tbl[0].getElementsByTagName('td')[3].innerHTML, 0, ',', '.')
                                tblCotizacionWindow.setDataAtCell(contarfila, 6, rows_tbl[0].getElementsByTagName('td')[6].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 7, rows_tbl[0].getElementsByTagName('td')[4].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 8, rows_tbl[0].getElementsByTagName('td')[5].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 9, rows_tbl[0].getElementsByTagName('td')[7].innerHTML);

                                getPrecioSugerido(rows_tbl[0].getElementsByTagName('td')[0].innerHTML);

                                if (cliente_antofa == 1) {
                                    //console.log('agregar productos masivos, cliente antofa;')
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'readOnly', false);
                                    //Stock
                                    var stock = [
                                        {nombre: 'Stgo', valor: rows_tbl[0].getElementsByTagName('td')[7].innerHTML},
                                        {
                                            nombre: 'Anto',
                                            valor: formatMoney(rows_tbl[0].getElementsByTagName('td')[9].innerHTML, 0, ',', '.')
                                        }
                                    ];
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'handsontable', {
                                        data: stock, autoColumnSize: true,
                                        getValue: function () {
                                            var selection = this.getSelectedLast();
                                            return this.getSourceDataAtRow(selection[0]).valor;
                                        }
                                    });
                                } else {
                                    tblCotizacionWindow.setCellMeta(contarfila, tblCotizacionWindow.propToCol('Stock'), 'readOnly', true);
                                }

                                tblCotizacionWindow.setDataAtCell(contarfila, 10, rows_tbl[0].getElementsByTagName('td')[8].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 11, "STGO");
                                tblCotizacionWindow.setDataAtCell(contarfila, 12, rows_tbl[0].getElementsByTagName('td')[10].innerHTML);
                                tblCotizacionWindow.setDataAtCell(contarfila, 13, recom);
                                tblCotizacionWindow.setDataAtCell(contarfila, 14, rows_tbl[0].getElementsByTagName('td')[11].innerHTML);
								tblCotizacionWindow.setDataAtCell(contarfila, 15, rows_tbl[0].getElementsByTagName('td')[12].innerHTML);

                                //Busca productos alternativos
                                var codpro = rows_tbl[0].getElementsByTagName('td')[0].innerHTML;
                                var btnAlter = document.getElementById('btn-alter');
                                if(btnAlter.className.includes('active')){
                                    buscaProductosAlternativos(codpro, tblCotizacionWindow);
                                }
                            }

                            //console.log(rows_tbl[0]);
                            tabla_temporal.deleteRow(0);
                        }
                        contarfila++;
                    }
                    cantProducts = rows_tbl.length;

                    // console.log('cantidad productos agregados(despu茅s):'+cantProducts);
                });

                tabla_temporal.innerHTML = '';

                if(!setmasivoimport) {
                    $('#btn_cerrar_modal').click();
                }

                set_masivo = false;
                setmasivoimport = false;

                tabla = tblCotizacionWindow.getData();
                tabla.forEach(function (row) {
                    row[0] = row[0] == null ? "" : row[0];
                    row[1] = row[1] == null ? "" : row[1];
                    row[2] = row[2] == null ? "" : row[2];
                    row[3] = row[3] == null ? "" : row[3];
                    row[4] = row[4] == null ? "" : row[4];
                    row[5] = row[5] == null ? "" : row[5];
                    row[6] = row[6] == null ? "" : row[6];
                    row[7] = row[7] == null ? "" : row[7];
                    row[8] = row[8] == null ? "" : row[8];
                    row[9] = row[9] == null ? "" : row[9];
                    row[10] = row[10] == null ? "" : row[10];
                    row[11] = row[11] == null ? "" : row[11];
                    row[12] = row[12] == null ? "" : row[12];
                    row[13] = row[13] == null ? "" : row[13];
                    row[14] = row[14] == null ? "" : row[14];
                });

                invocaValidaCotizacion(tabla, tblCotizacionWindow);
            }
        });

        var btnAlternativos = document.getElementById('btn-alter');
        Handsontable.dom.addEvent(btnAlternativos, 'click', function(){

            var codpro_select = tblCotizacion.getDataAtRow(last_cell_selection[0]['row'], 0)[0];

            if(prod_ref_alter!=codpro_select) {
                buscaProductosAlternativos(codpro_select, tblCotizacion);
            }
        });

        var btnCiclos = document.getElementById('btn-ciclo');
        Handsontable.dom.addEvent(btnCiclos, 'click', function(){
            if(first_prod==1){
                buscaProductosCiclos(tblCotizacion);
            }
        });

        tblCotizacion.addHook('afterChange', function(registroModificado, acciones) {
            if(acciones != 'loadData'){

                if(!set_masivo) {

                    registroModificado.forEach(function (elemento) {
                        var row = elemento[0];

                        if(elemento[3] != null && elemento[3] != "") {

                            var codpro = "";
                            var despro = "";
                            var marca = "";
                            var prepro = "";
                            var marpro = "";
                            var stopro = "";
                            var stoantofa = "";
                            var compro = 1;
                            var conpro = "";
                            var costo = "";
                            var convenio = "";
                            var transporte = "";
							var lista = 0;
                            if (firstcellchange == '') {
                                firstcellchange = registroModificado[0][1];
                            }

                            if (elemento[1] == 'Codigo' && firstcellchange == 'Codigo') {

                                var rutcli = rut_cliente;
                                codpro = elemento[3].toUpperCase();
                                var tipbus = 'CODPRO';
                                var parametros = {
                                    rutcli: rutcli,
                                    cod_producto: codpro,
                                    tipo_busqueda: tipbus,
                                    coddir: document.getElementsByName('select_direc')[0].value
                                };
                                ($.ajax({
                                    xhrFields: {
                                        withCredentials: true
                                    },
                                    headers: {
                                        'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                                    },
                                    data: parametros,
                                    url: '{{ env("URL_API") }}/api/productos/producto-especifico',
                                    type: 'POST',
                                    // cache: true,
                                    datatype: 'json',
                                    // async: true,
                                    beforeSend: function () {
                                        document.getElementById("pre_loader").className = "loader animation-start";
                                    },
                                    success: function (json) {

                                        if (json.code == 200) {

                                            codpro = json.response.codpro;
                                            despro = json.response.despro;
                                            marca = json.response.marca;
                                            prepro = json.response.precio;
                                            marpro = json.response.margen;
                                            stopro = json.response.stock;
                                            stoantofa = json.response.stock_ant;
                                            compro = json.response.categoria;
                                            conpro = json.response.condicion_de_venta;
                                            costo = json.response.costo;
                                            convenio = json.response.convenio;
                                            cliente_antofa = json.response.cliente_antofa;
                                            transporte = json.response.transporte;
											lista = json.response.lista;
                                            //console.log('modifica codigo, setmasivoimport: ' + setmasivoimport);

                                            tblCotizacion.setDataAtCell(row, 1, despro);
                                            tblCotizacion.setDataAtCell(row, 2, marca);
                                            tblCotizacion.setDataAtCell(row, 3, '$' + formatMoney(prepro, 0, ',', '.'));
                                            tblCotizacion.setDataAtCell(row, 4, '1');
                                            tblCotizacion.setDataAtCell(row, 5, '$' + formatMoney(prepro, 0, ',', '.'));
                                            tblCotizacion.setDataAtCell(row, 6, marpro + '%');
                                            tblCotizacion.setDataAtCell(row, 7, compro);
                                            tblCotizacion.setDataAtCell(row, 8, conpro);
                                            

                                            if (cliente_antofa == 1) {
                                                tblCotizacion.setCellMeta(row, tblCotizacion.propToCol('Stock'), 'readOnly', false);
                                                tblCotizacion.setDataAtCell(row, 9, formatMoney(stoantofa, 0, ',', '.'));
                                                //Stock
                                                var stock = [
                                                    {nombre: 'Anto', valor: formatMoney(stoantofa, 0, ',', '.')},
                                                    {nombre: 'Stgo', valor: formatMoney(stopro, 0, ',', '.')}
                                                ];
                                                tblCotizacion.setCellMeta(row, tblCotizacion.propToCol('Stock'), 'handsontable', {
                                                    data: stock, autoColumnSize: true,
                                                    getValue: function () {
                                                        var selection = this.getSelectedLast();
                                                        return this.getSourceDataAtRow(selection[0]).valor;
                                                    }
                                                });
                                                tblCotizacion.setDataAtCell(row, 11, "ANTO");
                                            } else {
                                                tblCotizacion.setDataAtCell(row, 9, formatMoney(stopro, 0, ',', '.'));
                                                tblCotizacion.setCellMeta(row, tblCotizacion.propToCol('Stock'), 'readOnly', true);
                                                tblCotizacion.setDataAtCell(row, 11, "STGO");
                                            }

                                            tblCotizacion.setDataAtCell(row, 10, costo);
                                            tblCotizacion.setDataAtCell(row, 12, convenio);
                                            tblCotizacion.setDataAtCell(row, 13, recom);
                                            tblCotizacion.setDataAtCell(row, 14, '$' + formatMoney(transporte, 0, ',', '.'));
											tblCotizacion.setDataAtCell(row, 15, lista);

                                            var productos = tblCotizacion.getData();
                                            invocaValidaCotizacion(productos, tblCotizacion);

                                        }
                                        else {
                                            tblCotizacion.setDataAtCell(row, 0, "");
                                            tblCotizacion.selectCell(row, 0);
                                            // tblCotizacion.alter('remove_row', row, 1);

                                            var nFrom = "top";
                                            var nAlign = "center";
                                            var nIcons = "fa fa-times";
                                            var nType = "danger";
                                            var nAnimIn = "animated bounceInRight";
                                            var nAnimOut = "animated bounceOutRight";
                                            var title = "";
                                            var message = json.response;
                                            // console.log(json);

                                            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                                        }
                                        firstcellchange = '';
                                        document.getElementById("pre_loader").className = "loader animation-start d-none";
                                    },
                                    error: function(e){
                                        var code = e.status;
                                        var text = e.statusText;
                                        registroError(ejecutivo, moduleid_global, 'buscar producto por codigo', code, text);
                                    }
                                }));

                                if (elemento[2] != elemento[3]) {
                                    //Avanza hacia la celda del precio
                                    tblCotizacion.selectCell(row, 3);

                                    //Obtiene y muestra precio sugerido si corresponde
                                    getPrecioSugerido(codpro);

                                    //Valida si el c贸digo ya ha sido ingresado antes en la cotizaci贸n
                                    validaCodDuplicado(codpro);

                                    //Busca productos alternativos
                                    var btnAlter = document.getElementById('btn-alter');
                                    if(btnAlter.className.includes('active')){
                                        buscaProductosAlternativos(codpro, tblCotizacion);
                                    }

                                }
                            }
                            else if (elemento[1] == 'Descripcion' && firstcellchange == 'Descripcion' && (registroModificado[0][3] != null && registroModificado[0][3] != '')) {

                                var celda_codigo = tblCotizacion.getDataAtRow(row);
                                if (celda_codigo[0] == "" || celda_codigo[0] == null) {
                                    tblCotizacion.setDataAtCell(row, 1, "");
                                }
                                tblCotizacion.selectCell(row, 3);

                                var fulltext;
                                var groupradio = document.getElementsByName('rb_criterio_busqueda');

                                for (var r in groupradio) {
                                    if (groupradio[r].checked == true) {
                                        fulltext = groupradio[r].value;
                                    }
                                }
                                findProductDesc(elemento[3], "despro", fulltext);
                                $('#tx_prod_desc').val(elemento[3]);
                                $('#btn_buscar_descripcion').click();
                                firstcellchange = '';
                            }
                            else if (elemento[2] != "" && (elemento[1] == 'Precio' || elemento[1] == 'Cantidad') && (firstcellchange == 'Precio' || firstcellchange == 'Cantidad')) {
                                //console.log(elemento);
                                var tabla = tblCotizacion.getData();

                                var celda_codigo = tblCotizacion.getDataAtRow(row);
                                if (celda_codigo[0] != "" && celda_codigo[0] != null) {

                                    if (elemento[1] == 'Precio') {
                                        //Avanza hacia la celda de la cantidad
                                        tblCotizacion.selectCell(row, 4);
                                    }
                                    else if (elemento[1] == 'Cantidad') {


                                        var count_vacias = 0;
                                        tabla.forEach(function (t) {
                                            if (t[0] == "" || t[0] == null) {
                                                count_vacias++;
                                            }
                                        });

                                        if (count_vacias == 0) {
                                            tblCotizacion.alter('insert_row', row + 1, 1);
                                            tabla = tblCotizacion.getData();

                                            tabla.forEach(function (row) {
                                                row[0] = row[0] == null ? "" : row[0];
                                                row[1] = row[1] == null ? "" : row[1];
                                                row[2] = row[2] == null ? "" : row[2];
                                                row[3] = row[3] == null ? "" : row[3];
                                                row[4] = row[4] == null ? "" : row[4];
                                                row[5] = row[5] == null ? "" : row[5];
                                                row[6] = row[6] == null ? "" : row[6];
                                                row[7] = row[7] == null ? "" : row[7];
                                                row[8] = row[8] == null ? "" : row[8];
                                                row[9] = row[9] == null ? "" : row[9];
                                                row[10] = row[10] == null ? "" : row[10];
                                                row[11] = row[11] == null ? "" : row[11];
                                                row[12] = row[12] == null ? "" : row[12];
                                                row[13] = row[13] == null ? "" : row[13];
                                                row[14] = row[14] == null ? "" : row[14];
                                            });
                                        }

                                        //Avanza hacia la fila siguiente
                                        tblCotizacion.selectCell((row + 1), 0);
                                    }


                                    var fila = tblCotizacion.getData()[elemento[0]];
                                    var precio = fila[3] != undefined ? fila[3] : 0;
                                    var precio_numeric = precio.replace('$', '').replace(',', '').replace('.', '').trim();
                                    var cantidad = fila[4] != undefined ? fila[4] : 0;
                                    var cantidad_numeric = cantidad != "" ? cantidad.replace(',', '').replace('.', '').trim() : 1;
                                    //console.log('precio: ' + precio_numeric + ' - cantidad: ' + cantidad_numeric);
                                    var total = precio_numeric * cantidad_numeric;
                                    var costo = fila[10];
                                    var marpro = precio_numeric > 0 ? (((precio_numeric - costo) / precio_numeric) * 100).toFixed(2).toString() + '%' : '0';

                                    tblCotizacion.setDataAtCell(row, 5, '$' + formatMoney(total, 0, ',', '.'));
                                    tblCotizacion.setDataAtCell(row, 6, marpro);
                                    firstcellchange = '';

                                    tabla = tblCotizacion.getData();

                                    //if(elemento[1] == 'Cantidad'){ //ver si esto era
										invocaValidaCotizacion(tabla, tblCotizacion);
									//}

                                    firstcellchange = '';
                                }

                            }
                            else if( elemento[1] == 'Stock' && firstcellchange == 'Stock'){
                                var celda_codigo = tblCotizacion.getDataAtRow(row);
                                if (celda_codigo[0] != "" && celda_codigo[0] != null) {

                                    var stock_options = tblCotizacion.getCellMeta(row, 9).handsontable.data;
                                    var stock_valor = elemento[3];
                                    var sucursal="STGO";

                                    stock_options.forEach(function(s){
                                        // console.log(s);
                                        if(stock_valor == s.valor){
                                            sucursal = s.nombre.toUpperCase();
                                        }
                                    });

                                    if(bolsa_cotizacion.length>0){
                                        bolsa_cotizacion.splice(0, bolsa_cotizacion.length);
                                    }

                                    tblCotizacion.setDataAtCell(row, 11, sucursal);

                                    // var check_sucursal = tblCotizacion.getCell(row, 9);
                                    // if(sucursal == "ANTO") {
                                    //     check_sucursal.className = 'antofa';
                                    // }

                                    //Actualiza bolsa de productos
                                    var productos = tblCotizacion.getData();
                                    if(bolsa_cotizacion.length>0){
                                        bolsa_cotizacion.splice(0, bolsa_cotizacion.length);
                                    }
                                    productos.forEach(function(pro){
                                        //console.log(pro);
                                        var codpro = pro[0];
                                        if(codpro!="" && codpro!=null) {
                                            var precio = parseInt(pro[3].replace('$', '').replace('.', ''));
                                            var costo = parseInt(pro[10]);
                                            var cantidad = parseInt(pro[4]);
                                            var suc = pro[11];
                                            var rec = pro[13];

                                            bolsa_cotizacion.push({codpro, precio, costo, cantidad, suc, rec});
                                        }
                                    });
                                    tblCotizacion.selectCell(row+1, 0);
                                    firstcellchange = '';

                                }
                            }
                        }

                    });
                }
                bolsa_cotizacion_completa = tblCotizacion.getData();
				setBolsa(bolsa_cotizacion_completa);
            }

        });

        tblCotizacion.addHook('afterRemoveRow', function(){
            bolsa_cotizacion_completa = tblCotizacion.getData();
			setBolsa(bolsa_cotizacion_completa);
        });

        hiddenColumnsPlugin.hideColumns([10]);
        tblCotizacion.render();

        if(data!=""){
            var tabla = tblCotizacion.getData();
            invocaValidaCotizacion(tabla, tblCotizacion);
        }
    }
    function findProductInput(){
        var valorBuscado = document.getElementById('tx_prod_desc').value;
        var rutcli = rut_cliente;

        if (rutcli != "") {

            if (valorBuscado != null && valorBuscado != "") {

                var fulltext;
                var groupradio = document.getElementsByName('rb_criterio_busqueda');

                for (var r in groupradio) {
                    if (groupradio[r].checked == true) {
                        fulltext = groupradio[r].value;
                    }
                }
                //console.log(fulltext);

                findProductDesc(valorBuscado, "despro", fulltext);

                //document.getElementById('valorBuscadoDescripcion').text = valorBuscado;
                //document.getElementById('tx_prod_desc').value = valorBuscado;
            }
        }
    }
    function findProductDesc(descripcion, tipbus, fulltext) {
        var rutcli = rut_cliente;

        var parametros = {
            search_string: descripcion,
            type_search: tipbus,
            fulltext: fulltext,
            rutcli: rutcli
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/productos/listar-productos',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json) {
                var dataSet = [];

                var check = false;
                var codpro = "";
                var despro = "";
                var marca = '';
                var prepro = '';
                var preprosf = '';
                var marpro = '';
                var stopro = '';
                var stoantofa = '';
                var compro = '';
                var conpro = '';
                var costo = '';
                var convenio = '';
                var estadoprod='';
                var en_ciclo_hasta='';
                var estado_ciclo='';

                if(json.response.length > 0) {
                    console.log(json.response);

                    for(var i in json.response)
                    {
                        // codpro
                        // despro
                        // precio
                        // costo
                        // costocomercial
                        // stock
                        // stock_ant
                        // contribucion
                        // margen
                        // condicion_de_venta
                        // sobreventa
                        // categoria
                        // marca
                        // linea

                        codpro = json.response[i].codpro;
                        despro = json.response[i].despro;
                        marca = json.response[i].marca;
						preprosf = parseInt(json.response[i].precio);
                        prepro = '$'+formatMoney(json.response[i].precio, 0, ',', '.');
                        // stopro = formatMoney((json.response[i].stock ==null?0:json.response[i].stock), 0, ',', '.');
                        stopro = json.response[i].stock ==null?0:json.response[i].stock;
                        // stoantofa = formatMoney((json.response[i].stock_ant ==null?0:json.response[i].stock_ant), 0, ',', '.');
                        stoantofa = json.response[i].stock_ant ==null?0:json.response[i].stock_ant;
                        compro = json.response[i].categoria;
                        conpro = json.response[i].condicion_de_venta;
                        costo = json.response[i].costo;
                        convenio = json.response[i].convenio;
                        estadoprod = json.response[i].estado_producto;
                        en_ciclo_hasta = json.response[i].en_ciclo_hasta!=null?moment(json.response[i].en_ciclo_hasta).format('L'):"-";
                        estado_ciclo = json.response[i].estado_ciclo;

                        // marpro = preprosf>0?(((preprosf-costo)/preprosf)*100).toFixed(2).toString()+'%':'0';
                        // marpro =marpro.replace('.',',');

                        marpro = json.response[i].margen + '%';

                        // dataSet.push([check, codpro, despro, marca, prepro, compro, conpro, marpro, stopro, costo, stoantofa, convenio]);
                        dataSet.push([check, codpro, despro, marca, preprosf, compro, conpro, marpro, stopro, costo, stoantofa, convenio, estadoprod, estado_ciclo, en_ciclo_hasta, json.response[i].lista]);
                    }

                    if(dataSet.length > 0){
                        document.getElementById('cant_registros').innerHTML = formatMoney(dataSet.length, 0, ',', '.');

                        var currencyCodes = ['A', 'B', 'C'];
                        var flagRenderer = function (instance, td, row, col, prop, value, cellProperties) {

                            var currencyCode = value;
                            while (td.firstChild) {
                                td.removeChild(td.firstChild);
                            }
                            if (currencyCodes.indexOf(currencyCode) > -1) {
                                var flagElement = document.createElement('DIV');
                                flagElement.className = 'flag cat-' + currencyCode.toLowerCase();
                                td.appendChild(flagElement);
                            } else {
                                var textNode = document.createTextNode(value === null ? '' : value);

                                td.appendChild(textNode);
                            }
                        };

                        var convenioCodes = ['S', 'N'];
                        var convenioRenderer = function (instance, td, row, col, prop, value, cellProperties) {

                            var convenioCode = value;
                            while (td.firstChild) {
                                td.removeChild(td.firstChild);
                            }
                            if (convenioCodes.indexOf(convenioCode) > -1) {
                                var flagElement = document.createElement('DIV');
                                flagElement.className = 'conv ' + convenioCode.toLowerCase();
                                td.appendChild(flagElement);
                            } else {
                                var textNode = document.createTextNode(value === null ? '' : value);

                                td.appendChild(textNode);
                            }
                        };

                        var stockCodes = ['ANTO', 'STGO'];
                        var stockRenderer = function (instance, td, row, col, prop, value, cellProperties) {

                            var stockRenderer = value;

                            while (td.firstChild) {
                                td.removeChild(td.firstChild);
                            }
                            if (stockCodes.indexOf(stockRenderer) > -1) {
                                //console.log(stockRenderer);
                                var flagElement = document.createElement('DIV');
                                flagElement.className = 'back-' + stockRenderer.toLowerCase();
                                td.appendChild(flagElement);
                            } else {
                                var textNode = document.createTextNode(value === null ? '' : value);

                                td.appendChild(textNode);
                            }
                        };

                        var cicloRenderer = function(instance, td, row, col, prop, value, cellProperties){
                            var cicloRenderer = value;

                            while (td.firstChild) {
                                td.removeChild(td.firstChild);
                            }
                            if(cicloRenderer=="1") {
                                var cicloElement = document.createElement('DIV');
                                cicloElement.className = 'ciclo yes';
                                td.appendChild(cicloElement);
                            }
                            else{
                                var textNode = document.createTextNode(value === "0" ? '-' : value);
                                td.appendChild(textNode);
                            }
                        };
						var precioRenderer = function(instance, td, row, col, prop, value, cellProperties){
							var precioCaptura = instance.getDataAtCell(row, 15);
							if(precioCaptura == 910){
								td.style.animation = "glowpiola 1s infinite alternate";
                                td.style.background = '#c3e6cb';
							}
							Handsontable.renderers.NumericRenderer.apply(this, arguments);
                        };
						var columnasOcultas = [9];
						if($("#sucursal-cliente").val() == 0){
							columnasOcultas[1] = 10;
						}
						if(!tieneDerecho(40001)){ //si no tiene derecho, se oculta la columna margen
							columnasOcultas[2] = 7;
						}
						columnasOcultas[3] = 15;

                        var configuraciones = {
                            data: dataSet,
                            columns: [
                                { type: "checkbox", width:20,className: "htCenter" },
                                { type: "text", width:50,className: "htCenter" , readOnly: true },
                                { type: "text", width:330, readOnly: true },
                                { type: "text", width:90, readOnly: true, className:"f-11" },
                                { type: "numeric", renderer: precioRenderer, width:60,className: "htCenter " , readOnly: true,
                                    numericFormat: {
                                        pattern: '$0,000',
                                        culture: 'es-CL'
                                    }
                                },
                                { type: "text", renderer: flagRenderer, readOnly: true, width:30,className: "htCenter" },
                                { type: "text", readOnly: true, width:70,className: "htCenter f-10" },
                                { type: "text", readOnly: true, width:50,className: "htCenter" },
                                { type: "numeric", width:40, className: "htCenter", readOnly: true,
                                    numericFormat: {
                                        pattern: '0,000',
                                        culture: 'es-CL'
                                    }
                                },
                                { type: "text", readOnly: true },
                                { type: "text", width: 40, readOnly: true },
                                { type: "text", renderer: convenioRenderer, readOnly: true, width: 25 },
                                { type: "text", width: 75, readOnly: true, className: "f-10" },
                                { type: "text", readOnly: true, renderer: cicloRenderer, width: 40, className: "htCenter" },
                                { type: "text", readOnly: true, width: 50, className: "htCenter f-10" },
								{ type: "text", readOnly: true, width: 50, className: "htCenter f-10" }
                            ],
                            hiddenColumns: {
                                columns: columnasOcultas,
                                indicators: true
                            },
                            //overflowX: "none",
                            stretchH: 'all',
                            width: '100%',
                            autoWrapRow: false,
                            height: 300,
                            //maxRows: 5000,
                            rowHeaders: false,
                            licenseKey: 'non-commercial-and-evaluation',
                            colHeaders: [
                                '<i class="fa fa-check-square-o" title="Seleccionar"></i>',
                                'C贸digo',
                                'Descripci贸n',
                                'Marca',
                                'Precio',
                                'C',
                                'S/P',
                                'Margen',
                                'Stock',
                                'Costo',
                                'StockAntofa',
                                'Conv',
                                'Estado prod',
                                'En ciclo',
                                'Hasta'
                            ],
                            cells: function (row, col, prop) {
                                if(this.instance.getData().length != 0){
                                    var cellProperties = {};

                                    if(col==8){
                                        if (this.instance.getData()[row][col] == 0) {
                                            cellProperties.renderer = stockValueRenderer;
                                        }
                                    }

                                    return cellProperties;
                                }
                            },
                            columnSorting: {
                                indicator: true
                            },
                            language: 'es-MX'
                            // cells: function (row, col, prop) {
                            //     if(this.instance.getData().length != 0){
                            //         var cellProperties = {};
                            //         // if (this.instance.getData()[row][11] == "S") {
                            //         //     cellProperties.className = 'conveni';
                            //         // }
                            //         // else{
                            //         //     cellProperties.className = 'no_conveni';
                            //         // }
                            //         // if(col==5) {
                            //         //     if (this.instance.getData()[row][col] == "A") {
                            //         //         cellProperties.className = 'com_a';
                            //         //     } else if (this.instance.getData()[row][col] == "B") {
                            //         //         cellProperties.className = 'com_b';
                            //         //     } else if (this.instance.getData()[row][col] == "C") {
                            //         //         cellProperties.className = 'com_c';
                            //         //     } else{
                            //         //         cellProperties.className = 'sin_com';
                            //         //     }
                            //         // }
                            //         return cellProperties;
                            //     }
                            // },
                        };

                        function stockValueRenderer(instance, td, row, col, prop, value, cellProperties) {
                            Handsontable.renderers.TextRenderer.apply(this, arguments);

                            if (value === 0) {
                                td.style.background = '#EBEBEB';
                            }
                        }

                        var elemento = document.getElementById('tbl_buscar_descripcion');
                        var tblBuscarDescripcion = new Handsontable(elemento, configuraciones);

                        tblBuscarDescripcion.addHook('afterChange', function(changes, acciones) {
                            if(acciones != 'loadData'){

                                if(changes!=undefined) {
                                    var tabla_temporal = document.getElementById('tbl_temporal');
                                    var rows_tbl = tabla_temporal.getElementsByTagName('tr');

                                    var fila = tblBuscarDescripcion.getDataAtRow(changes[0][0]);
                                    var existe = false;
                                    var contar = 0;

                                    /* Recorrer table temporal */
                                    if (rows_tbl.length > 0) {
                                        rows_tbl.forEach(function (r) {

                                            var td_tbl = r.getElementsByTagName('td');

                                            if (fila[1] == td_tbl[0].innerHTML) {
                                                existe = true;
                                                if(!fila[0]){
                                                    tabla_temporal.deleteRow(contar);
                                                }
                                            }
                                            contar++;
                                        });
                                    }
                                    if (!existe) {

                                        var NewRow = tabla_temporal.insertRow(contar);
                                        var Newcell1 = NewRow.insertCell(0);
                                        var Newcell2 = NewRow.insertCell(1);
                                        var Newcell3 = NewRow.insertCell(2);
                                        var Newcell4 = NewRow.insertCell(3);
                                        var Newcell5 = NewRow.insertCell(4);
                                        var Newcell6 = NewRow.insertCell(5);
                                        var Newcell7 = NewRow.insertCell(6);
                                        var Newcell8 = NewRow.insertCell(7);
                                        var Newcell9 = NewRow.insertCell(8);
                                        var Newcell10 = NewRow.insertCell(9);
                                        var Newcell11 = NewRow.insertCell(10);
                                        var Newcell12 = NewRow.insertCell(11);
										var Newcell13 = NewRow.insertCell(12);


                                        Newcell1.innerHTML = fila[1];
                                        Newcell2.innerHTML = fila[2];
                                        Newcell3.innerHTML = fila[3];
                                        Newcell4.innerHTML = fila[4];
                                        Newcell5.innerHTML = fila[5];
                                        Newcell6.innerHTML = fila[6];
                                        Newcell7.innerHTML = fila[7];
                                        Newcell8.innerHTML = fila[8];
                                        Newcell9.innerHTML = fila[9];
                                        Newcell10.innerHTML = fila[10];
                                        Newcell11.innerHTML = fila[11];
                                        Newcell12.innerHTML = '$0';
										Newcell13.innerHTML = fila[15];
                                    }
                                }
                            }
                        });

                        var count_click=0;
                        var fila_anterior=-1;
                        tblBuscarDescripcion.addHook('afterSelection', function(fila, columna){

                            if(fila==fila_anterior){
                                count_click++;
                            }
                            fila_anterior=fila;

                            if(columna==2 && count_click==1){
                                count_click=0;

                                //Captura evento doble click y presiona boton agregar a cotizacion
                                tblBuscarDescripcion.setDataAtCell(fila, 0, true);
                                var btnAgregarCotizacion = document.getElementById('btn_agregar_coti');
                                btnAgregarCotizacion.click();

                            }

                        });

                        //Destruye la instancia de handsontable para la busqueda de productos por descripci贸n
                        var btnAgregarCotizacion = document.getElementById('btn_agregar_coti');
                        var btnCloseBusquedaDescripcion = document.getElementById('btn_cerrar_modal');
                        var rbCriterioBusqueda1 = document.getElementsByName('rb_criterio_busqueda')[0];
                        var rbCriterioBusqueda2 = document.getElementsByName('rb_criterio_busqueda')[1];
                        var txDescBuscada = document.getElementById('tx_prod_desc');

                        Handsontable.dom.addEvent(btnAgregarCotizacion, 'click', function () {
                            if(!tblBuscarDescripcion.isDestroyed) {
                                tblBuscarDescripcion.destroy();
                            }
                        });
                        Handsontable.dom.addEvent(btnCloseBusquedaDescripcion, 'click', function () {
                            if(!tblBuscarDescripcion.isDestroyed) {
                                tblBuscarDescripcion.destroy();
                            }
                        });
                        Handsontable.dom.addEvent(rbCriterioBusqueda1, 'change', function () {
                            if(!tblBuscarDescripcion.isDestroyed) {
                                tblBuscarDescripcion.destroy();
                            }
                        });
                        Handsontable.dom.addEvent(rbCriterioBusqueda2, 'change', function () {
                            if(!tblBuscarDescripcion.isDestroyed) {
                                tblBuscarDescripcion.destroy();
                            }
                        });
                        Handsontable.dom.addEvent(txDescBuscada, 'click', function () {
                            if(!tblBuscarDescripcion.isDestroyed) {
                                tblBuscarDescripcion.destroy();
                            }
                        });

                        tblBuscarDescripcion.render();
                        document.getElementById("pre_loader").className = "loader animation-start d-none";
                    }
                }
                else{

                    document.getElementById('cant_registros').innerHTML = '0';
                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = 'No se han encontrado registros seg煤n el criterio de b煤squeda';

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar producto por descripcion', code, text);
            }
        });
    }
    function actualizaBolsaCotizacion(productos, tbl){
        /* Limpiar productos */
        var productos_ = [];

        if(bolsa_cotizacion.length>0){
            bolsa_cotizacion.splice(0, bolsa_cotizacion.length);
        }

        productos.forEach(function(pro){
            var codpro = pro[0];
            if(codpro!="" && codpro!=null) {
                var precio = parseInt(pro[3].replace('$', '').replace('.', ''));
                var costo = parseInt(pro[10]);
                var cantidad = parseInt(pro[4]);
                var suc = pro[11];
                var rec = pro[13];

                productos_.push({codpro, precio, costo, cantidad});
                bolsa_cotizacion.push({codpro, precio, costo, cantidad, suc, rec});
            }
        });
    }
    function invocaValidaCotizacion(productos, tbl){
        /* Limpiar productos */
        var productos_ = [];

        if(bolsa_cotizacion.length>0){
            bolsa_cotizacion.splice(0, bolsa_cotizacion.length);
        }

        productos.forEach(function(pro){
            var codpro = pro[0];
            if(codpro!="" && codpro!=null) {
                var precio = parseInt(pro[3].replace('$', '').replace('.', ''));
                var costo = parseInt(pro[10]);
                var cantidad = parseInt(pro[4].replace('$', '').replace('.', ''));
                var suc = pro[11];
                var rec = pro[13];

                productos_.push({codpro, precio, costo, cantidad});
                bolsa_cotizacion.push({codpro, precio, costo, cantidad, suc, rec});
            }
        });

        if(tbl.getData().length>0) {
            bolsa_cotizacion_completa = tbl.getData();
			setBolsa(bolsa_cotizacion_completa);
        }

        var rutcli = rut_cliente;

        validaCotizacion(rutcli, false, 3, ejecutivo, productos_, tbl);

        //Oculta buscador de numcot, si ya se ha empezado a generar coti desde cero
		var numcot_buscado = 0;
		if(document.getElementById('find_numcot') != null){ //si es edici髇 o generaci髇 de coti/nvt son distintos
        	numcot_buscado = document.getElementById('find_numcot').value;
		}
		else{
			numcot_buscado = from_coti;
		}
        if(bolsa_cotizacion.length>0){
            if(first_prod==0){
                first_prod=1;
            }
			if(document.getElementById('dv_buscar_numcot') != null){
				document.getElementById('dv_buscar_numcot').className = 'float-right m-r-350 m-t-5-neg width-350-p d-none';
			}
        }
        else{
            first_prod=0;
			if(document.getElementById('dv_buscar_numcot') != null){
				document.getElementById('dv_buscar_numcot').className = 'float-right m-r-350 m-t-5-neg width-350-p';
			}
        }
    }
    function validaCotizacion(rutcli, update, codemp, ejecutivo, productos, tbl){
        var coddir=document.getElementsByName('select_direc')[0].value;
		getRecomendados(rutcli, productos, tbl);
        var parametros = {
            rutcli: rutcli,
            update: update,
            codemp: codemp,
            coddir: coddir,
            vendedor: ejecutivo,
            productos: productos
        };
		if(from_coti != "" && numordWindow == 0 && from_otra_coti == 0){ //si estamos editando una cotizacion, y no es una nota web, ingresamos este parametro extra
			parametros.numcot = from_coti;
		}
        else{
            if(from_nota != ""){ //si estamos editando una nota de venta
                parametros.numnvt = from_nota;
            }
        }
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/validar-cotizacion',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code == 200 || yaValidada || (json.code == 202 && numordWindow == 0) ) {

                    //Valida precio m铆nimo
                    var pro = json.response.productos;

                    var tblCotizacion = tbl;

                    //console.log(json.response);

                    //Recorre productos y actualiza precios en la tabla cotizacion
                    pro.forEach(function(p){

                        if(p.flagType==1) //el controlador nos avisa si un producto viene con alguna pifia :D
                        {
                            var contar=0;
                            tblCotizacion.getData().forEach(function (row) {
                                var codpro_row = row[0];
                                if(codpro_row==p.codpro)
                                {
									set_masivo = true;
									setmasivoimportmsg = false;
                                    tblCotizacion.setDataAtCell(contar, 3, formatMoney(p.precio_minimo, 0, ',', '.'));
									//aca tenemos que repasar bolsa_cotizacion
									for(var z = 0; z < bolsa_cotizacion.length; z++){ //para cada elemento de la bolsa
										if(bolsa_cotizacion[z].codpro == p.codpro) //si el elemento corresponde al codigo analizado
										bolsa_cotizacion[z].precio = p.precio_minimo; //cambiamos el precio al precio minimo
									}
									var cantidad = row[4] != undefined ? row[4] : 0;
									var cantidad_numeric = cantidad != "" ? cantidad.replace(',', '').replace('.', '').trim() : 1;

									var totalNeto = p.precio_minimo * cantidad_numeric;
									tblCotizacion.setDataAtCell(contar, 5, '$' + formatMoney(totalNeto, 0, ',', '.'));
									//console.log(JSON.stringify(row));
									var costo = row[10];
									var mg = ((p.precio_minimo - costo)/p.precio_minimo) * 100;
									
									tblCotizacion.setDataAtCell(contar, 6, formatMoney(mg, 2, '.', '.') + "%");

									tblCotizacion.setDataAtCell(contar, 14, '$' + formatMoney(p.transporte, 0, ',', '.'));
									set_masivo = false;
                                    if(!setmasivoimportmsg) {
										tblCotizacion.selectCell((contar), 3);
                                        var nFrom = "top";
                                        var nAlign = "center";
                                        var nIcons = "fa fa-times";
                                        var nType = "danger";
                                        var nAnimIn = "animated bounceInRight";
                                        var nAnimOut = "animated bounceOutRight";
                                        var title = "";
                                        var message = p.msg;

                                        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
										setmasivoimportmsg = true;
                                    }
                                }

                                contar++;
                            });
                        }else{
							var contar=0;
							set_masivo = true;
                            tblCotizacion.getData().forEach(function (row) {
                                var codpro_row = row[0];
                                if(codpro_row==p.codpro)
                                {
									tblCotizacion.setDataAtCell(contar, 14, '$' + formatMoney(p.transporte, 0, ',', '.'));
                                }
                                contar++;
                            });
							set_masivo = false;
						}

                    });

                    // var contador=0;
                    // tblCotizacion.getData().forEach(function (row) {
                    //     //console.log(row);
                    //
                    //     var fila = tblCotizacion.getData()[contador];
                    //     //console.log(fila);
                    //     if (fila[0] != "") {
                    //
                    //         var precio = fila[3] != undefined ? fila[3] : 0;
                    //         var precio_numeric = precio.replace('$', '').replace(',', '').replace('.', '').trim();
                    //         var cantidad = fila[4] != undefined ? fila[4] : 0;
                    //         var cantidad_numeric = cantidad != "" ? cantidad.replace(',', '').replace('.', '').trim() : 1;
                    //         console.log('precio: ' + precio_numeric + ' - cantidad: ' + cantidad_numeric);
                    //         var total = precio_numeric * cantidad_numeric;
                    //         var costo = fila[10];
                    //         var marpro = precio_numeric > 0 ? (((precio_numeric - costo) / precio_numeric) * 100).toFixed(2).toString() + '%' : '0';
                    //
                    //         tblCotizacion.setDataAtCell(contador, 5, '$' + formatMoney(total, 0, ',', '.'));
                    //         tblCotizacion.setDataAtCell(contador, 6, marpro);
                    //
                    //         //tblCotizacion.selectCell((contador + 1), 0);
                    //     }
                    //     contador++;
                    // });

                    var subtotal = json.response.subtotal;
                    var cobrologistico = json.response.cobrologistico;
                    var iva = (subtotal * 19) / 100;
                    var total = Math.round(subtotal + iva);
                    //total = Math.round(total + cobrologistico);
                    var margen = json.response.margen != 0 ? json.response.margen.replace('.', ',') : 0;

                    subtotal = formatMoney(subtotal, 0, ',', '.');
                    iva = formatMoney(iva, 0, ',', '.');
                    total = formatMoney(total, 0, ',', '.');

                    var resumen_totales = [ subtotal, iva, total, margen, json.response.totalCostoTransporte ];

                    cobrologistico = formatMoney(cobrologistico, 0, ',', '.');

                    //Busca valor cobro logistico
                    //buscaCobroLogistico(subtotal, resumen_totales);

                    makeInfoTotalesCotizacion(resumen_totales, cobrologistico);
                }
                else{
					if(numordWindow!=0){ //si es orden web y no se ha modificado
						yaValidada = true;
						var pro = json.response.productos;
						var subtotal = 0;
						var canpro = 1;
						var tablazo = tbl.getData();
						console.log(JSON.stringify(tablazo));
						pro.forEach(function(p){ //totalizo desde lo cotizado
							tablazo.forEach(function (row) {
                                var codpro_row = row[0];
								if(codpro_row == p.codpro){
									canpro = row[4];
								}
							});
							if(p.codpro!='WW00008'){
								subtotal = subtotal + parseInt(p.precio_cotizado * parseInt(canpro));
							}
						});
						subtotal = subtotal + parseInt(cobroLogisticoWeb);
                    	var iva = (subtotal * 19) / 100;
                    	var total = Math.round(subtotal * 1.19);
						
                    	subtotal = formatMoney(subtotal, 0, ',', '.');
                    	iva = formatMoney(iva, 0, ',', '.');
                    	total = formatMoney(total, 0, ',', '.');

                    	var resumen_totales = [ subtotal, iva, total, mgWeb, json.response.totalCostoTransporte ];
                    	cobrologistico = formatMoney(cobrologistico, 0, ',', '.');
                    	makeInfoTotalesCotizacion(resumen_totales, cobroLogisticoWeb);
					}
                    console.log(json);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'validar cotizacion', code, text);
            }
        });
    }
    function getPrecioSugerido(codpro, getagain=false){

        var sugiere_precio=false;
        var precio_sugerido=0;

        var parametros={
            cod_emp: 3,
            rutcli: rut_cliente,
            cod_producto: codpro
        };
        //console.log(parametros);
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/productos/listar-precio-recomendado',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                //console.log(json);
                if(json.code==200){
                    //console.log(json);
                    if(json.response!=undefined&&json.response!=null){
                        if(json.response.estimacion == 'true'){
                            sugiere_precio=true;
                            precio_sugerido=formatMoney(json.response.precio, 0, ',', '.');

                            //Muestra precio sugerido
                            show_stack_precio_sugerido('default', precio_sugerido, codpro);
                        }
                        else{
                            if(getagain) {

                                var nFrom = "top";
                                var nAlign = "center";
                                var nIcons = "fa fa-warning";
                                var nType = "warning";
                                var nAnimIn = "animated bounceInRight";
                                var nAnimOut = "animated bounceOutRight";
                                var title = "";
                                var message = "No existe precio sugerido o no aplica";
                                // console.log(json);

                                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                            }
                        }
                    }
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'lista precio recomendado', code, text);
            }
        });

    }
    var stack_custom_left = {"dir1": "right", "dir2": "down"};
    function show_stack_precio_sugerido(type, precio, codpro) {
        var opts = {
            stack: stack_custom_left,
            text: 'Producto: ' + codpro + ' | <b>Precio sugerido: $' + precio + '</b>',
            hide: true,
            icon:'none',
            addclass: "stack-precio-sug bg-info",
            buttons: {
                closer: false,
                sticker: false
            },
            type: 'info'
        };
        switch (type) {
            case 'error':
                opts.text = "Watch out for that water tower!";
                opts.addclass = "stack-bottom-left bg-danger";
                opts.type = "error";
                break;

            case 'info':
                opts.text = "Have you met Ted?";
                opts.addclass = "stack-bottom-left bg-info";
                opts.type = "info";
                break;

            case 'success':
                opts.text = "I've invented a device that bites shiny metal asses.";
                opts.addclass = "stack-bottom-left bg-success";
                opts.type = "success";
                break;
        }
        var notice = new PNotify(opts);
        notice.get().click(function() {
            notice.remove();
        });
    }
    function validaCodDuplicado(codpro){
        var count = 0;
        bolsa_cotizacion.forEach(function(pro){
           // console.log(pro.codpro);

           if(pro.codpro == codpro){
               count++;
               if(count==1){
                   var msg = 'Producto ya ha sido agregado a la cotizaci贸n';
                   var nFrom = "top";
                   var nAlign = "center";
                   var nIcons = "fa fa-warning";
                   var nType = "warning";
                   var nAnimIn = "animated bounceInRight";
                   var nAnimOut = "animated bounceOutRight";
                   var title = "";
                   var message = msg;

                   notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
               }
           }
        });
    }

    /* Productos secci贸n inferior cotizaci贸n (Recomendados, Alternativos y Ciclos) */
    function recomendados(productos){

        if(count_carrusel>0){
            $('.autoplay').slick('unslick');
        }

        var items = '';

        //console.log(productos);

        productos.forEach(function(pro){
            var despro = pro.despro.replace(pro.marca, '');

            var com_class = '';
            switch (pro.categoria) {
                case "A":
                    com_class='com-a';
                    break
                case "B":
                    com_class='com-b';
                    break
                case "C":
                    com_class='com-c';
                    break
            }

           items = items + '<div class="bg-inverse m-r-10" style="position:relative;">\n' +
               '                                    <div id="pro_recom_' + pro.codpro + '" name="pro_recom" class="m-l-10 m-t-5">\n' +
               '                                        <div class="f-right m-t-10-neg m-r-0" style="top:0;">\n' +
               '                                            <i class="fa fa-circle '+ com_class +' f-18"></i>' +
               '                                            <button id="btn_' + pro.codpro + '" name="btn_recomendador" class="btn btn-primary btn-icon" title="Agregar a cotizaci贸n"><i class="fa fa-plus m-l-5"></i></button>\n' +
               '                                        </div>\n' +
               '                                        <div id="codpro_' + pro.codpro + '">' + pro.codpro + '</div>\n' +
               '                                        <div id="marca_' + pro.codpro + '" class="f-10">' + pro.marca + '</div>\n' +
               '                                        <div id="despro_' + pro.codpro + '" class="f-12">' + despro + '</div>\n' +
               '                                        <div id="precio_' + pro.codpro + '">Precio: $' + formatMoney(pro.precio, 0, ',', '.') + '</div>\n' +
               (tieneDerecho(40001)?('                                        <div id="margen_' + pro.codpro + '">Margen: ' + pro.margen + '%</div>\n'):'') +
               '                                        <div id="stock_' + pro.codpro + '">Stock: ' + formatMoney(pro.stock, 0, ',', '.') + '</div>\n' +
               '                                    </div>\n' +
               '                                    <div class="f-right m-r-0" style="bottom:0;position:absolute;right: 0">' +
               '                                         <span class="mytooltip tooltip-effect-1">\n' +
               '                                              <span class="tooltip-item"><i class="fa fa-camera"></i></span>\n' +
               '                                              <span class="tooltip-content clearfix" style="width: 60px;!important;margin-bottom:-5px;!important;margin-left:-120px;">\n' +
               '                                                   <img src="' + getUrlFotoProd(pro.codpro) + '" width="120px">\n' +
               '                                              </span>\n' +
               '                                         </span>' +
               '                                    </div>' +
               '                                </div>\n';
        });

        document.getElementById('carrusel_recomendador').innerHTML = items;

        $('.autoplay').slick({
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });

        //Recorre elementos recomendador
        var items_slick = document.getElementsByName('pro_recom');
        count_carrusel = 0;

        items_slick.forEach(function(i){
            if(i.id != ""){
                count_carrusel++;
            }
        });

        document.getElementById('btn_autoplay').title = 'Detener';
        document.getElementById('btn_autoplay').innerHTML = '<i id="icon_autoplay" class="fa fa-pause"></i>';
        // document.getElementById('dv_recomendados').className = "row m-t-40-neg m-b-30-neg";
        document.getElementById('dv_tabs').className = "col-xl-12";
        //document.getElementById('dv_recomendados').className = "row m-t-20-neg m-b-30-neg";

        // document.getElementsByClassName('slick-prev slick-arrow')[0].style = "background-color:#2b3d51;border:1px solid #2b3d51;border-radius:50% 50% 50% 50%;";
        // document.getElementsByClassName('slick-next slick-arrow')[0].style = "background-color:#2b3d51;border:1px solid #2b3d51;border-radius:50% 50% 50% 50%;";
    }
    function buscaProductosAlternativos(codpro, tblCotizacion){
        var parametros={
            "rutcli": rut_cliente,
            "codpro": codpro
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/productos/get-alternativos',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){
                    var productos = json.response;
                    console.log(productos);
                    if(productos.length>0){

                        prod_ref_alter = codpro;
                        alternativos(productos);
                        agregaEventoClick('btn_alternativo', 'alter', tblCotizacion, codpro);

                    }
                    else{
                        document.getElementById('carrusel_alternativos').innerHTML = '<p>No hay productos alternativos</p>';
                    }
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca productos alternativos', code, text);
            }
        });
    }
    function alternativos(productos){

        if(count_carrusel_alter>0){
            $('.autoplay_alter').slick('unslick');
        }

        var items = '';

        //console.log(productos);

        productos.forEach(function(pro){
            var despro = pro.despro.replace(pro.marca, '');
            var margen=pro.precio>0?(((pro.precio-pro.costo)/ pro.precio)*100).toFixed(2).toString():'0';//var margen = precio > 0 ? (((precio - costo) / precio) * 100).toFixed(2).toString() + '%' : '0';

            var com_class = '';
            switch (pro.categoria) {
                case "A":
                    com_class='com-a';
                    break
                case "B":
                    com_class='com-b';
                    break
                case "C":
                    com_class='com-c';
                    break
            }

            items = items + '<div class="bg-default2 m-r-10" style="position:relative;">\n' +
                '                                    <div id="pro_alter_' + pro.codpro + '" name="pro_alter" class="m-l-10 m-t-5">\n' +
                '                                        <div class="f-right m-t-10-neg m-r-0" style="top:0;">\n' +
                '                                            <i class="fa fa-circle '+ com_class +' f-18"></i>' +
                '                                            <button id="btn_alter_' + pro.codpro + '" name="btn_alternativo" class="btn btn-primary btn-icon" title="Reemplazar ' + prod_ref_alter + '"><i class="ion-arrow-swap m-l-5"></i></button>\n' +
                '                                        </div>\n' +
                '                                        <div id="codpro_' + pro.codpro + '">' + pro.codpro + '</div>\n' +
                '                                        <div id="marca_' + pro.codpro + '" class="f-10">' + pro.marca + '</div>\n' +
                '                                        <div id="despro_' + pro.codpro + '" class="f-12">' + despro + '</div>\n' +
                '                                        <div id="precio_' + pro.codpro + '">Precio: $' + formatMoney(pro.precio, 0, ',', '.') + '</div>\n' +
                (tieneDerecho(40001)?('                                        <div id="margen_' + pro.codpro + '">Margen: ' + margen + '%</div>\n'):'') +
                '                                        <div id="stock_' + pro.codpro + '">Stock: ' + formatMoney(pro.stock, 0, ',', '.') + '</div>\n' +
                '                                    </div>\n' +
                '                                    <div class="f-right m-r-0" style="bottom:0;position:absolute;right: 0">' +
                '                                         <span class="mytooltip tooltip-effect-1">\n' +
                '                                              <span class="tooltip-item"><i class="fa fa-camera"></i></span>\n' +
                '                                              <span class="tooltip-content clearfix" style="width: 60px;!important;margin-bottom:-5px;!important;margin-left:-120px;">\n' +
                '                                                   <img src="'+ getUrlFotoProd(pro.codpro) + '" width="120px">\n' +
                '                                              </span>\n' +
                '                                         </span>' +
                '                                    </div>' +
                '                                </div>\n';
        });

        document.getElementById('carrusel_alternativos').innerHTML = items;

        $('.autoplay_alter').slick({
            slidesToShow: 8,
            slidesToScroll: 8
        });

        //Recorre elementos recomendador
        var items_slick = document.getElementsByName('pro_alter');
        count_carrusel_alter = 0;

        items_slick.forEach(function(i){
            if(i.id != ""){
                count_carrusel_alter++;
            }
        });

        //document.getElementById('dv_alternativos').className = "row m-t-20-neg m-b-30-neg";
    }
    function buscaProductosCiclos(tblCotizacion){
        var parametros={
            "rutcli": rut_cliente
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/productos/get-ciclos',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){

                    var productos = json.response;
                    if(productos.length>0){
                        ciclos(productos);
                        agregaEventoClick('btn_ciclo', 'ciclo', tblCotizacion);
                    }
                    else{
                        document.getElementById('carrusel_ciclos').innerHTML = '<p>No hay productos en ciclo</p>';
                    }
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca productos en ciclo', code, text);
            }
        });
    }
    function ciclos(productos){

        if(count_carrusel_ciclo>0){
            $('.autoplay_ciclo').slick('unslick');
        }

        var items = '';

        //console.log(productos);

        productos.forEach(function(pro){
            var despro = pro.despro.replace(pro.marca, '');
            var margen=pro.precio>0?(((pro.precio-pro.costo)/ pro.precio)*100).toFixed(2).toString():'0';

            var com_class = '';
            switch (pro.categoria) {
                case "A":
                    com_class='com-a';
                    break
                case "B":
                    com_class='com-b';
                    break
                case "C":
                    com_class='com-c';
                    break
            }

            items = items + '<div class="bg-primary m-r-10" style="position:relative;">\n' +
                '                                    <div id="pro_ciclo_' + pro.codpro + '" name="pro_ciclo" class="m-l-10 m-t-5">\n' +
                '                                        <div class="f-right m-t-10-neg m-r-0" style="top:0;">\n' +
                '                                            <i class="fa fa-circle '+ com_class +' f-18"></i>' +
                '                                            <button id="btn_ciclo_' + pro.codpro + '" name="btn_ciclo" class="btn btn-inverse btn-icon" title="Agregar a cotizaci贸n"><i class="fa fa-plus m-l-5"></i></button>\n' +
                '                                        </div>\n' +
                '                                        <div id="codpro_' + pro.codpro + '">' + pro.codpro + '</div>\n' +
                '                                        <div id="marca_' + pro.codpro + '" class="f-10">' + pro.marca + '</div>\n' +
                '                                        <div id="despro_' + pro.codpro + '" class="f-12">' + despro + '</div>\n' +
                '                                        <div id="precio_' + pro.codpro + '">Precio: $' + formatMoney(pro.precio, 0, ',', '.') + '</div>\n' +
                (tieneDerecho(40001)?('                                        <div id="margen_' + pro.codpro + '">Margen: ' + margen + '%</div>\n'):'') +
                '                                        <div id="stock_' + pro.codpro + '">Stock: ' + formatMoney(pro.stock, 0, ',', '.') + '</div>\n' +
                '                                        <div id="hasta_' + pro.codpro + '">Hasta: ' + pro.fin + '</div>\n' +
                '                                    </div>\n' +
				'                                    <div class="f-left m-r-0" style="bottom:0;position:absolute;left: 0;padding: 0 5px; background-color: red">' +
				'                                        <div id="descuento_' + pro.codpro + '">' + pro.descuento + '% Dcto</div>\n' +
                '                                    </div>' +
                '                                    <div class="f-right m-r-0" style="bottom:0;position:absolute;right: 0">' +
                '                                         <span class="mytooltip tooltip-effect-1">\n' +
                '                                              <span class="tooltip-item"><i class="fa fa-camera"></i></span>\n' +
                '                                              <span class="tooltip-content clearfix" style="width: 60px;!important;margin-bottom:-5px;!important;margin-left:-120px;">\n' +
                '                                                   <img src="' + getUrlFotoProd(pro.codpro) + '" width="120px">\n' +
                '                                              </span>\n' +
                '                                         </span>' +
                '                                    </div>' +
                '                                </div>\n';
        });

        document.getElementById('carrusel_ciclos').innerHTML = items;
        $('.autoplay_ciclo').slick({
            slidesToShow: 8,
            slidesToScroll: 8
        });
		$('.autoplay_ciclo').slick('slickPlay');

        //Recorre elementos recomendador
        var items_slick = document.getElementsByName('pro_ciclo');
        count_carrusel_ciclo = 0;

        items_slick.forEach(function(i){
            if(i.id != ""){
                count_carrusel_ciclo++;
            }
        });

        //document.getElementById('dv_ciclos').className = "row m-t-20-neg m-b-30-neg";

    }
    function agregaEventoClick(name_btn, tipo_btn, tbl, codpro_reem=""){

        var btnsGroup = document.getElementsByName(name_btn);

        var total_filas_coti = tbl.getData().length;

        var reemplazo = 'btn_' + tipo_btn + '_';

        if(codpro_reem!=""){
            btnsGroup.forEach(function (btn) {
                Handsontable.dom.addEvent(btn, 'click', function(){
                    var codpro = btn.id.replace(reemplazo, '').trim();

                    //tbl.setDataAtCell()
                    var row_replace=0;
                    var row_replace_asign=false;
                    var data_coti = tbl.getData();
                    var count_rows=0;
                    data_coti.forEach(function (row) {
                        var valor_row = row[0] == null ? "" : row[0];
                        if(valor_row==codpro_reem && row_replace_asign!=true){
                            row_replace=count_rows;
                            row_replace_asign=true;
                        }
                        count_rows++;
                    });
                    if(row_replace_asign){
                        tbl.setDataAtCell(row_replace, 0, codpro);
                    }
                    //invocaRegistroBitacora('agregarprodrecom', '', '', codpro);
                });

                //recom=0;
            });
        }
        else {
            btnsGroup.forEach(function (btn) {
                Handsontable.dom.addEvent(btn, 'click', function () {
                    var codpro = btn.id.replace(reemplazo, '').trim();

                    var data_coti = tbl.getData();
                    data_coti.forEach(function (row) {
                        row[0] = row[0] == null ? "" : row[0];
                        row[1] = row[1] == null ? "" : row[1];
                        row[2] = row[2] == null ? "" : row[2];
                        row[3] = row[3] == null ? "" : row[3];
                        row[4] = row[4] == null ? "" : row[4];
                        row[5] = row[5] == null ? "" : row[5];
                        row[6] = row[6] == null ? "" : row[6];
                        row[7] = row[7] == null ? "" : row[7];
                        row[8] = row[8] == null ? "" : row[8];
                        row[9] = row[9] == null ? "" : row[9];
                        row[10] = row[10] == null ? "" : row[10];
                        row[11] = row[11] == null ? "" : row[11];
                        row[12] = row[12] == null ? "" : row[12];
                        row[13] = row[13] == null ? "" : row[13];
                        row[14] = row[14] == null ? "" : row[14];
                    });

                    var cant = 0;
                    var count = 0;
                    data_coti.forEach(function (d) {
                        if (d[0] != "") {
                            cant++;
                        }
                        count++;
                    });

                    var dif = total_filas_coti - cant;

                    if (cant < total_filas_coti) {
                        tbl.setDataAtCell(cant, 0, codpro);
                        //recom = 1;
                    } else {
                        //crear nueva fila
                        tbl.alter('insert_row', cant, 2);
                        tbl.setDataAtCell(cant, 0, codpro);
                        //recom = 1;
                    }

                    if (dif == 1) {
                        tbl.alter('insert_row', total_filas_coti, 1);
                    }

                    //invocaRegistroBitacora('agregarprodrecom', '', '', codpro);
                });

                //recom=0;
            });
        }
    }

	function invocaTransaccionCotizacion(){
		if(from_coti != ""){
			invocaUpdateCotizacion();
		}
		else{
			invocaGuardarCotizacion();
		}
	}

    function invocaGuardarCotizacion(){

        {{--var contacto = document.getElementById('tx_contacto').value;--}}
        {{--var observaciones = document.getElementById('tx_observaciones').value;--}}

        {{--if(contacto !== "") {--}}

        {{--    if (bolsa_cotizacion.length > 0) {--}}
        {{--        var centrocosto = document.getElementsByName('select_ceco')[0].value;--}}
        {{--        var rutcli = rut_cliente;--}}
        {{--        var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';--}}

        {{--        guardaCotizacion(rutcli, centrocosto, 3, ejecutivo, contacto, observaciones, bolsa_cotizacion);--}}
        {{--        $('#btn_cancelar').click();--}}

        {{--        sendMailAfterSave();--}}
        {{--    } else {--}}
        {{--        var msg = 'No hay productos agregados a la cotizaci贸n.';--}}
        {{--        var nFrom = "top";--}}
        {{--        var nAlign = "center";--}}
        {{--        var nIcons = "fa fa-times";--}}
        {{--        var nType = "warning";--}}
        {{--        var nAnimIn = "animated bounceInRight";--}}
        {{--        var nAnimOut = "animated bounceOutRight";--}}
        {{--        var title = "";--}}
        {{--        var message = msg;--}}

        {{--        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);--}}
        {{--        $('#btn_cancelar').click();--}}
        {{--    }--}}
        {{--}--}}
        {{--else {--}}
        {{--    var msg = '';--}}
        {{--    if(contacto==""){--}}
        {{--        msg = msg + '*Contacto ';--}}
        {{--    }--}}

        {{--    var nFrom = "top";--}}
        {{--    var nAlign = "center";--}}
        {{--    var nIcons = "fa fa-times";--}}
        {{--    var nType = "warning";--}}
        {{--    var nAnimIn = "animated bounceInRight";--}}
        {{--    var nAnimOut = "animated bounceOutRight";--}}
        {{--    var title = "";--}}
        {{--    var message = 'Debe completar el campo: ' + msg;--}}

        {{--    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);--}}
        {{--}--}}
            var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
            var contacto=ejecutivo;
            var observaciones= document.getElementById('cli_input_observacion').value;//'Cotizaci贸n generada a trav茅s de Portal ej 2.0';

        if (bolsa_cotizacion.length > 0) {
            var centrocosto = document.getElementsByName('select_ceco')[0].value;
            var rutcli = rut_cliente;
            {{--var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';--}}

            guardaCotizacion(rutcli, centrocosto, 3, ejecutivo, contacto, observaciones, bolsa_cotizacion);
            $('#btn_cancelar').click();


        } else {
            var msg = 'No hay productos agregados a la cotizaci贸n.';
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-times";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = msg;

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            $('#btn_cancelar').click();
        }
    }
    function guardaCotizacion(rutcli, cencos, codemp, ejecutivo, contacto, observacion, productos){
        var direccion=document.getElementsByName('select_direc')[0].value;
		
		
		if(direccion == undefined || direccion == "" || direccion == null){
			var code = '500';
            var text = 'Ingrese direccion de despacho';
            registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);

            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-times";
            var nType = "danger";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = text;

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

            document.getElementById("pre_loader").className = "loader animation-start d-none";
		}

        var subtotal=0;
        //Recorre productos y calcula subtotal
        productos.forEach(function(pro){
            subtotal = subtotal + (pro.precio * pro.cantidad);
        });

        var parametros={
            "rutcli": rut_cliente,
            "coddir": direccion,
            "subtotal": subtotal
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cobro-logistico',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {

                if(json.code==200){
                    var cobro = 0;
                    if(json.response.length>0){
                        var resultado = json.response[0];
                        cobro = resultado.cargo;
                    }

                    if(cobro>0){
                        var existe=false;
                        productos.forEach(function(pro){
                            if(pro.codpro=='WW00008'){
                               existe=true;
                            }
                        });

                        if(existe==false){
                            var codpro='WW00008';
                            var precio=cobro;
                            var costo=cobro;
                            var cantidad=1;
                            var suc=0;
                            var rec=0;
                            productos.push({codpro, precio, costo, cantidad, suc, rec});
                        }
                    }

                    //Guarda cotizacion
                    var parametros = {
                        rutcli: rutcli,
                        cencos: cencos,
                        vendedor: ejecutivo,
                        observacion: observacion,
                        contacto: contacto,
                        codemp: codemp,
                        productos: productos,
                        direccion: direccion
                    };
                    $.ajax({
                        xhrFields: {
                            withCredentials: true
                        },
                        headers: {
                            'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                        },
                        data:parametros,
                        url: '{{ env("URL_API") }}/api/cotizacion/guardar-cotizacion',
                        type: 'PUT',
                        cache: false,
                        datatype: 'json',
                        async: true,
                        beforeSend: function () {
                            document.getElementById("pre_loader").className = "loader animation-start";
                        },
                        success: function (json) {
                            if(json.code==200){
                                //console.log(json);
                                num_coti_guardada = json.response.numcot;

                                if(num_coti_guardada!=null && num_coti_guardada!=0){
                                    if(guardaCotiFromNuevaCoti){

                                        invocaRegistroBitacora('guardarcoti', '', '', num_coti_guardada);
                                        swal("Se ha guardado la cotizaci贸n N掳 " + num_coti_guardada, {
                                            icon: "success",
                                        }).then((ok) => {
                                            if (ok) {

                                                document.getElementById("pre_loader").className = "loader animation-start d-none";
                                                afterSave();
                                                sendMailAfterSave();
                                            }
                                        });
                                    }
                                    else{

                                        invocaRegistroBitacora('guardarcoti', '', '', num_coti_guardada);
                                        swal("Se ha guardado la cotizaci贸n N掳 " + num_coti_guardada, {
                                            icon: "success",
                                        }).then((ok) => {
                                            if (ok) {
                                                //location.reload();

                                                document.getElementById("pre_loader").className = "loader animation-start d-none";
                                                afterSave();
                                                sendMailAfterSave();
                                            }
                                        });
                                    }
                                }
                                else{
                                    var code = '500';
                                    var text = 'No se pudo guardar la cotizaci贸n';
                                    registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);

                                    var nFrom = "top";
                                    var nAlign = "center";
                                    var nIcons = "fa fa-times";
                                    var nType = "danger";
                                    var nAnimIn = "animated bounceInRight";
                                    var nAnimOut = "animated bounceOutRight";
                                    var title = "";
                                    var message = text;

                                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                                }
                            }
                            else{
                                //console.log(json);
                                var code = json.code;
								var text = "";
								if(json.response.respuesta != null){
									text = json.response.respuesta;
								}
								else{
									text = json.response;
								}
                                registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);

                                var nFrom = "top";
                                var nAlign = "center";
                                var nIcons = "fa fa-times";
                                var nType = "danger";
                                var nAnimIn = "animated bounceInRight";
                                var nAnimOut = "animated bounceOutRight";
                                var title = "";
                                var message = text;

                                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                                document.getElementById("pre_loader").className = "loader animation-start d-none";
                            }

                        },
                        error: function(e){
                            var code = e.status;
                            var text = e.statusText;
                            registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);

                            var nFrom = "top";
                            var nAlign = "center";
                            var nIcons = "fa fa-times";
                            var nType = "danger";
                            var nAnimIn = "animated bounceInRight";
                            var nAnimOut = "animated bounceOutRight";
                            var title = "";
                            var message = text;

                            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                            document.getElementById("pre_loader").className = "loader animation-start d-none";
                        }
                    });
                }
                else{
                    console.log(json);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
        });
    }

    //notadeventa
    function invocaGuardarNotaVenta(){
        var contacto=ejecutivo;
        var observaciones= document.getElementById('cli_input_observacion').value;//'Cotizaci贸n generada a trav茅s de Portal ej 2.0';
		var ordenCompra = document.getElementById('cli_input_ordencompra').value;
        if (bolsa_cotizacion.length > 0) {
			var rutcli = rut_cliente;
			var razons = document.getElementById('cli_input_razons').value;
            var centrocosto = document.getElementsByName('select_ceco')[0].value;
			
            var documentadoCon = document.getElementsByName('select_docfut')[0];
			documentadoCon = documentadoCon.options[documentadoCon.selectedIndex].text;
			var tipoNota = document.getElementsByName('select_tiponota')[0];
			tipoNota = tipoNota.options[tipoNota.selectedIndex].text;
			
			var direcc = document.getElementsByName('select_direc')[0];
			direcc = direcc.options[direcc.selectedIndex].text;
			var entregarA = document.getElementById('cli_input_at').value;
			
			var params = {rutcli: rutcli,
						  razons: razons,
						  cencos: centrocosto,
						  documentadoCon: documentadoCon,
						  tipoNota: tipoNota,
						  dirdes: direcc,
						  ordenCompra: ordenCompra,
						  observ: observaciones,
						  entregarA: entregarA};
			var titulo = "緿esea guardar nota de venta?";
            var icono = null;
            $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:{
                    rutcli: rutcli,
                    ordenCompra: ordenCompra
            },
            url: '{{ env("URL_API") }}/api/cotizacion/get-oc-existente',
            type: 'POST',
            cache: false,
			timeout: 3000,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                        document.getElementById("pre_loader").className = "loader animation-start";
                    },
            success: function (json) {
                document.getElementById("pre_loader").className = "loader animation-start d-none";
                if(json.code==200){
                    titulo = json.response.titulo;
                    if(json.response.icono == "none"){
                        icono = null;
                    }
                    else{
                        icono = json.response.icono;
                    }
                }
                swal({
                    title: titulo,
                    //text: datosNota,
                    content: generaTablaSwal('datosNota', params),
                    className: "col-sm-10",
                    icon: icono,
                    buttons: true,
                    dangerMode: true,
                    buttons: {
                        cancel: "No",
                        defeat: {
                            text:"Si",
                            value:"guardar"
                        },
                    },
                    closeOnCancel: false
                })
                .then((value) => {
                    if(value != null){
                        guardaNotaVenta(rutcli, centrocosto, 3, ejecutivo, contacto, observaciones, bolsa_cotizacion, ordenCompra);
                        $('#btn_cancelar').click();
                    }
                    else{
                        stopAudio();
                    }
                });
                askAudio();
            },
            error: function(e){
                swal({
                    title: titulo,
                    content: generaTablaSwal('datosNota', params),
                    className: "col-sm-10",
                    icon: icono,
                    buttons: true,
                    dangerMode: true,
                    buttons: {
                        cancel: "No",
                        defeat: {
                            text:"Si",
                            value:"guardar"
                        },
                    },
                    closeOnCancel: false
                })
                .then((value) => {
                    if(value != null){
                        guardaNotaVenta(rutcli, centrocosto, 3, ejecutivo, contacto, observaciones, bolsa_cotizacion, ordenCompra);
                        $('#btn_cancelar').click();
                    }
                    else{
                        stopAudio();
                    }
                });
                askAudio();
            }
            });
			
            
        } else {
            var msg = 'No hay productos agregados a la nota.';
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-times";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = msg;

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            $('#btn_cancelar').click();
        }
    }

    function guardaNotaVenta(rutcli, cencos, codemp, ejecutivo, contacto, observacion, productos, ordenCompra){
        var direccion=document.getElementsByName('select_direc')[0].value;
		var tipoNota = document.getElementsByName('select_tiponota')[0].value;
		var docfut = document.getElementsByName('select_docfut')[0].value;
		var eliminaCobroLogistico = document.getElementById('cli_input_quitar_cobro_logistico');
		if(eliminaCobroLogistico != null){
			eliminaCobroLogistico = eliminaCobroLogistico.checked;
		}
		var entregar = document.getElementById('cli_input_at').value;
		var tipoDoctoFaltante = "";
		var folioDoctoFaltante = 0;
		if(tipoNota == 23){
			tipoDoctoFaltante = document.getElementsByName('select_tipodocto')[0].value;
			folioDoctoFaltante = document.getElementById('folio_faltante').value;
		}

        var subtotal=0;
        //Recorre productos y calcula subtotal
        productos.forEach(function(pro){
            subtotal = subtotal + (pro.precio * pro.cantidad);
        });

        var parametros={
            "rutcli": rut_cliente,
            "coddir": direccion,
            "subtotal": subtotal
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cobro-logistico',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {

                if(json.code==200){
                    var cobro = 0;
                    if(json.response.length>0){
                        var resultado = json.response[0];
                        cobro = resultado.cargo;
                    }

                    if(cobro>0){
                        var existe=false;
                        productos.forEach(function(pro){
                            if(pro.codpro=='WW00008'){
                               existe=true;
                            }
                        });

                        if(existe==false){
                            var codpro='WW00008';
                            var precio=cobro;
                            var costo=cobro;
                            var cantidad=1;
                            var suc=0;
                            var rec=0;
                            productos.push({codpro, precio, costo, cantidad, suc, rec});
                        }
                    }

                    //Guarda cotizacion
					var parametros = {
                        	rutcli: rutcli,
                        	cencos: cencos,
                        	vendedor: ejecutivo,
                        	observacion: observacion,
							ordenCompra: ordenCompra,
                        	contacto: entregar,
                        	codemp: codemp,
							direccion: direccion,
							docfut: docfut,
                        	productos: productos
					};
					if(from_coti != "" && numordWindow == 0){
						parametros.numcot = from_coti;
					}
					if($("#fechaCajas").val()!=""){
						parametros.fechaCajas = $("#fechaCajas").val();
					}
                    if($("#fechaPromesa").val()!=""){
                        parametros.fechaPromesa = $("#fechaPromesa").val();
                    }
					if(folioDoctoFaltante != 0){ //faltante
						parametros.docfaltante = tipoDoctoFaltante + folioDoctoFaltante;
					}
					if(tipoNota == 6){ //excepcion
						parametros.excepcion = 1;
					}
                    if(tipoNota == 2){
                        parametros.retiraCliente = 1;
                    }
					if(eliminaCobroLogistico != null && eliminaCobroLogistico != false){
						parametros.eliminaCobroLogistico = 1;
					}

					if(numordWindow != 0){
						parametros.numord = numordWindow;
					}else{
						if(from_nota!=""){
							parametros.numnvt = from_nota;
						}
					}
					if(confirmaAnto == true){
						parametros.confirmaGrabacionAnto = 1;
					}
					if(despacharDesdeStgo == true){
						parametros.despacharDesdeStgo = 1;
					}
                    if($("#token_orden").val() != ""){
                        parametros.archivoOrdenCompra = $("#token_orden").val();
                    }
                    $.ajax({
                        xhrFields: {
                            withCredentials: true
                        },
                        headers: {
                            'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3',
							'apikey': '{{Session::get('apikey')}}'
                        },
                        data:parametros,
                        url: '{{ env("URL_API") }}/api/cotizacion/venta',
                        type: 'POST',
                        cache: false,
                        datatype: 'json',
                        async: true,
                        beforeSend: function () {
                            document.getElementById("pre_loader").className = "loader animation-start";
                        },
                        success: function (json) {
                            if(json.code==200){
                                notaGrabada = true;
								successAudio();
                                num_coti_guardada = json.response.numcot;
                                if(num_coti_guardada!=null && num_coti_guardada!=0){ //si directamente la nota esta guardada
										document.getElementById("pre_loader").className = "loader animation-start d-none";
                                        invocaRegistroBitacora('guardarcoti', '', '', num_coti_guardada);
                                        swal({
											title: "Nota de venta ingresada exitosamente",
											content: generaTablaSwal('notaExitosa', json),
                                            icon: "success",
											className: "col-sm-10",
                                        }).then((ok) => {
                                            if (ok) {
                                                afterSave();
                                                //sendMailAfterSave();
                                            }
                                        });
                                }
                                else{
									if(json.response.respuesta.mensaje != undefined){ //si se pide confirmacion
										swal({
                						title: json.response.respuesta.mensaje,
										content: generaTablaSwal('confirmaAntofagasta', json),
										//text: json.response.respuesta.mensaje,
                						icon: "info",
										className: "col-sm-10",
                						buttons: true,
                						dangerMode: true,
                						buttons: {
											grabar: {
                        						text:"Continuar separando",
                        						value:"grabar"
                    						},
											cancel: "Cancelar",
											@foreach($derechos as $derecho)
												@if($derecho->codder == 40005)
													@verbatim
											/*despacharDesdeStgo: {
											text: "Despachar todo desde Stgo",
											value: "despacharDesdeStgo"
											},*/
													@endverbatim
												@endif
											@endforeach										
                						},
										closeOnCancel: false,
										closeOnClickOutside: false
            							})
                						.then((value) => {
											if(value != null){
												if(value == "grabar"){ //si elige grabar
													confirmaAnto = true;
													invocaGuardarNotaVenta();
												}
												else{
													if(value == "despacharDesdeStgo"){ //si elige grabar desde santiago
														confirmaAnto = true;
														despacharDesdeStgo = true;
														invocaGuardarNotaVenta();
													}
												}
											}
											else{ //si no le interesa
												confirmaAnto = false;
												despacharDesdeStgo = false;
												document.getElementById("pre_loader").className = "loader animation-start d-none";
											}
                						});
									}
									else{
										failAudio();
                                    	var code = '500';
                                    	var text = 'No se pudo guardar la nota';
                                    	registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);
                                    	var nFrom = "top";
                                    	var nAlign = "center";
                                    	var nIcons = "fa fa-times";
                                    	var nType = "danger";
                                    	var nAnimIn = "animated bounceInRight";
                                    	var nAnimOut = "animated bounceOutRight";
                                    	var title = "";
                                    	var message = text;

                                    	notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
										confirmaAnto = false;
										despacharDesdeStgo = false;
                                    	document.getElementById("pre_loader").className = "loader animation-start d-none";
									}
                                }
                            }
                            else{
                                //console.log(json);
								confirmaAnto = false;
								failAudio();
                                var code = json.code;
                                var text = "";
								if(json.response.respuesta != null){
									text = json.response.respuesta;
								}
								else{
									text = json.message;
								}
                                registroError(ejecutivo, moduleid_global, 'guarda nota venta', code, text);

                                var nFrom = "top";
                                var nAlign = "center";
                                var nIcons = "fa fa-times";
                                var nType = "danger";
                                var nAnimIn = "animated bounceInRight";
                                var nAnimOut = "animated bounceOutRight";
                                var title = "";
                                var message = text;
								var nvts = "";
								//nuevo formato por si se cae la nota por sap o algo
								if(json.response.nvts!=undefined){
									if(json.response.prodsSinStockAnto != undefined){//si se definen productos sin stock para antofagasta desde el controlador y se esta editando nota
										swal({
											//title: json.response.respuesta.mensaje,
											content: generaTablaSwal('sinStockAnto', json),
											text: text,
                							icon: "warning",
											className: "col-sm-8"
										});
									}
									else{
										for (var i = json.response.nvts.length - 1; i >= 0; i--) {
                                    		nvts += "Nota productos: "+json.response.nvts[i].descripcion  + ", Sucursal: " + json.response.nvts[i].sucursal+ ", Mensaje desde SAP: "+ json.response.nvts[i].mensaje_sap +"\n";
                                		}
										swal(message, nvts, "warning");
									}
								}
								else{
									if(json.response.respuesta != undefined){
										message = json.response.respuesta;
									}
									else{
										message = json.response;
									}
									notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, "Grabacion Nota de venta: ", message);
									if(message.includes("orden de compra")){ //si el mensaje es por orden de compra, demosle glow e.e
										$('#btn_editar_cliente').css('animation','glow 1s infinite alternate');
										$('#cli_input_ordencompra').css('animation','glow 1s infinite alternate');
									}
								}
								
                                //notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
								confirmaAnto = false;
								despacharDesdeStgo = false;
                                document.getElementById("pre_loader").className = "loader animation-start d-none";
                            }

                        },
                        error: function(e){
							failAudio();
							confirmaAnto = false;
                            var code = e.status;
                            var text = "";
							
							if(e.responseText != null){
								text = e.responseText;
							}
							else{
								text = e.message;
							}

                            registroError(ejecutivo, moduleid_global, 'guarda nota de venta', code, text);

                            var nFrom = "top";
                            var nAlign = "center";
                            var nIcons = "fa fa-times";
                            var nType = "danger";
                            var nAnimIn = "animated bounceInRight";
                            var nAnimOut = "animated bounceOutRight";
                            var title = "";
                            var message = text;

                            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                            document.getElementById("pre_loader").className = "loader animation-start d-none";
                        }
                    });
                }
                else{
                    console.log(json);
                }
            },
            error: function(e){
				confirmaAnto = false;
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
        });
    }

    function enviaCotizacionUsach(rutcli, ejecutivo, codemp, productos){
        var parametros = {
            rutcli: rutcli,
            vendedor: ejecutivo,
            codemp: codemp,
            productos: productos
        };
        // console.log(parametros);
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/guardar-cotizacion-tracking',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'guarda cotizacion y envia a usach', code, text);
            }
        });
    }
    function afterSave(){
        if(num_coti_guardada>0) {
			cleanBolsa();
			from_coti = num_coti_guardada;
			if(document.getElementById('tt_numcot') != null){
				document.getElementById('tt_numcot').innerHTML = 'N掳 ' + num_coti_guardada;
			}
			if(document.getElementById('tt_numcot2') != null){
				document.getElementById('tt_numcot2').innerHTML = "";
			}
			if(document.getElementById('btn_guarda_nvt') != null && notaGrabada){
				document.getElementById('btn_guarda_nvt').className = 'btn btn-disabled disabled btn-mini waves-effect waves-light';
            	document.getElementById('btn_guarda_nvt').setAttribute('data-target', '');
				document.getElementById('btn_guarda_nvt').disabled = true;
			}
           
            {{--document.getElementById('btn_guarda_coti').className = 'btn btn-disabled disabled btn-mini waves-effect waves-light';--}}
            {{--document.getElementById('btn_guarda_coti').setAttribute('data-target', '');--}}
			document.getElementById('btn_guarda_coti').setAttribute( "onClick", "javascript: invocaUpdateCotizacion();" );
			
            {{--document.getElementById('dv_recomendados').className = 'row m-t-40-neg m-b-30-neg d-none';--}}
            document.getElementById('dv_tabs').className = "col-xl-12 m-t-30-neg d-none";
            {{--document.getElementById('dv_recomendados').className = 'row m-t-20-neg m-b-30-neg d-none';--}}
            document.getElementById('tx_guardado').className = 'label-main d-inline';

            //Enviar cotizaci贸n a Usach
            var productos = [];

            bolsa_cotizacion.forEach(function(pro){
                productos.push({'codpro': pro.codpro, 'precio': pro.precio, 'cantidad': pro.cantidad, 'rec': pro.rec});
            });

            var rutcli = rut_cliente;
            {{--var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';--}}

            enviaCotizacionUsach(rutcli, ejecutivo, 3, productos);
        }
    }
    function exportarCotizacionPdf(){
        var clase_guardado = document.getElementById('tx_guardado').className;
        //var num_coti = document.getElementById('tx_num_coti_guardada').innerHTML.replace('#', '');

        //console.log(clase_guardado);
        if(!clase_guardado.includes('d-none')){
            exportCotizacionPdf(num_coti_guardada);
        }
        else{
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-warning";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = 'Debe guardar la cotizaci贸n antes de exportarla en formato PDF.';

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
        }
    }
    function exportCotizacionPdf(numcot){
        window.open('{{ url('download-pdf-simple') }}/'+ numcot + "/");
    }

    function exportarCotizacionExcel(){

        var clase_guardado = document.getElementById('tx_guardado').className;
        //var num_coti = document.getElementById('tx_num_coti_guardada').innerHTML.replace('#', '');

        //console.log(clase_guardado);
        if(!clase_guardado.includes('d-none')){
            exportCotizacionExcel(num_coti_guardada);
        }
        else{
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-warning";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = 'Debe guardar la cotizaci贸n antes de exportarla en formato Excel.';

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
        }
    }
    function exportCotizacionExcel(numcot){
        window.open('{{ url('download-excel-simple') }}/'+ numcot + "/");
    }

    function limpiarTablaTemporalBuscDesc(){

        var tabla_temporal = document.getElementById('tbl_temporal');
        var rows_tbl = tabla_temporal.getElementsByTagName('tr');
        var contar = 0;

        /* Recorrer tabla temporal */
        if (rows_tbl.length > 0) {
            rows_tbl.forEach(function (r) {

                tabla_temporal.deleteRow(contar);
                contar++;
            });
        }
    }
    function descartarCotizacion(){
        if(bolsa_cotizacion.length > 0 && num_coti_guardada == 0){
            swal({
                title: "驴Quieres guardar antes la cotizaci贸n actual?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                    cancel: "Cancelar",
                    catch: {
                        text: "No guardar",
                        value: "nosave",
                    },
                    defeat: {
                        text:"Guardar",
                        value:"save"
                    },
                }
            })
        .then((value) => {
                switch (value) {

                    case "cancel":

                        break;

                    case "save":
                        document.getElementById('btn_guarda_coti').click();
                        guardaCotiFromNuevaCoti = true;
                        break;

                    case "nosave":
                         swal("No se guardaron los cambios.", {
                             icon: ""
                         }).then((ok) => {
                             if (ok) {
                                 //location.reload();
                                 invocaRegistroBitacora('descartarcoti');
                                 var previouspage='{{ $previouspage }}';
                                 window.location = "{{ url('generacotizacion') }}/" + rut_cliente + '/' + previouspage;
                             }
                         });
                }
            });
        }
        else{
            // location.reload();
            var previouspage='{{ $previouspage }}';
            window.location = "{{ url('generacotizacion') }}/" + rut_cliente + '/' + previouspage;
        }

    }
    function nuevaCotizacion(){
        if(bolsa_cotizacion.length > 0 && num_coti_guardada == 0){
            swal({
                title: "驴Quieres guardar antes la cotizaci贸n actual?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                    cancel: "Cancelar",
                    catch: {
                        text: "No guardar",
                        value: "nosave",
                    },
                    defeat: {
                        text:"Guardar",
                        value:"save"
                    },
                }
            })
                .then((value) => {
                    switch (value) {

                        case "cancel":

                            break;

                        case "save":
                            document.getElementById('btn_guarda_coti').click();
                            guardaCotiFromNuevaCoti = true;
                            invocaRegistroBitacora('nuevacoti');
                            break;

                        case "nosave":
                            swal("No se guardaron los cambios.", {
                                icon: ""
                            }).then((ok) => {
                                if (ok) {
                                    invocaRegistroBitacora('nuevacoti');
                                    //location.reload();
                                    var previouspage='{{ $previouspage }}';
                                    window.location = "{{ url('generacotizacion') }}/" + rut_cliente + '/' + previouspage;
                                }
                            });
                    }
                });
        }
        else{
            // location.reload();
            var previouspage='{{ $previouspage }}';
            window.location = "{{ url('generacotizacion') }}/" + rut_cliente + '/' + previouspage;
        }
    }
    function exportTemplateExcel(){
        invocaRegistroBitacora('descargarplantilla');
        window.open('{{ url('downloadtemplate') }}');

    }
    function invocaImportCotizacion(form, coddir){
		coddir = coddir || "";
		/*var proData, contentTy = true;
		var contentTy = "application/json";
		alert(document.getElementsByName('select_direc')[0].value);
		if(from_autocoti != ""){
			form = {"autoCoti" : rut_cliente,
					"coddir": coddir,
					"_token": '{{ csrf_token() }}'};
			proData = true;
			contentTy = undefined;
		}
		else{
			proData = false;
			contentTy = false;
		}*/
		
		var proData = false, contentTy = false;
        $.ajax({
            data: form,
            cache: false,
            processData: proData,
            contentType: contentTy,
            url: '{{ url('importexcelcotizacion') }}',
            type: 'POST',
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json) {
                if(json.code==200) {
                    setmasivoimport = true;

                    var tabla_temporal = document.getElementById('tbl_temporal');
					
                    //var rows_tbl = tabla_temporal.getElementsByTagName('tr');
                    //var contar=0;
                    //var validados=[];

                    var productos = json.response;

                    var products=[];

                    productos.forEach(function(p){
                        products.push({ codpro: p[0], precio: p[1], cantidad: p[2]});
                    });

                    var parametros = {
                        rutcli: rut_cliente,
                        codemp: 3,
                        productos: products
                    };
                    ($.ajax({
                        xhrFields: {
                            withCredentials: true
                        },
                        headers: {
                            'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                        },
                        data: parametros,
                        url: '{{ env("URL_API") }}/api/productos/validar-productos-import',
                        type: 'POST',
                        datatype: 'json',
                        async: false,
                        success: function (json) {

                            //console.log(json);
                            if(json.code==200) {
                                var contar = 0;

                                var productos_ = json.response;

                                productos_.forEach(function(p){

                                    var NewRow = tabla_temporal.insertRow(contar);
                                    var Newcell1 = NewRow.insertCell(0);
                                    var Newcell2 = NewRow.insertCell(1);
                                    var Newcell3 = NewRow.insertCell(2);
                                    var Newcell4 = NewRow.insertCell(3);
                                    var Newcell5 = NewRow.insertCell(4);
                                    var Newcell6 = NewRow.insertCell(5);
                                    var Newcell7 = NewRow.insertCell(6);
                                    var Newcell8 = NewRow.insertCell(7);
                                    var Newcell9 = NewRow.insertCell(8);
                                    var Newcell10 = NewRow.insertCell(9);
                                    var Newcell11 = NewRow.insertCell(10);
                                    var Newcell12 = NewRow.insertCell(11);

                                    var precio_numeric = p.precio;
                                    var cantidad_numeric = p.cantidad;

                                    Newcell1.innerHTML = p.codpro;
                                    Newcell2.innerHTML = p.despro;
                                    Newcell3.innerHTML = p.marca;
                                    Newcell4.innerHTML = '$' + formatMoney(p.precio, 0, ',', '.');
                                    Newcell5.innerHTML = p.categoria;
                                    Newcell6.innerHTML = p.condicion_de_venta;
                                    Newcell7.innerHTML = p.margen + '%';
                                    Newcell8.innerHTML = formatMoney(p.stock, 0, ',', '.');
                                    Newcell9.innerHTML = p.costo;
                                    Newcell10.innerHTML = formatMoney(p.stock_ant, 0, ',', '.');
                                    Newcell11.innerHTML = p.convenio;
                                    Newcell12.innerHTML = p.cantidad;

                                    contar++;
                                });
								yaValidada = false;
                                $('#btn_agregar_coti').click();
								if(from_autocoti == ""){
                                	$('#btnCloseModalImportCoti').click();
                                	document.getElementById('file-import-excel').value = '';
								}

                                invocaRegistroBitacora('importarcoti');

                            }
                        },
                        error: function(e){
                            var code = e.status;
                            var text = e.statusText;
							alert(JSON.stringify(e));
                            registroError(ejecutivo, moduleid_global, 'valida productos importados desde excel', code, text);
                        }
                    }));
                }

                document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'importar cotizacion desde excel', code, text);
            }
        });
    }
    function invocaSendCotizacionEmail(form){
        //console.log(form);
        var parametros = {
            numcot: form
        };
        //console.log(parametros);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            data: parametros,
            url: '{{ url('sendcotizacionemail') }}',
            type: 'POST',
            datatype: 'json',
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {
                //console.log(json);
                if(json.code==200) {
                    console.log(json);
                }

                //document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'envia cotizacion por correo', code, text);
            }
        });
    }

    function validarEmail(valor) {
        var emailvalido=false;

        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

        if (emailRegex.test(valor)){
            emailvalido=true;
        } else {
            emailvalido=false;
        }
        console.log(emailvalido);

        return emailvalido;
    }
    function invocaSendMailCotizacion(){

        var clase_guardado = document.getElementById('tx_guardado').className;

        if(!clase_guardado.includes('d-none')){
            document.getElementById('btn_enviar_correo').setAttribute("data-toggle", "modal");
            document.getElementById('btn_enviar_correo').setAttribute("data-target", "#modal-send-email");

            invocaRegistroBitacora('clicenviarcorreo');

            //generarArchivoAdjunto();
        }
        else{
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-warning";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = 'Debe guardar la cotizaci贸n antes de enviarla por correo.';

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
        }
    }
    function generarArchivoAdjunto(){

        var destinatario = document.getElementById('tx_correo_destinatario').value;

        if(destinatario!="") {
            var emailvalido = validarEmail(destinatario);

            if (emailvalido) {

                var numcot = num_coti_guardada;

                var parametros = {
                    "numcot": numcot
                };
                $.ajax({
                    xhrFields: {
                        withCredentials: true
                    },
                    headers: {
                        'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                    },
                    data: parametros,
                    url: '{{ env("URL_API") }}/api/cotizacion/exportar-cotizacion',
                    type: 'POST',
                    cache: false,
                    datatype: 'json',
                    beforeSend: function () {
                        document.getElementById("pre_loader").className = "loader animation-start";
                    },
                    success: function (json) {
                        if (json.code == 200) {

                            var productos = json.response.detalle_coti;
                            var totales = json.response.encabezado_coti;

                            var parametros = {
                                _token: '{{ csrf_token() }}',
                                productos: productos,
                                totales: totales
                            };
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                data: parametros,
                                url: '{{ url('generarpdfcotizacionandsave') }}',
                                type: 'POST',
                                datatype: 'json',
                                success: function (json) {
                                    var rutaarchivo = json.response.rutaarchivo;
                                    var nombrearchivo = json.response.nombrearchivo;
                                    var nombreejecutivo = '{{ Session::get('nombre_ejecutivo') }}';

                                    var destinatario = document.getElementById('tx_correo_destinatario').value;
                                    var comentarioejecutivo = document.getElementById('tx_comentario_ejecutivo').value;

                                    sendMailCotizacion(numcot, rutaarchivo, nombrearchivo, nombreejecutivo, destinatario, comentarioejecutivo);
                                },
                                error: function(e){
                                    console.log(ejecutivo);
                                    var code = e.status;
                                    var text = e.statusText;
                                    registroError(ejecutivo, moduleid_global, 'envia cotizacion por correo', code, text);
                                }
                            });
                        } else {
                            //generaArchivoAdjunto=false;
                        }
                    },
                    error:function(e){
                        var code = e.status;
                        var text = e.statusText;
                        registroError(ejecutivo, moduleid_global, 'generar pdf para enviar por correo', code, text);
                    }
                });
            } else {
                var nFrom = "top";
                var nAlign = "center";
                var nIcons = "fa fa-times";
                var nType = "danger";
                var nAnimIn = "animated bounceInRight";
                var nAnimOut = "animated bounceOutRight";
                var title = "";
                var message = 'El correo ingresado no est谩 correcto, favor vuelva a ingresarlo.';

                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                document.getElementById('tx_correo_destinatario').value = "";
            }
        }
    }
    function sendMailCotizacion(numcot, rutaarchivo, nombrearchivo, nombreejecutivo, destinatario, comentarioejecutivo){
        var parametros = {
            numcot: numcot,
            rutaarchivo: rutaarchivo,
            nombrearchivo: nombrearchivo,
            ejecutivo: nombreejecutivo,
            destinatario: destinatario,
            comentarioejecutivo: comentarioejecutivo
        };
        $.ajax({
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            data: parametros,
            url: '{{ url('sendmailcotizacion') }}',
            type: 'POST',
            datatype: 'json',
            success: function (json) {
                if(json.code==200){
                    invocaRegistroBitacora('enviarcorreo', '', '', numcot);

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-check";
                    var nType = "success";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
					document.getElementById('tx_correo_destinatario').value = "";
                	document.getElementById('tx_comentario_ejecutivo').value = "";
                    $('#close-send-email').click();
                }
				else{
					$('#modal_actualiza_mail_titulo').text(json.response);
					$('#modal_actualiza_mail').modal('show');
				}
                document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'enviar cotizacion por correo', code, text);

                document.getElementById("pre_loader").className = "loader animation-start d-none";
            }
        });
    }

	function actualizaMail(){
		var mailEjec = $("#modal_actualiza_mail_mail").val();
		var nFrom = "top";
        var nAlign = "center";
        var nIcons = "fa fa-times";
        var nType = "danger";
        var nAnimIn = "animated bounceInRight";
        var nAnimOut = "animated bounceOutRight";
        var title = "";
        var message = "Ingrese mail para continuar";
		if(mailEjec.length == 0){
        	notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
		}else{
			var parametros = {
                    "mail": mailEjec,
					"userid": '{{ Session::get('usuario')->get('userid') }}'
            };
			$.ajax({
            	xhrFields: {
                	withCredentials: true
                },
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                data: parametros,
                url: '{{ url('actualizar-mail') }}',
                type: 'POST',
                cache: false,
                datatype: 'json',
                beforeSend: function () {
                	document.getElementById("pre_loader").className = "loader animation-start";
                },
				success: function (json) {
                	if(json.code==200) {
                    	nIcons = "fa fa-check";
                    	nType = "success";
						message = "Mail actualizado exitosamente";
						notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
						$("#modal_actualiza_mail").modal("hide");
                	}
                	else{
						notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, json.response);
					}
					document.getElementById("pre_loader").className = "loader animation-start d-none";
            	},
            	error: function(e){
					document.getElementById("pre_loader").className = "loader animation-start d-none";
                	var code = e.status;
                	var text = e.statusText;
					notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, e.statusText);
                	registroError(ejecutivo, moduleid_global, 'actualiza mail', code, text);
            	}
        	});
			
		}
		
	}
    function sendMailAfterSave(){
        if(bolsa_cotizacion.length > 0 && num_coti_guardada != 0){
            swal({
                title: "驴Deseas enviar la cotizaci贸n por correo?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                    cancel: "No enviar",
                    defeat: {
                        text:"Enviar",
                        value:"send"
                    },
                }
            })
                .then((value) => {
                    switch (value) {

                        case "cancel":

                            break;

                        case "send":
                            var contacto = document.getElementById('tx_contacto').value;
                            document.getElementById('tx_correo_destinatario').value = contacto;

                            $('#btn_enviar_correo').click();
                            break;
                    }
                });
        }
    }

    /* Despacho */
    function buscarDireccionesDespacho(){ //esta llamada se podria quitar perfectamente, pero hay que dejarla para cuando el sistema sea mas estable
        //console.log('viene a buscar direccion de despacho');
        var cencos = document.getElementsByName("select_ceco")[0].value;
		if(cencos == ""){
			cencos = document.getElementsByName("select_ceco")[0][1].value;
		}

        var parametros = {
            "rutcli": rut_cliente,
            "cencos": cencos,
            "codemp": 3
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-direccion-despacho',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: false,
            success: function (json) {
                //console.log(json);
                if (json.code == 200) {
                    //console.log(json);

                    var direcciones= json.response;

                    var options_select_direc = '';

                    direcciones.forEach(function(direc){

                        var direccion=direc.direccion;
                        var comuna=direc.comuna;
                        var ciudad=direc.ciudad;
                        var region='';
                        var cod_direc=direc.codigo_direccion;
						var sucursal=direc.sucursal;
                        var retira_cliente = direc.retira_cliente;
                        options_select_direc = options_select_direc + '<option id="id_' + cod_direc + '" value="' + cod_direc + '" sucursal="'+ sucursal +'" retira_cliente ="' + direc.retira_cliente + '">' + direccion + ', ' + comuna + ', ' + ciudad + '</option>\n';

                    });


                    document.getElementsByName('select_direc')[0].innerHTML = options_select_direc;

                    updateDireccionDespachoPopover();
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar direccion despacho', code, text);
            }
        });
    }
    function updateDireccionDespachoPopover(){
		var objDireccion = document.querySelector("[name='select_direc']");
        var direccion = objDireccion.value;
        var texto_direccion = document.getElementById('id_' + direccion).innerText;
		var costoTransporte = 0;
		var totalCostoTransporte = 0;
		var sucursal = objDireccion.options[objDireccion.selectedIndex].getAttribute("sucursal");
        var retiraClienteOption = objDireccion.options[objDireccion.selectedIndex].getAttribute("retira_cliente");

        //a馻dimos o retiramos tipo de nota dependiendo de la direccion
        $("[name='select_tiponota'] option[value='2']").remove();
        if(retiraClienteOption == 1){
            $("[name='select_tiponota']").append('<option value="2">RETIRA CLIENTE</option>');
        }
        

		$("#sucursal-cliente").val(sucursal);
        //Llenar popover
        var rutcliente=document.getElementById('cli_input_rut').value.slice(0, -2);
        // console.log(rutcliente);
        var ceco = document.getElementsByName('select_ceco')[0].value;
        var forpag=document.getElementById('cli_input_formapago').value;
        var plapag=document.getElementById('cli_input_plazopago').value;
        var estado=document.getElementById('cli_input_estado').value;
        llenarPopoverCliente(rutcliente, ceco, forpag, plapag, estado, texto_direccion);
        var codpro, stoantofa, stopro, costo, prepro, cliente_antofa, codproTablaCoti;

        //console.log(document.getElementById('tb_infoclient_direc_despacho'));
        //document.getElementById('tb_infoclient_direc_despacho').innerHTML = direccion;
        if(ceco!=0){
            invocaRegistroBitacora('cambiardirecdes', olddirection, direccion, '');
        }
        else{
            olddirection=direccion;
        }

        //actualizar productos coti-nvt
        var tipbus = 'CODPRO';
		if(bolsa_cotizacion.length>=0){
			var parametros = {
            	rutcli: rutcliente,
            	cod_producto: bolsa_cotizacion,
            	tipo_busqueda: tipbus,
            	coddir: document.getElementsByName('select_direc')[0].value
        	};
        	$.ajax({
            	xhrFields: {
                	withCredentials: true
            	},
            	headers: {
                	'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            	},
            	data: parametros,
            	url: '{{ env("URL_API") }}/api/productos/productos-especificos',
            	type: 'POST',
            	// cache: true,
            	datatype: 'json',
            	// async: true,
            	beforeSend: function () {
                	document.getElementById("pre_loader").className = "loader animation-start";
            	},
            	success: function (json){
                	if (json.code == 200) {
                    	for (var i = json.response.length - 1; i >= 0; i--) {
                        	codpro = json.response[i].codpro;
                        	prepro = json.response[i].precio;
                        	stopro = json.response[i].stock;
                        	stoantofa = json.response[i].stock_ant;
                        	costo = json.response[i].costo;
                        	cliente_antofa = json.response[i].cliente_antofa;
							console.log(cliente_antofa + " productos-especificos");
							costoTransporte = json.response[i].transporte;
							totalCostoTransporte = totalCostoTransporte + costoTransporte;
                        	for (var j = 0; j < tblCotizacionWindow.countRows(); j++){
								set_masivo = true;
                            	codproTablaCoti = tblCotizacionWindow.getDataAtCell(j,0);
                            	if(codproTablaCoti == codpro){
                                	//precio, programar con la api de precios tblCotizacion.setDataAtCell(row, 3, '$' + formatMoney(prepro, 0, ',', '.'));
                                	//total, programar con la api de precios tblCotizacion.setDataAtCell(row, 5, '$' + formatMoney(prepro, 0, ',', '.'));
                                	if (cliente_antofa == 1) {
                                    	tblCotizacionWindow.setCellMeta(j, tblCotizacionWindow.propToCol('Stock'), 'readOnly', false);
                                    	tblCotizacionWindow.setDataAtCell(j, 9, formatMoney(stoantofa, 0, ',', '.'));
                                    	//Stock
                                    	var stock = [
                                        	{nombre: 'Anto', valor: formatMoney(stoantofa, 0, ',', '.')},
                                        	{nombre: 'Stgo', valor: formatMoney(stopro, 0, ',', '.')}
                                    	];
                                    	tblCotizacionWindow.setCellMeta(j, tblCotizacionWindow.propToCol('Stock'), 'handsontable', {
                                        	data: stock, autoColumnSize: true,
                                        	getValue: function () {
                                            	var selection = this.getSelectedLast();
                                            	return this.getSourceDataAtRow(selection[0]).valor;
                                        	}
                                    	});
                                    	tblCotizacionWindow.setDataAtCell(j, 11, "ANTO");
                                	} else {
                                    	tblCotizacionWindow.setDataAtCell(j, 9, formatMoney(stopro, 0, ',', '.'));
                                    	tblCotizacionWindow.setCellMeta(j, tblCotizacionWindow.propToCol('Stock'), 'readOnly', true);
                                    	tblCotizacionWindow.setDataAtCell(j, 11, "STGO");
                                	}
                                	//programar con la api de precios tblCotizacion.setDataAtCell(row, 10, costo);
                                	tblCotizacionWindow.setDataAtCell(j, 14, '$' + formatMoney(costoTransporte, 0, ',', '.'));
                            	}
                        	}
							
                    	}
						set_masivo = false;
						//seteo costo transporte seg鷑 sumatoria del controlador
						objSubtotales[4] = totalCostoTransporte;
						makeInfoTotalesCotizacion(objSubtotales);
                    	document.getElementById("pre_loader").className = "loader animation-start d-none";
                	}
            	},
            	error: function(e){
                	var code = e.status;
                	var text = e.statusText;
                	registroError(ejecutivo, moduleid_global, 'buscar producto por codigo', code, text);
            	}
        	});
		}
    }
	
    function obtieneDetalleCotizacion(numcot, numord, numnvt, autoCoti, cencos){
		numord = numord || 0;
		numcot = numcot || 0;
		numnvt = numnvt || 0;
		cencos = cencos || 0;
		autoCoti = autoCoti || "lala";
		var parametros;
		if(numord != 0){
			parametros = {
            	"numord": numord
        	};
		}
		if(numcot != 0){
			parametros = {
            	"numcot": numcot
        	};
		}
		if(numnvt != 0){
			parametros = {
            	"numnvt": numnvt
        	};
		}
		if(autoCoti != "lala"){
			parametros = {
            	"autocoti": rut_cliente,
				"coddir": autoCoti,
				"cencos": cencos
        	};

		}
        
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cotizacion',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json) {
                if (json.code == 200) {
					numordWindow = numord;
					if(json.response.encabezado_coti[0].coddir == null){ //si la coti viene sin direccion, la pedimos
						faltaDireccion = true;
						document.getElementById("btn_editar_cliente").click();
					}
					if(numordWindow != 0 || from_nota != "" || (numcot != 0 && from_otra_coti == 0)){ //si es una orden web o estamos editando una nota de venta
						mgWeb = json.response.encabezado_coti[0].margen;
						if(json.response.encabezado_coti[0].observ != ' '){
							swal("Observacion cliente", json.response.encabezado_coti[0].observ, "info");
							document.getElementById('cli_input_observacion').value = json.response.encabezado_coti[0].observ;
						}
						if(json.response.encabezado_coti[0].ordencompra != ' ' && json.response.encabezado_coti[0].ordencompra != undefined){
							document.getElementById('cli_input_ordencompra').value = json.response.encabezado_coti[0].ordencompra;
						}
						if(json.response.encabezado_coti[0].cencos != ' ' && json.response.encabezado_coti[0].cencos != undefined){
							document.getElementsByName('select_ceco')[0].value = json.response.encabezado_coti[0].cencos;
						}
						if(json.response.encabezado_coti[0].coddir != ' ' && json.response.encabezado_coti[0].coddir != undefined){
							document.getElementsByName('select_direc')[0].value = json.response.encabezado_coti[0].coddir;
						}
					}
					if(document.getElementById('tt_numcot2') != null){
						if(from_otra_coti == 0){
							document.getElementById('tt_numcot2').innerHTML = "Editando cotizaci贸n N掳 <b>" + numcot + "</b>";
						}
						else{
							$("#btn_guarda_coti").attr("onclick", "invocaGuardarCotizacion();");
							document.getElementById('tt_numcot2').innerHTML = "Cotizaci贸n generada desde N掳 <b>" + numcot + "</b>";
						}
					}
					else{
						if(from_otra_coti == 0){
							document.getElementById('tt_numcot').innerHTML = "Editando cotizaci贸n N掳 <b>" + numcot + "</b>";
						}
					}
					if(document.getElementById('dv_buscar_numcot') != null){ //ocultar buscar coti
						document.getElementById('dv_buscar_numcot').className = 'float-right m-r-350 m-t-5-neg width-350-p d-none';
					}
                    if(document.getElementById('dv_buscar_numord') != null){ //ocultar buscar numord (?)
						document.getElementById('dv_buscar_numord').className = 'float-right m-r-350 m-t-5-neg width-350-p d-none';
					}
										
                    var productos=json.response.detalle_coti;
                    var totales=json.response.encabezado_coti;

                    var neto = formatMoney(totales[0].neto, 0, ',', '.');
                    var iva = formatMoney(totales[0].iva, 0, ',', '.');
                    var total = formatMoney(totales[0].total, 0, ',', '.');
                    var margen = totales[0].margen;

                    var obj = [neto, iva, total, margen];
                    //obj.push([totales[0].neto, totales[0].iva, totales[0].total,totales[0].margen]);


                    //Busca c贸digo de cobro logistico
                    var cobrologistico=0;
                    productos.forEach(function(pro){
                        if(pro.codpro=="WW00008"){
                            cobrologistico = formatMoney(pro.precio, 0, ',', '.');
							cobroLogisticoWeb = formatMoney(pro.precio, 0, ',', '.');
                        }
                    });
					if(from_otra_coti == 0){
                    	makeInfoTotalesCotizacion(obj, cobrologistico);
					}
                    productos.forEach(function(pro){
                        if(pro.codpro!='WW00008'){
							var precio = '$1';
							var cantidad = '1';
							var total = '$1';
							var stock = formatMoney(pro.stock, 0, ',', '.');
							var precioNum = 1;
							var cantNum = 1;
							if(from_otra_coti == 0){//si no viene de otra coti, se traen los valores tal cual del origen
								precio = '$' + formatMoney(pro.precio, 0, ',', '.');
                            	cantidad = formatMoney(pro.cantid, 0, ',', '.');
                            	total = '$' + formatMoney(pro.totnet, 0, ',', '.');
							}
							else{ //si viene de importacion, revisamos parametros de entrada
								if(importarPrecio){
									precio = '$' + formatMoney(pro.precio, 0, ',', '.');
									precioNum = parseInt(pro.precio);
								}
								if(importarCantidad){
									cantidad = formatMoney(pro.cantid, 0, ',', '.');
									cantNum = parseInt(pro.cantid);
								}
								total = '$' + formatMoney((precioNum * cantNum), 0, ',', '.');
							}
                            
                            //bolsa_cotizacion_completa.push({ "Codigo": pro.codpro, "Descripcion": pro.despro, "Marca": pro.desmar, "Precio": pro.precio, "Cantidad": pro.cantid, "Total": pro.totnet });
                            bolsa_cotizacion_completa.push({ "Codigo": pro.codpro, "Descripcion": pro.despro, "Marca": pro.desmar, "Precio": precio, "Cantidad": cantidad, "Total": total, "Margen": pro.margen+'%', "Comision": pro.comision, "Condicion": pro.condicion_de_venta, "Stock": stock, "Costo": pro.costos, "Suc": pro.suc, "Convenio": pro.convenio, "Recomendado": pro.recomendado, "Transporte": "$0" });
                        }

                    });
					//limpiamos valores de importacion para evitar errores u otras cosas
					importarPrecio = true;
					importarCantidad = true;
                    bolsa_cotizacion_completa.push({ "Codigo": "", "Descripcion": "", "Marca": "", "Precio": "", "Cantidad": "", "Total": "", "Margen": "", "Comision": "", "Condicion": "", "Stock": "", "Costo": "", "Suc": "", "Convenio": "", "Recomendado": "", "Transporte": "" });
					makeTablaCotizacion(bolsa_cotizacion_completa);
					
                    document.getElementById("pre_loader").className = "loader animation-start d-none";
					updateDireccionDespachoPopover(); //finalmente, el fix
                }
                else{
					var botones = {
                    		defeat: {
                        		text:"Volver a ficha cliente",
                        		value:"volverAFicha"
                    		}
                		};
					if(numnvt != 0){
						botones.verNota = {
								text:"Ver Nota de Venta",
								value:"ver"
							};
					}
					swal({
                		title: "No se puede editar",
                		text: json.response,
                		icon: "error",
                		buttons: botones,
						closeOnCancel: false,
						closeOnClickOutside: false
            		})
                	.then((value) => {
						if(value != null){
							if(value == "ver"){
								$("#modal-cliente").modal("hide");
								numnvtWindow = numnvt;
								modalVerNota(numnvt);
							}
							else{
								redirecturl('btn_fichacliente');
							}
						}
                	});
                }

                document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener cotizacion', code, text);
            }
        });
    }
    function generarDesdeCoti(){
        var numcot_buscado = document.getElementById('find_numcot').value;
		//obtenemos valores para importacion
		importarPrecio = $("#cb_importar_precios").prop("checked");
		importarCantidad = $("#cb_importar_cantidad").prop("checked");
		$("#modal_importarcoti").modal("hide");
        if(numcot_buscado!=""){
			from_coti = "";
			numordWindow = 0;
            obtieneDetalleCotizacion(numcot_buscado);
        }
    }

	function generarDesdeNotaWeb(){
        var numord_buscado = document.getElementById('find_numord').value;
        if(numord_buscado!=""){
            obtieneDetalleCotizacion(0,numord_buscado);
        }
    }


    function buscaCobroLogistico(subtotal){
        subtotal = subtotal.replace('.', '');
        var coddir=document.getElementsByName('select_direc')[0].value;

        var parametros={
            "rutcli": rut_cliente,
            "coddir": coddir,
            "subtotal": subtotal
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cobro-logistico',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){

                    var resultado = json.response[0];

                    var cobro = resultado.cargo;
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
        });

    }

    /* Registro bit谩cora */
    function invocaRegistroBitacora(action, oldvalue='', newvalue='', updaterecord=''){
        /*
        Module: generarcotizacion -> 11
        Action: cambio centro costo	 -> 14
                cambio direccion despacho	 -> 15
                clic en descartar cotizacion	 -> 16
                clic en descargar plantilla	 -> 17
                clic en importar cotizacion	 -> 18
                clic en nueva cotizacion	 -> 19
                agrega producto recomendado	 -> 20
                clic en guardar cotizacion	 -> 21
                guarda cotizacion	 -> 22
                envia correo al guardar	 -> 23

                clic en ver cotizaciones	 -> 8
                clic en exportar pdf	 -> 10
                clic en enviar correo	 -> 11
                clic en refrescar pagina	 -> 13

                clic en ficha cliente	 -> 4
        */
        var action_num=0;
        switch (action) {
            case "cambiarceco":
                action_num=14;
                break;
            case "cambiardirecdes":
                action_num=15;
                break;
            case "descartarcoti":
                action_num=16;
                break;
            case "descargarplantilla":
                action_num=17;
                break;
            case "importarcoti":
                action_num=18;
                break;
            case "nuevacoti":
                action_num=19;
                break;
            case "agregarprodrecom":
                action_num=20;
                break;
            case "clicguardarcoti":
                action_num=21;
                break;
            case "guardarcoti":
                action_num=22;
                break;
            case "vercotis":
                action_num=8;
                break;
            case "exportpdf":
                action_num=10;
                break;
            case "clicenviarcorreo":
                action_num=11;
                break;
            case "enviarcorreo":
                action_num=26;
                break;
            case "refresh":
                action_num=13;
                break;
            case "fichacliente":
                action_num=4;
                break;
        }
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=11;
        var actionid=action_num;
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue, updaterecord);
    }
	
	function getRecomendados(rutcli, productos, tbl){
		
		var prods = [];
		for(var i= 0; i<productos.length; i++){
			prods.push({"sku":productos[i].codpro, "cantidad":productos[i].cantidad});
		}
		var coso = {"user_id" : "TESTUSER", "client_id" : rutcli, "products": prods};
		
		var parametros = {"rutcli": rutcli,
						  "json_prods": JSON.stringify(coso)
		}
		$.ajax({
			data:parametros,
            url: '{{ env("URL_API") }}/api/productos/get-recomendados',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.products!= undefined){
					if(json.products != undefined){
                    	var btnRecom = document.getElementById('btn-recom');
                        if(!btnRecom.className.includes('active')){
                            $('#btn-recom').click();
                        }
                        recomendados(json.products);

                        var btnRecomendador = document.getElementsByName('btn_recomendador');
                        var total_filas_coti = tbl.getData().length;

                        btnRecomendador.forEach(function (btn) {
                            Handsontable.dom.addEvent(btn, 'click', function(){
                                var codpro = btn.id.replace('btn_', '').trim();

                                var data_coti = tbl.getData();
                                data_coti.forEach(function (row) {
                                    row[0] = row[0] == null ? "" : row[0];
                                    row[1] = row[1] == null ? "" : row[1];
                                    row[2] = row[2] == null ? "" : row[2];
                                    row[3] = row[3] == null ? "" : row[3];
                                    row[4] = row[4] == null ? "" : row[4];
                                    row[5] = row[5] == null ? "" : row[5];
                                    row[6] = row[6] == null ? "" : row[6];
                                    row[7] = row[7] == null ? "" : row[7];
                                    row[8] = row[8] == null ? "" : row[8];
                                    row[9] = row[9] == null ? "" : row[9];
                                    row[10] = row[10] == null ? "" : row[10];
                                    row[11] = row[11] == null ? "" : row[11];
                                    row[12] = row[12] == null ? "" : row[12];
                                    row[13] = row[13] == null ? "" : row[13];
                                    row[14] = row[14] == null ? "" : row[14];
                                });

                                var cant = 0;
                                var count = 0;
                                data_coti.forEach(function (d) {
                                    if(d[0]!=""){
                                        cant++;
                                    }
                                    count++;
                                });

                                var dif = total_filas_coti - cant;

                                if(cant<total_filas_coti){
                                    tbl.setDataAtCell(cant, 0, codpro);
                                    recom = 1;
                                }
                                else{
                                    //crear nueva fila
                                    tbl.alter('insert_row', cant, 2);
                                    tbl.setDataAtCell(cant, 0, codpro);
                                    recom = 1;
                                }

                                if(dif==1){
                                    tbl.alter('insert_row', total_filas_coti, 1);
                                }

                                invocaRegistroBitacora('agregarprodrecom', '', '', codpro);
                            });

                            recom=0;
                        });
                    }
                }
            },
            error: function(e){
				document.getElementById('btn_autoplay').className="d-none";
                document.getElementById('carrusel_recomendador').innerHTML = '<p>No hay productos recomendados</p>';
                document.getElementById('dv_tabs').className = "col-xl-12 m-t-30-neg";

                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca recomendados', code, text);
            }
        });

	}
	
	function invocaUpdateCotizacion(){

        // console.log(bolsa_cotizacion);
		var direccion=document.getElementsByName('select_direc')[0].value;
		if(direccion == undefined || direccion == "" || direccion == null){
			var code = '500';
            var text = 'Ingrese direccion de despacho';
            registroError(ejecutivo, moduleid_global, 'guarda cotizacion', code, text);

            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-times";
            var nType = "danger";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = text;

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
			$('#btn_editar_cliente').css('animation','glow 1s infinite alternate');
            document.getElementById("pre_loader").className = "loader animation-start d-none";
		}

        if (bolsa_cotizacion.length > 0) {
            var rutcli = rut_cliente;

            updateCotizacion(rutcli, 3, bolsa_cotizacion);
            $('#btn_cancelar').click();
        } else {
            var msg = 'No se ha modificado la cotizaci贸n.';
            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-exclamation-triangle";
            var nType = "warning";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = msg;

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            $('#btn_cancelar').click();
        }
    }
    function updateCotizacion(rutcli, codemp, productos){
        var direccion=document.getElementsByName('select_direc')[0].value;
        var subtotal=0;
        //Recorre productos y calcula subtotal
        productos.forEach(function(pro){
            subtotal = subtotal + (pro.precio * pro.cantidad);
        });

        var parametros={
            "rutcli": rut_cliente,
            "coddir": direccion,
            "subtotal": subtotal
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cobro-logistico',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){

                    var cobro = 0;
                    if(json.response.length>0){
                        var resultado = json.response[0];
                        cobro = resultado.cargo;
                    }

                    if(cobro>0){
                        var existe=false;
                        productos.forEach(function(pro){
                            if(pro.codpro=='WW00008'){
                                existe=true;
                            }
                        });

                        if(existe==false){
                            var codpro='WW00008';
                            var precio=cobro;
                            var costo=cobro;
                            var cantidad=1;
                            var suc=0;
                            var rec=0;
                            productos.push({codpro, precio, costo, cantidad, suc, rec});
                        }
                    }

                    //Actualiza cotizacion
					var parametros;
					parametros = {
                    	rutcli: rutcli,
                    	codemp: codemp,
                    	productos: productos,
                    	numcot: from_coti,
                    	ejecutivo: ejecutivo,
						coddir: direccion
                    };
                    $.ajax({
                        xhrFields: {
                            withCredentials: true
                        },
                        headers: {
                            'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                        },
                        data:parametros,
                        url: '{{ env("URL_API") }}/api/cotizacion/update-cotizacion',
                        type: 'POST',
                        cache: false,
                        datatype: 'json',
                        async: true,
                        beforeSend: function () {
                            document.getElementById("pre_loader").className = "loader animation-start";
                        },
                        success: function (json) {
                            // console.log(json);
                            if(json.code==200){
                                num_coti_guardada = json.response.numcot;
                                invocaRegistroBitacora('actualizarcoti', '', '', num_coti_guardada);
                                swal("La cotizaci贸n N掳 " + num_coti_guardada + " ha sido actualizada!", {
                                    icon: "success",
                                }).then((ok) => {
                                    if (ok) {
                                        //location.reload();
                                        num_coti_guardada = json.response.numcot;
                                        document.getElementById("pre_loader").className = "loader animation-start d-none";
                                        afterSave();
                                    }
                                });
                            }
                            else{
                                var nFrom = "top";
                                var nAlign = "center";
                                var nIcons = "fa fa-times";
                                var nType = "danger";
                                var nAnimIn = "animated bounceInRight";
                                var nAnimOut = "animated bounceOutRight";
                                var title = "";
                                var message = json.response.respuesta + ' | Cotizaci贸n: ' + json.response.numcot;

                                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                                num_coti_guardada = json.response.numcot;
                                document.getElementById("pre_loader").className = "loader animation-start d-none";
                                //afterSave();
                            }
                        },
                        error: function(e){
                            var code = e.status;
                            var text = e.statusText;
                            registroError(ejecutivo, moduleid_global, 'actualizar cotizacion', code, text);

                            var nFrom = "top";
                            var nAlign = "center";
                            var nIcons = "fa fa-times";
                            var nType = "danger";
                            var nAnimIn = "animated bounceInRight";
                            var nAnimOut = "animated bounceOutRight";
                            var title = "";
                            var message = text;

                            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        }
                    });
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
        });


    }
	
	function muestraCalendario(){
		var tipoNota = document.getElementsByName('select_tiponota')[0].value;
		if(tipoNota == 19){ //Si nota es cajas
			$("#modal-calendario").modal("show");
			$("#cerrarCalendario").click(function(){
				if($("#fechaCajas").val()!=""){
					$("#modal-calendario").modal("hide");
					invocaGuardarNotaVenta();
				}
			});
			$.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            //data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/getCalendario',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){
					var fechaCaja = "";
					var textoCantidadCajas = "";
                    $('#fechaCajas').datepicker("destroy");
					$('#fechaCajas').datepicker({
                        dateFormat: "dd/mm/yy",
                        dayNames: diasSemana,
                        dayNamesMin: diasSemanaCorto,
                        monthNames: mesesAgno,
    					beforeShowDay: function(date){
							fechaCaja = "";
							textoCantidadCajas = "";
        					var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
							json.response.forEach(function(fecha){
								if(fecha.fecha == string){
									fechaCaja = fecha.fecha;
									textoCantidadCajas = "Normal:"+ fecha.disponible_nor + ", Especial:"+fecha.disponible_esp;
								}
							});
							return [ fechaCaja != "", "", textoCantidadCajas]
					    }
					});					
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
        	});
		}
		else{
			if(tipoNota == 23){ //si nota es faltante
				$("#infoRutcli").text(document.getElementById('cli_input_rut').value);
				$("#infoCencos").text(document.getElementsByName('select_ceco')[0].value);
				var direccionSeleccionada = document.getElementsByName('select_direc')[0];
				$("#infoDirdes").text(direccionSeleccionada.options[direccionSeleccionada.selectedIndex].text);
				$("#modal-faltante").modal("show");
				$("#cerrarFaltante").click(function(){
					if($("#folio_faltante").val()!=""){
						$("#modal-faltante").modal("hide");
						invocaPromesaDespacho();
					}
				});
			}
			else{ //si es nota de otro tipo
                invocaPromesaDespacho();
			}
		}
	}

    function invocaPromesaDespacho(){
        //cerrar calendario cajas
        $("#cerrarCalendario").click(function(){});
        if(from_nota == ""){ //si no se edita nota
            //abrir modal calendario promesa entrega
            $("#modal-promesa").modal("show");
            //cargamos datos promesa
            var direccionSeleccionada = document.getElementsByName('select_direc')[0].value;
            var parametros = {
                    "rutcli": rut_cliente,
                    "coddir": direccionSeleccionada
                };
            $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/getPromesaDespacho',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                        document.getElementById("pre_loader").className = "loader animation-start";
                    },
            success: function (json) {
                if(json.code==200){
                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                    var fechaCaja = "";
                    var textoCantidadCajas = "";
                    $('#fechaPromesa').datepicker("destroy");
                    $('#fechaPromesa').datepicker({
                        dateFormat: "dd/mm/yy",
                        dayNames: diasSemana,
                        dayNamesMin: diasSemanaCorto,
                        monthNames: mesesAgno,
                        beforeShowDay: function(date){
                            fechaCaja = "";
                            textoCantidadCajas = "";
                            var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                            json.response.forEach(function(fecha){
                                if(fecha.fecha == string){
                                    fechaCaja = fecha.fecha;
                                    textoCantidadCajas = "Fecha disponible";/*"Normal:"+ fecha.disponible_nor + ", Especial:"+fecha.disponible_esp;*/
                                }
                            });
                            return [ fechaCaja != "", "", textoCantidadCajas]
                        }
                    });                 
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca valor cobro logistico', code, text);
            }
            });
            $("#cerrarPromesa").click(function(){
                if($("#fechaPromesa").val()!=""){
                    $("#modal-promesa").modal("hide");
                    invocaGuardarNotaVenta();
                }
            });
        }
        else{
            invocaGuardarNotaVenta();
        }
    }

	function generaTablaSwal(eventoTabla, json){
		if(eventoTabla == 'notaExitosa'){
			var nvts = "";
        	var tablaContenido = document.createElement("table");
			tablaContenido.className = "table";
			var headerContenido = document.createElement("thead");
			var filaHeader = document.createElement("tr");
			var headerNvt = document.createElement("th");
			headerNvt.innerText = "Numero nota";
			var headerDescrip = document.createElement("th");
			headerDescrip.innerText = "Tipo Nota"; 
			var headerSucursal = document.createElement("th");
			headerSucursal.innerText = "Sucursal de despacho"; 
			var headerFolioSAP = document.createElement("th");
			headerFolioSAP.innerText = "Folio SAP"; 
			var headerMensajeSAP = document.createElement("th");
			headerMensajeSAP.innerText = "Estado SAP";
								
			filaHeader.appendChild(headerNvt);
			filaHeader.appendChild(headerDescrip);
			filaHeader.appendChild(headerSucursal);
			filaHeader.appendChild(headerFolioSAP);
			filaHeader.appendChild(headerMensajeSAP);
			headerContenido.appendChild(filaHeader);
			tablaContenido.appendChild(headerContenido);
								
			var bodyContenido = document.createElement("tbody");
								
        	for (var i = json.response.nvts.length - 1; i >= 0; i--) {
				var filaContenido = document.createElement("tr");
				var celdaNvt = document.createElement("td");
				celdaNvt.innerText = json.response.nvts[i].numnvt;
				celdaNvt.className = "text-nowrap text-left";
				var celdaDescrip = document.createElement("td");
				celdaDescrip.innerText = json.response.nvts[i].descripcion;
				celdaDescrip.className = "text-nowrap text-left";
				var celdaSucursal = document.createElement("td");
				celdaSucursal.innerText = json.response.nvts[i].sucursal;
				celdaSucursal.className = "text-nowrap text-left";
				var celdaFolioSAP = document.createElement("td");
				celdaFolioSAP.innerText = json.response.nvts[i].folio_sap;
				celdaFolioSAP.className = "text-nowrap text-left";
				var celdaMensajeSAP = document.createElement("td");
				celdaMensajeSAP.innerText = json.response.nvts[i].mensaje_sap;
				celdaMensajeSAP.className = "text-nowrap text-left";
				filaContenido.appendChild(celdaNvt);
				filaContenido.appendChild(celdaDescrip);
				filaContenido.appendChild(celdaSucursal);
				filaContenido.appendChild(celdaFolioSAP);
				filaContenido.appendChild(celdaMensajeSAP);
				bodyContenido.appendChild(filaContenido);
        		//nvts += json.response.nvts[i].numnvt + " "+ json.response.nvts[i].descripcion  + ", Sucursal: " + json.response.nvts[i].sucursal+ ", SAP: "+ json.response.nvts[i].folio_sap + " " + json.response.nvts[i].mensaje_sap +"\n";
        	}
			tablaContenido.appendChild(bodyContenido);
			var divContenido = document.createElement("div");
			divContenido.className = "col-sm-12 table-responsive";
			var divRowContenido = document.createElement("div");
			divRowContenido.className = "row";
			divContenido.appendChild(tablaContenido);
			divRowContenido.appendChild(divContenido);
			return divRowContenido;
		}
		if(eventoTabla == 'confirmaAntofagasta'){
			var tablaContenido = document.createElement("table");
			tablaContenido.className = "table";
			var headerContenido = document.createElement("thead");
			var filaHeader = document.createElement("tr");

			var headerCodigo = document.createElement("th");
			headerCodigo.innerText = "C贸digo";
			var headerDescrip = document.createElement("th");
			headerDescrip.innerText = "Descripci贸n"; 
			var headerAnto = document.createElement("th");
			headerAnto.innerText = "Antofagasta"; 
			var headerStgo = document.createElement("th");
			headerStgo.innerText = "Santiago"; 
								
			filaHeader.appendChild(headerCodigo);
			filaHeader.appendChild(headerDescrip);
			filaHeader.appendChild(headerAnto);
			filaHeader.appendChild(headerStgo);
			headerContenido.appendChild(filaHeader);
			tablaContenido.appendChild(headerContenido);
								
			var bodyContenido = document.createElement("tbody");
								
        	for (var i = 0; i <= json.response.respuesta.notas.length - 1; i++) {
					var filaContenido = document.createElement("tr");
					var celdaCodigo = document.createElement("td");
					celdaCodigo.innerText = json.response.respuesta.notas[i].codpro;
					celdaCodigo.className = "text-nowrap text-left";
					var celdaDescrip = document.createElement("td");
					celdaDescrip.innerText = json.response.respuesta.notas[i].descrip;
					celdaDescrip.className = "text-nowrap text-left";
					var celdaAnto = document.createElement("td");
					celdaAnto.innerText = json.response.respuesta.notas[i].anto;
					celdaAnto.className = "text-nowrap text-left";
					var celdaStgo = document.createElement("td");
					celdaStgo.innerText = json.response.respuesta.notas[i].stgo;
					celdaStgo.className = "text-nowrap text-left";
					//var celdaMensajeSAP = document.createElement("td");
					//celdaMensajeSAP.innerText = json.response.nvts[i].mensaje_sap;
					//celdaMensajeSAP.className = "text-nowrap";
					filaContenido.appendChild(celdaCodigo);
					filaContenido.appendChild(celdaDescrip);
					filaContenido.appendChild(celdaAnto);
					filaContenido.appendChild(celdaStgo);
					bodyContenido.appendChild(filaContenido);
        	}
			tablaContenido.appendChild(bodyContenido);
			var divContenido = document.createElement("div");
			divContenido.className = "col-sm-12 table-responsive";
			var divRowContenido = document.createElement("div");
			divRowContenido.className = "row";
			divContenido.appendChild(tablaContenido);
			divRowContenido.appendChild(divContenido);
			return divRowContenido;
		}
		if(eventoTabla == "datosNota"){
			var tablaContenido = document.createElement("table");
			tablaContenido.className = "table";
			var headerContenido = document.createElement("thead");
			var filaHeader = document.createElement("tr");

			var headerRut = document.createElement("th");
			headerRut.innerText = "RUT";
			var headerRazons = document.createElement("th");
			headerRazons.innerText = "Raz贸n social"; 
			var headerCencos = document.createElement("th");
			headerCencos.innerText = "Centro de Costo"; 
			var headerDocumentadoCon = document.createElement("th");
			headerDocumentadoCon.innerText = "Documentada con";
			var headerTipoNota = document.createElement("th");
			headerTipoNota.innerText = "Tipo Nota";
			var headerDirdes = document.createElement("th");
			headerDirdes.innerText = "Direcci贸n de despacho";
			var headerOrdenCompra = document.createElement("th");
			headerOrdenCompra.innerText = "Orden de Compra";
			var headerObserv = document.createElement("th");
			headerObserv.innerText = "Observaci贸n";
			var headerEntregarA = document.createElement("th");
			headerEntregarA.innerText = "Entregar A";
								
			filaHeader.appendChild(headerRut);
			filaHeader.appendChild(headerRazons);
			filaHeader.appendChild(headerCencos);
			filaHeader.appendChild(headerDocumentadoCon);
			filaHeader.appendChild(headerTipoNota);
			filaHeader.appendChild(headerDirdes);
			filaHeader.appendChild(headerOrdenCompra);
			filaHeader.appendChild(headerObserv);
			filaHeader.appendChild(headerEntregarA);
			headerContenido.appendChild(filaHeader);
			tablaContenido.appendChild(headerContenido);
								
			var bodyContenido = document.createElement("tbody");
								
			var filaContenido = document.createElement("tr");
			
			var celdaRut = document.createElement("td");
			celdaRut.innerText = json.rutcli;
			var celdaRazons = document.createElement("td");
			celdaRazons.innerText = json.razons;
			celdaRazons.className = "text-nowrap text-left";
			var celdaCencos = document.createElement("td");
			celdaCencos.innerText = json.cencos;
			celdaCencos.className = "text-nowrap text-left";
			var celdaDocumentadoCon = document.createElement("td");
			celdaDocumentadoCon.innerText = json.documentadoCon;
			celdaDocumentadoCon.className = "text-nowrap text-left";
			var celdaTipoNota = document.createElement("td");
			celdaTipoNota.innerText = json.tipoNota;
			celdaTipoNota.className = "text-nowrap text-left";
			var celdaDirdes = document.createElement("td");
			celdaDirdes.innerText = json.dirdes;
			celdaDirdes.className = "text-nowrap text-left";
			var celdaOrdenCompra = document.createElement("td");
			celdaOrdenCompra.innerText = json.ordenCompra;
			celdaOrdenCompra.className = "text-nowrap text-left";
			var celdaObservacion = document.createElement("td");
			celdaObservacion.innerText = json.observ;
			celdaObservacion.className = "text-nowrap text-left";
			var celdaEntregarA = document.createElement("td");
			celdaEntregarA.innerText = json.entregarA;
			celdaEntregarA.className = "text-nowrap text-left";
			filaContenido.appendChild(celdaRut);
			filaContenido.appendChild(celdaRazons);
			filaContenido.appendChild(celdaCencos);
			filaContenido.appendChild(celdaDocumentadoCon);
			filaContenido.appendChild(celdaTipoNota);
			filaContenido.appendChild(celdaDirdes);
			filaContenido.appendChild(celdaOrdenCompra);
			filaContenido.appendChild(celdaObservacion);
			filaContenido.appendChild(celdaEntregarA);
			bodyContenido.appendChild(filaContenido);

			tablaContenido.appendChild(bodyContenido);
			var divContenido = document.createElement("div");
			divContenido.className = "col-sm-12 table-responsive";
			var divRowContenido = document.createElement("div");
			divRowContenido.className = "row";
			divContenido.appendChild(tablaContenido);
			divRowContenido.appendChild(divContenido);
			return divRowContenido;
		}
		if(eventoTabla == 'sinStockAnto'){
			var tablaContenido = document.createElement("table");
			tablaContenido.className = "table";
			var headerContenido = document.createElement("thead");
			var filaHeader = document.createElement("tr");

			var headerCodigo = document.createElement("th");
			headerCodigo.innerText = "C贸digo";
			var headerDescrip = document.createElement("th");
			headerDescrip.innerText = "Descripci贸n";				
			filaHeader.appendChild(headerCodigo);
			filaHeader.appendChild(headerDescrip);
			headerContenido.appendChild(filaHeader);
			tablaContenido.appendChild(headerContenido);
								
			var bodyContenido = document.createElement("tbody");
			for (var i = json.response.prodsSinStockAnto.length - 1; i >= 0; i--) {
				var filaContenido = document.createElement("tr");
				var celdaCodigo = document.createElement("td");
				celdaCodigo.innerText = json.response.prodsSinStockAnto[i].codpro;
				celdaCodigo.className = "text-nowrap text-left";
				var celdaDescrip = document.createElement("td");
				celdaDescrip.innerText = json.response.prodsSinStockAnto[i].descrip;
				celdaDescrip.className = "text-nowrap text-left";
				filaContenido.appendChild(celdaCodigo);
				filaContenido.appendChild(celdaDescrip);
				bodyContenido.appendChild(filaContenido);
			}
								
			tablaContenido.appendChild(bodyContenido);
			var divContenido = document.createElement("div");
			divContenido.className = "col-sm-12 table-responsive";
			var divRowContenido = document.createElement("div");
			divRowContenido.className = "row";
			divContenido.appendChild(tablaContenido);
			divRowContenido.appendChild(divContenido);
			return divRowContenido;
		}
	}
	
	function modalVerNota(numnvt){
		var parametros = {
							numnvt:numnvt,
							soloLectura: 1
						};
		$.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/get-cotizacion',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json) {
				var totgen = (parseInt(json.response.encabezado_coti[0].neto) + parseInt(json.response.encabezado_coti[0].iva));
				$("#modal_notaventa_rutcli").text(json.response.encabezado_coti[0].rutcli);
				$("#modal_notaventa_razons").text(json.response.encabezado_coti[0].razons);
				$("#modal_notaventa_cencos").text(json.response.encabezado_coti[0].desc_ceco);
				$("#modal_notaventa_totnet").text("$" + formatMoney(json.response.encabezado_coti[0].neto, 0, ',', '.'));
				$("#modal_notaventa_iva").text("$" + formatMoney(json.response.encabezado_coti[0].iva, 0, ',', '.'));
				$("#modal_notaventa_totgen").text("$" + formatMoney(totgen, 0, ',', '.'));
				$("#modal_notaventa_docfut").text(json.response.encabezado_coti[0].docfut);
				$("#modal_notaventa_tiponota").text(json.response.encabezado_coti[0].tiponota);
				$("#modal_notaventa_dirdes").text(json.response.encabezado_coti[0].dirdes);
				$("#modal_notaventa_ordencompra").text(json.response.encabezado_coti[0].ordencompra);
				$("#modal_notaventa_observ").text(json.response.encabezado_coti[0].observ);
				$("#modal_notaventa_entregara").text(json.response.encabezado_coti[0].entregara);
				$("#modal_notaventa").modal("show");
				var bodyContenido = document.getElementById("modal_datosprod_productos");
				for(var i = 0; i< json.response.detalle_coti.length; i++){
					var totnet = parseInt(json.response.detalle_coti[i].precio) * parseInt(json.response.detalle_coti[i].cantid);
					var filaContenido = document.createElement("tr");
					var celdaCodpro = document.createElement("td");
					celdaCodpro.innerText = json.response.detalle_coti[i].codpro;
					celdaCodpro.className = "text-left";
					var celdaDespro = document.createElement("td");
					celdaDespro.innerText = json.response.detalle_coti[i].despro;
					celdaDespro.className = "text-left";
					var celdaCantid = document.createElement("td");
					celdaCantid.innerText = json.response.detalle_coti[i].cantid;
					celdaCantid.className = "text-left";
					var celdaPrecio = document.createElement("td");
					celdaPrecio.innerText = "$" + formatMoney(json.response.detalle_coti[i].precio, 0, ',', '.');
					celdaPrecio.className = "text-left";
					var celdaTotnet = document.createElement("td");
					celdaTotnet .innerText = "$" + formatMoney(totnet, 0, ',', '.');
					celdaTotnet .className = "text-left";
					filaContenido.appendChild(celdaCodpro);
					filaContenido.appendChild(celdaDespro);
					filaContenido.appendChild(celdaCantid);
					filaContenido.appendChild(celdaPrecio);
					filaContenido.appendChild(celdaTotnet);
					bodyContenido.appendChild(filaContenido);
				}
				document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener cotizacion', code, text);
            }			
		});
		
	}
	
	function getDocumentos(nota) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getDocumentos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    document.getElementById("pre_loader").className = "loader animation-start";
                },
                success: function (json) {
                    console.log(json.response);
                    if (json.response.length > 0) {
                        text = '';
                        tabla = '<table class="table table-striped table-bordered nowrap dataTable text-right" style="margin: 0 auto;">\n' +
                                '    <thead>\n' +
                                '        <tr class="table-inverse">\n' +
                                '            <th colspan="1">Nota de venta</th>\n' +
                                '            <th colspan="1">Neto nota</th>\n' +
                                '            <th colspan="1">Orden de Compra</th>\n' +
                                '            <th colspan="1">Guia despacho</th>\n' +
                                '            <th colspan="1">Neto guia</th>\n' +
                                '            <th colspan="1">Factura</th>\n' +
                                '            <th colspan="1">Neto Factura</th>\n' +
                                '        </tr>\n' +
                                '    </thead>\n' +
                                '    <body>';
                        for (i = 0; i < json.response.length; i++) {
                            text = text + '<tr>\n' +
                                    '<td>' + json.response[i].numnvt + '</td>\n' +
                                    '<td>' + '$' + formatMoney(json.response[i].totnet, 0, ',', '.') + '</td>\n' +
                                    '<td>' + json.response[i].docaso + '</td>\n' +
                                    '<td>' + json.response[i].numgui + '</td>\n' +
                                    '<td>' + '$' + formatMoney(json.response[i].total_guia, 0, ',', '.') + '</td>\n' +
                                    '<td>' + json.response[i].numfac + '</td>\n' +
                                    '<td>' + '$' + formatMoney(json.response[i].total_factura, 0, ',', '.') + '</td>\n' +
                                   '</tr>';
                        };
                        tabla = tabla +  text + '</body>\n' +
                            '</table>';
                            
                        document.getElementById("docRel").innerHTML = tabla;
						document.getElementById("pre_loader").className = "loader animation-start d-none";
                        $("#modal_documentos").modal("show");
                    } else {
                        var nFrom = "top";
                        var nAlign = "center";
                        var nIcons = "fa fa-times";
                        var nType = "danger";
                        var nAnimIn = "animated bounceInRight";
                        var nAnimOut = "animated bounceOutRight";
                        var title = "";
                        var message = "No tiene documentos asociados";

                        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                        document.getElementById("pre_loader").className = "loader animation-start d-none";
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }
	
	@if(env('ENV_DATABASE')!='prod')
        @verbatim
	function askAudio(){
		audioParado = false;
		snd1.play(); snd2.play();
		setTimeout(function(){
			if(!audioParado){
				snd3.play();
			}
		}, 3400);
	}
	
	function successAudio(){
		stopAudio();
		snd4.play();
		snd6.play();
	}
	
	function failAudio(){
		stopAudio();
		snd5.play();
	}
	
	function stopAudio(){
		audioParado = true;
		snd2.pause();
		snd2.currentTime = 0;
		snd3.pause();
		snd3.currentTime = 0;
	}
    @endverbatim
	@else
        @verbatim
		function askAudio(){}
		function successAudio(){}
		function failAudio(){}
		function stopAudio(){}
        @endverbatim
	@endif

    function invocaSubirOrdenCompra(form){
        form.append("rutcli", rut_cliente);
        var proData = false, contentTy = false;
        $.ajax({
            xhrFields: {
                    withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: form,
            cache: false,
            processData: proData,
            contentType: contentTy,
            url: '{{ env("URL_API") }}/api/cotizacion/subir-ordencompra',
            type: 'POST',
            beforeSend: function () {
                document.getElementById("pre_loader").className = "loader animation-start";
            },
            success: function (json){
                document.getElementById("pre_loader").className = "loader animation-start d-none";
                $("#token_orden").val(json.response.tokenarchivo);
                notify2("top", "center", "fa fa-times", "success",
                "animated bounceInRight", "animated bounceOutRight", "", json.response.mensaje);
            }
        });
    }
    /* fin:GeneraCotizacion */
</script>
