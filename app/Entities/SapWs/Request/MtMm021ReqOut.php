<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 12:22
 */

namespace App\Entities\SapWs\Request;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;

/* SAP GET STOCK */
class MtMm021ReqOut extends ReqOut
{
    private $material;
    private $materiales;
    private $centro;
    private $almacen;

    public function __construct()
    {
        $this->setHeaderHosts((env('DATABASE_ENV') == 'prod') ? env('WS_SAP_PROD') : env('WS_SAP_DEV'));
        $this->setHeaderUrl($this->getHeaderHosts().'/XISOAPAdapter/MessageServlet');
        $this->setHeaderMethod('POST');
        $this->setHeaderQueryString(array(
            'senderParty' => '',
            'senderService' => 'BC_TELEMARK',
            'receiverParty' => '',
            'receiverService' => '',
            'interface' => 'SI_MM021_SYN_OUT',
            'interfaceNamespace' => 'urn:swap:telemarke:s4h:legado_ventas:MM021'
        ));
        $this->setHeaderUrlfull($this->getHeaderHosts().'/XISOAPAdapter/MessageServlet'
            . '?senderParty=&senderService=BC_TELEMARK&receiverParty=&receiverService=&interface=SI_MM021_SYN_OUT&interfaceNamespace=urn:swap:telemarke:s4h:legado_ventas:MM021');

        $this->materiales = array();
    }

    public function getMaterial()
    {
        // 18 Characters
        return ($this->material) ? substr('000000000000000000' . $this->material, -18) : null;
    }

    public function getMateriales()
    {
        return $this->materiales;
    }

    public function setMaterial($material)
    {
        $this->material = $material;
    }

    public function setMateriales(array $materiales)
    {
        $this->materiales = $materiales;
    }

    public function getCentro()
    {
        // 4 Characters
        return ($this->centro) ? substr('000000000000000000' . $this->centro, -4) : null;
    }

    public function setCentro($centro)
    {
        $this->centro = $centro;
    }

    public function getAlmacen()
    {
        // 4 Characters
        return ($this->almacen) ? substr('000000000000000000' . $this->almacen, -4) : null;
    }

    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;
    }

    public function getRequestXml()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'
            . ' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" '
            . ' xmlns:urn="urn:swap:telemarke:s4h:legado_ventas:MM021"> '
            . ' <soapenv:Header /> '
            . ' <soapenv:Body> '
            . ' <urn:MT_MM021_REQ_OUT> '
            . ' <MATERIAL>'
            . ' <MATNR>' . $this->getMaterial() . '</MATNR> '
            . ' </MATERIAL>'
            . ' <CENTRO>'
            . ' <WERKS>' . $this->getCentro() . '</WERKS> '
            . ' </CENTRO> '
            . ' <ALMACEN> '
            . ' <LGORT>' . $this->getAlmacen() . '</LGORT> '
            . ' </ALMACEN> '
            . ' </urn:MT_MM021_REQ_OUT> '
            . ' </soapenv:Body> '
            . ' </soapenv:Envelope>';

        return $xml;
    }

    public function getRequestXmlMultipleMaterial()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'
            . ' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" '
            . ' xmlns:urn="urn:swap:telemarke:s4h:legado_ventas:MM021"> '
            . ' <soapenv:Header /> '
            . ' <soapenv:Body> '
            . ' <urn:MT_MM021_REQ_OUT> ';



        foreach ($this->getMateriales() as $material) {
            $xml .= ' <MATERIAL>'
                . ' <MATNR>' . substr('0000' . $material, -18) . '</MATNR> '
                . ' </MATERIAL>';
        }

        $xml .= ' <CENTRO>'
            . ' <WERKS>' . $this->getCentro() . '</WERKS> '
            . ' </CENTRO> '
            . ' <ALMACEN> '
            . ' <LGORT>' . $this->getAlmacen() . '</LGORT> '
            . ' </ALMACEN> '
            . ' </urn:MT_MM021_REQ_OUT> '
            . ' </soapenv:Body> '
            . ' </soapenv:Envelope>';

        return $xml;
    }


}