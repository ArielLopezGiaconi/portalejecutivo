<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 20/09/2018
 * Time: 17:03
 */

namespace App\PDO\WebServices;

use App\Entities\SapWs\Request\MtMm024ReqOut;
use App\Entities\SapWs\Response\MtMm024ResOut;
use \DB;
use Illuminate\Support\Collection;

class PriceWS extends WS
{
    public function setPriceFromVeracore($in_material, $in_proveedor, $in_org_compra, $in_precio)
    {
        $webService = new MtMm024ReqOut();
        $webService->setMaterial($in_material);
        $webService->setCodProveedor($in_proveedor);
        $webService->setCodOrgCompra($in_org_compra);
        $webService->setPrecioCompra($in_precio);
        $response = $this->consumeWSCurl(
            $webService->getRequestXml(),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM024'
        );

        $responseXml = new MtMm024ResOut($response);
        return $responseXml;
    }

    public function setAllPriceFromVeracore(array $in_data)
    {
        $webService = new MtMm024ReqOut();
        $response = $this->consumeWSCurl(
            $webService->getRequestXmlMultiple($in_data),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM024'
        );

        $responseXml = new MtMm024ResOut($response);
        return $responseXml;
    }
}