<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use \DB;

class ReCanProdPDO extends Model
{
    public static function getPrecioMinimo($codcnl, $codpro, $codemp = 3)
    {
        $sql = "select nvl(prefij,0) as prefij from re_canprod where codpro = :codpro 
        and codcnl = :codcnl 
        and codemp = :codemp";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codpro' => $codpro,
            'codcnl' => $codcnl,
            'codemp' => $codemp
        ]);
        return $resultado[0]->prefij;
    }
}
