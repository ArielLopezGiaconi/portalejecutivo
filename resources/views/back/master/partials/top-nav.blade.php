@php
    /* Obtener foto perfil */
     $rutusuario = Session::get('usuario')->get('rutusu');
     if(is_null(env('CUSTOM_PUBLIC_PATH'))) {
        $fotoperfil = public_path().'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
        }else {
            $fotoperfil = env('CUSTOM_PUBLIC_PATH').'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
        }
        
        if(file_exists($fotoperfil)){
            $fotoperfil = asset('assets/images/fotoperfil/'.$rutusuario.'.jpg');
        }
        else{
            $fotoperfil = asset('assets/images/default1.jpg');
        }
@endphp
@include('back.others.js.portal')
@if(env('DATABASE_ENV') != 'prod')
	@include('back.others.js.notificaciones')
@endif
<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">

        <div class="navbar-logo">
            <a class="mobile-menu" id="mobile-collapse">
                <i class="feather icon-menu"></i>
            </a>
            <a href="{{ url('portada') }}">
                <img class="img-fluid" src="{{ asset('assets/images/logo-horizontal2.png') }}" alt="Theme-Logo" style="width:160px;height: 30px;">
            </a>
            <a class="mobile-options">
                <i class="feather icon-more-horizontal"></i>
            </a>
        </div>
        <div class="navbar-container container-fluid">
            <ul class="nav-left">
				@if(env('DATABASE_ENV') != 'prod')
					<li id="entornoqa" class="m-t-10 m-l-10"><h2>ENTORNO PRUEBA</h2></li>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r121/three.min.js"></script>
					<script src="https://cdn.jsdelivr.net/npm/vanta@latest/dist/vanta.net.min.js"></script>
					<script>
						VANTA.NET({
							el: ".navbar",
  							mouseControls: true,
 	 						touchControls: true,
  							gyroControls: false,
							minHeight: 56,
 	 						scale: 0.50,
  							scaleMobile: 1.00,
							color: 0x404e67,
							backgroundColor: 0xffffff
							})
					</script>
				@endif            </ul>
            <ul class="nav-right">
				@if(env('DATABASE_ENV') != 'prod')
                <!-- Notificaciones: pr�xima funcionalidad del portal (etapa 2) Bienvenidos a la etapa 2! :D -->
                <li class="header-notification">
                    <div id="capaNotificaciones" class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown" id="btnNotificaciones">
                            <i class="feather icon-bell"></i>
                            <span id="qNotificaciones" class="badge bg-c-pink">0</span>
                        </div>
                        <ul id="listaNotificaciones" class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                            <li>
                                <h6>Notificaciones</h6>
                                {{--<label class="label label-danger">Nuevas</label>--}}
                            </li>
                        </ul>
                    </div>
                </li>
                {{--                        <li class="header-notification">--}}
                {{--                            <div class="dropdown-primary dropdown">--}}
                {{--                                <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">--}}
                {{--                                    <i class="feather icon-message-square"></i>--}}
                {{--                                    <span class="badge bg-c-green">3</span>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </li>--}}
				@endif            
                <li class="user-profile header-notification">
					@if(env('DATABASE_ENV') != 'prod')
                    <img id="support" src="assets/img/suppot.png" style="border-radius: 50px;" class="" onclick="Abrir()">
					@endif
                    <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <img id="fotoicono" src="{{ $fotoperfil }}" style="border-radius: 50px;" class="img-radius" alt="User-Profile-Image">
                            <span>{{ ucwords(strtolower(Session::get('usuario')->get('nombre').' '.Session::get('usuario')->get('apepat'))) }}</span>
                            <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                            <!-- Próximas opciones del menú (para la etapa 2 del proyecto) **Atte->JLO** -->
{{--                            <li>--}}
{{--                                <a href="#!">--}}
{{--                                    <i class="feather icon-settings"></i> Configuración--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="user-profile.htm">--}}
{{--                                    <i class="feather icon-user"></i> Mi perfil--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="email-inbox.htm">--}}
{{--                                    <i class="feather icon-message-square"></i> Mis mensajes--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="email-inbox.htm">--}}
{{--                                    <i class="feather icon-check-circle"></i> Mis tareas--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="email-inbox.htm">--}}
{{--                                    <i class="feather icon-bookmark"></i> Mis notas--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <input style="display: none" type="text" name="{{ $rutusuario }}" id="rutusu" value="{{ $rutusuario }}">
                            <input style="display: none" type="text" name="{{ $fotoperfil }}" id="linkfoto" value="{{ $fotoperfil }}">
                            <li>
                                <a id="cambiaFoto" onclick="CargaFoto()">
                                    <form method="post" action="" enctype="multipart/form-data">
                                        <i class="feather icofont icofont-ui-user" ></i> Cambiar Foto    
                                        <input style="display: none" type="file" name="image" id="image">
                                        <input style="display: none" class="upload" id="enviaFoto" type="submit">
                                    </form>
                                </a>
                            </li>
                            <li>
                                <a onclick="CuadroCabiarClave()">
                                    <i class="feather icofont icofont-ui-password" ></i> Cambiar contraseña
                                </a>
                            </li>
                            <li>
                                <a id="btn_salir" href="{{ url('logout') }}">
                                    <i class="feather icon-log-out"></i> Salir del portal
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
</nav>

<i id="cuadroConfirmacion" class="fa fa-info-circle" data-toggle="modal" data-target="#confirmacion" style="display: none"></i>

<div id="confirmacion" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <h2 class="modal-title card-block text-center">Atención</h2>
            <div class="card-block text-center">
                <h4 class="modal-title">Al realizar el cambio de contraseña se desconectara del portal</h4>
                <h4 class="modal-title">Guarde todos los cambios antes de continuar</h4>
                <h4 class="modal-title">¿Desea realizar el cambio de contraseña?</h4>
            </div>
            <button type="button" name="next" class="btn btn-danger next pull-right" value="Next" onclick="CambioPagina(0)" data-dismiss="modal">No</button>
            <button type="button" name="next" class="btn btn-primary next pull-right" value="Next" onclick="CambioPagina(5)" data-dismiss="modal">Si</button>
        </div>
    </div>
</div>

<i id="cuadroClave" class="fa fa-info-circle" data-toggle="modal" data-target="#cambioClave" style="display: none"></i>

<div id="cambioClave" class="modal fade" role="dialog">
    <div class="modal-dialog modal-mg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="row">
                <div class="col-lg-12">
                    <!-- tab header start -->
                    <div class="tab-header card">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                            <li class="nav-item col-lg-4">
                                <a id="Pagina1" class="nav-link active" data-toggle="tab" href="#personal" role="tab" aria-expanded="true">Paso 1</a>
                                <div class="slide" style="width: 100%"></div>
                            </li>
                            <li class="nav-item col-lg-4">
                                <a id="Pagina2" class="nav-link" data-toggle="tab" href="#binfo" role="tab" aria-expanded="false">Paso 2</a>
                                <div class="slide" style="width: 100%"></div>
                            </li>
                            <li class="nav-item col-lg-4">
                                <a id="Pagina3" class="nav-link" data-toggle="tab" href="#contacts" role="tab" aria-expanded="false">Paso 3</a>
                                <div class="slide" style="width: 100%"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- tab header end -->
                    <!-- tab content start -->
                    <div class="tab-content">
                        <input id="paginaClave" type="text" value="1" style="display: none">
                        <!-- tab panel personal start -->
                        <div class="tab-pane active" id="personal" role="tabpanel" aria-expanded="true">
                            <!-- personal card start -->
                            <div class="card">
                                <div class="card-header text-center">
                                    <h5 class="card-header-text">Valida contraseña actual</h5>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Contraseña Actual</label>
                                            <div class="form-group pass_show"> 
                                                <i id="bueno" class="icofont icofont-checked f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <i id="malo" class="icofont icofont-close-squared-alt f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <input id="claveOld" type="password" value="" class="form-control" placeholder="Current Password"> 
                                            </div>
                                            <div id="errorClave" class="form-group bg-c-pink text-center text-white" style="display: none"> <label style="margin-top: 10px;">No corresponde a la contraseña registrada</label> </div>
                                        </div>  
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <button type="button" name="next" class="btn btn-primary next pull-right" value="Next" onclick="CambioPagina(2)">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- personal card end-->
                        </div>
                        <!-- tab pane personal end -->
                        <!-- tab pane info start -->
                        <div class="tab-pane" id="binfo" role="tabpanel" aria-expanded="false">
                            <!-- info card start -->
                            <div class="card">
                                <div class="card-header text-center">
                                    <h5 class="card-header-text">Nueva Contraseña</h5>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Nueva contraseña</label>
                                            <div class="form-group pass_show"> 
                                                <i id="bueno1" class="icofont icofont-checked f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <i id="malo1" class="icofont icofont-close-squared-alt f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <input onkeyup="ValidaNewContraseñaNew()" type="password" id="claveNew1"  value="" class="form-control" placeholder="New Password" > 
                                            </div> 
                                            <label>Confirma contraseña</label>
                                            <div class="form-group pass_show"> 
                                                <i id="bueno2" class="icofont icofont-checked f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <i id="malo2" class="icofont icofont-close-squared-alt f-32" style="position: absolute; right: 20px; display: none"></i>
                                                <input onkeyup="ValidaNewContraseñaNew()" id="claveNew" type="password" value="" class="form-control" placeholder="Confirm Password"> 
                                            </div> 
                                        </div>  
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <button type="button" name="previous" class="btn btn-inverse btn-outline-inverse previous pull-left" value="Previous" onclick="previos(1)">Previous</button>
                                            <button type="button" name="next" class="btn btn-primary next pull-right" value="Next" onclick="CambioPagina(3)">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- info card end -->
                        </div>
                        <!-- tab pane info end -->
                        <!-- tab pane contact start -->
                        <div class="tab-pane" id="contacts" role="tabpanel" aria-expanded="false">
                            <div class="card">
                                <div class="card-header text-center">
                                    <h4 class="card-header-text">Información</h4>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="text"><b>Duración de la contraseña</b></label>
                                            <div class="form-group pass_show"> 
                                                <p>La duración de la contraseña sera de un total de 60 días seguidos y al contar del día 61 su contraseña estara caducada</p>
                                            </div> 
                                            <label><b>Tolerancia</b></label>
                                            <div class="form-group pass_show"> 
                                                <p>La tolerancia de la contraseña sera de 10 días seguidos y esto permitira el ingreso al portal con la contraseña caducada</p>
                                            </div> 
                                            <label><b>Politica y seguridad</b></label>
                                            <div class="form-group pass_show"> 
                                                <label>Politica</label>
                                                <p>Según normas de la empresa la contraseña no podra ser dada a ningun usuario y tampoco debe ser pedira al usuario </p>
                                                <label>Seguridad</label>
                                                <p>No deje su contraseña guardada en lugares de facil acceso, no de su contraseña a compañeros y procure cambiar con periocidad su contraseña</p>
                                            </div> 
                                        </div>  
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <button type="button" name="previous" class="btn btn-inverse btn-outline-inverse previous pull-left" value="Previous" onclick="previos(2)">Previous</button>
                                            <button type="button" name="next" class="btn btn-primary next pull-right" value="Next" onclick="CambioPagina(4)" data-dismiss="modal">Confirmar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab pane contact end -->
                    </div>
                    <!-- tab content end -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="position: absolute; bottom: 0; right: 0;">
    <div  class="chat-box" style="margin-right: 0%;">
        <ul class="text-right boxs" >
            <li  id="chat" class="chat-single-box card-shadow bg-white active" data-id="1" style="width: 350px; display: none">
                <div class="had-container">
                    <div class="chat-header p-10 bg-gray">
                        <div class="user-info d-inline-block f-left">
                            <p>Soporte Dimerc</p>
                        </div>
                        <div class="box-tools d-inline-block">
                            <a href="#" onclick="minimiza()">
                                <i class="icofont icofont-minus f-20 m-r-10"></i>
                            </a>
                            <a href="#" onclick="cerrar()">
                                <i class="icofont icofont-close f-20"></i>
                            </a>
                        </div>
                    </div>
                    <div id="body" class="chat-body p-10" style="width: 350px">
                        <div id="bodyChat" class="message-scrooler">
                            {{-- <div class="messages">
                                <div class="message out no-avatar media">
                                    <div class="body media-body text-right p-l-50">
                                        <div style="background-color: rgb(0, 132, 255)" class="content msg-reply f-14 text-white">Good morning..</div>
                                        <div class="seen">
                                            <i class="icon-clock f-12 m-r-5 "></i>
                                            <span class="f-12">a few seconds ago </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="sender media-right friend-box">
                                        <a href="javascript:void(0);" title="Me">
                                            <img src="{{ $fotoperfil }}" class="img-chat-profile" alt="Me">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="messages">
                                <div class="message out no-avatar media">
                                    <div class="sender media-left friend-box">
                                        <a href="javascript:void(0);" title="Me">
                                            <img src="assets/img/suppot.png" class="img-chat-profile" alt="Me">
                                        </a>
                                    </div>
                                    <div class="pull-left">
                                        <div class="seen">
                                            <div style="background-color: rgb(0, 132, 255)" class="content msg-reply f-14 text-white text-left">ok..</div>
                                            <i class="icon-clock f-12 txt-muted"></i>
                                            <span class="f-12">a few seconds ago </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="chat-footer b-t-muted">
                        <div class="input-group write-msg">
                            <input id="newMessage" type="text" onkeypress="InputChatEnter(event)" class="form-control input-value" placeholder="Type a Message">
                            <span class="input-group-btn">
                                <button id="paper-btn" class="btn btn-primary" type="button" onclick="InputChat()">
                                    <i class="icofont icofont-paper-plane"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </li>
            <li id="chat_min" class="chat-single-box active" data-id="1" style="display: none">
                <div class="had-container" >
                    <div class="chat-header p-10 " style="bottom: 0; position: absolute; width: 350px; right: 15px; background-color: #404E67 !important;">
                        <div class="user-info d-inline-block f-left">
                            <p class="text-white">Soporte Dimerc</p>
                        </div>
                        <div class="box-tools ">
                            <a class="text-white" href="#" onclick="minimiza()">
                                <i class="icofont icofont-minus f-20 m-r-10"></i>
                            </a>
                            <a class="text-white" href="#" onclick="cerrar()">
                                <i class="icofont icofont-close f-20"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- Sidebar chat start -->
<div id="sidebar" class="users p-chat-user showChat">
    <div class="had-container">
        <div class="card card_main p-fixed users-main">
            <div class="user-box">
                <div class="chat-inner-header">
                    <div class="back_chatBox">
                        <div class="right-icon-control">
                            <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                            <div class="form-icon">
                                <i class="icofont icofont-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-friend-list">
                    <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                        <a class="media-left" href="#!">
                            <img class="media-object img-radius img-radius" src="{{ asset('assets/images/avatar-3.jpg') }}" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="f-13 chat-header">Josephin Doe</div>
                        </div>
                    </div>
                    <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                        <a class="media-left" href="#!">
                            <img class="media-object img-radius" src="{{ asset('assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="f-13 chat-header">Lary Doe</div>
                        </div>
                    </div>
                    <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                            <img class="media-object img-radius" src="{{ asset('assets/images/avatar-4.jpg') }}" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="f-13 chat-header">Alice</div>
                        </div>
                    </div>
                    <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                        <a class="media-left" href="#!">
                            <img class="media-object img-radius" src="{{ asset('assets/images/avatar-3.jpg') }}" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="f-13 chat-header">Alia</div>
                        </div>
                    </div>
                    <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                        <a class="media-left" href="#!">
                            <img class="media-object img-radius" src="{{ asset('assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="f-13 chat-header">Suzen</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sidebar inner chat start-->
<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="feather icon-chevron-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-radius img-radius m-t-5" src="{{ asset('assets/images/avatar-3.jpg') }}" alt="Generic placeholder image">
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-radius img-radius m-t-5" src="{{ asset('assets/images/avatar-4.jpg') }}" alt="Generic placeholder image">
            </a>
        </div>
    </div>
    <div class="chat-reply-box p-b-20">
        <div class="right-icon-control">
            <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
            <div class="form-icon">
                <i class="feather icon-navigation"></i>
            </div>
        </div>
    </div>
</div>
<!-- Sidebar inner chat end-->
<script>

    function CargaFoto() {
        document.getElementById("image").click(); 
    }

    $('#btn_salir').click(function(){
        /*
        Module: logout -> 14
        Action: cierra sesion -> 25
        */
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=14;
        var actionid=25;
        var oldvalue='';
        var newvalue='';
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);

    });

    function resizeImg(){
        logo = document.getElementById("fotoPerfil");
        logo.width = 200;
        logo.height = 800;
    }
</script>