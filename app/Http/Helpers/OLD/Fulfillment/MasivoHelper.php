<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 01/10/2018
 * Time: 11:56
 */

namespace App\Http\Helpers\Fulfillment;

use App\Helpers\GeneralHelper;
use App\Http\Controllers\Reportes\ConsumosController;
use App\Http\Controllers\Stock\StockController;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\MaBodegasPDO;
use App\PDO\Oracle\DMVentas\MaEmpresaPDO;
use App\PDO\Oracle\DMVentas\MaProductPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MasivoHelper extends GeneralHelper
{
    public function validarIngresoMasivo(Request $request)
    {

    }

    public function ingresarMasivo(Request $request)
    {

    }
}