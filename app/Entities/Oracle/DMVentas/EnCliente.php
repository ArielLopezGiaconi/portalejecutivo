<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 23-01-2018
 * Time: 10:12
 */

namespace App\Entities\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class EnCliente implements \JsonSerializable {
    private $aprcom;
    private $asegur;
    private $cencos;
    private $ciaseg;
    private $clacli;
    private $clasif_clie;
    private $clicot;
    private $cliente_top;
    private $codbco;
    private $codemp;
    private $codest;
    private $codgir;
    private $codsuc;
    private $codven;
    private $contelec;
    private $ctacopia;
    private $diacot;
    private $diagra;
    private $dias;
    private $digcli;
    private $emails;
    private $env_corr;
    private $exenta;
    private $feccam;
    private $fecconv;
    private $fecdes;
    private $fecha_desblo;
    private $fecmod;
    private $gberno;
    private $mail_elec;
    private $margen;
    private $max_bidones;
    private $monto_peso;
    private $monto_uf;
    private $negocio;
    private $numadm;
    private $numemp;
    private $razons;
    private $ruta;
    private $rutcli;
    private $subnegocio;
    private $tipcli;
    private $tipconv;
    private $tipdoc;
    private $tippag;
    private $unidad_pago;
    private $venweb;

    private $segmento_cliente;
    private $negocio_eerr;
    private $credis_cliente;
    private $forma_pago;
    private $plazo_pago;
    private $estado_cliente;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->aprcom = isset($registro->aprcom) ? $registro->aprcom : null;
            $this->asegur = isset($registro->asegur) ? $registro->asegur : null;
            $this->cencos = isset($registro->cencos) ? $registro->cencos : null;
            $this->ciaseg = isset($registro->ciaseg) ? $registro->ciaseg : null;
            $this->clacli = isset($registro->clacli) ? $registro->clacli : null;
            $this->clasif_clie = isset($registro->clasif_clie) ? $registro->clasif_clie : null;
            $this->clicot = isset($registro->clicot) ? $registro->clicot : null;
            $this->cliente_top = isset($registro->cliente_top) ? $registro->cliente_top : null;
            $this->codbco = isset($registro->codbco) ? $registro->codbco : null;
            $this->codemp = isset($registro->codemp) ? $registro->codemp : null;
            $this->codest = isset($registro->codest) ? $registro->codest : null;
            $this->codgir = isset($registro->codgir) ? $registro->codgir : null;
            $this->codsuc = isset($registro->codsuc) ? $registro->codsuc : null;
            $this->codven = isset($registro->codven) ? $registro->codven : null;
            $this->contelec = isset($registro->contelec) ? $registro->contelec : null;
            $this->ctacopia = isset($registro->ctacopia) ? $registro->ctacopia : null;
            $this->diacot = isset($registro->diacot) ? $registro->diacot : null;
            $this->diagra = isset($registro->diagra) ? $registro->diagra : null;
            $this->dias = isset($registro->dias) ? $registro->dias : null;
            $this->digcli = isset($registro->digcli) ? $registro->digcli : null;
            $this->emails = isset($registro->emails) ? $registro->emails : null;
            $this->env_corr = isset($registro->env_corr) ? $registro->env_corr : null;
            $this->exenta = isset($registro->exenta) ? $registro->exenta : null;
            $this->feccam = isset($registro->feccam) ? $registro->feccam : null;
            $this->fecconv = isset($registro->fecconv) ? $registro->fecconv : null;
            $this->fecdes = isset($registro->fecdes) ? $registro->fecdes : null;
            $this->fecha_desblo = isset($registro->fecha_desblo) ? $registro->fecha_desblo : null;
            $this->fecmod = isset($registro->fecmod) ? $registro->fecmod : null;
            $this->gberno = isset($registro->gberno) ? $registro->gberno : null;
            $this->mail_elec = isset($registro->mail_elec) ? $registro->mail_elec : null;
            $this->margen = isset($registro->margen) ? $registro->margen : null;
            $this->max_bidones = isset($registro->max_bidones) ? $registro->max_bidones : null;
            $this->monto_peso = isset($registro->monto_peso) ? $registro->monto_peso : null;
            $this->monto_uf = isset($registro->monto_uf) ? $registro->monto_uf : null;
            $this->negocio = isset($registro->negocio) ? $registro->negocio : null;
            $this->numadm = isset($registro->numadm) ? $registro->numadm : null;
            $this->numemp = isset($registro->numemp) ? $registro->numemp : null;
            $this->razons = isset($registro->razons) ? $registro->razons : null;
            $this->ruta = isset($registro->ruta) ? $registro->ruta : null;
            $this->rutcli = isset($registro->rutcli) ? $registro->rutcli : null;
            $this->subnegocio = isset($registro->subnegocio) ? $registro->subnegocio : null;
            $this->tipcli = isset($registro->tipcli) ? $registro->tipcli : null;
            $this->tipconv = isset($registro->tipconv) ? $registro->tipconv : null;
            $this->tipdoc = isset($registro->tipdoc) ? $registro->tipdoc : null;
            $this->tippag = isset($registro->tippag) ? $registro->tippag : null;
            $this->unidad_pago = isset($registro->unidad_pago) ? $registro->unidad_pago : null;
            $this->venweb = isset($registro->venweb) ? $registro->venweb : null;
            $this->segmento_cliente = isset($registro->segmento_cliente) ? $registro->segmento_cliente : null;
            $this->negocio_eerr = isset($registro->negocio_eerr) ? $registro->negocio_eerr : null;
            $this->credis_cliente = isset($registro->credis_cliente) ? $registro->credis_cliente : null;
            $this->forma_pago = isset($registro->forma_pago) ? $registro->forma_pago : null;
            $this->plazo_pago = isset($registro->plazo_pago) ? $registro->plazo_pago : null;
            $this->estado_cliente = isset($registro->estado_cliente) ? $registro->estado_cliente : null;
        }
    }

    public function getAprcom()
    {
        return $this->aprcom;
    }

    public function setAprcom($aprcom)
    {
        $this->aprcom = $aprcom;
    }

    public function getAsegur()
    {
        return $this->asegur;
    }

    public function setAsegur($asegur)
    {
        $this->asegur = $asegur;
    }

    public function getCencos()
    {
        return $this->cencos;
    }

    public function setCencos($cencos)
    {
        $this->cencos = $cencos;
    }

    public function getCiaseg()
    {
        return $this->ciaseg;
    }

    public function setCiaseg($ciaseg)
    {
        $this->ciaseg = $ciaseg;
    }

    public function getClacli()
    {
        return $this->clacli;
    }

    public function setClacli($clacli)
    {
        $this->clacli = $clacli;
    }

    public function getClasifClie()
    {
        return $this->clasif_clie;
    }

    public function setClasifClie($clasif_clie)
    {
        $this->clasif_clie = $clasif_clie;
    }

    public function getClicot()
    {
        return $this->clicot;
    }

    public function setClicot($clicot)
    {
        $this->clicot = $clicot;
    }

    public function getClienteTop()
    {
        return $this->cliente_top;
    }

    public function setClienteTop($cliente_top)
    {
        $this->cliente_top = $cliente_top;
    }

    public function getCodbco()
    {
        return $this->codbco;
    }

    public function setCodbco($codbco)
    {
        $this->codbco = $codbco;
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getCodest()
    {
        return $this->codest;
    }

    public function setCodest($codest)
    {
        $this->codest = $codest;
    }

    public function getCodgir()
    {
        return $this->codgir;
    }

    public function setCodgir($codgir)
    {
        $this->codgir = $codgir;
    }

    public function getCodsuc()
    {
        return $this->codsuc;
    }

    public function setCodsuc($codsuc)
    {
        $this->codsuc = $codsuc;
    }

    public function getCodven()
    {
        return $this->codven;
    }

    public function setCodven($codven)
    {
        $this->codven = $codven;
    }

    public function getContelec()
    {
        return $this->contelec;
    }

    public function setContelec($contelec)
    {
        $this->contelec = $contelec;
    }

    public function getCtacopia()
    {
        return $this->ctacopia;
    }

    public function setCtacopia($ctacopia)
    {
        $this->ctacopia = $ctacopia;
    }

    public function getDiacot()
    {
        return $this->diacot;
    }

    public function setDiacot($diacot)
    {
        $this->diacot = $diacot;
    }

    public function getDiagra()
    {
        return $this->diagra;
    }

    public function setDiagra($diagra)
    {
        $this->diagra = $diagra;
    }

    public function getDias()
    {
        return $this->dias;
    }

    public function setDias($dias)
    {
        $this->dias = $dias;
    }

    public function getDigcli()
    {
        return $this->digcli;
    }

    public function setDigcli($digcli)
    {
        $this->digcli = $digcli;
    }

    public function getEmails()
    {
        return $this->emails;
    }

    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    public function getEnvCorr()
    {
        return $this->env_corr;
    }

    public function setEnvCorr($env_corr)
    {
        $this->env_corr = $env_corr;
    }

    public function getExenta()
    {
        return $this->exenta;
    }

    public function setExenta($exenta)
    {
        $this->exenta = $exenta;
    }

    public function getFeccam()
    {
        return $this->feccam;
    }

    public function setFeccam($feccam)
    {
        $this->feccam = $feccam;
    }

    public function getFecconv()
    {
        return $this->fecconv;
    }

    public function setFecconv($fecconv)
    {
        $this->fecconv = $fecconv;
    }

    public function getFecdes()
    {
        return $this->fecdes;
    }

    public function setFecdes($fecdes)
    {
        $this->fecdes = $fecdes;
    }

    public function getFechaDesblo()
    {
        return $this->fecha_desblo;
    }

    public function setFechaDesblo($fecha_desblo)
    {
        $this->fecha_desblo = $fecha_desblo;
    }

    public function getFecmod()
    {
        return $this->fecmod;
    }

    public function setFecmod($fecmod)
    {
        $this->fecmod = $fecmod;
    }

    public function getGberno()
    {
        return $this->gberno;
    }

    public function setGberno($gberno)
    {
        $this->gberno = $gberno;
    }

    public function getMailElec()
    {
        return $this->mail_elec;
    }

    public function setMailElec($mail_elec)
    {
        $this->mail_elec = $mail_elec;
    }

    public function getMargen()
    {
        return $this->margen;
    }

    public function setMargen($margen)
    {
        $this->margen = $margen;
    }

    public function getMaxBidones()
    {
        return $this->max_bidones;
    }

    public function setMaxBidones($max_bidones)
    {
        $this->max_bidones = $max_bidones;
    }

    public function getMontoPeso()
    {
        return $this->monto_peso;
    }

    public function setMontoPeso($monto_peso)
    {
        $this->monto_peso = $monto_peso;
    }

    public function getMontoUf()
    {
        return $this->monto_uf;
    }

    public function setMontoUf($monto_uf)
    {
        $this->monto_uf = $monto_uf;
    }

    public function getNegocio()
    {
        return $this->negocio;
    }

    public function setNegocio($negocio)
    {
        $this->negocio = $negocio;
    }

    public function getNumadm()
    {
        return $this->numadm;
    }

    public function setNumadm($numadm)
    {
        $this->numadm = $numadm;
    }

    public function getNumemp()
    {
        return $this->numemp;
    }

    public function setNumemp($numemp)
    {
        $this->numemp = $numemp;
    }

    public function getRazons()
    {
        return $this->razons;
    }

    public function setRazons($razons)
    {
        $this->razons = $razons;
    }

    public function getRuta()
    {
        return $this->ruta;
    }

    public function setRuta($ruta)
    {
        $this->ruta = $ruta;
    }

    public function getRutcli()
    {
        return $this->rutcli;
    }

    public function setRutcli($rutcli)
    {
        $this->rutcli = $rutcli;
    }

    public function getSubnegocio()
    {
        return $this->subnegocio;
    }

    public function setSubnegocio($subnegocio)
    {
        $this->subnegocio = $subnegocio;
    }

    public function getTipcli()
    {
        return $this->tipcli;
    }

    public function setTipcli($tipcli)
    {
        $this->tipcli = $tipcli;
    }

    public function getTipconv()
    {
        return $this->tipconv;
    }

    public function setTipconv($tipconv)
    {
        $this->tipconv = $tipconv;
    }

    public function getTipdoc()
    {
        return $this->tipdoc;
    }

    public function setTipdoc($tipdoc)
    {
        $this->tipdoc = $tipdoc;
    }

    public function getTippag()
    {
        return $this->tippag;
    }

    public function setTippag($tippag)
    {
        $this->tippag = $tippag;
    }

    public function getUnidadPago()
    {
        return $this->unidad_pago;
    }

    public function setUnidadPago($unidad_pago)
    {
        $this->unidad_pago = $unidad_pago;
    }

    public function getVenweb()
    {
        return $this->venweb;
    }

    public function setVenweb($venweb)
    {
        $this->venweb = $venweb;
    }

    public function getSegmentoCliente()
    {
        return $this->segmento_cliente;
    }

    public function setSegmentoCliente($segmento_cliente)
    {
        $this->segmento_cliente = $segmento_cliente;
    }

    public function getNegocioEerr()
    {
        return $this->negocio_eerr;
    }

    public function setNegocioEerr($negocio_eerr)
    {
        $this->negocio_eerr = $negocio_eerr;
    }

    public function getFormaPago()
    {
        return $this->forma_pago;
    }

    public function setFormaPago($forma_pago)
    {
        $this->forma_pago = $forma_pago;
    }

    public function getPlazoPago()
    {
        return $this->plazo_pago;
    }

    public function setPlazoPago($plazo_pago)
    {
        $this->plazo_pago = $plazo_pago;
    }

    public function getCredisCliente()
    {
        return $this->credis_cliente;
    }

    public function setCredisCliente($credis_cliente)
    {
        $this->credis_cliente = $credis_cliente;
    }

    public function getEstadoCliente()
    {
        return $this->estado_cliente;
    }

    public function setEstadoCliente($estado_cliente)
    {
        $this->estado_cliente = $estado_cliente;
    }

    public function jsonSerialize()
    {
        return [
            'aprcom' => $this->getAprcom(),
            'asegur' => $this->getAsegur(),
            'cencos' => $this->getCencos(),
            'ciaseg' => $this->getCiaseg(),
            'clacli' => $this->getClacli(),
            'clasif_clie' => $this->getClasifClie(),
            'clicot' => $this->getClicot(),
            'cliente_top' => $this->getClienteTop(),
            'codbco' => $this->getCodbco(),
            'codemp' => $this->getCodemp(),
            'codest' => $this->getCodest(),
            'codgir' => $this->getCodgir(),
            'codsuc' => $this->getCodsuc(),
            'codven' => $this->getCodven(),
            'contelec' => $this->getContelec(),
            'ctacopia' => $this->getCtacopia(),
            'diacot' => $this->getDiacot(),
            'diagra' => $this->getDiagra(),
            'dias' => $this->getDias(),
            'digcli' => $this->getDigcli(),
            'emails' => $this->getEmails(),
            'env_corr' => $this->getEnvCorr(),
            'exenta' => $this->getExenta(),
            'feccam' => $this->getFeccam(),
            'fecconv' => $this->getFecconv(),
            'fecdes' => $this->getFecdes(),
            'fecha_desblo' => $this->getFechaDesblo(),
            'fecmod' => $this->getFecmod(),
            'gberno' => $this->getGberno(),
            'mail_elec' => $this->getMailElec(),
            'margen' => $this->getMargen(),
            'max_bidones' => $this->getMaxBidones(),
            'monto_peso' => $this->getMontoPeso(),
            'monto_uf' => $this->getMontoUf(),
            'negocio' => $this->getNegocio(),
            'numadm' => $this->getNumadm(),
            'numemp' => $this->getNumemp(),
            'razons' => $this->getRazons(),
            'ruta' => $this->getRuta(),
            'rutcli' => $this->getRutcli(),
            'subnegocio' => $this->getSubnegocio(),
            'tipcli' => $this->getTipcli(),
            'tipconv' => $this->getTipconv(),
            'tipdoc' => $this->getTipdoc(),
            'tippag' => $this->getTippag(),
            'unidad_pago' => $this->getUnidadPago(),
            'venweb' => $this->getVenweb(),
        ];
    }
}