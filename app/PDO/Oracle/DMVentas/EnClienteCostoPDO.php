<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EnClienteCostoPDO extends Model
{
    public static function getPrecioEspecial($rutcli, $codpro, $codemp)
    {
        $sql = "select precio from en_cliente_costo 
        where trunc(sysdate) between fecini and fecfin
        and rutcli = :rutcli
        and codpro = :codpro
        and codemp = :codemp
        and estado = 'V'";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codemp' => $codemp,
            'rutcli' => $rutcli,
            'codpro' => $codpro
        ]);

        $precio = (count($resultado) > 0 && $resultado[0]->precio <> '') ? $resultado[0]->precio : 0;

        if ($precio == 0 && self::esGobierno($rutcli, $codemp)) {
            $sql = "select precio, codmon from en_cliente_costo where rutcli = 78912345
            and codemp = :codemp and codpro = :codpro and trunc(sysdate) between fecini and fecfin and estado = 'V'";
            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'codemp' => $codemp,
                'codpro' => $codpro
            ]);
            if (count($resultado) > 0) {
                $precio = $resultado[0]->precio;
                $moneda = $resultado[0]->codmon;
                if ($moneda <> 0) {
                    $valor_dolar = self::getDolarDia($moneda);
                    $precio = round($precio * $valor_dolar);
                }
            }
        }
        return $precio;
    }

    private static function esGobierno($rutcli, $codemp)
    {
        $sql = "select sap_get_negocio_eerr(:codemp,:rutcli) negocio from dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codemp' => $codemp,
            'rutcli' => $rutcli
        ]);
        $negocio = (count($resultado) > 0 && $resultado[0]->negocio <> '') ? $resultado[0]->negocio : false;
        if ($negocio == 'GOBIERNO') {
            return true;
        }
        return false;
    }

    private static function getDolarDia($codmon)
    {
        $sql = "select valpar from ma_paridad where codmon = :codmon and trunc(fecpar) = 
        (select max(trunc(fecpar)) from ma_paridad where codmon = :codmon)";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codmon' => $codmon
        ]);
        return $resultado[0]->valpar;
    }
}
