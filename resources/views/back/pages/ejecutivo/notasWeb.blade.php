@extends('back.master.masterpage')
@section('contenido')

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                {{-- <div class="card-header">
                    <div class="breadcrumb-header col">
                        <div class="big-icon">
                            <i class="fa fa-cloud-download text-inverse f-46"></i>  
                        </div>
                        <div class="d-inline-block">
                            <h5>Notas Web</h5>
                            <span class="f-16">Detalle - Notas de Ventas Web | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button>
                    </div>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div>
                </div> --}}
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-cloud-download text-inverse f-50"></i>  
                        </div>
                        <div class="col-sm-6">
                            <h3>Ordenes Web</h3>
                            <span class="f-16">Detalle - Ordenes de Ventas Web | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        <div class="col-sm-4">
                            {{-- <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button> --}}
                        </div> 
                    </div>
                    {{-- <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div> --}}
                </div> 
                <div class="card-block" style="">
                    <div class="col-md-3 col-sm-11 col-xs-12 form-group ">
                        <div class="card-sub">
                            <div class="card-block">
                                <label>Selección de Cartera</label>
                                <div class="col-sm-12">
                                    <div class="col-sm-12"> 
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" checked id="cartera1" name="cartera" value="1">
                                            <label class="form-check-label" for="cartera1">Solo mi Cartera</label>
                                        </div>
                                    
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="cartera2" name="cartera" value="2">
                                            <label class="form-check-label" for="cartera2">Cartera Partners</label>
                                        </div>
    
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="cartera3" name="cartera" value="3">
                                            <label class="form-check-label" for="cartera3">Ambas Carteras</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             
            <div id="CC" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">(C.C) Centro de Costo</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Centro de Costo que realiza la cotización y compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="NumeroOrden" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">N° Orden</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Número de la Orden Web asociada a la compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="Asociada" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Ord. Asociada</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Número de la Orden de Compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="Canal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Canal</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">PLataforma que se utilizo para generar la venta</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div class="table-responsive">
                    {{-- <h5 class="sub-title m-b-15">Ordenes Web</h5> --}}
                    <div id="detalle" onkeyup="Formato()"></div>
                    
                </div>
            </div>

            <div id="Eliminacion" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Eliminación de Orden Web</h2>
                        <div class="card-block text-center">
                            <h5 id="titleDelete" class="modal-title card-block text-center">¿Desea Eliminar la Orden </h5>
                            <span class="f-16" style="color: black">Observación</span>
                            <br>
                            <textarea id="observacion" cols="50" rows="4" placeholder="Observación..."></textarea>
                            <input type="text" id="orden" style="display:none">
                            <input type="text" id="rutcli" style="display:none">
                            <button class="btn btn-success" onclick="Eliminar()" style="height: 40px; margin-left: 5px">Aceptar</button>
                            <button class="btn btn-danger" id="cerrar" type="button" data-dismiss="modal" style="height: 40px; margin-left: 5px">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-cloud-download";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Ordenes Web";


            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            
            getDerechos();
            Buscar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function TomarFila1(){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=61;
            var oldvalue='';
            var newvalue='';
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                newvalue=cells[2].innerHTML;
                                registro(userid, moduleid, actionid, oldvalue, newvalue);
                                location.href = window.location.href.replace('/notasWeb','') + "/generacotizacion/" + cells[5].innerHTML + "/fichacliente/W" + cells[2].innerHTML;
                            };
            };
        }

        function TomarFila2(){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=62;
            var oldvalue='';
            var newvalue='';
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                newvalue = cells[2].innerHTML;
                                registro(userid, moduleid, actionid, oldvalue, newvalue);
                                //Eliminar(cells[1].innerHTML);
                                document.getElementById("titleDelete").innerHTML = "¿Desea Eliminar la Orden " + newvalue+ "?"
                                document.getElementById("observacion").value = "";
                                document.getElementById("rutcli").value = cells[5].innerHTML;
                                document.getElementById("orden").value = cells[2].innerHTML;
                            };
            };
        }

        function Eliminar(){
            var token = '{{ csrf_token() }}';
            var date = new Date();

            var parametros = {
                _token: token,
                fecha:  (date.getDate()) + '-' + (date.getMonth()+1) + '-' + date.getFullYear(),
                numord: document.getElementById("orden").value,
                rutcli: document.getElementById("rutcli").value,
                observ: document.getElementById("observacion").value
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/EliminaNotaWeb')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    document.getElementById("cerrar").click();
                    Buscar();
                    $("#spinner").hide();
                }   
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function FichaCliente(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=16;
            var actionid=41;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                    var cells = this.getElementsByTagName("td");
                    location.href = window.location.href.replace('/notasWeb','') + "/fichacliente/" + rut;
                };
            };
        }

        function Orden(){
            document.getElementById("fecha_Orden").click();
            document.getElementById("fecha_Orden").click();
            Formato();
        }

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[3];
                $(td).addClass('text-right');
                text = $(td).text().substring();
                $(td).text(text.substr(0,10));
                $(td).text(text.substr(8,2) + "-" + text.substr(5,2) + "-" + text.substr(0,4));

                td = tr[i].getElementsByTagName("td")[11];
                if ($(td).text().substring(0,1) != '$'){
                    $(td).text('$' + number_format( $(td).text()));
                }
            }
        }

        $('#cartera1').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=43;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });

        $('#cartera2').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=44;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });

        $('#cartera3').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=45;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });
        
        function Buscar() {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Fic</th>\n' +
                '                            <th>Acc</th>\n' +
                '                            <th>N° Orden\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#NumeroOrden"></i></th>\n' + 
                '                            <th id="fecha_Orden">Fecha Orden</th>\n' +
                '                            <th>Razon Social</th>\n' +
                '                            <th>Rut</th>\n' +
                '                            <th>C.C.\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#CC"></i></th>\n' + 
                '                            <th>Segmento\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th>\n' + 
                '                            <th>Ord. Asociada\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#Asociada"></i></th>\n' + 
                '                            <th>Canal\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#Canal"></i></th>\n' + 
                '                            <th>Venta</th>\n' +
                '                            <th>Neto</th>\n' +
                '                            <th>Doc.</th>\n' +
                '                            <th>Pagada</th>\n' +
                '                            <th>Observación</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;
            var date = new Date();
            var lafecha = '';

            lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-01';
            //console.log(lafecha);
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha,
                cartera: $('input:radio[name=cartera]:checked').val()
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getNotasWeb')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                        '   <button type="button"\n' +
                                            '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                            '       class="icofont icofont-ui-user f-16"></span>\n' +
                                            '   </button>\n' +
                                            '</div>\n';}},
                                {data: "numord",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Procesa Orden Web" onclick="TomarFila1()"><span\n' +
                                                    '       class="fa fa-download f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-danger waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Eliminar Orden Web" data-toggle="modal" data-target="#Eliminacion" onclick="TomarFila2()"><span\n' +
                                                    '       class="ion-close-circled"></span>\n' +
                                                    '   </button>\n' +
                                                    '</div>\n';}},
                                {data: "numord",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "fecord",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data.substring(0,10) + '</span>\n';}},
                                {data: "razon",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "segmento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "facnom",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "canal",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "venta",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "totnet",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "tipdoc",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "pagada",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "observ",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        $("#spinner").hide();
                        Orden();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection