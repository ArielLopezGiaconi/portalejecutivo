<script type="text/javascript" src="{{ asset('bower_components/jquery/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/jquery-ui/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/popper.js/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/js/jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ asset('bower_components/modernizr/js/modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/modernizr/js/css-scrollbars.js') }}"></script>
<!-- Chart js -->
{{--<script type="text/javascript" src="{{ asset('bower_components/chart.js/js/Chart.js') }}"></script>--}}
<!-- amchart js -->
<script src="{{ asset('assets/pages/widget/amchart/amcharts.js') }}"></script>
<script src="{{ asset('assets/pages/widget/amchart/serial.js') }}"></script>
<script src="{{ asset('assets/pages/widget/amchart/light.js') }}"></script>
<script src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/js/SmoothScroll.js') }}"></script>--}}
<script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
<!-- c3 chart js -->
{{--<script src="{{ asset('bower_components/d3/js/d3.min.js') }}"></script>--}}
{{--<script src="{{ asset('bower_components/c3/js/c3.js') }}"></script>--}}
<!-- sweet alert js -->

{{--<script type="text/javascript" src="{{ asset('bower_components/sweetalert/js/sweetalert.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/js/modal.js') }}"></script>--}}
<!-- sweet alert modal.js intialize js -->
<!-- modalEffects js nifty modal window effects -->
{{--<script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>


<!-- data-table js -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.quicksearch.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<!-- Chart js -->
<script type="text/javascript" src="{{ asset('bower_components/chart.js/js/Chart.js') }}"></script>
<!-- Google map js -->
{{--<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>--}}
{{--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/pages/google-maps/gmaps.js') }}"></script>--}}
<!-- gauge js -->
<script src="{{ asset('assets/pages/widget/gauge/gauge.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/pages/dashboard/crm-dashboard.min.js') }}"></script>--}}

<!-- amcharts -->
<script src="{{ asset('assets/pages/amcharts/core.js') }}"></script>
<script src="{{ asset('assets/pages/amcharts/charts.js') }}"></script>
<script src="{{ asset('assets/pages/amcharts/animated.js') }}"></script>

<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('bower_components/i18next/js/i18next.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/jquery-i18next/js/jquery-i18next.min.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('assets/pages/toolbar/jquery.toolbar.min.js') }}"></script>

<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('bower_components/i18next/js/i18next.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/jquery-i18next/js/jquery-i18next.min.js') }}"></script>
<!-- Custom js -->
{{--<script type="text/javascript" src="{{ asset('assets/pages/handson-table/cell-features.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/pages/handson-table/data-operation.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/pages/handson-table/rows-cols-table.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('assets/pages/toolbar/custom-toolbar.js') }}"></script>

<!-- custom js -->
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
<script src="{{ asset('assets/js/vartical-layout.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/pages/dashboard/custom-dashboard.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('assets/js/script.min.js') }}"></script>
<!-- notification js -->
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-growl.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/pages/notification/notification.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/locale/es.js') }}"></script>

{{-- <script src="{{ asset('flickity/owl.carousel.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('flickity/owl.carousel.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('flickity/highlight.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('flickity/flickity.pkgd.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('flickity/bootstrap.min.js') }}" type="text/javascript"></script> --}}

<script>
    document.cookie = 'same-site-cookie=foo; SameSite=Lax';
    document.cookie = 'cross-site-cookie=bar; SameSite=None; Secure';
</script>



<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            content: function () {
                return $('#primary-popover-content').html();
            }
        });
    });

</script>
@stack('js')
