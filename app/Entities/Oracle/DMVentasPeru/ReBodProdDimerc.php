<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 11:43
 */

namespace App\Entities\Oracle\DMVentasPeru;


class ReBodProdDimerc implements \JsonSerializable
{
    private $codemp;
    private $codbod;
    private $codpro;
    private $stocks;
    private $stkini;
    private $fecini;
    private $ultinv;
    private $fecinv;
    private $stkmax;
    private $stkmin;
    private $stkcom;
    private $stkdis;
    private $stkase;
    private $stkcri;
    private $stkint;
    private $usr_creac;
    private $fecha_creac;
    private $usr_modif;
    private $fecha_modif;
    private $costo;
    private $cosprom;
    private $cospro_cal_kardex;
    private $valor_cal_kardex;
    private $stock_cal_kardex;

    public function __construct()
    {

    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getCodbod()
    {
        return $this->codbod;
    }

    public function setCodbod($codbod)
    {
        $this->codbod = $codbod;
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getStocks()
    {
        return ($this->stocks) ? (int)$this->stocks : 0;
    }

    public function setStocks($stocks)
    {
        $this->stocks = $stocks;
    }

    public function getStkini()
    {
        return $this->stkini;
    }

    public function setStkini($stkini)
    {
        $this->stkini = $stkini;
    }

    public function getFecini()
    {
        return $this->fecini;
    }

    public function setFecini($fecini)
    {
        $this->fecini = $fecini;
    }

    public function getUltinv()
    {
        return $this->ultinv;
    }

    public function setUltinv($ultinv)
    {
        $this->ultinv = $ultinv;
    }

    public function getFecinv()
    {
        return $this->fecinv;
    }

    public function setFecinv($fecinv)
    {
        $this->fecinv = $fecinv;
    }

    public function getStkmax()
    {
        return $this->stkmax;
    }

    public function setStkmax($stkmax)
    {
        $this->stkmax = $stkmax;
    }

    public function getStkmin()
    {
        return $this->stkmin;
    }

    public function setStkmin($stkmin)
    {
        $this->stkmin = $stkmin;
    }

    public function getStkcom()
    {
        return ($this->stkcom) ? (int) $this->stkcom : 0;
    }

    public function setStkcom($stkcom)
    {
        $this->stkcom = $stkcom;
    }

    public function getStkdis()
    {
        return $this->stkdis;
    }

    public function setStkdis($stkdis)
    {
        $this->stkdis = $stkdis;
    }

    public function getStkase()
    {
        return $this->stkase;
    }

    public function setStkase($stkase)
    {
        $this->stkase = $stkase;
    }

    public function getStkcri()
    {
        return $this->stkcri;
    }

    public function setStkcri($stkcri)
    {
        $this->stkcri = $stkcri;
    }

    public function getStkint()
    {
        return $this->stkint;
    }

    public function setStkint($stkint)
    {
        $this->stkint = $stkint;
    }

    public function getUsrCreac()
    {
        return $this->usr_creac;
    }

    public function setUsrCreac($usr_creac)
    {
        $this->usr_creac = $usr_creac;
    }

    public function getFechaCreac()
    {
        return $this->fecha_creac;
    }

    public function setFechaCreac($fecha_creac)
    {
        $this->fecha_creac = $fecha_creac;
    }

    public function getUsrModif()
    {
        return $this->usr_modif;
    }

    public function setUsrModif($usr_modif)
    {
        $this->usr_modif = $usr_modif;
    }

    public function getFechaModif()
    {
        return $this->fecha_modif;
    }

    public function setFechaModif($fecha_modif)
    {
        $this->fecha_modif = $fecha_modif;
    }

    public function getCosto()
    {
        return $this->costo;
    }

    public function setCosto($costo)
    {
        $this->costo = $costo;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getCosproCalKardex()
    {
        return $this->cospro_cal_kardex;
    }

    public function setCosproCalKardex($cospro_cal_kardex)
    {
        $this->cospro_cal_kardex = $cospro_cal_kardex;
    }

    public function getValorCalKardex()
    {
        return $this->valor_cal_kardex;
    }

    public function setValorCalKardex($valor_cal_kardex)
    {
        $this->valor_cal_kardex = $valor_cal_kardex;
    }

    public function getStockCalKardex()
    {
        return $this->stock_cal_kardex;
    }

    public function setStockCalKardex($stock_cal_kardex)
    {
        $this->stock_cal_kardex = $stock_cal_kardex;
    }

    public function getStockDisponible()
    {
        return ($this->getStocks() - (int)$this->getStkcom() > 0 ) ? $this->getStocks() - (int)$this->getStkcom() : 0;
    }

    public function jsonSerialize()
    {
        return [
            'codemp' => $this->getCodemp(),
            'codbod' => $this->getCodbod(),
            'codpro' => $this->getCodpro(),
            'stocks' => $this->getStocks(),
            'stkini' => $this->getStkini(),
            'fecini' => $this->getFecini(),
            'ultinv' => $this->getUltinv(),
            'fecinv' => $this->getFecinv(),
            'stkmax' => $this->getStkmax(),
            'stkmin' => $this->getStkmin(),
            'stkcom' => $this->getStkcom(),
            'stkdis' => $this->getStkdis(),
            'stkase' => $this->getStkase(),
            'stkcri' => $this->getStkcri(),
            'stkint' => $this->getStkint(),
            'usr_creac' => $this->getUsrCreac(),
            'fecha_creac' => $this->getFechaCreac(),
            'usr_modif' => $this->getUsrModif(),
            'fecha_modif' => $this->getFechaModif(),
            'costo' => $this->getCosto(),
            'cosprom' => $this->getCosprom(),
            'cospro_cal_kardex' => $this->getCosproCalKardex(),
            'valor_cal_kardex' => $this->getValorCalKardex(),
            'stock_cal_kardex' => $this->getStockCalKardex(),
        ];
    }
}