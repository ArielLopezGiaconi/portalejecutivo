<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';
    protected $namespaceFront = 'App\Http\Controllers\front';
    protected $namespaceBack = 'App\Http\Controllers\back';
    protected $namespaceHelper = 'App\Http\Helpers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));

        Route::middleware('login')
            ->namespace($this->namespaceFront)
            ->prefix('/')
            ->group(base_path('routes/front/login.php'));

        Route::middleware('pages')
            ->namespace($this->namespaceBack)
            ->prefix('/')
            ->group(base_path('routes/back/pages.php'));

    }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));

        Route::middleware('clientes')
            ->namespace($this->namespaceHelper)
            ->prefix('api/clientes')
            ->group(base_path('routes/api/clientes.php'));

        Route::middleware('productos')
            ->namespace($this->namespaceHelper)
            ->prefix('api/productos')
            ->group(base_path('routes/api/productos.php'));

        Route::middleware('cotizacion')
            ->namespace($this->namespaceHelper)
            ->prefix('api/cotizacion')
            ->group(base_path('routes/api/cotizacion.php'));

    }
}
