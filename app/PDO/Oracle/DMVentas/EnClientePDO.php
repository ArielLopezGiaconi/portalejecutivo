<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 23-01-2018
 * Time: 10:53
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\EnCliente;
use App\PDO\MySql\Magento\EmailCobranzaPDO;
use App\PDO\MySql\Magento\SalesFlatOrderPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
//use DB;
use App\Entities\Oracle\DMVentas\NotaVenta;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

class EnClientePDO extends Model
{
    public static function getClienteByRut($in_rut)
    {
        $sql = "SELECT NVL (get_vendedor (3, " . $in_rut . "), 'S/E') ejecutivo, "
            . " NVL ((SELECT 'SI'"
            . " FROM en_cliente b"
            . " WHERE " . $in_rut . " = b.rutcli AND b.codemp = 3), 'NO') tlmkt,"
            . " getdominio (5, get_formapagocli (3, " . $in_rut . ", 0, 5)) formapago"
            . " FROM DUAL";

        $resultado = DB::connection('oracle_dmventas')->select($sql);
        return $resultado;
    }

    public static function getClientePorRut($in_rut, $in_codemp = 3, $have_codsap = true, $first_row = false)
    {
        $sql = "SELECT a.* FROM en_cliente a WHERE rutcli = :rut_cliente AND codemp = :cod_emp";
        if ($have_codsap) $sql .= " AND id_cliente_sap IS NOT NULL";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cliente' => $in_rut,
            'cod_emp' => $in_codemp
        ]);

        $arraReturn = EnClientePDO::returnClienteFormat($resultado, $first_row);
        return $arraReturn;
    }

    public static function getFullClientePorRut($in_rut, $in_codemp = 3, $usuario, $first_row = false)
    {
        $esAdmin = $usuario->superadmin;
        $rutusu = $usuario->rutusu;
        $arraReturn = array();
        if ($esAdmin == 1) {
            $sql = "SELECT a.*, "
                . " GET_SEGMENTO_CLIENTE(a.codemp, a.rutcli) as segmento_cliente,"
                . " SAP_GET_NEGOCIO_EERR(a.codemp, a.rutcli) as negocio_eerr,"
                . " GET_CREDIS(a.codemp, a.rutcli, a.cencos) as credis_cliente, "
                . " SAP_GET_DOMINIO(5, GET_FORMAPAGOCLI(a.codemp, a.rutcli, a.cencos, 5)) as forma_pago,"
                . " SAP_GET_DOMINIO(6, GET_FORMAPAGOCLI(a.codemp, a.rutcli, a.cencos, 6)) as plazo_pago,"
                . " SAP_GET_DOMINIO(7, a.codest) as estado_cliente "
                . " FROM en_cliente a WHERE rutcli = :rut_cliente AND codemp = :cod_emp";
            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'rut_cliente' => $in_rut,
                'cod_emp' => $in_codemp
            ]);
        } else {
            $sql = "SELECT a.*, "
                . " GET_SEGMENTO_CLIENTE(a.codemp, a.rutcli) as segmento_cliente,"
                . " SAP_GET_NEGOCIO_EERR(a.codemp, a.rutcli) as negocio_eerr,"
                . " GET_CREDIS(a.codemp, a.rutcli, a.cencos) as credis_cliente, "
                . " SAP_GET_DOMINIO(5, GET_FORMAPAGOCLI(a.codemp, a.rutcli, a.cencos, 5)) as forma_pago,"
                . " SAP_GET_DOMINIO(6, GET_FORMAPAGOCLI(a.codemp, a.rutcli, a.cencos, 6)) as plazo_pago,"
                . " SAP_GET_DOMINIO(7, a.codest) as estado_cliente "
                . " FROM en_cliente a WHERE rutcli = :rut_cliente AND codemp = :cod_emp "
                . "and exists ( "
                . "   select 1 from de_cliente b where b.rutcli = a.rutcli and b.codemp = a.codemp and b.rutven = :rutusu "
                . ")";
            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'rut_cliente' => $in_rut,
                'cod_emp' => $in_codemp,
                'rutusu' => $rutusu
            ]);
        }

        $arraReturn = EnClientePDO::returnClienteFormat($resultado, $first_row);
        return $arraReturn;
    }

    public static function centroCostoExiste($rut_cli, $centro_costo)
    {
        $sql = "SELECT COUNT(*) AS existe"
            . " FROM en_cliente en"
            . " INNER JOIN de_cliente de"
            . " ON en.rutcli = de.rutcli"
            . " WHERE en.rutcli = :rut_cli AND de.cencos = :centro_costo";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cli' => $rut_cli,
            'centro_costo' => $centro_costo
        ]);

        return ($resultado != null && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getLineaCreditoCliente($in_rut, $in_cencos = 0, $in_codemp = 3)
    {
        $sql = "SELECT get_credis(:codigo_empresa, :rut_cliente, :centro_costo) credito FROM DUAL";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codigo_empresa' => $in_codemp,
            'rut_cliente' => $in_rut,
            'centro_costo' => $in_cencos
        ]);

        return $resultado[0]->credito;
    }

    public static function getGrupoDeVentaEjecutivoPorRutCliente($in_rut)
    {
        $sql = "SELECT get_grupoventa('U',get_vendedor(3,:rut_cliente)) AS grupo_venta FROM dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cliente' => $in_rut
        ]);

        return ($resultado && count($resultado) > 0) ? $resultado[0]->grupo_venta : null;
    }

    public static function actualizarEjecutivoPorCliente($in_rutcliente, $in_rutejecutivo)
    {
        try {
            $sql = "UPDATE en_cliente SET codven = :nuevo_ejecutivo WHERE rutcli = :rut_cliente";
            DB::connection('oracle_dmventas')->select($sql, [
                'nuevo_ejecutivo' => $in_rutejecutivo,
                'rut_cliente' => $in_rutcliente
            ]);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function buscarClientesPorRazonSocial($in_razonsocial, $codemp = 3, $formato_rapido = true, $usuario)
    {
        $esAdmin = $usuario->superadmin;
        $rutusu = $usuario->rutusu;
        $arrayReturn = null;
        try {
            if ($esAdmin == 1) {
                $sql = "SELECT rutcli, razons FROM en_cliente WHERE razons LIKE :razon_social AND codemp = :codemp";
                $registros = DB::connection('oracle_dmventas')->select($sql, [
                    'razon_social' => '%' . $in_razonsocial . '%',
                    'codemp' => $codemp
                ]);
            } else {
                $sql = "select a.rutcli, a.razons from en_cliente a 
                where a.razons like :razon_social and codemp = :codemp
                and exists (
                    select 1 from de_cliente b where b.rutcli = a.rutcli and b.codemp = a.codemp and b.rutven = :rutusu 
                )";
                $registros = DB::connection('oracle_dmventas')->select($sql, [
                    'razon_social' => '%' . $in_razonsocial . '%',
                    'codemp' => $codemp,
                    'rutusu' => $rutusu
                ]);
            }

            if ($formato_rapido) {
                foreach ($registros as $registro) {
                    $arrayReturn[] = array(
                        'rutcli' => $registro->rutcli,
                        'razons' => $registro->razons,
                    );
                }
            } else {
                foreach ($registros as $registro) {
                    $cliente = new EnCliente($registro);
                    $arrayReturn[] = $cliente;
                }
            }

            return $arrayReturn;
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function existsByRutcli($rutcli, $codemp = 3)
    {
        $sql = "SELECT COUNT(*) AS existe FROM en_cliente"
            . " WHERE codemp = :cod_emp AND rutcli = :rutcli";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'cod_emp' => $codemp
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function existsCencosByRutcli($rutcli, $cencos, $codemp = 3)
    {
        $sql = "SELECT COUNT(*) AS existe FROM de_cliente"
            . " WHERE codemp = :cod_emp AND rutcli = :rutcli AND cencos = :cencos";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'cod_emp' => $codemp,
            "cencos" => $cencos
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getNegocioEerr($rutcli, $codemp = 3)
    {
        $sql = "select sap_get_negocio_eerr(:codemp, :rutcli) as negocio from dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'cod_emp' => $codemp
        ]);
        return (count($resultado) >= 1)
            ? $resultado[0]->negocio : 'SIN CLASIFICAR';

    }

    private static function returnClienteFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $cliente = new EnCliente($registro);
            if ($first_row) {
                return $cliente;
            }
            $arrayReturn[] = $cliente;
        }

        return $arrayReturn;
    }

    public static function getDetalleFichaCliente($rutcliente)
    {
        try{
            $sql = "select 
                    a.codemp,dm_ventas.bi_get_dias_en_cartera(a.rutcli,b.cencos,b.rutven) as dias_cartera,
                    nvl(d.numrel,a.rutcli) as relacion,get_potencial(3,a.rutcli) as potencial,
                    a.rutcli,a.digcli,get_segmento_cliente(a.codemp,a.rutcli) as segmento,a.razons,b.cencos,
                    get_negocio_eerr(a.codemp,a.rutcli) as negocio,
                    c.userid,c.nombre,c.apepat,get_grupo(c.userid) as equipo,
                    get_estadocliente(a.codemp,a.rutcli) as observacion,
                    getdominio('5',b.forpag) as pago,
                    case
                        when a.codest='3' then 'BLOQUEADO'
                        when a.codest='4' then 'BLOQUEADO'
                        when a.codest='5' then 'BLOQUEADO'
                        else 'NO BLOQUEADO'
                    end as estado,
                    b.lincre as linea, 
                    get_credis(a.codemp,a.rutcli,a.cencos) as disponible,
                    nvl((select 1 from re_sucursal_comuna x where x.codcom = b.comuna and x.codbod = 66),0) as es_antofagasta
                    from en_cliente a left join (select * from re_emprela where estado='2') d
                    on a.rutcli=d.rutcli and a.codemp=d.codemp,
                    de_cliente b,ma_usuario c
                    where a.rutcli=b.rutcli
                    and a.cencos=b.cencos
                    and b.rutven=c.rutusu
                    and a.rutcli=:rutcliente
                    and a.codemp=3
                    and a.codemp=b.codemp";

            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'rutcliente' => $rutcliente
            ]);
            return $resultado;
        }
        catch(\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }
}