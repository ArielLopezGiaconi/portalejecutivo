@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="icofont icofont-web text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Ingresos Web</h3>
                            <span class="f-16">Ingresos Web | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        <div class="col-sm-4">
                            {{-- <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button> --}}
                        </div> 
                    </div>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block" style="">
                    <div class="col-md-3 col-sm-11 col-xs-12 form-group ">
                        <div class="card-sub">
                            <div class="card-block">
                                <label>Selección de Cartera</label>
                                <div class="col-sm-12">
                                    <div class="col-sm-12"> 
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" checked id="cartera1" name="cartera" value="1">
                                            <label class="form-check-label" for="cartera1">Solo mi Cartera</label>
                                        </div>
                                    
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="cartera2" name="cartera" value="2">
                                            <label class="form-check-label" for="cartera2">Cartera Partners</label>
                                        </div>
    
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="cartera3" name="cartera" value="3">
                                            <label class="form-check-label" for="cartera3">Ambas Carteras</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card-block">
                <div class="table-responsive">
                    <div id="detalle" onkeyup="Formato()"></div>
                    
                    <div class="spinner" id="spinner" style="display: none;">
                        <center>
                            <div class="bolitas">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                            <div class="cargando">Cargando datos...</div>
                        </center>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>
        
        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "icofont icofont-web";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Ingresos Web";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            Buscar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function Orden(){
            document.getElementById("ultimaFecha").click();
            document.getElementById("ultimaFecha").click();
        }

        function FichaCliente(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                //console.log(window.location.href.replace('/ingresosWeb','') + "/fichacliente/" + cells[1].innerHTML);
                                location.href = window.location.href.replace('/ingresosWeb','') + "/fichacliente/" + cells[1].innerHTML;
                            };
            };
        }

        $('#cartera1').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=18;
            var actionid=46;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });

        $('#cartera2').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=18;
            var actionid=47;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });

        $('#cartera3').on('change', function() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=18;
            var actionid=48;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar();
        });

        function Buscar() {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Acc</th>\n' +
                '                            <th>Rut</th>\n' +
                '                            <th>Razon Social</th>\n' +
                '                            <th id="ultimaFecha" >Ultimo Ingreso</th>\n' +
                '                            <th>Usuario</th>\n' +
                '                            <th>Contacto</th>\n' +
                '                            <th>Apellido</th>\n' +
                '                            <th>Telefono</th>\n' +
                '                            <th>Fecha Ult. Nota</th>\n' +
                '                            <th>Fecha Ult. Orden</th>\n' +
                '                            <th>Fecha Orden</th>\n' +
                '                            <th>Fecha Anotación</th>\n' +
                '                            <th>Usuario Anotación</th>\n' +
                '                            <th>Anotación</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                cartera: $('input:radio[name=cartera]:checked').val()
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/ingresosWeb')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                            '   <button type="button"\n' +
                                            '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                            '       class="icofont icofont-ui-user f-16"></span>\n' +
                                            '   </button>\n' +
                                            '</div>\n';}},
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return  data;}}, 
                                {data: "razons",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "ultimo_ingreso",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "usuario",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "contacto",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "apellido",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "telefono",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "fecha_ultima_nv",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "fecha_ultima_orden_web",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "ultima_orden_web",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "fecha_anotacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "usuario_anotacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "anotacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',  
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        $("#spinner").hide();
                        Orden();
                    } else {
                        var text = '<div class="alert alert-danger background-danger">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                            '<i class="icofont icofont-close-line-circled text-white"></i>' +
                                        '</button>' +
                                        '<strong>Sin Ingresos Web</strong>' +
                                    '</div>';
                        document.getElementById("detalle").innerHTML = text
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Orden(){
            document.getElementById("ultimaFecha").click();
            document.getElementById("ultimaFecha").click();
        }
    </script>
@endsection