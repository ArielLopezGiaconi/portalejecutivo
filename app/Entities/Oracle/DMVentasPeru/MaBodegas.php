<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 11:35
 */

namespace App\Entities\Oracle\DMVentasPeru;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class MaBodegas implements \JsonSerializable
{
    private $codemp;
    private $codbod;
    private $vender;
    private $bodtip;
    private $rutcli;
    private $cencos;
    private $nombre;
    private $usr_creac;
    private $fecha_creac;
    private $usr_modif;
    private $fecha_modif;
    private $activa;
    private $contable;
    private $repartir_devolucion;
    private $cambio_codigo;
    private $devolucion_proveedor;
    private $inventario;
    private $afecto;
    private $costo_promedio;
    private $clasificacion_vendible;
    private $clasificacion_estado;
    private $especial;
    private $especial_orden;
    private $costo_promedio_propio;
    private $glosa;
    private $relacionadas;


    // new values
    private $sap_sociedad;
    private $sap_centro;
    private $sap_almacen;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->codemp = isset($registro->codemp) ? $registro->codemp : null;
            $this->codbod = isset($registro->codbod) ? $registro->codbod : null;
            $this->vender = isset($registro->vender) ? $registro->vender : null;
            $this->bodtip = isset($registro->bodtip) ? $registro->bodtip : null;
            $this->rutcli = isset($registro->rutcli) ? $registro->rutcli : null;
            $this->cencos = isset($registro->cencos) ? $registro->cencos : null;
            $this->nombre = isset($registro->nombre) ? $registro->nombre : null;
            $this->usr_creac = isset($registro->usr_creac) ? $registro->usr_creac : null;
            $this->fecha_creac = isset($registro->fecha_creac) ? $registro->fecha_creac : null;
            $this->usr_modif = isset($registro->usr_modif) ? $registro->usr_modif : null;
            $this->fecha_modif = isset($registro->fecha_modif) ? $registro->fecha_modif : null;
            $this->activa = isset($registro->activa) ? $registro->activa : null;
            $this->contable = isset($registro->contable) ? $registro->contable : null;
            $this->repartir_devolucion = isset($registro->repartir_devolucion) ? $registro->repartir_devolucion : null;
            $this->cambio_codigo = isset($registro->cambio_codigo) ? $registro->cambio_codigo : null;
            $this->devolucion_proveedor = isset($registro->devolucion_proveedor) ? $registro->devolucion_proveedor : null;
            $this->inventario = isset($registro->inventario) ? $registro->inventario : null;
            $this->afecto = isset($registro->afecto) ? $registro->afecto : null;
            $this->costo_promedio = isset($registro->costo_promedio) ? $registro->costo_promedio : null;
            $this->clasificacion_vendible = isset($registro->clasificacion_vendible) ? $registro->clasificacion_vendible : null;
            $this->clasificacion_estado = isset($registro->clasificacion_estado) ? $registro->clasificacion_estado : null;
            $this->especial = isset($registro->especial) ? $registro->especial : null;
            $this->especial_orden = isset($registro->especial_orden) ? $registro->especial_orden : null;
            $this->costo_promedio_propio = isset($registro->costo_promedio_propio) ? $registro->costo_promedio_propio : null;
            $this->glosa = isset($registro->glosa) ? $registro->glosa : null;
            $this->relacionadas = isset($registro->relacionadas) ? $registro->relacionadas : null;


            // new values
            $this->sap_sociedad = isset($registro->sap_sociedad) ? $registro->sap_sociedad : null;
            $this->sap_centro = isset($registro->sap_centro) ? $registro->sap_centro : null;
            $this->sap_almacen = isset($registro->sap_almacen) ? $registro->sap_almacen : null;
        }
    }

    public function getActiva()
    {
        return $this->activa;
    }

    public function setActiva($activa)
    {
        $this->activa = $activa;
    }

    public function getAfecto()
    {
        return $this->afecto;
    }

    public function setAfecto($afecto)
    {
        $this->afecto = $afecto;
    }

    public function getBodtip()
    {
        return $this->bodtip;
    }

    public function setBodtip($bodtip)
    {
        $this->bodtip = $bodtip;
    }

    public function getCambioCodigo()
    {
        return $this->cambio_codigo;
    }

    public function setCambioCodigo($cambio_codigo)
    {
        $this->cambio_codigo = $cambio_codigo;
    }

    public function getCencos()
    {
        return $this->cencos;
    }

    public function setCencos($cencos)
    {
        $this->cencos = $cencos;
    }

    public function getClasificacionEstado()
    {
        return $this->clasificacion_estado;
    }

    public function setClasificacionEstado($clasificacion_estado)
    {
        $this->clasificacion_estado = $clasificacion_estado;
    }

    public function getClasificacionVendible()
    {
        return $this->clasificacion_vendible;
    }

    public function setClasificacionVendible($clasificacion_vendible)
    {
        $this->clasificacion_vendible = $clasificacion_vendible;
    }

    public function getCodbod()
    {
        return $this->codbod;
    }

    public function setCodbod($codbod)
    {
        $this->codbod = $codbod;
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getContable()
    {
        return $this->contable;
    }

    public function setContable($contable)
    {
        $this->contable = $contable;
    }

    public function getCostoPromedio()
    {
        return $this->costo_promedio;
    }

    public function setCostoPromedio($costo_promedio)
    {
        $this->costo_promedio = $costo_promedio;
    }

    public function getCostoPromedioPropio()
    {
        return $this->costo_promedio_propio;
    }

    public function setCostoPromedioPropio($costo_promedio_propio)
    {
        $this->costo_promedio_propio = $costo_promedio_propio;
    }

    public function getDevolucionProveedor()
    {
        return $this->devolucion_proveedor;
    }

    public function setDevolucionProveedor($devolucion_proveedor)
    {
        $this->devolucion_proveedor = $devolucion_proveedor;
    }

    public function getEspecial()
    {
        return $this->especial;
    }

    public function setEspecial($especial)
    {
        $this->especial = $especial;
    }

    public function getEspecialOrden()
    {
        return $this->especial_orden;
    }

    public function setEspecialOrden($especial_orden)
    {
        $this->especial_orden = $especial_orden;
    }

    public function getFechaCreac()
    {
        return $this->fecha_creac;
    }

    public function setFechaCreac($fecha_creac)
    {
        $this->fecha_creac = $fecha_creac;
    }

    public function getFechaModif()
    {
        return $this->fecha_modif;
    }

    public function setFechaModif($fecha_modif)
    {
        $this->fecha_modif = $fecha_modif;
    }

    public function getGlosa()
    {
        return $this->glosa;
    }

    public function setGlosa($glosa)
    {
        $this->glosa = $glosa;
    }

    public function getInventario()
    {
        return $this->inventario;
    }

    public function setInventario($inventario)
    {
        $this->inventario = $inventario;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getRelacionadas()
    {
        return $this->relacionadas;
    }

    public function setRelacionadas($relacionadas)
    {
        $this->relacionadas = $relacionadas;
    }

    public function getRepartirDevolucion()
    {
        return $this->repartir_devolucion;
    }

    public function setRepartirDevolucion($repartir_devolucion)
    {
        $this->repartir_devolucion = $repartir_devolucion;
    }

    public function getRutcli()
    {
        return $this->rutcli;
    }

    public function setRutcli($rutcli)
    {
        $this->rutcli = $rutcli;
    }

    public function getUsrCreac()
    {
        return $this->usr_creac;
    }

    public function setUsrCreac($usr_creac)
    {
        $this->usr_creac = $usr_creac;
    }

    public function getUsrModif()
    {
        return $this->usr_modif;
    }

    public function setUsrModif($usr_modif)
    {
        $this->usr_modif = $usr_modif;
    }

    public function getVender()
    {
        return $this->vender;
    }

    public function setVender($vender)
    {
        $this->vender = $vender;
    }

    public function getSapSociedad()
    {
        return $this->sap_sociedad;
    }

    public function setSapSociedad($sap_sociedad)
    {
        $this->sap_sociedad = $sap_sociedad;
    }

    public function getSapCentro()
    {
        return $this->sap_centro;
    }

    public function setSapCentro($sap_centro)
    {
        $this->sap_centro = $sap_centro;
    }

    public function getSapAlmacen()
    {
        return $this->sap_almacen;
    }

    public function setSapAlmacen($sap_almacen)
    {
        $this->sap_almacen = $sap_almacen;
    }

    public function jsonSerialize()
    {
        return [
            'activa' => $this->getActiva(),
            'afecto' => $this->getAfecto(),
            'bodtip' => $this->getBodtip(),
            'cambio_codigo' => $this->getCambioCodigo(),
            'cencos' => $this->getCencos(),
            'clasificacion_estado' => $this->getClasificacionEstado(),
            'clasificacion_vendible' => $this->getClasificacionVendible(),
            'codbod' => $this->getCodbod(),
            'codemp' => $this->getCodemp(),
            'contable' => $this->getContable(),
            'costo_promedio' => $this->getCostoPromedio(),
            'costo_promedio_propio' => $this->getCostoPromedioPropio(),
            'devolucion_proveedor' => $this->getDevolucionProveedor(),
            'especial' => $this->getEspecial(),
            'especial_orden' => $this->getEspecialOrden(),
            'fecha_creac' => $this->getFechaCreac(),
            'fecha_modif' => $this->getFechaModif(),
            'glosa' => $this->getGlosa(),
            'inventario' => $this->getInventario(),
            'nombre' => $this->getNombre(),
            'relacionadas' => $this->getRelacionadas(),
            'repartir_devolucion' => $this->getRepartirDevolucion(),
            'rutcli' => $this->getRutcli(),
            'usr_creac' => $this->getUsrCreac(),
            'usr_modif' => $this->getUsrModif(),
            'vender' => $this->getVender(),

            // new values
            'sap_sociedad' => $this->getSapSociedad(),
            'sap_centro' => $this->getSapCentro(),
            'sap_almacen' => $this->getSapAlmacen(),
        ];
    }
}
