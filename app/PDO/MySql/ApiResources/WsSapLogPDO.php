<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 04-02-2019
 * Time: 17:27
 */

namespace App\PDO\MySql\ApiResources;

use App\Entities\MySql\Cerberus\SapWarehouseOplDaily;
use App\PDO\Oracle\DMVentas\MaProductPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class WsSapLogPDO extends Model
{
    public static function insertLog($url_full, $xml_request, $xml_response = null, $interface = null)
    {
        $sql = "INSERT INTO ws_sap_log (url_full, xml_request, xml_response, interface, fecha)"
            . " VALUES(:url, :xml_request, :xml_response, :interface, :fecha)";

        try {
            DB::connection('mysql_api_resources')->select($sql, [
                'url' => $url_full,
                'xml_request' => $xml_request,
                'xml_response' => $xml_response,
                'interface' => $interface,
                'fecha' => Carbon::now()->setTimezone('America/Santiago')->toDateTimeString(),
            ]);
        } catch (\PDOException $e) {
            return false;
        }

        return true;
    }
}