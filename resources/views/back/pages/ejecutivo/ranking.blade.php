@extends('back.master.masterpage')
@section('contenido')
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4><i class="ion-trophy text-inverse m-r-10 f-36 m-t-10-neg" style="box-shadow: none;-moz-box-shadow: none;"></i>Ranking ejecutivos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icofont icofont-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ url('ranking') }}">Ranking</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-5 m-t-5">
                            <div class="card m-b-0 z-depth-bottom-1">
                                <div class="card-block p-2">
                                    <div class="row align-items-center m-l-0">
                                        <div class="col-auto">
                                            <i class="feather icon-briefcase f-34 text-c-pink"></i>
                                        </div>
                                        <div class="col-auto">
                                            <h6 class="text-muted m-b-10">Grupo</h6>
                                            <h3 class="m-b-0 f-16" name="tx_grupo_res"></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 m-t-5">
                            <div class="card m-b-0 z-depth-bottom-1">
                                <div class="card-block p-2">
                                    <div class="row align-items-center m-l-0">
                                        <div class="col-auto">
                                            <i class="fa fa-calendar f-30 text-c-green"></i>
                                        </div>
                                        <div class="col-auto">
                                            <h6 class="text-muted m-b-10">Actualizado</h6>
                                            <h3 class="m-b-0 f-16 text-uppercase" name="tx_mes_anio_res"></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 m-t-5">
                            <div class="card bg-c-yellow text-white widget-visitor-card m-b-0 z-depth-bottom-1" style="padding:1px!important">
                                <div class="card-block-small text-center p-2">
                                    <h5 class="m-b-0 f-w-700" style="color:#303548;" name="tx_ranking_res"> </h5>
                                    <h7 class="f-w-700" name="tx_userid_res"></h7>
                                    <i class="fa fa-hashtag m-r-5 f-60"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <div class="page-body m-t-15-neg">
                    <div class="card">
                        <div class="card-block table-border-style">
                            <div class="">
                                <table class="table table-xxs table-styling">
                                    <thead>
                                    <tr class="">
                                        <th class="middle width-100 text-center"><i class="icofont icofont-user-suited f-18" title="Ejecutivo"></i></th>
                                        <th class="middle width-50 text-center"><i class="icofont icofont-users f-20" title="Equipo"></i></th>
                                        <th class="middle width-50 text-center">Factor</th>
                                        <th class="middle width-50 table-success text-center">Ranking general</th>
                                        <th class="middle width-50 text-center" title="Cumplimiento meta">% Meta</th>
                                        <th class="middle width-50 text-center" title="Crecimiento">Crec</th>
                                        <th class="middle width-50 text-center" title="Fidelización">Fidel</th>
                                        <th class="middle width-50 text-center" title="Penetración">Penet</th>
                                        <th class="middle width-50 text-center" title="Web">Web</th>
                                        <th class="middle width-50 text-center" title="Tickets nuevos">Tck <small>nuevos</small></th>
                                        <th class="middle width-50 text-center" title="Venta nuevos">Vta <small>nuevos</small></th>
                                        <th class="middle width-50 text-center" title="Balance">Balan</th>
                                        <th class="middle width-50 text-center" title="Part mer">Part Mer</th>
                                        <th class="middle width-50 text-center" title="Atendidos">Atend</th>
                                    </tr>
                                    </thead>
                                    <tbody name="tb_ranking_tbody">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    @include('back.others.js.ejecutivo')
    @include('back.others.js.portal')
@endsection
@section('custom-includes')
    <script>
        getRankingGrupo();
    </script>
@endsection