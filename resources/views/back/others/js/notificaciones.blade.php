<script>
	var userId = '{{ Session::get('usuario')->get('userid') }}';
	var notificacionesLeidas;
	function getNotificaciones(){
		var parametros = {"userid": userId};
		$.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data:parametros,
            url: '{{ env("URL_API") }}/api/portal/notificaciones',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if(json.code==200){
                    var notis = json.response;
					notificacionesLeidas = notis;
					muestraNotificaciones(notis);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'busca notificaciones', code, text);
            }
        });
		setTimeout(getNotificaciones, 5000);
	}

	function muestraNotificaciones(notificaciones){
		var html = '';
		var urlCotizacion = "{{ url('generacotizacion') }}";
		$("#listaNotificaciones").empty();
		notificaciones.forEach(function(notificacion){
			html = '<li>' +
                    	'<div class="media">' +
							'<a href="' + urlCotizacion + '/' + notificacion.rutcli+'/fichacliente/W'+ notificacion.numord + '">'+
                        		'<i class="feather icon-globe"></i>' +
									'<div class="media-body">'+
                                		'<h5 class="notification-user">' + notificacion.titulo +'</h5>'+
                                		'<p class="notification-msg">' + notificacion.cuerpo + '</p>' +
                                		'<span class="notification-time">' + notificacion.fecha + '</span>' +
                            		'</div>' +
							'</a>'
                    	'</div>' +
                     '</li>';
			$('#listaNotificaciones').append(html);
		});
		$('#qNotificaciones').text(notificaciones.length);
		if(notificaciones.length == 0){
			$("#qNotificaciones").attr("style","display:none !important;");
		}
		else{
			$("#qNotificaciones").attr("style","");
		}
	}
	$(document).ready(function(){
		getNotificaciones();
		
		$('#capaNotificaciones').on('hidden.bs.dropdown',function(){
			var arrayNotificacionesLeidas = [];
			notificacionesLeidas.forEach(function(notificacion){
				arrayNotificacionesLeidas.push(notificacion.id);
			});
			var parametros = {"notificaciones": arrayNotificacionesLeidas};
			$.ajax({
            	xhrFields: {
                	withCredentials: true
            	},
            	headers: {
                	'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            	},
            	data:parametros,
            	url: '{{ env("URL_API") }}/api/portal/desnotificar-notificaciones',
            	type: 'POST',
            	cache: false,
            	datatype: 'json',
            	async: true,
            	success: function (json) {
                	if(json.code==200){
                    	$("#listaNotificaciones").empty();
						$("#qNotificaciones").attr("style","display:none !important;");
                	}
            	},
            	error: function(e){
                	var code = e.status;
                	var text = e.statusText;
                	registroError(ejecutivo, moduleid_global, 'busca notificaciones', code, text);
            	}
        	});
		});
	});
</script>