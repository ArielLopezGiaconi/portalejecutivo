<?php


namespace App\Http\Controllers;

use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\Oracle\DMVentas\EnClienteCostoPDO;
use App\PDO\Oracle\DMVentas\EnConveniPDO;
use App\PDO\Oracle\DMVentas\EnCotizacPDO;
use App\PDO\Oracle\DMVentas\MaProductPDO;
use App\PDO\Oracle\DMVentas\ReClienteListaLineaPDO;
use App\PDO\Oracle\DMVentas\ReCanProdPDO;
use http\Env\Request;
use mysql_xdevapi\Session;

class CotizacionController extends Controller
{
    public function validarCotizacion($update, $usuario, $rutcli, $productos, $codemp)
    {
        $type = 'popular';
        $returnProductos = array();
        $returnProductosRecomendador = array();
        $i = 0;
        $x = 0;
        $subtotal = 0;
        $costoTotal = 0;
        foreach ($productos as $producto) {
            $codpro = $producto['codpro'];
            $cantidad = $producto['cantidad'];
            $precio_cotizado = $producto['precio'];
            $precio_especial = EnConveniPDO::getPrecioConvenio($rutcli, $codpro, $codemp);
            $costo = $producto['costo'];
            if ($precio_especial == 0) {
                $precio_especial = EnClienteCostoPDO::getPrecioEspecial($rutcli, $codpro, $codemp);
            }
            $codcnl = ReClienteListaLineaPDO::getCodCnl($rutcli, $codpro, $codemp);
            $precio_minimo = ReCanProdPDO::getPrecioMinimo($codcnl, $codpro, $codemp);

            if ($precio_especial < $precio_minimo && $precio_especial > 0) {
                $precio_minimo = $precio_especial;
            }

            if (($precio_cotizado < $precio_minimo) || ($precio_cotizado < $costo)) {
                $flag = false;
                $flagType = 1;
                $msg = "Precio ingresado es inferior al precio mínimo permitido [$" . $precio_minimo . "]";
            } else {
                $flag = true;
                $flagType = 0;
                $msg = "Producto agregado correctamente";
                $returnProductosRecomendador[$x]['sku'] = $codpro;
                $returnProductosRecomendador[$x]['cantidad'] = $cantidad;
                $subtotal = $subtotal + ($cantidad * $precio_cotizado);
                $costoTotal = $costoTotal + ($cantidad * $costo);
                $x++;
            }

            $returnProductos[$i]['codpro'] = $codpro;
            $returnProductos[$i]['precio_cotizado'] = $precio_cotizado;
            $returnProductos[$i]['flag'] = $flag;
            $returnProductos[$i]['flagType'] = $flagType;
            $returnProductos[$i]['msg'] = $msg;
            $returnProductos[$i]['precio_minimo'] = $precio_minimo;
            $i++;
        }
        try {
            $margen = number_format((($subtotal - $costoTotal) / $subtotal) * 100, 1);
        } catch (\Exception $e) {
            $margen = 0;
        }
        if ($update == "true") {
            $response_data = array(
                "productos" => $returnProductos,
                "subtotal" => $subtotal,
                'margen' => $margen
            );
        } else {
            $json_in = json_encode(array(
                "user_id" => $usuario->codusu,
                "client_id" => $rutcli,
                "products" => $returnProductosRecomendador
            ));
            $response_data = array(
                "productos" => $returnProductos,
                "subtotal" => $subtotal,
                'margen' => $margen,
                "recomendados" => $this->getRecomendados($json_in, $type, $rutcli)
            );
        }
        $response = new ResponseFormatt();
        $response->setCode(200)
            ->setResponse($response_data);
        return $response->returnToJson();
    }

    public function getRecomendados($json_in, $type, $rutcli)
    {
        $array_data = array();
        $url = env("WS_RECOMENDADOR") . "?type=" . $type;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_in);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        $i = 0;
        foreach ($result->products as $product) {
            $data_producto = MaProductPDO::getProductByType($rutcli, $product->sku, 'CODPRO');
            $array_data['products'][$i] = $data_producto;
            unset($data_producto);
            $i++;
        }
        return $array_data;
    }

    public function guardarCotizacion($ejecutivo, $rutcli, $cencos, $observacion, $contacto, $productos, $codemp)
    {
        foreach ($productos as $producto) {
            $codpro = $producto['codpro'];
            $precio_cotizado = $producto['precio'];
            $precio_especial = EnConveniPDO::getPrecioConvenio($rutcli, $codpro, $codemp);
            if ($precio_especial == 0) {
                $precio_especial = EnClienteCostoPDO::getPrecioEspecial($rutcli, $codpro, $codemp);
            }
            $codcnl = ReClienteListaLineaPDO::getCodCnl($rutcli, $codpro, $codemp);
            $precio_minimo = ReCanProdPDO::getPrecioMinimo($codcnl, $codpro, $codemp);

            if ($precio_especial < $precio_minimo && $precio_minimo > 0) {
                $precio_minimo = $precio_especial;
            }

            if ($precio_cotizado < $precio_minimo) {
                $response = new ResponseFormatt();
                $response->setCode(300)
                    ->setResponse(array(
                        "respuesta" => "El precio ingresado es menor al precio mínimo permitido \nNo se ha guardado la cotización \nCódigo de producto [ " . $codpro . " ]",
                        "numcot" => null
                    ));
                return $response->returnToJson();
            }
        }
        $forPlaPagCliente = DeClientePDO::getForPlaPag($rutcli, $cencos, $codemp);


        $numcot = EnCotizacPDO::saveCotizac($ejecutivo, $rutcli, $cencos, $forPlaPagCliente, $observacion, $contacto, $productos, $codemp);
        if ($numcot == 0) {
            $response = new ResponseFormatt();
            $response->setCode(404)
                ->setResponse(array(
                    "respuesta" => "No se pudo guardar la cotización en la BBDD",
                    "numcot" => null
                ));
            return $response->returnToJson();
        }
        $response = new ResponseFormatt();
        $response->setCode(200)
            ->setResponse(array(
                "respuesta" => "Cotización guardada con éxito",
                "numcot" => $numcot
            ));
        return $response->returnToJson();
    }
}