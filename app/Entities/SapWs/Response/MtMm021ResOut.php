<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 12:37
 */

namespace App\Entities\SapWs\Response;

use App\Entities\SapWs\Response\ResOut;
use Illuminate\Database\Query\Builder;
use DB;
use Orchestra\Parser\Xml\Facade as XmlParser;
use SimpleXMLElement;

class MtMm021ResOut extends ResOut
{
    private $xml;
    private $xml_stock;
    private $xml_return;
    private $many_products;

    public function __construct($xml_string)
    {
        $this->many_products = false;
        if($xml_string) {
            // Cargar XML
            $this->xml = simplexml_load_string($xml_string)
                ->children('SOAP', true)->Body
                ->children('ns0', true)->MT_MM021_RES_OUT;

            foreach ($this->xml->xpath('STOCK_MAT') as $elementoStock) {
                $this->xml_stock[] = new SimpleXMLElement($elementoStock->asXML());
            }
            foreach ($this->xml->xpath('RETURN') as $elementoReturn) {
                $this->xml_return[] = new SimpleXMLElement($elementoReturn->asXML());
            }
        }
    }

    public function getXmlStock()
    {
        return $this->xml_stock;
    }

    public function getXmlStockFormateado()
    {
        $arrayReturn = null;
        if($this->xml_stock != null) {
            foreach ($this->xml_stock as $item) {
                $arrayReturn[] = array(
                    'codsap' => ($item->MATNR) ? round(doubleval($item->MATNR . ''), 0) : null,
                    'material'=> ($item->MATNR) ? $item->MATNR . '' : null,
                    'almacen' => ('' . $item->LGORT) ? '' . $item->LGORT : null,
                    'centro' => ('' . $item->WERKS) ? '' . $item->WERKS : null,
                    'stock_fisico' => (intval('' . $item->LABST) >= 0 ) ? intval('' . $item->LABST) : 0,
                    'stock_compro' => (intval('' . $item->STOCK_COM) >= 0 ) ? intval('' . $item->STOCK_COM) : 0,
                    'stock_dispon' => intval('' . $item->STOCK_DIS),
                    'costo_promedio' => intval('' . $item->VERPR),
                );
            }
        }
        return $arrayReturn;
    }

    public function getXmlReturn()
    {
        return $this->xml_return;
    }

    public function getMensajeReturn()
    {
        if ($this->xml_return == null) {
            return null;
        }

        return $this->xml_return->MSG . '';
    }

    public function isManyProducts()
    {
        return $this->many_products;
    }

    public function getStockDisponible()
    {
        return (int)$this->xml_stock->STOCK_DIS;
    }

    public function getStockComprometido()
    {
        return (int)$this->xml_stock->STOCK_COM;
    }

    public function getCostoPromedio()
    {
        return (int)$this->xml_stock->VERPR;
    }

    public function countStock()
    {
        return count($this->xml->xpath('STOCK_MAT'));
    }
}