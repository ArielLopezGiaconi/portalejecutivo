<?php

namespace App\Http\Controllers\back;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\Controller;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;


class GestionesBackController extends Controller
{
    public function CambioClave(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $codusu = $usuario->get('codusu');
        $tipo = $request->get('tipo');
        $claveOld = $request->get('claveOld');
        $claveNew = $request->get('claveNew');

        $resultado = oraclePDO::cambioClave($tipo, strtoupper($claveOld), strtoupper($claveNew), $rutusu, $codusu);

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function subirImagen(Request $request)
    {
        
        $this->validate($request, [
            'photo' => 'required|image'
        ]);
    
        $file = $request->file('photo');
        $rutcli = $request->get('rut');

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($request);
        return $respuesta->returnToJson();
    
    }

    public function getReporteProductividad()
    {
        $url = PortalEjUsuariosPDO::getUrlPBI(30);
        return view('back.pages.ejecutivo.ReporteProductividad', array('url' => $url));
    }
    
    public function decode(Request $request)
    {
        try{

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(200)
                ->setResponse(rawurldecode($request->input('url')));
                
            return $FormatoRespuesta->returnToJson();

        } catch (\Exception $e) {
            return 'error';
        }
    }

    public function getDerechos()
    {
        $derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        
        Session::put('derechos', $derechos);

        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($derechos);
        return $respuesta->returnToJson();

    }

    public function descargaComisiones(request $request, $fecha)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        // $fecha = $request->get('fecha');

        $respuesta = PortalEjUsuariosPDO::Comisiones($rutusu, $fecha);

        $arrayData = array();
        $i = 1;
        //$arrayData[0]['rutcli'] = "Rut";
        $arrayData[0]['razons'] = "Razon social";
        $arrayData[0]['tipo_doc'] = "Tipo de documento";
        $arrayData[0]['fecha_documento'] = "Fecha documento";
        $arrayData[0]['foliodoc'] = "Folio documento";
        $arrayData[0]['numnvt'] = "Nota de venta";
        $arrayData[0]['neto'] = "Neto";
        $arrayData[0]['comision_total'] = "Comisión total";
        $arrayData[0]['comision_A'] = "Comisión A";
        $arrayData[0]['comision_B'] = "Comisión B";
        $arrayData[0]['comision_C'] = "Comision C";
        $arrayData[0]['comision_D'] = "Comision D";
        foreach ($respuesta as $data) {
            //$arrayData[$i]['rutcli'] = $data->rutcli;
            $arrayData[$i]['razons'] = $data->razons;
            $arrayData[$i]['tipo_doc'] = $data->tipo_doc;
            $arrayData[$i]['fecha_documento'] = $data->fecha_documento;
            $arrayData[$i]['foliodoc'] = $data->foliodoc;
            $arrayData[$i]['numnvt'] = $data->numnvt;
            $arrayData[$i]['neto'] = $data->neto;
            $arrayData[$i]['comision_total'] = $data->comision_total;
            $arrayData[$i]['comision_A'] = $data->comision_A;
            $arrayData[$i]['comision_B'] = $data->comision_B;
            $arrayData[$i]['comision_C'] = $data->comision_C;
            $arrayData[$i]['comision_D'] = $data->comision_D;
            $i++;
        }

        Excel::create('Comisiones' . $fecha, function ($excel) use ($arrayData) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('comisiones');
            $excel->setCreator('BusinessIntelligenceDimerc')->setCompany('Dimerc S.A');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($arrayData) {
                $sheet->fromArray($arrayData, null, 'A1', false, false);
            });
        })->download('xlsx');

        return 'ok';

    }

}