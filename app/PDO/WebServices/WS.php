<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 15:23
 */

namespace App\PDO\WebServices;

use App\PDO\MySql\ApiResources\WsSapLogPDO;
use GuzzleHttp\Client;
use App\PDO\Lib\ResponseFormatt;
use GuzzleHttp\Psr7\Request;
// use \HttpRequest;
use App\PDO\Lib\HttpRequestGeneric;

class WS
{
    public function consumeWSHttp($xml_input, $url_ws, array $url_querystring = null, $url_method = 'POST')
    {
        // 0.- Credenciales
        $credenciales = base64_encode(
            session('sap_user') .
            ":" .
            session('sap_pass')
        );

        // 1.- Request
        $request = new HttpRequestGeneric();// \HttpRequest();

        // 2.- Asignar parámetros
        $request->setUrl($url_ws);
        $request->setMethod(3);
        $request->setQueryData($url_querystring);
        $request->setHeaders(array(
            'Cache-Control' => 'no-cache',
            //'Authorization' => 'Basic cWFzcG86RGVsb2l0dGUyMDE5',
            'Authorization' => 'Basic ' . $credenciales,
            'Content-Type' => 'application/xml'
        ));

        $request->setBody($xml_input);

        // 3.- Enviar solicitud
        $response = $request->send();

        // 4.- Devolver respuesta
        // return dd($request);
        return $response;
    }

    public function consumeWSCurl($xml_input, $url_fullws,  $url_method = 'POST', $interface = null) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "50000",
            CURLOPT_URL => $url_fullws,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 900,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $url_method,
            CURLOPT_POSTFIELDS => $xml_input,
            CURLOPT_HTTPHEADER => array(
                'Cache-Control: no-cache',
                'Content-Type: application/xml',
                // 'Postman-Token: 23e3a6a8-3283-4bb5-b5c2-3c1b4533e508'
            ),
            CURLOPT_USERPWD => session('sap_user').':'.session('sap_pass')
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        // Guardar LOG
        $respuesta_log = WsSapLogPDO::insertLog($url_fullws, $xml_input, $response, $interface);

        // Retornar respuesta final
        return $response;
    }

    public function consumeWSClient($xml_input, $url_ws, array $url_querystring = null, $url_method = HTTP_METH_POST)
    {
        $credenciales = base64_encode(session('sap_user') . ":" . session('sap_pass'));
        $header = array(
            'Cache-Control' => 'no-cache',
            //'Authorization' => 'Basic cWFzcG86RGVsb2l0dGUyMDE5',
            'Authorization' => 'Basic ' . $credenciales,
            'Content-Type' => 'application/xml'
        );


        $request = new Request($url_method, $url_ws, $header, $xml_input);
        return null;


    }
}