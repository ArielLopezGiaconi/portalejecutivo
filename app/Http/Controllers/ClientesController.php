<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 13:40
 */

namespace App\Http\Controllers;

use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\ReDDesCliPDO;

class ClientesController extends Controller
{
    public function buscarPorRazonSocial($in_razon, $codemp = 3, $usuario)
    {
        // 1.- Buscar en base de datos
        $clientes = EnClientePDO::buscarClientesPorRazonSocial($in_razon, $codemp, true, $usuario);

        // RESPUESTA FINAL
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($clientes);

        return $respuesta->returnToJson();
    }

    public function buscarEspecificoPorRut($in_rutcli, $in_codemp = 3, $usuario)
    {
        // 1.- Buscar cliente específico en base de datos
        $clientes = EnClientePDO::getFullClientePorRut($in_rutcli, $in_codemp, $usuario, true);
        if (!$clientes) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(301)->setResponse("No se encontró el RUT en la base de datos");
            return $respuesta->returnToJson();
        }

        // 2.- Buscar centros de costos del cliente
        $cencos_list = DeClientePDO::getCencosByRutcliCustom1($in_rutcli, $in_codemp, $usuario);
        if (count($cencos_list) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(301)->setResponse("Cliente sin centros de costo asociados");
            return $respuesta->returnToJson();
        }


        // 3.- Buscar direcciones de despacho
        $dirdes_list = ReDDesCliPDO::getDireccionesPorRutCustom1($in_rutcli);
        if(!$dirdes_list) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(301)->setResponse("Cliente sin direcciones asociadas");
            return $respuesta->returnToJson();
        }

        // 4.- Solo guardar información relevante
        $arrayReturn = null;
        $arrayCencos = null;
        foreach ($cencos_list as $cencos) {
            $arrayCencos[] = array(
                'cen_cos' => $cencos->cencos,
                'des_cco' => $cencos->descco,
                'dir_cod' => $cencos->coddir,
                'dir_des' => $cencos->dirdes,
                'dir_com_cod' => $cencos->codcom,
                'dir_com_des' => $cencos->descom,
                'dir_ciu_cod' => $cencos->codciu,
                'dir_ciu_des' => $cencos->desciu,
                'dir_reg_cod' => $cencos->codreg,
                'dir_reg_des' => $cencos->desreg,
            );
        }

        $arrayDirdes = null;
        foreach ($dirdes_list as $dirdes) {
            $arrayDirdes[] = array(
                'dir_cod' => $cencos->coddir,
                'dir_des' => $cencos->dirdes,
                'com_cod' => $cencos->codcom,
                'com_des' => $cencos->descom,
                'ciu_cod' => $cencos->codciu,
                'ciu_des' => $cencos->desciu,
                'reg_cod' => $cencos->codreg,
                'reg_des' => $cencos->desreg,
            );
        }

        $arrayReturn = array(
            'rutcli' => $clientes->getRutcli(),
            'razons' => $clientes->getRazons(),
            'digcli' => $clientes->getDigcli(),
            'segmento' => $clientes->getSegmentoCliente(),
            'negocio_eerr' => $clientes->getNegocioEerr(),
            'forpag' => $clientes->getFormaPago(),
            'plapag' => $clientes->getPlazoPago(),
            'estado_cliente' => $clientes->getEstadoCliente(),
            'cencos' => $arrayCencos,
            'dirdes' => $arrayDirdes
        );


        // RESPUESTA FINAL
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($arrayReturn);

        return $respuesta->returnToJson();
    }
}