@extends('back.master.masterpage')
@section('contenido')
@php
    setlocale(LC_TIME, "spanish");

    $meses = array();
    $meses['01'] = 'Enero';
    $meses['02'] = 'Febrero';
    $meses['03'] = 'Marzo';
    $meses['04'] = 'Abril';
    $meses['05'] = 'Mayo';
    $meses['06'] = 'Junio';
    $meses['07'] = 'Julio';
    $meses['08'] = 'Agosto';
    $meses['09'] = 'Septiembre';
    $meses['10'] = 'Octubre';
    $meses['11'] = 'Noviembre';
    $meses['12'] = 'Diciembre';

    $mesesShort = array();
    $mesesShort['01'] = 'Ene';
    $mesesShort['02'] = 'Feb';
    $mesesShort['03'] = 'Mar';
    $mesesShort['04'] = 'Abr';
    $mesesShort['05'] = 'May';
    $mesesShort['06'] = 'Jun';
    $mesesShort['07'] = 'Jul';
    $mesesShort['08'] = 'Ago';
    $mesesShort['09'] = 'Sep';
    $mesesShort['10'] = 'Oct';
    $mesesShort['11'] = 'Nov';
    $mesesShort['12'] = 'Dic';

    /* Obtener foto perfil */
    $rutusuario = Session::get('usuario')->get('rutusu');
    if(is_null(env('CUSTOM_PUBLIC_PATH'))) {
    	$fotoperfil = public_path().'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
	}else {
		$fotoperfil = env('CUSTOM_PUBLIC_PATH').'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
	}

    if(file_exists($fotoperfil)){
        $fotoperfil = asset('assets/images/fotoperfil/'.$rutusuario.'.jpg');
    }
    else{
        $fotoperfil = asset('assets/images/default1.jpg');
    }

@endphp

<div class="main-body">
    <div class="page-wrapper">

        <div class="page-body">
            <div>
                <!-- dejar para portadaaa -->
                <div>
                    <div>
                        <div class="col-xl-12 bg-inverse rounded">
                            <div class="card-block text-white">
                                
                                <div class="row">
                                    <div class="row-lg-9 ">
                                        <img id="fotoPerfil" class="img-fluid img-radius" src="{{ $fotoperfil }}" alt="round-img" style="width: 100px; height: 100px; border-radius: 50px;">
                                    </div>
                                    <label name="{{ $rutusuario }}" id="rut"></label>

                                    <div class="card-block row-lg-9">
                                        <i class="icofont icofont-id f-20 middle m-l-10"></i>
                                        <h7 class="f-w-600">{{ Session::get('usuario')->get('userid') }}</h7>
                                        <br>
                                        <h7 class="f-w-700 m-t-5"> {{ ucwords(strtolower(Session::get('usuario')->get('nombre').' '.Session::get('usuario')->get('apepat'))) }}</h7>
                                        <br>
                                        <h7>Grupo {{ Session::get('usuario')->get('codcnl') }}</h7>
                                        {{-- <h7>Trabaja desde {{ date('d-M-Y', strtotime(Session::get('usuario')->get('fecing'))) }}</h7> --}}
                                    </div>
                                    <br>
                                    <div class="card-block text-white text-center col-lg-3 rounded">
                                        <h7 class="">POTENCIAL DE CARTERA</h7>
                                        <br>
                                        <h4 class="f-w-600 center m-t-10" id="potencial"></h4>
                                        
                                        <h6 class="m-t-10" id="abarcado"></h6>
                                    </div>
                                    
                                    <div class="card-block text-white text-center col-lg-2 rounded">
                                        <h7 class="text-uppercase">Ventas {{ ucwords(strftime("%B")) }} {{ date('Y') }}</h7>
                                        <br>
                                        <h4 class="f-w-600 m-t-10" id="venta"></h4>
                                    </div>
                                    <!-- <div class="row-lg-9 row-sm-4 m-l-10 m-t-15">
                                        <table class="table-custom table-styling bg-white text-inverse rounded">
                                            <thead>
                                                <tr class="text-inverse">
                                                    <th>Item</th>
                                                    <th>$</th>
                                                </tr>
                                            </thead>
                                            <tbody id="cuadroComisiones">
                                            </tbody>
                                        </table>
                                    </div> -->
                                    <div class="col-lg-4">
                                        <div class="card-block text-center">
                                            <span class="d-block text-c-white f-30">$<span id="Meta" name="tx_meta" class="f-30">999.999.999</span> <i class="fa fa-flag-checkered f-35"></i></span>
                                            <p class="m-b-0 text-white">META <strong>{{ strtoupper(strftime("%B")) }}</strong></p>
                                            <div class="progress m-t-10">
                                                <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated" max="100" id="barra_cumplimiento" style="width:0%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div id="carouselExampleIndicators" class="carousel slide col-lg-2" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                                                <div class="d-block" >
                                                    <div class="card bg-c-pink text-white widget-visitor-card">
                                                        <div class="card-block-small text-center">
                                                            <h2 id="dinero" >1,658</h2>
                                                            <h6>Venta Total</h6>
                                                            <i class="icon-wallet"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="d-block">
                                                    <div class="card bg-c-pink text-white widget-visitor-card">
                                                        <div class="card-block-small text-center">
                                                            <h2 id="boleta" >1,658</h2>
                                                            <h6>Venta Boleta</h6>
                                                            <i class="fa fa-ticket"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="d-block">
                                                    <div class="card bg-c-pink text-white widget-visitor-card">
                                                        <div class="card-block-small text-center">
                                                            <h2 id="antofagasta" >1,658</h2>
                                                            <h6>Venta Antofagasta</h6>
                                                            <i class="ti-map-alt"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="d-block">
                                                    <div class="card bg-c-pink text-white widget-visitor-card">
                                                        <div class="card-block-small text-center">
                                                            <h2 id="cajas" >1,658</h2>
                                                            <h6>Venta Cajas</h6>
                                                            <i class="fa fa-dropbox"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="d-block">
                                                    <div class="card bg-c-pink text-white widget-visitor-card">
                                                        <div class="card-block-small text-center">
                                                            <h2  id="web" >1,658</h2>
                                                            <h6>Porcentaje Web</h6>
                                                            <i class="icofont icofont-web"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div> --}}
                                    {{-- <div class="row-lg-9">
                                        <div class="card-block text-center">
                                            <span class="d-block text-c-white f-30">$<span id="dinero" name="tx_meta" class="f-30">999.999.999</span> <i class="fa fa-money text-c-black f-35"></i></span>
                                        </div>
                                    </div> --}}
                                    
                                    {{-- <div class="card-footer bg-c-green">
                                        <span class="f-11">Total ventas {{ ucwords(strftime("%B")) }} {{ date('Y') }}</span>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <span class="text-white f-14 f-left m-t-5">Venta</span>
                                            </div>
                                            <div class="col-xl-6">
                                                <span class="f-20 f-right f-w-600"></span><span id="venta" name="tx_venta">0.000.000</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <span class="text-white f-14 f-left m-t-5">Cumplimiento</span>
                                            </div>
                                            <div class="col-xl-6">
                                                <span id="Cumplimiento" class="f-20 f-right f-w-600"> 0</span><span name="tx_cumplimiento"></span>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div id="elPotencial" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">El Potencial</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cálculo matemático que define el máximo de ventas que puede tener un cliente</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="elPotencial100" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">sobre el 100%</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Venta promedio esta sobre el potencial</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="elPotencial70" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">sobre el 70%</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Venta promedio abarca mas del 70% sobre el potencial</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="elPotencial50" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">sobre el 50%</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Venta promedio abarca mas del 50% sobre el potencial</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="elPotencial30" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">bajo el 30%</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Venta promedio abarca menos del 30% sobre el potencial</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="elPotencialSin" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Sin Potencial</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Clientes que no tienen potencial</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Cliente" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Cliente</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Ha tenido a lo menos 1 venta en los últimos 4 meses</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Registro" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Registro</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cliente sin venta en los últimos 4 meses</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Rendida" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Rendida</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Documento con recepción conforme del cliente</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Notificada" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Notificada</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Estado del pedido (  Incidentada / Entregada)</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Registro" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Registro</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cliente sin venta en los últimos 4 meses</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Despachada" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Despachada</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Pedido en Ruta</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="FolioAsociado" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Folio Asociado</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Solicitud de retiro a mercadería del cliente</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Preparacion" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Preparación</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Proceso de armado de pedido</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="Generada" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Nota Generada</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Creación de pedido</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Activa" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Cotización Activa</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cotización esta dentro del plazo para ser vendida</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Efectiva" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Cotización Efectiva</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Se vendio a lo menos un producto de la cotización</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Vencida" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Cotización Vencida</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">precios entregados al cliente en la cotización ya no están vigentes. </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Activo" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Credito Activo</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cliente fue aprobado para la venta por el área de crédito</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Bloqueado" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Credito Bloqueado</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cliente fue bloqueado para la venta por el área de crédito</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="SinAnalisis" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Cotización Sin análisis</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Cliente no ha sido analizado por el área de crédito</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="DeudaActiva" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Deuda Activa</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Monto de facturas que no han vencido</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="DeudaVencida" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <h2 class="modal-title card-block text-center">Deuda Vencida</h2>
                                <div class="card-block text-center">
                                    <h3 class="modal-title">Monto de facturas vencidas</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="user-card-full m-t-10" style="background-color: transparent;">
                        <div class="row">
                            <div class="col-xl-7">
                                <table class="table-custom table-hover table-bordered table-styling bg-white" style="border: 2px solid #546686; width: 100%">
                                    <thead>
                                    <tr class="table-inverse">
                                        <th colspan="1">Resumen Cartera</th>
                                        <th colspan="2">Clientes
                                        <i class="fa fa-info-circle" data-toggle="modal" data-target="#Cliente"></th>
                                        <th colspan="2">Registros
                                        <i class="fa fa-info-circle" data-toggle="modal" data-target="#Registro"></th>
                                        <th colspan="2">total</th>
                                    </tr>
                                    <tr class="table-inverse">
                                        <th class="width-200 pull-left">Potencial 
                                        <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial"></i>
                                        </th>
                                        <th>Q</th>
                                        <th>Monto</th>
                                        <th>Q</th>
                                        <th>Monto</th>
                                        <th>Q</th>
                                        <th>Monto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a id="PotencialSobre100" href="{{ url('cartera/1') }}">1- Rango sobre el 100%</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial100"></i></td>
                                            <td class="text-right" id="cliCantRango1"></td>
                                            <td class="text-right" id="cliValRango1"></td>
                                            <td class="text-right" id="regiCantRango1"></td>
                                            <td class="text-right" id="regiValRango1"></td>
                                            <td class="text-right" id="totCantRango1"></td>
                                            <td class="text-right" id="totValtRango1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="Potencialsobre70" href="{{ url('cartera/2') }}">2- Rango sobre el 70%</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial70"></i></td>
                                            <td class="text-right" id="cliCantRango2"></td>
                                            <td class="text-right" id="cliValRango2"></td>
                                            <td class="text-right" id="regiCantRango2"></td>
                                            <td class="text-right" id="regiValRango2"></td>
                                            <td class="text-right" id="totCantRango2"></td>
                                            <td class="text-right" id="totValtRango2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="PotencialSobre50" href="{{ url('cartera/3') }}">3- Rango sobre el 50%</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial50"></i></td>
                                            <td class="text-right" id="cliCantRango3"></td>
                                            <td class="text-right" id="cliValRango3"></td>
                                            <td class="text-right" id="regiCantRango3"></td>
                                            <td class="text-right" id="regiValRango3"></td>
                                            <td class="text-right" id="totCantRango3"></td>
                                            <td class="text-right" id="totValtRango3"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="PotencialBajo30" href="{{ url('cartera/4') }}">5- Rango bajo el 30%</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial30"></i></td>
                                            <td class="text-right" id="cliCantRango4"></td>
                                            <td class="text-right" id="cliValRango4"></td>
                                            <td class="text-right" id="regiCantRango4"></td>
                                            <td class="text-right" id="regiValRango4"></td>
                                            <td class="text-right" id="totCantRango4"></td>
                                            <td class="text-right" id="totValtRango4"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="PotencialSinPotencial" href="{{ url('cartera/5') }}">6- Sin Potencial</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencialSin"></i></td>
                                            <td class="text-right" id="cliCantRango5"></td>
                                            <td class="text-right" id="cliValRango5"></td>
                                            <td class="text-right" id="regiCantRango5"></td>
                                            <td class="text-right" id="regiValRango5"></td>
                                            <td class="text-right" id="totCantRango5"></td>
                                            <td class="text-right" id="totValtRango5"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a><b>Totales</b></a></td>
                                            <td class="text-right" ><b id="cliCantRangoTotal"></td>
                                            <td class="text-right" ><b id="cliValRangoTotal"></td>
                                            <td class="text-right" ><b id="regiCantRangoTotal"></td>
                                            <td class="text-right" ><b id="regiValRangoTotal"></td>
                                            <td class="text-right" ><b id="totCantRangoTotal"></td>
                                            <td class="text-right" ><b id="totValtRangoTotal"></td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr class="table-inverse">
                                            <th colspan="1">Resumen Cartera</th>
                                            <th colspan="2">Clientes</th>
                                            <th colspan="2">Registros</th>
                                            <th colspan="2">total</th>
                                        </tr>
                                        <tr class="table-inverse">
                                            <th class="width-150">Notas</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a id="NotasRendida" href="{{ url('notas/1') }}">1- Rendida</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Rendida"></i></td>
                                            <td class="text-right" id="cliCantNota1"></td>
                                            <td class="text-right" id="cliValNota1"></td>
                                            <td class="text-right" id="regiCantNota1"></td>
                                            <td class="text-right" id="regiValNota1"></td>
                                            <td class="text-right" id="totCantNota1"></td>
                                            <td class="text-right" id="totValtNota1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="NotasNotificada" href="{{ url('notas/2') }}">2- Notificada</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Notificada"></i></td>
                                            <td class="text-right" id="cliCantNota2"></td>
                                            <td class="text-right" id="cliValNota2"></td>
                                            <td class="text-right" id="regiCantNota2"></td>
                                            <td class="text-right" id="regiValNota2"></td>
                                            <td class="text-right" id="totCantNota2"></td>
                                            <td class="text-right" id="totValtNota2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="NotasDespachada" href="{{ url('notas/3') }}">3- Despachada</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Despachada"></i></td>
                                            <td class="text-right" id="cliCantNota3"></td>
                                            <td class="text-right" id="cliValNota3"></td>
                                            <td class="text-right" id="regiCantNota3"></td>
                                            <td class="text-right" id="regiValNota3"></td>
                                            <td class="text-right" id="totCantNota3"></td>
                                            <td class="text-right" id="totValtNota3"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="NotasFolioAsociado" href="{{ url('notas/4') }}">4- Folio Asociado</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#FolioAsociado"></i></td>
                                            <td class="text-right" id="cliCantNota4"></td>
                                            <td class="text-right" id="cliValNota4"></td>
                                            <td class="text-right" id="regiCantNota4"></td>
                                            <td class="text-right" id="regiValNota4"></td>
                                            <td class="text-right" id="totCantNota4"></td>
                                            <td class="text-right" id="totValtNota4"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="NotasPreparacion" href="{{ url('notas/5') }}">5- Preparacion</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Preparacion"></i></td>
                                            <td class="text-right" id="cliCantNota5"></td>
                                            <td class="text-right" id="cliValNota5"></td>
                                            <td class="text-right" id="regiCantNota5"></td>
                                            <td class="text-right" id="regiValNota5"></td>
                                            <td class="text-right" id="totCantNota5"></td>
                                            <td class="text-right" id="totValtNota5"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="NotasGenerada" href="{{ url('notas/6') }}">6- Generada</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Generada"></i></td>
                                            <td class="text-right" id="cliCantNota6"></td>
                                            <td class="text-right" id="cliValNota6"></td>
                                            <td class="text-right" id="regiCantNota6"></td>
                                            <td class="text-right" id="regiValNota6"></td>
                                            <td class="text-right" id="totCantNota6"></td>
                                            <td class="text-right" id="totValtNota6"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b><a>Totales</a></b></td>
                                            <td class="text-right"><b id="cliCantNotaTotal"></b></td>
                                            <td class="text-right"><b id="cliValNotaTotal"></b></td>
                                            <td class="text-right"><b id="regiCantNotaTotal"></b></td>
                                            <td class="text-right"><b id="regiValNotaTotal"></b></td>
                                            <td class="text-right"><b id="totCantNotaTotal"></b></td>
                                            <td class="text-right"><b id="totValtNotaTotal"></b></td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr class="table-inverse">
                                            <th colspan="1">Resumen Cartera</th>
                                            <th colspan="2">Clientes</th>
                                            <th colspan="2">Registros</th>
                                            <th colspan="2">total</th>
                                        </tr>
                                        <tr class="table-inverse">
                                            <th class="width-150 text-left">Cotizaciones</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a id="CotizacionesActiva" href="{{ url('cotizaciones/1') }}">Activa</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Activa"></i></td>
                                            <td class="text-right" id="cliCantCoti1"></td>
                                            <td class="text-right" id="cliValCoti1"></td>
                                            <td class="text-right" id="regiCantCoti1"></td>
                                            <td class="text-right" id="regiValCoti1"></td>
                                            <td class="text-right" id="totCantCoti1"></td>
                                            <td class="text-right" id="totValtCoti1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="CotizacionesEfectiva" href="{{ url('cotizaciones/2') }}">Efectiva</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Efectiva"></i></td>
                                            <td class="text-right" id="cliCantCoti2"></td>
                                            <td class="text-right" id="cliValCoti2"></td>
                                            <td class="text-right" id="regiCantCoti2"></td>
                                            <td class="text-right" id="regiValCoti2"></td>
                                            <td class="text-right" id="totCantCoti2"></td>
                                            <td class="text-right" id="totValtCoti2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="CotizacionesVencida" href="{{ url('cotizaciones/3') }}">Vencida</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Vencida"></i></td>
                                            <td class="text-right" id="cliCantCoti3"></td>
                                            <td class="text-right" id="cliValCoti3"></td>
                                            <td class="text-right" id="regiCantCoti3"></td>
                                            <td class="text-right" id="regiValCoti3"></td>
                                            <td class="text-right" id="totCantCoti3"></td>
                                            <td class="text-right" id="totValtCoti3"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b><a>Totales</a></b></td>
                                            <td class="text-right"><b id="cliCantCotiTotal"></b></td>
                                            <td class="text-right"><b id="cliValCotiTotal"></b></td>
                                            <td class="text-right"><b id="regiCantCotiTotal"></b></td>
                                            <td class="text-right"><b id="regiValCotiTotal"></b></td>
                                            <td class="text-right"><b id="totCantCotiTotal"></b></td>
                                            <td class="text-right"><b id="totValtCotiTotal"></b></td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr class="table-inverse">
                                            <th colspan="1">Resumen Cartera</th>
                                            <th colspan="2">Clientes</th>
                                            <th colspan="2">Registros</th>
                                            <th colspan="2">total</th>
                                        </tr>
                                        <tr class="table-inverse">
                                            <th class="width-150 text-left">Status Credito</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a id="CreditoActivo" href="{{ url('cartera/11') }}">Activo</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Activo"></i></td>
                                            <td class="text-right" id="cliCantCredi1"></td>
                                            <td class="text-right" id="cliValCredi1"></td>
                                            <td class="text-right" id="regiCantCredi1"></td>
                                            <td class="text-right" id="regiValCredi1"></td>
                                            <td class="text-right" id="totCantCredi1"></td>
                                            <td class="text-right" id="totValtCredi1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="CreditoBloqueado" href="{{ url('cartera/12') }}">Bloqueado</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Bloqueado"></i></td>
                                            <td class="text-right" id="cliCantCredi2"></td>
                                            <td class="text-right" id="cliValCredi2"></td>
                                            <td class="text-right" id="regiCantCredi2"></td>
                                            <td class="text-right" id="regiValCredi2"></td>
                                            <td class="text-right" id="totCantCredi2"></td>
                                            <td class="text-right" id="totValtCredi2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="CreditoSinAnalisis" href="{{ url('cartera/13') }}">Sin analisis</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#SinAnalisis"></i></td>
                                            <td class="text-right" id="cliCantCredi3"></td>
                                            <td class="text-right" id="cliValCredi3"></td>
                                            <td class="text-right" id="regiCantCredi3"></td>
                                            <td class="text-right" id="regiValCredi3"></td>
                                            <td class="text-right" id="totCantCredi3"></td>
                                            <td class="text-right" id="totValtCredi3"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a><b>Totales</b></a></td>
                                            <td class="text-right"><b id="cliCantCrediTotales"></td>
                                            <td class="text-right"><b id="cliValCrediTotales"></td>
                                            <td class="text-right"><b id="regiCantCrediTotales"></td>
                                            <td class="text-right"><b id="regiValCrediTotales"></td>
                                            <td class="text-right"><b id="totCantCrediTotales"></td>
                                            <td class="text-right"><b id="totValtCrediTotales"></td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr class="table-inverse">
                                            <th colspan="1">Resumen Cartera</th>
                                            <th colspan="2">Clientes</th>
                                            <th colspan="2">Registros</th>
                                            <th colspan="2">total</th>
                                        </tr>
                                        <tr class="table-inverse">
                                            <th class="width-150 text-left">Deuda</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                            <th>Q</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><a id="DeudaActiva" href="{{ url('cartera/21') }}">Activa</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaActiva"></i></td>
                                            <td class="text-right" id="cliCantDeuda1"></td>
                                            <td class="text-right" id="cliValDeuda1"></td>
                                            <td class="text-right" id="regiCantDeuda1"></td>
                                            <td class="text-right" id="regiValDeuda1"></td>
                                            <td class="text-right" id="totCantDeuda1"></td>
                                            <td class="text-right" id="totValtDeuda1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a id="DeudaVencida" href="{{ url('cartera/22') }}">Vencida</a>
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaVencida"></i></td>
                                            <td class="text-right" id="cliCantDeuda2"></td>
                                            <td class="text-right" id="cliValDeuda2"></td>
                                            <td class="text-right" id="regiCantDeuda2"></td>
                                            <td class="text-right" id="regiValDeuda2"></td>
                                            <td class="text-right" id="totCantDeuda2"></td>
                                            <td class="text-right" id="totValtDeuda2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><a><b>Totales</b></a></td>
                                            <td class="text-right"><b id="cliCantDeudaTotales"></td>
                                            <td class="text-right"><b id="cliValDeudaTotales"></td>
                                            <td class="text-right"><b id="regiCantDeudaTotales"></td>
                                            <td class="text-right"><b id="regiValDeudaTotales"></td>
                                            <td class="text-right"><b id="totCantDeudaTotales"></td>
                                            <td class="text-right"><b id="totValtDeudaTotales"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xl-5 rounded">
                                <div class="table-responsive" id="tablaVentas">
                                    <table class="table-custom table-hover table-bordered table-styling bg-white" style="border: 2px solid #546686; width: 100%">
                                        <thead>
                                            <tr class="table-inverse">
                                                <th colspan="4">Ventas</th>
                                            </tr>
                                            <tr class="table-inverse">
                                                <th>Mes</th>
                                                <th>Año</th>
                                                <th>Venta Cartera</th>
                                                <th>Venta Ejecutivo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-left" id="Mes1"><a></a></td>
                                                <td class="text-right" id="Ano1"></td>
                                                <td class="text-right" id="Cartera1"></td>
                                                <td class="text-right" id="Ejecutivo1"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes2"><a></a></td>
                                                <td class="text-right" id="Ano2"></td>
                                                <td class="text-right" id="Cartera2"></td>
                                                <td class="text-right" id="Ejecutivo2"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes3"><a></a></td>
                                                <td class="text-right" id="Ano3"></td>
                                                <td class="text-right" id="Cartera3"></td>
                                                <td class="text-right" id="Ejecutivo3"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes4"><a></a></td>
                                                <td class="text-right" id="Ano4"></td>
                                                <td class="text-right" id="Cartera4"></td>
                                                <td class="text-right" id="Ejecutivo4"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes5"><a></a></td>
                                                <td class="text-right" id="Ano5"></td>
                                                <td class="text-right" id="Cartera5"></td>
                                                <td class="text-right" id="Ejecutivo5"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes6"><a></a></td>
                                                <td class="text-right" id="Ano6"></td>
                                                <td class="text-right" id="Cartera6"></td>
                                                <td class="text-right" id="Ejecutivo6"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes7"><a></a></td>
                                                <td class="text-right" id="Ano7"></td>
                                                <td class="text-right" id="Cartera7"></td>
                                                <td class="text-right" id="Ejecutivo7"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes8"><a></a></td>
                                                <td class="text-right" id="Ano8"></td>
                                                <td class="text-right" id="Cartera8"></td>
                                                <td class="text-right" id="Ejecutivo8"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes9"><a></a></td>
                                                <td class="text-right" id="Ano9"></td>
                                                <td class="text-right" id="Cartera9"></td>
                                                <td class="text-right" id="Ejecutivo9"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes10"><a></a></td>
                                                <td class="text-right" id="Ano10"></td>
                                                <td class="text-right" id="Cartera10"></td>
                                                <td class="text-right" id="Ejecutivo10"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes11"><a></a></td>
                                                <td class="text-right" id="Ano11"></td>
                                                <td class="text-right" id="Cartera11"></td>
                                                <td class="text-right" id="Ejecutivo11"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes12"><a></a></td>
                                                <td class="text-right" id="Ano12"></td>
                                                <td class="text-right" id="Cartera12"></td>
                                                <td class="text-right" id="Ejecutivo12"></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left" id="Mes13"><a></a></td>
                                                <td class="text-right" id="Ano13"></td>
                                                <td class="text-right" id="Cartera13"></td>
                                                <td class="text-right" id="Ejecutivo13"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- fin portada -->
            </div>
        </div>
    </div>
</div>


    

@include('back.others.js.ejecutivo')
@include('back.others.js.portal')
@endsection
@section('custom-includes')
    <script>

        /*
        Module: portada -> 1
        Action: clic en cartera -> 1
                clic en ranking -> 2
                clic en campañas -> 3

        */
        // $('#btn_cartera').click(function(){
        //     var userid='{{ Session::get('usuario')->get('codusu') }}';
        //     var moduleid=1;
        //     var actionid=1;
        //     var oldvalue='';
        //     var newvalue='';
        //     registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
        // });
        // $('#btn_ranking').click(function(){
        //     var userid='{{ Session::get('usuario')->get('codusu') }}';
        //     var moduleid=1;
        //     var actionid=2;
        //     var oldvalue='';
        //     var newvalue='';
        //     registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
        // });
        // $('#btn_campanas').click(function(){
        //     var userid='{{ Session::get('usuario')->get('codusu') }}';
        //     var moduleid=1;
        //     var actionid=3;
        //     var oldvalue='';
        //     var newvalue='';
        //     registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
        // });


        function invocaRegistroBitacoraLogin(){
            /*
            Module: login -> 0
            Action: inicia sesion -> 0
            */

            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=0;
            var actionid=0;
            var oldvalue='';
            var newvalue='';

            registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
        }

        $(window).on('load',function(){
            function notify(message, type){
                $.growl({
                    message: message
                },{
                    type: type,
                    allow_dismiss: false,
                    label: 'Cancel',
                    className: 'btn-xs btn-inverse',
                    placement: {
                        from: 'bottom',
                        align: 'right'
                    },
                    delay: 2500,
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    },
                    offset: {
                        x: 30,
                        y: 30
                    }
                });
            };

            var bienv = '{{ Session::get('bienvenida') }}';
            if(bienv)
            {
                notify('Bienvenido al potal del ejecutivo!', 'inverse');
                invocaRegistroBitacoraLogin();
            }

        });

        $(document).ready(function () {
            getDerechos();
            Potencial();
            Resumen();
            ResumenTotales();
            Ventas();
            Meta();
            BuscaComisiones();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
            getDerechos();
            resizeImg();
        });

        $('#NotasRendida').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#NotasNotificada').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#NotasDespachada').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#NotasFolioAsociado').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#NotasPreparacion').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#NotasGenerada').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CotizacionesActiva').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CotizacionesEfectiva').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CotizacionesVencida').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CreditoActivo').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CreditoBloqueado').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#CreditoSinAnalisis').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#DeudaActiva').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#DeudaVencida').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#PotencialSobre100').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#Potencialsobre70').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#PotencialSobre50').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#PotencialBajo30').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#PotencialSinPotencial').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        function ResumenTotales() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/resumenTotales')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        //TOTALES
                        document.getElementById("cliCantRangoTotal").innerHTML = Intl.NumberFormat().format(json.response[0].tot_clientes_cantidad);
                        document.getElementById("cliValRangoTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].tot_clientes_valorizado);
                        document.getElementById("regiCantRangoTotal").innerHTML = Intl.NumberFormat().format(json.response[0].tot_registros_cantidad);
                        document.getElementById("regiValRangoTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].tot_registros_valorizado);
                        document.getElementById("totCantRangoTotal").innerHTML = Intl.NumberFormat().format(json.response[0].tot_total_cantidad);
                        document.getElementById("totValtRangoTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].tot_total_valorizado);

                        document.getElementById("cliCantNotaTotal").innerHTML = Intl.NumberFormat().format(json.response[1].tot_clientes_cantidad);
                        document.getElementById("cliValNotaTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].tot_clientes_valorizado);
                        document.getElementById("regiCantNotaTotal").innerHTML = Intl.NumberFormat().format(json.response[1].tot_registros_cantidad);
                        document.getElementById("regiValNotaTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].tot_registros_valorizado);
                        document.getElementById("totCantNotaTotal").innerHTML = Intl.NumberFormat().format(json.response[1].tot_total_cantidad);
                        document.getElementById("totValtNotaTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].tot_total_valorizado);

                        document.getElementById("cliCantCotiTotal").innerHTML = Intl.NumberFormat().format(json.response[2].tot_clientes_cantidad);
                        document.getElementById("cliValCotiTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].tot_clientes_valorizado);
                        document.getElementById("regiCantCotiTotal").innerHTML = Intl.NumberFormat().format(json.response[2].tot_registros_cantidad);
                        document.getElementById("regiValCotiTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].tot_registros_valorizado);
                        document.getElementById("totCantCotiTotal").innerHTML = Intl.NumberFormat().format(json.response[2].tot_total_cantidad);
                        document.getElementById("totValtCotiTotal").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].tot_total_valorizado);
                        
                        document.getElementById("cliCantCrediTotales").innerHTML = Intl.NumberFormat().format(json.response[3].tot_clientes_cantidad);
                        document.getElementById("cliValCrediTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].tot_clientes_valorizado);
                        document.getElementById("regiCantCrediTotales").innerHTML = Intl.NumberFormat().format(json.response[3].tot_registros_cantidad);
                        document.getElementById("regiValCrediTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].tot_registros_valorizado);
                        document.getElementById("totCantCrediTotales").innerHTML = Intl.NumberFormat().format(json.response[3].tot_total_cantidad);
                        document.getElementById("totValtCrediTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].tot_total_valorizado);
                        
                        document.getElementById("cliCantDeudaTotales").innerHTML = Intl.NumberFormat().format(json.response[4].tot_clientes_cantidad);
                        document.getElementById("cliValDeudaTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].tot_clientes_valorizado);
                        document.getElementById("regiCantDeudaTotales").innerHTML = Intl.NumberFormat().format(json.response[4].tot_registros_cantidad);
                        document.getElementById("regiValDeudaTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].tot_registros_valorizado);
                        document.getElementById("totCantDeudaTotales").innerHTML = Intl.NumberFormat().format(json.response[4].tot_total_cantidad);
                        document.getElementById("totValtDeudaTotales").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].tot_total_valorizado);

                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Resumen() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/resumen')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        //POTENCIALES
                        document.getElementById("cliCantRango1").innerHTML = Intl.NumberFormat().format(json.response[0].clientes_cantidad);
                        document.getElementById("cliValRango1").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].clientes_valorizado);
                        document.getElementById("regiCantRango1").innerHTML = Intl.NumberFormat().format(json.response[0].registros_cantidad);
                        document.getElementById("regiValRango1").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].registros_valorizado);
                        document.getElementById("totCantRango1").innerHTML = Intl.NumberFormat().format(json.response[0].total_cantidad);
                        document.getElementById("totValtRango1").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].total_valorizado);

                        document.getElementById("cliCantRango2").innerHTML = Intl.NumberFormat().format(json.response[1].clientes_cantidad);
                        document.getElementById("cliValRango2").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].clientes_valorizado);
                        document.getElementById("regiCantRango2").innerHTML = Intl.NumberFormat().format(json.response[1].registros_cantidad);
                        document.getElementById("regiValRango2").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].registros_valorizado);
                        document.getElementById("totCantRango2").innerHTML = Intl.NumberFormat().format(json.response[1].total_cantidad);
                        document.getElementById("totValtRango2").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].total_valorizado);

                        document.getElementById("cliCantRango3").innerHTML = Intl.NumberFormat().format(json.response[2].clientes_cantidad);
                        document.getElementById("cliValRango3").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].clientes_valorizado);
                        document.getElementById("regiCantRango3").innerHTML = Intl.NumberFormat().format(json.response[2].registros_cantidad);
                        document.getElementById("regiValRango3").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].registros_valorizado);
                        document.getElementById("totCantRango3").innerHTML = Intl.NumberFormat().format(json.response[2].total_cantidad);
                        document.getElementById("totValtRango3").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].total_valorizado);

                        document.getElementById("cliCantRango4").innerHTML = Intl.NumberFormat().format(json.response[3].clientes_cantidad);
                        document.getElementById("cliValRango4").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].clientes_valorizado);
                        document.getElementById("regiCantRango4").innerHTML = Intl.NumberFormat().format(json.response[3].registros_cantidad);
                        document.getElementById("regiValRango4").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].registros_valorizado);
                        document.getElementById("totCantRango4").innerHTML = Intl.NumberFormat().format(json.response[3].total_cantidad);
                        document.getElementById("totValtRango4").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].total_valorizado);

                        document.getElementById("cliCantRango5").innerHTML = Intl.NumberFormat().format(json.response[4].clientes_cantidad);
                        document.getElementById("cliValRango5").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].clientes_valorizado);
                        document.getElementById("regiCantRango5").innerHTML = Intl.NumberFormat().format(json.response[4].registros_cantidad);
                        document.getElementById("regiValRango5").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].registros_valorizado);
                        document.getElementById("totCantRango5").innerHTML = Intl.NumberFormat().format(json.response[4].total_cantidad);
                        document.getElementById("totValtRango5").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].total_valorizado);

                        //NOTAS DE VENTA
                        document.getElementById("cliCantNota1").innerHTML = Intl.NumberFormat().format(json.response[5].clientes_cantidad);
                        document.getElementById("cliValNota1").innerHTML = "$" + Intl.NumberFormat().format(json.response[5].clientes_valorizado);
                        document.getElementById("regiCantNota1").innerHTML = Intl.NumberFormat().format(json.response[5].registros_cantidad);
                        document.getElementById("regiValNota1").innerHTML = "$" + Intl.NumberFormat().format(json.response[5].registros_valorizado);
                        document.getElementById("totCantNota1").innerHTML = Intl.NumberFormat().format(json.response[5].total_cantidad);
                        document.getElementById("totValtNota1").innerHTML = "$" + Intl.NumberFormat().format(json.response[5].total_valorizado);

                        document.getElementById("cliCantNota2").innerHTML = Intl.NumberFormat().format(json.response[6].clientes_cantidad);
                        document.getElementById("cliValNota2").innerHTML = "$" + Intl.NumberFormat().format(json.response[6].clientes_valorizado);
                        document.getElementById("regiCantNota2").innerHTML = Intl.NumberFormat().format(json.response[6].registros_cantidad);
                        document.getElementById("regiValNota2").innerHTML = "$" + Intl.NumberFormat().format(json.response[6].registros_valorizado);
                        document.getElementById("totCantNota2").innerHTML = Intl.NumberFormat().format(json.response[6].total_cantidad);
                        document.getElementById("totValtNota2").innerHTML = "$" + Intl.NumberFormat().format(json.response[6].total_valorizado);

                        document.getElementById("cliCantNota3").innerHTML = Intl.NumberFormat().format(json.response[7].clientes_cantidad);
                        document.getElementById("cliValNota3").innerHTML = "$" + Intl.NumberFormat().format(json.response[7].clientes_valorizado);
                        document.getElementById("regiCantNota3").innerHTML = Intl.NumberFormat().format(json.response[7].registros_cantidad);
                        document.getElementById("regiValNota3").innerHTML = "$" + Intl.NumberFormat().format(json.response[7].registros_valorizado);
                        document.getElementById("totCantNota3").innerHTML = Intl.NumberFormat().format(json.response[7].total_cantidad);
                        document.getElementById("totValtNota3").innerHTML = "$" + Intl.NumberFormat().format(json.response[7].total_valorizado);

                        document.getElementById("cliCantNota4").innerHTML = Intl.NumberFormat().format(json.response[8].clientes_cantidad);
                        document.getElementById("cliValNota4").innerHTML = "$" + Intl.NumberFormat().format(json.response[8].clientes_valorizado);
                        document.getElementById("regiCantNota4").innerHTML = Intl.NumberFormat().format(json.response[8].registros_cantidad);
                        document.getElementById("regiValNota4").innerHTML = "$" + Intl.NumberFormat().format(json.response[8].registros_valorizado);
                        document.getElementById("totCantNota4").innerHTML = Intl.NumberFormat().format(json.response[8].total_cantidad);
                        document.getElementById("totValtNota4").innerHTML = "$" + Intl.NumberFormat().format(json.response[8].total_valorizado);

                        document.getElementById("cliCantNota5").innerHTML = Intl.NumberFormat().format(json.response[9].clientes_cantidad);
                        document.getElementById("cliValNota5").innerHTML = "$" + Intl.NumberFormat().format(json.response[9].clientes_valorizado);
                        document.getElementById("regiCantNota5").innerHTML = Intl.NumberFormat().format(json.response[9].registros_cantidad);
                        document.getElementById("regiValNota5").innerHTML = "$" + Intl.NumberFormat().format(json.response[9].registros_valorizado);
                        document.getElementById("totCantNota5").innerHTML = Intl.NumberFormat().format(json.response[9].total_cantidad);
                        document.getElementById("totValtNota5").innerHTML = "$" + Intl.NumberFormat().format(json.response[9].total_valorizado);

                        document.getElementById("cliCantNota6").innerHTML = Intl.NumberFormat().format(json.response[10].clientes_cantidad);
                        document.getElementById("cliValNota6").innerHTML = "$" + Intl.NumberFormat().format(json.response[10].clientes_valorizado);
                        document.getElementById("regiCantNota6").innerHTML = Intl.NumberFormat().format(json.response[10].registros_cantidad);
                        document.getElementById("regiValNota6").innerHTML = "$" + Intl.NumberFormat().format(json.response[10].registros_valorizado);
                        document.getElementById("totCantNota6").innerHTML = Intl.NumberFormat().format(json.response[10].total_cantidad);
                        document.getElementById("totValtNota6").innerHTML = "$" + Intl.NumberFormat().format(json.response[10].total_valorizado);

                        //COTIZACIONES
                        document.getElementById("cliCantCoti1").innerHTML = Intl.NumberFormat().format(json.response[11].clientes_cantidad);
                        document.getElementById("cliValCoti1").innerHTML = "$" + Intl.NumberFormat().format(json.response[11].clientes_valorizado);
                        document.getElementById("regiCantCoti1").innerHTML = Intl.NumberFormat().format(json.response[11].registros_cantidad);
                        document.getElementById("regiValCoti1").innerHTML = "$" + Intl.NumberFormat().format(json.response[11].registros_valorizado);
                        document.getElementById("totCantCoti1").innerHTML = Intl.NumberFormat().format(json.response[11].total_cantidad);
                        document.getElementById("totValtCoti1").innerHTML = "$" + Intl.NumberFormat().format(json.response[11].total_valorizado);

                        document.getElementById("cliCantCoti2").innerHTML = Intl.NumberFormat().format(json.response[12].clientes_cantidad);
                        document.getElementById("cliValCoti2").innerHTML = "$" + Intl.NumberFormat().format(json.response[12].clientes_valorizado);
                        document.getElementById("regiCantCoti2").innerHTML = Intl.NumberFormat().format(json.response[12].registros_cantidad);
                        document.getElementById("regiValCoti2").innerHTML = "$" + Intl.NumberFormat().format(json.response[12].registros_valorizado);
                        document.getElementById("totCantCoti2").innerHTML = Intl.NumberFormat().format(json.response[12].total_cantidad);
                        document.getElementById("totValtCoti2").innerHTML = "$" + Intl.NumberFormat().format(json.response[12].total_valorizado);

                        document.getElementById("cliCantCoti3").innerHTML = Intl.NumberFormat().format(json.response[13].clientes_cantidad);
                        document.getElementById("cliValCoti3").innerHTML = "$" + Intl.NumberFormat().format(json.response[13].clientes_valorizado);
                        document.getElementById("regiCantCoti3").innerHTML = Intl.NumberFormat().format(json.response[13].registros_cantidad);
                        document.getElementById("regiValCoti3").innerHTML = "$" + Intl.NumberFormat().format(json.response[13].registros_valorizado);
                        document.getElementById("totCantCoti3").innerHTML = Intl.NumberFormat().format(json.response[13].total_cantidad);
                        document.getElementById("totValtCoti3").innerHTML = "$" + Intl.NumberFormat().format(json.response[13].total_valorizado);

                        //STATUS CREDITO
                        document.getElementById("cliCantCredi1").innerHTML = Intl.NumberFormat().format(json.response[14].clientes_cantidad);
                        document.getElementById("cliValCredi1").innerHTML = "$" + Intl.NumberFormat().format(json.response[14].clientes_valorizado);
                        document.getElementById("regiCantCredi1").innerHTML = Intl.NumberFormat().format(json.response[14].registros_cantidad);
                        document.getElementById("regiValCredi1").innerHTML = "$" + Intl.NumberFormat().format(json.response[14].registros_valorizado);
                        document.getElementById("totCantCredi1").innerHTML = Intl.NumberFormat().format(json.response[14].total_cantidad);
                        document.getElementById("totValtCredi1").innerHTML = "$" + Intl.NumberFormat().format(json.response[14].total_valorizado);

                        document.getElementById("cliCantCredi2").innerHTML = Intl.NumberFormat().format(json.response[15].clientes_cantidad);
                        document.getElementById("cliValCredi2").innerHTML = "$" + Intl.NumberFormat().format(json.response[15].clientes_valorizado);
                        document.getElementById("regiCantCredi2").innerHTML = Intl.NumberFormat().format(json.response[15].registros_cantidad);
                        document.getElementById("regiValCredi2").innerHTML = "$" + Intl.NumberFormat().format(json.response[15].registros_valorizado);
                        document.getElementById("totCantCredi2").innerHTML = Intl.NumberFormat().format(json.response[15].total_cantidad);
                        document.getElementById("totValtCredi2").innerHTML = "$" + Intl.NumberFormat().format(json.response[15].total_valorizado);

                        document.getElementById("cliCantCredi3").innerHTML = Intl.NumberFormat().format(json.response[16].clientes_cantidad);
                        document.getElementById("cliValCredi3").innerHTML = "$" + Intl.NumberFormat().format(json.response[16].clientes_valorizado);
                        document.getElementById("regiCantCredi3").innerHTML = Intl.NumberFormat().format(json.response[16].registros_cantidad);
                        document.getElementById("regiValCredi3").innerHTML = "$" + Intl.NumberFormat().format(json.response[16].registros_valorizado);
                        document.getElementById("totCantCredi3").innerHTML = Intl.NumberFormat().format(json.response[16].total_cantidad);
                        document.getElementById("totValtCredi3").innerHTML = "$" + Intl.NumberFormat().format(json.response[16].total_valorizado);

                        //DEUDA
                        document.getElementById("cliCantDeuda1").innerHTML = Intl.NumberFormat().format(json.response[17].clientes_cantidad);
                        document.getElementById("cliValDeuda1").innerHTML = "$" + Intl.NumberFormat().format(json.response[17].clientes_valorizado);
                        document.getElementById("regiCantDeuda1").innerHTML = Intl.NumberFormat().format(json.response[17].registros_cantidad);
                        document.getElementById("regiValDeuda1").innerHTML = "$" + Intl.NumberFormat().format(json.response[17].registros_valorizado);
                        document.getElementById("totCantDeuda1").innerHTML = Intl.NumberFormat().format(json.response[17].total_cantidad);
                        document.getElementById("totValtDeuda1").innerHTML = "$" + Intl.NumberFormat().format(json.response[17].total_valorizado);

                        document.getElementById("cliCantDeuda2").innerHTML = Intl.NumberFormat().format(json.response[18].clientes_cantidad);
                        document.getElementById("cliValDeuda2").innerHTML = "$" + Intl.NumberFormat().format(json.response[18].clientes_valorizado);
                        document.getElementById("regiCantDeuda2").innerHTML = Intl.NumberFormat().format(json.response[18].registros_cantidad);
                        document.getElementById("regiValDeuda2").innerHTML = "$" + Intl.NumberFormat().format(json.response[18].registros_valorizado);
                        document.getElementById("totCantDeuda2").innerHTML = Intl.NumberFormat().format(json.response[18].total_cantidad);
                        document.getElementById("totValtDeuda2").innerHTML = "$" + Intl.NumberFormat().format(json.response[18].total_valorizado);

                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Potencial() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/portada')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        document.getElementById("potencial").innerHTML = " $ " + Intl.NumberFormat().format(json.response.potencial_a_la_fecha);
                        document.getElementById("abarcado").innerHTML =  Math.round(json.response.per_abarcado_potencial) + "%  de cumplimiento";
                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Meta() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/meta')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    // console.log(parametros);
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        document.getElementById("Meta").innerHTML = Intl.NumberFormat().format(json.response.meta);
                        document.getElementById("venta").innerHTML = "$" + Intl.NumberFormat().format(json.response.venta_ejecutivo);
                        //document.getElementById("Cumplimiento").innerHTML =  Math.round(json.response.cumplimiento * 100) / 100 + "%";
                        document.getElementById("barra_cumplimiento").style.width =  (Math.round(json.response.cumplimiento * 100) / 100) + "%"; 
                        document.getElementById("barra_cumplimiento").innerText =  (Math.round(json.response.cumplimiento * 100) / 100) + "%"; 
                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Ventas() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };

            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getventacartera')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json);
                    if (Object.keys(json).length > 0) {
                        document.getElementById("Mes1").innerHTML = json.response[0].mes;
                        document.getElementById("Ano1").innerHTML = Intl.NumberFormat().format(json.response[0].anio);
                        document.getElementById("Cartera1").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].venta_cartera);
                        document.getElementById("Ejecutivo1").innerHTML = "$" + Intl.NumberFormat().format(json.response[0].venta_ejecutivo);

                        document.getElementById("Mes2").innerHTML = json.response[1].mes;
                        document.getElementById("Ano2").innerHTML = Intl.NumberFormat().format(json.response[1].anio);
                        document.getElementById("Cartera2").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].venta_cartera);
                        document.getElementById("Ejecutivo2").innerHTML = "$" + Intl.NumberFormat().format(json.response[1].venta_ejecutivo);

                        document.getElementById("Mes3").innerHTML = json.response[2].mes;
                        document.getElementById("Ano3").innerHTML = Intl.NumberFormat().format(json.response[2].anio);
                        document.getElementById("Cartera3").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].venta_cartera);
                        document.getElementById("Ejecutivo3").innerHTML = "$" + Intl.NumberFormat().format(json.response[2].venta_ejecutivo);

                        document.getElementById("Mes4").innerHTML = json.response[3].mes;
                        document.getElementById("Ano4").innerHTML = Intl.NumberFormat().format(json.response[3].anio);
                        document.getElementById("Cartera4").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].venta_cartera);
                        document.getElementById("Ejecutivo4").innerHTML = "$" + Intl.NumberFormat().format(json.response[3].venta_ejecutivo);

                        document.getElementById("Mes5").innerHTML = json.response[4].mes;
                        document.getElementById("Ano5").innerHTML = Intl.NumberFormat().format(json.response[4].anio);
                        document.getElementById("Cartera5").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].venta_cartera);
                        document.getElementById("Ejecutivo5").innerHTML = "$" + Intl.NumberFormat().format(json.response[4].venta_ejecutivo);

                        document.getElementById("Mes6").innerHTML = json.response[5].mes;
                        document.getElementById("Ano6").innerHTML = Intl.NumberFormat().format(json.response[5].anio);
                        document.getElementById("Cartera6").innerHTML = "$" + Intl.NumberFormat().format(json.response[5].venta_cartera);
                        document.getElementById("Ejecutivo6").innerHTML = "$" + Intl.NumberFormat().format(json.response[5].venta_ejecutivo);

                        document.getElementById("Mes7").innerHTML = json.response[6].mes;
                        document.getElementById("Ano7").innerHTML = Intl.NumberFormat().format(json.response[6].anio);
                        document.getElementById("Cartera7").innerHTML = "$" + Intl.NumberFormat().format(json.response[6].venta_cartera);
                        document.getElementById("Ejecutivo7").innerHTML = "$" + Intl.NumberFormat().format(json.response[6].venta_ejecutivo);

                        document.getElementById("Mes8").innerHTML = json.response[7].mes;
                        document.getElementById("Ano8").innerHTML = Intl.NumberFormat().format(json.response[7].anio);
                        document.getElementById("Cartera8").innerHTML = "$" + Intl.NumberFormat().format(json.response[7].venta_cartera);
                        document.getElementById("Ejecutivo8").innerHTML = "$" + Intl.NumberFormat().format(json.response[7].venta_ejecutivo);

                        document.getElementById("Mes9").innerHTML = json.response[8].mes;
                        document.getElementById("Ano9").innerHTML = Intl.NumberFormat().format(json.response[8].anio);
                        document.getElementById("Cartera9").innerHTML = "$" + Intl.NumberFormat().format(json.response[8].venta_cartera);
                        document.getElementById("Ejecutivo9").innerHTML = "$" + Intl.NumberFormat().format(json.response[8].venta_ejecutivo);

                        document.getElementById("Mes10").innerHTML = json.response[9].mes;
                        document.getElementById("Ano10").innerHTML = Intl.NumberFormat().format(json.response[9].anio);
                        document.getElementById("Cartera10").innerHTML = "$" + Intl.NumberFormat().format(json.response[9].venta_cartera);
                        document.getElementById("Ejecutivo10").innerHTML = "$" + Intl.NumberFormat().format(json.response[9].venta_ejecutivo);

                        document.getElementById("Mes11").innerHTML = json.response[10].mes;
                        document.getElementById("Ano11").innerHTML = Intl.NumberFormat().format(json.response[10].anio);
                        document.getElementById("Cartera11").innerHTML = "$" + Intl.NumberFormat().format(json.response[10].venta_cartera);
                        document.getElementById("Ejecutivo11").innerHTML = "$" + Intl.NumberFormat().format(json.response[10].venta_ejecutivo);

                        document.getElementById("Mes12").innerHTML = json.response[11].mes;
                        document.getElementById("Ano12").innerHTML = Intl.NumberFormat().format(json.response[11].anio);
                        document.getElementById("Cartera12").innerHTML = "$" + Intl.NumberFormat().format(json.response[11].venta_cartera);
                        document.getElementById("Ejecutivo12").innerHTML = "$" + Intl.NumberFormat().format(json.response[11].venta_ejecutivo);

                        document.getElementById("Mes13").innerHTML = json.response[12].mes;
                        document.getElementById("Ano13").innerHTML = Intl.NumberFormat().format(json.response[12].anio);
                        document.getElementById("Cartera13").innerHTML = "$" + Intl.NumberFormat().format(json.response[12].venta_cartera);
                        document.getElementById("Ejecutivo13").innerHTML = "$" + Intl.NumberFormat().format(json.response[12].venta_ejecutivo);
                        
                        $("#spinner").hide();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function BuscaComisiones() {
            var text = '';
            var date = new Date();
            var token = '{{ csrf_token() }}';
            var lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-01';
            var total = 0;
            var parametros = {
                _token: token,
                fecha: lafecha
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/comisionPortal')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        text = '<tr>' +
                                '<td>Comisión ABC</td>' +
                                '<td>' + json.response[0].comision_abc + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Bono Meta</td>' +
                                '<td>' + json.response[1].comision_abc + '</td>' + 
                            '</tr>' +
                            '<tr>' +
                                '<td>Bono Cross</td>' +
                                '<td>' + json.response[2].comision_abc + '</td>' +
                            '</tr>';
                        document.getElementById("cuadroComisiones").innerHTML = text;
                        //cuadroComisiones
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection
