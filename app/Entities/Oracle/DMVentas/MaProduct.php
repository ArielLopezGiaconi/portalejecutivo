<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 11:54
 */

namespace App\Entities\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class MaProduct implements \JsonSerializable
{
    public $codpro;
    public $despro;
    public $codlin;
    public $codfam;
    public $codmar;
    public $codmod;
    public $codbar;
    public $coduni;
    public $pesopr;
    public $estdes;
    public $stkfsc;
    public $stkcom;
    public $stkpen;
    public $minimo;
    public $maximo;
    public $costo;
    public $ultcos;
    public $indsbr;
    public $codbdg;
    public $unmivt;
    public $pedsto;
    public $moneda;
    public $colate;
    public $tipalm;
    public $estpro;
    public $reembj;
    public $ultinv;
    public $fecinv;
    public $pagcat;
    public $numfto;
    public $ultlib;
    public $numser;
    public $merbod;
    public $mercli;
    public $fecing;
    public $stkase;
    public $asegur;
    public $stkcri;
    public $stkint;
    public $proneo;
    public $stkdis;
    public $tradis;
    public $resfis;
    public $codigo_uni;
    public $despro2;
    public $oferta;
    public $stk_dimerc;
    public $exist_ant;
    public $exist_tra_oc;
    public $cospro_ant_dm;
    public $exist_tra_mo;
    public $maxcd;
    public $mincd;
    public $cricd;
    public $intcd;
    public $stkini;
    public $fecini;
    public $audita;
    public $stkarm;
    public $rutprv;
    public $merbod2;
    public $stk_dimeiggs;
    public $marca_dimeiggs;
    public $cosprom;
    public $con_vencimiento;
    public $control_calidad;
    public $stock_dc;
    public $stock_dg;
    public $paleta_dc;
    public $paleta_dg;
    public $unica_ubic;
    public $limite_max_inv;
    public $num_ubic;
    public $stkpen_dmg;
    public $tipoprod;
    public $activo_algoritmo_posiciones;
    public $costo_corp;
    public $cosprom_corp;
    public $importado;
    public $impuesto;
    public $gobierno;
    public $codsubfam;
    public $codsubfam2;
    public $codneg;
    public $xdelta;

    // new values
    public $codsap;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->codpro = (isset($registro->codpro)) ? $registro->codpro : null;
            $this->despro = (isset($registro->despro)) ? $registro->despro : null;
            $this->codlin = (isset($registro->codlin)) ? $registro->codlin : null;
            $this->codfam = (isset($registro->codfam)) ? $registro->codfam : null;
            $this->codmar = (isset($registro->codmar)) ? $registro->codmar : null;
            $this->codmod = (isset($registro->codmod)) ? $registro->codmod : null;
            $this->codbar = (isset($registro->codbar)) ? $registro->codbar : null;
            $this->coduni = (isset($registro->coduni)) ? $registro->coduni : null;
            $this->pesopr = (isset($registro->pesopr)) ? $registro->pesopr : null;
            $this->estdes = (isset($registro->estdes)) ? $registro->estdes : null;
            $this->minimo = (isset($registro->minimo)) ? $registro->minimo : null;
            $this->maximo = (isset($registro->maximo)) ? $registro->maximo : null;
            $this->costo = (isset($registro->costo)) ? $registro->costo : null;
            $this->ultcos = (isset($registro->ultcos)) ? $registro->ultcos : null;
            $this->indsbr = (isset($registro->indsbr)) ? $registro->indsbr : null;
            $this->codbdg = (isset($registro->codbdg)) ? $registro->codbdg : null;
            $this->unmivt = (isset($registro->unmivt)) ? $registro->unmivt : null;
            $this->pedsto = (isset($registro->pedsto)) ? $registro->pedsto : null;
            $this->moneda = (isset($registro->moneda)) ? $registro->moneda : null;
            $this->colate = (isset($registro->colate)) ? $registro->colate : null;
            $this->tipalm = (isset($registro->tipalm)) ? $registro->tipalm : null;
            $this->estpro = (isset($registro->estpro)) ? $registro->estpro : null;
            $this->reembj = (isset($registro->reembj)) ? $registro->reembj : null;
            $this->ultinv = (isset($registro->ultinv)) ? $registro->ultinv : null;
            $this->fecinv = (isset($registro->fecinv)) ? $registro->fecinv : null;
            $this->pagcat = (isset($registro->pagcat)) ? $registro->pagcat : null;
            $this->numfto = (isset($registro->numfto)) ? $registro->numfto : null;
            $this->ultlib = (isset($registro->ultlib)) ? $registro->ultlib : null;
            $this->numser = (isset($registro->numser)) ? $registro->numser : null;
            $this->merbod = (isset($registro->merbod)) ? $registro->merbod : null;
            $this->mercli = (isset($registro->mercli)) ? $registro->mercli : null;
            $this->fecing = (isset($registro->fecing)) ? $registro->fecing : null;
            $this->stkase = (isset($registro->stkase)) ? $registro->stkase : null;
            $this->asegur = (isset($registro->asegur)) ? $registro->asegur : null;
            $this->stkcri = (isset($registro->stkcri)) ? $registro->stkcri : null;
            $this->stkint = (isset($registro->stkint)) ? $registro->stkint : null;
            $this->proneo = (isset($registro->proneo)) ? $registro->proneo : null;
            $this->stkdis = (isset($registro->stkdis)) ? $registro->stkdis : null;
            $this->tradis = (isset($registro->tradis)) ? $registro->tradis : null;
            $this->resfis = (isset($registro->resfis)) ? $registro->resfis : null;
            $this->codigo_uni = (isset($registro->codigo_uni)) ? $registro->codigo_uni : null;
            $this->despro2 = (isset($registro->despro2)) ? $registro->despro2 : null;
            $this->oferta = (isset($registro->oferta)) ? $registro->oferta : null;
            $this->stk_dimerc = (isset($registro->stk_dimerc)) ? $registro->stk_dimerc : null;
            $this->exist_ant = (isset($registro->exist_ant)) ? $registro->exist_ant : null;
            $this->exist_tra_oc = (isset($registro->exist_tra_oc)) ? $registro->exist_tra_oc : null;
            $this->cospro_ant_dm = (isset($registro->cospro_ant_dm)) ? $registro->cospro_ant_dm : null;
            $this->exist_tra_mo = (isset($registro->exist_tra_mo)) ? $registro->exist_tra_mo : null;
            $this->maxcd = (isset($registro->maxcd)) ? $registro->maxcd : null;
            $this->mincd = (isset($registro->mincd)) ? $registro->mincd : null;
            $this->cricd = (isset($registro->cricd)) ? $registro->cricd : null;
            $this->intcd = (isset($registro->intcd)) ? $registro->intcd : null;
            $this->stkini = (isset($registro->stkini)) ? $registro->stkini : null;
            $this->fecini = (isset($registro->fecini)) ? $registro->fecini : null;
            $this->audita = (isset($registro->audita)) ? $registro->audita : null;
            $this->stkarm = (isset($registro->stkarm)) ? $registro->stkarm : null;
            $this->rutprv = (isset($registro->rutprv)) ? $registro->rutprv : null;
            $this->merbod2 = (isset($registro->merbod2)) ? $registro->merbod2 : null;
            $this->stk_dimeiggs = (isset($registro->stk_dimeiggs)) ? $registro->stk_dimeiggs : null;
            $this->marca_dimeiggs = (isset($registro->marca_dimeiggs)) ? $registro->marca_dimeiggs : null;
            $this->cosprom = (isset($registro->cosprom)) ? $registro->cosprom : null;
            $this->con_vencimiento = (isset($registro->con_vencimiento)) ? $registro->con_vencimiento : null;
            $this->control_calidad = (isset($registro->control_calidad)) ? $registro->control_calidad : null;
            $this->stock_dc = (isset($registro->stock_dc)) ? $registro->stock_dc : null;
            $this->stock_dg = (isset($registro->stock_dg)) ? $registro->stock_dg : null;
            $this->paleta_dc = (isset($registro->paleta_dc)) ? $registro->paleta_dc : null;
            $this->paleta_dg = (isset($registro->paleta_dg)) ? $registro->paleta_dg : null;
            $this->unica_ubic = (isset($registro->unica_ubic)) ? $registro->unica_ubic : null;
            $this->limite_max_inv = (isset($registro->limite_max_inv)) ? $registro->limite_max_inv : null;
            $this->num_ubic = (isset($registro->num_ubic)) ? $registro->num_ubic : null;
            $this->stkpen_dmg = (isset($registro->stkpen_dmg)) ? $registro->stkpen_dmg : null;
            $this->tipoprod = (isset($registro->tipoprod)) ? $registro->tipoprod : null;
            $this->activo_algoritmo_posiciones = (isset($registro->activo_algoritmo_posiciones)) ? $registro->activo_algoritmo_posiciones : null;
            $this->costo_corp = (isset($registro->costo_corp)) ? $registro->costo_corp : null;
            $this->cosprom_corp = (isset($registro->cosprom_corp)) ? $registro->cosprom_corp : null;
            $this->importado = (isset($registro->importado)) ? $registro->importado : null;
            $this->impuesto = (isset($registro->impuesto)) ? $registro->impuesto : null;
            $this->gobierno = (isset($registro->gobierno)) ? $registro->gobierno : null;
            $this->codsubfam = (isset($registro->codsubfam)) ? $registro->codsubfam : null;
            $this->codsubfam2 = (isset($registro->codsubfam2)) ? $registro->codsubfam2 : null;
            $this->codneg = (isset($registro->codneg)) ? $registro->codneg : null;
            $this->xdelta = (isset($registro->xdelta)) ? $registro->xdelta : null;
            $this->codsap = (isset($registro->codsap)) ? $registro->codsap : null;
        }

    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getDespro()
    {
        return $this->despro;
    }

    public function setDespro($despro)
    {
        $this->despro = $despro;
    }

    public function getCodlin()
    {
        return $this->codlin;
    }

    public function setCodlin($codlin)
    {
        $this->codlin = $codlin;
    }

    public function getCodfam()
    {
        return $this->codfam;
    }

    public function setCodfam($codfam)
    {
        $this->codfam = $codfam;
    }

    public function getCodmar()
    {
        return $this->codmar;
    }

    public function setCodmar($codmar)
    {
        $this->codmar = $codmar;
    }

    public function getCodmod()
    {
        return $this->codmod;
    }

    public function setCodmod($codmod)
    {
        $this->codmod = $codmod;
    }

    public function getCodbar()
    {
        return $this->codbar;
    }

    public function setCodbar($codbar)
    {
        $this->codbar = $codbar;
    }

    public function getCoduni()
    {
        return $this->coduni;
    }

    public function setCoduni($coduni)
    {
        $this->coduni = $coduni;
    }

    public function getPesopr()
    {
        return $this->pesopr;
    }

    public function setPesopr($pesopr)
    {
        $this->pesopr = $pesopr;
    }

    public function getEstdes()
    {
        return $this->estdes;
    }

    public function setEstdes($estdes)
    {
        $this->estdes = $estdes;
    }

    public function getStkfsc()
    {
        return $this->stkfsc;
    }

    public function setStkfsc($stkfsc)
    {
        $this->stkfsc = $stkfsc;
    }

    public function getStkcom()
    {
        return $this->stkcom;
    }

    public function setStkcom($stkcom)
    {
        $this->stkcom = $stkcom;
    }

    public function getStkpen()
    {
        return $this->stkpen;
    }

    public function setStkpen($stkpen)
    {
        $this->stkpen = $stkpen;
    }

    public function getMinimo()
    {
        return $this->minimo;
    }

    public function setMinimo($minimo)
    {
        $this->minimo = $minimo;
    }

    public function getMaximo()
    {
        return $this->maximo;
    }

    public function setMaximo($maximo)
    {
        $this->maximo = $maximo;
    }

    public function getCosto()
    {
        return $this->costo;
    }

    public function setCosto($costo)
    {
        $this->costo = $costo;
    }

    public function getUltcos()
    {
        return $this->ultcos;
    }

    public function setUltcos($ultcos)
    {
        $this->ultcos = $ultcos;
    }

    public function getIndsbr()
    {
        return $this->indsbr;
    }

    public function setIndsbr($indsbr)
    {
        $this->indsbr = $indsbr;
    }

    public function getCodbdg()
    {
        return $this->codbdg;
    }

    public function setCodbdg($codbdg)
    {
        $this->codbdg = $codbdg;
    }

    public function getUnmivt()
    {
        return $this->unmivt;
    }

    public function setUnmivt($unmivt)
    {
        $this->unmivt = $unmivt;
    }

    public function getPedsto()
    {
        return $this->pedsto;
    }

    public function setPedsto($pedsto)
    {
        $this->pedsto = $pedsto;
    }

    public function getMoneda()
    {
        return $this->moneda;
    }

    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    public function getColate()
    {
        return $this->colate;
    }

    public function setColate($colate)
    {
        $this->colate = $colate;
    }

    public function getTipalm()
    {
        return $this->tipalm;
    }

    public function setTipalm($tipalm)
    {
        $this->tipalm = $tipalm;
    }

    public function getEstpro()
    {
        return $this->estpro;
    }

    public function setEstpro($estpro)
    {
        $this->estpro = $estpro;
    }

    public function getReembj()
    {
        return $this->reembj;
    }

    public function setReembj($reembj)
    {
        $this->reembj = $reembj;
    }

    public function getUltinv()
    {
        return $this->ultinv;
    }

    public function setUltinv($ultinv)
    {
        $this->ultinv = $ultinv;
    }

    public function getFecinv()
    {
        return $this->fecinv;
    }

    public function setFecinv($fecinv)
    {
        $this->fecinv = $fecinv;
    }

    public function getPagcat()
    {
        return $this->pagcat;
    }

    public function setPagcat($pagcat)
    {
        $this->pagcat = $pagcat;
    }

    public function getNumfto()
    {
        return $this->numfto;
    }

    public function setNumfto($numfto)
    {
        $this->numfto = $numfto;
    }

    public function getUltlib()
    {
        return $this->ultlib;
    }

    public function setUltlib($ultlib)
    {
        $this->ultlib = $ultlib;
    }

    public function getNumser()
    {
        return $this->numser;
    }

    public function setNumser($numser)
    {
        $this->numser = $numser;
    }

    public function getMerbod()
    {
        return $this->merbod;
    }

    public function setMerbod($merbod)
    {
        $this->merbod = $merbod;
    }

    public function getMercli()
    {
        return $this->mercli;
    }

    public function setMercli($mercli)
    {
        $this->mercli = $mercli;
    }

    public function getFecing()
    {
        return $this->fecing;
    }

    public function setFecing($fecing)
    {
        $this->fecing = $fecing;
    }

    public function getStkase()
    {
        return $this->stkase;
    }

    public function setStkase($stkase)
    {
        $this->stkase = $stkase;
    }

    public function getAsegur()
    {
        return $this->asegur;
    }

    public function setAsegur($asegur)
    {
        $this->asegur = $asegur;
    }

    public function getStkcri()
    {
        return $this->stkcri;
    }

    public function setStkcri($stkcri)
    {
        $this->stkcri = $stkcri;
    }

    public function getStkint()
    {
        return $this->stkint;
    }

    public function setStkint($stkint)
    {
        $this->stkint = $stkint;
    }

    public function getProneo()
    {
        return $this->proneo;
    }

    public function setProneo($proneo)
    {
        $this->proneo = $proneo;
    }

    public function getStkdis()
    {
        return $this->stkdis;
    }

    public function setStkdis($stkdis)
    {
        $this->stkdis = $stkdis;
    }

    public function getTradis()
    {
        return $this->tradis;
    }

    public function setTradis($tradis)
    {
        $this->tradis = $tradis;
    }

    public function getResfis()
    {
        return $this->resfis;
    }

    public function setResfis($resfis)
    {
        $this->resfis = $resfis;
    }

    public function getCodigoUni()
    {
        return $this->codigo_uni;
    }

    public function setCodigoUni($codigo_uni)
    {
        $this->codigo_uni = $codigo_uni;
    }

    public function getDespro2()
    {
        return $this->despro2;
    }

    public function setDespro2($despro2)
    {
        $this->despro2 = $despro2;
    }

    public function getOferta()
    {
        return $this->oferta;
    }

    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
    }

    public function getStkDimerc()
    {
        return $this->stk_dimerc;
    }

    public function setStkDimerc($stk_dimerc)
    {
        $this->stk_dimerc = $stk_dimerc;
    }

    public function getExistAnt()
    {
        return $this->exist_ant;
    }

    public function setExistAnt($exist_ant)
    {
        $this->exist_ant = $exist_ant;
    }

    public function getExistTraOc()
    {
        return $this->exist_tra_oc;
    }

    public function setExistTraOc($exist_tra_oc)
    {
        $this->exist_tra_oc = $exist_tra_oc;
    }

    public function getCosproAntDm()
    {
        return $this->cospro_ant_dm;
    }

    public function setCosproAntDm($cospro_ant_dm)
    {
        $this->cospro_ant_dm = $cospro_ant_dm;
    }

    public function getExistTraMo()
    {
        return $this->exist_tra_mo;
    }

    public function setExistTraMo($exist_tra_mo)
    {
        $this->exist_tra_mo = $exist_tra_mo;
    }

    public function getMaxcd()
    {
        return $this->maxcd;
    }

    public function setMaxcd($maxcd)
    {
        $this->maxcd = $maxcd;
    }

    public function getMincd()
    {
        return $this->mincd;
    }

    public function setMincd($mincd)
    {
        $this->mincd = $mincd;
    }

    public function getCricd()
    {
        return $this->cricd;
    }

    public function setCricd($cricd)
    {
        $this->cricd = $cricd;
    }

    public function getIntcd()
    {
        return $this->intcd;
    }

    public function setIntcd($intcd)
    {
        $this->intcd = $intcd;
    }

    public function getStkini()
    {
        return $this->stkini;
    }

    public function setStkini($stkini)
    {
        $this->stkini = $stkini;
    }

    public function getFecini()
    {
        return $this->fecini;
    }

    public function setFecini($fecini)
    {
        $this->fecini = $fecini;
    }

    public function getAudita()
    {
        return $this->audita;
    }

    public function setAudita($audita)
    {
        $this->audita = $audita;
    }

    public function getStkarm()
    {
        return $this->stkarm;
    }

    public function setStkarm($stkarm)
    {
        $this->stkarm = $stkarm;
    }

    public function getRutprv()
    {
        return $this->rutprv;
    }

    public function setRutprv($rutprv)
    {
        $this->rutprv = $rutprv;
    }

    public function getMerbod2()
    {
        return $this->merbod2;
    }

    public function setMerbod2($merbod2)
    {
        $this->merbod2 = $merbod2;
    }

    public function getStkDimeiggs()
    {
        return $this->stk_dimeiggs;
    }

    public function setStkDimeiggs($stk_dimeiggs)
    {
        $this->stk_dimeiggs = $stk_dimeiggs;
    }

    public function getMarcaDimeiggs()
    {
        return $this->marca_dimeiggs;
    }

    public function setMarcaDimeiggs($marca_dimeiggs)
    {
        $this->marca_dimeiggs = $marca_dimeiggs;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getConVencimiento()
    {
        return $this->con_vencimiento;
    }

    public function setConVencimiento($con_vencimiento)
    {
        $this->con_vencimiento = $con_vencimiento;
    }

    public function getControlCalidad()
    {
        return $this->control_calidad;
    }

    public function setControlCalidad($control_calidad)
    {
        $this->control_calidad = $control_calidad;
    }

    public function getStockDc()
    {
        return $this->stock_dc;
    }

    public function setStockDc($stock_dc)
    {
        $this->stock_dc = $stock_dc;
    }

    public function getStockDg()
    {
        return $this->stock_dg;
    }

    public function setStockDg($stock_dg)
    {
        $this->stock_dg = $stock_dg;
    }

    public function getPaletaDc()
    {
        return $this->paleta_dc;
    }

    public function setPaletaDc($paleta_dc)
    {
        $this->paleta_dc = $paleta_dc;
    }

    public function getPaletaDg()
    {
        return $this->paleta_dg;
    }

    public function setPaletaDg($paleta_dg)
    {
        $this->paleta_dg = $paleta_dg;
    }

    public function getUnicaUbic()
    {
        return $this->unica_ubic;
    }

    public function setUnicaUbic($unica_ubic)
    {
        $this->unica_ubic = $unica_ubic;
    }

    public function getLimiteMaxInv()
    {
        return $this->limite_max_inv;
    }

    public function setLimiteMaxInv($limite_max_inv)
    {
        $this->limite_max_inv = $limite_max_inv;
    }

    public function getNumUbic()
    {
        return $this->num_ubic;
    }

    public function setNumUbic($num_ubic)
    {
        $this->num_ubic = $num_ubic;
    }

    public function getStkpenDmg()
    {
        return $this->stkpen_dmg;
    }

    public function setStkpenDmg($stkpen_dmg)
    {
        $this->stkpen_dmg = $stkpen_dmg;
    }

    public function getTipoprod()
    {
        return $this->tipoprod;
    }

    public function setTipoprod($tipoprod)
    {
        $this->tipoprod = $tipoprod;
    }

    public function getActivoAlgoritmoPosiciones()
    {
        return $this->activo_algoritmo_posiciones;
    }

    public function setActivoAlgoritmoPosiciones($activo_algoritmo_posiciones)
    {
        $this->activo_algoritmo_posiciones = $activo_algoritmo_posiciones;
    }

    public function getCostoCorp()
    {
        return $this->costo_corp;
    }

    public function setCostoCorp($costo_corp)
    {
        $this->costo_corp = $costo_corp;
    }

    public function getCospromCorp()
    {
        return $this->cosprom_corp;
    }

    public function setCospromCorp($cosprom_corp)
    {
        $this->cosprom_corp = $cosprom_corp;
    }

    public function getImportado()
    {
        return $this->importado;
    }

    public function setImportado($importado)
    {
        $this->importado = $importado;
    }

    public function getImpuesto()
    {
        return $this->impuesto;
    }

    public function setImpuesto($impuesto)
    {
        $this->impuesto = $impuesto;
    }

    public function getGobierno()
    {
        return $this->gobierno;
    }

    public function setGobierno($gobierno)
    {
        $this->gobierno = $gobierno;
    }

    public function getCodsubfam()
    {
        return $this->codsubfam;
    }

    public function setCodsubfam($codsubfam)
    {
        $this->codsubfam = $codsubfam;
    }

    public function getCodsubfam2()
    {
        return $this->codsubfam2;
    }

    public function setCodsubfam2($codsubfam2)
    {
        $this->codsubfam2 = $codsubfam2;
    }

    public function getCodneg()
    {
        return $this->codneg;
    }

    public function setCodneg($codneg)
    {
        $this->codneg = $codneg;
    }

    public function getXdelta()
    {
        return $this->xdelta;
    }

    public function setXdelta($xdelta)
    {
        $this->xdelta = $xdelta;
    }

    public function getCodsap()
    {
        return $this->codsap;
    }

    public function setCodsap($codsap)
    {
        $this->codsap = $codsap;
    }

    public function jsonSerialize()
    {
        return [
            'codpro' => $this->getCodpro(),
            'despro' => $this->getDespro(),
            'codlin' => $this->getCodlin(),
            'codfam' => $this->getCodfam(),
            'codmar' => $this->getCodmar(),
            'codmod' => $this->getCodmod(),
            'codbar' => $this->getCodbar(),
            'coduni' => $this->getCoduni(),
            'pesopr' => $this->getPesopr(),
            'estdes' => $this->getEstdes(),
            'stkfsc' => $this->getStkfsc(),
            'stkcom' => $this->getStkcom(),
            'stkpen' => $this->getStkpen(),
            'minimo' => $this->getMinimo(),
            'maximo' => $this->getMaximo(),
            'costo' => $this->getCosto(),
            'ultcos' => $this->getUltcos(),
            'indsbr' => $this->getIndsbr(),
            'codbdg' => $this->getCodbdg(),
            'unmivt' => $this->getUnmivt(),
            'pedsto' => $this->getPedsto(),
            'moneda' => $this->getMoneda(),
            'colate' => $this->getColate(),
            'tipalm' => $this->getTipalm(),
            'estpro' => $this->getEstpro(),
            'reembj' => $this->getReembj(),
            'ultinv' => $this->getUltinv(),
            'fecinv' => $this->getFecinv(),
            'pagcat' => $this->getPagcat(),
            'numfto' => $this->getNumfto(),
            'ultlib' => $this->getUltlib(),
            'numser' => $this->getNumser(),
            'merbod' => $this->getMerbod(),
            'mercli' => $this->getMercli(),
            'fecing' => $this->getFecing(),
            'stkase' => $this->getStkase(),
            'asegur' => $this->getAsegur(),
            'stkcri' => $this->getStkcri(),
            'stkint' => $this->getStkint(),
            'proneo' => $this->getProneo(),
            'stkdis' => $this->getStkdis(),
            'tradis' => $this->getTradis(),
            'resfis' => $this->getResfis(),
            'codigo_uni' => $this->getCodigoUni(),
            'despro2' => $this->getDespro2(),
            'oferta' => $this->getOferta(),
            'stk_dimerc' => $this->getStkDimerc(),
            'exist_ant' => $this->getExistAnt(),
            'exist_tra_oc' => $this->getExistTraOc(),
            'cospro_ant_dm' => $this->getCosproAntDm(),
            'exist_tra_mo' => $this->getExistTraMo(),
            'maxcd' => $this->getMaxcd(),
            'mincd' => $this->getMincd(),
            'cricd' => $this->getCricd(),
            'intcd' => $this->getIntcd(),
            'stkini' => $this->getStkini(),
            'fecini' => $this->getFecini(),
            'audita' => $this->getAudita(),
            'stkarm' => $this->getStkarm(),
            'rutprv' => $this->getRutprv(),
            'merbod2' => $this->getMerbod2(),
            'stk_dimeiggs' => $this->getStkDimeiggs(),
            'marca_dimeiggs' => $this->getMarcaDimeiggs(),
            'cosprom' => $this->getCosprom(),
            'con_vencimiento' => $this->getConVencimiento(),
            'control_calidad' => $this->getControlCalidad(),
            'stock_dc' => $this->getStockDc(),
            'stock_dg' => $this->getStockDg(),
            'paleta_dc' => $this->getPaletaDc(),
            'paleta_dg' => $this->getPaletaDg(),
            'unica_ubic' => $this->getUnicaUbic(),
            'limite_max_inv' => $this->getLimiteMaxInv(),
            'num_ubic' => $this->getNumUbic(),
            'stkpen_dmg' => $this->getStkpenDmg(),
            'tipoprod' => $this->getTipoprod(),
            'activo_algoritmo_posiciones' => $this->getActivoAlgoritmoPosiciones(),
            'costo_corp' => $this->getCostoCorp(),
            'cosprom_corp' => $this->getCospromCorp(),
            'importado' => $this->getImportado(),
            'impuesto' => $this->getImpuesto(),
            'gobierno' => $this->getGobierno(),
            'codsubfam' => $this->getCodsubfam(),
            'codsubfam2' => $this->getCodsubfam2(),
            'codneg' => $this->getCodneg(),
            'xdelta' => $this->getXdelta(),

            // new values
            'codsap' => $this->getCodsap(),
        ];
    }
}