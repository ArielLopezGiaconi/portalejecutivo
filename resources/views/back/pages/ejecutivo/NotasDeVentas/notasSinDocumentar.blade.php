@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="ion-clipboard text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Notas sin documentar</h3>
                            <span class="f-16">Detalle - Notas sin documentar | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    {{-- <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div> --}}
                </div>
                {{-- <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-3 col-sm-11 col-xs-12 form-group ">
                                    <div class="card-sub">
                                        <div class="card-block">
                                            <label>Selección de Cartera</label>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12"> 
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" checked id="cartera1" name="cartera" value="1">
                                                        <label class="form-check-label" for="cartera1">Solo mi Cartera</label>
                                                    </div>
                                                
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera2" name="cartera" value="2">
                                                        <label class="form-check-label" for="cartera2">Cartera Partners</label>
                                                    </div>

                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera3" name="cartera" value="3">
                                                        <label class="form-check-label" for="cartera3">Ambas Carteras</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 form-group">
                                    <div class="row">
                                        <div class="col col-md-4">
                                            <label>Rut de Cliente</label>
                                            <input class="form-control" name="rut" id="rut" type='number' required/>    
                                        </div>
                                        <div class="col col-md-8">
                                            <br>
                                            <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar1()">Buscar Rut</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-8">
                                            <label>Razon Social</label>
                                            <input class="form-control" name="razons" id="razons" type='string' onkeyup="this.value = this.value.toUpperCase();" required/>  
                                        </div>
                                        <div class="col col-md-4">
                                            <br>
                                            <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar2()">Buscar Razon</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> --}}

                {{-- <div class="row" style="margin-left: 0px;">
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                        
                        <label>Fecha desde</label>
                        <div class="input-group date" id="fecha">
                            <input class="form-control" name="fechaDesde" id="fechaDesde" type="date">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                        
                        <label>Fecha Hasta</label>
                        <div class="input-group date" id="fecha">
                            <input class="form-control" name="fechaHasta" id="fechaHasta" type="date">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                        <label>Estado</label>
                        <select name="status" id="status" class="form-control" required="">
                            <option value="0">0- Todas</option>
                            <option value="1">1- Rendida</option>
                            <option value="2">2- Notificada</option>
                            <option value="3">3- Despachada</option>
                            <option value="4">4- Folio Asociado</option>
                            <option value="5">5- Preparacion</option>
                            <option value="6">6- Generada</option>
                        </select>
                        <select name="statusOra" id="statusOra" class="form-control" required="">
                            <option value="0">Todas</option>
                            <option value="1">Despachada</option>
                            <option value="2">Notificada</option>
                            <option value="3">Rendida</option>
                            <option value="4">Con genera pendiente</option>
                            <option value="5">Pendiente Credito</option>
                            <option value="6">Pendiente</option>
                            <option value="7">Generada</option>
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <br>
                        <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar3()">Buscar por Fecha</button>
                    </div>
                </div> --}}

            </div>

            <div id="CC" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">(C.C) Centro de Costo</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Centro de Costo que realiza la cotización y compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <i id="levantaRelacionados" style="display:none" class="fa fa-info-circle" data-toggle="modal" data-target="#Documentos"></i>
            <i id="levantaMensaje" style="display:none" class="fa fa-info-circle" data-toggle="modal" data-target="#sinDocumentos"></i>
            <i id="levantaejemplo" style="display:none" class="fa fa-info-circle" data-toggle="modal" data-target="#ejemplo"></i>
            <i id="levantaEstado_nota" style="display:none" class="fa fa-info-circle" data-toggle="modal" data-target="#estado_nota"></i>

            <div id="Documentos" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Documentos Relacionados</h2>
                        <div id="docRel" class="card-block text-center">
                        </div>
                    </div>
                </div>
            </div>

            <div id="sinDocumentos" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Sin documentos</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">La nota no tiene documentos relacionados.</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ejemplo" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center"></h2>
                        <div class="card-block text-center">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-11 col-sm-9 col-md-10 col-lg-7 col-xl-10 text-center p-0 mt-3 mb-2">
                                        <div class="barraPedido cardton px-0 pt-4 pb-0 mt-3 mb-3">
                                            <h2 id="Titulo">En procceso de validación</h2>
                                            <h5 class="text-info" id="footer1"></h5>
                                            <div id="msform">
                                                <!-- progressbar -->
                                                <ul class="iconoBarraPedido" id="progressbar">
                                                    <li class="active" id="validacion"><strong>Validación</strong></li>
                                                    <li class="" id="preparacion"><strong>Preparación</strong></li>
                                                    <li class="" id="operador" class="bi bi-truck"><strong>Operador Logistico</strong></li>
                                                    <li class="" id="ruta"><strong>En ruta</strong></li>
                                                    <li class="" id="entregado" ><strong>Entregado</strong></li>
                                                </ul>
                                                
                                                {{-- <h4 id="footer2"></h4> --}}
                                                <div class="col">
                                                    <button id="verDocumento" onclick="levantaDocumento()" class="btn btn-mat btn-success pull-right" value="">Ver Documento</button>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div> 
                                                    <table class="table table-bordered nowrap dataTable text-right" style="margin: 0 auto;">
                                                    <tbody class="text-left f-16" id="infoStatus">
                                                    </tbody>
                                                    <tbody class="text-left f-16" id="statusNota1">
                                                    </tbody>
                                                    <tbody class="text-left f-16" id="statusNota2">
                                                    </tbody>
                                                    <tbody class="text-left f-16" id="statusNota3">
                                                    </tbody>
                                                    <tbody class="text-left f-16" id="statusNota4">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="estado_nota" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm-12">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Status Nota</h2>
                        <div>
                            <div id="abarcado" class="bg-c-green text-white text-center row-lg-9 m-b-10" style="border-radius: 15px;">
                                <h3 id="mensajePendiente" class="f-w-700 m-t-5">Activa</h3>
                            </div>
                        </div>
                        
                        {{-- <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Pedido ingresado a bodega</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento1" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso1" class="ti-face-sad text-inverse f-26 col-lg-1" style="margin-top: 20px"></i>
                        </div> --}}

                        <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Pedido en Preparación</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento2" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso2" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div>

                        {{-- <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Pedido preparado</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento3" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso3" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div> --}}

                        {{-- <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Pedido en zona de despacho</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento4" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso4" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div> --}}

                        <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Entregado a Operador logistico</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento5" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso5" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div>

                        <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title row-lg-10">Pedido en ruta</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento6" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso6" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div>

                        <div class="row card-block">
                            <div class="col-lg-10">
                                <h6 class="modal-title">Pedido entregado</h6>
                                <div class="progress" style="margin-top: 10px">
                                    <div class="progress-bar progress-bar-striped progress-bar-danger progress-bar-animated" max="100" id="barra_cumplimiento7" style="width:0%"></div>
                                </div>
                            </div>
                            <i id="paso7" class="ti-face-sad text-inverse f-26 col-sm-1" style="margin-top: 20px"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div id="Margen" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Margen</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Valor porcentual que representa la rentabilidad de la gestion, indicando el % de ingreso en relacion a la venta</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div class="table-responsive">
                    <div id="detalle" onkeyup="Formato()"></div>
                    <div class="spinner" id="spinner" style="display: none;">
                        <center>
                            <div class="bolitas">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                            <div class="cargando">Cargando datos...</div>
                        </center>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "ion-clipboard";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Notas de Venta";
            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
            var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            getDerechos();

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

            Buscar();
  
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var value = '{{ $valor }}';
            tr = table.getElementsByTagName("tr");

            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                td = tr[i].getElementsByTagName("td")[1];
                $(td).addClass('text-right');
                if (row.cells[10].innerHTML.substring(0,1) != '$'){
                    row.cells[10].innerHTML = '$' + number_format(row.cells[10].innerHTML,0);
                }
            };
        }

        function FichaCliente(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=16;
            var actionid=41;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                var value = '{{ $valor }}';
                if (value > 0) {
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        location.href = window.location.href.replace('/notasPedientes/' + value ,'') + "/fichacliente/" + cells[6].innerHTML;
                    };
                }else{
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        location.href = window.location.href.replace('/notasPedientes' + value ,'') + "/fichacliente/" + cells[6].innerHTML;
                    };
                }
            };
        }

        function ModificaNota(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=16;
            var actionid=42;
            var oldvalue='';
            var newvalue='';
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                var value = '{{ $valor }}';
                console.log(value);
                if (value > 0) {
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue = cells[1].innerHTML
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        location.href = window.location.href.replace('/notasPedientes/' + value ,'') + "/editanotaventa/" + cells[6].innerHTML + "/" + cells[1].innerHTML;
                    };
                }else{
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue = cells[1].innerHTML;
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        location.href = window.location.href.replace('/notasPedientes' + value ,'') + "/editanotaventa/" + cells[6].innerHTML + "/" + cells[1].innerHTML;
                    };
                }
            };
        }

        function Buscar() {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th onclick="Formato()" >Acc</th>\n' +
                '                            <th onclick="Formato()" >N° Nota Venta</th>\n' +
                '                            <th onclick="Formato()" >Credito</th>\n' +
                '                            <th onclick="Formato()" >Stock</th>\n' +
                '                            <th onclick="Formato()" >Orden de Compra</th>\n' +
                '                            <th onclick="Formato()" >Fecha Emisión</th>\n' +
                '                            <th onclick="Formato()" >Rut</th>\n' +
                '                            <th onclick="Formato()" >C.C.\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#CC"></i></th>\n' + 
                '                            <th onclick="Formato()" >Razon Social</th>\n' +
                '                            <th onclick="Formato()" >Segmento</th>\n' +
                '                            <th onclick="Formato()" >Neto</th>\n' +
                '                            <th onclick="Formato()" >Estado Nota</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var token = '{{ csrf_token() }}';
            
            var parametros = {
                _token: token
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getNotasSinDocumentar')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [
                                {data: "nota_de_venta", render: function (data, type, row, meta) {
                                        var boton = '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                        '   <button type="button"\n' +
                                        '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                        '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                        '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                        '       class="icofont icofont-ui-user f-16"></span>\n' +
                                        '   </button>\n' +
                                        '   <button type="button"\n' +
                                        '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                        '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                        '       title="Editar Nota" onclick="ModificaNota(' + data + ')"><span\n' +
                                        '       class="fa fa-pencil f-18"></span>\n' +
                                        '   </button>\n' +
                                        '   <button type="button"\n' +
                                        '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                        '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                        '       title="Documentos relacionados" onclick="getDocumentos(' + data + ')"><span\n' +
                                        '       class="ion-ios-paper-outline f-18"></span>\n' +
                                        '   </button>\n' +
                                        '   <button type="button"\n' +
                                        '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                        '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                        '       title="Status nota" onclick="getEstadoNota(' + data + ')"><span\n' +
                                        '       class="fa fa-truck f-18"></span>\n' +
                                        '   </button>\n' +
                                        '</div>\n';
                                        return boton;
                                    },
                                    className: 'botones_cotizaciones'
                                },
                                {data: "nota_de_venta",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "estcred_sap",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "estbod",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "orden_de_compra",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "fecha_emision",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "rut",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "razon_social",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "segmento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "neto",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "estado",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 12 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',  
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });

                        $("#spinner").hide();
                        Orden();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getDocumentos(nota) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getDocumentos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (json.response.length > 0) {
                        text = '';
                        tabla = '<table class="table table-striped table-bordered nowrap dataTable text-right" style="margin: 0 auto;">\n' +
                                '    <thead>\n' +
                                '        <tr class="table-inverse">\n' +
                                '            <th colspan="1">Nota de venta</th>\n' +
                                '            <th colspan="1">Neto nota</th>\n' +
                                '            <th colspan="1">Orden de Compra</th>\n' +
                                '            <th colspan="1">Guia despacho</th>\n' +
                                '            <th colspan="1">Neto guia</th>\n' +
                                '            <th colspan="1">Factura</th>\n' +
                                '            <th colspan="1">Neto Factura</th>\n' +
                                '        </tr>\n' +
                                '    </thead>\n' +
                                '    <body>';
                        for (i = 0; i < json.response.length; i++) {
                            text = text + '<tr>\n' +
                                    '<td>' + json.response[i].numnvt + '</td>\n' +
                                    '<td>' + '$' + number_format(json.response[i].totnet,0) + '</td>\n' +
                                    '<td>' + json.response[i].docaso + '</td>\n' +
                                    '<td>' + json.response[i].numgui + '</td>\n' +
                                    '<td>' + '$' + number_format(json.response[i].total_guia,0) + '</td>\n' +
                                    '<td>' + json.response[i].numfac + '</td>\n' +
                                    '<td>' + '$' + number_format(json.response[i].total_factura,0) + '</td>\n' +
                                   '</tr>';
                        };
                        tabla = tabla +  text + '</body>\n' +
                            '</table>';
                            
                        document.getElementById("docRel").innerHTML = tabla;
                        document.getElementById("levantaRelacionados").click();
                    } else {
                        document.getElementById("levantaMensaje").click();
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getEstadoNota(nota) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/getStatusNota')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    $("#verDocumento").hide(); 
                    document.getElementById("verDocumento").value = '';
                    if (json.response.length > 0) {
                        var entregado = '0';
                        var comenzado = '0';
                        //document.getElementById("levantaEstado_nota").click(); 
                        document.getElementById("levantaejemplo").click();

                        $("#preparacion").removeClass('active');
                        $("#operador").removeClass('active');
                        $("#ruta").removeClass('active');
                        $("#entregado").removeClass('active');

                        getInfoEstadoNota(nota);
                        document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                        document.getElementById("footer1").innerHTML = "<strong> En proceso de validación </strong>";
                        if (json.response[0].paso2 != '0' && json.response[0].paso67 === '') {
                            $("#preparacion").addClass('active');
                            document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                            document.getElementById("footer1").innerHTML = "<strong>En preparación</strong>";
                            comenzado = '1';
                            getInfoNota(nota, 2, 0);
                        }else{
                            if(json.response[0].paso67 != null){
                                if (json.response[0].paso67 == 'Entregada a Operador Logistico') {
                                    $("#preparacion").addClass('active');
                                    $("#operador").addClass('active');
                                    document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                    document.getElementById("footer1").innerHTML = "<strong>Entregada a operador logistico</strong>";
                                    getInfoNota(nota, 3, 0);
                                }else{
                                    if (json.response[0].paso67 == 'paso6') {
                                        $("#preparacion").addClass('active');
                                        $("#operador").addClass('active');
                                        $("#ruta").addClass('active');
                                        document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                        document.getElementById("footer1").innerHTML = "<strong>En ruta</strong>";
                                        
                                        getInfoNota(nota, 4, 2);
                                    }else{
                                        if (json.response[0].paso67 != '0'){
                                            //  PASO7
                                            if (json.response[0].paso67 != 'paso6') {
                                                $("#preparacion").addClass('active');
                                                $("#operador").addClass('active');
                                                $("#ruta").addClass('active');
                                                $("#entregado").addClass('active');
                                                document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                                document.getElementById("footer1").innerHTML = "<strong> " + json.response[0].paso67 + " </strong>";
                                                entregado = '1';
                                                getInfoNota(nota, 4, 1);
                                            }
                                        }
                                    }
                                }
                            }else{
                                document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                document.getElementById("footer1").innerHTML = "<strong> En proceso de validación </strong>";
                                getInfoNota(nota, 1, 0);
                            }
                        }
                        
                        if (entregado != '1'){
                            var text = '';
                            var pen_credito = '';
                            var pen_stock = '';

                            if (json.response[0].pen_credito != 0 && json.response[0].pen_stock == 0) {
                                document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                document.getElementById("footer1").innerHTML = "<strong> Pendiente por crédito</strong>";
                            }

                            if (json.response[0].pen_stock != 0 && json.response[0].pen_credito == 0) {
                                document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                document.getElementById("footer1").innerHTML = "<strong> Pendiente por stock </strong>";
                            }

                            if (json.response[0].pen_stock != 0 && json.response[0].pen_credito != 0) {
                                document.getElementById("Titulo").innerHTML = " Nota de venta " + nota;
                                document.getElementById("footer1").innerHTML = "<strong> Pendiente por crédito y stock </strong>";
                            }
                        }
                        
                    } else {
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function Orden(){
            // setTimeout('document.getElementById("Total").click()',250);
            // setTimeout('document.getElementById("Total").click()',250);
            Formato();
        }

        function levantaDocumento(){
            window.open(document.getElementById("verDocumento").value);
        }

        function getInfoNota(nota, tipo_, final) {
             
            // document.getElementById("footer2").innerText = '';
            switch (tipo_) {
                case 1:
                    getInfoNota1(nota, final);
                    break;
                case 2:
                    getInfoNota1(nota, final);
                    getInfoNota2(nota, final); 
                    break;
                case 3:
                    getInfoNota1(nota, final);
                    getInfoNota2(nota, final);
                    getInfoNota3(nota, final);
                    break;
                case 4:
                    getInfoNota1(nota, final);
                    getInfoNota2(nota, final);
                    getInfoNota3(nota, final);
                    getInfoNota4(nota, final);
                    break;
            }
        }

        function getInfoEstadoNota(nota, tipo_) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            $.ajax({
                data: parametros,
                url: '{{url('/getInfoEstadoNota')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response.response);
                    document.getElementById("infoStatus").innerHTML = '';
                    var text = '<tr><th colspan="2" class="text-center" ><h3>Seguimiento del pedido</h3></th></tr>' + 
                            '<tr><td><b>Dirección de despacho: </b> </td><td>' + json.response.response.dirdes + '</td><tr>\n' +
                            // '<tr><td><b>Origen: </b> </td><td>' + json.response.response.origen + '</td><tr>\n' +
                            '<tr><td><b>Centro de costo: </b> </td><td>' + json.response.response.cencos + '</td><tr>\n';
                            if (json.response.response.fecha_promesa = 'null') {
                                text = text + '<tr><td><b>Fecha Promesa: </b> </td><td></td><tr>\n'; 
                            }else{
                                text = text + '<tr><td><b>Fecha Promesa: </b> </td><td>' + json.response.response.fecha_promesa + '</td><tr>\n'; 
                            }
                    document.getElementById("infoStatus").innerHTML = text;
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getInfoNota1(nota, final) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/getInfoNota1')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response[0]);
                    document.getElementById("statusNota1").innerHTML = '';
                    if (json.response.length > 0) {
                        var text = '' + 
                        '<td><b>Fecha creación y validación: </b> </td><td>' + json.response[0].fecha_creac + '</td><tr>\n' +
                        '<tr><td><b>Mensaje Validación: </b> </td><td>' + json.response[0].mensaje_sap + '</td><tr>\n';
                        if (json.response[0].fecha_libera_sap = 'null') {
                            text = text + '<td><b>Fecha liberación de credito: </b> </td><td></td><tr>\n'; 
                        }else{
                            text = text + '<td><b>Fecha liberación de credito: </b> </td><td>' + json.response[0].fecha_libera_sap + '</td><tr>\n'; 
                        }
                        if (json.response[0].msg_libera_sap = 'null') {
                            text = text + '<td><b>Mensaje liberación de credito: </b> </td><td></td><tr>\n'; 
                        }else{
                            text = text + '<td><b>Mensaje liberación de credito: </b> </td><td>' + json.response[0].msg_libera_sap + '</td><tr>\n'; 
                        }
                        // text = text + '<td><b>Mensaje liberación: </b> </td><td>' + json.response[0].msg_libera_sap + '</td><tr>\n' +
                        // // '<td><b>Fecha emisión: </b> </td><td>' + json.response[0].fecemi + '</td><tr>\n' +
                        // '';
                        document.getElementById("statusNota1").innerHTML = text;
                        // if ($final =! 1) {
                        //     document.getElementById("footer2").innerText = "Fecha validación: " + json.response[0].fecha_creac;
                        // }
                    } else {
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getInfoNota2(nota, final) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/getInfoNota2')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json.response);
                    if (json.response.length > 0) {
                        document.getElementById("statusNota2").innerHTML = '';
                        var text = '' + 
                            '<tr><td><b>Fecha bodega: <b> </td><td>' + json.response[0].fecha_bodega + '</td><tr>\n' +
                            '<tr><td><b>Fecha preparación: </b> </td><td>' + json.response[0].fecha_preparacion + '</td><tr>\n' +
                            '';
                        document.getElementById("statusNota2").innerHTML = text;  
                        // if ($final =! 1) {       
                        //     document.getElementById("footer2").innerText = "Fecha preparación: " + json.response[0].fecha_creac;    
                        // } 
                    } else {
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getInfoNota3(nota, final) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/getInfoNota3')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json.response);
                    document.getElementById("statusNota3").innerHTML = '';
                    if (json.response.length > 0) {
                        var text = '' + 
                            '<tr><td><b>Operador logístico: </b> </td><td>' + json.response[0].operador + '</td><tr>\n' +
                            '<tr><td><b>Fecha de ruta: </b> </td><td>' + json.response[0].fecha_ruta + '</td><tr>\n' +    
                            '';
                        document.getElementById("statusNota3").innerHTML = text;
                        // if ($final =! 1) {
                        //     document.getElementById("footer2").innerText = "Operador: " + json.response[0].operador;             
                        // }
                    } else {
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getInfoNota4(nota, final) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                numnvt: nota
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/getInfoNota4')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    document.getElementById("statusNota4").innerHTML = '';
                    if (json.response.length > 0) {
                        if (final == 2) {
                            var text = '' + 
                            '<tr><td><b>Comentario transportista: </b> </td><td>' + json.response[0].comentario + '</td><tr>\n' +
                            // '<tr><td><b><b>Foto: </b> </td><td>' + json.response[0].foto + '</td><tr>\n' +
                            '<tr><td><b><b>Datos del vehículo: </b> </td><td>' + json.response[0].mobile_device + '</td><tr>\n' +
                            '<tr><td><b><b>Fecha de ruta: </b> </td><td>' + json.response[0].fecha + '</td><tr>\n' +
                            '';
                            document.getElementById("statusNota4").innerHTML = text;
                            console.log('el final: ' + final);
                        }else{
                            var text = '' + 
                            '<tr><td><b>Comentario transportista: </b> </td><td>' + json.response[0].comentario + '</td><tr>\n' +
                            // '<tr><td><b><b>Foto: </b> </td><td>' + json.response[0].foto + '</td><tr>\n' +
                            '<tr><td><b><b>Datos del vehículo: </b> </td><td>' + json.response[0].mobile_device + '</td><tr>\n' +
                            '<tr><td><b><b>Fecha de entrega: </b> </td><td>' + json.response[0].fecha + '</td><tr>\n' +
                            '';
                            document.getElementById("statusNota4").innerHTML = text;
                            console.log('el final: ' + final);
                        }
                        // if (final === 1){
                            // document.getElementById("footer2").innerText = "Fecha de entrega: " + json.response[0].fecha;  
                            if (json.response[0].foto){
                                var foto = "https://cldmcrcaapp26.dimerc.cl/?id=" + json.response[0].url_photos + "&proveedor=" + json.response[0].id_proveedor;
                                document.getElementById("verDocumento").value = foto;
                                console.log(json.response[0].foto);
                                if (json.response[0].foto != '0' ) {
                                    $("#verDocumento").show();
                                }
                            }
                            
                        // }else{
                        //     document.getElementById("footer2").innerText = "Movil: " + json.response[0].mobile_device;  
                        }        
                    // } else {
                    //     $("#spinner").hide();
                    // }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection