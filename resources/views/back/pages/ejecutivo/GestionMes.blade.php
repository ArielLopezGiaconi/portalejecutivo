@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="icofont icofont-open-eye text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo" >Mis Indicadores</h3>
                            <span class="f-16">Mis Indicadores | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        {{-- <div class="col-sm-4">
                            <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button>
                        </div>  --}}
                    </div>
                    <div class="card-header-right">
                        {{-- <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul> --}}
                    </div>
                </div>
                <div class="card-block" style="">
                    {{-- <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label>Periodo</label>
                                    <div class='input-group date' id='fecha'>
                                        <input class="form-control" name="Periodo" id="Periodo" type='date'/>
                                        <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()" style="margin-left: 5px">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div> --}}
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div class="table-responsive">
                    <div id="detalle"></div>
                    
                    <div class="spinner" id="spinner" style="display: none;">
                        <center>
                            <div class="bolitas">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                            <div class="cargando">Cargando datos...</div>
                        </center>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-hand-rock-o";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Acciones Agendadas";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            setTimeout('Buscar()',500);
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });


        function FichaCliente(rut){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=21;
            var actionid=53;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                //console.log(window.location.href.replace('/ingresosWeb','') + "/fichacliente/" + cells[1].innerHTML);
                                location.href = window.location.href.replace('/acciones','') + "/fichacliente/" + cells[1].innerHTML;
                            };
                
            };
        }
        
        function Buscar() {
            var buscar = 0;
            
            if (sessionStorage.getItem('40004') == 40004) {
                buscar = 1;
            }

            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Usuario</th>\n' +
                '                            <th>Q cotizaciones TLMK</th>\n' +
                '                            <th>Q cotizaciones portal</th>\n' +
                '                            <th>% cotizaciones</th>\n' +
                '                            <th>Q cotizaciones focos</th>\n' +
                '                            <th>Q notas TLMK</th>\n' +
                '                            <th>Q notas portal</th>\n' +
                '                            <th>% notas</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;
            
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                tipo: buscar
            };
            // console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/GestionMes')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [
                                {data: "userid",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "q_cotizaciones_tlmkt",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "q_cotizaciones_tlmkt_web",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "porcentaje_cotizaciones",
                                    render: function (data, type, row, meta) {
                                    return  '<span class="pull-left"><b>' + data + '</b></span>\n';}},
                                {data: "q_cotizaciones_focos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "q_notas_tlmkt",
                                    render: function (data, type, row, meta) {
                                    return  data;}}, 
                                {data: "q_notas_tlmkt_web",
                                    render: function (data, type, row, meta) {
                                    return  data;}}, 
                                {data: "porcentaje_notas",
                                    render: function (data, type, row, meta) {
                                    return  '<span class="pull-left"><b>' + data + '</b></span>\n';}},
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',  
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });

                        var table, tr, td, i, text;
                        table = document.getElementById("detallenota");
                        tr = table.getElementsByTagName("tr");

                        // Loop through all table rows, and hide those who don't match the search query
                        for (i = 0; i < tr.length; i++) {
                            td = tr[i].getElementsByTagName("td")[3];
                            $(td).css('color','#6382FC');

                            td = tr[i].getElementsByTagName("td")[7];
                            $(td).css('color','#6382FC');
                        }

                        $("#spinner").hide();
                    } else {
                        var text = '<div class="alert alert-danger background-danger">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                            '<i class="icofont icofont-close-line-circled text-white"></i>' +
                                        '</button>' +
                                        '<strong>Sin Ingresos Web</strong>' +
                                    '</div>';
                        document.getElementById("detalle").innerHTML = text
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection