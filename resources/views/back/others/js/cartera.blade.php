<script>
    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var moduleid_global = 2;
    var cencos=[];

    function getCarteraClientes() {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
        };
        $.ajax({
            data: parametros,
            url: '{{url('getcartera')}}',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    if (Object.keys(json.response).length > 0) {
                        $("#tb_cartera").DataTable({
                            bDestroy: true,
                            data: json.response,
                            columns: [
                                {
                                    data: "rutcli", render: function (data, type, row, meta) {
                                        var boton = '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                            '                                            <button type="button" onclick="fichacliente(' + data + ');"\n' +
                                            '                                                    class="tabledit-edit-button btn btn-primary waves-effect waves-light"\n' +
                                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '                                                    title="Detalle cliente"><span\n' +
                                            '                                                        class="icofont icofont-open-eye f-18"></span>\n' +
                                            '                                            </button>\n' +
                                            '                                            <button type="button" onclick="estrategias();" \n' +
                                            '                                                    class="tabledit-delete-button btn btn-danger waves-effect waves-light"\n' +
                                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '                                                    title="Estrategias"><span\n' +
                                            '                                                        class="icofont icofont-dart f-18"></span>\n' +
                                            '                                            </button>\n' +
                                            '                                            <button type="button" onclick="contacto(' + data + ');" \n' +
                                            '                                                    class="tabledit-delete-button btn btn-inverse waves-effect waves-light"\n' +
                                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '                                                    title="Contactar"><span\n' +
                                            '                                                        class="icofont icofont-ui-dial-phone f-14"></span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                    </div>';
                                        return boton;
                                    },
                                    className: 'botones_cartera'
                                },
                                {
                                    data: "clasificacion"
                                },
                                {
                                    data: "relacion_comercial"
                                },
                                {
                                    data: "rutcli"
                                },
                                {
                                    data: "razons"
                                },
                                {
                                    data: "negocio_eerr"
                                },
                                {
                                    data: "observacion_credito"
                                },
                                {
                                    data: "gestion_del_mes"
                                },
                                {
                                    data: "potencial",
                                    render: $.fn.dataTable.render.number('.', ',', 0, '$', '')
                                },
                                {
                                    data: "brecha",
                                    render: $.fn.dataTable.render.number('.', ',', 0, '$', '')
                                },
                                {
                                    data: "venta_anterior",
                                    render: $.fn.dataTable.render.number('.', ',', 0, '$', '')
                                },
                                {
                                    data: "venta_actual",
                                    render: $.fn.dataTable.render.number('.', ',', 0, '$', '')
                                },
                                {
                                    data: "ultima_gestion"
                                },
                                {
                                    data: "fecha_ultima_gestion"
                                },
                                {
                                    data: "proxima_accion"
                                },
                                {
                                    data: "fecha_proxima"
                                },
                                {
                                    data: "dias_para_salir_de_cartera"
                                },
                                {
                                    data: "motivo_de_salida"
                                }
                            ],
                            columnDefs: [{
                                "targets": 11,
                                "createdCell": function (td, cellData, rowData, row, col) {
                                    if (parseInt(rowData.venta_actual) == 0) {
                                        $(td).addClass('bg-warning');
                                        $(td).addClass('dt-right');
                                    } else if (parseInt(rowData.venta_actual) < parseInt(rowData.venta_anterior)) {
                                        $(td).addClass('bg-danger');
                                        $(td).addClass('dt-right');
                                    } else {
                                        $(td).addClass('bg-success');
                                        $(td).addClass('dt-right');
                                    }
                                }
                            }],
                            order: [[8, "desc"]],
                            language: {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ registros",
                                "sZeroRecords": "No se encontraron resultados",
                                "sEmptyTable": "Ningún dato disponible en esta tabla",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar:",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                        });
                    }
                }
                $('.dataTables_length').addClass('bs-select');
            },
            error: function (e) {
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener cartera clientes', code, text);
            }
        });
    }

    function fichacliente(rutcli) {

        /*
        Module: cartera -> 2
        Action: clic en ficha cliente	 -> 4
                clic en estrategias	 -> 5
                clic en contacto	 -> 6
        */
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=2;
        var actionid=4;
        var oldvalue='';
        var newvalue='';
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue, rutcli);

        {{--window.location = "{{ url('setfichacliente') }}/" + rutcli;--}}
        window.location = "{{ url('fichacliente') }}/" + rutcli;
    }

    function estrategias(){
        /*
        Module: cartera -> 2
        Action: clic en ficha cliente	 -> 4
                clic en estrategias	 -> 5
                clic en contacto	 -> 6
        */
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=2;
        var actionid=5;
        var oldvalue='';
        var newvalue='';
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
    }

    function contacto(rutcli){
        getCencosRutcli(rutcli);
        $('#btn_contacto_cliente').click();

        /*
        Module: cartera -> 2
        Action: clic en ficha cliente	 -> 4
                clic en estrategias	 -> 5
                clic en contacto	 -> 6
        */
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=2;
        var actionid=6;
        var oldvalue='';
        var newvalue='';
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue);
    }

    function getContacto(rutcli){
        var cencos = document.getElementsByName("select_ceco")[0].value;

        var parametros = {
            "rutcli": rutcli,
            "cencos": cencos,
            "codemp": 3
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-contacto',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {

                if (json.code == 200) {

                    var cont = json.response[0];

                    document.getElementById('cli_input_rut').value = rutcli;

                    if(json.response.length>0){
                        document.getElementById('cli_input_razons').value = cont.razons;
                        document.getElementById('cli_input_contacto').value = cont.contac;
                        document.getElementById('cli_input_cargo').value = cont.carcon;
                        document.getElementById('cli_input_fono').value = cont.fono;
                        document.getElementById('cli_input_celular').value = cont.celular;
                        document.getElementById('cli_input_mail').value = cont.mail;
                        document.getElementById('cli_input_obs').value = cont.observacion;
                        document.getElementById('cli_input_contacto_c').value = cont.contacto_cobranza;
                        document.getElementById('cli_input_fono_c').value = cont.fono_cobranza;
                        document.getElementById('cli_input_celular_c').value = cont.celular_cobranza;
                        document.getElementById('cli_input_mail_c').value = cont.mail_cobranza;
                        document.getElementById('cli_input_obs_c').value = cont.observacion_cobranza;
                    }
                    else{
                        document.getElementById('cli_input_razons').value = "";
                        document.getElementById('cli_input_contacto').value = "";
                        document.getElementById('cli_input_cargo').value = "";
                        document.getElementById('cli_input_fono').value = "";
                        document.getElementById('cli_input_celular').value = "";
                        document.getElementById('cli_input_mail').value = "";
                        document.getElementById('cli_input_obs').value = "";
                        document.getElementById('cli_input_contacto_c').value = "";
                        document.getElementById('cli_input_fono_c').value = "";
                        document.getElementById('cli_input_celular_c').value = "";
                        document.getElementById('cli_input_mail_c').value = "";
                        document.getElementById('cli_input_obs_c').value = "";
                    }

                    //document.getElementById("pre_loader").className = "loader animation-start d-none";
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar contacto cliente rut: ' + rutcli, code, text);
            }
        });
    }

    function seleccionaCeCo(){

        var rutcli = document.getElementById('cli_input_rut').value;
        getContacto(rutcli);
    }

    function getCencosRutcli(rutcli) {

        var vendedor = ejecutivo;

        var parametros = {
            "vendedor": vendedor,
            "rut": rutcli,
            "codemp": 3
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-especifico',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    var obj = json.response;
                    //console.log(obj);

                    cencos=obj.cencos;

                    var cencos_onlynumber = [];

                    cencos.forEach(function(cc){
                        cencos_onlynumber.push([parseInt(cc.cen_cos)]);
                    });

                    cencos_onlynumber=cencos_onlynumber.sort(function(a, b){return a-b});

                    var options_select_ceco = '';
                    options_select_ceco = '<option value="--">Seleccione CeCo</option>\n';

                    cencos_onlynumber.forEach(function(cc){

                        var descripcion='';
                        cencos.forEach(function(ceco){

                            if(ceco.cen_cos==cc.toString()){
                                descripcion=ceco.des_cco;
                            }
                        });

                        options_select_ceco = options_select_ceco + '<option value="' + cc + '">' + cc + ' - ' + descripcion + '</option>\n';

                    });

                    document.getElementsByName('select_ceco')[0].innerHTML = options_select_ceco;
                    document.getElementsByName('select_ceco')[0].value = "0";

                    var ceco = document.getElementsByName("select_ceco")[0].value;
                    getContacto(rutcli, ceco);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar cliente por rut', code, text);
            }
        });
    }
</script>