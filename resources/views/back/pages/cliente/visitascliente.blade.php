@extends('back.master.masterpage')
@section('contenido')
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card page-header p-0">
                <div class="card-block front-icon-breadcrumb row">
                    <div class="breadcrumb-header col">
                        <h4><i class="icofont icofont-foot-print text-inverse m-r-10 f-40 m-t-15-neg" style="box-shadow: none;-moz-box-shadow: none;"></i>Visitas a cliente</h4>
                    </div>
                    <div class="col">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i
                                                class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ url('visitascliente') }}">Visitas a cliente</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <h5 class="sub-title m-b-15">Registro de visitas</h5>
                        <table class="table table-xs table-striped table-hover" id="tb_cartera" style="font-size:12px;">
                            <thead>
                            <tr>
{{--                                <th class="tabledit-toolbar-column"--}}
{{--                                    style="vertical-align: middle; padding-right: 10px!important;padding-left: 20px!important;">--}}
{{--                                    Opciones--}}
{{--                                </th>--}}
                                <th style="vertical-align: middle">Fecha</th>
                                <th style="vertical-align: middle">Cliente</th>
                                <th style="vertical-align: middle">Rut</th>
                                <th style="vertical-align: middle">Contacto</th>
                                <th style="vertical-align: middle">Canal compra</th>
                                <th style="vertical-align: middle">Acompañante</th>
                                <th style="vertical-align: middle">Tema visita</th>
                                <th style="vertical-align: middle">Fecha próxima visita</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th style="vertical-align: middle">Fecha</th>
                                <th style="vertical-align: middle">Cliente</th>
                                <th style="vertical-align: middle">Rut</th>
                                <th style="vertical-align: middle">Contacto</th>
                                <th style="vertical-align: middle">Canal compra</th>
                                <th style="vertical-align: middle">Acompañante</th>
                                <th style="vertical-align: middle">Tema visita</th>
                                <th style="vertical-align: middle">Fecha próxima visita</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
    @include('back.others.js.portal')
@endsection
@section('custom-includes')
@endsection
