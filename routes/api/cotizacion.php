<?php
/**
 * @api {post} /api/cotizacion/validar-cotizacion Valida cotización
 * @apiVersion 1.0.0
 * @apiName validarCotizacion
 * @apiGroup Cotizacion
 * @apiParam {Boolean} [update = false] Parametro para identificar si esta actualizando (false) o ingresando (true) un nuevo producto a la cotización, si está ingresando retorna los productos recomendados
 * @apiParam {Number} [cod_emp=3] Codigo empresa
 * @apiParam {String} vendedor Usuario
 * @apiParam {Number} rutcli Rut del Cliente
 * @apiParam {Array} productos Arreglo de productos, debe incluir las llaves codpro (String), precio (Number), costo (Number), cantidad (Number)
 * @apiSuccess {Number} code Código de respuesta
 * @apiSuccess {Array} response Código de respuesta
 * @apiSuccess (response) {Array} productos Productos
 * @apiSuccess (response) {Number} subtotal Subtotal de la cotización
 * @apiSuccess (response) {Array} recomendados Productos recomendados
 * @apiParamExample {json} Request-Example:
 *     {
 *          "rutcli": 81698900,
 *          "update": false,
 *          "codemp": 3,
 *          "vendedor": "VVALENZUELA",
 *          "productos": [
 *              {
 *                  "codpro": "H401391",
 *                  "precio": 1900,
 *                  "costo": 1800,
 *                  "cantidad": 1
 *              },
 *              {
 *                  "codpro": "H401393",
 *                  "precio": 1990,
 *                  "costo": 1800,
 *                  "cantidad": 1
 *              }
 *          ]
 *     }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *     "response": [
 *          {
 *              "productos": [
 *                  {
 *                      "codpro": "H401391",
 *                      "precio_cotizado": 1900,
 *                      "flag": true,
 *                      "flagType": 0,
 *                      "msg": "Producto agregado correctamente",
 *                      "precio_minimo": 1850
 *                  },
 *                  {
 *                      "codpro": "H401393",
 *                      "precio_cotizado": 1990,
 *                      "flag": false,
 *                      "flagType": 1,
 *                      "msg": "Precio ingresado es inferior al precio mínimo permitido [$2376]",
 *                      "precio_minimo": 2376
 *                  }
 *              ],
 *              "subtotal": 1900,
 *              "margen": "5.26",
 *              "recomendados": {
 *                  "products": [
 *                      {
 *                          "codpro": "Z291620",
 *                          "codsap": "376353",
 *                          "despro": "TOALLA PAPEL JUMBO ECON BCA 250MT 2 ROLLOS ELITE",
 *                          "precio": 4990,
 *                          "costo": 4170,
 *                          "costocomercial": 11536,
 *                          "stock": 13332,
 *                          "contribucion": 820,
 *                          "margen": "16.43",
 *                          "condicion": "S", //S Normal con Sobreventa, N Normal sin sobreventa, P A Pedido
 *                          "categoria": "A",
 *                          "marca": "ELITE",
 *                          "linea": "ASEO Y LIMPIEZA"
 *                      },
 *                      {
 *                          "codpro": "Z289340",
 *                          "codsap": "379340",
 *                          "despro": "PAPEL HIGIENICO JUMBO 4ROLLOS 500MT ECON.BCO ELITE",
 *                          "precio": 4950,
 *                          "costo": 4646,
 *                          "costocomercial": 9447,
 *                          "stock": -400014,
 *                          "contribucion": 304,
 *                          "margen": "6.14",
 *                          "condicion": "S",
 *                          "categoria": "A1",
 *                          "marca": "ELITE",
 *                          "linea": "ASEO Y LIMPIEZA"
 *                      },
 *                  ]
 *              }
 *          }
 *      ]
 */
Route::post('validar-cotizacion', '\App\Http\Helpers\CotizacionHelper@validarCotizacion');


/**
 * @api {put} /api/cotizacion/guardar-cotizacion Guardar Cotización
 * @apiVersion 1.0.0
 * @apiName guardarCotizacion
 * @apiGroup Cotizacion
 * @apiParam {Number} rutcli Rut del Cliente
 * @apiParam {Number} cencos Centro de Costo del Cliente
 * @apiParam {String} vendedor Usuario
 * @apiParam {String} observacion Observación de la cotización
 * @apiParam {String} contacto Nombre del contacto
 * @apiParam {Number} [cod_emp=3] Codigo empresa
 * @apiParam {Array} productos Arreglo de productos, debe incluir las llaves codpro (String), precio (Number), costo (Number), cantidad (Number)
 * @apiSuccess {Number} code Código de respuesta
 * @apiSuccess {Array} response Código de respuesta
 * @apiSuccess (response) {Array} productos Productos
 * @apiSuccess (response) {Array} recomendados Productos recomendados
 * @apiParamExample {json} Request-Example:
 *      {
 *          "rutcli": 16460269,
 *          "cencos": 0,
 *          "vendedor": "FRAMOS",
 *          "observacion": "Cotización de Prueba",
 *          "contacto": "Francisco Ramos",
 *          "codemp": 3,
 *          "productos": [
 *              {
 *                  "codpro": "H401391",
 *                  "precio": 1900,
 *                  "costo": 1500,
 *                  "cantidad": 10
 *              },
 *              {
 *                  "codpro": "H401393",
 *                  "precio": 2500,
 *                  "costo": 1900,
 *                  "cantidad": 1
 *              }
 *          ]
 *      }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *     "response": [
 *          {
 *              "code": 200,
 *              "response": {
 *                  "respuesta": "Cotización guardada con éxito",
 *                  "numcot": "7915597"
 *              }
 *          }
 *      ]
 */
Route::put('guardar-cotizacion', '\App\Http\Helpers\CotizacionHelper@guardarCotizacion');