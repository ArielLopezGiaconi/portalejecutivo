<!DOCTYPE html>

<html>
    <head>
        <style>
            @font-face {
                font-family: 'Abel';
                src: url('{{ $urlFont }}') format("truetype");
                font-style: normal;
            }
            @font-face {
                font-family: 'Abel';
                src: url('{{ $urlFont }}') format("truetype");
                font-style: bold;
            }
            @font-face {
                font-family: 'Abel';
                src: url('{{ $urlFont }}') format("truetype");
                font-style: italic;
            }
            @font-face {
                font-family: 'Abel';
                src: url('{{ $urlFont }}') format("truetype");
                font-weight: bold;
                font-style: oblique;
            }
            @page {
                margin: 0cm 0cm;
                font-family: Abel;
            }
    
            body {
                margin: 3cm 0.5cm 0.5cm;
            }
    
            header {
                position: fixed;
                top: 0.2cm;
                left: 0.5cm;
                right: 0cm;
                height: 3cm;
                color: white;
                text-align: center;
                line-height: 30px;
            }
    
            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
                color: white;
                text-align: center;
                line-height: 0px;
            }
        </style>

    </head>
    <header>
        <div>
            <img src="{{ $header }}" 
            style="width: 20cm; height: 2.4cm; float: left;">
        </div>
    </header>
    <body>
        <main>   
            @foreach($datos[0]['encabezado'] as $resul)
            <!--h2 style="text-align: center; font-family: Abel" >Reporte Cobranza</h2-->
            <!--hr style="color: black; background-color: black; width:99%;" /-->
            <table style="font-size: 10px; font-family: Abel, sans-serif;" >
                <body>
                    <tr>
                        <td>Rut: {{ $resul->rutcli }}</td>
                    </tr>
                    <tr>
                        <td>Cliente: {{ $resul->razons }}</td>
                        <td style="padding-left: 2cm;">Encargado de pagos: {{ $resul->cobranza }}</td>
                    </tr>
                    <tr>
                        <td>Dirección: {{ $resul->direccion }}</td>
                        <td style="padding-left: 2cm;">Telefono: {{ $resul->telcob }}</td>
                    </tr>
                    <tr>
                        <td>Comuna: {{ $resul->comuna }}</td>
                        <td style="padding-left: 2cm;">Mail cobranza: {{ $resul->mail_cobrador }}</td>
                    </tr>
                    <tr>
                        <td>Ciudad: {{ $resul->ciudad }}</td>
                    </tr>
                </body>
            </table>
            <hr style="color: black; background-color: black; width:99%;" />
            @endforeach
            <table id="detallenota" class="table" style="font-size: 10px; font-family: Abel, sans-serif; border-collapse: collapse; width: 100%;">
                <thead>
                <tr role="row" >
                    {{-- <th style=" text-align: left;">Cliente</th> --}}
                    <th style=" text-align: left;">FACTURA</th>
                    <th style=" text-align: left;">CTO. COSTO</th>
                    <th style=" text-align: left;">NOMBRE DEUDOR</th>
                    <th style=" text-align: left;">OC</th> 
                    <th style=" text-align: left;">FECHA EMISION</th>
                    <th style=" text-align: left;">FECHA VENC.</th>
                    <th style=" text-align: left;">TOTAL</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datos[0]['detalle'] as $resul)
                    <tr>
                        <td >{{ $resul->factura }}</td>
                        <td >{{ $resul->cencos }}</td>
                        <td >{{ $resul->razons }}</td>
                        <td >{{ $resul->doc_asociados }}</td>
                        <td >{{ substr($resul->fecemi,0,10) }}</td>
                        <td >{{ substr($resul->fecven,0,10) }}</td>
                        <td style=" text-align: right;" >{{ number_format($resul->totgen, 0, ',','.') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr style="color: black; background-color: black; width:99%;" />
            <table style="font-size: 10px; font-family: Abel, sans-serif;" >
                <body>
                    @foreach($datos[0]['total'] as $resul)
                    <tr>
                        <td>TOTAL GENERAL:</td>
                        <td style="padding-left: 15.5cm;">{{ number_format($resul->total, 0, ',','.') }}</td>
                    </tr>
                    @endforeach
                </body>
            </table>
            <hr style="color: black; background-color: black; width:99%; font-family: Abel" />
            <p style="font-size: 8px; font-family: Abel; margin-top: 1cm;">
                <h4 style="font-family: Abel; font-size: 8px;">Tener presente</h4>
                <div style="font-family: Abel; font-size: 8px;">1.- Recordar que existe una modificación en la ley 19.983 relacionada con la aceptación o rechazo de documentos electronicos en el SII, nueva ley es la 20956</div>
                <div style="font-family: Abel; font-size: 8px;">2.- Si cancela la deuda con documentos, solo se autoriza pagos con vencimientos al día, cualquier plazo adicional debe ser analizado por el departamento de credito al correo cobranza@dimerc.cl o cobranzas@dimerc.cl .</div>
            </p>
        </main>
        <footer>
            <img src="{{ $footer }}" alt="round-img" 
            style="float:left; width: 100%; height: 3cm; ">
        </footer>
    </body>
</html>