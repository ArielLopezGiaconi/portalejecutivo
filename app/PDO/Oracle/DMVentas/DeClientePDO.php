<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 18:13
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\DeCliente;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class DeClientePDO extends Model
{
    public static function getDominio($rutcli, $cencos)
    {
        $sql = "SELECT getdominio(5, get_formapagocli(3, :rut_cli, :cen_cos, 5)) as fpago FROM dual";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cli' => $rutcli,
            'cen_cos' => $cencos
        ]);

        return ($resultado != null)
            ? $resultado : false;
    }

    public static function getCencosByRutcli($rutcli, $codemp = 3, $have_codsap = true, $first_row = false)
    {
        $arrayReturn = null;
        $sql = "SELECT * FROM de_cliente WHERE rutcli = :rut_cli AND codemp = :cod_emp";
        if ($have_codsap) $sql .= " AND id_cliente_sap IS NOT NULL";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cli' => $rutcli,
            'cod_emp' => $codemp,
        ]);

        $arrayReturn = DeClientePDO::returnFormat($resultado, $first_row);

        if ($arrayReturn) {
            $arrayReturn = new Collection($arrayReturn);
        }

        return $arrayReturn;
    }

    public static function getCencosByRutcliCustom1($rutcli, $codemp = 3, $usuario)
    {
        $esAdmin = $usuario->superadmin;
        $rutusu = $usuario->rutusu;
        $arraReturn = array();
        if ($esAdmin == 1) {
            $sql = "SELECT a.rutcli,"
                . " a.cencos, a.descco,"
                . " b.coddir, b.dirdes,"
                . " b.codcom, SAP_Get_ComCiuReg(b.codcom, 'C', 'D') AS descom,"
                . " b.codciu, SAP_Get_ComCiuReg(b.codcom, 'D', 'D') as desciu,"
                . " SAP_Get_ComCiuReg(b.codcom, 'R', 'C') AS codreg, SAP_Get_ComCiuReg(b.codcom, 'R', 'D') AS desreg"
                . " FROM de_cliente a"
                . " INNER JOIN re_ddescli b ON a.rutcli = b.rutcli AND TO_CHAR(b.coddir) = a.dirdes"
                . " WHERE a.rutcli = :rut_cli AND a.codemp = :cod_emp";

            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'rut_cli' => $rutcli,
                'cod_emp' => $codemp,
            ]);
        } else {
            $sql = "SELECT a.rutcli,"
                . " a.cencos, a.descco,"
                . " b.coddir, b.dirdes,"
                . " b.codcom, SAP_Get_ComCiuReg(b.codcom, 'C', 'D') AS descom,"
                . " b.codciu, SAP_Get_ComCiuReg(b.codcom, 'D', 'D') as desciu,"
                . " SAP_Get_ComCiuReg(b.codcom, 'R', 'C') AS codreg, SAP_Get_ComCiuReg(b.codcom, 'R', 'D') AS desreg"
                . " FROM de_cliente a"
                . " INNER JOIN re_ddescli b ON a.rutcli = b.rutcli AND TO_CHAR(b.coddir) = a.dirdes"
                . " WHERE a.rutcli = :rut_cli AND a.codemp = :cod_emp "
                . " AND a.rutven = :rutusu";

            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'rut_cli' => $rutcli,
                'cod_emp' => $codemp,
                'rutusu' => $rutusu
            ]);
        }

        $arrayReturn = new Collection($resultado);
        return $arrayReturn;

    }

    public static function getSpecificCencos($rutcli, $cencos = 0, $codemp = 3, $first_row = false)
    {
        $sql = "";
    }

    private static function returnFormat($resultado, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($resultado as $registro) {
            $cencos = new DeCliente($registro);
            if ($first_row) {
                return $cencos;
            }
            $arrayReturn[] = $cencos;
        }

        return $arrayReturn;
    }

    public static function getForPlaPag($rutcli, $cencos, $codemp)
    {
        $sql = "SELECT forpag, plapag FROM de_cliente WHERE rutcli = :rutcli AND cencos = :cencos 
        AND codemp = :codemp";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'cencos' => $cencos,
            'codemp' => $codemp
        ]);

        $arrayReturn = null;
        if ($resultado && count($resultado) > 0) {
            $registro = $resultado[0];
            $arrayReturn = array(
                'forpag' => $registro->forpag,
                'plapag' => $registro->plapag
            );
        }
        return $arrayReturn;
    }

    public static function canSeeCliente($rutcli, $rutven)
    {
                $sql = "SELECT COUNT(1) AS EXISTE
                FROM en_cliente a WHERE rutcli = :rutcli AND codemp = 3
                and exists (
                select 1 from de_cliente b
                where b.rutcli = a.rutcli
                and b.codemp = a.codemp
                and (b.rutven in(select c.rutusu
                from re_parejas c, ma_usuario d
                where c.codgru = d.grupar
                and d.rutusu = :rutusu)
                or b.rutven = :rutusu1))";    
                // $sql = "select
                //         count(*) as existe
                //         from portal_ej_new_gestion_cartera
                //         where rutcli=:rutcli";
                // and rutven in
                // (
                //     select
                //     coalesce(codven_parner,rutven)
                //     from portal_ej_parteners
                //     where codven=:rutven
                // )";

                // $sql = "SELECT COUNT(*) AS existe"
                //     . " FROM de_cliente"
                //     . " WHERE rutcli = :rutcli AND rutven = :rutven and codemp = 3";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'rutusu' => $rutven,
            'rutusu1' => $rutven
        ]);

        return ($resultado != null && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getContactsByRutcli($rutcli, $rutven, $codemp = 3)
    {
        $sql = "select upper(contac) || ' -- ' || fono01 || ' -- ' || emails as contacto 
        from de_cliente 
        where rutcli = :rutcli and codemp = :codemp
        group by upper(contac), fono01, emails";
        
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'codemp' => $codemp
        ]);

        return $resultado;
    }

    public static function getMailsByRutcli($rutcli, $rutven, $codemp = 3)
    {
        $sql = "select cencos, emails as contacto 
        from de_cliente 
        where rutcli = :rutcli and codemp = :codemp and rutven = :rutven
        group by emails, cencos";
        
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'rutven' => $rutven,
            'codemp' => $codemp
        ]);

        return $resultado;
    }
}