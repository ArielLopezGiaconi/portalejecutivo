<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 29/08/2018
 * Time: 13:38
 */

namespace App\Http\Helpers\Veracore;

use App\Helpers\GeneralHelper;
use App\Http\Controllers\Veracore\StockController;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\MaProductPDO;


class StockHelper extends GeneralHelper
{
    public function setStockVeracore($in_codpro = null, $in_stock = -1)
    {
        // 1.- Validar Ingreso codpro
        if ($in_codpro == null || trim($in_codpro) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codpro del producto a consultar [1|codpro]");

            return $responseFormatt->returnToJson();
        }

        // 2.- Validar número de Stock
        if (is_int($in_stock) && $in_stock < 0) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar número de stock válido, mayor o igual a 0");

            return $responseFormatt->returnToJson();
        }

        // 3.- Validar existencia producto Código Dimerc
        $product = MaProductPDO::getProductByCod($in_codpro);
        if (!$product) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Producto no existe en la base de datos (" . $in_codpro . ")");

            return $responseFormatt->returnToJson();
        }

        // 4.- Validar que producto tenga su CODSAP
        if($product->getCodsap() == null || trim($product->getCodsap()) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Producto no tiene asociado su código SAP.");

            return $responseFormatt->returnToJson();
        }

        // 3.- Validar que exista stock del producto en bodega específica
        $controlador = new StockController();
        return $controlador->setStockVeracore($product->getCodsap(), (int)$in_stock);
    }

    public function setPriceVeracore() {

    }

    public function updateAllPriceSap() {
        $controlador = new StockController();
        return $controlador->updateAllPriceSap();
    }

    public function updateAllStockSap() {
        $controlador = new StockController();
        return $controlador->updateAllStockSapV2();
    }
}
