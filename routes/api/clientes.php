<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 12:54
 */

/**
 * @api {post} /api/clientes/buscar-rsocial Buscar por razón social
 * @apiVersion 1.0.0
 * @apiName buscarPorRazonSocial
 * @apiGroup Clientes
 * @apiParam {String} vendedor  Nombre de usuario
 * @apiParam {String} razon_social  Nombre razón social de la empresa
 * @apiParam {Number} [cod_emp=3] Codigo empresa
 * @apiSuccess {Number} code Código de respuesta
 * @apiSuccess {Array} response Código de respuesta
 * @apiSuccess (response) {String} rutcli RUT Cliente
 * @apiSuccess (response) {String} razons Razón social del cliente
 * @apiParamExample {json} Request-Example:
 *     {
 *       "vendedor": "TVILUGRON",
 *       "razon_social": "UNIVERSIDAD"
 *     }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *     "response": [
 *     {
 *       "rutcli": "72831900",
 *       "razons": "ASOC DE FUNCIONARIOS NO ACADEMICOS UNIVERSIDAD DEL BIO BIO"
 *     },
 *     {
 *      "rutcli": "74432500",
 *      "razons": "ASOC NAC DE ACADEMICOS DE LA UNIVERSIDAD DE CHILE"
 *     }
 *  }
 * ]
 */
Route::post('buscar-rsocial', '\App\Http\Helpers\ClientesHelper@buscarPorRazonSocial');

/**
 * @api {post} /api/clientes/buscar-especifico Buscar cliente específico por RUT
 * @apiVersion 1.0.0
 * @apiName buscarEspecificoPorRut
 * @apiGroup Clientes
 * @apiParam {String} vendedor  Nombre de usuario
 * @apiParam {String} rut  Nombre razón social de la empresa
 * @apiParam {Number} [cod_emp=3] Codigo empresa
 * @apiParamExample {json} Request-Example:
 *     {
 *          "vendedor": "TVILUGRON",
 *          "rut": 99147000,
 *          "codemp" : 3
 *     }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * "response": [
 * {
 *      "rutcli": "99147000",
 *      "razons": "BCI SEGUROS GENERALES S A",
 *      "digcli": "K",
 *      "segmento": "HOLDING",
 *      "negocio_eerr": "DIMERC CORP",
 *      "forpag": "FACTURA",
 *      "plapag": "30 DIAS",
 *      "estado_cliente": null,
 *      "cencos": [
 *      {
 *          "cen_cos": "26",
 *          "des_cco": "G. FINANZAS Y GESTION",
 *          "dir_cod": "165",
 *          "dir_des": "HUERFANOS 1189 PISO 3",
 *          "dir_com_cod": "292",
 *          "dir_com_des": "SANTIAGO",
 *          "dir_ciu_cod": "130",
 *          "dir_ciu_des": "SANTIAGO",
 *          "dir_reg_cod": "13",
 *          "dir_reg_des": "REGION METROPOLITANA"
 *      }
 *      ],
 *      "dirdes": [ {
 *          "dir_cod": "154",
 *          "dir_des": "HUERFANOS 1189 PISO 5",
 *          "com_cod": "292",
 *          "com_des": "SANTIAGO",
 *          "ciu_cod": "130",
 *          "ciu_des": "SANTIAGO",
 *          "reg_cod": "13",
 *          "reg_des": "REGION METROPOLITANA"
 *      }]
 * }
 * ]
 */
Route::post('buscar-especifico', '\App\Http\Helpers\ClientesHelper@buscarEspecificoPorRut');

// Route::get('específico', '');