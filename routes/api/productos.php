<?php
/**
 * @api {post} /api/productos/listar-productos Buscar productos por descripción o código
 * @apiVersion 1.0.0
 * @apiName listarProductos
 * @apiGroup Productos
 * @apiParam {String} search_string Parametro de busqueda
 * @apiParam {String} [type_search = 'codpro'] Parametro de busqueda
 * @apiParam {Boolean} [fulltext = false] Parametro de busqueda
 * @apiSuccess {Number} code Código de respuesta
 * @apiSuccess {Array} response Código de respuesta
 * @apiSuccess (response) {String} codpro Codigo del Producto
 * @apiSuccess (response) {String} despro Descripción del Producto
 * @apiSuccess (response) {String} codsap Código SAP del Producto
 * @apiParamExample {json} Request-Example:
 *     {
 *          "type_search": "despro",
 *          "search_string": "TE",
 *          "fulltext": false
 *     }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *     "response": [
 *      {
 *          "codpro": "AP02820",
 *          "despro": "TELON C/TRIPODE 178X178MT APOLLO",
 *          "codsap": "204008"
 *      },
 *      {
 *          "codpro": "Z180012",
 *          "despro": "TE YELLOW LABEL CAJA 20UN LIPTON",
 *          "codsap": "103224"
 *      },
 *      {
 *          "codpro": "Z106624",
 *          "despro": "TE HIERBAS MENTA CAJA 20UN LIPTON",
 *          "codsap": "103218"
 *      },
 *      {
 *          "codpro": "Z106924",
 *          "despro": "TE HIERBAS MANZANILLA CAJA 20UN LIPTON",
 *          "codsap": "353563"
 *      },
 *      {
 *          "codpro": "Z105824",
 *          "despro": "TE CLUB ORIGINAL 20UN (EX CAJA ROJA)",
 *          "codsap": "103214"
 *      },
 *      {
 *          "codpro": "P285820",
 *          "despro": "TELON MURAL 152X 152MT APOLLO",
 *          "codsap": "204006"
 *      },
 *      ]
 */
Route::post('listar-productos', '\App\Http\Helpers\ProductosHelper@listarProductos');


/**
 * @api {post} /api/productos/producto-especifico Buscar producto especifico por codpro o codsap
 * @apiVersion 1.0.0
 * @apiName productoEspecifico
 * @apiGroup Productos
 * @apiParam {Number} rutcli Rut del cliente
 * @apiParam {Number} [cod_emp=3] Codigo empresa
 * @apiParam {String} cod_producto Codigo del producto a buscar (codpro o codsap)
 * @apiParam {String} tipo_busqueda CODPRO o CODSAP
 * @apiSuccess {Number} code Código de respuesta
 * @apiSuccess {Array} response Código de respuesta
 * @apiSuccess (response) {String} codpro Codigo del Producto
 * @apiSuccess (response) {String} codsap Codigo SAP del Producto
 * @apiSuccess (response) {String} despro Descripción del Producto
 * @apiSuccess (response) {Number} precio Precio del Producto
 * @apiSuccess (response) {Number} costo Costo del Producto
 * @apiSuccess (response) {Number} costocomercial Costo Comercial del Producto
 * @apiSuccess (response) {Number} stock Stock disponible del Producto
 * @apiSuccess (response) {Number} margen Margen del Producto
 * @apiSuccess (response) {String} condicion Condición de venta del Producto, S = Normal con sobreventa, P = A Pedido, N = Normal sin sobreventa
 * @apiSuccess (response) {String} categoria Categoria de comisión del producto
 * @apiSuccess (response) {String} marca Marca del producto
 * @apiParamExample {json} Request-Example:
 *      {
 *          "rutcli": 16460269,
 *          "cod_producto": "H401391",
 *          "tipo_busqueda": "CODPRO"
 *      }
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *     "response": [
 *          "codpro": "H401391",
 *          "codsap": "401391",
 *          "despro": "PAPEL FOTOCOPIA CARTA 500HJ DIMERC",
 *          "precio": 1850,
 *          "costo": 1610,
 *          "costocomercial": 1990,
 *          "stock": 120798,
 *          "contribucion": 240,
 *          "margen": "12.97",
 *          "condicion": "S",
 *          "categoria": "A",
 *          "marca": "DIMERC",
 *          "linea": "ASEO Y LIMPIEZA"
 *      ]
 */
Route::post('producto-especifico', '\App\Http\Helpers\ProductosHelper@productoEspecifico');
