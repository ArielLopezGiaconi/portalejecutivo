<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">	
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <style>
        /* @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,300;0,400;0,600;0,700;1,300;1,400;1,500&display=swap'); */
        @import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap');
        body{
            /* font-family: 'Montserrat', sans-serif; */
            /* margin: 0.5cm 0.5cm 0.5cm; */
            font-family: 'Quicksand', sans-serif;
            font-size: 8px;
            width: 100%;
        }
        .banner{
            width: 100%;
        }
        .title{
            border: 1px solid #A01829;
            background-color: #A01829;
            font-size: 12px;
        }
        .title2{
            font-size: 12px;
        }
        h1{
            color: #fff;
        }
        h2{
            display: inline;
        }
        .red{
            color: #A01829;
        }
        .grafico{
            width: 100%;
        }
        span{
            float: left;
        }
        /* .no-list ol li {
            list-style: none;
        } */
        .customer{
            width: 100%;
            background-color: #bec0ba;
        }
        .customer2{
            background-color: #6c767c;
        }
        .part{
            font-size: 19px;
        }
        .sku_cmc{
            width: 100%;
            font-size: 8px;
        }
        .line_b{
            border: 1px solid #2f6791;
        }
        .cmd{
            table-layout: fixed;
        }
        .tdcmd{
            word-wrap:break-word
        }
        .bg-b{
            background-color: #266ca4;
        }
        .demo{
            background-color: #d8e4ec;
        }
        .none{
            background-color: transparent!;
        }
        .red{
            color: #CF1B2B;
        }
        #chartdiv {
            width: 100%;
            height: 500px;
        }
        #chartdiv2 {
            width: 100%;
            height: 500px;
        }
        .bor-top-red{
            border-top: 3px solid #cf1b2b;
        }
        .marg-top{
            margin-top:2px;
            margin-bottom: 5px;
        }.gray-bg{
            background-color:lightgray;
        }
        .extra-pad{
            padding: 1px;
        }
    </style>
    <title>Dimerc</title>
</head>
<body>
    <div class="col-md-12">
        <img src="https://apispricing.dimerc.cl/apis_pricing_dimerc/img/banner.png" class="banner" alt="banner dimerc">
    </div>
    <div class="container no-list col-md-12">
        {{-- ------------------------Este espacio es para los datos de cabecera del reporte--------------------------------------- --}}
        <div class="row bor-top-red">
            <div class="col gray-bg">
                <label>HOLDING</label>
                <label>RAZON SOCIAL</label>
            </div>
            <div class="col">
                <label>:{{ $cabecera[0]->rutcli }}</label>
                <label>:{{ $cabecera[0]->razons }}</label>
            </div>
            <div class="col gray-bg">
                <label>KAM CUENTA</label>
                <label>CORREO</label>
                <label>FECHA REPORTE</label>
            </div>
            <div class="col">
                <label>:{{ $cabecera[0]->nombre }}</label>
                <label>:{{ $cabecera[0]->mail }}</label>
                <label>:{{ $cabecera[0]->fecha }}</label>
            </div>
        </div>
        {{-- ---------------------------------Este espacio es para los datos generales del cliente------------------------------------- --}}
        <div class="row">
            <div class="col title text-white">
                <label>1. INFORMACIÓN GENERAL</label>
            </div>
        </div>
        {{-- ----------------------------Aqui va el primer reporte------------------------------------ --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> CONSUMO POR</label>
                <label class="red">LINEA MENSUAL</label>
            </div>
        </div>
        <div class="row marg-top">
            <!-- <img src="assets/img/grafico.png" alt="grafico"> -->
                {{-- <div class="col-3">
                    
                </div> --}}
                <div class="col-9">
                    <div id="chartdiv"></div>
                </div>
        </div>
        {{-- -----------------------------------Aqui va el segundo reporte----------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> CONSUMO POR</label>
                <label class="red">LINEA ULTIMO SEMESTRE</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3">
                <!-- <img src="assets/img/grafico2.png" class="grafico" alt="grafico"> -->
            </div> --}}
            <div class="col">
                <div id="chartdiv2"></div>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°3 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> PRINCIPALES 10 DE SKU CON MÁS </label>
                <label class="red">CONSUMO DEL MES EN CURSO</label>
            </div>
        </div>
        <div class="row">
            {{-- <div class="col-3">
                
            </div> --}}
            <div class="col extra-pad">
                <table class="" >
                    <tr class="customer">
                        <th>CÓDIGO</th>
                        <th>DESCRIPCIÓN</th>
                        <th>MARCA</th>
                        <th>CANTIDAD</th>
                        <th></th>
                        <th>MONTO</th>
                        {{-- <th class="customer2" >% DEL TOTAL</th> --}}
                    </tr>
                    
                    @foreach($reporte3[0]->datos as $resul)
                    <tr>
                        <td>{{ $resul->codpro }}</td>
                        <td>{{ $resul->despro }}</td>
                        <td>{{ $resul->marca }}</td>
                        <td>{{ $resul->cantid }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->neto,0,',','.') }}</td>
                        {{-- <td></td> --}}
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°4 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> PRINCIPALES 10 DE SKU CON MÁS </label>
                <label class="red">CONSUMO TRIMESTRAL</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3">
                
            </div> --}}
            <div class="col extra-pad">
                <table class="" >
                    <tr class="line_b">
                        <th class="customer2">PROMEDIO CONSUMO TRIMESTRAL</th>
                        <th>$</th>
                        <th>{{ number_format($reporte4[0]->promedio,0,',','.') }}</th>
                    </tr>
                    
                    <tr class="customer">
                        <th>CÓDIGO</th>
                        <th>DESCRIPCIÓN</th>
                        <th>CANTIDAD</th>
                        <th></th>
                        <th>MONTO</th>
                        <th>NETO</th>
                        <th>TOTAL</th>
                        <th class="customer2" >% DE INSIDENCIA TRIMESTRAL</th>
                        
                    </tr>
                    @foreach($reporte4[0]->datos as $resul)
                    <tr>
                        <td>{{ $resul->codpro }}</td>
                        <td>{{ $resul->despro }}</td>
                        <td>{{ $resul->cantid }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->neto,0,',','.') }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->total_neto,0,',','.') }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°5 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> PRINCIPALES 10 DE CENTROS DE COSTO CON </label>
                <label class="red">MAYOR CANTIDAD DE PEDIDOS DEL MES EN CURSOL</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3">
                
            </div> --}}
            <div class="col">
                <table class="cmd extra-pad">
                    <tr class="">
                        <th class="customer2">CONSUMO TOTAL DEL MES EN CURSO</th>
                        <th>$</th>
                        <th>{{ number_format$reporte5[0]->total,0,',','.') }}</th>
                    </tr>
                    
                    <tr class="customer">
                        <th>CC</th>
                        <th>NOMBRE</th>
                        <th>CANT DE PEDIDOS</th>
                        <th></th>
                        <th>MONTO TOTAL DE COMPRA</th>
                        <th>MONTO PROMEDIO POR CC</th>
                        <th class="customer2" >% DE INSIDENCIA EN LA COMPRA TOTAL DEL MES</th>
                        
                    </tr>
                    @foreach($reporte5[0]->datos as $resul)
                    <tr>
                        <td>{{ $resul->cencos }}</td>
                        <td>{{ $resul->descco }}</td>
                        <td>{{ $resul->cantid }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->neto,0,',','.') }}</td>
                        <td>{{ number_format($resul->prom_cc,0,',','.') }}</td>
                        <td>{{ ($resul->porc_del_total*100) }}%</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°6 -------------------------------------------- --}}
        <div class="row">
            <div class="col title2">
                <label> PRINCIPALES 10 DE CENTROS DE COSTO CON </label>
                <label class="red">MAYOR CONSUMO DEL MES EN CURSO</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3">
                
            </div> --}}
            <div class="col">
                <table class="extra-pad" >
                    <tr class="line_b">
                        <th class="customer2">CONSUMO TOTAL DEL MES EN CURSO</th>
                        <th></th>
                        <th>$</th>
                        <th>{{ number_format($reporte6[0]->total,0,',','.') }}</th>
                        <th></th>
                    </tr>
                                        
                    <tr class="customer">
                        <th>CC</th>
                        <th>NOMBRE</th>
                        <td></td>
                        <th>MONTO TOTAL DE COMPRA</th>
                        <th class="customer2" >% DE INSIDENCIA EN LA COMPRA TOTAL DEL MES</th>
                    </tr>
                    @foreach($reporte6[0]->datos as $resul)
                    <tr>
                        <td>{{ $resul->cencos }}</td>
                        <td>{{ $resul->descco }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->neto,0,',','.') }}</td>
                        <td>{{ ($resul->porc_del_total*100) }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°7 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> PRINCIPALES 5 DE SKU MAYOR CONSUMO DE LOS </label>
                <label class="red">10 CENTROS DE COSTO CON MÁS COMPRA</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3"></div> --}}
            <div class="col">
                <table class="extra-pad" >
                    <tr class="">
                        <th class="customer2">CONSUMO TRIMESTRAL</th>
                        <th></th>
                        <th>$</th>
                        <th>{{ number_format($reporte7[0]->Valorizado_mes,0,',','.')) }}</th>
                    </tr>
                    
                    <tr class="customer">
                        <th>CÓDIGO</th>
                        <th>DESCRIPCIÓN</th>
                        <th>CANTIDAD TRIMESTRAL</th>
                        <th>CONSUMO MENSUAL</th>
                        <th>VALORIZADO TOTAL DEL MES</th>
                        <th class="customer2" >% DE INSIDENCIA TRIMESTRAL</th>
                        
                    </tr>
                    @foreach($reporte7[0]->datos as $resul)
                    <tr>
                        <td>{{ $resul->codpro }}</td>
                        <td>{{ $resul->despro }}</td>
                        <td>{{ $resul->total_cantid }}</td>
                        <td>{{ number_format($reporte7[0]->Total_5_skus,0,',','.')) }}</td>
                        <td>{{ number_format($reporte7[0]->Consumo,0,',','.')) }}</td>
                        <td>{{ ($reporte7[0]->Porcentaje*100) }}%</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°8 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> CONSUMO DE MARCA PROPIA</label>
                <label class="red">ULTIMO SEMESTRE</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-3"></div> --}}
            <div class="col">
                <table class="extra-pad" >
                    <tr class="customer">
                        <th>AÑO</th>
                        <th>MES</th>
                        <th></th>
                        <th>CONSUMO TOTAL DEL MES</th>
                        <th></th>
                        <th>CONSUMO TOTAL MP</th>
                        <th class="customer2" >% MP DEL TOTAL DE LA COMPRA</th>
                    </tr>
                    @foreach($reporte8 as $resul)
                    <tr>
                        <td>{{ $resul->ano }}</td>
                        <td>{{ $resul->mes }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->consumo_total_mes,0,',','.')) }}</td>
                        <td>$</td>
                        <td>{{ number_format($resul->consumo_mp,0,',','.')) }}</td>
                        <td>{{ ($resul->porc_mp_total*100 )}}%</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    {{-- --------------------------------------------- Aqui va el reporte N°9 -------------------------------------------- --}}
    <div class="container no-list col-md-12">
        <div class="row marg-top">
            <div class="col title text-white">
                <label> 2. CONVENIO</label>
            </div>
        </div>
        @foreach($reporte9 as $resul)                    
        <div class="row marg-top">
            <div class="col-6">
                <div class="row">
                    <div class="col-6 gray-bg">FECHA DE INICIO CONVENIO</div>
                    <div class="col">{{ $resul->feciniconv }}</div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-6 gray-bg">FECHA DE TERMINO CONVENIO</div>
                    <div class="col">{{ $resul->fecfinconv }}</div>
                </div>
            </div>

        </div>
        <div class="row marg-top gray-bg text-center">
            <div class="col">
                VENTA DE SU SKU EN CONVENIO
            </div>
            <div class="col">
                VENTA DE SU SKU FUERA DE CONVENIO
            </div>
        </div>
        <div class="row">
            <div class="col gray-bg">
                <ol>
                    <li>CONSUMO {{ $resul->nombre_mes }} / {{ $resul->ano }}</li>
                    <li>% DEL CONSUMO TOTAL</li>
                </ol>
            </div>
            <div class="col">
                <ol>
                    <li>:$ {{ number_format($resul->neto_conv,0,',','.')) }}</li>
                    <li>:% {{ ($resul->porc_convenio*100) }}</li>
                </ol>
            </div>
            <div class="col gray-bg">
                <ol>
                    <li>CONSUMO {{ $resul->nombre_mes }} / {{ $resul->ano }}</li>
                    <li>% DEL CONSUMO TOTAL</li>
                </ol>
            </div>
            <div class="col">
                <ol>
                    <li>:$ {{ number_format($resul->neto_sinconv,0,',','.')) }}</li>
                    <li>:%{{ ($resul->porc_sinconvenio*100) }}</li>
                </ol>
            </div>
        </div>
        @endforeach
        {{-- --------------------------------------------- Aqui va el reporte N°10 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> SKU</label>
                <label class="red">AGREGADOS A CONVENIO</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-4"></div> --}}
            <div class="col">
                <table class="">
                    <tr class="customer">
                        <th>CÓDIGO</th>
                        <th>DESCRIPCIÓN</th>
                        <th>MARCA</th>
                        <th>ADMINISTRADOR</th>
                    </tr>
                    
                    @foreach($reporte10 as $resul)
                    <tr>
                        <td>{{ $resul->codpro }}</td>
                        <td>{{ $resul->despro }}</td>
                        <td>{{ $resul->marca }}</td>
                        <td>{{ $resul->admin }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- --------------------------------------------- Aqui va el reporte N°11 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title2">
                <label> SKU DE</label>
                <label class="red">CONVENIO SIN CONSUMO ULTIMO TRIMESTE</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-5"></div> --}}
            <div class="col">
                <table class="">
                    <tr class="customer">
                        <th>CÓDIGO</th>
                        <th>DESCRIPCIÓN</th>
                        <th>MARCA</th>
                    </tr>
                    
                    @foreach($reporte10 as $resul)
                    <tr>
                        <td>{{ $resul->codpro }}</td>
                        <td>{{ $resul->despro }}</td>
                        <td>{{ $resul->marca }}</td>
                    </tr>
                    @endforeach
                    
                </table>
            </div>
        </div>
        
        {{-- --------------------------------------------- Aqui va el reporte N°12 -------------------------------------------- --}}
        <div class="row marg-top">
            <div class="col title text-white">
                <label> 3. WEB</label>
            </div>
        </div>
        <div class="row marg-top">
            <div class="col title2">
                <label class="red">VENTA GENERADA POR WEB</label>
                <label>, MODELO DE INTEGRACIÓN (EDI) U OTROS</label>
            </div>
        </div>
        <div class="row marg-top">
            {{-- <div class="col-2"></div> --}}
            <div class="col">
                <table class="" >
                    <tr class="customer">
                        <th>MES</th>
                        <th>TOTAL DEL MES</th>
                        <th>TOTAL WEB</th>
                        <th>% DEL TOTAL</th>
                        <th>TOTAL INTEGRACION</th>
                        <th>% TOTAL</th>
                        <th>TOTAL OTROS</th>
                        <th>% TOTAL</th>
                    </tr>
                    
                    @foreach($reporte12 as $resul)
                    <tr>
                        <td>{{ $resul->nombre_mes }}</td>
                        <td>$ {{ number_format($resul->total_mes,0,',','.')) }}</td>
                        <td>$ {{ number_format($resul->total_web,0,',','.')) }}</td>
                        <td>{{ ($resul->porc_web*100) }} %</td>
                        <td>$ {{ number_format($resul->total_integ,0,',','.')) }}</td>
                        <td>{{ ($resul->porc_integ*100) }}%</td>
                        <td>$ {{ number_format($resul->total_otros,0,',','.')) }}</td>
                        <td>{{ ($resul->porc_otros*100) }}%</td>
                    </tr>
                    @endforeach
                    
                </table>
            </div>
        </div>
    </div>

        <footer class="bg-danger text-white" >
            <div class="col-md-12">
                <img src="https://apispricing.dimerc.cl/apis_pricing_dimerc/img/footer.png" class="banner" alt="banner dimerc">
            </div>
        </footer>
        
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/556cf60d5f.js"></script>
        <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
        <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
        <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
        <script>
            am4core.ready(function() {
            
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            
            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);
            
            // Add data
            chart.data = JSON.parse('{!! $reporte1 !!}');

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "linea";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            
            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
              if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
              }
              return dy;
            });
            
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            
            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "neto";
            series.dataFields.categoryX = "linea";
            series.name = "Visits";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;
            
            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            
            }); // end am4core.ready()
        </script>


        <script>
            am4core.ready(function() {
            
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            
            var chart = am4core.create('chartdiv2', am4charts.XYChart)
            chart.colors.step = 2;
            
            chart.legend = new am4charts.Legend()
            chart.legend.position = 'top'
            chart.legend.paddingBottom = 20
            chart.legend.labels.template.maxWidth = 95
            var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
            xAxis.dataFields.category = 'category'
            xAxis.renderer.cellStartLocation = 0.1
            xAxis.renderer.cellEndLocation = 0.9
            xAxis.renderer.grid.template.location = 0;
            
            var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
            yAxis.min = 0;
            
            function createSeries(value, name) {
                var series = chart.series.push(new am4charts.ColumnSeries())
                series.dataFields.valueY = value
                series.dataFields.categoryX = 'category'
                series.name = name
            
                series.events.on("hidden", arrangeColumns);
                series.events.on("shown", arrangeColumns);
            
                var bullet = series.bullets.push(new am4charts.LabelBullet())
                bullet.interactionsEnabled = false
                bullet.dy = 30;
                bullet.label.text = '{valueY}'
                bullet.label.fill = am4core.color('#ffffff')
            
                return series;
            }
            
            chart.data = JSON.parse('{!! $reporte2 !!}');
            
            createSeries('first', '{{ $meses[0]["nombre_mes1"] }}');
            createSeries('second', '{{ $meses[0]["nombre_mes2"] }}');
            createSeries('third', '{{ $meses[0]["nombre_mes3"] }}');
            
            function arrangeColumns() {
            
                var series = chart.series.getIndex(0);
            
                var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                if (series.dataItems.length > 1) {
                    var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                    var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                    var delta = ((x1 - x0) / chart.series.length) * w;
                    if (am4core.isNumber(delta)) {
                        var middle = chart.series.length / 2;
            
                        var newIndex = 0;
                        chart.series.each(function(series) {
                            if (!series.isHidden && !series.isHiding) {
                                series.dummyData = newIndex;
                                newIndex++;
                            }
                            else {
                                series.dummyData = chart.series.indexOf(series);
                            }
                        })
                        var visibleCount = newIndex;
                        var newMiddle = visibleCount / 2;
            
                        chart.series.each(function(series) {
                            var trueIndex = chart.series.indexOf(series);
                            var newIndex = series.dummyData;
            
                            var dx = (newIndex - trueIndex + middle - newMiddle) * delta
            
                            series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                            series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        })
                    }
                }
            }
            
            }); // end am4core.ready()
        </script>
    
    
    </body>
</html>