<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EnCotizacPDO extends Model
{
    public static function saveCotizac($ejecutivo, $rutcli, $cencos, $forPlaPagCliente, $observacion, $contacto, $productos, $codemp)
    {
        DB::connection('oracle_dmventas')->beginTransaction();
        $numcot = self::getNextNumcot();
        if ($numcot == 0) {
            DB::connection('oracle_dmventas')->rollback();
            return $numcot;
        }
        if (!self::saveDeCotizac($numcot, $productos, $codemp)) {
            DB::connection('oracle_dmventas')->rollback();
            return 0;
        }

        if (!self::saveEnCotizac($ejecutivo, $numcot, $rutcli, $cencos, $forPlaPagCliente, $observacion, $contacto, $codemp)) {
            DB::connection('oracle_dmventas')->rollback();
            return 0;
        }
        DB::connection('oracle_dmventas')->commit();
        return $numcot;
    }

    private static function saveEnCotizac($ejecutivo, $numcot, $rutcli, $cencos, $forPlaPagCliente, $observacion, $contacto, $codemp)
    {
        try {
            $observ = "Contacto: " . $contacto . " " . $observacion;
            $totales = self::getTotalesCotizacion($numcot);
            $sql = "insert into en_cotizac(numcot, rutcli, cencos, fecemi, fecven, codven, codcnl, subtot, totnet, totiva, totgen, margen, descli, desfin, forpag, plapag, codest, codusu, observ, codmon, cambio, negocio, planti,numver,codemp)"
                . " values (:numcot, :rutcli, :cencos, trunc(sysdate), trunc(sysdate+5), :codven, :codcnl, :subtot, :totnet, "
                . ":totiva, :totgen, :margen, :descli, :desfin, :forpag, :plapag, :codest, :codusu, :observ, :codmon, "
                . ":cambio, :negocio, :planti, :numver, :codemp)";
            DB::connection('oracle_dmventas')->statement($sql, [
                'numcot' => $numcot,
                'rutcli' => $rutcli,
                'cencos' => $cencos,
                'codven' => $ejecutivo['rut'],
                'codcnl' => $ejecutivo['grupo_venta'],
                'subtot' => $totales['totnet'],
                'totnet' => $totales['totnet'],
                'totiva' => $totales['totiva'],
                'totgen' => $totales['totgen'],
                'margen' => $totales['margen'],
                'descli' => 0,
                'desfin' => 0,
                'forpag' => $forPlaPagCliente['forpag'],
                'plapag' => $forPlaPagCliente['plapag'],
                'codest' => 0,
                'codusu' => $ejecutivo['rut'],
                'observ' => $observ,
                'codmon' => 0,
                'cambio' => 1,
                'negocio' => 1,
                'planti' => 'N',
                'numver' => 'APICOT',
                'codemp' => $codemp
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private static function saveDeCotizac($numcot, $productos, $codemp)
    {
        try {
            $i = 1;
            foreach ($productos as $producto) {
                $sql = "insert into de_cotizac (numcot, sequen, codpro, cantid, precio, costos, totnet, pordes, mondes, margen, codemp) "
                    . " values(:numcot, :sequen, :codpro, :cantid, :precio, :costos, :totnet, :pordes, :mondes, :margen, :codemp)";
                DB::connection('oracle_dmventas')->statement($sql, [
                    'numcot' => $numcot,
                    'sequen' => $i,
                    'codpro' => $producto['codpro'],
                    'cantid' => $producto['cantidad'],
                    'precio' => $producto['precio'],
                    'costos' => $producto['costo'],
                    'totnet' => $producto['precio'] * $producto['cantidad'],
                    'pordes' => 0,
                    'mondes' => 0,
                    'margen' => round((($producto['precio'] - $producto['costo']) / $producto['precio']) * 100, 2),
                    'codemp' => $codemp
                ]);
                $i++;
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private static function getTotalesCotizacion($numcot)
    {
        $sql = "select sum(precio * cantid) as subtot, sum(costos * cantid) as costo from DE_COTIZAC WHERE NUMCOT = " . $numcot;
        $resultado = DB::connection('oracle_dmventas')->select($sql);

        $totnet = ($resultado != null && $resultado[0]->subtot > 0) ? $resultado[0]->subtot : 0;
        $costos = ($resultado != null && $resultado[0]->costo > 0) ? $resultado[0]->costo : 0;
        $totiva = round($totnet * 0.19);
        $totgen = $totnet + $totiva;
        $margen = round((($totnet - $costos) / $totnet) * 100, 2);
        $arrayReturn = array(
            "totnet" => $totnet,
            "costos" => $costos,
            "totiva" => $totiva,
            "totgen" => $totgen,
            "margen" => $margen
        );
        return $arrayReturn;
    }

    private static function getNextNumcot()
    {
        $sql = "select cotiza_seq.nextval as numcot from dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql);
        return ($resultado != null && $resultado[0]->numcot > 0) ? $resultado[0]->numcot : 0;
    }

    public static function getListadoCotizaciones($rutcli, $ejecutivo){
        $sql = "select numcot, fecemi, fecven, cencos, subtot, totiva, totgen, margen
                from en_cotizac
                where rutcli =:rutcli
                AND CODUSU =:ejecutivo
                and fecemi between 
                add_months(to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy'),-3) 
                and sysdate";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'ejecutivo' => $ejecutivo
        ]);

        return $resultado;
    }

    public static function getListadoCotizaciones2($rutcli, $ejecutivo){
        $sql = "select numcot, fecemi, fecven, cencos, subtot, totiva, totgen, margen
                from en_cotizac
                where rutcli =:rutcli
                AND CODUSU =:ejecutivo
                and fecemi between 
                add_months(to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy'),-3) 
                and sysdate";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rutcli' => $rutcli,
            'ejecutivo' => $ejecutivo
        ]);

        return $resultado;
    }
	
	public static function getAutoCoti($rutcli){
		$sql = "select codpro
				from portal_ejecutivo.portal_ej_para_detalle_cotis_automaticas
				where rutcli = :rutcli";
		$resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
            'rutcli' => $rutcli
        ]);
		$prods = array();
		foreach($resultado as $prod){
			array_push($prods, array("codpro" => $prod->codpro,
						  "cantidad" => 1));
		}
        return $prods;
	}

	public static function insertCotiLink($numcot, $mailDestino){
		//obtenemos rut de cliente y hash para la coti
		$sql = "SELECT a.rutcli, TO_CHAR(COTIZA_MAIL_SEQ.nextval) || md5(:numcot || '_d1m3rc' || sysdate ) hash
				FROM EN_COTIZAC a
				where numcot = :numcot";
		$resultado = DB::connection('oracle_dmventas')->select($sql, [
            'numcot' => $numcot
        ]);
		$md5 = "";
		if(count($resultado)>0){
			$md5 = $resultado[0]->hash;
		}
		else{
			return $md5;
		}
		//introducimos hash md5 a la tabla de la web
		if($md5 != ""){
			$sql = "INSERT INTO dm_transfer_web.RE_LINK_COTIZAC (HASH, RUTCLI, NUMCOT)
					VALUES (:hash, :rutcli, :numcot)";
                DB::connection('oracle_dmtransferweb')->statement($sql, [
                    'hash' => $md5,
                    'rutcli' => $resultado[0]->rutcli,
					'numcot' => $numcot
                ]);
		}
		//introducimos data al log de cotizaciones
		$sql = "INSERT INTO LG_COTIZACIONES(NUMCOT, RUTCLI, FECHA_ENVIO, FECHA_CREAC, USR_CREAC, EMAIL, HAST)
				VALUES(:numcot, :rutcli, sysdate, sysdate, 'DM_APPBI', :mail, :hash)";
		DB::connection('oracle_dmventas')->statement($sql, [
                    'hash' => $md5,
                    'rutcli' => $resultado[0]->rutcli,
					'numcot' => $numcot,
					'mail' => $mailDestino
                ]);
		return $md5;
	}
}