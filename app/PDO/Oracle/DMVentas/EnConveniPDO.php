<?php

namespace App\PDO\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EnConveniPDO extends Model
{
    public static function getPrecioConvenio($rutcli, $codpro, $codemp)
    {
        $sql = "select b.precio from en_conveni a, de_conveni b 
        where trunc(sysdate) between a.fecemi and a.fecven
        and a.numcot = b.numcot
        and a.codemp = :codemp
        and a.rutcli = :rutcli
        and b.codpro = :codpro";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codemp' => $codemp,
            'rutcli' => $rutcli,
            'codpro' => $codpro
        ]);

        $precio = (count($resultado) > 0 && $resultado[0]->precio <> '') ? $resultado[0]->precio : 0;

        if ($precio == 0 && self::esGobierno($rutcli, $codemp)) {
            $sql = "select b.precio, nvl(b.moneda,'P') as tipo from en_conveni a, de_conveni b 
            where trunc(sysdate) between a.fecemi and a.fecven
            and a.numcot = b.numcot
            and a.codemp = :codemp
            and a.rutcli = 78912345
            and b.codpro = :codpro";
            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'codemp' => $codemp,
                'codpro' => $codpro
            ]);
            if (count($resultado) > 0) {
                $precio = $resultado[0]->precio;
                $moneda = $resultado[0]->tipo;
                if ($moneda <> 'P') {
                    $valor_dolar = self::getDolarDia(1);
                    $precio = round($precio * $valor_dolar);
                }
            }
        }
        return $precio;
    }

    private static function esGobierno($rutcli, $codemp)
    {
        $sql = "select sap_get_negocio_eerr(:codemp,:rutcli) negocio from dual";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codemp' => $codemp,
            'rutcli' => $rutcli
        ]);
        $negocio = (count($resultado) > 0 && $resultado[0]->negocio <> '') ? $resultado[0]->negocio : false;
        if ($negocio == 'GOBIERNO') {
            return true;
        }
        return false;
    }

    private static function getDolarDia($codmon)
    {
        $sql = "select valpar from ma_paridad where codmon = :codmon and trunc(fecpar) = 
        (select max(trunc(fecpar)) from ma_paridad where codmon = :codmon)";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'codmon' => $codmon
        ]);
        return $resultado[0]->valpar;
    }
}
