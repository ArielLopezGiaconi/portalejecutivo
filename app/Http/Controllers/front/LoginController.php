<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use App\PDO\Oracle\DMVentas\ReClavsisPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
    public function iniciarSesion(Request $formulario)
    {
        $userid = $formulario->userid;
        if (isset($userid)) {
            $password = strtoupper($formulario->password);
            if (MaUsuarioPDO::existsUserByUserid(strtoupper($userid))) {
                $usuario = MaUsuarioPDO::getUserByUseridOnly(strtoupper($userid));
                $rutusu = $usuario->get('rutusu');
                //Verifica si usuario tiene clave para ingresar a portalejecutivo
                if (ReClavsisPDO::existsUserByRutusu($rutusu)) {
                    //Busca clave usuario y verifica si coincide con la ingresada por usuario
                    $claves = ReClavsisPDO::getPasswordByRutusu($rutusu);
                    $pass_user = $claves->get('clave1');
                    if ($password == $pass_user) { //aca recien se obtiene la apikey, el usuario deberia corresponder
						$apikey = MaUsuarioPDO::getApiKey(strtoupper($userid), $password);
						if($apikey !== false){
							Session::put('auth', true);
                        	Session::put('usuario', $usuario);
                        	Session::put('nombre_ejecutivo', ucwords(strtolower($usuario->get('nombre').' '.$usuario->get('apepat'))));
                        	Session::put('searchclient', '');
                        	Session::flash('bienvenida', true);
							Session::put('apikey', $apikey);
                        	return redirect('portada');
						}
						else{
							Session::flash('error', 'Problema con APIKEY. Intente nuevamente.');
						}                        
                    } else {
                        Session::flash('error', 'Contraseña ingresada es incorrecta. Intente nuevamente.');
                    }
                } else {
                    Session::flash('error', 'Usuario no posee clave en portalejecutivo.');
                }
            } else {
                Session::flash('error', 'No se encontró usuario en nuestra base de datos. Intente con otro usuario.');
            }
        }
        return redirect()->to('login');
    }

    public function cerrarSesion()
    {
        Session::put('auth', false);
        Session::put('usuario', "");
        Session::put('searchclient', '');
        Session::flash('success', 'Sesión cerrada.');
        return redirect()->to('login');
    }

}