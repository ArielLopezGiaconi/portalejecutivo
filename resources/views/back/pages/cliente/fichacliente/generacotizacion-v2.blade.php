@extends('back.master.masterpage')
@section('contenido')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <div class="pcoded-inner-content">
        <div class="card">
            <div class="card-header">
                <div class="col-xl-12 col-sm-6 m-b-30">
                    <div class="sub-title m-t-10-neg">
                        <i class="ti-pencil-alt f-18 m-r-5"></i>
                        generar cotización
                        <a id="tt_numcot" class="text-success f-weight m-l-20"></a>
                        <div id="tx_guardado" class="label-main d-none">
                            <label class="label label-lg label-success">Guardada</label>
                        </div>
                        <div class="row">
                            <div class="col-xl-4">
                                <input class="form-control form-control-sm" id="filtroCodpro" type="text" placeholder="Buscar por código" hidden="true">
                                <input class="form-control form-control-sm" id="filtroDespro" type="text" placeholder="Buscar por descripción" hidden="true">
                                <input class="form-control form-control-sm" id="filtroMarca" type="text" placeholder="Buscar por marca" hidden="true">
                            </div>
                        </div>
{{--                        <span class="mytooltip tooltip-effect-1">--}}
{{--                            <span class="tooltip-item"><i class="fa fa-camera"></i></span>--}}
{{--                            <span class="tooltip-content clearfix" style="width: 60px;!important;">--}}
{{--                                <img src="https://www.dimerc.cl/sisdim/imagen.php?filename=Z108224&med=500" alt="Ecluid.png">--}}
{{--                            </span>--}}
{{--                        </span>--}}
                        <a id="tt_numcot2" class="text-custom m-l-20"></a>
                        <div id="dv_buscar_numcot" class="float-right m-r-350 m-t-5-neg">
                            <span class="m-r-10 f-left">Generar desde</span>
                            <input id="find_numcot" type="text" class="form-control form-control-sm width-200-p" placeholder="# Número cotización">
                        </div>
{{--                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-precio-sugerido2">Click here! <i class="icofont icofont-play-alt-2"></i></button>--}}
                    </div>
                </div>
                <div class="card-header-right">
                    <div class="btn-group m-t-25-neg" role="group" data-toggle="tooltip" data-placement="top" title="">
{{--                        <button id="btn_guarda_coti" type="button" class="btn btn-success btn-mini waves-effect waves-light" data-toggle="modal" data-target="#default-Modal"><i class="icofont icofont-tick-mark text-white"></i>Guardar cotización</button>--}}
                        <button id="btn_guarda_coti" type="button" class="btn btn-success btn-mini waves-effect waves-light" onclick="invocaTransaccionCotizacion();"><i class="icofont icofont-tick-mark text-white"></i>Guardar cotización</button>
                        <button id="btn_guarda_nvt" type="button" class="btn btn-warning btn-mini waves-effect waves-light" onclick="muestraCalendario();"><i class="icofont icofont-tick-mark text-white"></i>Guardar Nota</button>
                        <div class="btn-group dropdown-split-inverse">
                            <button type="button" class="btn btn-mini"><i class="icofont icofont-repair"></i>Opciones</button>
                            <button type="button" class="btn btn-sm l-height dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only"></span>
                            </button>
                            <div class="dropdown-menu">
                                <a id="btn_listcoti" class="dropdown-item waves-effect waves-light" onclick="redirecturl(id);"><i class="icofont icofont-eye-alt f-18 middle"></i> Ver cotizaciones</a>
                                <a class="dropdown-item waves-effect waves-light" onclick="descartarCotizacion();"><i class="icofont icofont-ui-delete m-r-10"></i> Descartar cotización actual</a>
                                <a class="dropdown-item waves-effect waves-light" onclick="exportTemplateExcel();"><i class="ti-file f-16 middle m-r-10"></i> Descargar plantilla / importar cotización</a>
                                <a class="dropdown-item waves-effect waves-light" data-toggle="modal" data-target="#modal-import-coti"><i class="ti-import f-16 middle m-r-10"></i> Importar cotización desde archivo</a>
                                <a class="dropdown-item waves-effect waves-light" onclick="exportarCotizacionPdf();"><i class="ti-export f-16 middle m-r-10"></i> Exportar cotización a PDF</a>
                                <a class="dropdown-item waves-effect waves-light" onclick="exportarCotizacionExcel();"><i class="ti-export f-16 middle m-r-10"></i> Exportar cotización a Excel</a>
                                <a id="btn_enviar_correo" class="dropdown-item waves-effect waves-light" onclick="invocaSendMailCotizacion();"><i class="ti-email f-16 middle m-r-10"></i> Enviar cotización por correo</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item waves-effect waves-" onclick="nuevaCotizacion();"><i class="icofont icofont-ui-add"></i> Nueva cotización</a>
                            </div>
                        </div>
                    </div>
                    <button id="btn_actualizar" type="button" onclick="refreshPage();" class="btn btn-success btn-icon waves-effect waves-light m-t-25-neg" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refrescar página">
                        <i class="fa fa-refresh text-white"></i>
                    </button>
                    <button id="btn_volver" type="button" onclick="" class="btn btn-inverse btn-icon waves-effect waves-light m-t-25-neg" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Volver a ">
                        <i class="ti-back-right text-white"></i>
                    </button>
                </div>
            </div>
            <div class="modal fade" id="modal-cliente" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Información cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Razón social</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_razons" type="text" class="form-control form-control-sm" placeholder="Razón social" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Rut cliente</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_rut" type="text" class="form-control form-control-sm" placeholder="Rut cliente" maxlength="8" onKeyPress="return soloNumeros(event);">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Negocio</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_negocio" type="text" class="form-control form-control-sm" placeholder="Negocio" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Segmento</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_segmento" type="text" class="form-control form-control-sm" placeholder="Segmento" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Forma pago</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_formapago" type="text" class="form-control form-control-sm" placeholder="Forma pago" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Plazo pago</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_plazopago" type="text" class="form-control form-control-sm" placeholder="Plazo pago" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Estado cliente</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_estado" type="text" class="form-control form-control-sm" placeholder="Estado cliente" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Centro costo</div>
                                        <div class="col-lg-8">
                                            <select name="select_ceco" class="form-control form-control-sm" onchange="seleccionaCeCo();">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Descripción CeCo</div>
                                        <div class="col-lg-8">
                                            <input id="cli_input_desceco" type="text" class="form-control form-control-sm" placeholder="Descripción CeCo" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">Documentado con</div>
                                        <div class="col-lg-8">
                                            <select name="select_docfut" class="form-control form-control-sm">
												<option value="F">FACTURA</option>
												<option value="G">GUIA</option>
                                 			</select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-default">
                            <div class="col-lg-12">
								<div class="row">
                                	<div class="col-lg-4 text-dark f-weight">Tipo nota</div>
                                	<div class="col-lg-8">
                                 		<select name="select_tiponota" class="form-control form-control-sm">
											<option value="30">NORMAL</option>
											<option value="19">DESPACHO CAJAS CD</option>
											<option value="23">FALTANTES</option>
											<option value="6" >EXCEPCION</option>
                                 		</select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 text-dark f-weight">Dirección de despacho</div>
                                    <div class="col-lg-8">
                                        <select name="select_direc" class="form-control form-control-sm" onchange="updateDireccionDespachoPopover();">
{{--                                            <option>Alberto Pepper 1784, Santiago, RM</option>--}}
                                        </select>
                                    </div>
                                </div>								
								<div class="row">
                                    <div class="col-lg-4 text-dark f-weight">Orden de compra</div>
                                    <div class="col-lg-8">
                                        <input id="cli_input_ordencompra" type="text" class="form-control form-control-sm" placeholder="Orden de Compra">
                                   	</div>
                                </div>
								<div class="row">
                                    <div class="col-lg-4 text-dark f-weight">Observacion</div>
                                    <div class="col-lg-8">
                                        <input id="cli_input_observacion" type="text" class="form-control form-control-sm" placeholder="Observacion">
                                   	</div>
                                </div>
								<div class="row">
                                    <div class="col-lg-4 text-dark f-weight">Entregar a</div>
                                    <div class="col-lg-8">
                                        <input id="cli_input_at" type="text" class="form-control form-control-sm" placeholder="Ingrese AT">
                                   	</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 text-dark f-weight">Subir orden de compra</div>
                                    <div class="col-lg-8">
                                        <form id="f-import-ordencompra" name="f-import-ordencompra" method="post" action="" class="f-import-ordencompra" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <input type="file" id="file-import-ordencompra" name="file" class="form-control form-control-sm" accept="application/pdf" required>
                                                            <input type="hidden" id="token_orden">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 m-t-25">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <button type="submit" class="btn btn-grd-primary btn-round btn-block" >Subir orden</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-import-coti" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Importar cotización desde Excel</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnCloseModalImportCoti">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{--                            <h5>Static Modal</h5>--}}
                            <form id="f-import-cotizacion" name="f-import-cotizacion" method="post" action="" class="formarchivo" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12 m-b-25">El archivo debe tener la estructura definida. Para descargar la plantilla haga clic <a class="label label-success" onclick="exportTemplateExcel();">aquí</a></div>

                                            <div class="col-lg-4">Seleccionar archivo</div>
                                            <div class="col-lg-8">
                                                <input type="file" id="file-import-excel" name="file" class="form-control form-control-sm" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 m-t-25">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-grd-primary btn-round btn-block" >Importar cotización</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-send-email" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Enviar cotización por email</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-send-email">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-block">
                                <div class="form-group row">
                                    <p>Ingrese correo al que desea enviar la cotización seleccionada:</p>
                                    <label class="col-sm-5 col-form-label">Ingrese destinatario:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control form-control-sm form-bg-warning" style="width: 100%;" id="tx_correo_destinatario" placeholder="Correo electrónico" autocomplete="off" required>
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 col-form-label">Comentario opcional:</label>
                                    <div class="col-sm-7">
                                        <textarea rows="5" cols="5" class="form-control" id="tx_comentario_ejecutivo" placeholder="Comentario opcional" autocomplete="off"></textarea>
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5"></label>
                                    <div class="col-sm-7">
                                        <input type="hidden" value="" id="h_numcot">
                                        <button type="button" class="btn btn-primary btn-mini m-b-0" onclick="generarArchivoAdjunto();">Enviar cotización</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div id="modal-calendario" class="modal fade" role="dialog">
  				<div class="modal-dialog" role="document">
    				<div class="modal-content">
      					{{--<div class="modal-header">--}}
        					{{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
      					{{--</div>--}}
      					<div class="modal-body">
        					<p>Seleccione fecha de preparacion disponible <input class="form-control" type="text" id="fechaCajas"></p>
      					</div>
      					<div class="modal-footer">
        					<button type="button" class="btn btn-success" id="cerrarCalendario">Continuar</button>
      					</div>
					</div>
  				</div>
			</div>
            <div id="modal-promesa" class="modal fade" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Promesa entrega</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-send-email">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Seleccione fecha disponible en el cuadro inferior<input class="form-control" type="text" id="fechaPromesa"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="cerrarPromesa">Continuar</button>
                        </div>
                    </div>
                </div>
            </div>
			<div id="modal-faltante" class="modal fade" role="dialog">
  				<div class="modal-dialog" role="document">
    				<div class="modal-content">
      					<div class="modal-body">
							<p>Seleccione tipo de documento
							<select name="select_tipodocto" class="form-control form-control-sm">
								<option value="F">FACTURA</option>
								<option value="G">GUIA</option>
								<option value="B">BOLETA</option>
                            </select></p>
        					<input class="form-control" type="number" placeholder="Folio Documento" id="folio_faltante">
							<p>RUT:</p><p id="infoRutcli"></p>
							<p>CENCOS:</p><p id="infoCencos"></p>
							<p>DIRECCION DESPACHO:<p id="infoDirdes"></p></p>
      					</div>
      					<div class="modal-footer">
        					<button type="button" class="btn btn-success" id="cerrarFaltante">Continuar</button>
      					</div>
					</div>
  				</div>
			</div>
            <div id="dv_detallecotizacion" class="row m-t-55-neg">
                <div class="col-sm-12">
                    <div class="">
                        <div class="card-header m-t-10-neg">

                            <div class="row">
                                <div class="col-lg-2">
                                    <h5 id="titulo_det_coti">Detalle cotización</h5>
{{--                                    <span id="tx_num_coti_guardada" class="d-inline m-r-5-neg"></span>--}}
                                </div>
                                <div class="col-lg-5">
                                    <div class="btn-group" role="group" title="" style="width: 100%;">
                                        <button id="btn_editar_cliente" type="button" class="btn btn-danger btn-sm btn-icon" style="width:20px;height: 20px;padding:4px;line-height: 10px;" data-toggle="modal" data-target="#modal-cliente" title="Editar cliente"><i class="fa fa-gear"></i></button>
                                        <button id="btn_infoclient" type="button" class="btn btn-default btn-mini waves-effect waves-light m-t-5-neg" style="width:90%;" data-toggle="popover" data-trigger="focus" data-html="true" title data-placement="bottom" data-original-title="">
                                            <i class="ti-briefcase f-18 middle f-left"></i><span id="tx_nameclient" class="d-inline text-inverse">nombre cliente</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <button id="btn_subtotal" type="button" class="btn btn-default btn-mini btn-block waves-effect waves-light m-t-5-neg" data-toggle="popover" data-trigger="focus" data-html="true" title data-placement="bottom" data-original-title="">
                                        <i class="icon-calculator f-18 middle f-left"></i><span id="tx_subtotal" class="d-inline text-inverse">Subtotal: $0 | Margen: 0,0% | Costo transp: $0</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-block m-t-10-neg">
                            @include('back.master.others.pre-loader')
                            <div id="tbl_cotizacion"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dv_tabs" class="col-xl-12">
                <ul class="nav nav-tabs md-tabs m-t-5-neg" role="tablist" style="height: 60px;!important;">
                    <li class="nav-item">
                        <a id="btn-recom" class="nav-link active" data-toggle="tab" href="#tb_recomendados" role="tab"><i class="ion-star f-20"></i> Otros clientes han llevado</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a id="btn-alter" class="nav-link" data-toggle="tab" href="#tb_alternativos" role="tab"><i class="ion-arrow-swap f-20"></i> Alternativos</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a id="btn-ciclo" class="nav-link" data-toggle="tab" href="#tb_ciclos" role="tab"><i class="fa fa-refresh f-20"></i> Ofertas</a>
                        <div class="slide"></div>
                    </li>
                </ul>
                <div class="tab-content card-block rubberBand animated">
                    <div class="tab-pane active" id="tb_recomendados" role="tabpanel">
                        <div id="dv_recomendados" class="row m-t-20-neg m-b-30-neg">
                            <div class="col-sm-12">
                                <div class="">
{{--                                    <div class="card-header m-t-10-neg">--}}
{{--                                        <h5>Productos recomendados</h5>--}}
{{--                                        <button id="btn_autoplay" class="btn btn-sm hor-grd btn-grd-warning btn-recom" style="left: -75px;border-right: 0;border-radius: 50% 0 0 50%;"><i id="icon_autoplay" class="fa fa-pause"></i></button>--}}
{{--                                    </div>--}}

                                    <div class="card-block">
                                        <button id="btn_autoplay" class="btn btn-sm hor-grd btn-grd-danger m-r-5" style="margin-left:-40px;float: left;border-right: 0;border-radius: 0 50% 50% 0;" title=""><i id="icon_autoplay" class="fa fa-pause"></i></button>
                                        <div id="carrusel_recomendador" class="autoplay">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tb_alternativos" role="tabpanel">
                        <div id="dv_alternativos" class="row m-t-20-neg m-b-30-neg">
                            <div class="col-sm-12">
                                <div class="">
                                    <div class="card-block">
                                        <div id="carrusel_alternativos" class="autoplay_alter">
                                        </div>

{{--                                        <div class="swiper-container">--}}
{{--                                            <div id="carrusel_alternativos" class="swiper-wrapper">--}}

{{--                                            </div>--}}
{{--                                        </div>--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tb_ciclos" role="tabpanel">
                        <div id="dv_ciclos" class="row m-t-20-neg m-b-30-neg">
                            <div class="col-sm-12">
                                <div class="">
                                    <div class="card-block">
{{--                                        <button id="btn_autoplay_c" class="btn btn-sm hor-grd btn-grd-danger m-r-5" style="margin-left:-40px;float: left;border-right: 0;border-radius: 0 50% 50% 0;" title=""><i id="icon_autoplay_c" class="fa fa-pause"></i></button>--}}
                                        <div id="carrusel_ciclos" class="autoplay_ciclo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

{{--            <div id="dv_recomendados" class="row m-t-40-neg m-b-30-neg">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header m-t-10-neg">--}}
{{--                            <h5>Productos recomendados</h5>--}}
{{--                            <button id="btn_autoplay" class="btn btn-sm hor-grd btn-grd-warning"><i id="icon_autoplay" class="fa fa-pause"></i> Pause recomendador</button>--}}
{{--                        </div>--}}
{{--                        <div class="card-block rubberBand animated">--}}
{{--                            <div id="carrusel_recomendador" class="autoplay">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <button id="btn_buscar_descripcion" type="button" class="btn btn-primary waves-effect d-none" data-toggle="modal" data-target="#large-Modal">Large</button>
            <div class="modal fade" id="large-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;margin-left:-20px;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content" style="height: 500px;width:1300px;">
                        <div class="modal-header">
                            <div class="col-lg-3">
                                <h5 class="modal-title f-16">Buscar producto por descripción</h5>
                            </div>

                            <div class="col-lg-5">
                                <div class="input-group input-group-button">
                                    <input id="tx_prod_desc" type="text" class="form-control form-control-sm" placeholder="Descripción" value="" autocomplete="off">
                                    <span class="input-group-addon btn btn-sm btn-primary" id="basic-addon10" onclick="findProductInput();">
                                        <span class=""><i class="fa fa-search"></i></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-radio m-l-60">
                                <form>
                                    <div class="radio radiofill radio-inline">
                                        <label>
                                            <input type="radio" name="rb_criterio_busqueda" checked="checked" value="0" onchange="findProductInput();">
                                            <i class="helper"></i>Que comience con
                                        </label>
                                    </div>
                                    <div class="radio radiofill radio-inline">
                                        <label>
                                            <input type="radio" name="rb_criterio_busqueda" value="1" onchange="findProductInput();">
                                            <i class="helper"></i>Que contenga
                                        </label>
                                    </div>
                                </form>
                            </div>
{{--                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="limpiarTablaTemporalBuscDesc();">--}}
{{--                                <span aria-hidden="true">×</span>--}}
{{--                            </button>--}}
                        </div>
                        <div class="modal-body">
                            <div id="tbl_buscar_descripcion" class="f-12"></div>
                            <table id="tbl_temporal" class="d-none">
                            </table>
                        </div>
                        <div class="modal-footer">
                            <span class="f-left m-l-20" style="position: absolute;left:0;">Registros encontrados: <b id="cant_registros"></b></span>
                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal" id="btn_cerrar_modal" onclick="limpiarTablaTemporalBuscDesc();">Cerrar</button>
                            <button type="button" class="btn btn-primary waves-effect waves-light" id="btn_agregar_coti"><span class="">Agregar a cotización</span></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Guardar cotización</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                <p>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">@</span>
                                        <input id="tx_contacto" type="text" class="form-control" placeholder="Contacto envío" autocomplete="off" required>
                                    </div>
                                    <textarea id="tx_observaciones" class="form-control max-textarea" maxlength="255" rows="4" placeholder="Observaciones" required></textarea>
                                </p>

                        </div>
                        <div class="modal-footer">
                            <button id="btn_cancelar" type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cancelar</button>
                            <button id="btn_confirm_guarda_coti" type="button" class="btn btn-primary waves-effect waves-light " onclick="invocaTransaccionCotizacion();">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
            <button id="btn_precios" type="button" class="btn btn-primary waves-effect d-none" data-toggle="modal" data-target="#modal_precios">Precios</button>
            <div class="modal fade" id="modal_precios" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document" style="max-width: 650px;!important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Últimos precios</h6>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body" id="m_body_precios">

                        </div>
{{--                        <div class="modal-footer">--}}
{{--                            <button id="btn_cancelar" type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cancelar</button>--}}
{{--                            <button id="btn_confirm_guarda_coti" type="button" class="btn btn-primary waves-effect waves-light " onclick="invocaGuardarCotizacion();">Guardar</button>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
			<div class="modal fade" id="modal_datosprod" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document" style="max-width: 650px;!important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Datos Producto</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body" id="m_body_datosprod">
							<div class="row">
								<div class = "col-xl-8">
									<p class="d-inline">
										<b>Cod: </b>
										<code id="modal_datosprod_codpro"></code>
									</p>
									<p>
										<b>Des: </b>
										<code id="modal_datosprod_despro"></code>
									</p>
								</div>
								<div class = "col-xl-4">
									<img id="modal_datosprod_img" width="120px">
								</div>
							</div>
							<div class="row">
								<div class = "col-xl-12">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Embalaje</th>
												<th>Subembalaje</th>
												<th>Peso</th>
												<th>Volumen</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td id="modal_datosprod_embalaje"></td>
												<td id="modal_datosprod_subembalaje"></td>
												<td id="modal_datosprod_peso"></td>
												<td id="modal_datosprod_volumen"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="modal_notaventa" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document" style="max-width: 650px;!important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Datos Nota</h6>
							<button class="btn btn-default" id="btnVolverAFicha">Volver a Ficha Cliente</button>
							<button class="btn btn-default" onclick="getDocumentos(numnvtWindow)">Ver Documentos</button>
                        </div>
						<div class="modal-header">
                        	<div class="container" style="height: 20vh; overflow-y: auto;">
								<div class = "col-xl-12">
									<p class="mb-0">
										<b>RUT: </b>
										<code id="modal_notaventa_rutcli"></code>
									</p>
									<p class="mb-0">
										<b>Razón social: </b>
										<code id="modal_notaventa_razons"></code>
									</p>
									<p class="mb-0">
										<b>Centro de Costo: </b>
										<code id="modal_notaventa_cencos"></code>
									</p>
									<p class="mb-0">
										<b>Total neto: </b>
										<code id="modal_notaventa_totnet"></code>
									</p>
									<p class="mb-0">
										<b>IVA: </b>
										<code id="modal_notaventa_iva"></code>
									</p>
									<p class="mb-0">
										<b>Total General: </b>
										<code id="modal_notaventa_totgen"></code>
									</p>
									<p class="mb-0">
										<b>Documentado Con: </b>
										<code id="modal_notaventa_docfut"></code>
									</p>
									<p class="mb-0">
										<b>Tipo Nota: </b>
										<code id="modal_notaventa_tiponota"></code>
									</p>
									<p class="mb-0">
										<b>Dirección de despacho: </b>
										<code id="modal_notaventa_dirdes"></code>
									</p>
									<p class="mb-0">
										<b>Orden de Compra: </b>
										<code id="modal_notaventa_ordencompra"></code>
									</p>
									<p class="mb-0">
										<b>Observación: </b>
										<code id="modal_notaventa_observ"></code>
									</p>
									<p class="mb-0">
										<b>Entregar A: </b>
										<code id="modal_notaventa_entregara"></code>
									</p>
								</div>
							</div>
                        </div>
                        <div class="modal-body" id="m_body_datosprod" style="padding-top: 0px !important;">
							<div class="row">
								<div class = "col-xl-12 tableFixHead">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Código</th>
												<th>Descripción</th>
												<th>Cantidad</th>
												<th>Precio</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody id="modal_datosprod_productos">											
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
			<div id="modal_documentos" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title">Documentos Relacionados</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
						<div class="modal-body">
							<div id="docRel" class="card-block text-center">
                        	</div>
						</div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="modal_importarcoti" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document" style="max-width: 650px;!important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Importar cotización</h6>
                        </div>
                        <div class="modal-body" id="m_body_importarcoti">
							<div class="row">
								<div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
											<input id="cb_importar_cantidad" type="checkbox" class="form-control form-control-sm">
										</div>
                                        <div class="col-lg-8">
											Importar cantidad
                                        </div>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
											<input id="cb_importar_precios" type="checkbox" class="form-control form-control-sm">
										</div>
                                        <div class="col-lg-8">
											Importar precios
                                        </div>
                                    </div>
                                </div>

							</div>
                        </div>
						<div class="modal-footer">
							<button class="btn-default" onclick="generarDesdeCoti();">Generar Cotización</button>
						</div>
                    </div>
                </div>
            </div>
			<div id="modal_actualiza_mail" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title" id="modal_actualiza_mail_titulo"></h6>
                            
                        </div>
						<div class="modal-body">
							<div class="col-lg-12">
                            	<div class="row">
                                	<div class="col-lg-12">
										<input id="modal_actualiza_mail_mail" type="text" class="form-control form-control-sm" placeholder="Ingrese mail">
									</div>
                            	</div>
                            </div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" onclick="actualizaMail();">Actualizar Mail</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<input type="hidden" id="sucursal-cliente">
    @include('back.others.js.portal')
{{--    @include('back.others.js.clientes')--}}
@endsection
@section('custom-includes')
{{--    <script data-jsfiddle="common" src="{{ asset('assets/js/handsontable.full.js') }}"></script>--}}
    <!--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/swiper/css/swiper.min.css') }}">
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/es-MX.js') }}"></script>
    <link href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" rel="stylesheet">

    <!-- pnotify js -->
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.desktop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.callbacks.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.animate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.history.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.mobile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/pnotify/js/pnotify.nonblock.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/pnotify/notify.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/node_modules/moment/locale/es.js') }}"></script>

{{--    <script src="//cdnjs.cloudflare.com/ajax/libs/numbro/2.1.2/min/languages.min.js"></script>--}}

    {{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- swiper js -->
    <script type="text/javascript" src="{{ asset('bower_components/swiper/js/swiper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/swiper-custom.js') }}"></script>

{{--    <script type="text/javascript" src="{{ asset('bower_components/sweetalert/js/sweetalert.min.js') }}"></script>--}}
{{--    <script type="text/javascript" src="{{ asset('assets/js/modal.js') }}"></script>--}}
{{--    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>--}}
{{--    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>--}}
	@include('back.others.js.cotizacion-v2-1')
	<style>
		.tableFixHead          { overflow-y: auto; height: 40vh; }
		.tableFixHead thead th { position: sticky; top: 0; }
		.tableFixHead > table > thead > tr > th     { background:#eee; }
		@keyframes glow {
  			from {
    			box-shadow: 0 0 10px -10px #0df3a3;
  			}
  			to {
    			box-shadow: 0 0 10px 10px #0df3a3;
  			}
		}
		@keyframes glowpiola {
  			from {
    			box-shadow: 0 0 5px -5px #c3e6cb;
  			}
  			to {
    			box-shadow: 0 0 5px 5px #c3e6cb;
  			}
		}

	</style>
    <script>
        initializeGeneraCotizacion();

        $(document).on('submit', '.formarchivo', function(e){
            e.preventDefault();
            var formu=$(this);
            var nombrearchivo=$(this).attr("id");
            var formData = new FormData($("#"+nombrearchivo+"")[0]);

            invocaImportCotizacion(formData);
        });

        $(document).on('submit', '.f-import-ordencompra', function(e){
            e.preventDefault();
            var formu=$(this);
            var nombrearchivo=$(this).attr("id");
            var formData = new FormData($("#"+nombrearchivo+"")[0]);
            invocaSubirOrdenCompra(formData);
        });

        $(document).ready(function() {
            moment.locale('es');

            document.getElementById("pre_loader").className = "loader animation-start d-none";
            document.getElementById('dv_tabs').className = "d-none";
            //document.getElementById('dv_recomendados').className = "d-none";
            pantallaCompleta('generacoti');
            asignarUrlPreviousPage();

            var nFrom = "top";
            var nAlign = "center";
            var nIcons = "fa fa-exclamation-triangle";
            var nType = "inverse";
            var nAnimIn = "animated fadeInRight";
            var nAnimOut = "animated fadeOutRight";
            var title = "";
            var message = "Debe seleccionar centro de costo y dirección de despacho";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

            $('#btn_editar_cliente').click();

            $('#tx_prod_desc').keypress(function(event){

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    findProductInput();
                    $('#tx_prod_desc').click();
                }
            });

            $('#tx_contacto').keypress(function(event){

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $('#tx_observaciones').focus();
                }
            });

            $('#tx_observaciones').keypress(function(event){

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $('#btn_confirm_guarda_coti').click();
                }
            });

            $('#cli_input_rut').keypress(function(event){
               var keycode = (event.keyCode ? event.keyCode : event.which);
               if(keycode=='13'){

                   var value = $('#cli_input_rut').val();
                   getInfoClienteRut(value);
               }
            });

            $('#find_numcot').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
					var cotiBadilla = document.getElementById('find_numcot').value;
					swal({
        			title: "Importar cotización",
					text: "¿Qué necesita hacer con la cotización "+ cotiBadilla + "?",
            		icon: "info",
            		buttons: true,
            		dangerMode: true,
            		buttons: {
						defeat: {
                			text:"Generar cotización nueva desde esta",
                			value:"generar"
                		},
						deldata: {
                			text:"Editar Cotización",
                			value:"editar"
                 		}
            		},
					closeOnCancel: false
            		})
            		.then((value) => {
						if(value != null){
							if(value == "generar"){ //si elige generar
								from_otra_coti = document.getElementById('find_numcot').value;
                    			$("#modal_importarcoti").modal("show");
							}
							if(value == "editar"){ //si elige editar
								var urlRedirect = '{{url('/editacotizacion/')}}/' + rut_cliente +'/'+ cotiBadilla + '/fichacliente';
								window.location = urlRedirect;
							}
						}
            		});
                }
            });

            $('#btn_autoplay').on('click', function() {
                var texto = document.getElementById('btn_autoplay').innerHTML;
                if(texto.includes('pause')){
                    $('.autoplay').slick('slickPause');
                    document.getElementById('btn_autoplay').innerHTML = '<i id="icon_autoplay" class="fa fa-play"></i>';
                    document.getElementById('btn_autoplay').title = 'Avanzar';
                }
                else{
                    $('.autoplay').slick('slickPlay');
                    document.getElementById('btn_autoplay').innerHTML = '<i id="icon_autoplay" class="fa fa-pause"></i>';
                    document.getElementById('btn_autoplay').title = 'Detener';
                }

            });

            $('#btn-alter').on('click', function(){
                var texto = document.getElementById('btn_autoplay').innerHTML;
                if(texto.includes('pause')){
                    $('#btn_autoplay').click();
                }
            });

            $('#btn-ciclo').on('click', function(){
                var texto = document.getElementById('btn_autoplay').innerHTML;
                if(texto.includes('pause')){
                    $('#btn_autoplay').click();
                }
            });

            $('#btn-recom').on('click', function(){
                var texto = document.getElementById('btn_autoplay').innerHTML;
                if(texto.includes('play')){

                    $('#btn_autoplay').click();
                }

            });

            $('#btn_guarda_coti').click(function(){
                invocaRegistroBitacora('clicguardarcoti');
            });
			$('#btnVolverAFicha').click(function(){
				redirecturl("btn_fichacliente");
			});
        });


        function soloNumeros(e){
            var key = window.Event ? e.which : e.keyCode
            return (key >= 48 && key <= 57)
        }

    </script>
@endsection