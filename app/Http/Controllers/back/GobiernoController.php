<?php


namespace App\Http\Controllers\back;
use App\EPDO\MySQL\PricingServer\PortalEjecutivo\PortalEjUsuariosEPDO;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use App\PDO\Oracle\DMVentas\EnCotizacPDO;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PDF;
use Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Mail;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use View;
use Swift_Attachment;
use Symfony\Component\HttpFoundation\StreamedResponse;

class GobiernoController
{
    public function getMesesConsulta($ano, $mes){
        $meses = array();
        if($mes == 1){
            $nombre_mes1 = 'Octubre';
            $nombre_mes2 = 'Noviembre';
            $nombre_mes3 = 'Diciembre';
            $mes1 = 10;
            $mes2 = 11;
            $mes3 = 12;
            $ano1 = $ano-1;
            $ano2 = $ano-1;
            $ano3 = $ano-1;
        }elseif($mes == 2){
            $nombre_mes1 = 'Noviembre';
            $nombre_mes2 = 'Diciembre';
            $nombre_mes3 = 'Enero';
            $mes1 = 11;
            $mes2 = 12;
            $mes3 = 1;
            $ano1 = $ano;
            $ano2 = $ano-1;
            $ano3 = $ano-1;
        }elseif($mes == 3){
            $nombre_mes1 = 'Diciembre';
            $nombre_mes2 = 'Enero';
            $nombre_mes3 = 'Febrero';
            $mes1 = 12;
            $mes2 = 1;
            $mes3 = 2;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano-1;
        }elseif($mes == 4){
            $nombre_mes1 = 'Enero';
            $nombre_mes2 = 'Febrero';
            $nombre_mes3 = 'Marzo';
            $mes1 = 1;
            $mes2 = 2;
            $mes3 = 3;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 5){
            $nombre_mes1 = 'Febrero';
            $nombre_mes2 = 'Marzo';
            $nombre_mes3 = 'Abril';
            $mes1 = 2;
            $mes2 = 3;
            $mes3 = 4;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 6){
            $nombre_mes1 = 'Marzo';
            $nombre_mes2 = 'Abril';
            $nombre_mes3 = 'Mayo';
            $mes1 = 3;
            $mes2 = 4;
            $mes3 = 5;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 7){
            $nombre_mes1 = 'Abril';
            $nombre_mes2 = 'Mayo';
            $nombre_mes3 = 'Junio';
            $mes1 = 4;
            $mes2 = 5;
            $mes3 = 6;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 8){
            $nombre_mes1 = 'Mayo';
            $nombre_mes2 = 'Junio';
            $nombre_mes3 = 'Julio';
            $mes1 = 5;
            $mes2 = 6;
            $mes3 = 7;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 9){
            $nombre_mes1 = 'Junio';
            $nombre_mes2 = 'Julio';
            $nombre_mes3 = 'Agosto';
            $mes1 = 6;
            $mes2 = 7;
            $mes3 = 8;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 10){
            $nombre_mes1 = 'Julio';
            $nombre_mes2 = 'Agosto';
            $nombre_mes3 = 'Septiembre';
            $mes1 = 7;
            $mes2 = 8;
            $mes3 = 9;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 11){
            $nombre_mes1 = 'Agosto';
            $nombre_mes2 = 'Septiembre';
            $nombre_mes3 = 'Octubre';
            $mes1 = 8;
            $mes2 = 9;
            $mes3 = 10;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }elseif($mes == 12){
            $nombre_mes1 = 'Septiembre';
            $nombre_mes2 = 'Octubre';
            $nombre_mes3 = 'Noviembre';
            $mes1 = 9;
            $mes2 = 10;
            $mes3 = 11;
            $ano1 = $ano;
            $ano2 = $ano;
            $ano3 = $ano;
        }

        $meses[0]["nombre_mes1"] = $nombre_mes1;
        $meses[0]["nombre_mes2"] = $nombre_mes2;
        $meses[0]["nombre_mes3"] = $nombre_mes3;
        $meses[0]["mes1"] = $mes1;
        $meses[0]["mes2"] = $mes2;
        $meses[0]["mes3"] = $mes3;
        $meses[0]["ano1"] = $ano1;
        $meses[0]["ano2"] = $ano2;
        $meses[0]["ano3"] = $ano3;
        
        return $meses;
    }

    public function getDatosReportesCorp(){
        try {

            $user = 'consultaDimerc';
            $pass = '3st4E5lAC0ntr4s3ña.';
            $array = explode("/", $_GET['datos']);

            //$user, $pass, $tipo, $ano, $mes, $rut
            $cabecera = oraclePDO::getCabeceraReportesCorp($array[0], $array[1], $array[3]);
            
            $meses = $this->getMesesConsulta($array[2], $array[3]);
            $reporte1 = $this->getReporteCliente1($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte2vista1 = $this->getReporteCliente2Vista1($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte2 = $this->getReporteCliente2($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte3 = $this->getReporteCliente3($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte4 = $this->getReporteCliente4($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte5 = $this->getReporteCliente5($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte6 = $this->getReporteCliente6($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte7 = $this->getReporteCliente7($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte8 = $this->getReporteCliente8($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte9 = $this->getReporteCliente9($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte10 = $this->getReporteCliente10($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte11 = $this->getReporteCliente11($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte12 = $this->getReporteCliente12($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte13 = $this->getReporteCliente13($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            $reporte14 = $this->getReporteCliente14($user, $pass, $array[0], $array[2], $array[3], $array[1]);
            
            return view('back.pages.FormatoPDF.Cartola', ['meses' => $meses,
                                                            'cabecera' => $cabecera,
                                                            'reporte1' => $reporte1,
                                                            'reporte2vista1' => json_decode($reporte2vista1),
                                                            'reporte2' => $reporte2,
                                                            'reporte3' => json_decode($reporte3),
                                                            'reporte4' => json_decode($reporte4),
                                                            'reporte5' => json_decode($reporte5),
                                                            'reporte6' => json_decode($reporte6),
                                                            'reporte7' => json_decode($reporte7),
                                                            'reporte8' => json_decode($reporte8),
                                                            'reporte9' => json_decode($reporte9),
                                                            'reporte10' => json_decode($reporte10),
                                                            'reporte11' => json_decode($reporte11),
                                                            'reporte12' => json_decode($reporte12),
                                                            'reporte13' => json_decode($reporte13),
                                                            'reporte14' => json_decode($reporte14)]);

        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente1($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 1,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente2($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 2,
                    "vista": 2,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente2Vista1($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 2,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente3($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 3,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente4($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 4,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente5($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 5,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente6($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 6,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente7($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 7,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente8($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 8,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente9($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 9,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente10($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 10,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente11($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 11,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente12($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 12,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente13($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 13,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

    public function getReporteCliente14($user, $pass, $tipo, $ano, $mes, $rut){
        try {
            $url = 'https://apispricing.dimerc.cl/apis_pricing_dimerc/BusinessIntelligence/cartolaCliente';
            $body = '{
                "json": {
                    "reporte": 14,
                    "vista": 1,
                    "tipo": ' . $tipo . ',
                    "ano": ' . $ano . ',
                    "mes": ' . $mes . ',
                    "rut": ' . $rut . ' 
                }
            }';
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_USERPWD => $user . ":" . $pass,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            //return $response;
            $valor = json_decode($response);

            curl_close($curl);
            return json_encode($valor->response);
            
        } catch (\Exception $e) {

            $respuesta = $e->getMessage();

            $FormatoRespuesta = new ResponseFormatt();
            $FormatoRespuesta->setCode(409)
                ->setResponse($respuesta);

            return $respuesta;
        }
    }

}