@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-usd text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Mis comisiones</h3>
                            <span class="f-16">Mis comisiones | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                        {{-- <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul> --}}
                    </div>
                </div>
                <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-10 col-xs-10 form-group">
                                    <label>Periodo</label>
                                    <div class='input-group'>
                                        <select name="Periodo" id="Periodo" class="form-control">
                                        </select>
                                        <button id="Buscar" class="btn btn-danger" onclick="Buscar()" style="height: 40px; margin-left: 5px">Buscar</button>
                                        <button id="exportar" class="btn btn-succes" onclick="exportar()" style="height: 40px; margin-left: 5px">Exportar Farmer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card table-responsive col-xl-12">
                    <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                        <thead>
                            <tr class="table-inverse">
                                <th>Equipo</th>
                                <th>Usuario</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody id="cuadro1.1">
                        </tbody>
                    </table>

                </div>
                <div class="card table-responsive col-xl-5">
                    <div>
                        <h4>Comisión por Venta ABC</h4>
                    </div>
                    <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                        <thead>
                            <tr class="table-inverse">
                                <th>Categoria</th>
                                <th>Venta</th>
                                <th>Comisión ABC</th>
                            </tr>
                        </thead>
                        <tbody id="cuadro2.1">
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="card table-responsive col-xl-12">
                <div>
                    <h4>Bonos por Meta Mes</h4>
                </div>
                <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                    <thead>
                        <tr class="table-inverse">
                            <th>Meta de Venta</th>
                            <th>Venta de Cartera</th>
                            <th>% Cumplimiento</th>
                            <th>Factor</th>
                            <th>Comisión Adicional</th>
                        </tr>
                    </thead>
                    <tbody id="cuadro1.2">
                    </tbody>
                </table>

            </div>
            <div class="card table-responsive col-xl-12">
                <div>
                    <h4>Bono por Venta Cross</h4>
                </div>
                <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                    <thead>
                        <tr class="table-inverse">
                            <th>Venta Cross</th>
                            <th>Factor</th>
                            <th>Bono Cross</th>
                        </tr>
                    </thead>
                    <tbody id="cuadro1.3">
                    </tbody>
                </table>

            </div>
            <div class="card table-responsive col-xl-12">
                <div>
                    <h4>Resumen Total Comisiones Ganadas</h4>
                </div>
                <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                    <thead>
                        <tr class="table-inverse">
                            <th>Comisión ABC</th>
                            <th>Comisión Adicional</th>
                            <th>Bono Cross</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody id="cuadro1.4">
                    </tbody>
                </table>

            </div>
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-usd";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Mis Comisiones";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            getPeriodosComisiones();
            Buscar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function getPeriodosComisiones(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('/pricing/getPeriodosComisiones') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<option value="' + json.response[i].periodo + '">' + json.response[i].periodo + '</option>';
                        }
                        document.getElementById("Periodo").innerHTML = text;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
            });
        }

        function exportar(){
            var fecha = document.getElementById('Periodo').value;
            fecha = fecha.substring(0,2) + fecha.substring(3,5) + fecha.substring(6,10);
            //console.log(fecha);
            window.open("{{url('descarga/comisiones')}}/" + fecha);
            //descargaComisiones();
        }

        function Buscar(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=19;
            var actionid=49;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            BuscaCuadro2();
            BuscaCuadro1();
        }

        function BuscaCuadro1() {
            var text1 = '';
            var text2 = '';
            var text3 = '';
            var text4 = '';
            var date = new Date();
            var token = '{{ csrf_token() }}';
            var lafecha = document.getElementById("Periodo").value;
            if (lafecha === '') {
                lafecha = date.getFullYear() + '-' + 
                    (date.getMonth()+1) + '-01';
            }else{
                lafecha = lafecha.substring(6,10) + '-' + lafecha.substring(3,5) + '-' + lafecha.substring(0,2);
            }
            var parametros = {
                _token: token,
                fecha: lafecha
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/comisionesCuadro1')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var i;
                        var comision = document.getElementById("comision").innerHTML;
                        for (i = 0; i < json.response.length; i++) {
                            text1 += '<tr>' +
                                '<td>' + json.response[i].equipo + '</td>' +
                                '<td>' + json.response[i].usuario + '</td>' + 
                                '<td>' + json.response[i].nombre + '</td>' +
                            '</tr>';
                            var adicional = parseInt(json.response[i].comision_final.substring(1,json.response[i].comision_final.length).replace(',','')) - 
                                            parseInt(comision.substring(1,json.response[i].comision.length).replace('.',''));
                            //console.log(comision.substring(1,json.response[i].comision.length).replace('.',''));
                            var adicional = (json.response[i].comision_final - json.response[i].comision);
                            //console.log(json.response[i].comision_final - json.response[i].comision);
                            text2 += '<tr>' +
                                '<td>' + json.response[i].Meta_venta + '</td>' +
                                '<td>' + json.response[i].Venta_de_su_cartera + '</td>' +
                                '<td>' + json.response[i].cumplimiento_meta + '</td>' +
                                '<td>' + json.response[i].porcentaje_incremento + '</td>' +
                                '<td>' + '$' + number_format(adicional,0) + '</td>' +
                            '</tr>';
                            text3 += '<tr>' +
                                '<td>' + json.response[i].venta_asociada_cross + '</td>' +
                                '<td> 2.4%</td>' +
                                '<td>' + json.response[i].bono_cross + '</td>' +
                            '</tr>';
                            text4 += '<tr>' +
                                '<td>' + comision + '</td>' +
                                '<td>' + '$' + number_format(adicional,0) + '</td>' +
                                '<td> $' + number_format(json.response[i].bono_cross,0) + '</td>' +
                                '<td>' + json.response[i].total_bonos + '</td>' +
                            '</tr>';
                        }
                        document.getElementById("cuadro1.1").innerHTML = text1;
                        document.getElementById("cuadro1.2").innerHTML = text2;
                        document.getElementById("cuadro1.3").innerHTML = text3;
                        document.getElementById("cuadro1.4").innerHTML = text4;
                        
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function BuscaCuadro2() {
            var text = '';
            var date = new Date();
            var token = '{{ csrf_token() }}';
            var lafecha = document.getElementById("Periodo").value;
            var total = 0;
            if (lafecha === '') {
                lafecha = date.getFullYear() + '-' + 
                    (date.getMonth()+1) + '-01';
            }else{
                lafecha = lafecha.substring(6,10) + '-' + lafecha.substring(3,5) + '-' + lafecha.substring(0,2);
            }
            var parametros = {
                _token: token,
                fecha: lafecha
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/comisionesCuadro2')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<tr>' +
                                '<td>' + json.response[i].categoria_comision + '</td>' +
                                '<td>' + json.response[i].venta + '</td>' + 
                                '<td>' + json.response[i].comision + '</td>' +
                            '</tr>';
                            //console.log(parseInt(json.response[i].comision.substring(1,json.response[i].comision.length).replace(',','')));
                            total = total + parseInt(json.response[i].comision.substring(1,json.response[i].comision.length).replace(',',''));
                        }
                        text += '<tr><td></td><td><h4>Total</h4></td><td id="comision">' + '$' + Intl.NumberFormat().format(total) + '</td></tr>';
                        document.getElementById("cuadro2.1").innerHTML = text;
                        

                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function descargaComisiones() {
            var text = '';
            var date = new Date();
            var token = '{{ csrf_token() }}';
            var lafecha = document.getElementById("Periodo").value;
            var total = 0;
            if (lafecha === '') {
                lafecha = date.getFullYear() + '-' + 
                    (date.getMonth()+1) + '-01';
            }else{
                lafecha = lafecha.substring(6,10) + '-' + lafecha.substring(3,5) + '-' + lafecha.substring(0,2);
            }
            var parametros = {
                _token: token,
                fecha: lafecha
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/descarga/comisiones')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection