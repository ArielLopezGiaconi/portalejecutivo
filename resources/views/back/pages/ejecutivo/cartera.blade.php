@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-briefcase text-inverse f-46"></i>
                        </div>
                        <div class="col-sm-4">
                            <h3>Cartera clientes</h3>
                            <span class="f-16">Análisis cartera | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>  
                    </div>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div>
                    <a id="mostrar" style="display:none" data-toggle="modal" data-target="#carteraExterna"></a>
                    <div>
                        <input type="search" class="form-control pull-right col-lg-2" placeholder="ingrese rut" onkeypress="getRut(event)" id="busquedaRut">
                        <label style="margin-right: 10px" class="f-18 pull-right">Buscar cliente</label>    
                    </div>
                </div>
                <div id="mensaje" style="display:none" class="alert-danger background-danger">
                    <h3 class="text center"><i class="ion-close-circled text-white" style="margin-right: 5px; margin-left: 5px"></i><strong>Cliente no encontrado!</strong> Favor verifique he intente nuevamente</h3>
                </div>
                <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-4 col-sm-11 col-xs-12 form-group ">
                                    <div class="card-sub">
                                        <div class="card-block">
                                            <label>Selección de Brecha</label>
                                            {{-- <a class="mytooltip tooltip-effect-9" href="#">
                                                <font style="vertical-align: inherit;">
                                                    <i class="fa fa-info-circle"></i>
                                                </font>
                                                <span class="tooltip-content3">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">La brecha es ... bla.. bla... bla... y mas bla.. bla..</font>
                                                    </font>
                                                </span>
                                            </a> --}}
                                            <div class="col-sm-12">
                                                <div class="col-sm-12"> 
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" checked id="brecha1" name="brecha" value="1">
                                                        <label class="form-check-label" for="brecha1">Brecha Contra Potencial</label>
                                                    </div>
                                                    
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="brecha2" name="brecha" value="2">
                                                        <label class="form-check-label" for="brecha2">Brecha Contra Venta Promedio</label>
                                                    </div>
                                                    
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="brecha3" name="brecha" value="3">
                                                        <label class="form-check-label" for="brecha3">Brecha Contra año Anterior</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-11 col-xs-12 form-group ">
                                    <div class="card-sub">
                                        <div class="card-block">
                                            <label>Selección de Venta de Comparación</label>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12"> 
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" checked id="venta1" name="venta" value="1">
                                                        <label class="form-check-label" for="venta1">Mes Cerrado</label>
                                                    </div>
                                                    
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="venta2" name="venta" value="2">
                                                        <label class="form-check-label" for="venta2">Mismo día Habil</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-11 col-xs-12 form-group ">
                                    <div class="card-sub">
                                        <div class="card-block">
                                            <label>Selección de Cartera</label>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12"> 
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" checked id="cartera1" name="cartera" value="1">
                                                        <label class="form-check-label" for="cartera1">Solo mi Cartera</label>
                                                    </div>
                                                
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera2" name="cartera" value="2">
                                                        <label class="form-check-label" for="cartera2">Cartera Partners</label>
                                                    </div>

                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera3" name="cartera" value="3">
                                                        <label class="form-check-label" for="cartera3">Ambas Carteras</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-clasificacion" type="checkbox"/>
                                    <label>Clasificación</label>
                                    <select name="clasificacion" id="clasificacion" class="form-control" disabled="false" required>
                                        <option value="Cliente">Cliente</option>
                                        <option value="Registro">Registro</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-segmento" type="checkbox"/>
                                    <label>Segmento</label>
                                    <select name="segmento" id="segmento" class="form-control" disabled="false" required>
                                        <option value="B2C">B2C</option>
                                        <option value="GOBIERNO">GOBIERNO</option>
                                        <option value="GRAN EMPRESA">GRAN EMPRESA</option>
                                        <option value="HOLDING">HOLDING</option>
                                        <option value="PYME COMERCIO">PYME COMERCIO</option>
                                        <option value="PYME MEDIANA">PYME MEDIANA</option>
                                        <option value="PYME MICRO">PYME MICRO</option>
                                        <option value="PYME PEQUEÑA">PYME PEQUEÑA</option>
                                        <option value="SIN CLASIFICAR">SIN CLASIFICAR</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-credito" type="checkbox"/>
                                    <label>Status Credito</label>
                                    <select name="credito" id="credito" class="form-control" disabled="false" required>
                                        <option value="Activo">Activo</option>
                                        <option value="Bloqueado">Bloqueado</option>
                                        <option value="Sin analisis">Sin analisis</option>>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-rango" type="checkbox"/>
                                    <label>Rango</label>
                                    <select name="rango" id="rango" class="form-control" disabled="false" required>
                                        <option value="1- Rango sobre el 100%">1- Rango sobre el 100%</option>
                                        <option value="2- Rango sobre el 70%">2- Rango sobre el 70%</option>
                                        <option value="3- Rango sobre el 50%">3- Rango sobre el 50%</option>
                                        <option value="5- Rango bajo el 30%">5- Rango bajo el 30%</option>
                                        <option value="6- Sin Potencial">6- Sin Potencial</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-gestMes" type="checkbox"/>
                                    <label>Gestión Mensual</label>
                                    <select name="gestMes" id="gestMes" class="form-control" disabled="false" required>
                                        <option value="Cotizacion">Cotizacion</option>
                                        <option value="Nota de Venta">Nota de Venta</option>
                                        <option value="Sin Gestion">Sin Gestion</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-deuda" type="checkbox"/>
                                    <label>Estado Deuda</label>
                                    <select name="deuda" id="deuda" class="form-control" disabled="false" required>
                                        <option value="Activa">Activa</option>
                                        <option value="Vencida">Vencida</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-fechaProximaAccion" type="checkbox"/>
                                    <label>Proxima Accion</label>
                                    <div class='input-group date' id='fecha'>
                                        <input class="form-control" name="fechaProximaAccion" id="fechaProximaAccion" type='date' disabled="false" required/>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-foco" type="checkbox"/>
                                    <label>Focos</label>
                                    <select name="foco" id="foco" class="form-control" disabled="false" required>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <input name="check" class="pull-right" id="check-DCartera" type="checkbox"/>
                                    <label>Dias en cartera</label>
                                    <select name="DCartera" id="DCartera" class="form-control" disabled="false" required>
                                        <option value="Todos">Todos</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <button id="Filtrar" class="btn btn-danger pull-right" onclick="Filtrar()">Filtrar</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div id="carteraExterna" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Cliente fuera de cartera</h2>
                        <div class="card-block">
                            <table class="table table-striped table-bordered nowrap dataTable" style="margin: 0 auto;">
                                <tr>
                                    <td>Rut</td>
                                    <td id="rut" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Razon social</td>
                                    <td id="razons"  class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Rel. comercial</td>
                                    <td id="id" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Segmento</td>
                                    <td id="segmen" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Clasificación</td>
                                    <td id="clasificaci" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Ejecutivo Asignado</td>
                                    <td id="ejecutivo" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Fecha ult. gestión</td>
                                    <td id="fechaUlt" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Ult. gestión</td>
                                    <td id="ultGestion" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Días de salida cat.</td>
                                    <td id="dias" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Potencial</td>
                                    <td id="poten" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Analisis de plazos</td>
                                    <td id="analisis" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Ultima Nota</td>
                                    <td id="nota" class="text-right"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="RelComercial" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Rel. Comercial</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Agrupacion de ruts que comparten el mismo comprador o son parte de un conglomerado</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="GestionMensual" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Gestión Mensual</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Define la accion de mayor relevancia realizada en el rut, durante el mes en curso</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="LaBrecha" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Brecha</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Diferencia entre la venta promedio y el potencial del cliente</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="SalidaCartera" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Salida de Cartera</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Dias que faltan para que, segun definiciones de politica de cartera , el rut pueda ser reasignado si no se concreta venta</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="elPotencial" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">El Potencial</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Cálculo matemático que define el máximo de ventas que puede tener un cliente</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="DeudaActiva" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Deuda Activa</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Monto de facturas que no han vencido</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="DeudaVencida" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Deuda Vencida</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Monto de facturas vencidas</h3>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="card page-header p-0">
                
                <div class="card-block">

                    <table id="table">
                        <thead></thead>
                        <tbody></tbody>
                    </table>

                    <div class="table-responsive">
                        <h5 class="sub-title m-b-15">Clientes y registros dentro de plazos de politica</h5>
                        {{-- <h6>Selección de columnas a ocultar</h6>
                        <select id="seleccion" name="seleccion[]" class="js-example-tokenizer col-sm-12 select2-hidden-accessible" multiple="" tabindex="-1" aria-hidden="true">
                            <option value="1">Clasificación</option>
                            <option value="2">Rut</option>
                            <option value="3">Rel. comercial</option>
                            <option value="4">Razon social</option>
                            <option value="5">Unidad</option>
                            <option value="6">Segmento</option>
                            <option value="7">Status de credito</option>
                            <option value="8">Gestion mensual</option>
                            <option value="9">Última gestión</option>
                            <option value="10">Fecha última gestión</option>
                            <option value="11">Potencial</option>
                            <option value="12">Rango</option>
                            <option value="13">Venta año anterior</option>
                            <option value="14">Venta Promedio</option>
                            <option value="15">Venta Actual</option>
                            <option value="16">Brecha</option>
                            <option value="17">Deuda Activa</option>
                            <option value="18">Deuda vencida</option>
                            <option value="19">Salida Cartera</option>
                            <option value="20">Próx. Acc. agendada</option>
                            <option value="21">Fecha Próx. Accion</option>
                            <option value="22">Actualización</option>
                        </select> --}}
                          
                        <div class="dataTables_wrapper" id="detalle" onkeyup="Formato()">
                            <!-- {{-- <table id="tabla" class="table table-hover display nowrap table-bordered table-styling sortable">
                                <thead>
                                    <tr>
                                        <th  onclick="Formato()" >Acc</th>
                                        <th id="1" onclick="Formato()" >Clasificacion</th>
                                        <th id="2" class="" onclick="Formato()" >Rut</th>
                                        <th id="3" onclick="Formato()" >Rel. Comercial
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#RelComercial"></i></th>
                                        <th id="4" onclick="Formato()" >Razon Social</th>
                                        <th id="5" onclick="Formato()" >Unidad
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#Unidad"></i></th>
                                        <th id="6" onclick="Formato()" >Segmento
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th> 
                                        <th id="7" onclick="Formato()" >Status de Credito</th>
                                        <th id="8" onclick="Formato()" >Gestión Mensual
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#GestionMensual"></i></th>
                                        <th id="9"onclick="Formato()" >Ultima Gestión</th>
                                        <th id="10"onclick="Formato()" >Fecha Ultima Gestión</th>
                                        <th id="11"onclick="Formato()"  id="potencial">Potencial
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial"></i> </th>
                                        <th id="12" onclick="Formato()" >Rango</th>
                                        <th id="13" onclick="Formato()" >Venta Año Anterior</th>
                                        <th id="14" onclick="Formato()" >Venta Promedio</th>
                                        <th id="15" onclick="Formato()" >Venta Actual</th>
                                        <th id="16" onclick="Formato()" >Brecha
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#LaBrecha"></i></th>
                                        <th id="17" onclick="Formato()" >Deuda Activa
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaActiva"></i></th>
                                        <th id="18" onclick="Formato()" >Deuda Vencida
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaVencida"></i></th>
                                        <th id="19" onclick="Formato()" >Salida de Cartera
                                            <i class="fa fa-info-circle" data-toggle="modal" data-target="#SalidaCartera"></i>
                                        </th>
                                        <th id="20" onclick="Formato()" >Próx. Acción Agenda</th>
                                        <th id="21" onclick="Formato()" >Fecha Próx. Acción</th>
                                        <th id="22" onclick="Formato()" >Actualización</th>
                                    </tr>
                                </thead>
                                <tbody id="cuadro1.1">
                                </tbody>
                            </table> --}} -->
                        </div>
                        
                        <div class="spinner" id="spinner" style="display: none;">
                            <center>
                                <div class="bolitas">
                                    <div class="dot1"></div>
                                    <div class="dot2"></div>
                                </div>
                                <div class="cargando">Cargando datos...</div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    @include('back.others.js.portal')
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>
    @include('back.others.js.cartera')
    <script>

        $(document).ready(function () {
            //getCarteraClientes();
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-briefcase";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Cartera clientes";

            //alert($.fn.jquery);
            var $table = $('#table');

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            
            //tabla($table);
            //$('#seleccion').val(["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21"]);
            getDerechos();

            var value = '{{ $valor }}';
            //console.log('elvalor llega:' + value);
            if(value > 0){
                if (value < 10) {
                    document.getElementById("check-rango").click();
                    if(value == '1'){
                        document.getElementById('rango').value = "1- Rango sobre el 100%";
                    }else{
                        if(value == '2'){
                            document.getElementById('rango').value = "2- Rango sobre el 70%";
                        }else{
                            if(value == '3'){
                                document.getElementById('rango').value = "3- Rango sobre el 50%";
                            }else{
                                if(value == '4'){
                                    document.getElementById('rango').value = "5- Rango bajo el 30%";
                                }else{
                                    if(value == '5'){
                                        document.getElementById('rango').value = "6- Sin Potencial";
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if(value > 20){
                        document.getElementById("check-deuda").click();
                        if(value == '21'){
                            document.getElementById('deuda').value = "Activa";
                        }else{
                            if(value == '22'){
                                document.getElementById('deuda').value = "Vencida";
                            }
                        }
                    }else{
                        document.getElementById("check-credito").click();
                        if(value == '11'){
                            document.getElementById('credito').value = "Activo";
                        }else{
                            if(value == '12'){
                                document.getElementById('credito').value = "Bloqueado";
                            }else{
                                if(value == '13'){
                                    document.getElementById('credito').value = "Sin analisis";
                                }
                            }
                        }
                    }
                    
                }
            }
            
            getfocosCartera();
            document.getElementById('icono').click();
            Filtrar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
            
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=65;
            var oldvalue='';
            var newvalue='';
        });

        function tabla($el) {
            var cells = 20
            var rows = 20
            var i
            var j
            var row
            var columns = [
                {
                    field: 'state',
                    checkbox: true,
                    valign: 'middle'
                }
            ]
            var data = []

            for (i = 0; i < cells; i++) {
                columns.push({
                    field: 'field' + i,
                    title: 'Cell' + i,
                    sortable: true,
                    valign: 'middle',
                    formatter: function (val) {
                        return '<div class="item">' + val + '</div>'
                    },
                    events: {
                        'click .item': function () {
                            console.log('click')
                        }
                    }
                })
            }
            for (i = 0; i < rows; i++) {
                row = {}
                for (j = 0; j < cells + 3; j++) {
                    row['field' + j] = 'Row-' + i + '-' + j
                }
                data.push(row)
            }
            // $($el).Tabledit({
            //     height: $('#height').prop('checked') ? 400 : undefined,
            //     aoColumns: columns,
            //     columns: columns,
            //     data: data,
            //     toolbar: '.toolbar',
            //     search: true,
            //     showColumns: true,
            //     showToggle: true,
            //     clickToSelect: true,
            //     fixedColumns: true,
            //     fixedNumber: 2,
            //     fixedRightNumber: 2
            // })
            
            $el.bootstrapTable('destroy').bootstrapTable({
                // height: $('#height').prop('checked') ? 400 : undefined,
                // aoColumns: columns,
                columns: columns,
                data: data,
                // toolbar: '.toolbar',
                // search: true,
                // showColumns: true,
                // showToggle: true,
                // clickToSelect: true,
                // fixedColumns: true,
                // fixedNumber: 2,
                // fixedRightNumber: 2
            })
        }

        $('#seleccion').on('change',function(){
            console.log($('#seleccion').val());
            lista = $('#seleccion').val();
            var table, tr, td, i, text;
            table = document.getElementById("tabla");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                $(td).removeClass('ocultar');  
                $(td).show();
                td = tr[i].getElementsByTagName("td")[1];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[2];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[3];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[4];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[5];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[6];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[7];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[8];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[9];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[10];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[11];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[12];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[13];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[14];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[15];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[16];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[17];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[18];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[19];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[20];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
                td = tr[i].getElementsByTagName("td")[21];
                $(td).removeClass('ocultar');  
                $(td).show();
                $("#1").hide(); 
            }

            for (i = 0; i < tr.length; i++) {
                for (a = 0; a < lista.length; a++) {
                    td = tr[i].getElementsByTagName("td")[lista[a]];
                    $(td).addClass('ocultar');  
                }
            }
            
            for (a = 0; a < lista.length; a++) {
                $("#" + lista[a]).hide(); 
            }

            $(".ocultar").hide();
            
        });

        $('#cartera1').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#cartera2').click(function(){
            document.getElementById("Filtrar").click();
        });
        
        $('#cartera3').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#venta1').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#venta2').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#brecha1').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#brecha2').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#brecha3').click(function(){
            document.getElementById("Filtrar").click();
        });

        $('#Filtrar').click(function(){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=27;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        });

        $('#check-clasificacion').on('change', function() {
            if (document.getElementById("clasificacion").disabled == true){
                document.getElementById("clasificacion").disabled = false;
            }else{
                document.getElementById("clasificacion").disabled = true;
            }
        });

        $('#check-segmento').on('change', function() {
            if (document.getElementById("segmento").disabled == true){
                document.getElementById("segmento").disabled = false;
            }else{
                document.getElementById("segmento").disabled = true;
            }
        });

        $('#check-credito').on('change', function() {
            if (document.getElementById("credito").disabled == true){
                document.getElementById("credito").disabled = false;
            }else{
                document.getElementById("credito").disabled = true;
            }
        });

        $('#check-rango').on('change', function() {
            if (document.getElementById("rango").disabled == true){
                document.getElementById("rango").disabled = false;
            }else{
                document.getElementById("rango").disabled = true;
            }
        });

        $('#check-gestMes').on('change', function() {
            if (document.getElementById("gestMes").disabled == true){
                document.getElementById("gestMes").disabled = false;
            }else{
                document.getElementById("gestMes").disabled = true;
            }
        });

        $('#check-deuda').on('change', function() {
            if (document.getElementById("deuda").disabled == true){
                document.getElementById("deuda").disabled = false;
            }else{
                document.getElementById("deuda").disabled = true;
            }
        });

        $('#check-fechaProximaAccion').on('change', function() {
            if (document.getElementById("fechaProximaAccion").disabled == true){
                document.getElementById("fechaProximaAccion").disabled = false;
            }else{
                document.getElementById("fechaProximaAccion").disabled = true;
            }
        });

        $('#check-foco').on('change', function() {
            if (document.getElementById("foco").disabled == true){
                document.getElementById("foco").disabled = false;
            }else{
                document.getElementById("foco").disabled = true;
            }
        }); 

        $('#check-DCartera').on('change', function() {
            if (document.getElementById("DCartera").disabled == true){
                document.getElementById("DCartera").disabled = false;
            }else{
                document.getElementById("DCartera").disabled = true;
            }
        });

        function FichaCliente(rut){
            var value = '{{ $valor }}';
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=2;
            var actionid=28;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("tabla");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                if (value > 0) {
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                location.href = window.location.href.replace('/cartera/' + value ,'') + "/fichacliente/" + cells[2].innerHTML;
                            };
                }else{
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                location.href = window.location.href.replace('/cartera','') + "/fichacliente/" + cells[2].innerHTML;
                            };
                }
            };
        }

        function Formato(){
            var table = document.getElementById("tabla");
            var rows = table.getElementsByTagName("tr");
            
            row = table.rows[0];
                
            $(row.cells[2]).removeClass('sorting');
            $(row.cells[2]).removeClass('sorting_asc');
            $(row.cells[2]).removeClass('sorting_desc');

            for (i = 1; i < rows.length; i++) {
                row = table.rows[i]; 
                
                // $(row.cells[2]).addClass('columnaFija');

                if (row.cells[11].innerHTML.substring(0,1) != '$'){
                    row.cells[11].innerHTML = '$' + row.cells[11].innerHTML;
                }
                if (row.cells[13].innerHTML.substring(0,1) != '$'){
                    row.cells[13].innerHTML = '$' + row.cells[13].innerHTML;
                }
                if (row.cells[14].innerHTML.substring(0,1) != '$'){
                    row.cells[14].innerHTML = '$' + row.cells[14].innerHTML;
                }
                if (row.cells[15].innerHTML.substring(0,1) != '$'){
                    row.cells[15].innerHTML = '$' + row.cells[15].innerHTML;
                }
                if (row.cells[16].innerHTML.substring(0,1) != '$'){
                    row.cells[16].innerHTML = '$' + row.cells[16].innerHTML;
                }
                if (row.cells[17].innerHTML.substring(0,1) != '$'){
                    row.cells[17].innerHTML = '$' + row.cells[17].innerHTML;
                }
                if (row.cells[18].innerHTML.substring(0,1) != '$'){
                    row.cells[18].innerHTML = '$' + row.cells[18].innerHTML;
                }
            };

        }

        $('#tabla').on('mouseover', function() {
            Formato();
        });

        function orden(){
            // document.getElementById("11").click();
            // document.getElementById("11").click();  
        }
        
        function Filtrar() {
            var table = '<table id="tabla" class="table table-hover display nowrap table-bordered table_fija"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th  onclick="Formato()" >Acc</th>\n' +
                '                            <th onclick="Formato()" >Clasificacion</th>\n' +
                '                            <th class="" onclick="Formato()" >Rut</th>\n' +
                '                            <th onclick="Formato()" >Rel. Comercial\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#RelComercial"></i></th>\n' + 
                '                            <th onclick="Formato()" >Razon Social</th>\n' +
                '                            <th onclick="Formato()" >Unidad\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#Unidad"></i></th>\n' + 
                '                            <th onclick="Formato()" >Segmento\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th>\n' + 
                '                            <th onclick="Formato()" >Status de Credito</th>\n' +
                '                            <th onclick="Formato()" >Gestión Mensual\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#GestionMensual"></i></th>\n' + 
                '                            <th onclick="Formato()" >Ultima Gestión</th>\n' +
                '                            <th onclick="Formato()" >Fecha Ultima Gestión</th>\n' +
                '                            <th onclick="Formato()"  id="potencial">Potencial\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#elPotencial"></i> </th>\n' +
                '                            <th onclick="Formato()" >Rango</th>\n' +
                '                            <th onclick="Formato()" >Venta Año Anterior</th>\n' +
                '                            <th onclick="Formato()" >Venta Promedio</th>\n' +
                '                            <th onclick="Formato()" >Venta Actual</th>\n' +
                '                            <th onclick="Formato()" >Brecha\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#LaBrecha"></i></th>\n' + 
                '                            <th onclick="Formato()" >Deuda Activa\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaActiva"></i></th>\n' + 
                '                            <th onclick="Formato()" >Deuda Vencida\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#DeudaVencida"></i></th>\n' + 
                '                            <th onclick="Formato()" >Salida de Cartera\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#SalidaCartera"></i></th>\n' + 
                '                            <th onclick="Formato()" >Próx. Acción Agenda</th>\n' +
                '                            <th onclick="Formato()" >Fecha Próx. Acción</th>\n' +
                '                            <th onclick="Formato()" >Actualización</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';

            document.getElementById("detalle").innerHTML = table;
            var clasificacionV = 'null';
            var segmentoV = 'null';
            var creditoV = 'null';
            var rangoV = 'null';
            var gestMesV = 'null';
            var deudaV = 'null';
            var fechaProximaAccionV = 'null';
            var brechaV = $('input:radio[name=brecha]:checked').val();
            var ventaV = $('input:radio[name=venta]:checked').val();
            var carteraV = $('input:radio[name=cartera]:checked').val();
            var focoV = 'null';
            var DCarteraV = 'null';

            var token = '{{ csrf_token() }}';
            if (document.getElementById("clasificacion").disabled == false){
                clasificacionV = document.getElementById("clasificacion").value;
            }
            if (document.getElementById("segmento").disabled == false){
                segmentoV = document.getElementById("segmento").value;
            }
            if (document.getElementById("credito").disabled == false){
                creditoV = document.getElementById("credito").value;
            }
            if (document.getElementById("rango").disabled == false){
                rangoV = document.getElementById("rango").value;
            }
            if (document.getElementById("gestMes").disabled == false){
                gestMesV = document.getElementById("gestMes").value;
            }
            if (document.getElementById("deuda").disabled == false){
                if (document.getElementById("deuda").value == "Activa") {
                    deudaV = "deuda_activa>0";
                }else{
                    deudaV = "deuda_vencida>0";
                }
            }
            if (document.getElementById("fechaProximaAccion").disabled == false){
                fechaProximaAccionV = document.getElementById("fechaProximaAccion").value;
            }
            if (document.getElementById("foco").disabled == false){
                focoV = document.getElementById("foco").value;
            } 
            if (document.getElementById("DCartera").disabled == false){
                DCarteraV = document.getElementById("DCartera").value;
            }

            var parametros = {
                _token: token,
                clasificacion: clasificacionV,
                segmento: segmentoV,
                credito: creditoV,
                rango: rangoV,
                gestMes: gestMesV,
                deuda: deudaV,
                fechaProximaAccion: fechaProximaAccionV,
                brecha: brechaV,
                venta: ventaV,
                cartera: carteraV,
                foco: focoV,
                DCartera: DCarteraV
            };
            
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/cartera')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#tabla").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rut",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                            '   <button type="button"\n' +
                                            '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:20px!important;height:20px!important;"\n' +
                                            '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                            '       class="icofont icofont-ui-user f-16"></span>\n' +
                                            '   </button>\n' +
                                            '</div>\n';}},
                                {data: "clasificacion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "rut",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "relacion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "razon_social",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left" title="' + data + '">' + data + '</span>\n';}}, 
                                {data: "unidad",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "segmento",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "status_credito",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "gestion_del_mes",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "ultima_gestion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "fecha_ultima_gestion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "potencial",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "rango",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},  
                                {data: "vta_ano_anterior",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "vta_promedio",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "venta_actual",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "brecha",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "deuda_activa",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "deuda_vencida",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "dias_para_salir_de_cartera",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "proxima_accion_agendada",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "fecha_proxima_accion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "actualizacion_de_informacion",
                                    class:'p-tbl-cartera',
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}
                            ],
                            //"ordering": false,
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        $("#spinner").hide();
                        orden();
                        Formato();
                        Formato();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getfocosCartera(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('getfocosCartera') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<option value="' + json.response[i].foco + '">' + json.response[i].foco + '</option>';
                        }
                        document.getElementById("foco").innerHTML = text;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function getRut(e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                var token = '{{ csrf_token() }}';
                var parametros = {
                    _token: token,
                    rutcli: document.getElementById("busquedaRut").value
                };
                var text = '';
                $.ajax({
                    url: '{{ url('getRut') }}',
                    data: parametros,
                    type: 'POST',
                    cache: false,
                    datatype: 'json',
                    async: true,
                    success: function (json) {
                        //console.log(json.response);
                        if (json.response.length > 0) {
                            document.getElementById("rut").innerHTML = json.response[0].rutcli;
                            document.getElementById("razons").innerHTML = json.response[0].razons;
                            document.getElementById("id").innerHTML = json.response[0].id;
                            document.getElementById("clasificaci").innerHTML = json.response[0].clasificacion;
                            document.getElementById("ejecutivo").innerHTML = json.response[0].ejecutivo;
                            document.getElementById("fechaUlt").innerHTML = json.response[0].fecha_ultima_gestion_id;
                            document.getElementById("ultGestion").innerHTML = json.response[0].ultima_gestion_id;
                            document.getElementById("dias").innerHTML = json.response[0].dias_restantes;
                            document.getElementById("poten").innerHTML = '$' + number_format(json.response[0].potencial,0);
                            document.getElementById("analisis").innerHTML = json.response[0].analisis_plazos;
                            document.getElementById("segmen").innerHTML = json.response[0].segmento;
                            document.getElementById("nota").innerHTML = 'hace ' + json.response[0].dias_nota_rut + ' días';
                            document.getElementById("mostrar").click();
                        } else {
                            document.getElementById("mensaje").style.display = "block";
                            setTimeout('document.getElementById("mensaje").style.display = "none"',6000);
                            //alert("Sin información");
                            $("#spinner").hide();
                        }
                    },
                    error: function(e){
                        var code = e.status;
                        var text = e.statusText;
                        //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                    }
                });
            }
        }
    </script>
@endsection