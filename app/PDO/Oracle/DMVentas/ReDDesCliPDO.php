<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 16:48
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\DeCliente;
use App\Entities\Oracle\DMVentas\ReDDesCli;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class ReDDesCliPDO extends Model
{
    public static function getDireccionesPorRut($rutcli, $codemp = 3, $have_codsap = true, $first_row = false)
    {
        $arrayReturn = null;
        $sql = "SELECT * FROM re_ddescli WHERE rutcli = :rut_cli AND codemp = :cod_emp";
        if ($have_codsap) $sql .= " AND id_cliente_sap IS NOT NULL";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cli' => $rutcli,
            'cod_emp' => $codemp,
        ]);

        $arrayReturn = self::returnFormat($resultado, $first_row);

        if ($arrayReturn) {
            $arrayReturn = new Collection($arrayReturn);
        }

        return $arrayReturn;
    }

    public static function getDireccionesPorRutCustom1($rutcli)
    {
        $arrayReturn = null;
        $sql = "SELECT a.rutcli, a.coddir, a.dirdes,"
            . " a.codcom, SAP_Get_ComCiuReg(a.codcom, 'C', 'D') AS descom,"
            . " a.codciu, SAP_Get_ComCiuReg(a.codcom, 'D', 'D') as desciu,"
            . " SAP_Get_ComCiuReg(a.codcom, 'R', 'C') AS codreg, SAP_Get_ComCiuReg(a.codcom, 'R', 'D') AS desreg"
            . " FROM re_ddescli a WHERE a.rutcli = :rut_cli";
        $arrayReturn = DB::connection('oracle_dmventas')->select($sql, [
            'rut_cli' => $rutcli,
        ]);
        if ($arrayReturn) {
            $arrayReturn = new Collection($arrayReturn);
        }

        return $arrayReturn;
    }

    private static function returnFormat($resultado, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($resultado as $registro) {
            $cencos = new ReDDesCli($registro);
            if ($first_row) {
                return $cencos;
            }
            $arrayReturn[] = $cencos;
        }

        return $arrayReturn;
    }
}