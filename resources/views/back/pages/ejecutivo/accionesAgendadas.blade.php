@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-hand-rock-o text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo" >Acciones Agendadas</h3>
                            <span class="f-16">Acciones Agendadas | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        {{-- <div class="col-sm-4">
                            <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()">Buscar</button>
                        </div>  --}}
                    </div>
                    <div class="card-header-right">
                        {{-- <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul> --}}
                    </div>
                </div>
                <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label>Periodo</label>
                                    <div class='input-group date' id='fecha'>
                                        <input class="form-control" name="Periodo" id="Periodo" type='date'/>
                                    </div>
                                </div>

                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <label>Activo</label>
                                    <select name="filtroActivo" id="filtroActivo" class="form-control" required="">
                                        <option value="0">Todas</option>
                                        <option value="1">Activo</option>
                                        <option value="2">No activo</option>
                                    </select>
                                </div>

                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <label>Permanente</label>
                                    <select name="filtroPermanente" id="filtroPermanente" class="form-control" required="">
                                        <option value="0">Todas</option>
                                        <option value="1">Permanente</option>
                                        <option value="2">No permanente</option>
                                    </select>
                                </div>
                                
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <br>
                                    <div class='input-group date' id='fecha'>
                                        <button id="Buscar" class="btn btn-danger pull-right" onclick="Buscar()" style="margin-left: 5px">Buscar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div class="table-responsive">
                    <div id="detalle"></div>
                    
                    <div class="spinner" id="spinner" style="display: none;">
                        <center>
                            <div class="bolitas">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                            <div class="cargando">Cargando datos...</div>
                        </center>
                    </div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-hand-rock-o";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Acciones Agendadas";
            var date = new Date();
            var fecha = new Date((date.getMonth()+1) + '-' + (date.getDate()) + '-' + date.getFullYear());

            //console.log(new Date((date.getMonth()+1) + '-' + (date.getDate()) + '-' + date.getFullYear()));
            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            document.getElementById("Periodo").value = fecha.toISOString().substr(0, 10);

            getDerechos();
            Buscar();
            //getFechas();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });


        function FichaCliente(rut){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=21;
            var actionid=53;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                    row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                //console.log(window.location.href.replace('/ingresosWeb','') + "/fichacliente/" + cells[1].innerHTML);
                                location.href = window.location.href.replace('/acciones','') + "/fichacliente/" + cells[1].innerHTML;
                            };
                
            };
        }

        function getFechas() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                fecha: document.getElementById("Periodo").value
            };
            var text = '';
            console.log(parametros);
            $.ajax({
                url: '{{ url('/pricing/getFechasAcciones') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    console.log(json);
                    console.log('json');
                    if (Object.keys(json).length > 0) {
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<option value="' + json.response[i].fecha_accion_agendada.substring(0,10) + '">' + json.response[i].fecha_accion_agendada.substring(0,10) + '</option>';
                        }
                        document.getElementById("Periodo").innerHTML = text;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
            });
        }

        function Buscar() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=20;
            var actionid=52;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar1();
        }
        
        function Buscar1() {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Acc</th>\n' +
                '                            <th>Rut</th>\n' +
                '                            <th>Razon Social</th>\n' +
                // '                            <th>Segmento\n' +
                // '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th>\n' + 
                // '                            <th>Fecha de Acción</th>\n' +
                '                            <th>Acción Agendada</th>\n' +
                '                            <th>Fecha Ult. Acción</th>\n' +
                '                            <th>Ult. Acción</th>\n' +
                '                            <th>Observación</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var date = new Date();
            var lafecha = document.getElementById("Periodo").value;
            // if (lafecha === '') {
            //     lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + (date.getDay()+1);
            //     if ((date.getDay()+1) > 9) {
            //         document.getElementById("titulo").innerHTML = "Acciones Agendadas " + (date.getDay()+1) + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
            //     } else {
            //         document.getElementById("titulo").innerHTML = "Acciones Agendadas 0" + (date.getDay()+1) + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
            //     }
            // }else {
                document.getElementById("titulo").innerHTML = "Acciones Agendadas " + lafecha.substring(8,10) + '-' + lafecha.substring(5,7) + '-' + lafecha.substring(0,4);
            // }
            
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha,
                activo: document.getElementById("filtroActivo").value,
                permanente: document.getElementById("filtroPermanente").value
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/acciones')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                            '   <button type="button"\n' +
                                            '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                            '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                            '       title="Ficha Cliente" onclick="FichaCliente(' + data + ')"><span\n' +
                                            '       class="icofont icofont-ui-user f-16"></span>\n' +
                                            '   </button>\n' +
                                            '</div>\n';}},
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return  data;}}, 
                                {data: "razons",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "accion_agendada_para_hoy",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "fecha_ultima_accion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "accion_realizada",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "observacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',  
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        var table, tr, td, i, text;
                        table = document.getElementById("detallenota");
                        var rows = table.getElementsByTagName("tr");
                        tr = table.getElementsByTagName("tr");

                        for (i = 0; i < tr.length; i++) {
                            td = tr[i].getElementsByTagName("td")[4];
                            $(td).addClass('text-right');
                            text = $(td).text().substring();
                            $(td).text(text.substr(0,10));
                            $(td).text(text.substr(8,2) + "-" + text.substr(5,2) + "-" + text.substr(0,4));

                        }

                        $("#spinner").hide();
                    } else {
                        var text = '<div class="alert alert-danger background-danger">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                            '<i class="icofont icofont-close-line-circled text-white"></i>' +
                                        '</button>' +
                                        '<strong>Sin Ingresos Web</strong>' +
                                    '</div>';
                        document.getElementById("detalle").innerHTML = text
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection