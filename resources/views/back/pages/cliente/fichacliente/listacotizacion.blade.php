@extends('back.master.masterpage')
@section('contenido')
    <style>

        .botones_cotizaciones {
            white-space: nowrap;
            width: 1%;
            padding: 10px 10px 10px 10px !important;
        }

        .filas {
            white-space: nowrap;
            width: 1%;
            padding: 10px 15px 10px 15px !important;
        }

        .dt-center { text-align: center;!important; }

        .est-coti-1{
            color: #fe9365 !important

        }
        .est-coti-2{
            color: #fe5d70 !important

        }
        .est-coti-3{
            color: #0ac282 !important
        }

    </style>
    <div class="pcoded-inner-content">
        <div class="card">
            <div class="card-header">
                <h5>Listado cotizaciones</h5>
                <div class="card-header-right">
                    <button class="btn btn-mat btn-primary btn-sm" id="btn_gencoti" onclick="javascript:redirecturl(id);">
                        <i class="fa fa-plus f-14 text-white"></i> Generar nueva cotización</button>
                    <button class="btn btn-mat btn-warning btn-sm" id="btn_fichacliente" onclick="javascript:redirecturl(id);">
                        <i class="icofont icofont-open-eye f-18 text-white"></i> Ver ficha cliente</button>
                    <button id="btn_actualizar" type="button" onclick="refreshPage();" class="btn btn-success btn-icon waves-effect waves-light" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refrescar página">
                        <i class="fa fa-refresh text-white"></i>
                    </button>
                    <button id="btn_volver" type="button" onclick="" class="btn btn-inverse btn-icon waves-effect waves-light" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Volver a">
                        <i class="ti-back-right text-white"></i>
                    </button>
                </div>
                <span class="m-t-25">Información cliente seleccionado:</span>
                <div class="row m-t-10">
                    <div class="col-lg-4">
                        <div class="row">
                            <label class="col-sm-4 col-lg-2 col-form-label p-r-0">Rut</label>
                            <div class="col-sm-8 col-lg-10 p-l-0">
                                <div class="input-group input-group-sm input-group-button">
                                    <input id="tx_rut_cliente" type="text" class="form-control" placeholder="Rut cliente" value="" readonly>
                                    <button class="btn btn-primary btn-mini p-r-5 p-l-5" title="Cambiar" data-toggle="modal" data-target="#rut_cliente_modal">
                                        <i class="fa fa-search f-14 m-r-0 m-l-0"></i>
                                    </button>
                                    <div class="modal fade" id="rut_cliente_modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <h5>Buscar cliente por rut</h5>
                                                    <div class="card-block">
                                                        <div class="form-group row">
                                                            <label class="col-sm-5 col-form-label">Ingrese rut</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control form-control-sm" style="width: 100%;" id="tx_rut_buscar" placeholder="Rut" autocomplete="off">
                                                                <span class="messages popover-valid"></span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-5"></label>
                                                            <div class="col-sm-7">
                                                                <button type="button" class="btn btn-primary btn-mini m-b-0" onclick="findClientForRut();">Buscar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-mini waves-effect " data-dismiss="modal">Cerrar</button>
                                                    <button id="select_rut" type="button" class="btn btn-primary btn-mini waves-effect waves-light " onclick="selectClientFromRut();pñ.ñ5">Seleccionar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <label class="col-sm-5 col-lg-2 col-form-label p-r-0">Razón social</label>
                            <div class="col-sm-7 col-lg-10 p-l-0">
                                <div class="input-group input-group-sm input-group-button">
                                    <input id="tx_razons_cliente" type="text" class="form-control" placeholder="Razón social" value="" readonly>
                                    <button class="btn btn-primary btn-mini p-r-5 p-l-5" title="Cambiar" data-toggle="modal" data-target="#razons_cliente_modal">
                                        <i class="fa fa-search f-14 m-r-0 m-l-0"></i>
                                    </button>
                                    <div class="modal fade" id="razons_cliente_modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <h5>Buscar cliente por razón social</h5>
                                                    <div class="card-block">
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Ingrese razón social</label>
                                                            <div class="col-sm-9 m-t-15">
                                                                <input type="text" class="form-control form-control-sm" style="width: 100%;" id="tx_razons_buscar" placeholder="Razón social" autocomplete="off">
                                                                <span class="messages popover-valid"></span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3"></label>
                                                            <div class="col-sm-9">
                                                                <button id="btn_buscar_razons" type="button" class="btn btn-primary btn-mini m-b-0" onclick="findClientForRazons();">Buscar</button>
                                                            </div>
                                                        </div>
                                                        <div class="row m-t-20" id="div_tb_clientes_razons">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-mini waves-effect " data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-primary btn-mini waves-effect waves-light " onclick="selectClientFromTableResult();">Seleccionar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-block">
                <div class="modal fade" id="modal-send-email" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Enviar cotización por email</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-send-email">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="card-block">
                                    <div class="form-group row">
                                        <p>Ingrese correo al que desea enviar la cotización seleccionada:</p>
                                        <label class="col-sm-5 col-form-label">Ingrese destinatario:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm form-bg-warning" style="width: 100%;" id="tx_correo_destinatario" placeholder="Correo electrónico" autocomplete="off" required>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Comentario opcional:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" cols="5" class="form-control" id="tx_comentario_ejecutivo" placeholder="Comentario opcional" autocomplete="off"></textarea>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5"></label>
                                        <div class="col-sm-7">
                                            <input type="hidden" value="" id="h_numcot">
                                            <button type="button" class="btn btn-primary btn-mini m-b-0" onclick="generarArchivoAdjunto();">Enviar cotización</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dt-responsive table-responsive">
                    <table id="tb_listado_cotizaciones" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>N° coti</th>
                            <th>F. emisión</th>
                            <th>F. vencimiento</th>
                            <th>CeCo</th>
                            <th>Subtotal</th>
                            <th>Iva</th>
                            <th>Total</th>
                            <th>Margen</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>N° coti</th>
                            <th>F. emisión</th>
                            <th>F. vencimiento</th>
                            <th>CeCo</th>
                            <th>Subtotal</th>
                            <th>Iva</th>
                            <th>Total</th>
                            <th>Margen</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('back.others.js.listacotizaciones')
    @include('back.master.others.pre-loader')
    @include('back.others.js.portal')
@endsection
@section('custom-includes')
    <script>
        $(document).ready(function(){
            moment.locale('es');
            document.getElementById("pre_loader").className = "loader animation-start d-none";
            asignarUrlPreviousPage();
        });

        setInputsDataCliente();
        getCotizaciones();

    </script>
@endsection
