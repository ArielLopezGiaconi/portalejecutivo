<?php


namespace App\PDO\Oracle\DMVentas;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;


class QvMetasVendedorPDO extends Model
{
    public static function getMetaEjecutivo($userid, $rutusu)
    {
        try{
            $sql = "SELECT sum(metaventa) as meta from qv_metas_vendedor_d
            WHERE TRUNC(fecha, 'Month') = TRUNC(SYSDATE, 'Month')
            and userid = :userid
            and rutven = :rutusu";

            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'userid' => $userid,
                'rutusu' => $rutusu
            ]);
            return $resultado;
        }
        catch(\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }
}