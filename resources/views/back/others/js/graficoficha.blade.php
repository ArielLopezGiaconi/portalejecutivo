<script>
    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var moduleid_global = 6;

    function graficoCliente() {
        var token = '{{ csrf_token() }}';
        var rutcli = '{{ $rutcli }}';
        var parametros = {
            _token: token,
            _rutcli: rutcli
        };
        $.ajax({
            data: parametros,
            url: '{{ url('getgraficoficha') }}',
            type: 'POST',
            cache: true,
            datatype: 'json',
            async: true,
            beforeSend: function () {

            },
            success: function (json) {
                if (json.code == 200) {
                    var Data = json.response;
                    am4core.ready(function () {
                        // Themes begin
                        am4core.useTheme(am4themes_animated);
                        // Themes end
                        am4core.options.autoSetClassName = true;
                        // Create chart instance
                        var chart = am4core.create("chartdiv", am4charts.XYChart);
                        chart.colors.step = 2;
                        chart.maskBullets = false;
                        // Add data
                        /*
                        * Ocuparemos townSize como diferencia entre potencial y venta especifica
                        * Townsize define el tamaño de la burbuja
                        *
                        *
                        * */
                        chart.data = Data;
                        // Create axes
                        //Eje de Fechas
                        var dateAxis = chart.xAxes.push(new am4charts.DateAxis())
                        dateAxis.dataFields.category = "category";
                        dateAxis.renderer.grid.template.location = 0;
                        dateAxis.renderer.minGridDistance = 50;
                        dateAxis.renderer.grid.template.disabled = true;
                        dateAxis.renderer.fullWidthTooltip = true;
                        dateAxis.fontSize = 12;
                        dateAxis.dateFormats.setKey("month", "YYYY MM");
                        dateAxis.periodChangeDateFormats.setKey("month", "[bold]yyyy MM[/]");
                        //Eje de Ventas
                        var VentaAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        //VentaAxis.title.text = "Venta";
                        //VentaAxis.title.fontWeight = "bold";
                        VentaAxis.renderer.grid.template.disabled = true;
                        VentaAxis.fontSize = 12;
                        VentaAxis.numberFormatter = new am4core.NumberFormatter();
                        VentaAxis.numberFormatter.numberFormat = "#,###a";
                        //Eje de Margen
                        var MargenAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        //MargenAxis.title.text = "Margen";
                        //durationAxis.baseUnit = "minute";
                        MargenAxis.renderer.grid.template.disabled = true;
                        //Esto define si es eje izquierdo o derecho
                        MargenAxis.renderer.opposite = true;
                        MargenAxis.numberFormatter = new am4core.NumberFormatter();
                        MargenAxis.numberFormatter.numberFormat = "#'%'";
                        MargenAxis.fontSize = 12;
                        //Eje de Potenciales
                        var PotencialAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        PotencialAxis.renderer.grid.template.disabled = true;
                        PotencialAxis.renderer.labels.template.disabled = true;
                        // Create series
                        //Serie de Venta (Columnas)
                        var VentaSeries = chart.series.push(new am4charts.ColumnSeries());
                        VentaSeries.id = "g1";
                        VentaSeries.dataFields.valueY = "Venta";
                        VentaSeries.dataFields.dateX = "date";
                        VentaSeries.yAxis = VentaAxis;
                        VentaSeries.tooltip.autoTextColor = false;
                        VentaSeries.tooltip.label.fill = am4core.color("#FFFFFF");
                        VentaSeries.tooltipText = "Venta: {valueY.formatNumber('#,###')}";
                        //cambia a notacion por letra
                        //VentaSeries.tooltipText = "Venta: {valueY.formatNumber('#,###.00a')}";
                        VentaSeries.name = "Venta";
                        VentaSeries.columns.template.fillOpacity = 0.7;
                        VentaSeries.columns.template.stroke = am4core.color("#088A8E"); // red outline
                        VentaSeries.columns.template.fill = am4core.color("#1AB1B4"); // green fill
                        var VentaState = VentaSeries.columns.template.states.create("hover");
                        VentaState.properties.fillOpacity = 0.9;
                        //Serie de Margen (duracion)
                        var MargenSeries = chart.series.push(new am4charts.LineSeries());
                        MargenSeries.id = "g3";
                        MargenSeries.dataFields.valueY = "Margen";
                        MargenSeries.dataFields.dateX = "date";
                        MargenSeries.yAxis = MargenAxis;
                        MargenSeries.name = "Margen";
                        MargenSeries.strokeWidth = 2;
                        MargenSeries.tooltipText = "Margen: {valueY}%";
                        MargenSeries.stroke = am4core.color("#44495B");
                        var MargenBullet = MargenSeries.bullets.push(new am4charts.CircleBullet());
                        MargenBullet.circle.fill = am4core.color("#0AC282");
                        MargenBullet.circle.strokeWidth = 1;
                        //Esta linea define el radio del circulo, puede ser una variable o un nominal
                        MargenBullet.circle.propertyFields.radius = "4";
                        var MargenState = MargenBullet.states.create("hover");
                        MargenState.properties.scale = 1.2;
                        //var MargenBullet = MargenSeries.bullets.push(new am4charts.CircleBullet());
                        //var MargenRectangle = MargenBullet.createChild(am4core.Rectangle);
                        //MargenBullet.horizontalCenter = "middle";
                        //MargenBullet.verticalCenter = "middle";
                        //MargenBullet.width = 7;
                        //MargenBullet.height = 7;
                        //MargenRectangle.width = 7;
                        //MargenRectangle.height = 7;
                        //Serie de potencial
                        var PotencialSeries = chart.series.push(new am4charts.LineSeries());
                        PotencialSeries.id = "g2";
                        PotencialSeries.dataFields.valueY = "Potencial";
                        PotencialSeries.dataFields.dateX = "date";
                        PotencialSeries.yAxis = VentaAxis;
                        PotencialSeries.name = "Potencial";
                        PotencialSeries.strokeWidth = 2;
                        //Piteate el townname
                        //PotencialSeries.tooltipText = "Potencial: {valueY} ({townName})";
                        PotencialSeries.tooltipText = "Potencial: {valueY} ({Brecha}% de Brecha)";
                        PotencialSeries.stroke = am4core.color("#C70000");
                        //Color de texto en tooltyp
                        PotencialSeries.tooltip.autoTextColor = false;
                        PotencialSeries.tooltip.label.fill = am4core.color("#FFFFFF");
                        //Color de fondo en tooltyp
                        PotencialSeries.tooltip.getFillFromObject = false;
                        PotencialSeries.tooltip.background.fill = am4core.color("#C70000");
                        /*Esta parte lo que hace es generar circulos que con cierto tamaño en la serie
                        var PotencialBullet = PotencialSeries.bullets.push(new am4charts.CircleBullet());
                        PotencialBullet.circle.fill = am4core.color("#fff");
                        PotencialBullet.circle.strokeWidth = 4;
                        /*Esta linea define el radio del circulo, puede ser una variable o un nominal*/
                        /*
                        PotencialBullet.circle.propertyFields.radius = "4";
                        */
                        /*
                        var PotencialState = PotencialBullet.states.create("hover");
                        PotencialState.properties.scale = 1.2;
                        */
                        /*
                        var PotencialLabel = PotencialSeries.bullets.push(new am4charts.LabelBullet());
                        PotencialLabel.label.text = "{townName2}";
                        PotencialLabel.label.horizontalCenter = "left";
                        PotencialLabel.label.dx = 14;
                        */
                        // Add legend
                        chart.legend = new am4charts.Legend();
                        chart.legend.fontSize = 12;
                        // Add cursor
                        chart.cursor = new am4charts.XYCursor();
                        chart.cursor.fullWidthLineX = true;
                        chart.cursor.xAxis = dateAxis;
                        chart.cursor.lineX.strokeOpacity = 0;
                        chart.cursor.lineX.fill = am4core.color("#000");
                        chart.cursor.lineX.fillOpacity = 0.1;

                    }); // end am4core.ready()
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener grafico cliente', code, text);
            }
        });
    }
</script>