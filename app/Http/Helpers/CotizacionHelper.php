<?php


namespace App\Http\Helpers;

use App\EPDO\MySQL\PricingServer\PortalEjecutivo\PortalEjUsuariosEPDO;
use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\CotizacionController;
use App\PDO\Oracle\DMVentas\MaEmpresaPDO;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use Illuminate\Http\Request;

class CotizacionHelper
{
    public function validarCotizacion(Request $request)
    {
        $update = $request->get('update', false) ? $request->get('update') : false;

        $codemp = $request->get('cod_emp', false) ? $request->get('cod_emp') : 3;
        if (!MaEmpresaPDO::existsCompanyByCod($codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con código [" . $codemp . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        $vendedor = $request->get('vendedor', null);
        if (!isset($vendedor)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [vendedor]");
            return $respuesta->returnToJson();
        }

        $usuario = PortalEjUsuariosEPDO::where('codusu', '=', $vendedor)
            ->where('estado', '=', 1)
            ->first();

        if (count($usuario) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró vendedor en BBDD");
            return $respuesta->returnToJson();
        }

        $rutcli = $request->get('rutcli', null);
        if (!isset($rutcli)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [rutcli]");
            return $respuesta->returnToJson();
        }
        if (!preg_match("/^[0-9]+$/", $rutcli)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("El atributo [rutcli] no es valido");
            return $respuesta->returnToJson();
        }

        if (!EnClientePDO::existsByRutcli($rutcli, $codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con rut [" . $rutcli . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        $productos = $request->get('productos', null);
        if (!isset($productos)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró arreglo [productos]");
            return $respuesta->returnToJson();
        }
        if (!is_array($productos) || count($productos) == 0) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("El arreglo [productos] esta vacio");
            return $respuesta->returnToJson();
        }
        $i = 0;
        foreach ($productos as $producto) {
            if (!array_key_exists('codpro', $producto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [codpro]");
                return $respuesta->returnToJson();
            }

            if (!array_key_exists('costo', $producto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [costo]");
                return $respuesta->returnToJson();
            }

            if (!preg_match("/^[0-9]+$/", $producto['costo'])) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [costo]");
                return $respuesta->returnToJson();
            }

            if (!array_key_exists('precio', $producto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [precio]");
                return $respuesta->returnToJson();
            }

            if (!preg_match("/^[0-9]+$/", $producto['precio'])) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [precio]");
                return $respuesta->returnToJson();
            }

            if (!array_key_exists('cantidad', $producto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [cantidad]");
                return $respuesta->returnToJson();
            }

            if (!preg_match("/^[0-9]+$/", $producto['cantidad'])) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [cantidad]");
                return $respuesta->returnToJson();
            }
            $i++;
        }
        $controlador = new CotizacionController();
        return $controlador->validarCotizacion($update, $usuario, $rutcli, $productos, $codemp);
    }

    public function guardarCotizacion(Request $request)
    {
        $temporal = $request->get('temporal', false);
        if($temporal){
            $productos = $request->get('productos', null);
            Session::put('cotizacion', $productos);
        }
        else {

            $codemp = $request->get('cod_emp', false) ? $request->get('cod_emp') : 3;
            if (!MaEmpresaPDO::existsCompanyByCod($codemp)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(202)->setResponse("La empresa con código [" . $codemp . "] no se encontró en la base de datos");
                return $respuesta->returnToJson();
            }

            $vendedor = $request->get('vendedor', null);
            if (!isset($vendedor)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró atributo [vendedor]");
                return $respuesta->returnToJson();
            }

            $usuario = PortalEjUsuariosEPDO::where('codusu', '=', $vendedor)
                ->where('estado', '=', 1)
                ->first();

            if (count($usuario) == 0) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró vendedor en BBDD");
                return $respuesta->returnToJson();
            }

            $observacion = $request->get('observacion', null);
            if (!isset($observacion)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró atributo [observacion]");
                return $respuesta->returnToJson();
            }

            $contacto = $request->get('contacto', null);
            if (!isset($contacto)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró atributo [contacto]");
                return $respuesta->returnToJson();
            }

            $rutcli = $request->get('rutcli', null);
            if (!isset($rutcli)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró atributo [rutcli]");
                return $respuesta->returnToJson();
            }
            if (!preg_match("/^[0-9]+$/", $rutcli)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El atributo [rutcli] no es valido");
                return $respuesta->returnToJson();
            }

            $cencos = $request->get('cencos', null);
            if (!isset($cencos)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró atributo [cencos]");
                return $respuesta->returnToJson();
            }

            if (!preg_match("/^[0-9]+$/", $cencos)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El atributo [cencos] no es valido");
                return $respuesta->returnToJson();
            }

            if (!EnClientePDO::existsByRutcli($rutcli, $codemp)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(202)->setResponse("La empresa con rut [" . $rutcli . "] no se encontró en la base de datos");
                return $respuesta->returnToJson();
            }

            if (!EnClientePDO::existsCencosByRutcli($rutcli, $cencos, $codemp)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(202)->setResponse("La empresa con rut [" . $rutcli . "] no tiene el cencos [" . $cencos . "] en la base de datos");
                return $respuesta->returnToJson();
            }

            $productos = $request->get('productos', null);
            if (!isset($productos)) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("No se encontró arreglo [productos]");
                return $respuesta->returnToJson();
            }
            if (!is_array($productos) || count($productos) == 0) {
                $respuesta = new ResponseFormatt();
                $respuesta->setCode(201)->setResponse("El arreglo [productos] esta vacio");
                return $respuesta->returnToJson();
            }
            $i = 0;
            foreach ($productos as $producto) {
                if (!array_key_exists('codpro', $producto)) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [codpro]");
                    return $respuesta->returnToJson();
                }

                if (!array_key_exists('precio', $producto)) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [precio]");
                    return $respuesta->returnToJson();
                }

                if (!preg_match("/^[0-9]+$/", $producto['precio'])) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [precio]");
                    return $respuesta->returnToJson();
                }

                if (!array_key_exists('costo', $producto)) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [costo]");
                    return $respuesta->returnToJson();
                }

                if (!preg_match("/^[0-9]+$/", $producto['costo'])) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [costo]");
                    return $respuesta->returnToJson();
                }

                if ($producto['costo'] > $producto['precio']) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(222)->setResponse("El arreglo [productos][" . $i . "] contiene margen negativo");
                    return $respuesta->returnToJson();
                }

                if (!array_key_exists('cantidad', $producto)) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene el atributo [cantidad]");
                    return $respuesta->returnToJson();
                }

                if (!preg_match("/^[0-9]+$/", $producto['cantidad'])) {
                    $respuesta = new ResponseFormatt();
                    $respuesta->setCode(201)->setResponse("El arreglo [productos][" . $i . "] no contiene un número en el atributo [cantidad]");
                    return $respuesta->returnToJson();
                }
                $i++;
            }
            $datos_ejecutivo = MaUsuarioPDO::getUserByUserid($usuario->codusu);
            $controlador = new CotizacionController();
            return $controlador->guardarCotizacion($datos_ejecutivo, $rutcli, $cencos, $observacion, $contacto, $productos, $codemp);
        }
    }
}