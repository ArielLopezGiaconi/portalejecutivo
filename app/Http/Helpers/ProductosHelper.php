<?php


namespace App\Http\Helpers;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\ProductosController;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use App\PDO\Oracle\DMVentas\MaEmpresaPDO;
use Illuminate\Http\Request;

class ProductosHelper
{
    public function listarProductos(Request $request)
    {
        $search_string = $request->get('search_string', null);
        $type_search = $request->get('type_search', false) ? $request->get('type_search') : 'codpro';
        $fulltext = $request->get('fulltext',false);
        if (!isset($search_string)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [search_string]");
            return $respuesta->returnToJson();
        }

        if ($type_search <> 'codpro' and $type_search <> 'despro') {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("El atributo [type_search] no es valido");
            return $respuesta->returnToJson();
        }

        $search_string = strtoupper(trim($search_string));
        $type_search = strtoupper(trim($type_search));
        $controlador = new ProductosController();
        return $controlador->listarProductos($search_string, $type_search, $fulltext);
    }

    public function productoEspecifico(Request $request)
    {
        echo 'llega al controlador';
        $codemp = $request->get('cod_emp', false) ? $request->get('cod_emp') : 3;
        if (!MaEmpresaPDO::existsCompanyByCod($codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con código [" . $codemp . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        $rutcli = $request->get('rutcli', null);
        if (!isset($rutcli)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [rutcli]");
            return $respuesta->returnToJson();
        }
        /*if (!is_int($rutcli)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("El atributo [rutcli] no es valido");
            return $respuesta->returnToJson();
        }*/

        if (!EnClientePDO::existsByRutcli($rutcli, $codemp)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(202)->setResponse("La empresa con rut [" . $rutcli . "] no se encontró en la base de datos");
            return $respuesta->returnToJson();
        }

        $cod_producto = $request->get('cod_producto', null);
        if (!isset($cod_producto)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [cod_producto]");
            return $respuesta->returnToJson();
        }
        $tipo_busqueda = $request->get('tipo_busqueda', null);
        if (!isset($tipo_busqueda)) {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("No se encontró atributo [tipo_busqueda]");
            return $respuesta->returnToJson();
        }

        $cod_producto = strtoupper($cod_producto);
        $tipo_busqueda = strtoupper($tipo_busqueda);
        if ($tipo_busqueda != 'CODPRO' and $tipo_busqueda != 'CODSAP') {
            $respuesta = new ResponseFormatt();
            $respuesta->setCode(201)->setResponse("al atributo [tipo_busqueda] no es válido");
            return $respuesta->returnToJson();
        }

        $controlador = new ProductosController();
        return $controlador->listarProductoEspecifico($rutcli, $cod_producto, $tipo_busqueda);
    }
}