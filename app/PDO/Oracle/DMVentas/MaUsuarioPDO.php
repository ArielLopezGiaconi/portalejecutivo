<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 22/06/2018
 * Time: 10:05
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\MaUsuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MaUsuarioPDO extends Model
{
    public static function existsUserByUserid($in_user) {
        $sql = "SELECT COUNT(*) AS existe FROM ma_usuario WHERE userid = :user_id";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'user_id' => $in_user
        ]);

        return ($resultado != null && $resultado[0]->existe > 0)
            ? true : false;
    }

	public static function updateMail($userid, $mailEjecutivo) {
        $sql = 'UPDATE MA_USUARIO
				SET mail01 = :mailejecutivo
				where userid = :userid';

        $resultado = DB::connection('oracle_dmventas')->statement($sql, [
            'mailejecutivo' => $mailEjecutivo,
			'userid' => $userid
        ]);

        return true;
    }

    public static function getUserByUserid($in_user) {
        $sql = "SELECT rutusu, userid, nombre, apepat, mail01, nvl(codcnl,800) as codcnl FROM ma_usuario WHERE userid = :user_id";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'user_id' => $in_user
        ]);

        $arrayReturn = null;
        if($resultado && count($resultado) > 0) {
            $registro = $resultado[0];
            $arrayReturn = array(
                'rut' => $registro->rutusu,
                'userid' => $registro->userid,
                'nombre' => $registro->nombre . ' ' . $registro->apepat,
                'email' => $registro->mail01,
                'grupo_venta' => $registro->codcnl
            );
        }

        return  $arrayReturn;
    }

    public static function getUserByUseridPass($in_userid, $in_password, $in_codemp = 3) {
        $sql = "SELECT * FROM ma_usuario WHERE userid = :user_id AND clave = :password AND codemp = :cod_emp";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'user_id' => $in_userid,
            'password' => $in_password,
            'cod_emp' => $in_codemp
        ]);

        if($resultado && count($resultado) > 0) {
            $usuario = new MaUsuario($resultado[0]);
            return new Collection($usuario);
        }

        return null;
    }

    public static function getUserByUseridOnly($in_userid) {
        $sql = "SELECT * FROM ma_usuario WHERE userid = :user_id";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'user_id' => $in_userid
        ]);

        if($resultado && count($resultado) > 0) {
            $usuario = new MaUsuario($resultado[0]);
            return new Collection($usuario);
        }

        return null;
    }
	
	public static function getDerechos($token){
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
  			CURLOPT_URL => getenv("URL_API_AUTH")."permisos",
  			CURLOPT_RETURNTRANSFER => true,
  			CURLOPT_ENCODING => '',
  			CURLOPT_MAXREDIRS => 10,
  			CURLOPT_TIMEOUT => 0,
  			CURLOPT_FOLLOWLOCATION => true,
  			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  			CURLOPT_CUSTOMREQUEST => 'GET',
  			CURLOPT_HTTPHEADER => array(
    			'apikey: '. $token
  			),
		));

		$response = curl_exec($curl);
		$arrDerechos = array();

		curl_close($curl);
		
		$result = json_decode($response);
		
		if(isset($result->response) && $result->code == 200){ //convertimos en arreglo asociativo para hacer mas rapida la comparacion despues
			/*foreach($result->response as $derecho){
				$arrDerechos[$derecho->codder] = $derecho->descripcion;
			}*/
			
			return $result->response;
		}
		else{
			return false;
		}
	}
	
	public static function getApiKey($user, $pass){
		$curl = curl_init();

		curl_setopt_array($curl, array(
  			CURLOPT_URL => getenv("URL_API_AUTH")."apikey?usuario=".urlencode($user)."&contrasena=".urlencode($pass),
  			CURLOPT_RETURNTRANSFER => true,
  			CURLOPT_ENCODING => '',
  			CURLOPT_MAXREDIRS => 10,
  			CURLOPT_TIMEOUT => 0,
  			CURLOPT_FOLLOWLOCATION => true,
  			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  			CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);
		$arrDerechos = array();

		curl_close($curl);
		$result = json_decode($response);
		if(isset($result->response) && $result->code == 200){ //retornamos token al controlador
			return $result->response;
		}
		else{
			return false;
		}
	}

}