<?php


namespace App\Http\Controllers;

use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\MaProductPDO;

class ProductosController extends Controller
{
    public function listarProductos($search_string, $type_search, $fulltext)
    {
        $productos = MaProductPDO::getAllProducts($search_string, $type_search, $fulltext);
        $response = new ResponseFormatt();
        $response->setCode(200)
            ->setResponse($productos);

        return $response->returnToJson();
    }

    public function listarProductoEspecifico($rutcli, $codpro, $tipo_busqueda)
    {
        $producto = MaProductPDO::getProductByType($rutcli, $codpro, $tipo_busqueda);
        $response = new ResponseFormatt();
        if ($producto) {
            $response->setCode(200)
                ->setResponse($producto);
        } else {
            $response->setCode(201)
                ->setResponse("Producto no existe en BBDD");
        }
        return $response->returnToJson();
    }

    public function getStockSap($codpro)
    {
        $url = "api.dimerc.cl/sap/stock/individual/product/" . $codpro;
        $username = "api_dimerc";
        $password = "D1m3rcl4bs";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        if ($result->code == 200) {
            if ($result->response != null) {
                return $result->response->stock;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}