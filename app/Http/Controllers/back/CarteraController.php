<?php

namespace App\Http\Controllers\back;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\Controller;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Illuminate\Http\Response;

class CarteraController extends Controller
{
    public function loadDocumentAction(Request $request)
    {
        //$documentId = $request->get('documento');
        $documentId = $_GET['valor'];;
        $respuesta = new ResponseFormatt();
        //$documentId = $this->getRequest()->getParam("documentId"); ///debería ser el ID del documento, acá usa un método magento, creo que es el número de documento.

        $url = "http://10.10.185.64/docuws/docuws.asmx";
        $xml = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ConsultaPorCodigoDocured xmlns="http://Alniti/"><Usuario>wscob_con</Usuario><Password>WSCOB</Password><CodigoDocured>' . $documentId . '</CodigoDocured></ConsultaPorCodigoDocured></soap:Body></soap:Envelope>';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: text/xml; charset=utf-8",
            "SOAPAction: http://Alniti/ConsultaPorCodigoDocured"
        ));
        $response = curl_exec($ch);
        
        $response = str_replace("<soap:Body>", "", $response);
        $response = str_replace("</soap:Body>", "", $response);
        $parser = simplexml_load_string($response);
        $result = (string) $parser->ConsultaPorCodigoDocuredResponse->ConsultaPorCodigoDocuredResult->string[1];
        // DECODIFICANDO
        $pdf_decoded = base64_decode($result);
        // GENERARDO CABECERAS
        header("Content-Description: File Transfer");
        header("Content-Type: application/pdf");
        header("Content-Disposition: attachment; filename=".$documentId.".pdf");
        header("Content-Transfer-Encoding: binary");
        header("Expires: 0");
        header("Cache-Control: must-revalidate");
        header("Pragma: public");
        header("Content-Length: " . strlen($pdf_decoded));
        // LIMPIA LA SALIDA
        ob_clean();
        flush();
        echo $pdf_decoded;
    }
    
    public function getfocosCartera() {
        $usuario = Session::get('usuario');
        
        $resultado = PortalEjUsuariosPDO::getfocosCartera();
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getfocosKarim(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $campana = $request->get('valor');
        
        $resultado = PortalEjUsuariosPDO::getfocosKarim($rutusu, $campana);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getFechasAcciones(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');

        //$resultado = oraclePDO::getFechasAcciones($codusu, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($fecha);
        return $respuesta->returnToJson();
    }

    public function getAcciones(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');
        $rutusu = $usuario->get('rutusu');
        $activo = $request->get('activo');
        $permanente = $request->get('permanente');
        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 2, 1);
        
        $resultado = oraclePDO::getAcciones($resultado, $fecha, $activo, $permanente);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getRut(Request $request) {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $rut = $request->get('rutcli');
        
        $resultado = PortalEjUsuariosPDO::getRut($rut, $rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getDocumentosRelacionados(Request $request) {
        $usuario = Session::get('usuario');
        
        $numnvt = $request->get('numnvt');
        
        $resultado = oraclePDO::getDocumentosRelacionados($numnvt);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getAccionesQ(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');
        $fecha = $request->get('fecha');
        $rutusu = $usuario->get('rutusu');
        $resultado = PortalEjUsuariosPDO::getpartner($codusu, $rutusu, 2, 1);
        
        $resultado = oraclePDO::getAccionesQ($resultado, $fecha);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getCarteraEjecutivo(Request $request)
    {
        if (!Session::get('auth')) {
            return redirect()->to('login');
        } 
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $carteraData = PortalEjUsuariosPDO::getCarteraEjecutivo($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($carteraData);
        return $respuesta->returnToJson();
    }

    public function getCartera(Request $request)
    {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        $clasificacion = $request->get('clasificacion');
        $segmento = $request->get('segmento');
        $credito = $request->get('credito');
        $rango = $request->get('rango');
        $gestMes = $request->get('gestMes');
        $deuda = $request->get('deuda');
        $fechaProximaAccion = $request->get('fechaProximaAccion');
        $brecha = $request->get('brecha');
        $venta = $request->get('venta');
        $cartera = $request->get('cartera');
        $foco = $request->get('foco');
        $DCartera = $request->get('DCartera');
        $carteraData = PortalEjUsuariosPDO::getCartera($rutusu, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $cartera, $deuda, $foco, $DCartera);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($carteraData);
        return $respuesta->returnToJson();
    }
}