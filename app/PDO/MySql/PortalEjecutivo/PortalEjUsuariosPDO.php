<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 31-05-2019
 * Time: 13:15
 */

namespace App\PDO\MySql\PortalEjecutivo;

use App\Entities\MySql\Dimerc\DimProdOpl;
use Illuminate\Database\Eloquent\Model;
use App\PDO\Oracle\DMVentas\oraclePDO;
use Mockery\Exception;
use DB;

class PortalEjUsuariosPDO extends Model
{
    public static function isExistsUserActive($in_userid, $validate_active = true)
    {
        $sql = "SELECT COUNT(*) AS existe FROM "
            . " portal_ej_usuarios"
            . " WHERE codusu = :userid"
            . (($validate_active) ? " AND estado = 1" : "");

        $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
            'userid' => $in_userid
        ]);

        return (count($resultado) > 0 && $resultado[0]->existe == '1');
    }

    public static function ConsultaCartera($in_userid)
    {
        $sql = "SELECT COUNT(*) AS existe FROM "
            . " portal_ej_usuarios"
            . " WHERE codusu = :userid"
            . (($validate_active) ? " AND estado = 1" : "");

        $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
            'userid' => $in_userid
        ]);

        return (count($resultado) > 0 && $resultado[0]->existe == '1');
    }

    public static function getUserActive($in_userid, $validate_active = true)
    {
        $sql = "SELECT COUNT(*) AS existe FROM "
            . " portal_ej_usuarios"
            . " WHERE codusu = :userid"
            . (($validate_active) ? " AND estado = 1" : "");

        $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
            'userid' => $in_userid
        ]);

        return (count($resultado) > 0 && $resultado[0]->existe == '1');
    }

    public static function getVentaEjecutivo($userid, $rutusu)
    {
        try {
            $sql = "select * from portal_ejecutivo.portal_ej_vta_realizada_por_ejecutivo
            where userid = :userid
            and rutusu = :rutusu";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'userid' => $userid,
                'rutusu' => $rutusu
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getMeta($rutusu)
    {
        try {
            
            $sql = "select
                    meta,venta_ejecutivo,cumplimiento
                    from portal_ej_metas_ejecutivos
                    where periodo in
                    (
                    select
                    max(periodo)
                    from portal_ej_metas_ejecutivos
                    )
                    and codven=" . $rutusu;
                    
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado[0];

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getDinero($rutusu)
    {
        try {
            
            $sql = "select codven, venta_total, venta_boleta, venta_antofagasta, venta_cajas, porcentaje_web from dashboard.pri_dashboard_ki_ejecutivos where codven='" . $rutusu . "'";
                    
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado[0];

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getPotencial($rutusu)
    {
        try {

            $sql = "select																									
            sum(potencial)        as potencial_a_la_fecha,																									
            round((sum(promedio_4_meses)/sum(potencial))*100,2) as per_abarcado_potencial																									
            from portal_ej_new_gestion_cartera																									
            where rutven=:rutven";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'rutven' => $rutusu
            ]);

            return $resultado[0];

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function registro($usuario, $moduleid, $actionid, $oldvalue, $newvalue, $updaterecord)
    {
        try {

            $sql = "insert into portal_ej_bitacora (userid, regdate, moduleid,actionid,oldvalue,newvalue,updaterecord,source)
            values('" . $usuario . "', sysdate(), " . $moduleid . "," . $actionid . ",'" . $oldvalue . "','" . $newvalue . "','" . $updaterecord . "','" . getenv("DATABASE_ENV") . "')";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->statement($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return 'ok';

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getEstadisticasDia($grupo)
    {
        try {

            $sql = "select
                    nombre,
                    ruts_gestionados_en_el_dia        as  clientes_ruts_gestionados,
                    notas_q_ruts                      as  q_clientes,
                    notas_q_notas                     as  q_notas_emitidas,
                    metaventa                         as  meta_venta_del_dia,
                    notas_valorizado                  as  valorizado_notas,
                    per_cumplimiento                  as  cumplimiento,
                    cotizciones_q_ruts               as  q_clientes_cotizados,
                    cotizaciones_q                    as  q_cotizaciones_emitidas,
                    cotizaciones_valorizado           as  valorizado_cotizaciones,
                    web_q_clientes                    as  q_clientes_web,
                    web_q_ordenes                     as  ordenes_web_sin_procesar,
                    web_valorizado                    as  valorizado
                    from reporteria_lideres_estadisticas_del_dia
                    where equipo=" . $grupo . "
                      and nombre <> 'TOTAL'
                    order by notas_valorizado desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getEstadisticasDiaTotales($grupo)
    {
        try {

            $sql = "select
                    nombre,
                    ruts_gestionados_en_el_dia        as  clientes_ruts_gestionados,
                    notas_q_ruts                      as  q_clientes,
                    notas_q_notas                     as  q_notas_emitidas,
                    metaventa                         as  meta_venta_del_dia,
                    notas_valorizado                  as  valorizado_notas,
                    per_cumplimiento                  as  cumplimiento,
                    cotizciones_q_ruts               as  q_clientes_cotizados,
                    cotizaciones_q                    as  q_cotizaciones_emitidas,
                    cotizaciones_valorizado           as  valorizado_cotizaciones,
                    web_q_clientes                    as  q_clientes_web,
                    web_q_ordenes                     as  ordenes_web_sin_procesar,
                    web_valorizado                    as  valorizado
                    from reporteria_lideres_estadisticas_del_dia
                    where equipo=" . $grupo . "
                      and nombre = 'TOTAL'
                    order by notas_valorizado desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getEstadisticasMes($grupo)
    {
        try {

            $sql = "select
            nombre,
            q_ruts_facturados,
            venta_facturada as facturado_a_la_fecha,
            metaventa_a_la_fecha,
            per_cumplimiento_a_la_fecha,
            proyeccion_de_cierre,
            cumplimiento_cierre
            from reporteria_lideres_estadisticas_del_mes
                    where equipo=" . $grupo . "
                      and nombre <> 'TOTAL'
                      order by venta_facturada desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getEstadisticasMesTotales($grupo)
    {
        try {

            $sql = "select
            nombre,
            q_ruts_facturados,
            venta_facturada as facturado_a_la_fecha,
            metaventa_a_la_fecha,
            per_cumplimiento_a_la_fecha,
            proyeccion_de_cierre,
            cumplimiento_cierre
            from reporteria_lideres_estadisticas_del_mes
                    where equipo=" . $grupo . "
                      and nombre = 'TOTAL'
                      order by venta_facturada desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCarteraMes($grupo)
    {
        try {

            $sql = "select nombre,
                    ruts_en_cartera,
                    q_registros,
                    q_clientes,
                    q_registros_gestionados,
                    q_clientes_gestionados,
                    per_cartera_gestionada,
                    clientes_a_perder,
                    clientes_nuevos,
                    ruts_facturados,
                    per_cartera_facturada
                    from reporteria_lideres_cartera_mes
                    where equipo=" . $grupo . "
                      and nombre <> 'TOTAL'
                      order by q_clientes_gestionados desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCarteraMesTotales($grupo)
    {
        try {

            $sql = "select nombre,
                    ruts_en_cartera,
                    q_registros,
                    q_clientes,
                    q_registros_gestionados,
                    q_clientes_gestionados,
                    per_cartera_gestionada,
                    clientes_a_perder,
                    clientes_nuevos,
                    ruts_facturados,
                    per_cartera_facturada
                    from reporteria_lideres_cartera_mes
                            where equipo=" . $grupo . "
                            and nombre = 'TOTAL'
                      order by q_clientes_gestionados desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getseguimientoNota($grupo)
    {
        try {

            $sql = "select nombre,
                    q_notas,
                    val_notas,
                    q_nv_facturadas,	
                    val_nv_facturadas,
                    per_nv_facturadas,
                    q_nv_entregadas,	
                    val_nv_entregadas,
                    per_nv_entregadas,
                    q_nv_preparacion,
                    val_nv_preparacion,
                    per_nv_preparacion,
                    q_nv_pendiente_stock,	
                    val_nv_pendiente_stock,
                    per_nv_pendiente_stock
                    from reporteria_lideres_notas_de_venta
                            where equipo=" . $grupo . "
                            and nombre <> 'TOTAL'
                    order by val_notas desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getseguimientoNotaTotales($grupo)
    {
        try {

            $sql = "select nombre,
                    q_notas,
                    val_notas,
                    q_nv_facturadas,	
                    val_nv_facturadas,
                    per_nv_facturadas,
                    q_nv_entregadas,	
                    val_nv_entregadas,
                    per_nv_entregadas,
                    q_nv_preparacion,
                    val_nv_preparacion,
                    per_nv_preparacion,
                    q_nv_pendiente_stock,	
                    val_nv_pendiente_stock,
                    per_nv_pendiente_stock
                    from reporteria_lideres_notas_de_venta
                            where equipo=" . $grupo . "
                            and nombre = 'TOTAL'
                    order by val_notas desc";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            DB::connection('mysql_portal_ejecutivo')->commit();

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getruts($rutusu, $cartera)
    {
        try {
            if ($cartera == '1') {
                $ruts = '=' . $rutusu;
            }elseif ($cartera == '2') {
                $ruts = "";
                $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                    'rutven' => $rutusu,
                    'rutven2' => $rutusu,
                ]);

                foreach ($resultado as $rut) {
                    $ruts = $ruts . ',' . $rut->partner;
                }
                
                $ruts = 'in(' . substr($ruts, 1, strlen($ruts)) . ') ';

            }elseif ($cartera == '3') {
                $ruts = "";
                $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                    'rutven' => $rutusu,
                    'rutven2' => $rutusu,
                ]);
                
                foreach ($resultado as $rut) {
                    $ruts = $ruts . ',' . $rut->partner;
                }
                
                $ruts = 'in(' . $rutusu . $ruts . ') ';

            }

            return $ruts;
            
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotas($rutusu, $busqueda, $fechaDesde, $fechaHasta, $tipo, $cartera, $status)
    {
        try {
            $ruts = PortalEjUsuariosPDO::getruts($rutusu, $cartera);
            
            if ($tipo == 1) {
                $sql = "select
                numnvt,
                fecha_creac,
                fecha_bodega,
                rutcli,
                cencos,
                razons,
                totnet,
                round(totnet*0.19,0) as iva,
                totnet+round(totnet*0.19,0) as total,
                margen,
                estado_nota,
                orden_de_compra,
                ejecutivo
                from portal_ej_datalle_notas_no_nulas
                where codven " . $ruts . "
                  and rutcli = " . $busqueda . "
                  and fecha_creac between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if ($status <> 'null'){
                    $sql = $sql . "and estado_nota = '" . $status . "'";
                  }
                $sql = $sql . "order by fecha_creac desc";
            }elseif ($tipo == 2) {
                $sql = "select
                numnvt,
                fecha_creac,
                fecha_bodega,
                rutcli,
                cencos,
                razons,
                totnet,
                round(totnet*0.19,0) as iva,
                totnet+round(totnet*0.19,0) as total,
                margen,
                estado_nota, 
                orden_de_compra,
                ejecutivo
                from portal_ej_datalle_notas_no_nulas
                where codven " . $ruts . "
                  and razons like '%" . $busqueda . "%'
                  and fecha_creac between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if ($status <> 'null'){
                    $sql = $sql . "and estado_nota = '" . $status . "'";
                  }
                $sql = $sql . "order by fecha_creac desc";
            }elseif ($tipo == 0) {
                $sql = "select
                numnvt,
                fecha_creac,
                fecha_bodega,
                rutcli,
                cencos,
                razons,
                totnet,
                round(totnet*0.19,0) as iva,
                totnet+round(totnet*0.19,0) as total,
                margen,
                estado_nota, 
                orden_de_compra,
                ejecutivo
                from portal_ej_datalle_notas_no_nulas
                where codven " . $ruts . "
                  and fecha_creac between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if ($status <> 'null'){
                    $sql = $sql . " and estado_nota = '" . $status . "'";
                  }
                $sql = $sql . " order by date_format(fecha_creac,'%d-%m-%Y') desc";
            }
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getRut($rut, $rutusu)
    {
        try {
            $sql = 'select razons,
                    rutcli,
                    id,
                    ejecutivo, 
                    segmento, 
                    dias_restantes, 
                    ultima_gestion_id, 
                    DATE_FORMAT(fecha_ultima_gestion_id, "%d-%m-%Y") as fecha_ultima_gestion_id,
                    clasificacion,
                    potencial,
                    analisis_plazos,
                    dias_nota_rut
                    from portal_ej_new_gestion_cartera
                    where rutcli = ' . $rut . '
                      and rutven <> ' . $rutusu;

            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getfocosCliente($rutcli, $rutusu)
    {
        try {
           
            $sql = "select 
            linea, foco 
            from portal_ej_focos_por_periodo 
            where rutcli = " . $rutcli . "
              and rutven = " . $rutusu . "
            group by foco";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getfocosCartera()
    {
        try {
           
            $sql = "select
            distinct(foco) as foco
            from portal_ejecutivo.portal_ej_focos_por_periodo
            where periodo_analisis=str_to_date(date_format(curdate(),'%Y%m01'),'%Y%m%d')
            order by foco";
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getfocosKarim($rutusu, $campaña)
    {
        try {
           
            $sql = "select
            format(ifnull(credito,0),0) as credito,
            status_credito,
            califica_aseo,
            califica_cafeteria,
            califica_libreria,
            califica_papel,
            califica_seguridad,
            califica_tecnologia,
            califica_tissue,
            ejecutivo,
            equipo,
            insercion,
            lineas_que_califican_cross,
            format(potencial,0) as potencial,
            razons,
            rutcli,
            rutven,
            segmento,
            format(vta_6_meses_aseo,0) as vta_6_meses_aseo,
            format(vta_6_meses_cafeteria,0) as vta_6_meses_cafeteria,
            format(vta_6_meses_libreria,0) as vta_6_meses_libreria,
            format(vta_6_meses_papel,0) as vta_6_meses_papel,
            format(vta_6_meses_seguridad,0) as vta_6_meses_seguridad,
            format(vta_6_meses_tecnologia,0) as vta_6_meses_tecnologia,
            format(vta_6_meses_tissue,0) as vta_6_meses_tissue,
            format(vta_6_meses_total,0) as vta_6_meses_total,
            format(vta_meses_actual_aseo,0) as vta_meses_actual_aseo,
            format(vta_meses_actual_cafeteria,0) as vta_meses_actual_cafeteria,
            format(vta_meses_actual_libreria,0) as vta_meses_actual_libreria,
            format(vta_meses_actual_papel,0) as vta_meses_actual_papel,
            format(vta_meses_actual_seguridad,0) as vta_meses_actual_seguridad,
            format(vta_meses_actual_tecnologia,0) as vta_meses_actual_tecnologia,
            format(vta_meses_actual_tissue,0) as vta_meses_actual_tissue,
            format(vta_meses_actual_total,0) as vta_meses_actual_total
            from portal_ejecutivo.portal_ej_focos_formato_karim
            where rutven =" . $rutusu . "
              and lineas_que_califican_cross>0 ";
            $sql = $sql . " order by potencial desc";
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function ingresosWeb($ruts)
    {
        try {
            
            $sql = "select
            rutcli,razons,
            ifnull(date_format(ultimo_ingreso,'%d-%m-%Y'),'') as ultimo_ingreso,
            usuario,contacto,apellido,telefono,
            ifnull(date_format(fecha_ultima_nv,'%d-%m-%Y'),'') as fecha_ultima_nv,
            ifnull(date_format(fecha_ultima_orden_web,'%d-%m-%Y'),'') as fecha_ultima_orden_web,
            ifnull(date_format(ultima_orden_web,'%d-%m-%Y'),'') as ultima_orden_web,
            ifnull(date_format(fecha_anotacion,'%d-%m-%Y'),'') as fecha_anotacion,
            usuario_anotacion,anotacion
            from portal_ej_intencion_compra_web
            where ejecutivo " . $ruts . "
            order by ultimo_ingreso desc";
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function ingresosWebQ($codusu)
    {
        try {
            
            $sql = "select
            count(rutcli) cantidad
            from portal_ej_intencion_compra_web
            where ejecutivo='" . $codusu . "'
            order by ultimo_ingreso desc";
            
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getPeriodosComisiones()
    {
        try {
            
            $sql = "select distinct DATE_FORMAT(periodo, '%d/%m/%Y') periodo from portal_ejecutivo.bonos_detalle_comisiones order by periodo desc";
            
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getComisionesPortal($rutusu, $fecha)
    {
        try {
            $sql = "select concat('$',format(sum(a.comision_abc),0)) comision_abc from ( 
                select
                ejecutivo_a_imputar_comision, categoria_comision, round(sum(comision_calculada)) as comision_abc
                from portal_ejecutivo.bonos_detalle_comisiones
                where ejecutivo_a_imputar_comision=" . $rutusu . "
                and periodo='" . $fecha . "'
                group by categoria_comision, ejecutivo_a_imputar_comision
                ) a
                union all 
                select							 					
                concat('$',format(round(sum(comision_final)) - round(sum(comision_calculada)),0)) as Bono_Meta					
                from portal_ejecutivo.bonos_detalle_comisiones					
                where ejecutivo_a_imputar_comision=" . $rutusu . "					
                and periodo='" . $fecha . "'					
                group by ejecutivo_a_imputar_comision
                union all
                select								
                concat('$',format(sum(bono_cross),0)) as bono_cross					
                from portal_ejecutivo.bonos_cumplimiento_cross					
                where dueno_a_la_fecha_de_emision_documento=" . $rutusu . "			
                and periodo='" . $fecha . "'					
                group by dueno_a_la_fecha_de_emision_documento";

            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getComisiones1($rutusu, $fecha)
    {
        try {
            
            $sql = "select					
            a.codven,		
            d.equipo,
            d.usuario,
            d.nombre,
            concat('$',format(ventas_del_mes,0))        as  Ventas,					
            concat('$',format(meta,0))                                        as  Meta_venta,					
            concat('$',format(ventas_asociadas_a_cartera,0))                  as  Venta_de_su_cartera,					
            concat(format(round(cumplimiento*100,1),0),'%')                   as  cumplimiento_meta,					
            concat(format(round(comision/ventas_del_mes*100,1),0),'%')        as  porcentaje_comision,					
            comision                                                          as  comision,					
            concat(format(round((comision_final/comision-1)*100,1),0),'%')    as  porcentaje_incremento,					
            comision_final                                                    as  comision_final,					
            concat('$',format(venta_asociada_cross,0))                        as  venta_asociada_cross,					
            concat('$',format(bono_cross,0))                                  as  bono_cross,					
            concat('$',format(bono_cross+comision_final,0))                   as  total_bonos				
            from					
            (					
              select					
              codven,					
              meta,ventas_del_mes,ventas_asociadas_a_cartera,cumplimiento,tramo,factor_de_suma					
              from portal_ejecutivo.bonos_cumplimiento_meta					
              where codven=" . $rutusu . "					
              and periodo='" . $fecha . "'					
            ) a,					
            (					
              select					
              dueno_a_la_fecha_de_emision_documento as codven,					
              sum(califica) as q_lineas_capturadas,					
              sum(if(califica=1,p0,0)) as venta_asociada_cross,					
              sum(bono_cross) as bono_cross					
              from portal_ejecutivo.bonos_cumplimiento_cross					
              where dueno_a_la_fecha_de_emision_documento=" . $rutusu . "					
              and periodo='" . $fecha . "'					
              group by dueno_a_la_fecha_de_emision_documento					
            ) b,					
            (					
              select					
              ejecutivo_a_imputar_comision as codven,					
              round(sum(comision_calculada)) as comision,					
              round(sum(comision_final)) as comision_final					
              from portal_ejecutivo.bonos_detalle_comisiones					
              where ejecutivo_a_imputar_comision=" . $rutusu . "					
              and periodo='" . $fecha . "'					
              group by ejecutivo_a_imputar_comision					
              ) c, analisis_tristan.ma_usuario d					
              where a.codven=b.codven					
              and a.codven=c.codven		
              and a.codven=d.rutusu";
              //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getComisiones2($rutusu, $fecha)
    {
        try {
             
            $sql = "select
            ejecutivo_a_imputar_comision as codven,
            case categoria_comision
            when 'X' then 'NCR' else categoria_comision
            end categoria_comision,
            concat('$',format(sum(neto),0)) as venta,
            concat('$',format(round(sum(comision_calculada)),0)) as comision,
            concat('$',format(round(sum(comision_final)),0)) as comision_final
            from portal_ejecutivo.bonos_detalle_comisiones
            where ejecutivo_a_imputar_comision=" . $rutusu . "
            and periodo='" . $fecha . "'
            group by ejecutivo_a_imputar_comision,categoria_comision
            order by categoria_comision";
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getDatosCliente($rutcli)
    {
        try {
            
            $sql = "select																																			
                    /*Detalle general*/																																			
                    rutcli,																																			
                    id,																																			
                    razons,																																			
                    segmento,																																			
                    clasificacion,																																			
                    if(dias_restantes<=0,'Disponible para traspaso',dias_restantes) as dias_para_salir_de_cartera,														
                    /*Detalle Credito*/																																			
                    status_credito,																																			
                    credito,																																			
                    credito_disponible,																																			
                    ifnull(deuda_activa+deuda_vencida,0) as deuda,																																			
                    ifnull('SIN MAPEAR','') as forma_de_pago,   /*lo agregare hoy*/																																			
                    ifnull('SIN MAPEAR','') as plazo_de_pago,   /*lo agregare hoy*/																																		
                    /*Promedio de venta*/																																			
                    ifnull(promedio_4_meses,0) as venta_promedio,																																			
                    potencial,																																			
                    rango_potencial,																																			
                    coalesce(potencial,0)-coalesce(venta_actual,0) as brecha_contra_potencial,																																																											
                    /*Cotizaciones y Ntas*/																																			
                    ifnull(sum(mes_actual_monto_cotizado_activo),0) as monto_cotizaciones_activas,																																			
                    ifnull(sum(total_ano_valorizado_notas_no_despachadas),0) as notas_sin_despachar	
                    from portal_ej_new_gestion_cartera																																		
                    where rutcli=" . $rutcli . "																																			
                    ;";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCovidComuna($grupo)
    {
        try {
            
            $sql = "select * from contingencia_covid_comuna
                    where equipo = " . $grupo . "
                    ;";
            
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCotizaciones($rutusu, $busqueda, $fechaDesde, $fechaHasta, $tipo, $cartera, $status)
    {
        try {
            $ruts = PortalEjUsuariosPDO::getruts($rutusu, $cartera);
            
            if ($tipo == 1) {
                $sql = "select
                numcot,
                fecemi,
                fecven,
                rutcli,
                cencos,
                ifnull(razons,'SIN DATO') as razons, 
                ifnull(segmento,'SIN DATO') as segmento,
                monto_cotizado as monto_cotizado,
                round(monto_cotizado*0.19,0) as iva,
                round(monto_cotizado+round(monto_cotizado*0.19,0),0) as total,
                estado, ejecutivo
                from portal_ej_analisis_cotizaciones_resumen
                where codven " . $ruts . " 
                  and rutcli = " . $busqueda . "
                  and fecemi between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if($status <> 'Todas'){
                    $sql = $sql . "and estado = '" . $status . "'";
                  }
                  $sql = $sql . " order by fecemi desc";
            }elseif ($tipo == 2) {
                $sql = "select
                numcot,
                fecemi,
                fecven,
                rutcli,
                cencos,
                ifnull(razons,'SIN DATO') as razons, 
                ifnull(segmento,'SIN DATO') as segmento,
                monto_cotizado as monto_cotizado,
                round(monto_cotizado*0.19,0) as iva,
                round(monto_cotizado+round(monto_cotizado*0.19,0),0) as total,
                estado, ejecutivo
                from portal_ej_analisis_cotizaciones_resumen
                where codven " . $ruts . " 
                  and razons like '%" . $busqueda . "%'
                  and fecemi between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if($status <> 'Todas'){
                    $sql = $sql . "and estado = '" . $status . "'";
                  }
                  $sql = $sql . " order by fecemi desc";
            }elseif ($tipo == 0) {
                $sql = "select
                numcot,
                fecemi,
                fecven,
                rutcli,
                cencos,
                ifnull(razons,'SIN DATO') as razons, 
                ifnull(segmento,'SIN DATO') as segmento,
                monto_cotizado as monto_cotizado,
                round(monto_cotizado*0.19,0) as iva,
                round(monto_cotizado+round(monto_cotizado*0.19,0),0) as total,
                estado, ejecutivo
                from portal_ej_analisis_cotizaciones_resumen
                where codven " . $ruts . " 
                  and fecemi between '" . $fechaDesde . " 00:00:00' and '" . $fechaHasta . " 23:59:59'";
                  if($status <> 'Todas'){
                    $sql = $sql . "and estado = '" . $status . "'";
                  }
                  $sql = $sql . " order by fecemi desc";
            }
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getResumenCartera($rutusu)
    {
        try {
            $sql = "select 																
                    id_concepto,concepto,agrupacion,																
                    sum(clientes_cantidad)          as clientes_cantidad,																
                    sum(clientes_valorizado)        as clientes_valorizado,																
                    sum(registros_cantidaD)         as registros_cantidad,																
                    sum(registros_valorizado)       as registros_valorizado,																
                    sum(total_cantidad)             as total_cantidad,																
                    sum(total_valorizado)           as total_valorizado																
                    from portal_ej_resumen_de_portada																
                    where rutven in 																
                    (" . $rutusu . ") 
                    and id_concepto <> 6
                    group by id_concepto,concepto,agrupacion																
                    order by id_concepto asc,agrupacion asc";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getResumenTotales($rutusu)
    {
        try {
            $sql = "select 																
                    id_concepto,concepto,																
                    sum(clientes_cantidad)          as tot_clientes_cantidad,																
                    sum(clientes_valorizado)        as tot_clientes_valorizado,																
                    sum(registros_cantidaD)         as tot_registros_cantidad,																
                    sum(registros_valorizado)       as tot_registros_valorizado,																
                    sum(total_cantidad)             as tot_total_cantidad,																
                    sum(total_valorizado)           as tot_total_valorizado																
                    from portal_ej_resumen_de_portada																
                    where rutven in 																
                    (" . $rutusu . ") 
                    and id_concepto <> 6
                    group by id_concepto,concepto																
                    order by id_concepto asc";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getVentaCartera($rutusu)
    {
        try {
            $sql = "select																	
            mes,																	
            anio,																	
            venta_cartera,																	
            venta_ejecutivo																	
            from portal_ejecutivo.portal_ej_resumen_ventas_ejecutivos																	
            where codven=" . $rutusu . "																	
            order by conteo asc	";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getRankingEjecutivo($userid)
    {
        try {
            $sql = "select a.ejecutivo, date_format(a.periodo,'%m %Y') as periodo, 
                    date_format(a.periodo, '%Y') as anio, date_format(a.periodo,'%m') as mes, a.ranking, a.concepto
                    from ranking_ejecutivo a,
                    (
                        select 
                        ejecutivo,ranking,max(periodo) as max 
                        from ranking_ejecutivo
                        where ejecutivo = :userid
                        group by ejecutivo
                    ) b
                    where a.periodo = b.max
                    and a.ejecutivo=b.ejecutivo";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'userid' => $userid
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception();
            return false;
        }
    }

    public static function getRankingGrupo($userid)
    {
        try {
            $sql = "select
                    date_format(a.periodo,'%m %Y') as periodo,
                    date_format(a.periodo,'%m') as mes,
                    date_format(a.periodo,'%Y') as anio,
                    a.concepto,a.ejecutivo,a.equipo,a.factor,a.ranking,
                    Rank_Cumplimiento_Meta,Rank_Crecimiento,Rank_Fidelizacion,Rank_Penetracion,
                    Rank_Web,Rank_Ticket_Nuevos,Rank_Venta_Clientes_Nuevos,Rank_Balance,Rank_Part_Mer,Rank_Atendidos,
                    if(b.ejecutivo=a.ejecutivo,1,0) as es_el
                    from
                    ranking_ejecutivo a,
                    (
                        select
                        b.ejecutivo,b.max,a.concepto
                        from ranking_ejecutivo a,
                        (
                          select 
                          ejecutivo,max(periodo) as max 
                          from ranking_ejecutivo
                          where ejecutivo=:userid
                          group by ejecutivo
                        ) b
                        where a.periodo=b.max
                        and a.ejecutivo=b.ejecutivo
                    ) b
                    where a.periodo=b.max
                    and a.concepto=b.concepto
                    order by a.ranking asc;";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'userid' => $userid
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception();
        }
    }

    public static function getCarteraEjecutivo($rutusu)
    {
        try {
            $sql = "select
            clasificacion,
            id as relacion_comercial,
            rutcli,razons,
            negocio_eerr,observacion as observacion_credito,
            gestion_del_mes,ifnull(potencial,0) as potencial,
            ifnull(potencial-venta_actual,0) as brecha,
            ifnull(venta_anterior,0) as venta_anterior,ifnull(venta_actual,0) as venta_actual,
            ifnull(accion,'S/Gestion') as ultima_gestion,date_format(fecha_accion,'%d/%m/%Y') as fecha_ultima_gestion,
            proxima_accion,date_format(fecha_proxima,'%d/%m/%Y') as fecha_proxima,
            dias_restantes as dias_para_salir_de_cartera,
            analisis_plazos as motivo_de_salida
            from portal_ej_new_gestion_cartera
            where rutven=:rutven
            order by potencial desc";

            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'rutven' => $rutusu
            ]);

            return $resultado;
            
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getpartner($codusu, $rutusu, $tipo, $cartera)
    {
        try {
            if ($cartera == 1){
                if ($tipo == 1) {
                    $ruts = '=' . $rutusu;
                }else{
                    $ruts = "='" . $codusu . "'";
                }
            }elseif ($cartera == 2){
                if ($tipo == 1) {
                    $ruts = "";
                    $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                    $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                        'rutven' => $rutusu,
                        'rutven2' => $rutusu,
                    ]);
                    
                    foreach ($resultado as $rut) {
                        $ruts = $ruts . ',' . $rut->partner;
                    }

                    $ruts = 'in(' . substr($ruts, 1, strlen($ruts)) . ') ';
                }else{
                    $ruts = "";
                    $sql = "select distinct(userid_partner) partner from portal_ej_parteners where codven=:rutven and codven_parner <> :rutven2";

                    $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                        'rutven' => $rutusu,
                        'rutven2' => $rutusu,
                    ]);
                    
                    foreach ($resultado as $rut) {
                        $ruts = $ruts . ",'" . $rut->partner . "'";
                    }
                    
                    $ruts = "in(" . substr($ruts, 1, strlen($ruts)) . ") ";
                }
            }elseif ($cartera == 3){
                if ($tipo == 1) {
                    $ruts = "";
                    $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                    $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                        'rutven' => $rutusu,
                        'rutven2' => $rutusu,
                    ]);
                    
                    foreach ($resultado as $rut) {
                        $ruts = $ruts . ',' . $rut->partner;
                    }
                    
                    $ruts = 'in(' . $rutusu . $ruts . ") ";
                }else{
                    $ruts = "";
                    $sql = "select distinct(userid_partner) partner from portal_ej_parteners where codven=:rutven and codven_parner <> :rutven2";

                    $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                        'rutven' => $rutusu,
                        'rutven2' => $rutusu,
                    ]);
                    
                    foreach ($resultado as $rut) {
                        $ruts = $ruts . ",'" . $rut->partner . "'";
                    }
                    
                    $ruts = "in('" . $codusu . "'" . $ruts . ") ";
                }
            }

            return $ruts;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCartera($rutusu, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $cartera, $deuda, $foco, $DCartera)
    {
        try {
            if ($cartera == '1') {
                $rutusu = '=' . $rutusu;
                $resultado = PortalEjUsuariosPDO::getCarteraQuery($rutusu, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $deuda, $foco, $DCartera);

            }elseif ($cartera == '2') {
                $ruts = "";
                $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                    'rutven' => $rutusu,
                    'rutven2' => $rutusu,
                ]);
                
                foreach ($resultado as $rut) {
                    $ruts = $ruts . ',' . $rut->partner;
                }
                
                $ruts = 'in(' . substr($ruts, 1, strlen($ruts)) . ') ';
                
                $resultado = PortalEjUsuariosPDO::getCarteraQuery($ruts, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $deuda, $foco, $DCartera);

            }elseif ($cartera == '3') {
                $ruts = "";
                $sql = "select distinct(codven_parner) partner from portal_ej_parteners	where codven=:rutven and codven_parner <> :rutven2";

                $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                    'rutven' => $rutusu,
                    'rutven2' => $rutusu,
                ]);
                
                foreach ($resultado as $rut) {
                    $ruts = $ruts . ',' . $rut->partner;
                }
                
                $ruts = 'in(' . $rutusu . $ruts . ') ';

                $resultado = PortalEjUsuariosPDO::getCarteraQuery($ruts, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $deuda, $foco, $DCartera);

            }

            return $resultado;
            
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCarteraQuery($rutusu, $clasificacion, $segmento, $credito, $rango, $fechaProximaAccion, $gestMes, $brecha, $venta, $deuda, $foco, $DCartera)
    {
        try {
            //return $foco;
            if ($venta == '1'){
                $sql = "select
                    clasificacion                                   as clasificacion,                 /*Busqueda directa*/
                    id                                              as relacion,                      /*Busqueda directa*/
                    rutcli                                          as rut,                           /*Busqueda directa*/
                    razons                                          as razon_social,                  /*Busqueda directa*/
                    unidad                                          as unidad,                        /*Busqueda directa*/
                    segmento                                        as segmento,                      /*Busqueda directa*/
                    status_credito                                  as status_credito,                /*Busqueda directa*/
                    gestion_del_mes_comercial_id                    as gestion_del_mes,               /*Busqueda directa*/ 
                    ultima_gestion_id                               as ultima_gestion,                /*Busqueda directa*/
                    ifnull(date_format(fecha_ultima_gestion_id,'%d-%m-%Y'),'') as fecha_ultima_gestion,          /*Menor A (mas antiguas que) la fecha seleccionada */
                    concat('$',format(coalesce(potencial,0),0))                           as potencial,                     /*Between */
                    rango_potencial                                 as rango,                         /*Busqueda directa*/
                    concat('$',format(coalesce(venta_anterior,0),0))                     as vta_ano_anterior,              /*Between */
                    concat('$',format(coalesce(promedio_4_meses,0),0))                   as vta_promedio,                  /*Between */
                    concat('$',format(coalesce(venta_actual,0),0))                        as venta_actual,                  /*Between */";
                if ($brecha == '1'){
                    $sql = $sql . " concat('$',format(coalesce(potencial,0)-coalesce(venta_actual,0),0))  as brecha,                        /*Between */";
                }elseif ($brecha == '2'){
                    $sql = $sql . " concat('$',format(coalesce(promedio_4_meses,0)-coalesce(venta_actual,0),0))  as brecha,        /*Between */";
                }elseif ($brecha == '3'){
                    $sql = $sql . " concat('$',format(coalesce(venta_anterior,0)-coalesce(venta_actual,0),0)) as brecha,          /*Between */";
                }
                $sql = $sql . " concat('$',format(coalesce(deuda_activa,0),0))                        as deuda_activa,                  /*Mayor A*/
                    concat('$',format(coalesce(deuda_vencida,0),0))                       as deuda_vencida,                 /*Mayor A*/          
                    ''                                              as focos,                         /*Especifico, pero por el momento no lo pongas porque debo cerrar una estructura antes*/
                    dias_restantes                                  as dias_para_salir_de_cartera,    /*Menor A*/
                    proxima_accion                                  as proxima_accion_agendada,       /*Busqueda directa*/ 
                    date_format(fecha_proxima,'%d-%m-%Y')           as fecha_proxima_accion,          /*Busqueda directa, esta pensado para que vean fecha hoy, que accion tengo agendada*/
                    date_format(insercion,'%d-%m-%Y')               as actualizacion_de_informacion   /*no es necesario filtro, es solo informativo*/
                    from portal_ejecutivo.portal_ej_new_gestion_cartera
                    where rutven " . $rutusu;
            }else{
                $sql = "select
                    clasificacion                                   as clasificacion,                 /*Busqueda directa*/
                    id                                              as relacion,                      /*Busqueda directa*/
                    rutcli                                          as rut,                           /*Busqueda directa*/
                    razons                                          as razon_social,                  /*Busqueda directa*/
                    unidad                                          as unidad,                        /*Busqueda directa*/
                    segmento                                        as segmento,                      /*Busqueda directa*/
                    status_credito                                  as status_credito,                /*Busqueda directa*/
                    gestion_del_mes_comercial_id                    as gestion_del_mes,               /*Busqueda directa*/ 
                    ultima_gestion_id                               as ultima_gestion,                /*Busqueda directa*/
                    ifnull(date_format(fecha_ultima_gestion_id,'%d-%m-%Y'),'') as fecha_ultima_gestion,          /*Menor A (mas antiguas que) la fecha seleccionada */
                    concat('$',format(coalesce(potencial,0),0))                           as potencial,                     /*Between */
                    rango_potencial                                 as rango,                         /*Busqueda directa*/
                    concat('$',format(coalesce(venta_anterior_mismo_dh,0),0))             as vta_ano_anterior,              /*Between */
                    concat('$',format(coalesce(promedio_4_meses_mismo_dh,0),0))           as vta_promedio,                  /*Between */
                    concat('$',format(coalesce(venta_actual,0),0))                        as venta_actual,                  /*Between */";
                if ($brecha == '1'){
                    $sql = $sql . " concat('$',format(coalesce(potencial,0)-coalesce(venta_actual,0),0))  as brecha,                        /*Between */";
                }elseif ($brecha == '2'){
                    $sql = $sql . " concat('$',format(coalesce(promedio_4_meses_mismo_dh,0)-coalesce(venta_actual,0),0))  as brecha,        /*Between */";
                }elseif ($brecha == '3'){
                    $sql = $sql . " concat('$',format(coalesce(venta_anterior_mismo_dh,0)-coalesce(venta_actual,0),0))  as brecha,          /*Between */";
                }
                $sql = $sql . " concat('$',format(coalesce(deuda_activa,0),0))                        as deuda_activa,                  /*Mayor A*/
                    concat('$',format(coalesce(deuda_vencida,0),0))                       as deuda_vencida,                 /*Mayor A*/          
                    ''                                              as focos,                         /*Especifico, pero por el momento no lo pongas porque debo cerrar una estructura antes*/
                    dias_restantes                                  as dias_para_salir_de_cartera,    /*Menor A*/
                    proxima_accion                                  as proxima_accion_agendada,       /*Busqueda directa*/ 
                    date_format(fecha_proxima,'%d-%m-%Y')           as fecha_proxima_accion,          /*Busqueda directa, esta pensado para que vean fecha hoy, que accion tengo agendada*/
                    date_format(insercion,'%d-%m-%Y')               as actualizacion_de_informacion   /*no es necesario filtro, es solo informativo*/
                    from portal_ejecutivo.portal_ej_new_gestion_cartera
                    where rutven " . $rutusu;
            }

            if ($clasificacion <> "null") {
                $sql = $sql . " and clasificacion like '%" . $clasificacion . "%'";
            }

            if ($segmento <> "null") {
                $sql = $sql . " and segmento like '%" . $segmento . "%'";
            }
            if ($credito <> "null") {
                $sql = $sql . " and status_credito like '%" . $credito . "%'";
            }
            
            if ($rango <> "null") {
                $sql = $sql . " and rango_potencial like '%" . $rango . "%'";
            } 

            if ($gestMes <> "null") {
                $sql = $sql . " and gestion_del_mes_comercial_id like '%" . $gestMes . "%'";
            }
            
            if ($deuda <> "null") {
                $sql = $sql . " and " . $deuda;
            }

            if ($fechaProximaAccion <> "null") {
                $sql = $sql . " and fecha_proxima = '" . $fechaProximaAccion . "'";
            }

            if ($DCartera <> "null") {
                $sql = $sql . " and dias_en_cartera  <= " . $DCartera . "";
            }

            if ($foco <> "null") {
                $sql = $sql . " and rutcli in(select 
                                            rutcli 
                                            from portal_ej_focos_por_periodo 
                                            where rutven" . $rutusu . "
                                            and foco='" . $foco . "')";
            }
            
            $sql = $sql . " order by coalesce(potencial,0) desc";
            //return $sql;
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

            return $resultado;
            
        } catch (\Exception $e) {

            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return $e->getMessage();
            
        }
    }

    public static function getDetalleFichaCliente($rutcliente)
    {
        try {
            $sql = "select *
                    from portal_ej_fidelizacion_rut
                    where rutcli=:rutcliente;";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'rutcliente' => $rutcliente
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function saveAnotacion($inputs, $rutcli)
    {
        if (!array_key_exists('fecha', $inputs)) {
            $pendiente = "0000-00-00";
        } else {
            $pendiente = $inputs['fecha'];
        }

        if (!array_key_exists('prox_accion', $inputs)) {
            $proxima = "";
        } else {
            $proxima = $inputs['prox_accion'];
        }

        $consulta = "
	    update portal_ej_gestion_cartera
	    set 
	    gestion_del_mes='Anotado',
	    fecha_proxima_accion='" . $pendiente . "',
	    prox_accion='" . $proxima . "'
	    where rutcli='" . $rutcli . "'
	    and gestion_del_mes not in ('Vendido','Cotizado','Vendido NV')
	    ";

        $consulta_2 = "
	    update portal_ej_new_gestion_cartera
	    set
	    atendido_anotacion=1,
	    dias_anota_rut=0,
	    dias_anota_id=0
	    where rutcli='" . $rutcli . "'
	    ";

        $consulta_3 = "
	    update portal_ej_new_gestion_cartera
	    set 
	    gestion_del_mes='Anotado',
	    fecha_proxima='" . $pendiente . "',
	    proxima_accion='" . $proxima . "'
	    where rutcli='" . $rutcli . "'
	    and gestion_del_mes not in ('Vendido','Cotizado','Vendido NV')
	    ";
        DB::connection('mysql_portal_ejecutivo')->beginTransaction();
        try {
            DB::connection('mysql_portal_ejecutivo')->statement($consulta);
            DB::connection('mysql_portal_ejecutivo')->statement($consulta_2);
            DB::connection('mysql_portal_ejecutivo')->statement($consulta_3);
            DB::connection('mysql_portal_ejecutivo')->commit();
            return true;
        } catch (\Exception $e) {
            DB::connection('mysql_portal_ejecutivo')->rollback();
            return false;
        }
    }

    public static function getGraficoFicha($rutcli)
    {
        try {
            $sql = "select
            periodo,
            coalesce(potencial,'null') as potencial,
            coalesce(venta,'null') as venta,
            coalesce(margen,'null') as margen,
            coalesce(brecha,'null') as brecha
            from portal_ej_grafico_por_rut
            where rutcli=:rutcli
            order by periodo_fecha asc";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'rutcli' => $rutcli
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCampanaVtaMes($userid, $periodo)
    {
        try {
            $sql = "select
                    month(periodo) as periodo_mes,
                    year(periodo) as periodo_anio,
                    periodo,usuario,
                    atendidos_ruts                    as  ruts_atendidos,
                    atendidos_neto                    as  venta_generada_por_ejecutivo,
                    meta_venta                        as  meta_de_venta,
                    cumplimiento_meta                 as  cumplimiento_meta,
                    atendidos_margen                  as  margen_ejecutivo,
                    atendidos_participacion_mp        as  per_participacion_mp,
                    prom_penetracion_linea_en_cartera as  promedio_penetracion_lineas,
                    atendidos_per_edi                 as  porcentaje_venta_integraciones,
                    atendidos_per_internet            as  porcentaje_venta_internet
                    from portal_ej_performance_ejecutivo
                    where usuario=:usuario
                    and periodo=:periodo;";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'usuario' => $userid,
                'periodo' => $periodo
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }


    public static function getCampanaClienteCartera($userid, $periodo)
    {
        try {
            $sql = "select
                    month(periodo) as periodo_mes,
                    year(periodo) as periodo_anio,
                    periodo,usuario,
                    por_perder                          as  clientes_por_perder_inicio_mes,
                    perdidos                            as  clientes_perdidos,
                    round((perdidos/por_perder)*100,1)  as  porcentaje_recuperacion,
                    nuevos                              as  q_clientes_nuevos,
                    mantenido_en_cuatro_meses           as  clientes_mantenidos_cuatro_meses,
                    per_cartera_mantenida               as  porcentaje_cartera_mantenida,
                    fidelizado_cuatro_meses             as  clientes_fidelizados_cuatro_meses,
                    per_fidelizado_cuatro_meses         as  porcentaje_fidelizado_cuatro_meses,
                    fidelizado_tres_meses               as  clientes_fidelizados_tres_meses,
                    fidelizado_dos_meses                as  clientes_fidelizados_dos_meses
                    from portal_ej_performance_ejecutivo
                    where usuario=:usuario
                    and periodo=:periodo;";
            $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql, [
                'usuario' => $userid,
                'periodo' => $periodo
            ]);
            return $resultado;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getTiposCliente()
    {
        try {
            $sql = "select * from pri_tipo_cliente order by id";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            return $resultado;
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }

    public static function getDataDashboard($negocio)
    {
        try {
            $sql = "select * from pri_dashboard_ki where negocio = '" . $negocio . "' and estado = 1";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            return $resultado;
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }

    public static function getMetaVenta($negocio, $venta_real)
    {
        $periodo = date('Y-m-d');
        $sql = "SELECT IFNULL((SELECT vpdh FROM pri_metas_dashboard WHERE 
        periodo = '" . $periodo . "' and negocio = '" . $negocio . "'),0) meta_venta";
        $meta = 0;
        $resultado = DB::connection('mysql_dashboard')->select($sql);
        foreach ($resultado as $result) {
            $meta = $result->meta_venta;
        }
        if ($venta_real >= $meta) {
            $diff = round(($meta) / 1000000, 0);
            $msg = '<i class="text-success" style="font-weight:bold;"><i class="fa fa-thumbs-up"></i></i> Nuestra meta del día es ' . $diff . ' MM';
        } else {
            $diff = round(($meta) / 1000000, 0);
            $msg = '<i class="text-danger" style="font-weight:bold;"><i class="fa fa-thumbs-down"></i></i> Nuestra meta del día es ' . $diff . ' MM';
        }
        return $msg;
    }

    public static function getMetaMF($negocio, $mf_real)
    {
        $periodo = date('Y-m-d');
        $sql = "
        SELECT IFNULL(
        (SELECT margen_frontal FROM pri_metas_dashboard WHERE periodo = '" . $periodo . "' and negocio = '" . $negocio . "'),0) meta_mf";
        $meta = 0;
        $resultado = DB::connection('mysql_dashboard')->select($sql);
        foreach ($resultado as $result) {
            $meta = $result->meta_mf;
        }
        if ($mf_real >= $meta) {
            $diff = round(($mf_real - $meta), 2);
            $msg = '<i class="text-success" style="font-weight:bold;"><i class="fa fa-thumbs-up"></i></i> Superamos por ' . $diff . '%';
        } else {
            $diff = round(($meta - $mf_real), 2);
            $msg = '<i class="text-danger" style="font-weight:bold;"><i class="fa fa-thumbs-down"></i></i> Estamos bajo ' . $diff . '%';
        }
        return $msg;
    }

    public static function getMetaCM($negocio, $cm_real)
    {
        $periodo = date('Y-m-d');
        $sql = "
        SELECT IFNULL(
        (SELECT correcion_monetaria FROM pri_metas_dashboard WHERE 
        periodo = '" . $periodo . "' and negocio = '" . $negocio . "'),0) meta_cm";
        $meta = 0;
        $resultado = DB::connection('mysql_dashboard')->select($sql);
        foreach ($resultado as $result) {
            $meta = $result->meta_cm;
        }
        if ($cm_real >= $meta) {
            $diff = round(($cm_real - $meta), 2);
            $msg = '<i class="text-success" style="font-weight:bold;"><i class="fa fa-thumbs-up"></i></i> Superamos por ' . $diff . '%';
        } else {
            $diff = round(($meta - $cm_real), 2);
            $msg = '<i class="text-danger" style="font-weight:bold;"><i class="fa fa-thumbs-down"></i></i> Estamos bajo ' . $diff . '%';
        }
        return $msg;
    }

    public static function getMetaMP($negocio, $mp_real)
    {
        $periodo = date('Y-m-d');
        $sql = "
        select ifnull(
        (SELECT marca_propia FROM pri_metas_dashboard WHERE periodo = '" . $periodo . "' and negocio = '" . $negocio . "'),0) meta_mp";
        $meta = 0;
        $resultado = DB::connection('mysql_dashboard')->select($sql);
        foreach ($resultado as $result) {
            $meta = $result->meta_mp;
        }
        if ($mp_real >= $meta) {
            $diff = round(($mp_real - $meta), 2);
            $msg = '<i class="text-success" style="font-weight:bold;"><i class="fa fa-thumbs-up"></i></i> Superamos por ' . $diff . '%';
        } else {
            $diff = round(($meta - $mp_real), 2);
            $msg = '<i class="text-danger" style="font-weight:bold;"><i class="fa fa-thumbs-down"></i></i> Estamos bajo ' . $diff . '%';
        }
        return $msg;
    }

    public static function getMetaMG($negocio, $mg_real)
    {
        $periodo = date('Y-m-d');
        $sql = "
        SELECT IFNULL(
        (SELECT margen_frontal + correcion_monetaria + marca_propia FROM pri_metas_dashboard WHERE periodo = '" . $periodo . "' and negocio = '" . $negocio . "'),0) meta_mg";
        $meta = 0;
        $resultado = DB::connection('mysql_dashboard')->select($sql);
        foreach ($resultado as $result) {
            $meta = $result->meta_mg;
        }
        if ($mg_real >= $meta) {
            $diff = round(($mg_real - $meta), 2);
            $msg = '<i class="text-success" style="font-weight:bold;"><i class="fa fa-thumbs-up"></i></i> Superamos por ' . $diff . '%';
        } else {
            $diff = round(($meta - $mg_real), 2);
            $msg = '<i class="text-danger" style="font-weight:bold;"><i class="fa fa-thumbs-down"></i></i> Estamos bajo ' . $diff . '%';
        }
        return $msg;
    }

    public static function getDataDashboardLineas($negocio, $linea)
    {
        try {
            $sql = "select * from pri_dashboard_ki_lineas where negocio = '" . $negocio . "' and linea = '" . $linea . "' and estado = 1";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            return $resultado;
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }

    public static function getCount($tipo)
    {
        $corte = self::getHoraCorte();
        try {
            $sql_filter = "";
            
            if ($tipo != 'All') {
                $tipo = str_replace("_", " ", $tipo);
                $tipo = strtoupper($tipo);
                $sql_filter = " and NEGOCIO_EERR = '" . $tipo . "' ";
            }
            $sql = "select
            ifnull((select count(distinct(NUMNVT)) from pri_margen_notas_history where FECHA_CREAC >= date_format(DATE_SUB(NOW(), INTERVAL CASE 
                WHEN DAYOFWEEK(CURRENT_DATE) = 1 THEN 2
                WHEN DAYOFWEEK(CURRENT_DATE) = 2 THEN 3
                ELSE 1
            END DAY), '%Y-%m-%d " . $corte . ":01') " . $sql_filter . "),0) + ifnull(
            (select count(distinct(NUMNVT)) from pri_margen_notas_current where FECHA_CREAC <= date_format(CURRENT_DATE,'%Y-%m-%d " . $corte . ":00') " . $sql_filter . "),0) as venta_dia";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            foreach ($resultado as $result) {
                return $result->venta_dia;
            }
            return 0;
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }

    public static function getLastUpdate()
    {
        $corte = self::getHoraCorte();
        try {
            $sql = "select date_format(DATE_SUB(NOW(), INTERVAL 
                CASE 
                        WHEN DAYOFWEEK(CURRENT_DATE) = 1 THEN 2
                        WHEN DAYOFWEEK(CURRENT_DATE) = 2 THEN 3
                        ELSE 1
                END 
            DAY), '%Y-%m-%d " . $corte . ":01') desde,date_format(max(FECHA_CREAC),'%d-%m-%Y %H:%i:%S') as last_update from pri_margen_notas_current where FECHA_CREAC <= date_format(CURRENT_DATE,'%Y-%m-%d " . $corte . ":00')";
            //return $sql;
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            foreach ($resultado as $result) {
                return $result;
            }
            return 0;
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }
    
    public static function getHoraCorte()
    {
        try {
            $sql = "select valor from pri_configuracion where id = 1";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            return !empty($resultado[0]->valor) ? $resultado[0]->valor : '16:00';
        } catch (Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }

    public static function Comisiones($rut, $fecha)
    {
        $date = strtotime($fecha);
        $mes = date("m", $date); 
        $ano = date("Y", $date);
        
        $sql = "select
                a.rutcli,b.razons,a.tipo_doc,a.fecha_documento,a.foliodoc,a.numnvt,
                round(sum(a.neto),0) as neto,
                round(sum(comision_calculada),0) as comision_total,
                round(sum(if(categoria_comision='A',comision_calculada,0)),0) as comision_A,
                round(sum(if(categoria_comision='B',comision_calculada,0)),0) as comision_B,
                round(sum(if(categoria_comision='C',comision_calculada,0)),0) as comision_C,
                round(sum(if(categoria_comision not in ('A','B','C'),comision_calculada,0)),0) as comision_D
                from bonos_detalle_comisiones a, analisis_tristan.data_clientes b
                where a.rutcli=b.rutcli
                and ejecutivo_a_imputar_comision= " . $rut . " 
                and a.fecha_documento between '" . substr($fecha, 4, 4) . substr($fecha, 2, 2) . '01' . "' and LAST_DAY('" . substr($fecha, 4, 4) . substr($fecha, 2, 2) . '01' . "')
                group by a.rutcli,b.razons,a.tipo_doc,a.fecha_documento,a.foliodoc,a.numnvt
                order by a.fecha_documento desc";
        
        $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);

        return $resultado;
    }

    public static function getUrlPBI($id)
    {
        try {
            $sql = "SELECT url_encrypt FROM pri_re_url_encrypt where id = $id;";
            $resultado = DB::connection('mysql_dashboard')->select($sql);
            return $resultado[0]->url_encrypt;
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
        }
    }
    
}
