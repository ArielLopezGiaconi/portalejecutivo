{{--<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.20/dataRender/datetime.js"></script>--}}
<script>
    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var rut_buscado='';
    var generaArchivoAdjunto=false;
    var moduleid_global = 10;

    function validarEmail(valor) {
        var emailvalido=false;

        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

        if (emailRegex.test(valor)){
            emailvalido=true;
        } else {
            emailvalido=false;
        }
        //console.log(emailvalido);

        return emailvalido;
    }
    
    function redirecturl(id){
        switch (id) {
            case 'btn_gencoti':
                invocaRegistroBitacora('gencoti');
                window.location = "{{ url('generacotizacion') }}/" + rut_cliente + "/listacotizaciones";

                break;
            case 'btn_editcoti':
                window.location = "{{ url('editacotizacion') }}/" + rut_cliente + "/listacotizaciones";
                break;
            case 'btn_fichacliente':
                invocaRegistroBitacora('fichacliente');
                window.location = "{{ url('fichacliente') }}/" + rut_cliente;

                break;
        }
    }
    function refreshPage(){
        invocaRegistroBitacora('refresh');
        location.reload();

    }
    
    function setInputsDataCliente(){
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rut_cliente
        };
        $.ajax({
            url: '{{ url('getdetalleclienteoracle') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200){

                    var rut=json.response[0].rutcli + '-' + json.response[0].digcli;
                    var razons=json.response[0].razons;

                    document.getElementById('tx_rut_cliente').value = rut;
                    document.getElementById('tx_razons_cliente').value= razons;
                }
                else{
                }
            },
            error:function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener detalle cliente oracle con rut', code, text);
            }
        });
    }
    function getCotizaciones() {
        var token = '{{ csrf_token() }}';
        var rutcli=rut_cliente;
        var ejecutivo='{{ Session::get('usuario')->get('rutusu') }}';
        var parametros = {
           // _token: token,
            rutcli: rutcli,
            ejecutivo: ejecutivo
        };
        //console.log(parametros);

        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/listado-cotizaciones',
            //data: parametros,
            {{--url: '{{url('getlistadocotizaciones')}}',--}}
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    if (Object.keys(json.response).length > 0) {
                        //console.log(json);

                        // if ($.fn.DataTable.isDataTable("#tb_listado_cotizaciones")) {
                        //     $('#tb_listado_cotizaciones').DataTable().clear().destroy();
                        // }
                        console.log(json.response);
                        makeDatatableListaCotizaciones(json.response);
                    }
                    else{
                        makeDatatableListaCotizaciones('');
                    }
                }
                else{
                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                    makeDatatableListaCotizaciones('');
                }
            },
            error: function (e) {
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'listar cotizaciones', code, text);
            }
        });
    }
    function makeDatatableListaCotizaciones(data){
        $("#tb_listado_cotizaciones").DataTable({
            width: '100%',
            "order": [[ 0, "desc" ]],
            data: data,
            columns: [
                {
                    data: "numcot",
                    className:'filas'
                },
                {
                    data: "fecemi",
                    render: function ( data, type, row ) {
                        return moment(data).format('L');
                        //return moment(data).format('DD-MM-YYYY');
                    },
                    className:'filas'
                },
                {
                    data: "fecven",
                    render: function ( data, type, row ) {
                        return moment(data).format('L');
                        //return moment(data).format('DD-MM-YYYY');
                    },
                    className:'filas'
                },
                {
                    data: "cencos",
                    className:'filas'
                },
                {
                    data: "subtot",
                    render: $.fn.dataTable.render.number('.', ',', 0, '$', ''),
                    className:'filas'
                },
                {
                    data: "totiva",
                    render: $.fn.dataTable.render.number('.', ',', 0, '$', ''),
                    className:'filas'
                },
                {
                    data: "totgen",
                    render: $.fn.dataTable.render.number('.', ',', 0, '$', ''),
                    className:'filas'
                },
                {
                    data: 'margen',
                    render: function ( data, type, row ) {
                        return data + '%';
                    },
                    className:'filas'
                },
                {
                    data: 'estado',
                    render: function (estado, type, row){
                        // var porcentaje = '';
                        // switch (estado) {
                        //     case '1':
                        //         return '<i class="fa fa-clock-o f-28 est-coti-1" title="Pendiente de venta '+ porcentaje +'"></i>';
                        //         break;
                        //     case '2':
                        //         return '<i class="fa fa-times-circle f-28 est-coti-2" title="Vencida sin venta"></i>';
                        //         break;
                        //     case '3':
                        //         return '<i class="fa fa-check-circle f-28 est-coti-3" title="Vendida"></i>';
                        //         break;
                        // }
                        return '<i class="icofont icofont-repair f-22" title="* Funcionalidad en desarrollo"></i>';
                    },
                    className: 'dt-center filas'
                },
                {
                    data: "numcot", render: function (data, type, row, meta) {
                        var boton = '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                            '                                            <button type="button"\n' +
                            '                                                    class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                            '                                                    title="Exporta PDF" onclick="exportCotizacionPdf('+ data +');"><span\n' +
                            '                                                        class="fa fa-download f-16"></span>\n' +
                            '                                            </button>\n' +
                            '                                            <button type="button"\n' +
                            '                                                    class="tabledit-edit-button btn btn-warning waves-effect waves-light"\n' +
                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                            '                                                    title="Enviar por correo" data-toggle="modal" data-target="#modal-send-email" onclick="invocaSendMailCotizacion('+ data +');"><span\n' +
                            '                                                        class="fa fa-envelope f-16"></span>\n' +
                            '                                            </button>\n' +
                            '                                            <button type="button"\n' +
                            '                                                    class="tabledit-delete-button btn btn-inverse waves-effect waves-light"\n' +
                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                            '                                                    title="Editar cotización" onclick="editarCotizacion('+ data +');"><span\n' +
                            '                                                        class="fa fa-pencil f-18"></span>\n' +
                            '                                            </button>\n' +
                            '                                            <button type="button"\n' +
                            '                                                    class="tabledit-delete-button btn btn-primary waves-effect waves-light"\n' +
                            '                                                    style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                            '                                                    title="Generar cotización a partir de ésta" onclick="generarCotizacion('+ data +');"><span\n' +
                            '                                                        class="fa fa-magic f-18"></span>\n' +
                            '                                            </button>\n' +
                            '                                        </div>\n' +
                            '                                    </div>';
                        return boton;
                    },
                    className: 'botones_cotizaciones'
                }
            ],
            language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
        });

    }
    function findClientForRut(){
        var valor_buscado = document.getElementById('tx_rut_buscar').value;
        // valor_buscado = valor_buscado.toString().toUpperCase();

        var parametros={
            vendedor: ejecutivo,
            rut: valor_buscado,
            codemp: 3
        };

        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-especifico',
            type: 'POST',
            // cache: true,
            datatype: 'json',
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {

                if (json.code == 200) {
                    console.log(json);
                    document.getElementById('tx_rut_buscar').value = json.response.rutcli + '-' + json.response.digcli;
                    rut_buscado = json.response.rutcli;

                } else {
                    rut_buscado = '';

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }
                // document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar cliente por rut', code, text);
            }
        })
    }
    function findClientForRazons(){
        var valor_buscado = document.getElementById('tx_razons_buscar').value;
        valor_buscado = valor_buscado.toString().toUpperCase();

        var parametros={
            vendedor: ejecutivo,
            razon_social: valor_buscado
        };

        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-rsocial',
            type: 'POST',
            // cache: true,
            datatype: 'json',
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {

                if (json.code == 200) {
                    console.log(json);
                    var data = json.response;
                    makeDataTableClientes(data);

                } else {

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }
                // document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar cliente por razon social', code, text);
            }
        })
    }
    function makeDataTableClientes(data){

        var rows = '<tbody>\n';
        data.forEach(function(cli){

            rows = rows + '<tr>\n' +
                '                                                                            <td><input type="radio" name="radio_rutcli" value="' + cli.rutcli + '"></td>\n' +
                '                                                                            <td>' + cli.rutcli + '</td>\n' +
                '                                                                            <td>' + cli.razons + '</td>\n' +
                '                                                                        </tr>\n';
        });
        rows = rows + '</tbody>\n';
        // console.log(rows);

        var content = '<table id="tb_clientes_razons" class="table table-xs fixed_header" >\n' +
            '                                                                    <thead>\n' +
            '                                                                    <tr>\n' +
            '                                                                        <th>Seleccionar</th>\n' +
            '                                                                        <th>Rut</th>\n' +
            '                                                                        <th>Razón social</th>\n' +
            '                                                                    </tr>\n' +
            '                                                                    </thead>\n' +
            rows +
            '                                                                </table>';

        document.getElementById('div_tb_clientes_razons').innerHTML=content;
    }
    function selectClientFromRut(){
        if(rut_buscado!=''){
            window.location = '{{ url('listacotizacion') }}/' + rut_buscado;
        }
    }
    function selectClientFromTableResult(){
        // var table = document.getElementById('tb_clientes_razons');
        var radios = document.getElementsByName('radio_rutcli');
        // var inputs = table.getElementsByTagName('input');
        var rut_selected='';
        radios.forEach(function(ra){
            var checked = ra.checked;
            if(checked){
                rut_selected = ra.value;
            }

        });

        window.location = '{{ url('listacotizacion') }}/' + rut_selected;
    }

    function exportCotizacionPdf(numcot){
        var parametros = {
            "numcot": numcot
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/exportar-cotizacion',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {

                    var productos=json.response.detalle_coti;
                    var totales=json.response.encabezado_coti;


                    var parametros = {
                        _token: '{{ csrf_token() }}',
                        "productos": productos,
                        "totales": totales
                    };
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                        data: parametros,
                        url: '{{ url('generarpdfcotizacion') }}',
                        type: 'POST',
                        datatype: 'json',
                        success: function (json) {
                            window.open('{{ url('download-pdf') }}/'+ numcot);
                        },
                        error: function(e){
                            var code = e.status;
                            var text = e.statusText;
                            registroError(ejecutivo, moduleid_global, 'genera pdf para exportar', code, text);
                        }
                    });
                }
                else{
                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }

                //document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'exportar cotizacion', code, text);
            }
        });
    }

    function invocaSendMailCotizacion(numcot){

        document.getElementById('h_numcot').value = numcot;
        invocaRegistroBitacora('clicenviarcorreo', '', '', numcot);

    }

    function generarArchivoAdjunto(){

        var destinatario = document.getElementById('tx_correo_destinatario').value;
        if(destinatario!="") {
            var emailvalido = validarEmail(destinatario);

            if (emailvalido) {

                var numcot = document.getElementById('h_numcot').value;
                console.log(numcot);

                var parametros = {
                    "numcot": numcot
                };
                $.ajax({
                    xhrFields: {
                        withCredentials: true
                    },
                    headers: {
                        'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                    },
                    data: parametros,
                    url: '{{ env("URL_API") }}/api/cotizacion/exportar-cotizacion',
                    type: 'POST',
                    cache: false,
                    datatype: 'json',
                    beforeSend: function () {
                        document.getElementById("pre_loader").className = "loader animation-start";
                    },
                    success: function (json) {
                        if (json.code == 200) {

                            var productos = json.response.detalle_coti;
                            var totales = json.response.encabezado_coti;

                            var parametros = {
                                _token: '{{ csrf_token() }}',
                                productos: productos,
                                totales: totales
                            };
                            $.ajax({
                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                data: parametros,
                                url: '{{ url('generarpdfcotizacionandsave') }}',
                                type: 'POST',
                                datatype: 'json',
                                success: function (json) {
                                    var rutaarchivo = json.response.rutaarchivo;
                                    var nombrearchivo = json.response.nombrearchivo;
                                    var nombreejecutivo = '{{ Session::get('nombre_ejecutivo') }}';

                                    var destinatario = document.getElementById('tx_correo_destinatario').value;
                                    var comentarioejecutivo = document.getElementById('tx_comentario_ejecutivo').value;

                                    sendMailCotizacion(numcot, rutaarchivo, nombrearchivo, nombreejecutivo, destinatario, comentarioejecutivo);
                                }
                            });
                        } else {
                            //generaArchivoAdjunto=false;
                        }
                    },
                    error: function(e){
                        var code = e.status;
                        var text = e.statusText;
                        registroError(ejecutivo, moduleid_global, 'genera pdf para adjuntar a correo', code, text);
                    }
                });
            } else {
                var nFrom = "top";
                var nAlign = "center";
                var nIcons = "fa fa-times";
                var nType = "danger";
                var nAnimIn = "animated bounceInRight";
                var nAnimOut = "animated bounceOutRight";
                var title = "";
                var message = 'El correo ingresado no está correcto, favor vuelva a ingresarlo.';

                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                document.getElementById('tx_correo_destinatario').value = "";
            }
        }
    }
    function sendMailCotizacion(numcot, rutaarchivo, nombrearchivo, nombreejecutivo, destinatario, comentarioejecutivo){
        var parametros = {
            numcot: numcot,
            rutaarchivo: rutaarchivo,
            nombrearchivo: nombrearchivo,
            ejecutivo: nombreejecutivo,
            destinatario: destinatario,
            comentarioejecutivo: comentarioejecutivo
        };
        $.ajax({
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            data: parametros,
            url: '{{ url('sendmailcotizacion') }}',
            type: 'POST',
            datatype: 'json',
            success: function (json) {
                if(json.code==200){
                    invocaRegistroBitacora('enviarcorreo', '', '', numcot);

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-check";
                    var nType = "success";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                    $('#close-send-email').click();
                }

                document.getElementById('h_numcot').value="";
                document.getElementById('tx_correo_destinatario').value = "";
                document.getElementById('tx_comentario_ejecutivo').value = "";

                document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                console.log(ejecutivo);
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'enviar correo cotizacion', code, text);

                document.getElementById("pre_loader").className = "loader animation-start d-none";
            }
        });
    }

    function editarCotizacion(numcot){

        //invocaRegistroBitacora('editcoti', '', '', numcot);

        var parametros = {
            "numcot": numcot
        };
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/cotizacion/valida-vigencia-cotizacion',
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {
                if (json.code == 200) {
                    console.log(json.response.vigente);
                    if(json.response.vigente == "1"){
                        window.location = "{{ url('editacotizacion') }}/" + rut_cliente +'/'+ numcot + '/listacotizaciones';
                    }
                    else{
                        var nFrom = "top";
                        var nAlign = "center";
                        var nIcons = "fa fa-times";
                        var nType = "danger";
                        var nAnimIn = "animated bounceInRight";
                        var nAnimOut = "animated bounceOutRight";
                        var title = "";
                        var message = 'Cotización vencida, no se puede editar';

                        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                    }
                }
                else{
                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }

                //document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'validar vigencia cotizacion', code, text);
            }
        });


    }
    
    function generarCotizacion(numcot){
        invocaRegistroBitacora('generacotiapartirde', '', '', numcot);

        window.location = "{{ url('generacotizacion') }}/" + rut_cliente + '/listacotizaciones' + '/'+ numcot;
    }

    function invocaRegistroBitacora(action, oldvalue='', newvalue='', updaterecord=''){
        /*
        Module: vercotizaciones -> 10
        Action: clic en exportar pdf	 -> 10
                clic en enviar correo	 -> 11
                enviar correo	 -> 26
                clic en editar cotizacion	 -> 12
                clic en refrescar pagina	 -> 13

                clic en ficha cliente	 -> 4
                clic en generar cotizacion	 -> 9
        */
        var action_num=0;
        switch (action) {
            case "exportpdf":
                action_num=10;
                break;
            case "clicenviarcorreo":
                action_num=11;
                break;
            case "enviarcorreo":
                action_num=26;
                break;
            case "editcoti":
                action_num=12;
                break;
            case "refresh":
                action_num=13;
                break;
            case "fichacliente":
                action_num=4;
                break;
            case "gencoti":
                action_num=9;
                break;

        }
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=10;
        var actionid=action_num;
        registroBitacora(userid, moduleid, actionid, oldvalue, newvalue, updaterecord);
    }

</script>
