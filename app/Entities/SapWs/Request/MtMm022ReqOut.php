<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 05/09/2018
 * Time: 13:45
 */


namespace App\Entities\SapWs\Request;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use DB;
use Illuminate\Support\Collection;

/* CREACIÓN DE MERCADERÍA */
class MtMm022ReqOut extends ReqOut
{
    private $tipo_articulo;
    private $manufacturer_sap_id;
    private $manufacturer_codpro;
    private $peso_neto;
    private $peso_medida;
    private $jerarquia;
    private $linea_jerarquia;
    private $volumen;
    private $volumen_unidad;
    private $producto_nombre;
    private $producto_nombrecorto;
    private $producto_descripcion;
    private $grupo_compra;
    private $grupo_material;
    private $codigo_proveedor;
    private $organizacion_compra;
    private $multiplo_venta;
    private $costo_neto;
    private $codigo_barra;
    private $idioma;
    private $centro_bodega;
    private $control_de_precio;
    private $precio_variable;
    private $codigo_material;
    private $precio_compra;
    private $codpro_proveedor;

    public function __construct()
    {
        $this->setHeaderHosts((env('DATABASE_ENV') == 'prod') ? env('WS_SAP_PROD') : env('WS_SAP_DEV'));
        $this->setHeaderUrl($this->getHeaderHosts().'/XISOAPAdapter/MessageServlet'); // dimpoqasci.dimerc.cl
        $this->setHeaderMethod('POST');
        $this->setHeaderUrlfull(
            $this->getHeaderUrl()
            . '?senderParty='
            . '&senderService=BC_VERACORE'
            . '&receiverParty='
            . '&receiverService='
            . '&interface=SI_MM022_SYN_OUT'
            . '&interfaceNamespace=urn:swap:veracore:s4h:creacion_productos_veracore:MM022');

        $this->tipo_articulo = 'L01-OYL';
        $this->manufacturer_sap_id = '00001';
        $this->codpro_proveedor = 'sd16-871234';
        $this->peso_neto = '50';
        $this->peso_medida = 'KG';
        $this->jerarquia = '0101010101';
        $this->volumen = '10';
        $this->volumen_unidad = 'M3';
        $this->codigo_barra = '1111111111111';
        $this->idioma = 'S'; // Spanish
        $this->producto_nombrecorto = '';
        $this->producto_nombre = '';
        $this->producto_descripcion = '';
        $this->centro_bodega = '2001';
        $this->linea_jerarquia = '01';
        $this->grupo_material = '02'; // hasta agotar stock
        $this->control_de_precio = 'S'; // Standar
        $this->precio_variable = '999';
        $this->organizacion_compra = '2000';
        $this->codigo_proveedor = '1000028';
        $this->codigo_material = '';
        $this->multiplo_venta = '1';
        $this->precio_compra = '1250';
    }

    public function getTipoArticulo()
    {
        return $this->tipo_articulo;
    }

    public function setTipoArticulo($tipo_articulo)
    {
        $this->tipo_articulo = $tipo_articulo;
    }

    public function getManufacturerSapId()
    {
        return $this->manufacturer_sap_id;
    }

    public function setManufacturerSapId($manufacturer_sap_id)
    {
        $this->manufacturer_sap_id = $manufacturer_sap_id;
    }

    public function getManufacturerCodpro()
    {
        return $this->manufacturer_codpro;
    }

    public function setManufacturerCodpro($manufacturer_codpro)
    {
        $this->manufacturer_codpro = $manufacturer_codpro;
    }

    public function getPesoNeto()
    {
        return $this->peso_neto;
    }

    public function setPesoNeto($peso_neto)
    {
        $this->peso_neto = $peso_neto;
    }

    public function getJerarquia()
    {
        return $this->jerarquia;
    }

    public function setJerarquia($jerarquia)
    {
        $this->jerarquia = $jerarquia;
    }

    public function getLineaJerarquia()
    {
        return $this->linea_jerarquia;
    }

    public function setLineaJerarquia($linea_jerarquia)
    {
        $this->linea_jerarquia = $linea_jerarquia;
    }

    public function getVolumenUnidad()
    {
        return $this->volumen_unidad;
    }

    public function setVolumenUnidad($volumen_unidad)
    {
        $this->volumen_unidad = $volumen_unidad;
    }

    public function getProductoNombre()
    {
        return $this->producto_nombre;
    }

    public function setProductoNombre($producto_nombre)
    {
        $this->producto_nombre = $producto_nombre;
    }

    public function getProductoNombrecorto()
    {
        return $this->producto_nombrecorto;
    }

    public function setProductoNombrecorto($producto_nombrecorto)
    {
        $this->producto_nombrecorto = $producto_nombrecorto;
    }

    public function getProductoDescripcion()
    {
        return $this->producto_descripcion;
    }

    public function setProductoDescripcion($producto_descripcion)
    {
        $this->producto_descripcion = $producto_descripcion;
    }

    public function getGrupoCompra()
    {
        return $this->grupo_compra;
    }

    public function setGrupoCompra($grupo_compra)
    {
        $this->grupo_compra = $grupo_compra;
    }

    public function getGrupoMaterial()
    {
        return $this->grupo_material;
    }

    public function setGrupoMaterial($grupo_material)
    {
        $this->grupo_material = $grupo_material;
    }

    public function getCodigoProveedor()
    {
        return $this->codigo_proveedor;
    }

    public function setCodigoProveedor($codigo_proveedor)
    {
        $this->codigo_proveedor = $codigo_proveedor;
    }

    public function getOrganizacionCompra()
    {
        return $this->organizacion_compra;
    }

    public function setOrganizacionCompra($organizacion_compra)
    {
        $this->organizacion_compra = $organizacion_compra;
    }

    public function getMultiploVenta()
    {
        return $this->multiplo_venta;
    }

    public function setMultiploVenta($multiplo_venta)
    {
        $this->multiplo_venta = $multiplo_venta;
    }

    public function getCostoNeto()
    {
        return $this->costo_neto;
    }

    public function setCostoNeto($costo_neto)
    {
        $this->costo_neto = $costo_neto;
    }

    public function getCodigoBarra()
    {
        return $this->codigo_barra;
    }

    public function setCodigoBarra($codigo_barra)
    {
        $this->codigo_barra = $codigo_barra;
    }

    public function getPesoMedida()
    {
        return $this->peso_medida;
    }

    public function setPesoMedida($peso_medida)
    {
        $this->peso_medida = $peso_medida;
    }

    public function getVolumen()
    {
        return $this->volumen;
    }

    public function setVolumen($volumen)
    {
        $this->volumen = $volumen;
    }

    public function getIdioma()
    {
        return $this->idioma;
    }

    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
    }

    public function getCentroBodega()
    {
        return $this->centro_bodega;
    }

    public function setCentroBodega($centro_bodega)
    {
        $this->centro_bodega = $centro_bodega;
    }

    public function getControlDePrecio()
    {
        return $this->control_de_precio;
    }

    public function setControlDePrecio($control_de_precio)
    {
        $this->control_de_precio = $control_de_precio;
    }

    public function getPrecioVariable()
    {
        return $this->precio_variable;
    }

    public function setPrecioVariable($precio_variable)
    {
        $this->precio_variable = $precio_variable;
    }

    public function getCodigoMaterial()
    {
        return $this->codigo_material;
    }

    public function setCodigoMaterial($codigo_material)
    {
        $this->codigo_material = $codigo_material;
    }

    public function getPrecioCompra()
    {
        return $this->precio_compra;
    }

    public function setPrecioCompra($precio_compra)
    {
        $this->precio_compra = $precio_compra;
    }

    public function getCodproProveedor()
    {
        return $this->codpro_proveedor;
    }

    public function setCodproProveedor($codpro_proveedor)
    {
        $this->codpro_proveedor = $codpro_proveedor;
    }

    public function getRequestXml()
    {
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:swap:veracore:s4h:creacion_productos_veracore:MM022">'
            . ' <soapenv:Header/>'
            . ' <soapenv:Body>'
            . ' <urn:MT_MM022_REQ_OUT>'
            . ' <IND_SECTOR>1</IND_SECTOR>' // RAMO COMERCIO ~~~ Static Value
            . ' <MATL_TYPE>ZMER</MATL_TYPE>'    // TIPO MATERIAL: MERCADERÍA ~~~ Static Value
            . ' <MATL_GROUP>'. $this->getTipoArticulo() .'</MATL_GROUP>'   // GRUPO ARTÍCULOS ~~~ Article Type: Office(L01-OYL), Food(L03-ALI), Technology(L04-TEC), Toys(L07-JUG), etc...
            . ' <EXTMATLGRP>'. $this->getManufacturerSapId() .'</EXTMATLGRP>' //  Manufacturer SAP ID code (Ex. Apple(00001)
            . ' <OLD_MAT_NO>'. $this->getCodproProveedor() .'</OLD_MAT_NO>'  // CÓDIGO PROVEEDOR - Old Product Value
            . ' <BASE_UOM>C/U</BASE_UOM>'   // UNIDAD MEDIDA BASE ~~~ Static Value
            . ' <PO_UNIT></PO_UNIT>'
            . ' <SIZE_DIM></SIZE_DIM>'
            . ' <NET_WEIGHT>'. $this->getPesoNeto() .'</NET_WEIGHT>'    // Product Weight
            . ' <UNIT_OF_WT>'. $this->getPesoMedida() .'</UNIT_OF_WT>' // Product Weight Unit
            . ' <DIVISION>10</DIVISION>'    // SECTOR (?) - Static Value, Operational(10)
            . ' <PROD_HIER>'. $this->getJerarquia() .'</PROD_HIER>'  // Jararquía (01 LINEA | 02 FAMILIA | 03 SUB-FAMILIA |) ~~~ Product Hierarchy
            . ' <VOLUME>'. $this->getVolumen() .'</VOLUME>' // Product Volume ~~~ We will use stacic Value for Dropship Products
            . ' <VOLUMEUNIT>'. $this->getVolumenUnidad() .'</VOLUMEUNIT>' // Product Volumen Unit
            . ' <ITEM_CAT>BANS</ITEM_CAT>' // Static Value - Production position. For Dropship products is always BANS
            . ' <EAN_UPC>'. $this->getCodigoBarra() .'</EAN_UPC>' // EAN Code
            . ' <LANGU>'. $this->getIdioma() .'</LANGU>' // Static Value ~~~ Language(S)
            . ' <MATL_DESC>'. $this->getProductoNombrecorto() .'</MATL_DESC>' // Product Short Description
            . ' <APPLOBJECT>Material</APPLOBJECT>' // Object of application
            . ' <TEXT_NAME>'. $this->getProductoNombre() .'</TEXT_NAME>' //  Product Name
            . ' <TEXT_ID>GRUN</TEXT_ID>' // Static Value - Language(S)
            . ' <LANGU2></LANGU2>'
            . ' <TEXT_LINE>'. $this->getProductoDescripcion() .'</TEXT_LINE>' // Product Long Description
            . ' <ALT_UNIT>C/U</ALT_UNIT>'
            . ' <NUMERATOR>1</NUMERATOR>'
            . ' <DENOMINATR>1</DENOMINATR>'
            . ' <GROSS_WT>51</GROSS_WT>'
            . ' <UNIT_OF_WT2>KG</UNIT_OF_WT2>'
            . ' <ALT_UNIT2></ALT_UNIT2>'
            . ' <NUMERATOR2></NUMERATOR2>'
            . ' <DENOMINATR2></DENOMINATR2>'
            . ' <GROSS_WT22></GROSS_WT22>'
            . ' <UNIT_OF_WT22></UNIT_OF_WT22>'
            . ' <PLANT>'. $this->getCentroBodega() .'</PLANT>' // Warehouse Center Location(2001) for Lampa Dimerc
            . ' <PUR_GROUP>'. $this->getLineaJerarquia() .'</PUR_GROUP>' // LINEA DE PRODUCTO. SE PUEDE SACAR DE JERARQUÍA ~~~ Purchase Group Ex.: Office(D01), Tissue(D03), Technology(D05)
            . ' <ISSUE_UNIT></ISSUE_UNIT>'
            . ' <LOADINGGRP>0001</LOADINGGRP>' // Loading Group - Static Value
            . ' <PROFIT_CTR>DUMMY</PROFIT_CTR>' // Beneficiary Center
            . ' <AVAILCHECK>'. $this->getGrupoMaterial() .'</AVAILCHECK>' // GRUPO MATERIAL. 02 = HASTA AGOTAR STOCK ~~~ Availability
            . ' <PRICE_CTRL>'. $this->getControlDePrecio() .'</PRICE_CTRL>' // S = STANDAR | V = VARIABLE INTERNO ~~~ Static Value - Price control indicator
            . ' <MOVING_PR>'. $this->getPrecioVariable() .'</MOVING_PR>' // Medium Variable Price
            . ' <VAL_CLASS>M001</VAL_CLASS>' // Static Value - Valorization Category
            . ' <SALES_ORG>'. $this->getOrganizacionCompra() .'</SALES_ORG>' //  Dimerc(2000)
            . ' <DISTR_CHAN>10</DISTR_CHAN>' // Web(10)
            . ' <SALES_UNIT></SALES_UNIT>'
            . ' <ITEM_CAT2>BANS</ITEM_CAT2>' // Static Value - For Dropship is always BANS
            . ' <MAT_PR_GRP></MAT_PR_GRP>' // Material Price Group
            . ' <ACCT_ASSGT>01</ACCT_ASSGT>' // Imputation Group Material
            . ' <COMM_GROUP></COMM_GROUP>' // Comision Group - Ex.: A, B, C, E, F, G
            . ' <MATL_GRP_1>Z02</MATL_GRP_1>' // Z02 = Hasta agotar Stock ~~~ Dropship(Z01), Fulfillment(Z02)
            . ' <MATL_GRP_2>Z02</MATL_GRP_2>' // Z02 = Fulfillment ~~~ Dropship(Z01), Fulfillment(Z02)
            . ' <PROD_ATT_1></PROD_ATT_1>' //  Imported ?
            . ' <PROD_ATT_2></PROD_ATT_2>' //  Dimerc Brand ?
            . ' <PROD_ATT_3></PROD_ATT_3>' //  "A pedido" ?
            . ' <APPLOBJECT2></APPLOBJECT2>' //  Application Object
            . ' <TEXT_NAME2></TEXT_NAME2>' // ‘cod_material’ + sales_org +  distr_chan     TDOBNAME Nombre
            . ' <TEXT_ID2></TEXT_ID2>'
            . ' <LANGU22></LANGU22>'
            . ' <TEXT_LINE2></TEXT_LINE2>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>' // Static Value
            . ' <TAX_TYPE_1>ZMW1</TAX_TYPE_1>' //ZMW1 = Impuesto IVA | ZMW2 = ILA Licores y Pisco | ZWM3 = ILA Vinos y Cerverzas ~~~ Static Value
            . ' <TAXCLASS_1>1</TAXCLASS_1>' // 1 if Product is affected to TAX
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>'
            . ' <TAX_TYPE_2>ZMW1</TAX_TYPE_2>'
            . ' <TAXCLASS_2>1</TAXCLASS_2>'
            . ' </T_MLAN>'
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>'
            . ' <TAX_TYPE_2>ZMW2</TAX_TYPE_2>'
            . ' <TAXCLASS_2>0</TAXCLASS_2>'
            . ' </T_MLAN>'
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY> '
            . ' <TAX_TYPE_2>ZMW3</TAX_TYPE_2> '
            . ' <TAXCLASS_2>0</TAXCLASS_2> '
            . ' </T_MLAN>'
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>'
            . ' <TAX_TYPE_2>ZMW4</TAX_TYPE_2>'
            . ' <TAXCLASS_2>0</TAXCLASS_2>'
            . ' </T_MLAN>'
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>'
            . ' <TAX_TYPE_2>ZMW5</TAX_TYPE_2>'
            . ' <TAXCLASS_2>0</TAXCLASS_2>'
            . ' </T_MLAN>'
            . ' <T_MLAN>'
            . ' <DEPCOUNTRY>CL</DEPCOUNTRY>'
            . ' <TAX_TYPE_2>ZMW6</TAX_TYPE_2>'
            . ' <TAXCLASS_2>0</TAXCLASS_2>'
            . ' </T_MLAN>'
            . ' <COD_PROVEEDOR>'. $this->getCodigoProveedor() .'</COD_PROVEEDOR>' // SAP Vendor ID code
            . ' <COD_MATERIAL>'. $this->getCodigoMaterial() .'</COD_MATERIAL>' //  When modify a Dropship Product, include product SAP id here
            . ' <COD_ORG_COMPRA>'. $this->getOrganizacionCompra() .'</COD_ORG_COMPRA>' // Dimerc(2000)
            . ' <IND_REG_INFO>0</IND_REG_INFO> '
            . ' <CANT_STANDAR_PED>'. $this->getMultiploVenta() .'</CANT_STANDAR_PED> ' // Quantity
            . ' <PRECIO_COMPRA>'. $this->getPrecioCompra().'</PRECIO_COMPRA>' // Net Price
            . ' </urn:MT_MM022_REQ_OUT>'
            . ' </soapenv:Body>'
            . ' </soapenv:Envelope>';

        return $xml;
    }
}