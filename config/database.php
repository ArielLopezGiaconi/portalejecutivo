<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
        /* CONEXIONES ACTUALES */
        'oracle_dmventas' => (env('DATABASE_ENV', 'prod') == 'prod')
            ? // Producción
            array(
                'driver' => 'oracle',
                'host' => 'tnsdimprd.dimerc.cl',
                'port' => '1521',
                'database' => 'DM_VENTAS',
                'username' => 'DM_VENTAS',
                'password' => 'dimerc',
                'charset' => 'AL32UTF8',
                'prefix' => '',
                'strict' => false,
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
                'engine' => null,
                'service_name' => 'prod'
            )
            : // Desarrollo
            array(
                'driver' => 'oracle',
                'host' => 'dimqa.dimerc.cl',
                'port' => '1521',
                'database' => 'DM_VENTAS',
                'username' => 'DM_APPBI',
                'password' => 'DM_APPBI1784',
                'charset' => 'AL32UTF8',
                'prefix' => '',
                'strict' => false,
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
                'engine' => null,
                'service_name' => 'prod'
            )
        ,

        'oracle_ejecutivo' => (env('DATABASE_ENV', 'prod') == 'prod')
            ? // Producción
            array(
                'driver' => 'oracle',
                'host' => 'tnsdimprd.dimerc.cl',
                'port' => '1521',
                'database' => '',
                'username' => '',
                'password' => '',
                'charset' => 'AL32UTF8',
                'prefix' => '',
                'strict' => false,
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
                'engine' => null,
                'service_name' => 'prod'
            )
            : // Desarrollo
            array(
                'driver' => 'oracle',
                'host' => 'dimqa.dimerc.cl',
                'port' => '1521',
                'database' => '',
                'username' => '',
                'password' => '',
                'charset' => 'AL32UTF8',
                'prefix' => '',
                'strict' => false,
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
                'engine' => null,
                'service_name' => 'prod'
            )
        ,

        'oracle_dmventasQA' => array (
            'driver' => 'oracle',
            'host' => 'dimqa.dimerc.cl',
            'port' => '1521',
            'database' => 'DESA01',
            'username' => 'DM_ALOPEZ',
            'password' => 'DM_ALOPEZ',
            'charset' => 'AL32UTF8',
            'prefix' => '',
            'strict'    => false,
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
            'engine' => null,
            'service_name' => 'prod'
        ),
        
        'oracle_unificado' => (env('DATABASE_ENV', 'prod') == 'prod')
            ? // Producción
            array(
                'driver' => 'oracle',
                'host' => '10.10.40.210',
                'port' => '1521',
                'database' => 'HOLDING',
                'username' => 'HOLDING',
                'password' => 'sysadm',
                'charset' => 'utf8',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
                'service_name' => 'ora92',
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
            )
            : // Desarrollo
            array(
                'driver' => 'oracle',
                'host' => '10.10.20.188',
                'port' => '1521',
                'database' => 'HOLDING',
                'username' => 'HOLDING',
                'password' => 'sysadm',
                'charset' => 'utf8',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
                'service_name' => 'DESA03',
                PDO::ATTR_CASE => PDO::CASE_LOWER,
                'options' => [
                    PDO::ATTR_EMULATE_PREPARES => true,
                    PDO::ATTR_CASE => PDO::CASE_LOWER
                ],
            )
        ,

        'oracle_dmtransferweb' => array(
            'driver' => 'oracle',
            'host' => 'dimprd.dimerc.cl',
            'port' => '1521',
            'database' => 'DM_TRANSFER_WEB',
            'username' => 'DM_TRANSFER_WEB',
            'password' => 'dm_ventas',
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'service_name' => 'prod',
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
        ),

        'mysql_magentodimerc' => [
            'driver' => 'mysql',
            'host' => (env('DATABASE_ENV', 'dev') == 'dev') ? '192.168.0.2' : '10.10.201.101',
            'port' => env('DB_PORT', '3306'),
            'database' => 'dimerc',
            'username' => 'fburgos',
            'password' => '6aUJ83MNERO32s',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => false,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'mysql_portal_ejecutivo' => [
            'driver' => 'mysql',
            'host' => '192.168.10.51',
            'port' => '3306',
            'database' => 'portal_ejecutivo',
            'username' => 'root',
            'password' => 'd1mySQL20_18',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict'    => false,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'mysql_dashboard' => [
            'driver' => 'mysql',
            'host' => '192.168.10.51',
            'port' => '3306',
            'database' => 'dashboard',
            'username' => 'root',
            'password' => 'd1mySQL20_18',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict'    => false,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'mysql_magento_ofimarket' => [
            'driver' => 'mysql',
            // 'host' => (env('DATABASE_ENV', 'dev') == 'prod') ? '192.168.0.2' : '10.10.201.101',
            'host' => '10.10.201.101',
            'port' => env('DB_PORT', '3306'),
            'database' => 'ofimarket_production',
            'username' => 'fburgos',
            'password' => '6aUJ83MNERO32s',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => false,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'mysql_api_resources' => [
            'driver' => 'mysql',
            'host' => '10.10.201.110',
            'port' => '3306',
            'database' => 'api_resources',
            'username' => 'root',
            'password' => 'mysqlcerberusbi2017',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => false,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'peru_mysql_magento' => [
            'driver' => 'mysql',
            'host' => '192.168.51.207',
            'port' => env('DB_PORT', '3306'),
            'database' => 'dimerc',
            'username' => 'root',
            'password' => 'S0p$i$2018',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => false,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'peru_oracle_dmventas' => [
            'driver' => 'oracle',
            'host' => (env('DATABASE_ENV', 'dev') == 'prod') ? '192.168.51.211' : '192.168.51.222',
            'port' => (env('DATABASE_ENV', 'dev') == 'prod') ? '1526' : '1521',
            'database' => 'DM_VENTAS',
            'username' => 'DM_VENTAS',
            'password' => 'dm_ventas',
            'charset' => 'utf8',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'service_name' => (env('DATABASE_ENV', 'dev') == 'prod') ? 'PPROD' : 'DPROD',
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            'options' => [
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
        ],




        /* CONEXIONES ANTIGUAS */
        /*
        'mysql_cerberus_marketingdigital' => [
            'driver' => 'mysql',
            'host' => '10.10.201.110',
            'port' => '3306',
            'database' => 'marketing_digital',
            'username' => 'root',
            'password' => 'mysqlcerberusbi2017',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict'    => false,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'mysql_portalejecutivo' => [
            'driver' => 'mysql',
            'host' => '10.10.127.43',
            'port' => '3306',
            'database' => 'portalejecutivo',
            'username' => 'root',
            'password' => 'Pal0Blanc0',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'strict'    => false,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true
            ],
            'engine' => null,
        ],

        'oracle_dmventas' => array (
            'driver' => 'oracle',
            'host' => '10.10.20.1',
            'port' => '1521',
            'database' => 'DM_VENTAS',
            'username' => 'DM_VENTAS',
            'password' => 'dimerc',
            'charset' => 'AL32UTF8',
            'prefix' => '',
            'strict'    => false,
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
            'engine' => null,
            'service_name' => 'prod'
        ),

        'oracle_dmventas_dev' => array(
            'driver' => 'oracle',
            'host' => '10.10.180.152',
            'port' => '1521',
            'database' => 'DM_VENTAS',
            'username' => 'DM_VENTAS',
            'password' => 'dimerc',
            'charset' => 'AL32UTF8',
            'prefix' => '',
            'strict'    => false,
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            'options'   => [
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER
            ],
            'engine' => null,
            'service_name' => 'DESA01'
        ),*/
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],


];
