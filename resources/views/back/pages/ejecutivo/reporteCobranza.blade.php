@extends('back.master.masterpage')
@section('contenido')

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-money text-inverse f-50"></i>  
                        </div>
                        <div class="col-sm-6">
                            <h3>Reporte de cobranza</h3>
                            <span class="f-16">Detalle - Reporte de cobranza | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        <div class="col-sm-4">
                        </div> 
                    </div>
                </div> 

                <div class="card-block" style="">
                    
                    <div class="row">
                        <div class="">
                           <table class=" table-hover display nowrap dataTable">
                            <thead class="table-hover col-12">
                                <tr class="table-inverse">
                                        <th class="width-250" colspan="1"></th>
                                        <th class="width-250" colspan="1"></th>
                                        <th class="width-250" colspan="1"></th>
                                        <th class="width-250" colspan="1"></th>
                                        <th class="width-250" colspan="1"></th>
                                        <th class="width-250" colspan="1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left font-weight-bold">Cobrador: </td>
                                        <td class="text-left" id="cobrador"></td>
                                        <td class="text-left font-weight-bold">Mail: </td>
                                        <td class="text-left" id="mail_cobrador"></td>
                                        <td class="text-left font-weight-bold">Anexo: </td>
                                        <td class="text-left" id="anexo"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Contacto cobranza: </td>
                                        <td class="text-left" id="idContacCob"></td>
                                        <td class="text-left font-weight-bold">Celular contacto: </td>
                                        <td class="text-left" id="idTelCob"></td>
                                        <td class="text-left font-weight-bold">Email contacto: </td>
                                        <td class="text-left" id="idEmailcob"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Status Credito: </td>
                                        <td class="text-left" id="status"></td>
                                        <td class="text-left font-weight-bold">Credito: </td>
                                        <td class="text-left" id="credito"></td>
                                        <td class="text-left font-weight-bold">Mora actual: </td>
                                        <td class="text-left" id="mora"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
             
            <div id="CC" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">(C.C) Centro de Costo</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Centro de Costo que realiza la cotización y compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div id="alerta" style="display: none" class="alert alert-danger background-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        {{-- <i class="icofont icofont-close-line-circled text-white"></i> --}}
                    </button>
                    <strong>No hay documentos con deuda a la fecha.</strong>
                </div>
                <div id="alerta2" style="display: none" class="alert alert-warning background-warning">
                    <button type="button" class="close" data-dismiss="alert">
                        {{-- <i class="icofont icofont-close-line-circled text-white"></i> --}}
                    </button>
                    <strong>Cliente no existe o no tiene permiso para visualizarlo.</strong>
                </div>
                <div class="table-responsive">
                    {{-- <h5 class="sub-title m-b-15">Ordenes Web</h5> --}}
                    <div class="dt-buttons">
                        <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="detallenota" onclick="imprimir()">
                            <span>Descargar a pdf</span>
                        </a>
                    </div>
                    <div id="detalle" onkeyup="Formato()"></div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-cloud-download";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Ordenes Web";


            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            
            getDerechos();
            
            var rutcli = '{{ $rutcli }}';
            Buscar(rutcli);
            getReporteCobranzaDatos(rutcli);
            buscaCliente(rutcli);
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function imprimir(){
            var rutcli = '{{ $rutcli }}';
            location.href = window.location.href.replace('reporteCobranza/' ,'imprimir/?valor=');
        }

        function buscaCliente(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            $.ajax({
                url: '{{ url('getDatosCliente') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    console.log(json.response);
                    if (json.code == 200) {
                        document.getElementById("status").innerHTML = json.response[0].status_credito;
                        document.getElementById("credito").innerHTML = json.response[0].credito;
                        document.getElementById("mora").innerHTML = json.response[0].deuda;
                        // if (json.response[0].status_credito == "Activo") {
                        //     $("#statusCredito").css({ 'font-weight': 'bold'});
                        //     $("#statusCredito").css('background','#32FF00');
                        //     //$("#statusCredito").css({ 'color': 'green', 'font-weight': 'bold'})
                        // }else{
                        //     //$("#statusCredito").css({ 'color': 'red'})
                        //     $("#statusCredito").css({ 'font-weight': 'bold'});
                        //     $("#statusCredito").css('background','#FF0000');
                        // }
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function getReporteCobranzaDatos(rut) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rut,
                cencos: 0
            };
            //console.log(parametros);
            $.ajax({
                url: '{{ url('/pricing/reporteCobranzaDatos') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        document.getElementById("cobrador").innerHTML = json.response[0].cobrador;
                        document.getElementById("mail_cobrador").innerHTML = json.response[0].mail_cobrador;
                        document.getElementById("anexo").innerHTML = json.response[0].anexo_cobrador;
                        document.getElementById("idContacCob").innerHTML = json.response[0].cobranza;
                        document.getElementById("idTelCob").innerHTML = json.response[0].telcob;
                        document.getElementById("idEmailcob").innerHTML = json.response[0].mail_cob;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }
        
        function FichaCliente(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=16;
            var actionid=41;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                    var cells = this.getElementsByTagName("td");
                    location.href = window.location.href.replace('/notasWeb','') + "/fichacliente/" + rut;
                };
            };
        }

        function Orden(){
            Formato();
        }

        function BuscarRut(){
            Buscar(document.getElementById("rut").value);
        }

        function volver(){
            var rutcli = '{{ $rutcli }}';
            location.href = window.location.href.replace('reporteCobranza','fichacliente');
        }

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[5];
                $(td).addClass('text-right');
                text = $(td).text().substring();
                $(td).text(text.substr(0,10));
                $(td).text(text.substr(8,2) + "-" + text.substr(5,2) + "-" + text.substr(0,4));

                td = tr[i].getElementsByTagName("td")[6];
                $(td).addClass('text-right');
                text = $(td).text().substring();
                $(td).text(text.substr(0,10));
                $(td).text(text.substr(8,2) + "-" + text.substr(5,2) + "-" + text.substr(0,4));

                td = tr[i].getElementsByTagName("td")[7];
                if ($(td).text().substring(0,1) != '$'){
                    $(td).text('$' + number_format( $(td).text()));
                    $(td).addClass('text-right');
                }
            }
        }

        function Buscar(rut) {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Cliente</th>\n' + 
                '                            <th>Rut</th>\n' +
                '                            <th>Nombre deudor</th>\n' +
                '                            <th>Factura</th>\n' +
                '                            <th>Cto. Costo</th>\n' +
                '                            <th>Fecha Emisión</th>\n' + 
                '                            <th>Fecha vencimiento</th>\n' + 
                '                            <th>Total</th>\n' + 
                '                            <th>Promesa</th>\n' + 
                '                            <th>OC</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>\n' ;
            document.getElementById("detalle").innerHTML = table;
            
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rut
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/reporteCobranza')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json);
                    if (json == false) {
                        document.getElementById("alerta2").style.display = "block";
                            setTimeout(function() {
                                document.getElementById("alerta2").style.display = "none"
                                }, 5000);
                    }else{
                        if (json.response.length > 0) {
                            var datatable = $("#detallenota").DataTable({
                                data: json.response,
                                columns: [
                                    {data: "id_cliente_sap",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data+ '</span>\n';}},
                                    {data: "rutcli",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data+ '</span>\n';}},
                                    {data: "razons",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data+ '</span>\n';}},
                                    {data: "factura",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data+ '</span>\n';}},
                                    {data: "cencos",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data + '</span>\n';}},
                                    {data: "fecemi",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data + '</span>\n';}},
                                    {data: "fecven",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "totgen",
                                        render: function (data, type, row, meta) {
                                        return data;}},
                                    {data: "promesa",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "doc_asociados",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data + '</span>\n';}}
                                ],
                                "scrollX": true,
                                "scrollY": 400,
                                "iDisplayLength": 100,
                                "ColumnsDef": [
                                    { "sWidth": "300px", "aTargets": [ 9 ] }
                                ],
                                "fixedHeader": true,
                                dom: 'Bfrtip',
                                buttons: [
                                    { extend:'excelHtml5', text: 'Descargar a excel'},
                                    // { extend:'pdfHtml5', text: 'Descargar a PDF'}
                                ]
                                ,
                                language: {
                                    buttons: {
                                        copyTitle: 'Copiado en el portapapeles',
                                        copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                        copySuccess: {
                                            _: '%d filas copiadas al portapapeles',
                                            1: '1 fila copiada al portapapeles'
                                        }
                                    },
                                    "lengthMenu": "Mostrando _MENU_ registros por página",
                                    "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                    "infoFiltered": "(filtrado de _MAX_ registros)",
                                    "search": "Buscar:",
                                    "paginate": {
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    }
                                }
                            });
                            $("#spinner").hide();
                            Orden();
                        } else {
                            document.getElementById("alerta").style.display = "block";
                            setTimeout(function() {
                                document.getElementById("alerta").style.display = "none"
                                }, 5000);
                            $("#spinner").hide();
                        }
                    }
                }
                , error: function (e) {
                    console.log('Error:' + e.message);
                }
            });
        }

    </script>
@endsection