@php
    /* Obtener foto perfil */
     $rutusuario = Session::get('usuario')->get('rutusu');
     if(is_null(env('CUSTOM_PUBLIC_PATH'))) {
        $fotoperfil = public_path().'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
        }else {
            $fotoperfil = env('CUSTOM_PUBLIC_PATH').'/assets/images/fotoperfil/'. $rutusuario . '.jpg';
        }
        
        if(file_exists($fotoperfil)){
            $fotoperfil = asset('assets/images/fotoperfil/'.$rutusuario.'.jpg');
        }
        else{
            $fotoperfil = asset('assets/images/default1.jpg');
        }
@endphp
<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
    
        <div class="pcoded-navigatio-lavel">Ejecutivo</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{ url('/') }}">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Portada</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('cartera') }}">
                    <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                    <span class="pcoded-mtext">Cartera</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('cotizaciones') }}">
                    <span class="pcoded-micon"><i class="ion-document-text"></i></span>
                    <span class="pcoded-mtext">Cotizaciones</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('notasWeb') }}">
                    <span class="pcoded-micon"><i class="fa fa-cloud-download"></i></span>
                    <span class="pcoded-mtext">Ordenes Web</span>
                    <span id="Pendientes" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            <li class="">
                <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather ion-clipboard"></i></span>
                        <span class="pcoded-mtext">Notas de Venta</span>
                        <span id="NotasPendientes" style="display:none" class="pcoded-badge label label-danger"></span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class=" ">
                            <a href="{{ url('notas') }}">
                                <span class="pcoded-mtext">Consulta notas</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">Sin documentar</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </li>

            <li class="">
                <a href="{{ url('ingresosWeb') }}">
                    <span class="pcoded-micon"><i class="ion-clipboard"></i></span>
                    <span class="pcoded-mtext">Ingresos Web</span>
                    <span id="IngresosWeb" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            
            <li class="">
                <a href="focos">
                    <span class="pcoded-micon"><i class="zmdi zmdi-lamp"></i></span>
                    <span class="pcoded-mtext">Focos</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('acciones') }}">
                    <span class="pcoded-micon"><i class="fa fa-hand-rock-o"></i></span>
                    <span class="pcoded-mtext">Acc. Agendadas</span>
                    <span id="Acciones" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            {{-- <li>
                <a href="{{ url('chat') }}">
                    <span class="pcoded-micon"><i class="fa fa-wechat"></i></span>
                    <span class="pcoded-mtext">Chat</span>
                </a>
            </li> --}}
            {{-- <li class="">
                <a href="{{ url('ranking') }}">
                    <span class="pcoded-micon"><i class="ion-trophy"></i></span>
                    <span class="pcoded-mtext">Ranking</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('campanas') }}">
                    <span class="pcoded-micon"><i class="ti-money"></i></span>
                    <span class="pcoded-mtext">Campañas</span>
                </a>
            </li> --}}
            
{{--            <li class="">--}}
{{--                <a href="{{ url('briefing') }}">--}}
{{--                    <span class="pcoded-micon"><i class="feather icon-monitor"></i></span>--}}
{{--                    <span class="pcoded-mtext">Briefing</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <!--<li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-scissors"></i><b>S</b></span>
                    <span class="pcoded-mtext">Herramientas</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="search-result.htm">
                            <span class="pcoded-mtext">Mi chat</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="search-result.htm">
                            <span class="pcoded-mtext">Mis notas</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="search-result2.htm">
                            <span class="pcoded-mtext">Mis tareas</span>
                        </a>
                    </li>
                </ul>
            </li>-->
        </ul>
        {{-- <div class="pcoded-navigatio-lavel">Cliente</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{ url('fichacliente') }}">
                    <span class="pcoded-micon"><i class="fa fa-folder-open"></i></span>
                    <span class="pcoded-mtext">Ficha cliente</span>
                </a>
            </li> --}}
{{--            <li class="">--}}
{{--                <a href="{{ url('generacotizacion') }}">--}}
{{--                    <span class="pcoded-micon"><i class="fa fa-edit"></i></span>--}}
{{--                    <span class="pcoded-mtext">Generar cotización</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            {{-- <li class="">
                <a href="{{ url('visitascliente') }}">
                    <span class="pcoded-micon"><i class="icofont icofont-foot-print"></i></span>
                    <span class="pcoded-mtext">Visitas a cliente</span>
                </a>
            </li> --}}
        </ul>
        <div class="pcoded-navigatio-lavel">Reportes</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{ url('GestionMes') }}">
                    <span class="pcoded-micon"><i class="icofont icofont-open-eye"></i></span>
                    <span class="pcoded-mtext">Mis Indicadores</span>
                    <span id="Acciones" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            <li id="indicadores" class="" style="display:none">
                <a href="{{ url('Indicadores') }}">
                    <span class="pcoded-micon"><i class="fa fa-table"></i></span>
                    <span class="pcoded-mtext">Indicadores</span>
                    <span id="Acciones" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            <li id="productividad" class="" style="display:none">
                <a href="{{ url('ReporteProductividad') }}">
                    <span class="pcoded-micon"><i class="fa fa-black-tie"></i></span>
                    <span class="pcoded-mtext">Reporte productividad FFVV</span>
                    <span id="Acciones" style="display:none" class="pcoded-badge label label-danger"></span>
                </a>
            </li>
            <li>
                <a class="btn" role="button" onclick="campaña()">
                    <span class="pcoded-micon"><i class="ti-pulse"></i></span>
                    <span class="pcoded-mtext">Campaña</span>
                </a>
                <form id="formCamp" action="http://192.168.10.58/Prd/I3/BI/Campana%20Noviembre%202020.php" method="post" >
                    <input type="text" style="display:none" name="usuario" id="user" value="pgarzon">
                    <input type="password" style="display:none" name="clave" id="clave" value="mendoza5">
                    <button type="submit" style="display:none" id="enviar">Enviar</button>
                </form>
            </li> 
            <li class="">
                <a href="{{ url('comisiones') }}">
                    <span class="pcoded-micon"><i class="fa fa-usd"></i></span>
                    <span class="pcoded-mtext">Mis Comisiones</span>
                </a>
            </li>
            {{-- <li class="">
                <a href="{{ url('Gobierno') }}">
                    <span class="pcoded-micon"><i class="fa fa-usd"></i></span>
                    <span class="pcoded-mtext">Reportes Gobierno</span>
                </a>
            </li> --}}
            {{-- <li class="">
                <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather ion-clipboard"></i></span>
                        <span class="pcoded-mtext">Gobierno</span>
                        <span id="NotasPendientes" style="display:none" class="pcoded-badge label label-danger"></span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class=" ">
                            <a href="{{ url('notas') }}">
                                <span class="pcoded-mtext">Consumo por linea mensual</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">Consumo por linea ultimo trimestre</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">top 10 sku con mas cosumo mes en curso</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">top 10 sku con mas consumo trimestral</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">top 10 cc con mas pedidos mes en curso</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">top 10 cc con mas consumo mes en curso</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">top 5 sku con mas consumo de los 10 cc con mas compra</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">consumo marca propia ultimo trimestre</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">Venta Convenio</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">sku agregados a convenio</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">sku de convenio sin consumo ultimo trimestre</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">venta generada por la web</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">Cobranza deuda</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{ url('notasPedientes') }}">
                                <span class="pcoded-mtext">Cobranza notas de credito</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </li> --}}
            <li id="cuarentenas" class="" style="display:none">
                <a href="{{ url('cuarentenas') }}">
                    <span class="pcoded-micon"><i class="fa fa-usd"></i></span>
                    <span class="pcoded-mtext">Venta y cuarentenas</span>
                </a>
            </li>

            <div class="pcoded-navigatio-lavel">Sitios</div>
            <li class="">
                <a href="{{ url('manual') }}" target="_blank">
                    <span class="pcoded-micon"><i class="feather ion-help"></i></span>
                    <span class="pcoded-mtext">Manual</span>
                </a>
            </li>
            <li class="">
                <a href="https://empresasdimerc.izytimecontrol.com/#/dimercautoconsulta" target="_blank">
                    <span class="pcoded-micon"><i class="feather icon-clock"></i></span>
                    <span class="pcoded-mtext">Minuta casino</span>
                </a>
            </li>
            <li class="">
                <a href="https://dimercti.sladesk.com/login" target="_blank">
                    <span class="pcoded-micon"><i class="feather icon-check-square"></i></span>
                    <span class="pcoded-mtext">Requerimientos TI</span>
                    <!--<span class="pcoded-badge label label-warning">NEW</span>-->
                </a>
            </li>
            <li class="">
                <a href="https://dimerc.sladesk.com/login" target="_blank">
                    <span class="pcoded-micon"><i class="feather icon-check-square"></i></span>
                    <span class="pcoded-mtext">Requerimientos</span>
                    <!--<span class="pcoded-badge label label-warning">NEW</span>-->
                </a>
            </li>
            <li class="">
                <a href="http://192.168.10.57/gesclientes_nuevo3/" target="_blank">
                    <span class="pcoded-micon"><i class="feather icon-edit-2"></i></span>
                    <span class="pcoded-mtext">Portal gestión clientes</span>
                    <!--<span class="pcoded-badge label label-warning">NEW</span>-->
                </a>
            </li>
        </ul>
    </div>
    <table id="tablaDerechos" style="display: block"></table>
</nav>

<script>
    function CambioPagina(valor){
        if (valor == 1){
            CambioClave(document.getElementById("paginaClave").value, valor, document.getElementById("claveOld").value, document.getElementById("claveNew").value);
        }else if(valor == 2){
            CambioClave(document.getElementById("paginaClave").value, valor, document.getElementById("claveOld").value, document.getElementById("claveNew").value);
        }else if(valor == 3){
            CambioClave(document.getElementById("paginaClave").value, valor, document.getElementById("claveOld").value, document.getElementById("claveNew").value);
        }else if(valor == 4){
            CambioClave(document.getElementById("paginaClave").value, valor, document.getElementById("claveOld").value, document.getElementById("claveNew").value);
        }else if(valor == 5){
            CambioClave(document.getElementById("paginaClave").value, valor, document.getElementById("claveOld").value, document.getElementById("claveNew").value);
        }
    }

    function CuadroCabiarClave(){
        document.getElementById("cuadroClave").click();
    }

    function previos(valor){
        if(valor == 1){
            document.getElementById("Pagina1").click();
        }else if(valor == 2){
            document.getElementById("Pagina2").click();
        }
    }

    function CambioClave(pagina, tipo, claveOld, claveNew) {
        var token = '{{ csrf_token() }}';

        if(tipo == 4){
            document.getElementById("cuadroConfirmacion").click();
        }

        var parametros = {
            _token: token,
            tipo: tipo,
            claveOld: claveOld,
            claveNew: claveNew
        };
        console.log(parametros);
        $.ajax({
            data: parametros,
            url: '{{url('/pricing/cambioClave')}}',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                console.log(json.response);
                if(tipo == 2){
                    if (json.response == 0){
                        $("#claveOld").removeClass("bg-c-green");
                        $("#claveOld").addClass("bg-c-pink");
                        document.getElementById("bueno").style.display = "none";
                        document.getElementById("malo").style.display = "block";
                        document.getElementById("errorClave").style.display = "block";
                    }else{
                        $("#claveOld").removeClass("bg-c-pink");
                        $("#claveOld").addClass("bg-c-green");
                        document.getElementById("bueno").style.display = "block";
                        document.getElementById("malo").style.display = "none";
                        document.getElementById("Pagina2").click();
                    }
                    
                }else if(tipo == 3){
                    document.getElementById("Pagina3").click();
                }else if(tipo == 5){
                    document.getElementById("btn_salir").click();
                }
            }
            , error: function (e) {
                console.log(e.message);
            }
        });
    }

    function ValidaNewContraseñaNew(){
        if(document.getElementById("claveNew1").value == document.getElementById("claveNew").value){
            console.log('bueno');
            $("#claveNew1").removeClass("bg-c-pink");
            $("#claveNew").removeClass("bg-c-pink");
            $("#claveNew1").addClass("bg-c-green");
            $("#claveNew").addClass("bg-c-green");
            document.getElementById("bueno1").style.display = "block";
            document.getElementById("bueno2").style.display = "block";
            document.getElementById("malo1").style.display = "none";
            document.getElementById("malo2").style.display = "none";
        }else{
            console.log('malo');
            $("#claveNew1").removeClass("bg-c-green");
            $("#claveNew").removeClass("bg-c-green");
            $("#claveNew1").addClass("bg-c-pink");
            $("#claveNew").addClass("bg-c-pink");
            document.getElementById("bueno1").style.display = "none";
            document.getElementById("bueno2").style.display = "none";
            document.getElementById("malo1").style.display = "block";
            document.getElementById("malo2").style.display = "block";
        }
    }

    function minimiza(){
        if(document.getElementById("chat").style.display == "none"){
            document.getElementById("chat").style.display = "block";
            document.getElementById("chat_min").style.display = "none";
        }else {
            document.getElementById("chat").style.display = "none";
            document.getElementById("chat_min").style.display = "block";
        }
    }

    function cerrar(){
        document.getElementById("chat").style.display = "none";
        document.getElementById("chat_min").style.display = "none";
        document.getElementById("bodyChat").innerHTML = '';
    }

    function Abrir(){
        if(document.getElementById("chat").style.display == "none"){
            document.getElementById("chat").style.display = "block";
        }
    }

    function InputChatEnter(e){
        if(e.keyCode === 13 && !e.shiftKe){
            InputChat();
        }
    }
    
    function InputChat(){
        if(document.getElementById("newMessage").value.trim() != ''){
            var texto = document.getElementById("bodyChat").innerHTML;
            texto = texto + ' <div class="messages">\n' + 
                            '    <div class="message out no-avatar media">\n' +
                            '        <div class="body media-body text-right p-l-50">\n' +
                            '            <div style="background-color: rgb(0, 132, 255)" class="content msg-reply f-14 text-white">' + document.getElementById("newMessage").value + '</div>\n' +
                            '            <div class="seen">\n' +
                            '                <i class="icon-clock f-12 m-r-5 "></i>\n' +
                            '                <span class="f-12">a few seconds ago </span>\n' +
                            '                <div class="clear"></div>\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '        <div class="sender media-right friend-box">\n' +
                            '            <a href="javascript:void(0);" title="Me">\n' +
                            '                <img src="{{ $fotoperfil }}" class="img-chat-profile" alt="Me">\n' +
                            '            </a>\n' +
                            '        </div>\n' +
                            '    </div>\n' +
                            '</div>';
            document.getElementById("bodyChat").innerHTML = texto;
            document.getElementById("newMessage").value = '';
            document.getElementById("body").scroll(0,100000);
        }
    }

    function OutPutChat(){
        document.getElementById("chat").style.display = "block";
    }

    function pendientes() {
        var token = '{{ csrf_token() }}';
        var date = new Date();
        var lafecha = '';
        
        lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-01';
        var token = '{{ csrf_token() }}';

        var parametros = {
            _token: token,
            fecha: lafecha
        };
        $.ajax({
            data: parametros,
            url: '{{url('/pricing/getNotasWebQ')}}',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                //console.log(json.response);
                if (json.response[0].numero == 0) {
                    document.getElementById("Pendientes").style.display = "none";
                    //console.log(json.response);
                }else {
                    document.getElementById("Pendientes").style.display = "block";
                    document.getElementById("Pendientes").innerText = json.response[0].numero;
                    //console.log(json.response);
                }
            }
            , error: function (e) {
                console.log(e.message);
            }
        });
    }

        function NotasPendientes() {
            var token = '{{ csrf_token() }}';
            var date = new Date();
            var lafecha = '';
            
            lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-01';
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha
            };
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/getNotasSinDocumentarQ')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (json.response[0].qnotas == 0) {
                        document.getElementById("NotasPendientes").style.display = "none";
                        //console.log(json.response);
                    }else {
                        document.getElementById("NotasPendientes").style.display = "block";
                        document.getElementById("NotasPendientes").innerText = json.response[0].qnotas;
                        // console.log(json.response[0]);
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function IngresosWeb() {
            var token = '{{ csrf_token() }}';
            var date = new Date();
            var lafecha = '';
            
            lafecha = date.getFullYear() + '-' + (date.getMonth()-1) + '-01';
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/ingresosWebQ')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response[0].numero);
                    if (json.response[0].cantidad == 0) {
                        document.getElementById("IngresosWeb").style.display = "none";
                        //console.log(json.response[0].numero);
                    }else {
                        document.getElementById("IngresosWeb").style.display = "block";
                        document.getElementById("IngresosWeb").innerText = json.response[0].cantidad;
                    }
                    AccionesQ();
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function AccionesQ() {
            var token = '{{ csrf_token() }}';
            var date = new Date();
            var lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + (date.getDate());
            
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/accionesQ')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log('acciones' + json.response);
                    if (json.response[0].cantidad == 0) {
                        document.getElementById("Acciones").style.display = "none";
                        //console.log(json.response[0].numero);
                    }else {
                        document.getElementById("Acciones").style.display = "block";
                        document.getElementById("Acciones").innerText = json.response[0].cantidad;
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function campaña() {
            ObtieneDatos();
        }

        function ObtieneDatos() {
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token
            };

            $.ajax({
                data: parametros,
                url: '{{url('/pricing/ObtieneDatos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    EviarCampaña(json.response.codusu, json.response.clave);
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function registro(_userid, _moduleid, _actionid, _oldvalue, _newvalue) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                userid: _userid,
                moduleid: _moduleid,
                actionid: _actionid,
                oldvalue: _oldvalue,
                newvalue: _newvalue,
                updaterecord: ''
            };
            //console.log('los parametros' + parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/bitacora')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getDerechos() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('getDerechos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    sessionStorage.clear();
                    console.log(json.response);
                        for (i = 0; i < json.response.length; i++) {  
                            sessionStorage.setItem(json.response[i].codder,json.response[i].codder);
                            if ( json.response[i].codder == '40004' ) {
                                document.getElementById("indicadores").style.display = "block"; 
                                document.getElementById("productividad").style.display = "block"; 
                                document.getElementById("cuarentenas").style.display = "block";
                            }
                        }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function EviarCampaña(elusuario, laclave) {
            document.getElementById("clave").value = laclave;
            document.getElementById("user").value = elusuario;
            document.getElementById("enviar").click();
        }

        function number_format(amount, decimals) {
            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0) 
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }

        function stickyColumns() {
            // Número de elementos que se mantendran fijos
            var stickElements = $('.sticky-column');   
            
            // Variable que permite identificar el numero de columnas a fijar
            var totalColumns = $('thead .sticky-column').length;
            
            // Formula para identificar la primer columnas
            // Si el total de columnas es 2 obtendremos (2n-1)  
            var firstColumn = totalColumns+'n'+'-'+(totalColumns-1);
            
            // Formula para identificar la segunda columna
            // Si el total de columnas es 2 obtendremos (2n+2)
            var secondColum = totalColumns+'n'+'+'+(totalColumns);
            
            // Obtenemos el ancho de la primer elemeto de la primera columna
            // este elemento no sirve para posicionar la primera columna totalmente a la izquierda
            var firstElement = $('tbody .sticky-column:first-child()').outerWidth();
            
            stickElements.each(function(){ 
            // Si el elemento a fijar tiene el valor del primer elemento de la primera columan 
            // Posicionamos los elementos totalmente a la izquierda
            if($(this).outerWidth() == firstElement){ 
                $(this).css('left','0px');
            } else {
                // Si no lo posicionamos a X(tamaño del primer elemento) pixeles a la izquierda
                $(this).css({
                'left': firstElement,
                'width': '220px'
                });
            }
            });
            
            // Mueve los elementos fijos al realizar el scroll de la tabla
            $('.fixed-table').scroll(function() {
                $('.sticky-column:nth-child('+firstColumn+')').css('left',$(this).scrollLeft());
                $('.sticky-column:nth-child('+secondColum+')').css('left',firstElement+$(this).scrollLeft());
            });
        }

        function Subir() {
            var formData = new FormData();
            var files = $('#image')[0].files[0];
            var files2 = document.getElementById("rutusu").value;
            //console.log(document.getElementById("fotoicono").src);
            formData.append(files2, files);
            //formData.append('file',files2);
            $.ajax({
                url: 'upload.php',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 1) {
                        //$(".card-img-top").attr("src", response);
                        //console.log(response);
                        alert('Error la imagen debe ser como maximo de 2 mb.');
                    } else {
                        if (response == 2) {
                            //alert('Error formato no soportado, la imagen debe ser ".jpg" .');
                            alert('Error al grabar imagen.');
                        }else{
                            if (response == 0) {
                                alert('Error al grabar imagen.');
                            }else{
                                //$("#fotoPerfil").attr("src", response);
                                //$("#fotoicono").attr("src", response);
                                document.getElementById("fotoPerfil").src = document.getElementById("linkfoto").value;
                                document.getElementById("fotoicono").src = document.getElementById("linkfoto").value;
                            }
                        }
                    }
                    document.getElementById("image").value = '';
                    
                }
            });
        }

        function subirImagen() {
            console.log(document.getElementById("image").value);
            if(document.getElementById("image").value != ''){
                //document.getElementById("enviaFoto").click();
                Subir();
            }
        }

</script>
