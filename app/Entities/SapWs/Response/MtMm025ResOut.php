<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 05/09/2018
 * Time: 15:54
 */

namespace App\Entities\SapWs\Response;

use App\Entities\SapWs\Response\ResOut;
use Illuminate\Database\Query\Builder;
use DB;
use Orchestra\Parser\Xml\Facade as XmlParser;
use SimpleXMLElement;

class MtMm025ResOut extends ResOut
{
    private $xml_return;

    public function __construct($xml_string)
    {
        $this->many_products = false;

        // Cargar XML
        $this->xml_return = simplexml_load_string($xml_string)
            ->children('SOAP', true)->Body
            ->children('ns0', true)->MT_MM025_RES_OUT;

        /*foreach ($this->xml_return->xpath('T_RESPONSE') as $elementoReturn) {
            $this->xml_return = new SimpleXMLElement($elementoReturn->asXML());
        }*/
    }

    public function getMensajeReturn()
    {
        if ($this->xml_return == null) {
            return null;
        }

        return $this->xml_return->MSG . '';
    }
}