<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 03-05-2019
 * Time: 16:40
 */

namespace App\Entities\Oracle\DMVentas;

class ReDDesCli implements \JsonSerializable
{
    private $rutcli;
    private $coddir;
    private $dirdes;
    private $codcom;
    private $codciu;
    private $codest;
    private $codsec;
    private $codubi;
    private $codseq;
    private $codreg;
    private $ubicacion;
    private $pagina;
    private $fila;
    private $columna;
    private $id;
    private $mapcity;
    private $frecuencia;
    private $coddir_unificado;
    private $fecha_ult_vta;
    private $destino;
    private $estado_mapcity;
    private $jaula;
    private $dirdes_old;
    private $latitud;
    private $longitud;
    private $jaula_sucursal;
    private $codloc;
    private $fecha_creac;
    private $id_cliente_sap;
    private $tipo_via;
    private $nombre_via;
    private $numero;
    private $piso;
    private $depto;
    private $codigo_postal;
    private $validado_formulario;
    private $jaula_dux;
    private $confirmado_rounting;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->rutcli = isset($registro->rutcli) ? $registro->rutcli : null;
            $this->coddir = isset($registro->coddir) ? $registro->coddir : null;
            $this->dirdes = isset($registro->dirdes) ? $registro->dirdes : null;
            $this->codcom = isset($registro->codcom) ? $registro->codcom : null;
            $this->codciu = isset($registro->codciu) ? $registro->codciu : null;
            $this->codest = isset($registro->codest) ? $registro->codest : null;
            $this->codsec = isset($registro->codsec) ? $registro->codsec : null;
            $this->codubi = isset($registro->codubi) ? $registro->codubi : null;
            $this->codseq = isset($registro->codseq) ? $registro->codseq : null;
            $this->codreg = isset($registro->codreg) ? $registro->codreg : null;
            $this->ubicacion = isset($registro->ubicacion) ? $registro->ubicacion : null;
            $this->pagina = isset($registro->pagina) ? $registro->pagina : null;
            $this->fila = isset($registro->fila) ? $registro->fila : null;
            $this->columna = isset($registro->columna) ? $registro->columna : null;
            $this->id = isset($registro->id) ? $registro->id : null;
            $this->mapcity = isset($registro->mapcity) ? $registro->mapcity : null;
            $this->frecuencia = isset($registro->frecuencia) ? $registro->frecuencia : null;
            $this->coddir_unificado = isset($registro->coddir_unificado) ? $registro->coddir_unificado : null;
            $this->fecha_ult_vta = isset($registro->fecha_ult_vta) ? $registro->fecha_ult_vta : null;
            $this->destino = isset($registro->destino) ? $registro->destino : null;
            $this->estado_mapcity = isset($registro->estado_mapcity) ? $registro->estado_mapcity : null;
            $this->jaula = isset($registro->jaula) ? $registro->jaula : null;
            $this->dirdes_old = isset($registro->dirdes_old) ? $registro->dirdes_old : null;
            $this->latitud = isset($registro->latitud) ? $registro->latitud : null;
            $this->longitud = isset($registro->longitud) ? $registro->longitud : null;
            $this->jaula_sucursal = isset($registro->jaula_sucursal) ? $registro->jaula_sucursal : null;
            $this->codloc = isset($registro->codloc) ? $registro->codloc : null;
            $this->fecha_creac = isset($registro->fecha_creac) ? $registro->fecha_creac : null;
            $this->id_cliente_sap = isset($registro->id_cliente_sap) ? $registro->id_cliente_sap : null;
            $this->tipo_via = isset($registro->tipo_via) ? $registro->tipo_via : null;
            $this->nombre_via = isset($registro->nombre_via) ? $registro->nombre_via : null;
            $this->numero = isset($registro->numero) ? $registro->numero : null;
            $this->piso = isset($registro->piso) ? $registro->piso : null;
            $this->depto = isset($registro->depto) ? $registro->depto : null;
            $this->codigo_postal = isset($registro->codigo_postal) ? $registro->codigo_postal : null;
            $this->validado_formulario = isset($registro->validado_formulario) ? $registro->validado_formulario : null;
            $this->jaula_dux = isset($registro->jaula_dux) ? $registro->jaula_dux : null;
            $this->confirmado_rounting = isset($registro->confirmado_rounting) ? $registro->confirmado_rounting : null;
        }
    }

    public function jsonSerialize()
    {
        return [
            'rutcli' => $this->rutcli,
            'coddir' => $this->coddir,
            'dirdes' => $this->dirdes,
            'codcom' => $this->codcom,
            'codciu' => $this->codciu,
            'codest' => $this->codest,
            'codsec' => $this->codsec,
            'codubi' => $this->codubi,
            'codseq' => $this->codseq,
            'codreg' => $this->codreg,
            'ubicacion' => $this->ubicacion,
            'pagina' => $this->pagina,
            'fila' => $this->fila,
            'columna' => $this->columna,
            'id' => $this->id,
            'mapcity' => $this->mapcity,
            'frecuencia' => $this->frecuencia,
            'coddir_unificado' => $this->coddir_unificado,
            'fecha_ult_vta' => $this->fecha_ult_vta,
            'destino' => $this->destino,
            'estado_mapcity' => $this->estado_mapcity,
            'jaula' => $this->jaula,
            'dirdes_old' => $this->dirdes_old,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud,
            'jaula_sucursal' => $this->jaula_sucursal,
            'codloc' => $this->codloc,
            'fecha_creac' => $this->fecha_creac,
            'id_cliente_sap' => $this->id_cliente_sap,
            'tipo_via' => $this->tipo_via,
            'nombre_via' => $this->nombre_via,
            'numero' => $this->numero,
            'piso' => $this->piso,
            'depto' => $this->depto,
            'codigo_postal' => $this->codigo_postal,
            'validado_formulario' => $this->validado_formulario,
            'jaula_dux' => $this->jaula_dux,
            'confirmado_rounting' => $this->confirmado_rounting,
        ];
    }
}