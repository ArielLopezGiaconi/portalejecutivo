<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 25-01-2018
 * Time: 12:18
 */

namespace App\Entities\Oracle\Holding;

use App\PDO\Oracle\Holding\ReBodProdPDO;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class SapFboTfStock
{
    private $codemp;
    private $codbod;
    private $codpro;
    private $stkfis;
    private $stkcom;
    private $stkdis;
    private $cosprom;
    private $fecha_creac;
    private $fecha_proce;
    private $estado;
    private $pre_existe;

    public function __construct($registro = null)
    {
        if ($registro) {
            $this->codemp = ($registro->codemp) ? $registro->codemp : null;
            $this->codbod = ($registro->codbod) ? $registro->codbod : null;
            $this->codpro = ($registro->codpro) ? $registro->codpro : null;
            $this->stkfis = ($registro->stkfis) ? $registro->stkfis : null;
            $this->stkcom = ($registro->stkcom) ? $registro->stkcom : null;
            $this->stkdis = ($registro->stkdis) ? $registro->stkdis : null;
            $this->cosprom = ($registro->cosprom) ? $registro->cosprom : null;
            $this->fecha_creac = ($registro->fecha_creac) ? $registro->fecha_creac : null;
            $this->fecha_proce = ($registro->fecha_proce) ? $registro->fecha_proce : null;
            $this->estado = ($registro->estado) ? $registro->estado : null;
            $this->pre_existe = ($registro->pre_existe) ? $registro->pre_existe : null;
        }
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getCodbod()
    {
        return $this->codbod;
    }

    public function setCodbod($codbod)
    {
        $this->codbod = $codbod;
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getStkfis()
    {
        return $this->stkfis;
    }

    public function setStkfis($stkfis)
    {
        $this->stkfis = $stkfis;
    }

    public function getStkcom()
    {
        return $this->stkcom;
    }

    public function setStkcom($stkcom)
    {
        $this->stkcom = $stkcom;
    }

    public function getStkdis()
    {
        return $this->stkdis;
    }

    public function setStkdis($stkdis)
    {
        $this->stkdis = $stkdis;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getFechaCreac()
    {
        return $this->fecha_creac;
    }

    public function setFechaCreac($fecha_creac)
    {
        $this->fecha_creac = $fecha_creac;
    }

    public function getFechaProce()
    {
        return $this->fecha_proce;
    }

    public function setFechaProce($fecha_proce)
    {
        $this->fecha_proce = $fecha_proce;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function getPreExiste()
    {
        return $this->pre_existe;
    }

    public function setPreExiste($pre_existe)
    {
        $this->pre_existe = $pre_existe;
    }

    public function obtainPreExiste() {
        $this->pre_existe = (ReBodProdPDO::existsStockProduct($this->getCodemp(), $this->getCodbod(), $this->getCodpro())) ? 'S' : 'N';
    }
}