<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 05/09/2018
 * Time: 13:45
 */


namespace App\Entities\SapWs\Request;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;
use Illuminate\Support\Collection;

/* SAP GET STOCK */
class MtMm025ReqOut extends ReqOut
{
    private $material;
    private $centro;
    private $almacen;
    private $stock;

    public function __construct()
    {
        $this->setHeaderHosts((env('DATABASE_ENV') == 'prod') ? env('WS_SAP_PROD') : env('WS_SAP_DEV'));
        $this->setHeaderUrl($this->getHeaderHosts().'/XISOAPAdapter/MessageServlet'); // 192.168.251.18
        $this->setHeaderMethod('POST');
        $this->setHeaderUrlfull(
            $this->getHeaderUrl()
            . '?senderParty='
            . '&senderService=BC_VERACORE'
            . '&receiverParty='
            . '&receiverService='
            . '&interface=SI_MM025_SYN_OUT'
            . '&interfaceNamespace=urn:swap:veracore:s4h:stock_veracore:MM025');

        $this->materiales_stocks = null;
        /* Formato de Materiales: ['material' => '', 'stock' => ''] */
    }

    public function getMaterial()
    {
        // 18 Characters
        return substr('000000000000000000' . $this->material, -18);
    }

    public function setMaterial($material)
    {
        $this->material = $material;
    }

    public function getCentro()
    {
        // 4 Characters
        return substr('000000000000000000' . $this->centro, -4);
    }

    public function setCentro($centro)
    {
        $this->centro = $centro;
    }

    public function getAlmacen()
    {
        // 4 Characters
        return substr('000000000000000000' . $this->almacen, -4);
    }

    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    public function getRequestXml()
    {
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'
            . ' xmlns:urn="urn:swap:veracore:s4h:stock_veracore:MM025">'
            . ' <soapenv:Header/>'
            . ' <soapenv:Body>'
            . ' <urn:MT_MM025_REQ_OUT>'
            . ' <T_STOCK>'
            . ' <MATNR>' . $this->getMaterial() . '</MATNR>'  // MATERIAL: [FF455740]
            . ' <WERKS>' . $this->getCentro() . '</WERKS>' // CENTRO LOGÍSTICO SAP: [Bodega 114]
            . ' <LGORT>' . $this->getAlmacen() . '</LGORT>' // ALMACEN FULFILLMENT:  [Bodega 114]
            . ' <LBKUM>' . $this->getStock() . '</LBKUM>' // STOCK
            . ' </T_STOCK>'
            . ' </urn:MT_MM025_REQ_OUT>'
            . ' </soapenv:Body>'
            . ' </soapenv:Envelope>';

        return $xml;
    }

    public function getRequestXmlMultiple(array $in_data)
    {
        $xml ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'
            . ' xmlns:urn="urn:swap:veracore:s4h:stock_veracore:MM025">'
            . ' <soapenv:Header/>'
            . ' <soapenv:Body>'
            . ' <urn:MT_MM025_REQ_OUT>';

        foreach ($in_data as $registro) {
            $xml.=' <T_STOCK>'
                . ' <MATNR>' . $registro -> codsap . '</MATNR>'  // MATERIAL: [FF455740]
                . ' <WERKS>' . $registro -> sap_centro . '</WERKS>' // CENTRO LOGÍSTICO SAP: [Bodega 114]
                . ' <LGORT>' . $registro -> sap_almacen . '</LGORT>' // ALMACEN FULFILLMENT:  [Bodega 114]
                . ' <LBKUM>' . $registro -> stocks . '</LBKUM>' // STOCK
                . ' </T_STOCK>';
        }

        $xml.=' </urn:MT_MM025_REQ_OUT>'
            . ' </soapenv:Body>'
            . ' </soapenv:Envelope>';

        return $xml;
    }
}