<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 05/09/2018
 * Time: 15:54
 */

namespace App\Entities\SapWs\Response;

use App\Entities\SapWs\Response\ResOut;
use Illuminate\Database\Query\Builder;
use DB;
use Orchestra\Parser\Xml\Facade as XmlParser;
use SimpleXMLElement;

class MtMm024ResOut extends ResOut
{
    private $xml_return;

    public function __construct($xml_string)
    {
        // Cargar XML
        $this->xml_return = simplexml_load_string($xml_string)
            ->children('SOAP', true)->Body
            ->children('ns0', true)->MT_MM024_RES_OUT;

        foreach ($this->xml_return->xpath('MSG') as $elementoReturn) {
            $this->xml_return = new SimpleXMLElement($elementoReturn->asXML());
        }
    }

    public function getMensajeReturn()
    {
        if ($this->xml_return == null) {
            return null;
        }

        return $this->xml_return . '';
    }
}