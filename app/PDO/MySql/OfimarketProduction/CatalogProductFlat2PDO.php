<?php
/**
 * Created by PhpStorm.
 * User: Fburgos
 * Date: 04-02-2019
 * Time: 16:27
 */

namespace App\PDO\MySql\OfimarketProduction;

use App\Entities\MySql\Cerberus\DropshipDw;
use App\Entities\Oracle\Holding\MaProduct;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class CatalogProductFlat2PDO extends Model
{
    public static function getAllSku()
    {
        try {
            $sql = "SELECT sku FROM catalog_product_flat_2"
                . " GROUP BY sku"
                . " ORDER BY RAND() DESC"
                . " LIMIT 0, 10000";

            $resultado = DB::connection('mysql_magento_ofimarket')->select($sql);

            $arrayReturn = [];
            foreach ($resultado as $registro) {
                $producto = new MaProduct();
                $producto->setCodsap($registro->sku);
                $producto->setCodpro($registro->sku);
                $arrayReturn[] = $producto;
            }

            return $arrayReturn;
        } catch (\Exception $e) {
            return null;
        }
    }
}