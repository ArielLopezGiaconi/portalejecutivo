<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use Illuminate\Support\Facades\Session;

class RankingController extends Controller
{
    public function getRankingGrupo() {
        $userid = Session::get('usuario')->get('userid');

        $meta = PortalEjUsuariosPDO::getRankingGrupo($userid);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($meta);
        return $respuesta->returnToJson();
    }
}