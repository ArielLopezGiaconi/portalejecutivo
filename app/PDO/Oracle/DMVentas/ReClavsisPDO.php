<?php


namespace App\PDO\Oracle\DMVentas;

//use App\Entities\Oracle\DMVentas\MaUsuario;
use App\Entities\Oracle\DMVentas\ReClavsis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ReClavsisPDO extends Model
{
    public static function existsUserByRutusu($in_rutusu) {
        $sql = "SELECT COUNT(*) AS existe FROM re_clavsis WHERE rutusu = :rut_usu";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_usu' => $in_rutusu
        ]);

        return ($resultado != null && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function getPasswordByRutusu($in_rutusu) {

        $sql = "SELECT * FROM re_clavsis WHERE rutusu = :rut_usu";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'rut_usu' => $in_rutusu
        ]);

        if($resultado && count($resultado) > 0) {
            $clavesis = new ReClavsis($resultado[0]);
            return new Collection($clavesis);
        }
        else{
            return null;
        }


    }

}