<?php


namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CampanasController extends Controller
{
    public function getCampanaVtaMes(Request $request)
    {
        $usuario = Session::get('usuario')->get('codusu');
        $periodo = $request->get('periodo');
        $result = PortalEjUsuariosPDO::getCampanaVtaMes($usuario, $periodo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($result);
        return $respuesta->returnToJson();
    }

    public function getCampanaClienteCartera(Request $request)
    {
        $usuario = Session::get('usuario')->get('codusu');
        $periodo = $request->get('periodo');
        $result = PortalEjUsuariosPDO::getCampanaClienteCartera($usuario, $periodo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($result);
        return $respuesta->returnToJson();
    }

}