@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-black-tie text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Reporte de productividad FFVV</h3>
                            <span class="f-16">Reportes - Reporte de productividad FFVV | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                                     style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div id="content" width="800px" height="600px"></div>
                <img class="img" width="460" height="35">
            </div>

@endsection
@section('custom-includes')
    <script>

        $(document).ready(function () {
            var url = "https%3A%2F%2Fapp.powerbi.com%2Fview%3Fr%3DeyJrIjoiYWEyNGVlMzktZDhlYy00ZjE3LWI3MzktOGQxZTdhMWVkMDZkIiwidCI6IjIzYmMzNThkLTc1ZGUtNGIzOS05OGZhLWQ5N2I2ZDBjYzFmMyIsImMiOjR9%26pageName%3DReportSection80ff2caf7a91037440a8";
            decode(url);
            getDerechos();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function decode(url) { 
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                url: url
            };
            $.ajax({
                data: parametros,
                url: '{{url('/decode')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    document.getElementById('content').innerHTML = '<object id="PowwerBI" width="100%" height="800" data="' + json.response + '" frameborder="0" allowFullScreen="true"/>';
                    //console.log('caca');
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
    <style>
        .img {
            position: absolute;
            right: 25px;
            top: 937px;
            z-index: 1;
            background-color: rgb(224, 223, 230);  
        }
    </style>
@endsection
