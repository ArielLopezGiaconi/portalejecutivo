<?php

namespace App\Http\Controllers\back;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\Controller;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\QvMetasVendedorPDO;
use App\PDO\Oracle\DMVentas\oraclePDO;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class PortadaController extends Controller
{
    public function getVentaEjecutivo() {
        $userid = Session::get('usuario')->get('userid');
        $rutusu = Session::get('usuario')->get('rutusu');

        $venta_ejecutivo = PortalEjUsuariosPDO::getVentaEjecutivo($userid, $rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_ejecutivo);
        return $respuesta->returnToJson();
    }

    public function registro(Request $request) {
        $usuario = Session::get('usuario');
        $codusu = $usuario->get('codusu');

        $moduleid = $request->get('moduleid');
        $actionid = $request->get('actionid');
        $oldvalue = $request->get('oldvalue');
        $newvalue = $request->get('newvalue');
        $updaterecord = $request->get('updaterecord');
        
        $venta_ejecutivo = PortalEjUsuariosPDO::registro($codusu, $moduleid, $actionid, $oldvalue, $newvalue, $updaterecord);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_ejecutivo);
        return $respuesta->returnToJson();
    }

    public function GestionMes(Request $request) {
        $usuario = Session::get('usuario');
        $codcnl = $usuario->get('codcnl');
        $rutusu = $usuario->get('rutusu');
        $tipo = $request->get('tipo');
        
        $venta_ejecutivo = oraclePDO::GestionMes($rutusu, $codcnl, $tipo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_ejecutivo);
        return $respuesta->returnToJson();
    }

    public function getPotencial() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        
        $venta_ejecutivo = PortalEjUsuariosPDO::getPotencial($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_ejecutivo);
        return $respuesta->returnToJson();
    }

    public function ObtieneDatos() {
        $usuario = Session::get('usuario');
        
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($usuario);
        return $respuesta->returnToJson();
    }

    public function getResumenCartera() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $venta_cartera = PortalEjUsuariosPDO::getResumenCartera($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_cartera);
        return $respuesta->returnToJson();
    }

    public function getResumenTotales() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $venta_cartera = PortalEjUsuariosPDO::getResumenTotales($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_cartera);
        return $respuesta->returnToJson();
    }

    public function getVentaCartera() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');

        $venta_cartera = PortalEjUsuariosPDO::getVentaCartera($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($venta_cartera);
        return $respuesta->returnToJson();
    }

    public function getMeta() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        
        $meta = PortalEjUsuariosPDO::getMeta($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($meta);
        return $respuesta->returnToJson();
    }     

    public function getDinero() {
        $usuario = Session::get('usuario');
        $rutusu = $usuario->get('rutusu');
        
        $meta = PortalEjUsuariosPDO::getDinero($rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($meta);
        return $respuesta->returnToJson();
    }

    public function getmanual() {
        //$url = Storage::url('file.jpg'); ('Manual.pdf');

        $headers = '';
        $name = '';
        
        return Storage::download('Manual.pdf', $name, $headers);
    }

    public function getMetaEjecutivo() {
        $userid = Session::get('usuario')->get('userid');
        $rutusu = Session::get('usuario')->get('rutusu');

        $meta = QvMetasVendedorPDO::getMetaEjecutivo($userid, $rutusu);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($meta);
        return $respuesta->returnToJson();
    }

    public function getRankingEjecutivo() {
        $userid = Session::get('usuario')->get('userid');

        $meta = PortalEjUsuariosPDO::getRankingEjecutivo($userid);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($meta);
        return $respuesta->returnToJson();
    }
}