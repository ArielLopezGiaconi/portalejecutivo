@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="icofont icofont-open-eye text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo" >Seguimiento nota</h3>
                            <span class="f-16">Seguimiento nota | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                             style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                    </div>
                </div>
                <div class="card-block" style="">
                </div>
            </div>

            <div class="card-block">
                <div id="tablaTotales" class="table-responsive">
                <table class="table-custom table-hover table-bordered table-styling" style="border: 2px solid #546686; width: 100%">
                    <thead>
                    <tr class="table-inverse">
                        <th colspan="14">Totales</th>
                    </tr>
                    <tr class="table-inverse">
                        <th>Q notas</th>
                        <th>Val. notas</th>
                        <th>Q ntv facturadas</th>
                        <th>Val. nvt facturadas</th>
                        <th>% ntv facturadas</th>
                        <th>Q ntv entregadas</th>
                        <th>Val. ntv entregadas</th>
                        <th>% ntv. entregadas</th>
                        <th>Q nvt preparacion</th>
                        <th>Val. ntv preparacion</th>
                        <th>% ntv preparacion</th>
                        <th>Q ntv pendiente stock</th>
                        <th>Val. ntv pendiente stock</th>
                        <th>% ntv pendiente stock</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="valor1"></td>
                            <td id="valor2"></td>
                            <td id="valor3"></td>
                            <td id="valor4"></td>
                            <td id="valor5"></td>
                            <td id="valor6"></td>
                            <td id="valor7"></td>
                            <td id="valor8"></td>
                            <td id="valor9"></td>
                            <td id="valor10"></td>
                            <td id="valor11"></td>
                            <td id="valor12"></td>
                            <td id="valor13"></td>
                            <td id="valor14"></td>
                        </tr>
                    </tbody>
                </table>
                </div>

                <div class="table-responsive">
                    {{-- <h5 class="sub-title m-b-15">Ordenes Web</h5> --}}
                    <div id="detalle" onkeyup="Formato()"></div>
                    
                </div>
            </div>
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-hand-rock-o";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Acciones Agendadas";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            getEstadisticas();
            getEstadisticasTotales();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
            setTimeout(function() {
                tabla = document.getElementById("tablaTotales");
                $(tabla).addClass('bounceInDown animated'); 
                tabla = document.getElementById("detalle");
                $(tabla).addClass('bounceInDown animated');
            }, 1000);
        });

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text,0));
                
                td = tr[i].getElementsByTagName("td")[2];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text('$' + number_format(text,0));

                td = tr[i].getElementsByTagName("td")[3];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text,0));

                td = tr[i].getElementsByTagName("td")[4];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text('$' + number_format(text,0));

                td = tr[i].getElementsByTagName("td")[5];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text,2) + '%');

                td = tr[i].getElementsByTagName("td")[6];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text, 2) + '%');

                td = tr[i].getElementsByTagName("td")[7];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text('$' + number_format(text,0));

                td = tr[i].getElementsByTagName("td")[8];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text,2) + '%');

                td = tr[i].getElementsByTagName("td")[9];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text,0));

                td = tr[i].getElementsByTagName("td")[10];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text('$' + number_format(text,0));

                td = tr[i].getElementsByTagName("td")[11];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text, 1) + '%');

                td = tr[i].getElementsByTagName("td")[12];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text, 0));

                td = tr[i].getElementsByTagName("td")[13];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text('$' + number_format(text, 0));

                td = tr[i].getElementsByTagName("td")[14];
                $(td).addClass('text-right');
                text = $(td).text();
                $(td).text(number_format(text, 1) + '%');

            }
        }

        function getEstadisticas() {
            var table = '<table name="EstadisticasMes" id="detallenota" onclick="Formato()" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th onclick="Formato()" id="Ejecutivo">Ejecutivo</th>\n' +
                '                            <th onclick="Formato()" >Q notas</th>\n' +
                '                            <th onclick="Formato()" >Val. </br> notas</th>\n' +
                '                            <th onclick="Formato()" >Q ntv </br> facturadas</th>\n' +
                '                            <th onclick="Formato()" >Val. ntv </br> facturadas</th>\n' +
                '                            <th onclick="Formato()" >% ntv </br> facturadas</th>\n' +
                '                            <th onclick="Formato()" >Q ntv </br> entregadas</th>\n' +
                '                            <th onclick="Formato()" >Val. ntv </br> entregadas</th>\n' +
                '                            <th onclick="Formato()" >% ntv </br> entregadas</th>\n' +
                '                            <th onclick="Formato()" >Q ntv </br> preparación</th>\n' +
                '                            <th onclick="Formato()" >Val. ntv </br> preparación</th>\n' +
                '                            <th onclick="Formato()" >% ntv </br> preparación</th>\n' +
                '                            <th onclick="Formato()" >Q ntv </br> pendiente stock</th>\n' +
                '                            <th onclick="Formato()" >Val. ntv </br> pendiente stock</th>\n' +
                '                            <th onclick="Formato()" >% ntv </br> pendiente stock</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/indicadores/seguimientoNota')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenota").DataTable({
                            data: json.response,
                            columns: [ 
                                {data: "nombre",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "q_notas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "val_notas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "q_nv_facturadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "val_nv_facturadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "per_nv_facturadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "q_nv_entregadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "val_nv_entregadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "per_nv_entregadas",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "q_nv_preparacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "val_nv_preparacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "per_nv_preparacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "q_nv_pendiente_stock",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "val_nv_pendiente_stock",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "per_nv_pendiente_stock",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',  
                            buttons: [
                                { extend:'excelHtml5', text: 'Excel'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        $("#spinner").hide();
                        Formato();
                        document.getElementById("Ejecutivo").click();
                        Formato();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function getEstadisticasTotales() {
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('/indicadores/seguimientoNotaTotales')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        
                        document.getElementById("valor1").innerText = number_format(json.response[0].q_notas, 0);
                        document.getElementById("valor2").innerText = '$' + number_format(json.response[0].val_notas, 0);
                        document.getElementById("valor3").innerText = number_format(json.response[0].q_nv_facturadas, 0);
                        document.getElementById("valor4").innerText = '$' + number_format(json.response[0].val_nv_facturadas, 0);
                        document.getElementById("valor5").innerText = number_format(json.response[0].per_nv_facturadas, 2) + '%';
                        document.getElementById("valor6").innerText = number_format(json.response[0].q_nv_entregadas, 0);
                        document.getElementById("valor7").innerText = '$' + number_format(json.response[0].val_nv_entregadas, 0);
                        document.getElementById("valor8").innerText = number_format(json.response[0].per_nv_entregadas, 2) + '%';
                        document.getElementById("valor9").innerText = number_format(json.response[0].q_nv_preparacion, 0);
                        document.getElementById("valor10").innerText = '$' + number_format(json.response[0].val_nv_preparacion, 0);
                        document.getElementById("valor11").innerText = number_format(json.response[0].per_nv_preparacion, 2) + '%';
                        document.getElementById("valor12").innerText = number_format(json.response[0].q_nv_pendiente_stock, 0);
                        document.getElementById("valor13").innerText = '$' + number_format(json.response[0].val_nv_pendiente_stock, 2);
                        document.getElementById("valor14").innerText = number_format(json.response[0].per_nv_pendiente_stock, 2) + '%';
                        
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection