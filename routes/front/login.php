<?php

Route::get('/', function () {
    if(Session::get('auth')) {
        return redirect()->to('portada');
    }
    else {
        return redirect()->to('login');
    }
});

//Route::get('login', 'LoginController@iniciarSesion');

Route::get('login', function(){
    return view('front.pages.auth.login');
});

Route::post('iniciasesion', 'LoginController@iniciarSesion');
Route::get('logout', 'LoginController@cerrarSesion');
