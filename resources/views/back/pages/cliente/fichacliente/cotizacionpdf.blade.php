<?php
function moneda_chilena($numero){
    $numero = (string)$numero;
    $puntos = floor((strlen($numero)-1)/3);
    $tmp = "";
    $pos = 1;
    for($i=strlen($numero)-1; $i>=0; $i--){
        $tmp = $tmp.substr($numero, $i, 1);
        if($pos%3==0 && $pos!=strlen($numero))
            $tmp = $tmp.".";
        $pos = $pos + 1;
    }
    $formateado = "$ ".strrev($tmp);
    return $formateado;
}
?><!DOCTYPE html>
<html>
<head>
    <style>
        .bg{

            background: url("{{ asset('assets/1/img/bg_cotizacion.png') }}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<div>
    <img src="{{ asset('assets/1/img/bg_cotizacion.png') }}" width="800" style="margin-top:-50px;margin-left:-60px;position: absolute;z-index:1;"/>
    <div style="float: left;margin-top:40px;margin-left:280px;font-size:20px;display:block;position:absolute;z-index:2;">
        <span><b>N° {{ $totales['numcot'] }}</b></span>
    </div>
    <div style="float:left;left: 10px; margin-top: 110px;display: block;clear: both;position:absolute;z-index:3;">
        <div style="float: left">
            <table style="font-size: 12px;float:left;">
                <tbody>
                <tr>
                    <td><b>CLIENTE:</b></td>
                    <td>{{ $totales['razons'] }}</td>
                </tr>
                <tr>
                    <td><b>RUT:</b></td>
                    <td>{{ $totales['rutcli'] }}-{{ $totales['digcli'] }}</td>
                </tr>
                <tr>
                    <td><b>C.COSTO:</b></td>
                    <td>{{ $totales['desc_ceco'] }}</td>
                </tr>
                <tr>
                    <td><b>DIRECCIÓN:</b></td>
                    <td>{{ $totales['direc_ceco'] }}</td>
                </tr>
                <tr>
                    <td><b>COMUNA:</b></td>
                    <td>{{ $totales['comuna'] }}</td>
                </tr>
                <tr>
                    <td><b>CONDICIÓN:</b></td>
                    <td>Cotización válida hasta el {{ $totales['fecven'] }} o hasta agotar stock</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div style="float: left">
            <table style="font-size:12px;float: left;margin-left:400px;">
                <tbody>
                <tr>
                    <td><b>F. emisión:</b></td>
                    <td>{{ $totales['fecemi'] }}</td>
                </tr>
                <tr>
                    <td><b>F. vencimiento:</b></td>
                    <td>{{ $totales['fecven'] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <p style="margin-top:100px;">_______________________________________________________________________________________</p>
        <div style="float: left">
            <table style="font-size: 11px;width:730px;margin-top:0px;height: 500px;">
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th>Marca</th>
                    <th>Und</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody id="tbl_coti_pdf" style="font-family: CourierNew_9;">
                @foreach($productos as $pro)
                    <tr>
                        <td>{{ $pro['codpro'] }}</td>
                        <td>{{ $pro['despro'] }}</td>
                        <td>{{ $pro['desmar'] }}</td>
                        <td>{{ $pro['und'] }}</td>
                        <td>{{ $pro['cantid'] }}</td>
                        <td>{{ moneda_chilena($pro['precio']) }}</td>
                        <td>{{ moneda_chilena($pro['totnet']) }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <p>_______________________________________________________________________________________</p>
            <div style="float: left;clear:both;">
                <table style="font-size:12px;float: left;margin-left:440px;">
                    <tbody>
                    <tr>
                        <td><b>Subtotal:</b></td>
                        <td>{{ moneda_chilena($totales['subtotal']) }}</td>
                    </tr>
                    <tr>
                        <td><b>Neto:</b></td>
                        <td>{{ moneda_chilena($totales['neto']) }}</td>
                    </tr>
                    <tr>
                        <td><b>Iva:</b></td>
                        <td>{{ moneda_chilena($totales['iva']) }}</td>
                    </tr>
                    <tr>
                        <td><b>Total:</b></td>
                        <td>{{ moneda_chilena($totales['total']) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="float:left;margin-top:100px;bottom:0;">
                <p style="font-size:12px;">Saludos cordiales,</p>

                <table style="font-size:12px;float: left;margin-left:400px;padding: 0px 0px 0px 0px;">
                    <tbody>
                    <tr><td><b><i>Ejecutivo Comercial</i></b></td></tr>
                    <tr><td><b><i>{{ ucwords(strtolower(Session::get('usuario')->get('nombre').' '.Session::get('usuario')->get('apepat').' '.Session::get('usuario')->get('apemat'))) }}</i></b></td></tr>
                    <tr><td><b><i>Fono directo: +56 22 385 {{ Session::get('usuario')->get('anexos') }}</i></b></td></tr>
                    <tr><td><b><i>{{ Session::get('usuario')->get('mail01') }}</i></b></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</html>