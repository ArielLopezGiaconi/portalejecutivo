<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 16:33
 */


namespace App\Entities\SapWs\Request;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;

/* SAP GET STOCK */
class ReqOut
{
    private $header_url;
    private $header_query_string;
    private $header_method;
    private $header_urlfull;
    private $header_hosts;

    public function __construct()
    {
        $this->header_hosts = (env('DATABASE_ENV') == 'prod') ? env('WS_SAP_PROD') : env('WS_SAP_DEV');
    }

    public function getHeaderUrl()
    {
        return $this->header_url;
    }

    public function getHeaderQueryString()
    {
        return $this->header_query_string;
    }

    public function getHeaderMethod()
    {
        return $this->header_method;
    }

    public function setHeaderUrl($header_url)
    {
        $this->header_url = $header_url;
    }

    public function setHeaderQueryString($header_query_string)
    {
        $this->header_query_string = $header_query_string;
    }

    public function setHeaderMethod($header_method)
    {
        $this->header_method = $header_method;
    }

    public function getHeaderUrlFull()
    {
        return $this->header_urlfull;
    }

    public function setHeaderUrlfull($header_urlfull)
    {
        $this->header_urlfull = $header_urlfull;
    }

    public function getHeaderHosts()
    {
        return $this->header_hosts;
    }

    public function setHeaderHosts($header_hosts)
    {
        $this->header_hosts = $header_hosts;
    }
}