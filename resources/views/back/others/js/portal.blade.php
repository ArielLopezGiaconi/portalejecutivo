<script>
	@if(isset($derechos))
	 var derechos = @json($derechos);
	@endif
	function tieneDerecho(codder){
		for(var i = 0; i<derechos.length; i++){
			if(derechos[i].codder == codder){
				return true;
			}
		}
		return false;
	}

	function getUrlFotoProd(codpro){
		return "https://ww2.dimerc.cl/catalog/thumbnail/get/size/200x200/sku/" + codpro + ".jpg";
	}

     function registroBitacora(userid, moduleid, actionid, oldvalue, newvalue, updaterecord=''){
         var parametros = {
             userid: userid,
             moduleid: moduleid,
             actionid: actionid,
             oldvalue: oldvalue,
             newvalue: newvalue,
             updaterecord: updaterecord
         };
         console.log(parametros);
         $.ajax({
             xhrFields: {
                 withCredentials: true
             },
             headers: {
                 'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
             },
             data: parametros,
             url: '{{ env("URL_API") }}/api/clientes/registroBitacora',
             type: 'POST',
             cache: false,
             dataType: 'json',
             async: true,
             success: function (json) {
                 if (json.code == 200) {
                     console.log(json);
                 }
             }
         });
     }
    
    function registroError(userid, moduleid, action, errorcode, errormessage){
        var parametros = {
            userid: userid,
            moduleid: moduleid,
            action: action,
            errorcode: errorcode,
            errormessage: errormessage
        };
        //console.log(parametros);
        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/registro-error',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: false,
            success: function (json) {
                if (json.code == 200) {
                    console.log(json);
                }
            }
        });
    }
</script>
