<script>
    var meses = [];
    meses['01'] ='Enero';
    meses['02'] = 'Febrero';
    meses['03'] = 'Marzo';
    meses['04'] = 'Abril';
    meses['05'] = 'Mayo';
    meses['06'] = 'Junio';
    meses['07'] = 'Julio';
    meses['08'] = 'Agosto';
    meses['09'] = 'Septiembre';
    meses['10'] = 'Octubre';
    meses['11'] = 'Noviembre';
    meses['12'] = 'Diciembre';

    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    function getCampanas() {
        // var periodo = '20190901';
        var fecha = new Date();
        var año=fecha.getFullYear().toString();
        var mes=fecha.getMonth().toString();
        mes=mes.length>1?mes:'0' + mes;
        var dia='01';

        var periodo = año + mes + dia;

        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            periodo: periodo
        };

        //Obtener campaña ventas del mes
        $.ajax({
            data: parametros,
            url: '{{url('getcampanavtames')}}',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    makeSectionCampanaVtaMes(json.response);
                }
            }
            , error: function (e) {
                console.log('error: ' + e.message);
            }
        });

        //Obtener campaña cartera clientes
        $.ajax({
            data: parametros,
            url: '{{url('getcampanaclientecartera')}}',
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    // console.log(json);
                    makeSectionCampanaClienteCartera(json.response);
                }
            }
            , error: function (e) {
                console.log('error: ' + e.message);
            }
        });
    }
    function makeSectionCampanaVtaMes(obj){

        var periodo_mes =obj[0].periodo_mes;
        periodo_mes=periodo_mes.length>1?periodo_mes:'0'+periodo_mes;
        var periodo_año=obj[0].periodo_anio;
        var ejecutivo=obj[0].usuario;

        var ruts_atendidos=obj[0].ruts_atendidos;
        var venta_generada_por_ejecutivo=obj[0].venta_generada_por_ejecutivo;
        venta_generada_por_ejecutivo='$' + formatMoney(venta_generada_por_ejecutivo, 0, ',', '.');
        var meta_de_venta=obj[0].meta_de_venta;
        meta_de_venta='$' + formatMoney(meta_de_venta, 0, ',', '.');
        var cumplimiento_meta=obj[0].cumplimiento_meta;
        cumplimiento_meta=cumplimiento_meta + ' %';
        var margen_ejecutivo=obj[0].margen_ejecutivo;
        margen_ejecutivo=margen_ejecutivo + ' %';
        var per_participacion_mp=obj[0].per_participacion_mp;
        per_participacion_mp=per_participacion_mp + ' %';
        var promedio_penetracion_lineas=obj[0].promedio_penetracion_lineas;
        promedio_penetracion_lineas=promedio_penetracion_lineas + ' %';
        var porcentaje_venta_integraciones=obj[0].porcentaje_venta_integraciones;
        porcentaje_venta_integraciones=porcentaje_venta_integraciones + ' %';
        var porcentaje_venta_internet=obj[0].porcentaje_venta_internet;
        porcentaje_venta_internet=porcentaje_venta_internet + ' %';

        var section = '<div class="col-md-12">\n' +
            '                        <div class="card table-card">\n' +
            '                            <div class="card-header">\n' +
            '                                <h5>Campaña ' + meses[periodo_mes] + ' ' + periodo_año + ' - Cumplimiento Meta de Venta</h5>\n' +
            '                            </div>\n' +
            '                            <div class="card-block">\n' +
            '                                <div class="table-responsive">\n' +
            '                                    <table class="table table-hover table-borderless">\n' +
            '                                        <thead>\n' +
            '                                        <tr>\n' +
            '                                            <th>Periodo</th>\n' +
            '                                            <th>Ejecutivo</th>\n' +
            '                                            <th>Ruts atendidos</th>\n' +
            '                                            <th>Vta generada por ejecutivo</th>\n' +
            '                                            <th>Meta venta</th>\n' +
            '                                            <th>Cumplimiento meta</th>\n' +
            '                                            <th>Margen ejecutivo</th>\n' +
            '                                        </tr>\n' +
            '                                        </thead>\n' +
            '                                        <tbody>\n' +
            '                                        <tr>\n' +
            '                                            <td><label class="label label-success">' + periodo_mes + '-' + periodo_año + '</label></td>\n' +
            '                                            <td>' + ejecutivo + '</td>\n' +
            '                                            <td>' + ruts_atendidos + '</td>\n' +
            '                                            <td>' + venta_generada_por_ejecutivo + '</td>\n' +
            '                                            <td>' + meta_de_venta + '</td>\n' +
            '                                            <td>' + cumplimiento_meta + '</td>\n' +
            '                                            <td>' + margen_ejecutivo + '</td>\n' +
            '                                        </tr>\n' +
            '                                        </tbody>\n' +
            '                                    </table>\n' +
            '                                    <div class="text-right m-r-20">\n' +
            '                                        <a href="#!" class=" b-b-primary text-primary" data-target="#default-Modal" data-toggle="modal">Ver detalles</a>\n' +
            '                                        <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">\n' +
            '                                            <div class="modal-dialog" role="document">\n' +
            '                                                <div class="modal-content">\n' +
            '                                                    <div class="modal-header">\n' +
            '                                                        <h5 class="modal-title">Detalle campaña</h5>\n' +
            '                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
            '                                                            <span aria-hidden="true">×</span>\n' +
            '                                                        </button>\n' +
            '                                                    </div>\n' +
            '                                                    <div class="modal-body text-left">\n' +
            '                                                        <div class="row">\n' +
            '                                                            <div class="col-lg-6">\n' +
            '                                                                Periodo</br>\n' +
            '                                                                Ejecutivo</br>\n' +
            '                                                                Ruts atendidos</br>\n' +
            '                                                                Vta generada por ejecutivo</br>\n' +
            '                                                                Meta venta</br>\n' +
            '                                                                Cumplimiento meta</br>\n' +
            '                                                                Margen ejecutivo</br>\n' +
            '                                                                Participación mp</br>\n' +
            '                                                                Promedio penetración líneas</br>\n' +
            '                                                                Porcentaje venta integraciones</br>\n' +
            '                                                                Porcentaje venta internet</br>\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-lg-6">\n' +
            '                                                                <label class="label label-success">' + periodo_mes + '-' + periodo_año + '</label></br>\n' +
            '                                                                ' + ejecutivo + '</br>\n' +
            '                                                                ' + ruts_atendidos + '</br>\n' +
            '                                                                ' + venta_generada_por_ejecutivo + '</br>\n' +
            '                                                                ' + meta_de_venta + '</br>\n' +
            '                                                                ' + cumplimiento_meta + '</br>\n' +
            '                                                                ' + margen_ejecutivo + '</br>\n' +
            '                                                                ' + per_participacion_mp + '</br>\n' +
            '                                                                ' + promedio_penetracion_lineas + '</br>\n' +
            '                                                                ' + porcentaje_venta_integraciones + '</br>\n' +
            '                                                                ' + porcentaje_venta_internet + '</br>\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '                                                    <div class="modal-footer">\n' +
            '                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>\n' +
            '                                                    </div>\n' +
            '                                                </div>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n';

        var contenido_previo = document.getElementById('div_campañas').innerHTML;

        document.getElementById('div_campañas').innerHTML = contenido_previo + section;

    }
    function makeSectionCampanaClienteCartera(obj){
        console.log(obj[0]);

        var periodo_mes =obj[0].periodo_mes;
        periodo_mes=periodo_mes.length>1?periodo_mes:'0'+periodo_mes;
        var periodo_año=obj[0].periodo_anio;
        var ejecutivo=obj[0].usuario;

        var clientes_por_perder_inicio_mes = obj[0].clientes_por_perder_inicio_mes;
        var clientes_perdidos = obj[0].clientes_perdidos;
        var porcentaje_recuperacion = obj[0].porcentaje_recuperacion + ' %';
        var q_clientes_nuevos = obj[0].q_clientes_nuevos;
        var clientes_mantenidos_cuatro_meses = obj[0].clientes_mantenidos_cuatro_meses;
        var porcentaje_cartera_mantenida = obj[0].porcentaje_cartera_mantenida + ' %';
        var clientes_fidelizados_cuatro_meses = obj[0].clientes_fidelizados_cuatro_meses;
        var porcentaje_fidelizado_cuatro_meses = obj[0].porcentaje_fidelizado_cuatro_meses + ' %';
        var clientes_fidelizados_tres_meses = obj[0].clientes_fidelizados_tres_meses;
        var clientes_fidelizados_dos_meses = obj[0].clientes_fidelizados_dos_meses;

        var section = '<div class="col-md-12">\n' +
            '                        <div class="card table-card">\n' +
            '                            <div class="card-header">\n' +
            '                                <h5>Campaña ' + meses[periodo_mes] + ' ' + periodo_año + ' - Clientes en Cartera</h5>\n' +
            '                            </div>\n' +
            '                            <div class="card-block">\n' +
            '                                <div class="table-responsive">\n' +
            '                                    <table class="table table-hover table-borderless">\n' +
            '                                        <thead>\n' +
            '                                        <tr>\n' +
            '                                            <th>Periodo</th>\n' +
            '                                            <th>Ejecutivo</th>\n' +
            '                                            <th>Porcentaje recuperación</th>\n' +
            '                                            <th>Porcentaje cartera mantenida</th>\n' +
            '                                            <th>Porcentaje fidelizado cuatro meses</th>\n' +
            '                                        </tr>\n' +
            '                                        </thead>\n' +
            '                                        <tbody>\n' +
            '                                        <tr>\n' +
            '                                            <td><label class="label label-success">' + periodo_mes + '-' + periodo_año + '</label></td>\n' +
            '                                            <td>' + ejecutivo + '</td>\n' +
            '                                            <td>' + porcentaje_recuperacion + '</td>\n' +
            '                                            <td>' + porcentaje_cartera_mantenida + '</td>\n' +
            '                                            <td>' + porcentaje_fidelizado_cuatro_meses + '</td>\n' +
            '                                        </tr>\n' +
            '                                        </tbody>\n' +
            '                                    </table>\n' +
            '                                    <div class="text-right m-r-20">\n' +
            '                                        <a href="#!" class=" b-b-primary text-primary" data-target="#default-Modal2" data-toggle="modal">Ver detalles</a>\n' +
            '                                        <div class="modal fade" id="default-Modal2" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">\n' +
            '                                            <div class="modal-dialog" role="document">\n' +
            '                                                <div class="modal-content">\n' +
            '                                                    <div class="modal-header">\n' +
            '                                                        <h5 class="modal-title">Detalle campaña</h5>\n' +
            '                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
            '                                                            <span aria-hidden="true">×</span>\n' +
            '                                                        </button>\n' +
            '                                                    </div>\n' +
            '                                                    <div class="modal-body text-left">\n' +
            '                                                        <div class="row">\n' +
            '                                                            <div class="col-lg-7">\n' +
            '                                                                Periodo</br>\n' +
            '                                                                Ejecutivo</br>\n' +
            '                                                                Clientes por perder inicio mes</br>\n' +
            '                                                                Clientes perdidos</br>\n' +
            '                                                                Porcentaje recuperación</br>\n' +
            '                                                                Q clientes nuevos</br>\n' +
            '                                                                Clientes mantenidos cuatro meses</br>\n' +
            '                                                                Porcentaje cartera mantenida</br>\n' +
            '                                                                Clientes fidelizados cuatro meses</br>\n' +
            '                                                                Porcentaje fidelizados cuatro meses</br>\n' +
            '                                                                Clientes fidelizados tres meses</br>\n' +
            '                                                                Clientes fidelizados dos meses</br>\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-lg-5">\n' +
            '                                                                <label class="label label-success">' + periodo_mes + '-' + periodo_año + '</label></br>\n' +
            '                                                                ' + ejecutivo + '</br>\n' +
            '                                                                ' + clientes_por_perder_inicio_mes + '</br>\n' +
            '                                                                ' + clientes_perdidos + '</br>\n' +
            '                                                                ' + porcentaje_recuperacion + '</br>\n' +
            '                                                                ' + q_clientes_nuevos + '</br>\n' +
            '                                                                ' + clientes_mantenidos_cuatro_meses + '</br>\n' +
            '                                                                ' + porcentaje_cartera_mantenida + '</br>\n' +
            '                                                                ' + clientes_fidelizados_cuatro_meses + '</br>\n' +
            '                                                                ' + porcentaje_fidelizado_cuatro_meses + '</br>\n' +
            '                                                                ' + clientes_fidelizados_tres_meses + '</br>\n' +
            '                                                                ' + clientes_fidelizados_dos_meses + '</br>\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '                                                    <div class="modal-footer">\n' +
            '                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>\n' +
            '                                                    </div>\n' +
            '                                                </div>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n';

        var contenido_previo = document.getElementById('div_campañas').innerHTML;

        document.getElementById('div_campañas').innerHTML = contenido_previo + section;
    }

</script>
