@extends('back.master.masterpage')
@section('contenido')
    @php
        //Session::put('searchingclient', false);
    @endphp
@include('back.others.css.fichacliente')
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="card">
                    <div class="card-header">
                        <div class="breadcrumb-header row">
                            <div class="col-sm-1">
                                <i class="fa fa-folder-open text-inverse f-46"></i> 
                            </div>
                            <div class="col-sm-6">
                                <h3>Ficha cliente</h3>
                                {{-- <span class="f-16">Ficha cliente | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>Paola Andrea Garzon</strong></span> --}}
                            </div>
                            
                        </div>
                        <div class="card-header-right">
                            
                        </div>
                    </div>
                    <div class="card-block">
                        <form>
                            <table class="table-hover col-12">
                                <thead>
                                    <tr class="table-inverse">
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left font-weight-bold">Rut:</td>
                                        <td class="text-left" id="rut"></td>
                                        <td class="text-left font-weight-bold" >Status Credito:</td>
                                        <td class="text-left" id="statusCredito"></td>
                                        <td class="text-left font-weight-bold">Venta Promedio:</td>
                                        <td class="text-left" id="VentaProm"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Relación Comercial:</td>
                                        <td class="text-left" id="rel_comercial"></td>
                                        <td class="text-left font-weight-bold">Credito:</td>
                                        <td class="text-left" id="Credito"></td>
                                        <td class="text-left font-weight-bold">Potencial:</td>
                                        <td class="text-left" id="Potencial"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Razon Social:</td>
                                        <td class="text-left" id="Razons"></td>
                                        <td class="text-left font-weight-bold">Mora Actual:</td>
                                        <td class="text-left" id="Mora"></td>
                                        <td class="text-left font-weight-bold">Rango Potencial:</td>
                                        <td class="text-left" id="RangoPotencial"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Segmento:</td>
                                        <td class="text-left" id="Segmento"></td>
                                        <td class="text-left font-weight-bold">Días Salida de Cart.:</td>
                                        <td class="text-left" id="diasCartera"></td>
                                        
                                        <td class="text-left font-weight-bold">Brecha:</td>
                                        <td class="text-left" id="Brecha"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Convenio:</td>
                                        <td class="text-left" id="convenio"></td>
                                        <td class="text-left font-weight-bold">Clasificación:</td>
                                        <td class="text-left" id="Clasificacion"></td>
                                        {{-- <td class="text-left font-weight-bold">Convenio inicia:</td>
                                        <td class="text-left" id="fecini"></td> --}}
                                        <td class="text-left font-weight-bold">Cotizaciones Activas: </td>
                                        <td class="text-left" id="Cotizac"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        {{-- <td class="text-left font-weight-bold">Convenio finaliza:</td>
                                        <td class="text-left" id="fecven"></td> --}}
                                        <td class="text-left font-weight-bold">NV. Sin Despachar: </td>
                                        <td class="text-left" id="NotasVentas"></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr class="table-inverse">
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                    </tr>
                                    <tr class="">
                                        <th class="width-200" colspan="6"><h5>Datos por centro de costo</h5></th>
                                    </tr>
                                    <tr class="table-inverse">
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                        <th class="width-200" colspan="1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left font-weight-bold">Centro de costo</td>
                                        <td class="text-left">
                                            <select name="cencos" id="cencos" class="f-12" required="">
                                            </select>
                                        </td>
                                        <td class="text-left font-weight-bold">Cobrador: </td>
                                        <td class="text-left" id="cobrador"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Dirección de facturación: </td>
                                        <td class="text-left" id="idDireccion"></td>
                                        <td class="text-left font-weight-bold">Comuna: </td>
                                        <td class="text-left" id="idComuna"></td>
                                        <td class="text-left font-weight-bold">Provincia: </td>
                                        <td class="text-left" id="idProvincia"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Forma de pago: </td>
                                        <td class="text-left" id="idForma"></td>
                                        <td class="text-left font-weight-bold">Contacto: </td>
                                        <td class="text-left" id="idContacto"></td>
                                        <td class="text-left font-weight-bold">Cargo: </td>
                                        <td class="text-left" id="Cargo"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Teléfono contacto: </td>
                                        <td class="text-left" id="idTelContac"></td>
                                        <td class="text-left font-weight-bold">Celular contacto: </td>
                                        <td class="text-left" id="idCelContac"></td>
                                        <td class="text-left font-weight-bold">Email contacto: </td>
                                        <td class="text-left" id="idEmailContac"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold">Contacto cobranza: </td>
                                        <td class="text-left" id="idContacCob"></td>
                                        <td class="text-left font-weight-bold">Teléfono Cobranza: </td>
                                        <td class="text-left" id="idTelCob"></td>
                                        <td class="text-left font-weight-bold">Email cobranza: </td>
                                        <td class="text-left" id="idEmailcob"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div id="mensaje" class="card bg-c-pink text-white text-center" style="display: none">
                        <span class="f-18">Observación Grabada</span>
                    </div>
                    <div class="row-sm-12">
                        <div class="card bg-c-blue text-white">
                            <div class="card-block">
                                <span class="pull-left f-18">Observación</span>
                                <button title="Guardar observación" onclick="grabaObserv()" class="fa fa-save btn btn-grd-inverse btn-sm btn-round f-18 pull-right"></button>
                                <input id="observacionCliente" class="col-sm-12" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="modal-send-email" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 id="tituloMail">Enviar Cartola de cliente</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-send-email">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>

                            <div class="modal-body">
                                <div class="card-block">
                                    <div class="form-group row">
                                        <p>Ingrese correo al que desea enviar la Cartola seleccionada:</p>
                                        <label class="col-sm-5 col-form-label">Ingrese destinatario:</label>
                                        <div class="col-sm-7">
                                            <input list="lista" type="text" class="form-control form-control-sm form-bg-warning" style="width: 100%;" id="correo_destinatario" placeholder="Correo electrónico" autocomplete="off" required>
                                            <datalist id="lista">
                                            </datalist>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Año:</label>
                                        <div class="col-sm-7">
                                            <select name="year" id="year" class="form-control">
                                                <option value="2021">2021</option>
                                                <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Mes:</label>
                                        <div class="col-sm-7">
                                            <select name="month" id="month" class="form-control">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Tipo Consulta:</label>
                                        <div class="col-sm-7">
                                            <select name="tipo" id="tipo" class="form-control">
                                                <option value="1">Cliente</option>
                                                <option value="2">Holding</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Comentario opcional:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" cols="5" class="form-control" id="comentario_mail" placeholder="Comentario opcional" autocomplete="off"></textarea>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="row" id="enviar">
                                        <label class="col-sm-5"></label>
                                        <div class="col-sm-7">
                                            <input type="hidden" value="" id="h_numcot">
                                            <button id="enviarCorreo" type="button submit" class="btn btn-primary btn-mini m-b-0" onclick="EnvioCartola()">Enviar</button>
                                            <button id="cerrarCorp" type="button" class="btn btn-default" data-dismiss="modal"  style="margin-left: 185px; display: none;">Cerrar</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div id='enviando' class="preloader3 loader-block" style="margin-left: 185px; display: none;">
                                            <div class="circ1"></div>
                                            <div class="circ2"></div>
                                            <div class="circ3"></div>
                                            <div class="circ4"></div>
                                        </div>
                                    </div>
                                    <div class="row" id='enviado' style="display: none">
                                        <h3>Correo Enviado Existosamente</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="razons_cliente_modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5>Buscar cliente por razón social</h5>
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Ingrese razón social</label>
                                        <div class="col-sm-9 m-t-15">
                                            <input type="text" class="form-control form-control-sm" style="width: 100%;" id="tx_razons_buscar" placeholder="Razón social" autocomplete="off">
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3"></label>
                                        <div class="col-sm-9">
                                            <button id="btn_buscar_razons" type="button" class="btn btn-primary btn-mini m-b-0" onclick="findClientForRazons();">Buscar</button>
                                        </div>
                                    </div>
                                    <div class="row m-t-20" id="div_tb_clientes_razons">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="fr_btn_ok" type="button" class="btn btn-primary btn-mini waves-effect hidden" data-dismiss="modal">Ok</button>
                                <button id="fr_btn_close" type="button" class="btn btn-default btn-mini waves-effect " data-dismiss="modal">Cerrar</button>
                                <button id="fr_btn_select" type="button" class="btn btn-primary btn-mini waves-effect waves-light " onclick="selectClientFromTableResult();">Seleccionar</button>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <ul class='nav nav-tabs  tabs' role='tablist'>
                    <li class='nav-item'>
                        <a class='nav-link active' data-toggle='tab' href='#find-rut' role='tab' aria-expanded='true'>Por Rut</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' data-toggle='tab' href='#find-razons' role='tab' aria-expanded='false'>Por razón social</a>
                    </li>
                </ul>
                <div class='tab-content tabs card-block'>
                    <div class='tab-pane active' id='find-rut' role='tabpanel' aria-expanded='true'>
                        <form action='javascript:buscafichacliente();'><div class='form-group row'><div class='col-sm-12'><input id='tx_rut_cliente' type='text' class='form-control' placeholder='Rut cliente' autocomplete='off' required></div></div><div class='text-right'><p>*Sin guion ni digito verificador</p><button class='btn btn-inverse'><i class='fa fa-search m-r-5'></i> Buscar</button></div></form>
                    </div>
                    <div class='tab-pane' id='find-razons' role='tabpanel' aria-expanded='false'>
                        <form action='javascript:buscaclienterazons();'><div class='form-group row'><div class='col-sm-12'><input id='tx_razons_cliente' type='text' class='form-control' placeholder='Razón social' autocomplete='off' required></div></div><div class='text-right'><button class='btn btn-inverse'><i class='fa fa-search m-r-5'></i> Buscar</button></div></form>
                    </div>
                </div> --}}

                <div class="page-body d-none" id="p_body">
                    <div class="row">
                        {{-- <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-yellow text-white">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <p class="m-b-5 sub-title2">Venta promedio</p>
                                            <h4 class="m-b-0 f-20"><span id="tx_venta">-</span></h4>
                                            <p class="m-t-5 f-12" style="margin-bottom: -15px;">Promedio 12 meses</p>
                                        </div>
                                        <div class="col col-auto text-right">
                                            <i class="zmdi zmdi-money-box f-50 text-c-yellow"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-green text-white">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <p class="m-b-5 sub-title2">Mg promedio</p>
                                            <h4 class="m-b-0 f-20"><span id="tx_margen">-</span></h4>
                                            <p class="m-t-5 f-12" style="margin-bottom: -15px;">Promedio 12 meses</p>
                                        </div>
                                        <div class="col col-auto text-right">
                                            <i class="icofont icofont-money-bag f-50 text-c-green"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-pink text-white">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <p class="m-b-5 sub-title2">Potencial</p>
                                            <h4 class="m-b-0 f-20"><span id="tx_potencial">-</span></h4>
                                            <p class="m-t-5 f-12" style="margin-bottom: -15px;">Brecha : <span
                                                        id="tx_pot_brecha"></span></p>
                                        </div>
                                        <div class="col col-auto text-right">
                                            <i class="icofont icofont-chart-histogram-alt f-40 text-c-pink"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="col-lg-3">
                            <div class="card" style="max-height: 101px;height: 101px;">
                                <div class="card-block" style="padding-top: 0px;">
                                    <div class="col-12" style="padding: 0px;margin-top: 15px;">
                                        <p class="m-b-0" style="margin-top: -5px;"><i
                                                    class="icofont icofont-bubble-right f-20 middle"></i> <span
                                                    class="f-12">Cliente [flotante hst]</span></p>
                                        <p class="m-b-0" style="margin-top: -5px;"><i
                                                    class="icofont icofont-bubble-right f-20 middle"></i> <span
                                                    class="f-12">Se mantiene en cartera</span></p>
                                        <p class="m-b-0" style="margin-top: -5px;"><i
                                                    class="icofont icofont-bubble-right f-20 middle"></i> <span
                                                    class="f-12"><strong class="text-c-black">Gestión mes:</strong> Venta nv</span>
                                        </p>
                                        <p class="m-b-0" style="margin-top: -5px;"><i
                                                    class="icofont icofont-bubble-right f-20 middle"></i> <span
                                                    class="f-12"><strong class="text-c-black">Días restantes:</strong> 58</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-xl-9 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        {{-- <div class="col-lg-3">
                                            <button id="btn_info_cliente" class="btn btn-inverse btn-sm btn-block"
                                                    data-toggle="popover" data-trigger="focus" data-placement="right"
                                                    title="Información cliente" data-content="">
                                                <i class="icofont icofont-tack-pin f-14 middle"></i> Información
                                            </button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button id="btn_admin_cuenta" class="btn btn-inverse btn-sm btn-block"
                                                    data-toggle="popover" data-trigger="focus" data-placement="top"
                                                    title="Administra la cuenta" data-content="">
                                                <i class="icofont icofont-tools f-16 middle"></i> Administra la cuenta
                                            </button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button id="btn_info_credito" class="btn btn-inverse btn-sm btn-block"
                                                    data-toggle="popover" data-trigger="focus" data-placement="top"
                                                    title="Información crédito" data-content="">
                                                <i class="ion-social-usd f-14 middle"></i> Información crédito
                                            </button>
                                        </div> --}}
                                        <div class="col-lg-3">
                                            <div class="animation-model">
                                                {{-- <button type="button" class="btn btn-disabled btn-danger btn-sm btn-block disabled">
                                                    <i class="icofont icofont-tick-boxed f-16 middle"></i> Gestiones en
                                                    cliente
                                                </button> --}}
                                                <div class="modal fade" id="exampleModalScrollable" tabindex="-1"
                                                     role="dialog" aria-labelledby="exampleModalScrollableTitle"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-scrollable modal-lg"
                                                         role="document">
                                                        <div class="modal-content">
                                                            {{-- <div class="modal-header">
                                                                <h5 class="modal-title"
                                                                    id="exampleModalScrollableTitle"><i
                                                                            class="icofont icofont-tick-boxed f-20 middle m-r-10"></i>Gestiones
                                                                    en el cliente</h5>
                                                                <button type="button" class="btn btn-danger btn-mini"
                                                                        data-dismiss="modal"
                                                                        style="padding-right: 4px;"><i
                                                                            class="ti-close"></i></button>
                                                            </div> --}}
                                                            {{-- <div class="card-block m-b-15"
                                                                 style="padding: 0px 15px 0px 15px!important;">
                                                                <div style="padding: 0px 1.25rem 0px 1.25rem!important;">
                                                                    <div class="row m-t-15">

                                                                        <div class="col-lg-4">
                                                                            Filtrar por tipo de gestión
                                                                        </div>
                                                                        <div class="col-lg-5">
                                                                            <select name="select"
                                                                                    class="form-control form-control-lg form-control-inverse p-t-0 p-b-0 progress-xl">
                                                                                <option value="opt1">-- Todos --
                                                                                </option>
                                                                                <option value="opt2">Cotizaciones
                                                                                </option>
                                                                                <option value="opt3">Notas de venta
                                                                                </option>
                                                                                <option value="opt4">Cambio de
                                                                                    ejecutivo
                                                                                </option>
                                                                                <option value="opt5">Anotación
                                                                                    Comercial
                                                                                </option>
                                                                                <option value="opt6">Solicitud de
                                                                                    convenio
                                                                                </option>
                                                                                <option value="opt7">Anulacion solicitud
                                                                                    de convenio
                                                                                </option>
                                                                                <option value="opt8">Asignacion de
                                                                                    Convenio
                                                                                </option>
                                                                                <option value="opt8">Propuesta de
                                                                                    Convenio
                                                                                </option>
                                                                                <option value="opt8">Aprobacion de
                                                                                    Convenio
                                                                                </option>
                                                                                <option value="opt8">Cierre de
                                                                                    convenio
                                                                                </option>
                                                                                <option value="opt8">Rechazo de
                                                                                    convenio
                                                                                </option>
                                                                                <option value="opt8">Sin respuesta a
                                                                                    convenio
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> --}}
                                                            {{-- <div class="modal-body p-t-0">
                                                                <div class="card-block p-t-0">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-xxs table-hover table-borderless ">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:110px;">Tipo</th>
                                                                                <th style="width:110px;">Detalle</th>
                                                                                <th style="width:150px;">Valorizado</th>
                                                                                <th style="width:200px;">Observación
                                                                                </th>
                                                                                <th style="width:110px;">Responsable
                                                                                </th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><label class="label label-success">Nota
                                                                                        venta</label></td>
                                                                                <td>N° 9385806</td>
                                                                                <td>$167.341</td>
                                                                                <td>Favor enviar a oficina providencia
                                                                                    1381
                                                                                </td>
                                                                                <td>LMELLA</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-block-big" style="padding: 0px 0px 20px 0px;">
                                    <div id="chartdiv"
                                         style="overflow: hidden; position: relative; text-align: left; width: 100%; height: 350px; padding: 0px; touch-action: auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-12">
                            <div class="card feed-card">
                                <div>
                                    <div class="card-block">
                                        <h2 class="text-center">Acciones</h2>
                                        <div class="tab-pane active" id="Acciones" role="tabpanel" aria-expanded="true">
                                            <div class="form-group">
                                                <button id="BotonContactos" onclick="orden()" name="btns_acciones" class="btn btn-grd-inverse btn-sm btn-round btn-block" data-toggle="modal" data-target="#contactos">
                                                    Ver contactos
                                                </button>
                                                <button id="" onclick="bitacora4()" name="btns_acciones" class="btn btn-grd-inverse btn-sm btn-round btn-block">
                                                    Reporte cobranza
                                                </button>
                                                <button onclick="bitacora3()" class="btn btn-grd-inverse btn-sm btn-round btn-block waves-effect md-trigger"
                                                        data-toggle="modal" data-target="#anotacionesModal" name="btns_acciones">
                                                    Registrar anotaciones
                                                </button>
                                                <button name="btns_acciones" id="btn_gencotiBitacora" class="btn btn-grd-inverse btn-sm btn-round btn-block"
                                                         onclick="bitacora1()">
                                                    Genera cotización o Nota de Venta
                                                </button>
                                                <button name="btns_acciones" id="btn_gencoti" style="display:none" onclick="javascript:redirecturl(id);" class="btn btn-grd-inverse btn-sm btn-round btn-block">
                                                    Genera cotización o Nota de Venta
                                                </button>
                                                <button name="btns_acciones" id="btn_listcoti" class="btn btn-grd-inverse btn-sm btn-round btn-block"
                                                        onclick="vercoti()">
                                                    Ver cotizaciones
                                                </button>
                                                <button name="btns_acciones" class="btn btn-grd-inverse btn-sm btn-round btn-block" onclick="vernota()">
                                                    Ver notas de Venta
                                                </button>
                                                <button name="btns_acciones" disabled=true class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled">
                                                    Documenta visita
                                                </button>
                                                <button name="btns_acciones" onclick="bitacora2()" class="btn btn-grd-inverse btn-sm btn-round btn-block" data-toggle="modal" data-target="#UltimasGestiones">
                                                    Ver ultimas gestiones
                                                </button>
                                                <button name="btns_acciones" disabled=true class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block" data-toggle="modal" data-target="#LosFocos">
                                                    Ver focos
                                                </button>
                                                <button name="btns_acciones" onclick="Orden2()" class="btn btn-grd-inverse btn-sm btn-round btn-block" data-toggle="modal" data-target="#ordenes">
                                                    Repetir Ordenes Web
                                                </button>
                                                <button name="btns_acciones" onclick="" class="btn btn-grd-inverse btn-sm btn-round btn-block" data-toggle="modal" data-target="#modal-send-email">
                                                    Reportes Corp
                                                </button>
                                                {{-- <button name="btns_acciones" class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled">
                                                    Foco del Cliente
                                                </button>
                                                <button name="btns_acciones" class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled">Llamada IP
                                                </button>
                                                <button name="btns_acciones" class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled">Enviar
                                                    correo
                                                </button>
                                                <button name="btns_acciones" class="btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled">Revisar
                                                    estrategias
                                                </button> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="card-header text-center">
                                    <h5>Acciones</h5>
                                </div>
                                <div class="card-block m-b-5">
                                    <div class="col-lg-12 col-md-12">
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-12">
                        </div>
                        <div id="dv_oculto" class="d-none">
                            <input id="potencial" value=""/>
                            <input id="vta_promedio" value=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="UltimasGestiones" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Gestiones realizadas</h4>
                </div>
                <input type="text" id="filtroGes" onkeyup="filtroGestiones()" placeholder="" title="Type in a name">
                <div style="height: 600px; overflow-y: scroll;">
                    <div class="modal-body">
                        <div class="card latest-update-card" id="gestionesid">
                            <div class="card-block">
                                <table class="latest-update-box">
                                    <tbody id="gestiones">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    
    <div id="contactos" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Contactos</h4>
                </div>
                <form method="post" onsubmit="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div id="losContactos" class="col-sm-12">
                                <span>Centro de Costo </span><input type="text" id="filtroCentroCosto" onkeyup="filtroCencos()" placeholder="" title="Type in a name">
                                <table id="Detallecontactos" class="table table-hover">
                                    <thead>
                                        <tr class="">
                                            <th id="rutOrden" colspan="1">Rut</th>
                                            <th id="CC" colspan="1">C.C</th>
                                            <th colspan="1">Tipo Contacto</th>
                                            <th colspan="1">Fono</th>
                                            <th colspan="1">Celular o Fax</th>
                                            <th colspan="1">Contacto</th>
                                            <th colspan="1">Cargo de Contacto</th>
                                            <th colspan="1">Mail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="relacionComercial" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Relaciones comerciales</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div id="lasrelaciones" class="col-sm-12">
                            <table id="DetalleRelaciones" class="table table-hover">
                                <thead>
                                    <tr class="">
                                        <th id="RutRelaciones" colspan="1">Rut</th>
                                        <th colspan="1">Razon social</th>
                                        <th colspan="1">Ejecutivo asignado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="ordenes" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Repetir ordenes web de hace 10 días</h4>
                </div>
                <form method="post" onsubmit="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div id="lasOrdenes" class="col-sm-12">
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="anotacionesModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="tituloAnotaciones" class="modal-title">Registrar Anotaciones</h4>
                </div>
                <form method="post" action="{{url('guardaranotaciones')}}/{{$rutcli}}" onsubmit="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Gestión realizada</label>
                            <div class="col-sm-10">
                                <select name="anotacion" id="anotacion" class="form-control" required
                                        onchange="opcionesAnotaciones(this)">
                                    <option value="">Selecciona una gestión</option>
                                    <option value="No esta / no contesta">No esta / no contesta</option>
                                    <option value="Se envia carta de presentacion">Se envía carta de presentación
                                    </option>
                                    <option value="Se contacta a cliente">Se contacta a cliente</option>
                                    <option value="Se envia Oferta">Se envía Oferta</option>
                                    <option value="Se envia Oferta Cajas">Se envía Oferta Cajas</option>
                                    <option value="Cambie lista de precios">Cambie lista de precios</option>
                                    <option value="Se envia propuesta de precios">Se envía propuesta de precios</option>
                                    <option value="Se cobran documentos pendientes">Se cobran documentos pendientes
                                    </option>
                                    <option value="Perdi Cotizacion">Perdí Cotización</option>
                                    <option value="Observacion a Potencial">Observación a Potencial Línea</option>
                                </select>
                            </div>
                        </div>
                        <div id="opciones_anotaciones">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success spinner-border spinner-border-sm mr-2" onclick="bitacora()" id="guarda">Guardar</button>
                        <button type="submit" class="btn btn-success" style="display: none;" id="Guardar_Anotacion">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="LosFocos" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg-4">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Focos</h4>
                </div>
                <form method="post" onsubmit="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-block">
                        <div id="focos" class="offset-md-2 col-md-8 text-left">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@include('back.others.js.portal')
@include('back.others.js.clientes')
@include('back.others.js.graficoficha')
@endsection
@section('custom-includes')
    <script type="text/javascript" src="{{ asset('assets/pages/todo/todo.js') }}"></script>
    <script>
        $(document).ready(function () {
            if ('{{ Session::get('error') }}' != '') {
                var nFrom = "top";
                var nAlign = "center";
                var nIcons = "fa fa-times";
                var nType = "danger";
                var nAnimIn = "animated bounceInRight";
                var nAnimOut = "animated bounceOutRight";
                var title = "";
                var message = '{{ Session::get('error') }}';

                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            }

            if ('{{ Session::get('success') }}' != '') {
                var nFrom = "top";
                var nAlign = "center";
                var nIcons = "fa fa-check";
                var nType = "success";
                var nAnimIn = "animated bounceInRight";
                var nAnimOut = "animated bounceOutRight";
                var title = "";
                var message = '{{ Session::get('success') }}';

                notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            }

            pantallaCompleta('fichacliente');
            graficoCliente();
            getDerechos();

            $('#fr_btn_ok').click(function(){
                $('#btn_buscar_cliente').click();
                document.getElementById('fr_lk_rut').className = 'nav-link';
                document.getElementById('fr_lk_razons').className = 'nav-link active';
                document.getElementById('find-rut').className='tab-pane';
                document.getElementById('find-razons').className='tab-pane active';
            });

            var rutcli = '{{ $rutcli }}';
            
            buscaCliente(rutcli);
            getCentrosCostos(rutcli);
            validaConvenio(rutcli);
            observacionCliente(rutcli);
            buscaGestiones(rutcli);
            buscaContactos(rutcli);
            buscaFocosCliente(rutcli);
            BuscarOrdenes();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('NotasPendientes()',5000);
            buscaRelaciones(rutcli);
            setTimeout('getCredito()',3000);
            setInterval('subirImagen()',2000);
        });

        $('#cencos').on('change', function() {
            getCredito();
            getCentrosCostos(document.getElementById("rut").innerText);
        });

        function bitacora(){
            $("#tituloAnotaciones").addClass("f-38");
            $("#tituloAnotaciones").text("Enviando Formulario");
            document.getElementById("guarda").style.display = "none";
            var userid='';
            var moduleid=6;
            var actionid=60;
            var oldvalue='';
            var newvalue=document.getElementById("rut").innerText;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            document.getElementById("Guardar_Anotacion").click();
        }

        function bitacora1(){
            var userid='';
            var moduleid=6;
            var actionid=56;
            var oldvalue='';
            var newvalue=document.getElementById("rut").innerText;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            document.getElementById("btn_gencoti").click();
        }

        function bitacora2(){
            var userid='';
            var moduleid=6;
            var actionid=59;
            var oldvalue='';
            var newvalue=document.getElementById("rut").innerText;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        } 

        function bitacora3(){
            invocaRegistroBitacora('registroanotaciones');
        }

        function bitacora4(){
            location.href = window.location.href.replace('fichacliente/' + document.getElementById("rut").innerText,'') + "reporteCobranza/" + document.getElementById("rut").innerText;
            //console.log(window.location.href.replace('fichacliente/' + document.getElementById("rut").innerText,'') + "reporteCobranza/" + document.getElementById("rut").innerText);
        }

        function number_format(amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

            return amount_parts.join('.');
        }

        function Orden2(){
            var userid='';
            var moduleid=6;
            var actionid=63;
            var oldvalue='';
            var newvalue=document.getElementById("rut").innerText;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            setTimeout(function() {
                document.getElementById("fecha_Orden").click();
            }, 1000);
        }

        function EnvioCartola(){
            if (document.getElementById("correo_destinatario").value == '') {
                alert("Debe escribir un destinatario");
                return false;
            }

            var token = '{{ csrf_token() }}';
            if (document.getElementById("tipo").value == 1) {
                var parametros = {
                    _token: token,
                    mail: document.getElementById("correo_destinatario").value,
                    comentario: document.getElementById("comentario_mail").value,
                    tipo: document.getElementById("tipo").value,
                    rut: document.getElementById("rut").innerText,
                    ano: document.getElementById("year").value,
                    mes: document.getElementById("month").value
                };
            }else{
                var parametros = {
                    _token: token,
                    mail: document.getElementById("correo_destinatario").value,
                    comentario: document.getElementById("comentario_mail").value,
                    tipo: document.getElementById("tipo").value,
                    rut: document.getElementById("rel_comercial").innerText,
                    ano: document.getElementById("year").value,
                    mes: document.getElementById("month").value
                };
            }
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/EnviaCartolaCorp')}}',
                type: 'GET',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    document.getElementById("enviarCorreo").style.display = "none";
                    document.getElementById("enviando").style.display = "block";
                },
                success: function (json) {
                    console.log('paso');
                    document.getElementById("enviando").style.display = "none";
                    document.getElementById("enviado").style.display = "block";
                    setTimeout('document.getElementById("cerrarCorp").click()',1000);
                    setTimeout('document.getElementById("enviado").style.display = "none"',1000);
                    setTimeout('document.getElementById("enviarCorreo").style.display = "block"',1000);
                }
                , error: function (e) {
                    console.log(e);
                    document.getElementById("enviando").style.display = "none";
                    document.getElementById("enviado").style.display = "block";
                    setTimeout('document.getElementById("cerrarCorp").click()',1000);
                    setTimeout('document.getElementById("enviado").style.display = "none"',1000);
                    setTimeout('document.getElementById("enviarCorreo").style.display = "block"',1000);
                    console.log(e.message);
                }
            });
        }

        function TomarFila1(){
            var table = document.getElementById("detalleOrdenes");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=17;
            var actionid=61;
            var oldvalue='';
            var newvalue='';
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                newvalue=cells[5].innerHTML;
                                registro(userid, moduleid, actionid, oldvalue, newvalue);
                                //console.log('http://10.10.201.100/sisdim/webDimApp/admin_v1/pedidos/indexEjecutivo.php?ej=' + newvalue);
                                location.href = 'http://10.10.201.100/sisdim/webDimApp/admin_v1/pedidos/indexEjecutivo.php?ej=' + newvalue;
                            };
            };
        }

        function BuscarOrdenes() {
            var table = '<table id="detalleOrdenes" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Acc</th>\n' +
                '                            <th>N° Orden</th>\n' +
                '                            <th>Nota de venta</th>\n' +
                '                            <th id="fecha_Orden">Fecha Orden</th>\n' +
                '                            <th>Razon Social</th>\n' +
                '                            <th>Rut</th>\n' +
                '                            <th>C.C.</th>\n' +
                '                            <th>Segmento</th>\n' +
                '                            <th>Ord. Asociada</th>\n' +
                '                            <th>Canal</th>\n' +
                '                            <th>Venta</th>\n' +
                '                            <th>Neto</th>\n' +
                '                            <th>Doc.</th>\n' +
                '                            <th>Pagada</th>\n' +
                '                            <th>Observación</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("lasOrdenes").innerHTML = table;
            var date = new Date();
            var lafecha = '';
            
            lafecha = date.getFullYear() + '-' + (date.getMonth()+1) + '-01';
            //console.log(lafecha);
            var token = '{{ csrf_token() }}';

            var parametros = {
                _token: token,
                fecha: lafecha,
                cartera: 1,
                rut: '{{ $rutcli }}'
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                headers: {
                    'Authorization':'token'
                },
                url: '{{url('/pricing/getNotasWebHist')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        // console.log(json.response);
                        var datatable = $("#detalleOrdenes").DataTable({
                            data: json.response,
                            columns: [
                                {data: "numord",
                                    render: function (data, type, row, meta) {
                                    return '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Procesa Orden Web" onclick="TomarFila1()"><span\n' +
                                                    '       class="fa fa-download f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '</div>\n';}},
                                {data: "numord",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "numnvt",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "fecord",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data.substring(0,10) + '</span>\n';}},
                                {data: "razon",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "segmento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "facnom",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "canal",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "venta",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "totnet",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + "$" + Intl.NumberFormat().format(data) + '</span>\n';}},
                                {data: "tipdoc",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "pagada",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "observ",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                        $("#spinner").hide();
                        Orden2();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function buscaCliente(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            $.ajax({
                url: '{{ url('getDatosCliente') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    if (json.code == 200) {
                        //console.log(json);
                        document.getElementById("rut").innerHTML = json.response[0].rutcli; 
                        document.getElementById("rel_comercial").innerHTML = json.response[0].id + '<a style="color:blue" title="Ver relaciones" id="rel_comercial_numero" onclick="ordenRelaciones()" data-toggle="modal" data-target="#relacionComercial"><span class="fa fa-search f-14 pull-right"></span></a>'
                        document.getElementById("Razons").innerHTML = json.response[0].razons;
                        document.getElementById("Segmento").innerHTML = json.response[0].segmento;
                        document.getElementById("Clasificacion").innerHTML = json.response[0].clasificacion;
                        document.getElementById("diasCartera").innerHTML = json.response[0].dias_para_salir_de_cartera + " días";
                        if (json.response[0].status_credito == "Activo") {
                            $("#statusCredito").css({ 'font-weight': 'bold'});
                            $("#statusCredito").css('background','#32FF00');
                            //$("#statusCredito").css({ 'color': 'green', 'font-weight': 'bold'})
                        }else{
                            //$("#statusCredito").css({ 'color': 'red'})
                            $("#statusCredito").css({ 'font-weight': 'bold'});
                            $("#statusCredito").css('background','#FF0000');
                        }
                        document.getElementById("statusCredito").innerHTML = json.response[0].status_credito;
                        // document.getElementById("Credito").innerHTML = '$' + number_format(json.response[0].credito,0);
                        // document.getElementById("Mora").innerHTML = '$' + number_format(json.response[0].deuda,0);
                        // document.getElementById("FormaPago").innerHTML = json.response[0].forma_de_pago;
                        // document.getElementById("PlazoPago").innerHTML = json.response[0].plazo_de_pago;
                        document.getElementById("VentaProm").innerHTML = '$' + number_format(json.response[0].venta_promedio,0);
                        document.getElementById("Potencial").innerHTML = '$' + number_format(json.response[0].potencial,0);
                        document.getElementById("RangoPotencial").innerHTML = json.response[0].rango_potencial;
                        document.getElementById("Brecha").innerHTML = '$' + number_format(json.response[0].brecha_contra_potencial,0);
                        document.getElementById("Cotizac").innerHTML = '$' + number_format(json.response[0].monto_cotizaciones_activas,0);
                        document.getElementById("NotasVentas").innerHTML = '$' + number_format(json.response[0].notas_sin_despachar,0);
                    }else{
                        $('#tx_rut_cliente').val('');

                        var nFrom = "top";
                        var nAlign = "center";
                        var nIcons = "fa fa-times";
                        var nType = "danger";
                        var nAnimIn = "animated bounceInRight";
                        var nAnimOut = "animated bounceOutRight";
                        var title = "";
                        var message = json;

                        notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function orden() {
            var userid='';
            var moduleid=6;
            var actionid=54;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            setTimeout(function() {
                document.getElementById("rutOrden").click();
                document.getElementById("CC").click();
            }, 1000);
        }

        function ordenRelaciones() {
            var userid='';
            var moduleid=6;
            var actionid=54;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            setTimeout(function() {
                document.getElementById("RutRelaciones").click();
            }, 1000);
        }

        function buscaGestiones(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('getGestiones') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        //console.log(json.response);
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<tr><div class="row p-b-15">\n' +
                                        '<div class="col-auto text-right update-meta">\n' +
                                            '<p class="text-muted m-b-0 d-inline">' + json.response[i].fecha.substring(0,10) + '</p>\n';
                            if (json.response[i].tipo == "Nota de Venta") {
                                text += '<i class="ion-clipboard bg-simple-c-blue update-icon"></i>\n';
                            }else{
                                if (json.response[i].tipo == "Cotizacion") {
                                    text += '<i class="ion-document-text bg-simple-c-pink update-icon"></i>\n';
                                }else{
                                    text += '<i class="fa fa-hand-rock-o bg-simple-c-green update-icon"></i>\n';
                                }
                            }
                            text += '</div>\n' +
                                        '<div class="col">\n';
                            if (json.response[i].valorizado > 0) {
                                text += '<h6>' + json.response[i].tipo + ' N°' + json.response[i].documento + ' $' + number_format(json.response[i].valorizado,0) + '</h6>\n';
                            }else{
                                text += '<h6>' + json.response[i].tipo + '</h6>\n';
                            }
                            text += '<p class="text-muted m-b-0"> Realizado por: ' + json.response[i].de + '</p>\n' +
                                            '<p class="text-muted m-b-0">' + json.response[i].observacion + '</p>\n' +
                                            '<p class="text-muted m-b-0"> hace ' + json.response[i].q_dias + ' días</p>\n' +
                                        '</div>\n' +
                                    '</tr></div>\n';
                        }
                        document.getElementById("gestiones").innerHTML = text;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function validaConvenio(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('validaConvenio') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        //console.log(json.response);
                        if (json.response == "") {
                            document.getElementById("convenio").innerHTML = 'Sin Convenio';
                        }else{
                            var fecemi = json.response[0].fecemi;
                            var fecven = json.response[0].fecven;
                            document.getElementById("convenio").innerHTML = json.response[0].numcot + '<a style="color:blue" title="Descargar convenio" id="descarga" onclick="descargaConvenio()"><span class="zmdi zmdi-download f-20 pull-right"></span></a>';
                            document.getElementById("fecini").innerHTML = fecemi.substr(8,2) + "-" + fecemi.substr(5,2) + "-" + fecemi.substr(0,4);
                            document.getElementById("fecven").innerHTML = fecven.substr(8,2) + "-" + fecven.substr(5,2) + "-" + fecven.substr(0,4);
                        }
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function getCredito() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: '{{ $rutcli }}',
                cencos: document.getElementById("cencos").value
            };
            //console.log(parametros);
            $.ajax({
                url: '{{ url('getCredito') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        document.getElementById("Credito").innerHTML = '$' + number_format(json.response[0].credito,0);
                        document.getElementById("Mora").innerHTML = '$' + number_format(json.response[0].comprometido,0);
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                    getCobrador();
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function getCobrador() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: '{{ $rutcli }}',
                cencos: document.getElementById("cencos").value
            };
            //console.log(parametros);
            $.ajax({
                url: '{{ url('getCobrador') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        document.getElementById("cobrador").innerHTML = json.response[0].codusu + ' - ' + json.response[0].nombre + ' - ' + json.response[0].rutusu;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function getCentrosCostos(rut) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rut
            };
            var text = "";
            $.ajax({
                url: '{{ url('getCencos') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        ActualizaTabla(rut, json.response[0].codigo)
                        //console.log(document.getElementById("cencos").length);
                        if (document.getElementById("cencos").length == 0) {
                            for (i = 0; i < json.response.length; i++) {
                                text += '<option value="' + json.response[i].codigo + '">' + json.response[i].codigo + ' - ' + json.response[i].descripcion + ' - ' + json.response[i].cencos + '</option>';
                            }
                            document.getElementById("idDireccion").innerText = json.response[0].direcc;
                            document.getElementById("idComuna").innerText = json.response[0].comuna;
                            document.getElementById("idProvincia").innerText = json.response[0].ciudad;
                            document.getElementById("idForma").innerText = json.response[0].forma_pago;
                            document.getElementById("idContacto").innerText = json.response[0].contac;
                            document.getElementById("Cargo").innerText = json.response[0].cargo;
                            document.getElementById("idTelContac").innerText = json.response[0].telefono;
                            document.getElementById("idCelContac").innerText = json.response[0].celular;
                            document.getElementById("idEmailContac").innerText = json.response[0].emails;
                            document.getElementById("idContacCob").innerText = json.response[0].cobranza;
                            document.getElementById("idTelCob").innerText = json.response[0].telcob;
                            document.getElementById("idEmailcob").innerText = json.response[0].mail_cob;

                            document.getElementById("cencos").innerHTML = text;
                            
                        }else {
                            for (i = 0; i < json.response.length; i++) {
                                if (document.getElementById("cencos").value == json.response[i].codigo) {
                                    document.getElementById("idDireccion").innerText = json.response[i].direcc;
                                    document.getElementById("idComuna").innerText = json.response[i].comuna;
                                    document.getElementById("idProvincia").innerText = json.response[i].ciudad;
                                    document.getElementById("idForma").innerText = json.response[i].forma_pago;
                                    document.getElementById("idContacto").innerText = json.response[i].contac;
                                    document.getElementById("Cargo").innerText = json.response[i].cargo;
                                    document.getElementById("idTelContac").innerText = json.response[i].telefono;
                                    document.getElementById("idCelContac").innerText = json.response[i].celular;
                                    document.getElementById("idEmailContac").innerText = json.response[i].emails;
                                    document.getElementById("idContacCob").innerText = json.response[i].cobranza;
                                    document.getElementById("idTelCob").innerText = json.response[i].telcob;
                                    document.getElementById("idEmailcob").innerText = json.response[i].mail_cob;
                                }
                            }
                        }
                        
                        
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function observacionCliente(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('observacionCliente') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        console.log(json.response);
                        document.getElementById("observacionCliente").value = json.response[0].observ;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }
        
        function muestraMensaje(){
            if (document.getElementById("mensaje").style.display == "none") {
                document.getElementById("mensaje").style.display = "block";
            }else {
                document.getElementById("mensaje").style.display = "none";
            }
        }

        function grabaObserv() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: document.getElementById("rut").innerText,
                observacion: document.getElementById("observacionCliente").value
            };
            //console.log(parametros);
            var text = '';
            $.ajax({
                url: '{{ url('grabaObserv') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        //console.log(json.response);
                        muestraMensaje();
                        setTimeout('muestraMensaje()',3000);
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function filtroCencos() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filtroCentroCosto");
            filter = input.value.toUpperCase();
            table = document.getElementById("Detallecontactos");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
                }
            }
        }

        function filtroGestiones() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filtroGes");
            filter = input.value.toUpperCase();
            table = document.getElementById("gestionesid");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {

                //console.log(table.getElementsByClassName("row p-b-15")[i].innerText);
                td = table.getElementsByClassName("row p-b-15")[i];

                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    table.getElementsByClassName("row p-b-15")[i].style.display = "";
                } else {
                    table.getElementsByClassName("row p-b-15")[i].style.display = "none";
                }
            }
        }

        function buscaContactos(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            $.ajax({
                data: parametros,
                url: '{{url('getContactos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#Detallecontactos").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "tipo_contacto",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "fono",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "celular_o_fax",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}},
                                {data: "contac",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "carcon",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "mail",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                    } else {
                        alert("Sin información");
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function buscaRelaciones(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            $.ajax({
                data: parametros,
                url: '{{url('getRelaciones')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json + "respuesta");
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#DetalleRelaciones").DataTable({
                            data: json.response,
                            columns: [
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "razons",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}},
                                {data: "ejecutivo",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });
                    } else {
                        alert("Sin información");
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function buscaFocosCliente(rutcli) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rutcli
            };
            var text = '';
            $.ajax({
                url: '{{ url('getfocosCliente') }}',
                data: parametros,
                type: 'POST',
                cache: false,
                datatype: 'json',
                async: true,
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        //console.log(json.response.length);
                        var i;
                        for (i = 0; i < json.response.length; i++) {
                            text += '<div class="form-check">' +
                                '<input name="check" class="form-check-input" id="check-' + json.response[i].linea + '" type="checkbox">' +
                                '<label class="form-check-label" for="check-' + json.response[i].linea + '">' + json.response[i].foco + '</label>' +
                            '</div>';
                        }
                        document.getElementById("focos").innerHTML = text;
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                },
                error: function(e){
                    var code = e.status;
                    var text = e.statusText;
                    //registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
                }
            });
        }

        function vercoti(){
            var userid='';
            var moduleid=6;
            var actionid=57;
            var oldvalue='';
            var rutcli = '{{ $rutcli }}';
            var newvalue=rutcli;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            location.href = window.location.href.replace('/fichacliente/'+ rutcli,'') + "/cotizaciones/" + rutcli;
        }

        function vernota(){
            var userid='';
            var moduleid=6;
            var actionid=58;
            var oldvalue='';
            var rutcli = '{{ $rutcli }}';
            var newvalue=rutcli;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            location.href = window.location.href.replace('/fichacliente/'+ rutcli,'') + "/notas/" + rutcli;
        }

        function descargaConvenio() {
            var userid='';
            var moduleid=6;
            var actionid=64;
            var oldvalue='';
            var rutcli = '{{ $rutcli }}';
            var newvalue=rutcli;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            window.open("{{url('descargaConvenio')}}/" + rutcli);
        }

        function ActualizaTabla(rut, cencos) {            
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: rut,
                cencos: cencos
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{ url('getCredito2') }}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json);
                    Buscar(rut);
                }
                , error: function (e) {
                    console.log('Error:' + e.message);
                }
            });
        }
    </script>

@endsection
