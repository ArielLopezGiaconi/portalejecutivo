<!DOCTYPE html>
<html>
<head>
	<title>Cotizaciones</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<style>
	@media only screen and (min-width: 200px) and (max-width: 450px){

	}
	@media only screen and (max-width: 768px){
		.main-dv table{
			max-width: 600px;
			width: 100%;
			margin: 0 auto;
		}
		#main-tbl tr td{
			width: 100%;
			float: left;
		}
		#main-tbl tr .logo img{
			padding: 12px 0px 0 !important;
			margin-bottom: 15px;
			width: 100%;
			display: block;
			max-width: 100%;
			height: auto;
		}
		img{
			display: block;
			max-width: 100%;
			height: auto;
		}
		.padd-ltr{
			padding: 0 10px;
		}
		.fnt{
			font-size: 31px !important;
		}
		.foot1{
			padding: 12px 0 0;
			width: 100%;
			display: block;
			text-align: center;
		}
		.foot2{
			width: 100%;
			float: left;
		}
		.foot2 a img{
			width: 100%;
		}
	}
	a{
		text-decoration: none;
	}

</style>
</head>

<body style="margin: 0; padding: 0; font-family: 'Open Sans', sans-serif;width: 100%;height: 100%;" >
	<div class="main-dv">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f1f1f1">
			<tr>
				<td align="center" valign="top">
					<table width="700" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid rgba(220, 220, 220, 0.62);background: #fff;">
						<tr width="700" valign="top">
							<td width="100%" align="left">
								<table id="main-tbl" width="100%" align="center">
									<tr>
										<td width="50%" align="left" class="logo">
											<img src="https://www.dimerc.cl/media/mailing/bg1.png" width="290" style="padding: 18px 18px 0;">
										</td>
										<td width="50%" align="center" cellspacing="0" cellpadding="0">
											<span style="font-size: 23px;font-weight: 700; margin-bottom: 6px;display: block;">
												Nuevas Lineas y
											</span>
											<img src="https://www.dimerc.cl/media/mailing/hed2.png" width="315">
										</td>
									</tr>
								</table>
							</td>
						</tr> <!-- Logo Section -->

						<tr>
							<td height="20"></td>
						</tr>

						<tr width="700" valign="top" align="center">
							<td width="100%" align="left">
								<table style="text-align: left; width: 100%; padding:60px" align="left" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td class="bodytitle "><span style="font-size: xx-large;">&iexcl;Ya est&aacute; lista tu cotizaci&oacute;n {{ $numcot }} !&nbsp;</span> <br /><br /><font color="#153643" size="4"><font color="#153643" size="4">Rev&iacute;sala f&aacute;cilmente desde <span style="text-decoration: underline;"><span style="color: #c62031;"><a href="https://www.dimerc.cl/cotizacion/ingreso.php?h={{ $linkcoti }}"><span style="color: #c62031; text-decoration: underline;">aqu&iacute; </span></a></span></span></font></font>
												<table class="buttonwrapper" bgcolor="#C81C2C" border="0" cellspacing="0" style="text-align: left; margin-left: auto; margin-right: auto; margin-top: 20px;" cellpadding="0">
													<tbody>
														<tr>
															<td class="button" style="width: 100%;" height="45"><a style="color: #FFF;padding-left: 10px;padding-right: 10px;" href="https://www.dimerc.cl/cotizacion/ingreso.php?h={{ $linkcoti }}">Revisar Cotizaci&oacute;n</a></td>
														</tr>
													</tbody>
												</table>
												<p><font color="" size="4"><br />
													<font color="" size="4"> Tambi&eacute;n podr&aacute;s finalizar tu compra en <span style="text-decoration: underline;"><span style="color: #c62031;"><a href="https://www.dimerc.cl/cotizacion/ingreso.php?h={{ $linkcoti }}"><span style="color: #c62031; text-decoration: underline;">dimerc.cl </span></a></span></span></font></font></p>
												</td>
											</tr>
											<tr>
												<td align="left" class="bodytitle"><span class="titlecomment">Observaciones: </span><span class="bodycomment">@if(isset($comentarioejecutivo))
																																						[ {{ $comentarioejecutivo }} ]
																																					  @endif
																																	</span></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>

							<tr>
								<td height="35"></td>
							</tr>

							<tr>
								<td height="30"></td>
							</tr>

							<tr width="700" valign="top" align="center">
								<td width="100%" align="center">
									<table width="100%" align="center">
										<tr>
											<td width="50%" align="center">
												<a href="#">
													<img src="https://www.dimerc.cl/media/mailing/hed5.png" width="190">
													
												</a>
											</td>

											<td width="50%" align="center">
												<a href="#">
													<img src="https://www.dimerc.cl/media/mailing/hed6.png" width="215">
												</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr width="700" bgcolor="#cf152d"> 
								<td width="100%" align="center">
									<table width="675" align="center">
										<tr>
											<td class="foot1" width="295" align="left" style="padding: 25px 0 0;">
												<span style="font-size: 23px;color: #fff;font-weight: 100;">
													<b> TODO </b> PARA TU TRABAJO <br>
													<b> EN UN SOLO LUGAR &nbsp;;) </b>
												</span>
											</td>

											<td class="foot2" width="405" align="right" valign="center"> 
												<a href="#">
													<img src="https://www.dimerc.cl/media/mailing/icons.png" width="400" style="margin: 22px 0 0;"> 
												</a>
											</td>
										</tr>
									</table>
					            </td>
                            </tr>

                            <tr width="700" valign="top" align="center" bgcolor="#cf152d">
                                <td width="100%" align="center">
                                    <table>
                                        <tr>
                                            <td width="700" align="center">
                                                <img src="https://www.dimerc.cl/media/mailing/dotted.png" width="595">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr width="700" valign="top" align="center" bgcolor="#cf152d">
                                <td width="100%" align="center">
                                    <table>
                                        <tr>
                                            <td width="233" align="center">
                                                <a href="#">
                                                    <span style="color: #fff; font-weight: 600; font-style: italic; font-size: 18px;">
                                                        dimerc.cl
                                                    </span>
                                                </a>
                                            </td>
                                            <td width="233" align="center">
                                                <a href="#">
                                                    <img src="https://www.dimerc.cl/media/mailing/03.png" style="width: 25px;display: inline-block;"/><span style="color: #fff; font-weight: 600; font-style: italic; font-size: 18px;">
                                                        info@dimerc.cl
                                                    </span>
                                                </a>
                                            </td>
                                            <td width="233" align="center">
                                                <a href="#">
                                                    <img src="https://www.dimerc.cl/media/mailing/02.png" style="width: 25px;display: inline-block;"/><span style="color: #fff; font-weight: 600; font-style: italic; font-size: 18px;">
                                                        2 2385 82 00
                                                    </span>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>