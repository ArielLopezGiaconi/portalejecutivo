<div class="page-body">
    <div class="row" >
        <div  class="chat-box" style="margin-right: 0%;">
            <ul class="text-right boxs" >
                <li  id="chat" class="chat-single-box card-shadow bg-white active" data-id="1" style="width: 350px; display: none">
                    <div class="had-container">
                        <div class="chat-header p-10 bg-gray">
                            <div class="user-info d-inline-block f-left">
                                <p>Soporte Dimerc</p>
                            </div>
                            <div class="box-tools d-inline-block">
                                <a href="#" onclick="minimiza()">
                                    <i class="icofont icofont-minus f-20 m-r-10"></i>
                                </a>
                                <a href="#" onclick="cerrar()">
                                    <i class="icofont icofont-close f-20"></i>
                                </a>
                            </div>
                        </div>
                        <div id="body" class="chat-body p-10" style="width: 350px">
                            <div id="bodyChat" class="message-scrooler">
                                {{-- <div class="messages">
                                    <div class="message out no-avatar media">
                                        <div class="body media-body text-right p-l-50">
                                            <div style="background-color: rgb(0, 132, 255)" class="content msg-reply f-14 text-white">Good morning..</div>
                                            <div class="seen">
                                                <i class="icon-clock f-12 m-r-5 "></i>
                                                <span class="f-12">a few seconds ago </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="sender media-right friend-box">
                                            <a href="javascript:void(0);" title="Me">
                                                <img src="{{ $fotoperfil }}" class="img-chat-profile" alt="Me">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="messages">
                                    <div class="message out no-avatar media">
                                        <div class="sender media-left friend-box">
                                            <a href="javascript:void(0);" title="Me">
                                                <img src="assets/img/suppot.png" class="img-chat-profile" alt="Me">
                                            </a>
                                        </div>
                                        <div class="pull-left">
                                            <div class="seen">
                                                <div style="background-color: rgb(0, 132, 255)" class="content msg-reply f-14 text-white text-left">ok..</div>
                                                <i class="icon-clock f-12 txt-muted"></i>
                                                <span class="f-12">a few seconds ago </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="chat-footer b-t-muted">
                            <div class="input-group write-msg">
                                <input id="newMessage" type="text" onkeypress="InputChatEnter(event)" class="form-control input-value" placeholder="Type a Message">
                                <span class="input-group-btn">
                                    <button id="paper-btn" class="btn btn-primary" type="button" onclick="InputChat()">
                                        <i class="icofont icofont-paper-plane"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </li>
                <li id="chat_min" class="chat-single-box active" data-id="1" style="display: none">
                    <div class="had-container" >
                        <div class="chat-header p-10 " style="bottom: 0; position: absolute; width: 350px; right: 15px; background-color: #404E67 !important;">
                            <div class="user-info d-inline-block f-left">
                                <p class="text-white">Soporte Dimerc</p>
                            </div>
                            <div class="box-tools ">
                                <a class="text-white" href="#" onclick="minimiza()">
                                    <i class="icofont icofont-minus f-20 m-r-10"></i>
                                </a>
                                <a class="text-white" href="#" onclick="cerrar()">
                                    <i class="icofont icofont-close f-20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>