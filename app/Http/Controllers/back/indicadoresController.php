<?php

namespace App\Http\Controllers\back;

use App\PDO\Lib\ResponseFormatt;
use App\Http\Controllers\Controller;
use App\PDO\MySql\PortalEjecutivo\PortalEjUsuariosPDO;
use App\PDO\Oracle\DMVentas\DeClientePDO;
use App\PDO\Oracle\DMVentas\DmVentasPDO;
use App\PDO\Oracle\DMVentas\EnClientePDO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class indicadoresController extends Controller
{
    //************************************ ESTADISTICAS DEL DIA
    public function getEstadisticasDia(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getEstadisticasDia($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getEstadisticasDiaTotales(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getEstadisticasDiaTotales($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    //************************************ ESTADISTICAS DEL MES
    public function getEstadisticasMes(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getEstadisticasMes($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getEstadisticasMesTotales(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getEstadisticasMesTotales($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    //************************************ CARTERA DEL MES
    public function getCarteraMes(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getCarteraMes($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }
    
    public function getCarteraMesTotales(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getCarteraMesTotales($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    //************************************ SEGUIMIENTO NOTAS
    public function getseguimientoNota(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getseguimientoNota($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }
    
    public function getseguimientoNotaTotales(Request $request)
    {
        $usuario = Session::get('usuario');
        $grupo = $usuario->get('codcnl');

        $resultado = PortalEjUsuariosPDO::getseguimientoNotaTotales($grupo);
        $respuesta = new ResponseFormatt();
        $respuesta->setCode(200)
            ->setResponse($resultado);
        return $respuesta->returnToJson();
    }

    public function getIndicadores(Request $request)
    {
        $data = array();
        $clientes = PortalEjUsuariosPDO::getTiposCliente();
        
        foreach ($clientes as $cliente) {
            $result = PortalEjUsuariosPDO::getDataDashboard($cliente->option);
            $venta = $result[0]->venta_dia;
            $cm = $result[0]->correccionmonetaria;
            $mp = $result[0]->aporte_marcapropia;
            $costos = $result[0]->costos;
            $venta_web = $result[0]->venta_web;
            $venta_tlmk = $result[0]->venta_tlmk;
            $venta_convenio = $result[0]->venta_convenio;
            $venta_no_convenio = $result[0]->venta_no_convenio;
            // $ffds = $result[0]->venta_ffds;
            $bole = $result[0]->venta_boleta;
            // $tech = $result[0]->venta_tech;
            $anto = $result[0]->venta_antofagasta;
            $cajas = $result[0]->venta_cajas;
            if ($venta == 0) {
                $participacion_ecommerce = 0;
                $participacion_convenio = 0;
                $margen_frontal = 0;
                $perct_cm = 0;
                $perct_mp = 0;
                $global = 0;
            } else {
                $participacion_ecommerce = ($venta_web / ($venta_web + $venta_tlmk)) * 100;
                $participacion_convenio = ($venta_convenio / ($venta_convenio + $venta_no_convenio)) * 100;
                $margen_frontal = (($venta - $costos) / $venta) * 100;
                $perct_cm = ($cm / $venta) * 100;
                $perct_mp = ($mp / $venta) * 100;
                $global = $margen_frontal + $perct_cm + $perct_mp;
            }

            $data['ventaDia_' . $cliente->alias] = "$" . number_format($venta, 0, ',', '.');
            $data['participacionEcommerce_' . $cliente->alias] = number_format($participacion_ecommerce, 2, ',', '.') . "%";
            $data['participacionConvenio_' . $cliente->alias] = number_format($participacion_convenio, 2, ',', '.') . "%";
            // $data['ffds_' . $cliente->alias] = "$" . number_format($ffds, 0, ',', '.');
            $data['bole_' . $cliente->alias] = "$" . number_format($bole, 0, ',', '.');
            $data['anto_' . $cliente->alias] = "$" . number_format($anto, 0, ',', '.');
            $data['cajas_' . $cliente->alias] = "$" . number_format($cajas, 0, ',', '.');
            // $data['anto_' . $cliente->alias] = "$" . number_format($anto, 0, ',', '.');
            $data['margenfrontal_' . $cliente->alias] = number_format($margen_frontal, 2, ',', '.') . "%";
            $data['cm_' . $cliente->alias] = number_format($perct_cm, 2, ',', '.') . "%";
            $data['marcapropia_' . $cliente->alias] = number_format($perct_mp, 2, ',', '.') . "%";
            $data['margenglobal_' . $cliente->alias] = number_format($global, 2, ',', '.') . "%";

            /*Metas*/
            $data['meta_venta_' . $cliente->alias] = PortalEjUsuariosPDO::getMetaVenta($cliente->option, $venta);
            $data['meta_mf_' . $cliente->alias] = PortalEjUsuariosPDO::getMetaMF($cliente->option, number_format($margen_frontal, 2));
            $data['meta_cm_' . $cliente->alias] = PortalEjUsuariosPDO::getMetaCM($cliente->option, number_format($perct_cm, 2));
            $data['meta_mp_' . $cliente->alias] = PortalEjUsuariosPDO::getMetaMP($cliente->option, number_format($perct_mp, 2));
            $data['meta_mg_' . $cliente->alias] = PortalEjUsuariosPDO::getMetaMG($cliente->option, number_format($global, 2));
            /*Metas*/

            /*datos lineas*/
            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'ASEO, HIGIENE Y OTROS');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_aseo_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_aseo_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_aseo_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_aseo_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_aseo_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'CAFETERIA Y MENAJE');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_cafeteria_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_cafeteria_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_cafeteria_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_cafeteria_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_cafeteria_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'CAJAS');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_cajas_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_cajas_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_cajas_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_cajas_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_cajas_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'TECNOLOGIA');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_tecnologia_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_tecnologia_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_tecnologia_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_tecnologia_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_tecnologia_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'LIBRERIA');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_libreria_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_libreria_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_libreria_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_libreria_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_libreria_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'NO DEFINIDO');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_nd_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_nd_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_nd_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_nd_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_nd_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'PAPEL FOTOCOPIA');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_papelfotocopia_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_papelfotocopia_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_papelfotocopia_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_papelfotocopia_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_papelfotocopia_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'TISSUE');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_tissue_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_tissue_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_tissue_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_tissue_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_tissue_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);

            $result_linea = PortalEjUsuariosPDO::getDataDashboardLineas($cliente->option, 'SEGURIDAD INDUSTRIAL');
            $venta_linea = $result_linea[0]->venta_dia;
            $cm_linea = $result_linea[0]->correccionmonetaria;
            $mp_linea = $result_linea[0]->aporte_marcapropia;
            $costo_linea = $result_linea[0]->costos;
            if ($venta_linea == 0) {
                $margen_frontal_linea = 0;
                $perct_cm_linea = 0;
                $perct_mp_linea = 0;
                $global_linea = 0;
            } else {
                $margen_frontal_linea = (($venta_linea - $costo_linea) / $venta_linea) * 100;
                $perct_cm_linea = ($cm_linea / $venta_linea) * 100;
                $perct_mp_linea = ($mp_linea / $venta_linea) * 100;
                $global_linea = $margen_frontal_linea + $perct_cm_linea + $perct_mp_linea;
            }
            $data['venta_seguridad_' . $cliente->alias] = "$" . number_format($venta_linea, 0, ',', '.');
            $data['margen_seguridad_' . $cliente->alias] = number_format($margen_frontal_linea, 2, ',', '.') . "%";
            $data['cm_seguridad_' . $cliente->alias] = number_format($perct_cm_linea, 2, ',', '.') . "%";
            $data['mp_seguridad_' . $cliente->alias] = number_format($perct_mp_linea, 2, ',', '.') . "%";
            $data['comercial_seguridad_' . $cliente->alias] = number_format($global_linea, 2, ',', '.') . "%";
            unset($result_linea);
            /*datos lineas*/

        }

        //totales
        $last = PortalEjUsuariosPDO::getLastUpdate();
       
        $data['count_total'] = " - " . PortalEjUsuariosPDO::getCount('All') . " Notas de venta desde el " . $last->desde . " a " . $last->last_update;
        $data['count_ge'] = " - " . PortalEjUsuariosPDO::getCount('gran_empresa') . " Notas de venta desde el " . $last->desde . " a " . $last->last_update;
        $data['count_pyme'] = " - " . PortalEjUsuariosPDO::getCount('pyme') . " Notas de venta desde el " . $last->desde . " a " . $last->last_update;
        $data['count_corp'] = " - " . PortalEjUsuariosPDO::getCount('dimerc_corp') . " Notas de venta desde el " . $last->desde . " a " . $last->last_update;
        $data['count_gob'] = " - " . PortalEjUsuariosPDO::getCount('gobierno') . " Notas de venta desde el " . $last->desde . " a " . $last->last_update;

        return json_encode($data);
    }

}