<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 11:35
 */

namespace App\Entities\Oracle\Holding;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class MaEmpresa
{
    private $codemp;
    private $emisor;
    private $prelis_expresado_neto;
    private $usr_creac;
    private $fecha_creac;
    private $usr_modif;
    private $fecha_modif;
    private $nombre;
    private $direcc;
    private $codcom;
    private $codciu;
    private $bodvta;
    private $bodtra;
    private $bodapa;
    private $bodven;
    private $bodfal;
    private $slogan;
    private $codreg;
    private $coduni;
    private $pordif;
    private $tolera;
    private $vigenc;
    private $rutemp;
    private $digemp;
    private $mensages;
    private $vcto_cabecera;
    private $rappel;
    private $factor_puntos;
    private $valida_costo_promedio;
    private $codsap;
    private $sap_sociedad;
    private $sap_centro_principal;
    private $sap_clase_devolucion;
    private $sap_clase_ncr;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->codemp = isset($registro->codemp) ? $registro->codemp : null;
            $this->emisor = isset($registro->emisor) ? $registro->emisor : null;
            $this->prelis_expresado_neto = isset($registro->prelis_expresado_neto) ? $registro->prelis_expresado_neto : null;
            $this->usr_creac = isset($registro->usr_creac) ? $registro->usr_creac : null;
            $this->fecha_creac = isset($registro->fecha_creac) ? $registro->fecha_creac : null;
            $this->usr_modif = isset($registro->usr_modif) ? $registro->usr_modif : null;
            $this->fecha_modif = isset($registro->fecha_modif) ? $registro->fecha_modif : null;
            $this->nombre = isset($registro->nombre) ? $registro->nombre : null;
            $this->direcc = isset($registro->direcc) ? $registro->direcc : null;
            $this->codcom = isset($registro->codcom) ? $registro->codcom : null;
            $this->codciu = isset($registro->codciu) ? $registro->codciu : null;
            $this->bodvta = isset($registro->bodvta) ? $registro->bodvta : null;
            $this->bodtra = isset($registro->bodtra) ? $registro->bodtra : null;
            $this->bodapa = isset($registro->bodapa) ? $registro->bodapa : null;
            $this->bodven = isset($registro->bodven) ? $registro->bodven : null;
            $this->bodfal = isset($registro->bodfal) ? $registro->bodfal : null;
            $this->slogan = isset($registro->slogan) ? $registro->slogan : null;
            $this->codreg = isset($registro->codreg) ? $registro->codreg : null;
            $this->coduni = isset($registro->coduni) ? $registro->coduni : null;
            $this->pordif = isset($registro->pordif) ? $registro->pordif : null;
            $this->tolera = isset($registro->tolera) ? $registro->tolera : null;
            $this->vigenc = isset($registro->vigenc) ? $registro->vigenc : null;
            $this->rutemp = isset($registro->rutemp) ? $registro->rutemp : null;
            $this->digemp = isset($registro->digemp) ? $registro->digemp : null;
            $this->mensages = isset($registro->mensages) ? $registro->mensages : null;
            $this->vcto_cabecera = isset($registro->vcto_cabecera) ? $registro->vcto_cabecera : null;
            $this->rappel = isset($registro->rappel) ? $registro->rappel : null;
            $this->factor_puntos = isset($registro->factor_puntos) ? $registro->factor_puntos : null;
            $this->valida_costo_promedio = isset($registro->valida_costo_promedio) ? $registro->valida_costo_promedio : null;
            $this->codsap = isset($registro->codsap) ? $registro->codsap : null;
            $this->sap_sociedad = isset($registro->sap_sociedad) ? $registro->sap_sociedad : null;
            $this->sap_centro_principal = isset($registro->sap_centro_principal) ? $registro->sap_centro_principal : null;
            $this->sap_clase_devolucion = isset($registro->sap_clase_devolucion) ? $registro->sap_clase_devolucion : null;
            $this->sap_clase_ncr = isset($registro->sap_clase_ncr) ? $registro->sap_clase_ncr : null;
        }
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getEmisor()
    {
        return $this->emisor;
    }

    public function setEmisor($emisor)
    {
        $this->emisor = $emisor;
    }

    public function getPrelisExpresadoNeto()
    {
        return $this->prelis_expresado_neto;
    }

    public function setPrelisExpresadoNeto($prelis_expresado_neto)
    {
        $this->prelis_expresado_neto = $prelis_expresado_neto;
    }

    public function getUsrCreac()
    {
        return $this->usr_creac;
    }

    public function setUsrCreac($usr_creac)
    {
        $this->usr_creac = $usr_creac;
    }

    public function getFechaCreac()
    {
        return $this->fecha_creac;
    }

    public function setFechaCreac($fecha_creac)
    {
        $this->fecha_creac = $fecha_creac;
    }

    public function getUsrModif()
    {
        return $this->usr_modif;
    }

    public function setUsrModif($usr_modif)
    {
        $this->usr_modif = $usr_modif;
    }

    public function getFechaModif()
    {
        return $this->fecha_modif;
    }

    public function setFechaModif($fecha_modif)
    {
        $this->fecha_modif = $fecha_modif;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getDirecc()
    {
        return $this->direcc;
    }

    public function setDirecc($direcc)
    {
        $this->direcc = $direcc;
    }

    public function getCodcom()
    {
        return $this->codcom;
    }

    public function setCodcom($codcom)
    {
        $this->codcom = $codcom;
    }

    public function getCodciu()
    {
        return $this->codciu;
    }

    public function setCodciu($codciu)
    {
        $this->codciu = $codciu;
    }

    public function getBodvta()
    {
        return $this->bodvta;
    }

    public function setBodvta($bodvta)
    {
        $this->bodvta = $bodvta;
    }

    public function getBodtra()
    {
        return $this->bodtra;
    }

    public function setBodtra($bodtra)
    {
        $this->bodtra = $bodtra;
    }

    public function getBodapa()
    {
        return $this->bodapa;
    }

    public function setBodapa($bodapa)
    {
        $this->bodapa = $bodapa;
    }

    public function getBodven()
    {
        return $this->bodven;
    }

    public function setBodven($bodven)
    {
        $this->bodven = $bodven;
    }

    public function getBodfal()
    {
        return $this->bodfal;
    }

    public function setBodfal($bodfal)
    {
        $this->bodfal = $bodfal;
    }

    public function getSlogan()
    {
        return $this->slogan;
    }

    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    public function getCodreg()
    {
        return $this->codreg;
    }

    public function setCodreg($codreg)
    {
        $this->codreg = $codreg;
    }

    public function getCoduni()
    {
        return $this->coduni;
    }

    public function setCoduni($coduni)
    {
        $this->coduni = $coduni;
    }

    public function getPordif()
    {
        return $this->pordif;
    }

    public function setPordif($pordif)
    {
        $this->pordif = $pordif;
    }

    public function getTolera()
    {
        return $this->tolera;
    }

    public function setTolera($tolera)
    {
        $this->tolera = $tolera;
    }

    public function getVigenc()
    {
        return $this->vigenc;
    }

    public function setVigenc($vigenc)
    {
        $this->vigenc = $vigenc;
    }

    public function getRutemp()
    {
        return $this->rutemp;
    }

    public function setRutemp($rutemp)
    {
        $this->rutemp = $rutemp;
    }

    public function getDigemp()
    {
        return $this->digemp;
    }

    public function setDigemp($digemp)
    {
        $this->digemp = $digemp;
    }

    public function getMensages()
    {
        return $this->mensages;
    }

    public function setMensages($mensages)
    {
        $this->mensages = $mensages;
    }

    public function getVctoCabecera()
    {
        return $this->vcto_cabecera;
    }

    public function setVctoCabecera($vcto_cabecera)
    {
        $this->vcto_cabecera = $vcto_cabecera;
    }

    public function getRappel()
    {
        return $this->rappel;
    }

    public function setRappel($rappel)
    {
        $this->rappel = $rappel;
    }

    public function getFactorPuntos()
    {
        return $this->factor_puntos;
    }

    public function setFactorPuntos($factor_puntos)
    {
        $this->factor_puntos = $factor_puntos;
    }

    public function getValidaCostoPromedio()
    {
        return $this->valida_costo_promedio;
    }

    public function setValidaCostoPromedio($valida_costo_promedio)
    {
        $this->valida_costo_promedio = $valida_costo_promedio;
    }

    public function getCodsap()
    {
        return $this->codsap;
    }

    public function setCodsap($codsap)
    {
        $this->codsap = $codsap;
    }

    public function getSapSociedad()
    {
        return $this->sap_sociedad;
    }

    public function setSapSociedad($sap_sociedad)
    {
        $this->sap_sociedad = $sap_sociedad;
    }

    public function getSapCentroPrincipal()
    {
        return $this->sap_centro_principal;
    }

    public function setSapCentroPrincipal($sap_centro_principal)
    {
        $this->sap_centro_principal = $sap_centro_principal;
    }

    public function getSapClaseDevolucion()
    {
        return $this->sap_clase_devolucion;
    }

    public function setSapClaseDevolucion($sap_clase_devolucion)
    {
        $this->sap_clase_devolucion = $sap_clase_devolucion;
    }

    public function getSapClaseNcr()
    {
        return $this->sap_clase_ncr;
    }

    public function setSapClaseNcr($sap_clase_ncr)
    {
        $this->sap_clase_ncr = $sap_clase_ncr;
    }
}
