<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 16:26
 */

namespace App\Entities\MySql\Dimerc;

// use Illuminate\Database\Eloquent\Model;

class DimProdOpl implements \JsonSerializable
{
    private $id;
    private $rut;
    private $sku;

    public function __construct()
    {
        $this->id = null;
        $this->rut = 0;
        $this->sku = null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getRut()
    {
        return $this->rut;
    }

    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'rut' => $this->getRut(),
            'sku' => $this->getSku(),
        ];
    }
}
