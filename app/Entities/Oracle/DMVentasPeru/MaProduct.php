<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 11:54
 */

namespace App\Entities\Oracle\DMVentasPeru;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class MaProduct implements \JsonSerializable
{
    public $codpro;
    public $despro;
    public $codlin;
    public $codfam;
    public $codmar;
    public $codmod;
    public $codbar;
    public $coduni;
    public $pesopr;
    public $estdes;
    public $minimo;
    public $maximo;
    public $ultcos;
    public $indsbr;
    public $codbdg;
    public $unmivt;
    public $pedsto;
    public $moneda;
    public $colate;
    public $tipalm;
    public $estpro;
    public $reembj;
    public $ultinv;
    public $fecinv;
    public $pagcat;
    public $numfto;
    public $ultlib;
    public $numser;
    public $merbod;
    public $mercli;
    public $fecing;
    public $asegur;
    public $stkcri;
    public $stkint;
    public $proneo;
    public $tradis;
    public $resfis;
    public $codigo_uni;
    public $despro2;
    public $oferta;
    public $exist_ant;
    public $exist_tra_oc;
    public $cospro_ant_dm;
    public $exist_tra_mo;
    public $maxcd;
    public $mincd;
    public $cricd;
    public $intcd;
    public $stkini;
    public $fecini;
    public $audita;
    public $stkarm;
    public $rutprv;
    public $merbod2;
    public $stk_dimeiggs;
    public $marca_dimeiggs;
    public $con_vencimiento;
    public $embalaje;
    public $subembalaje;
    public $ultima_recepcion;
    public $ultima_nota;
    public $ultima_gmm;
    public $afecto;
    public $igv;
    public $importado;
    public $tipoprod;
    public $vida_util;
    public $xdelta;
    public $codsap;
    public $desprocorta;
    public $desprolarga;
    public $codjer;
    public $cod_linea;
    public $deslin;
    public $codsec;
    public $dessec;
    public $codrub;
    public $desrub;
    public $codsubgru;
    public $dessubgru;
    public $codgru;
    public $desgru;
    public $desmar;
    public $peso_neto;
    public $uni_peso;
    public $uni_volumen;
    public $den_conv_emb;
    public $peso_neto_emb;
    public $peso;
    public $volumen;
    public $peso_emb;
    public $peso_neto_sub_emb;
    public $peso_bruto_sub_emb;
    public $volumen_emb;
    public $uni_peso_emb;
    public $uni_volumen_emb;
    public $volumen_sub_emb;
    public $uni_peso_sub_emb;
    public $uni_volumen_sub_emb;
    public $pais;
    public $impuesto;
    public $nomprv;
    public $tipoprod_sap;
    public $codlin_sap;
    public $codmar_sap;
    public $coduni_sap;
    public $codneg;
    public $categoria;
    public $marca_propia;
    public $tiprot;
    public $den_conv_sub_emb;
    public $uni_sub_emb;
    public $uni_embalaje;
    public $cosprom;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->codpro = isset($registro->codpro) ? $registro->codpro : null;
            $this->despro = isset($registro->despro) ? $registro->despro : null;
            $this->codlin = isset($registro->codlin) ? $registro->codlin : null;
            $this->codfam = isset($registro->codfam) ? $registro->codfam : null;
            $this->codmar = isset($registro->codmar) ? $registro->codmar : null;
            $this->codmod = isset($registro->codmod) ? $registro->codmod : null;
            $this->codbar = isset($registro->codbar) ? $registro->codbar : null;
            $this->coduni = isset($registro->coduni) ? $registro->coduni : null;
            $this->pesopr = isset($registro->pesopr) ? $registro->pesopr : null;
            $this->estdes = isset($registro->estdes) ? $registro->estdes : null;
            $this->minimo = isset($registro->minimo) ? $registro->minimo : null;
            $this->maximo = isset($registro->maximo) ? $registro->maximo : null;
            $this->ultcos = isset($registro->ultcos) ? $registro->ultcos : null;
            $this->indsbr = isset($registro->indsbr) ? $registro->indsbr : null;
            $this->codbdg = isset($registro->codbdg) ? $registro->codbdg : null;
            $this->unmivt = isset($registro->unmivt) ? $registro->unmivt : null;
            $this->pedsto = isset($registro->pedsto) ? $registro->pedsto : null;
            $this->moneda = isset($registro->moneda) ? $registro->moneda : null;
            $this->colate = isset($registro->colate) ? $registro->colate : null;
            $this->tipalm = isset($registro->tipalm) ? $registro->tipalm : null;
            $this->estpro = isset($registro->estpro) ? $registro->estpro : null;
            $this->reembj = isset($registro->reembj) ? $registro->reembj : null;
            $this->ultinv = isset($registro->ultinv) ? $registro->ultinv : null;
            $this->fecinv = isset($registro->fecinv) ? $registro->fecinv : null;
            $this->pagcat = isset($registro->pagcat) ? $registro->pagcat : null;
            $this->numfto = isset($registro->numfto) ? $registro->numfto : null;
            $this->ultlib = isset($registro->ultlib) ? $registro->ultlib : null;
            $this->numser = isset($registro->numser) ? $registro->numser : null;
            $this->merbod = isset($registro->merbod) ? $registro->merbod : null;
            $this->mercli = isset($registro->mercli) ? $registro->mercli : null;
            $this->fecing = isset($registro->fecing) ? $registro->fecing : null;
            $this->asegur = isset($registro->asegur) ? $registro->asegur : null;
            $this->stkcri = isset($registro->stkcri) ? $registro->stkcri : null;
            $this->stkint = isset($registro->stkint) ? $registro->stkint : null;
            $this->proneo = isset($registro->proneo) ? $registro->proneo : null;
            $this->tradis = isset($registro->tradis) ? $registro->tradis : null;
            $this->resfis = isset($registro->resfis) ? $registro->resfis : null;
            $this->codigo_uni = isset($registro->codigo_uni) ? $registro->codigo_uni : null;
            $this->despro2 = isset($registro->despro2) ? $registro->despro2 : null;
            $this->oferta = isset($registro->oferta) ? $registro->oferta : null;
            $this->exist_ant = isset($registro->exist_ant) ? $registro->exist_ant : null;
            $this->exist_tra_oc = isset($registro->exist_tra_oc) ? $registro->exist_tra_oc : null;
            $this->cospro_ant_dm = isset($registro->cospro_ant_dm) ? $registro->cospro_ant_dm : null;
            $this->exist_tra_mo = isset($registro->exist_tra_mo) ? $registro->exist_tra_mo : null;
            $this->maxcd = isset($registro->maxcd) ? $registro->maxcd : null;
            $this->mincd = isset($registro->mincd) ? $registro->mincd : null;
            $this->cricd = isset($registro->cricd) ? $registro->cricd : null;
            $this->intcd = isset($registro->intcd) ? $registro->intcd : null;
            $this->stkini = isset($registro->stkini) ? $registro->stkini : null;
            $this->fecini = isset($registro->fecini) ? $registro->fecini : null;
            $this->audita = isset($registro->audita) ? $registro->audita : null;
            $this->stkarm = isset($registro->stkarm) ? $registro->stkarm : null;
            $this->rutprv = isset($registro->rutprv) ? $registro->rutprv : null;
            $this->merbod2 = isset($registro->merbod2) ? $registro->merbod2 : null;
            $this->stk_dimeiggs = isset($registro->stk_dimeiggs) ? $registro->stk_dimeiggs : null;
            $this->marca_dimeiggs = isset($registro->marca_dimeiggs) ? $registro->marca_dimeiggs : null;
            $this->con_vencimiento = isset($registro->con_vencimiento) ? $registro->con_vencimiento : null;
            $this->embalaje = isset($registro->embalaje) ? $registro->embalaje : null;
            $this->subembalaje = isset($registro->subembalaje) ? $registro->subembalaje : null;
            $this->ultima_recepcion = isset($registro->ultima_recepcion) ? $registro->ultima_recepcion : null;
            $this->ultima_nota = isset($registro->ultima_nota) ? $registro->ultima_nota : null;
            $this->ultima_gmm = isset($registro->ultima_gmm) ? $registro->ultima_gmm : null;
            $this->afecto = isset($registro->afecto) ? $registro->afecto : null;
            $this->igv = isset($registro->igv) ? $registro->igv : null;
            $this->importado = isset($registro->importado) ? $registro->importado : null;
            $this->tipoprod = isset($registro->tipoprod) ? $registro->tipoprod : null;
            $this->vida_util = isset($registro->vida_util) ? $registro->vida_util : null;
            $this->xdelta = isset($registro->xdelta) ? $registro->xdelta : null;
            $this->codsap = isset($registro->codsap) ? $registro->codsap : null;
            $this->desprocorta = isset($registro->desprocorta) ? $registro->desprocorta : null;
            $this->desprolarga = isset($registro->desprolarga) ? $registro->desprolarga : null;
            $this->codjer = isset($registro->codjer) ? $registro->codjer : null;
            $this->cod_linea = isset($registro->cod_linea) ? $registro->cod_linea : null;
            $this->deslin = isset($registro->deslin) ? $registro->deslin : null;
            $this->codsec = isset($registro->codsec) ? $registro->codsec : null;
            $this->dessec = isset($registro->dessec) ? $registro->dessec : null;
            $this->codrub = isset($registro->codrub) ? $registro->codrub : null;
            $this->desrub = isset($registro->desrub) ? $registro->desrub : null;
            $this->codsubgru = isset($registro->codsubgru) ? $registro->codsubgru : null;
            $this->dessubgru = isset($registro->dessubgru) ? $registro->dessubgru : null;
            $this->codgru = isset($registro->codgru) ? $registro->codgru : null;
            $this->desgru = isset($registro->desgru) ? $registro->desgru : null;
            $this->desmar = isset($registro->desmar) ? $registro->desmar : null;
            $this->peso_neto = isset($registro->peso_neto) ? $registro->peso_neto : null;
            $this->uni_peso = isset($registro->uni_peso) ? $registro->uni_peso : null;
            $this->uni_volumen = isset($registro->uni_volumen) ? $registro->uni_volumen : null;
            $this->den_conv_emb = isset($registro->den_conv_emb) ? $registro->den_conv_emb : null;
            $this->peso_neto_emb = isset($registro->peso_neto_emb) ? $registro->peso_neto_emb : null;
            $this->peso = isset($registro->peso) ? $registro->peso : null;
            $this->volumen = isset($registro->volumen) ? $registro->volumen : null;
            $this->peso_emb = isset($registro->peso_emb) ? $registro->peso_emb : null;
            $this->peso_neto_sub_emb = isset($registro->peso_neto_sub_emb) ? $registro->peso_neto_sub_emb : null;
            $this->peso_bruto_sub_emb = isset($registro->peso_bruto_sub_emb) ? $registro->peso_bruto_sub_emb : null;
            $this->volumen_emb = isset($registro->volumen_emb) ? $registro->volumen_emb : null;
            $this->uni_peso_emb = isset($registro->uni_peso_emb) ? $registro->uni_peso_emb : null;
            $this->uni_volumen_emb = isset($registro->uni_volumen_emb) ? $registro->uni_volumen_emb : null;
            $this->volumen_sub_emb = isset($registro->volumen_sub_emb) ? $registro->volumen_sub_emb : null;
            $this->uni_peso_sub_emb = isset($registro->uni_peso_sub_emb) ? $registro->uni_peso_sub_emb : null;
            $this->uni_volumen_sub_emb = isset($registro->uni_volumen_sub_emb) ? $registro->uni_volumen_sub_emb : null;
            $this->pais = isset($registro->pais) ? $registro->pais : null;
            $this->impuesto = isset($registro->impuesto) ? $registro->impuesto : null;
            $this->nomprv = isset($registro->nomprv) ? $registro->nomprv : null;
            $this->tipoprod_sap = isset($registro->tipoprod_sap) ? $registro->tipoprod_sap : null;
            $this->codlin_sap = isset($registro->codlin_sap) ? $registro->codlin_sap : null;
            $this->codmar_sap = isset($registro->codmar_sap) ? $registro->codmar_sap : null;
            $this->coduni_sap = isset($registro->coduni_sap) ? $registro->coduni_sap : null;
            $this->codneg = isset($registro->codneg) ? $registro->codneg : null;
            $this->categoria = isset($registro->categoria) ? $registro->categoria : null;
            $this->marca_propia = isset($registro->marca_propia) ? $registro->marca_propia : null;
            $this->tiprot = isset($registro->tiprot) ? $registro->tiprot : null;
            $this->den_conv_sub_emb = isset($registro->den_conv_sub_emb) ? $registro->den_conv_sub_emb : null;
            $this->uni_sub_emb = isset($registro->uni_sub_emb) ? $registro->uni_sub_emb : null;
            $this->uni_embalaje = isset($registro->uni_embalaje) ? $registro->uni_embalaje : null;
            $this->cosprom = isset($registro->cosprom) ? $registro->cosprom : null;
        }
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getDespro()
    {
        return $this->despro;
    }

    public function setDespro($despro)
    {
        $this->despro = $despro;
    }

    public function getCodlin()
    {
        return $this->codlin;
    }

    public function setCodlin($codlin)
    {
        $this->codlin = $codlin;
    }

    public function getCodfam()
    {
        return $this->codfam;
    }

    public function setCodfam($codfam)
    {
        $this->codfam = $codfam;
    }

    public function getCodmar()
    {
        return $this->codmar;
    }

    public function setCodmar($codmar)
    {
        $this->codmar = $codmar;
    }

    public function getCodmod()
    {
        return $this->codmod;
    }

    public function setCodmod($codmod)
    {
        $this->codmod = $codmod;
    }

    public function getCodbar()
    {
        return $this->codbar;
    }

    public function setCodbar($codbar)
    {
        $this->codbar = $codbar;
    }

    public function getCoduni()
    {
        return $this->coduni;
    }

    public function setCoduni($coduni)
    {
        $this->coduni = $coduni;
    }

    public function getPesopr()
    {
        return $this->pesopr;
    }

    public function setPesopr($pesopr)
    {
        $this->pesopr = $pesopr;
    }

    public function getEstdes()
    {
        return $this->estdes;
    }

    public function setEstdes($estdes)
    {
        $this->estdes = $estdes;
    }

    public function getStkfsc()
    {
        return $this->stkfsc;
    }

    public function setStkfsc($stkfsc)
    {
        $this->stkfsc = $stkfsc;
    }

    public function getStkcom()
    {
        return $this->stkcom;
    }

    public function setStkcom($stkcom)
    {
        $this->stkcom = $stkcom;
    }

    public function getStkpen()
    {
        return $this->stkpen;
    }

    public function setStkpen($stkpen)
    {
        $this->stkpen = $stkpen;
    }

    public function getMinimo()
    {
        return $this->minimo;
    }

    public function setMinimo($minimo)
    {
        $this->minimo = $minimo;
    }

    public function getMaximo()
    {
        return $this->maximo;
    }

    public function setMaximo($maximo)
    {
        $this->maximo = $maximo;
    }

    public function getUltcos()
    {
        return $this->ultcos;
    }

    public function setUltcos($ultcos)
    {
        $this->ultcos = $ultcos;
    }

    public function getIndsbr()
    {
        return $this->indsbr;
    }

    public function setIndsbr($indsbr)
    {
        $this->indsbr = $indsbr;
    }

    public function getCodbdg()
    {
        return $this->codbdg;
    }

    public function setCodbdg($codbdg)
    {
        $this->codbdg = $codbdg;
    }

    public function getUnmivt()
    {
        return $this->unmivt;
    }

    public function setUnmivt($unmivt)
    {
        $this->unmivt = $unmivt;
    }

    public function getPedsto()
    {
        return $this->pedsto;
    }

    public function setPedsto($pedsto)
    {
        $this->pedsto = $pedsto;
    }

    public function getMoneda()
    {
        return $this->moneda;
    }

    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    public function getColate()
    {
        return $this->colate;
    }

    public function setColate($colate)
    {
        $this->colate = $colate;
    }

    public function getTipalm()
    {
        return $this->tipalm;
    }

    public function setTipalm($tipalm)
    {
        $this->tipalm = $tipalm;
    }

    public function getEstpro()
    {
        return $this->estpro;
    }

    public function setEstpro($estpro)
    {
        $this->estpro = $estpro;
    }

    public function getReembj()
    {
        return $this->reembj;
    }

    public function setReembj($reembj)
    {
        $this->reembj = $reembj;
    }

    public function getUltinv()
    {
        return $this->ultinv;
    }

    public function setUltinv($ultinv)
    {
        $this->ultinv = $ultinv;
    }

    public function getFecinv()
    {
        return $this->fecinv;
    }

    public function setFecinv($fecinv)
    {
        $this->fecinv = $fecinv;
    }

    public function getPagcat()
    {
        return $this->pagcat;
    }

    public function setPagcat($pagcat)
    {
        $this->pagcat = $pagcat;
    }

    public function getNumfto()
    {
        return $this->numfto;
    }

    public function setNumfto($numfto)
    {
        $this->numfto = $numfto;
    }

    public function getUltlib()
    {
        return $this->ultlib;
    }

    public function setUltlib($ultlib)
    {
        $this->ultlib = $ultlib;
    }

    public function getNumser()
    {
        return $this->numser;
    }

    public function setNumser($numser)
    {
        $this->numser = $numser;
    }

    public function getMerbod()
    {
        return $this->merbod;
    }

    public function setMerbod($merbod)
    {
        $this->merbod = $merbod;
    }

    public function getMercli()
    {
        return $this->mercli;
    }

    public function setMercli($mercli)
    {
        $this->mercli = $mercli;
    }

    public function getFecing()
    {
        return $this->fecing;
    }

    public function setFecing($fecing)
    {
        $this->fecing = $fecing;
    }

    public function getAsegur()
    {
        return $this->asegur;
    }

    public function setAsegur($asegur)
    {
        $this->asegur = $asegur;
    }

    public function getStkcri()
    {
        return $this->stkcri;
    }

    public function setStkcri($stkcri)
    {
        $this->stkcri = $stkcri;
    }

    public function getStkint()
    {
        return $this->stkint;
    }

    public function setStkint($stkint)
    {
        $this->stkint = $stkint;
    }

    public function getProneo()
    {
        return $this->proneo;
    }

    public function setProneo($proneo)
    {
        $this->proneo = $proneo;
    }

    public function getTradis()
    {
        return $this->tradis;
    }

    public function setTradis($tradis)
    {
        $this->tradis = $tradis;
    }

    public function getResfis()
    {
        return $this->resfis;
    }

    public function setResfis($resfis)
    {
        $this->resfis = $resfis;
    }

    public function getCodigoUni()
    {
        return $this->codigo_uni;
    }

    public function setCodigoUni($codigo_uni)
    {
        $this->codigo_uni = $codigo_uni;
    }

    public function getDespro2()
    {
        return $this->despro2;
    }

    public function setDespro2($despro2)
    {
        $this->despro2 = $despro2;
    }

    public function getOferta()
    {
        return $this->oferta;
    }

    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
    }

    public function getExistAnt()
    {
        return $this->exist_ant;
    }

    public function setExistAnt($exist_ant)
    {
        $this->exist_ant = $exist_ant;
    }

    public function getExistTraOc()
    {
        return $this->exist_tra_oc;
    }

    public function setExistTraOc($exist_tra_oc)
    {
        $this->exist_tra_oc = $exist_tra_oc;
    }

    public function getCosproAntDm()
    {
        return $this->cospro_ant_dm;
    }

    public function setCosproAntDm($cospro_ant_dm)
    {
        $this->cospro_ant_dm = $cospro_ant_dm;
    }

    public function getExistTraMo()
    {
        return $this->exist_tra_mo;
    }

    public function setExistTraMo($exist_tra_mo)
    {
        $this->exist_tra_mo = $exist_tra_mo;
    }

    public function getMaxcd()
    {
        return $this->maxcd;
    }

    public function setMaxcd($maxcd)
    {
        $this->maxcd = $maxcd;
    }

    public function getMincd()
    {
        return $this->mincd;
    }

    public function setMincd($mincd)
    {
        $this->mincd = $mincd;
    }

    public function getCricd()
    {
        return $this->cricd;
    }

    public function setCricd($cricd)
    {
        $this->cricd = $cricd;
    }

    public function getIntcd()
    {
        return $this->intcd;
    }

    public function setIntcd($intcd)
    {
        $this->intcd = $intcd;
    }

    public function getStkini()
    {
        return $this->stkini;
    }

    public function setStkini($stkini)
    {
        $this->stkini = $stkini;
    }

    public function getFecini()
    {
        return $this->fecini;
    }

    public function setFecini($fecini)
    {
        $this->fecini = $fecini;
    }

    public function getAudita()
    {
        return $this->audita;
    }

    public function setAudita($audita)
    {
        $this->audita = $audita;
    }

    public function getStkarm()
    {
        return $this->stkarm;
    }

    public function setStkarm($stkarm)
    {
        $this->stkarm = $stkarm;
    }

    public function getRutprv()
    {
        return $this->rutprv;
    }

    public function setRutprv($rutprv)
    {
        $this->rutprv = $rutprv;
    }

    public function getMerbod2()
    {
        return $this->merbod2;
    }

    public function setMerbod2($merbod2)
    {
        $this->merbod2 = $merbod2;
    }

    public function getStkDimeiggs()
    {
        return $this->stk_dimeiggs;
    }

    public function setStkDimeiggs($stk_dimeiggs)
    {
        $this->stk_dimeiggs = $stk_dimeiggs;
    }

    public function getMarcaDimeiggs()
    {
        return $this->marca_dimeiggs;
    }

    public function setMarcaDimeiggs($marca_dimeiggs)
    {
        $this->marca_dimeiggs = $marca_dimeiggs;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getConVencimiento()
    {
        return $this->con_vencimiento;
    }

    public function setConVencimiento($con_vencimiento)
    {
        $this->con_vencimiento = $con_vencimiento;
    }

    public function getTipoprod()
    {
        return $this->tipoprod;
    }

    public function setTipoprod($tipoprod)
    {
        $this->tipoprod = $tipoprod;
    }

    public function getImportado()
    {
        return $this->importado;
    }

    public function setImportado($importado)
    {
        $this->importado = $importado;
    }

    public function getImpuesto()
    {
        return $this->impuesto;
    }

    public function getCodneg()
    {
        return $this->codneg;
    }

    public function setCodneg($codneg)
    {
        $this->codneg = $codneg;
    }

    public function getXdelta()
    {
        return $this->xdelta;
    }

    public function setXdelta($xdelta)
    {
        $this->xdelta = $xdelta;
    }

    public function getCodsap()
    {
        return $this->codsap;
    }

    public function setCodsap($codsap)
    {
        $this->codsap = $codsap;
    }

    public function getEmbalaje()
    {
        return $this->embalaje;
    }

    public function setEmbalaje($embalaje)
    {
        $this->embalaje = $embalaje;
    }

    public function getSubembalaje()
    {
        return $this->subembalaje;
    }

    public function setSubembalaje($subembalaje)
    {
        $this->subembalaje = $subembalaje;
    }

    public function getUltimaRecepcion()
    {
        return $this->ultima_recepcion;
    }

    public function setUltimaRecepcion($ultima_recepcion)
    {
        $this->ultima_recepcion = $ultima_recepcion;
    }

    public function getUltimaNota()
    {
        return $this->ultima_nota;
    }

    public function setUltimaNota($ultima_nota)
    {
        $this->ultima_nota = $ultima_nota;
    }

    public function getUltimaGmm()
    {
        return $this->ultima_gmm;
    }

    public function setUltimaGmm($ultima_gmm)
    {
        $this->ultima_gmm = $ultima_gmm;
    }

    public function getAfecto()
    {
        return $this->afecto;
    }

    public function setAfecto($afecto)
    {
        $this->afecto = $afecto;
    }

    public function getIgv()
    {
        return $this->igv;
    }

    public function setIgv($igv)
    {
        $this->igv = $igv;
    }

    public function getVidaUtil()
    {
        return $this->vida_util;
    }

    public function setVidaUtil($vida_util)
    {
        $this->vida_util = $vida_util;
    }

    public function getDesprocorta()
    {
        return $this->desprocorta;
    }

    public function setDesprocorta($desprocorta)
    {
        $this->desprocorta = $desprocorta;
    }

    public function getDesprolarga()
    {
        return $this->desprolarga;
    }

    public function setDesprolarga($desprolarga)
    {
        $this->desprolarga = $desprolarga;
    }

    public function getCodjer()
    {
        return $this->codjer;
    }

    public function setCodjer($codjer)
    {
        $this->codjer = $codjer;
    }

    public function getCodLinea()
    {
        return $this->cod_linea;
    }

    public function setCodLinea($cod_linea)
    {
        $this->cod_linea = $cod_linea;
    }

    public function getDeslin()
    {
        return $this->deslin;
    }

    public function setDeslin($deslin)
    {
        $this->deslin = $deslin;
    }

    public function getCodsec()
    {
        return $this->codsec;
    }

    public function setCodsec($codsec)
    {
        $this->codsec = $codsec;
    }

    public function getDessec()
    {
        return $this->dessec;
    }

    public function setDessec($dessec)
    {
        $this->dessec = $dessec;
    }

    public function getCodrub()
    {
        return $this->codrub;
    }

    public function setCodrub($codrub)
    {
        $this->codrub = $codrub;
    }

    public function getDesrub()
    {
        return $this->desrub;
    }

    public function setDesrub($desrub)
    {
        $this->desrub = $desrub;
    }

    public function getCodsubgru()
    {
        return $this->codsubgru;
    }

    public function setCodsubgru($codsubgru)
    {
        $this->codsubgru = $codsubgru;
    }

    public function getDessubgru()
    {
        return $this->dessubgru;
    }

    public function setDessubgru($dessubgru)
    {
        $this->dessubgru = $dessubgru;
    }

    public function getCodgru()
    {
        return $this->codgru;
    }

    public function setCodgru($codgru)
    {
        $this->codgru = $codgru;
    }

    public function getDesgru()
    {
        return $this->desgru;
    }

    public function setDesgru($desgru)
    {
        $this->desgru = $desgru;
    }

    public function getDesmar()
    {
        return $this->desmar;
    }

    public function setDesmar($desmar)
    {
        $this->desmar = $desmar;
    }

    public function getPesoNeto()
    {
        return $this->peso_neto;
    }

    public function setPesoNeto($peso_neto)
    {
        $this->peso_neto = $peso_neto;
    }

    public function getUniPeso()
    {
        return $this->uni_peso;
    }

    public function setUniPeso($uni_peso)
    {
        $this->uni_peso = $uni_peso;
    }

    public function getUniVolumen()
    {
        return $this->uni_volumen;
    }

    public function setUniVolumen($uni_volumen)
    {
        $this->uni_volumen = $uni_volumen;
    }

    public function getDenConvEmb()
    {
        return $this->den_conv_emb;
    }

    public function setDenConvEmb($den_conv_emb)
    {
        $this->den_conv_emb = $den_conv_emb;
    }

    public function getPesoNetoEmb()
    {
        return $this->peso_neto_emb;
    }

    public function setPesoNetoEmb($peso_neto_emb)
    {
        $this->peso_neto_emb = $peso_neto_emb;
    }

    public function getPeso()
    {
        return $this->peso;
    }

    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    public function getVolumen()
    {
        return $this->volumen;
    }

    public function setVolumen($volumen)
    {
        $this->volumen = $volumen;
    }

    public function getPesoEmb()
    {
        return $this->peso_emb;
    }

    public function setPesoEmb($peso_emb)
    {
        $this->peso_emb = $peso_emb;
    }

    public function getPesoNetoSubEmb()
    {
        return $this->peso_neto_sub_emb;
    }

    public function setPesoNetoSubEmb($peso_neto_sub_emb)
    {
        $this->peso_neto_sub_emb = $peso_neto_sub_emb;
    }

    public function getPesoBrutoSubEmb()
    {
        return $this->peso_bruto_sub_emb;
    }

    public function setPesoBrutoSubEmb($peso_bruto_sub_emb)
    {
        $this->peso_bruto_sub_emb = $peso_bruto_sub_emb;
    }

    public function getVolumenEmb()
    {
        return $this->volumen_emb;
    }

    public function setVolumenEmb($volumen_emb)
    {
        $this->volumen_emb = $volumen_emb;
    }

    public function getUniPesoEmb()
    {
        return $this->uni_peso_emb;
    }

    public function setUniPesoEmb($uni_peso_emb)
    {
        $this->uni_peso_emb = $uni_peso_emb;
    }

    public function getUniVolumenEmb()
    {
        return $this->uni_volumen_emb;
    }

    public function setUniVolumenEmb($uni_volumen_emb)
    {
        $this->uni_volumen_emb = $uni_volumen_emb;
    }

    public function getVolumenSubEmb()
    {
        return $this->volumen_sub_emb;
    }

    public function setVolumenSubEmb($volumen_sub_emb)
    {
        $this->volumen_sub_emb = $volumen_sub_emb;
    }

    public function getUniPesoSubEmb()
    {
        return $this->uni_peso_sub_emb;
    }

    public function setUniPesoSubEmb($uni_peso_sub_emb)
    {
        $this->uni_peso_sub_emb = $uni_peso_sub_emb;
    }

    public function getUniVolumenSubEmb()
    {
        return $this->uni_volumen_sub_emb;
    }

    public function setUniVolumenSubEmb($uni_volumen_sub_emb)
    {
        $this->uni_volumen_sub_emb = $uni_volumen_sub_emb;
    }

    public function getPais()
    {
        return $this->pais;
    }

    public function setPais($pais)
    {
        $this->pais = $pais;
    }

    public function getNomprv()
    {
        return $this->nomprv;
    }

    public function setNomprv($nomprv)
    {
        $this->nomprv = $nomprv;
    }

    public function getTipoprodSap()
    {
        return $this->tipoprod_sap;
    }

    public function setTipoprodSap($tipoprod_sap)
    {
        $this->tipoprod_sap = $tipoprod_sap;
    }

    public function getCodlinSap()
    {
        return $this->codlin_sap;
    }

    public function setCodlinSap($codlin_sap)
    {
        $this->codlin_sap = $codlin_sap;
    }

    public function getCodmarSap()
    {
        return $this->codmar_sap;
    }

    public function setCodmarSap($codmar_sap)
    {
        $this->codmar_sap = $codmar_sap;
    }

    public function getCoduniSap()
    {
        return $this->coduni_sap;
    }

    public function setCoduniSap($coduni_sap)
    {
        $this->coduni_sap = $coduni_sap;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getMarcaPropia()
    {
        return $this->marca_propia;
    }

    public function setMarcaPropia($marca_propia)
    {
        $this->marca_propia = $marca_propia;
    }

    public function getTiprot()
    {
        return $this->tiprot;
    }

    public function setTiprot($tiprot)
    {
        $this->tiprot = $tiprot;
    }

    public function getDenConvSubEmb()
    {
        return $this->den_conv_sub_emb;
    }

    public function setDenConvSubEmb($den_conv_sub_emb)
    {
        $this->den_conv_sub_emb = $den_conv_sub_emb;
    }

    public function getUniSubEmb()
    {
        return $this->uni_sub_emb;
    }

    public function setUniSubEmb($uni_sub_emb)
    {
        $this->uni_sub_emb = $uni_sub_emb;
    }

    public function getUniEmbalaje()
    {
        return $this->uni_embalaje;
    }

    public function setUniEmbalaje($uni_embalaje)
    {
        $this->uni_embalaje = $uni_embalaje;
    }

    public function jsonSerialize()
    {
        return [
            'codpro' => $this->getCodpro(),
            'despro' => $this->getDespro(),
            'codlin' => $this->getCodlin(),
            'codfam' => $this->getCodfam(),
            'codmar' => $this->getCodmar(),
            'codmod' => $this->getCodmod(),
            'codbar' => $this->getCodbar(),
            'coduni' => $this->getCoduni(),
            'pesopr' => $this->getPesopr(),
            'estdes' => $this->getEstdes(),
            'stkfsc' => $this->getStkfsc(),
            'stkcom' => $this->getStkcom(),
            'stkpen' => $this->getStkpen(),
            'minimo' => $this->getMinimo(),
            'maximo' => $this->getMaximo(),
            'ultcos' => $this->getUltcos(),
            'indsbr' => $this->getIndsbr(),
            'codbdg' => $this->getCodbdg(),
            'unmivt' => $this->getUnmivt(),
            'pedsto' => $this->getPedsto(),
            'moneda' => $this->getMoneda(),
            'colate' => $this->getColate(),
            'tipalm' => $this->getTipalm(),
            'estpro' => $this->getEstpro(),
            'reembj' => $this->getReembj(),
            'ultinv' => $this->getUltinv(),
            'fecinv' => $this->getFecinv(),
            'pagcat' => $this->getPagcat(),
            'numfto' => $this->getNumfto(),
            'ultlib' => $this->getUltlib(),
            'numser' => $this->getNumser(),
            'merbod' => $this->getMerbod(),
            'mercli' => $this->getMercli(),
            'fecing' => $this->getFecing(),
            'asegur' => $this->getAsegur(),
            'stkcri' => $this->getStkcri(),
            'stkint' => $this->getStkint(),
            'proneo' => $this->getProneo(),
            'tradis' => $this->getTradis(),
            'resfis' => $this->getResfis(),
            'codigo_uni' => $this->getCodigoUni(),
            'despro2' => $this->getDespro2(),
            'oferta' => $this->getOferta(),
            'exist_ant' => $this->getExistAnt(),
            'exist_tra_oc' => $this->getExistTraOc(),
            'cospro_ant_dm' => $this->getCosproAntDm(),
            'exist_tra_mo' => $this->getExistTraMo(),
            'maxcd' => $this->getMaxcd(),
            'mincd' => $this->getMincd(),
            'cricd' => $this->getCricd(),
            'intcd' => $this->getIntcd(),
            'stkini' => $this->getStkini(),
            'fecini' => $this->getFecini(),
            'audita' => $this->getAudita(),
            'stkarm' => $this->getStkarm(),
            'rutprv' => $this->getRutprv(),
            'merbod2' => $this->getMerbod2(),
            'stk_dimeiggs' => $this->getStkDimeiggs(),
            'marca_dimeiggs' => $this->getMarcaDimeiggs(),
            'cosprom' => $this->getCosprom(),
            'con_vencimiento' => $this->getConVencimiento(),
            'tipoprod' => $this->getTipoprod(),
            'importado' => $this->getImportado(),
            'impuesto' => $this->getImpuesto(),
            'codneg' => $this->getCodneg(),
            'xdelta' => $this->getXdelta(),

            // new values
            'codsap' => $this->getCodsap(),


            'embalaje' => $this->getEmbalaje(),
            'sub_embalaje' => $this->getSubembalaje(),
            'ultimarecepcion' => $this->getUltimaRecepcion(),
            'ultimanota' => $this->getUltimaNota(),
            'ultimagmm' => $this->getUltimaGmm(),
            'afecto' => $this->getAfecto(),
            'igv' => $this->getIgv(),
            'vidautil' => $this->getVidaUtil(),
            'desprocorta' => $this->getDesprocorta(),
            'desprolarga' => $this->getDesprolarga(),
            'codjer' => $this->getCodjer(),
            'codlinea' => $this->getCodLinea(),
            'deslin' => $this->getDeslin(),
            'codsec' => $this->getCodsec(),
            'dessec' => $this->getDessec(),
            'codrub' => $this->getCodrub(),
            'desrub' => $this->getDesrub(),
            'codsubgru' => $this->getCodsubgru(),
            'dessubgru' => $this->getDessubgru(),
            'codgru' => $this->getCodgru(),
            'desgru' => $this->getDesgru(),
            'desmar' => $this->getDesmar(),
            'pesoneto' => $this->getPesoNeto(),
            'unipeso' => $this->getUniPeso(),
            'univolumen' => $this->getUniVolumen(),
            'denconvemb' => $this->getDenConvEmb(),
            'pesonetoemb' => $this->getPesoNetoEmb(),
            'peso' => $this->getPeso(),
            'volumen' => $this->getVolumen(),
            'pesoemb' => $this->getPesoEmb(),
            'pesonetosubemb' => $this->getPesoNetoSubEmb(),
            'pesobrutosubemb' => $this->getPesoBrutoSubEmb(),
            'volumenemb' => $this->getVolumenEmb(),
            'unipesoemb' => $this->getUniPesoEmb(),
            'univolumenemb' => $this->getUniVolumenEmb(),
            'volumensubemb' => $this->getVolumenSubEmb(),
            'unipesosubemb' => $this->getUniPesoSubEmb(),
            'univolumensubemb' => $this->getUniVolumenSubEmb(),
            'pais' => $this->getPais(),
            'nomprv' => $this->getNomprv(),
            'tipoprodsap' => $this->getTipoprodSap(),
            'codlinsap' => $this->getCodlinSap(),
            'codmarsap' => $this->getCodmarSap(),
            'codunisap' => $this->getCoduniSap(),
            'categoria' => $this->getCategoria(),
            'marcapropia' => $this->getMarcaPropia(),
            'tiprot' => $this->getTiprot(),
            'denconvsubemb' => $this->getDenConvSubEmb(),
            'unisubemb' => $this->getUniSubEmb(),
            'uniembalaje' => $this->getUniEmbalaje(),
        ];
    }
}