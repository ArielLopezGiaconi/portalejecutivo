<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 11:54
 */

namespace App\Entities\Oracle\Holding;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use DB;

class MaProduct
{
    public $codpro;
    public $despro;
    public $tipo_prod;
    public $desprocorta;
    public $desprolarga;
    public $codjer;
    public $codlin;
    public $deslin;
    public $codsec;
    public $dessec;
    public $codrub;
    public $desrub;
    public $codsubgru;
    public $dessubgru;
    public $codgru;
    public $desgru;
    public $codmar;
    public $desmar;
    public $peso;
    public $peso_neto;
    public $uni_peso;
    public $volumen;
    public $uni_volumen;
    public $coduni;
    public $uni_embalaje;
    public $den_conv_emb;
    public $embalaje;
    public $peso_neto_emb;
    public $peso_emb;
    public $volumen_emb;
    public $uni_peso_emb;
    public $uni_volumen_emb;
    public $volumen_sub_emb;
    public $uni_peso_sub_emb;
    public $uni_volumen_sub_emb;
    public $uni_sub_emb;
    public $den_conv_sub_emb;
    public $sub_embalaje;
    public $peso_neto_sub_emb;
    public $peso_bruto_sub_emb;
    public $codsap;
    public $codigo_uni;
    public $tipoprod;
    public $indsbr;
    public $tiprot;
    public $marca_propia;
    public $importado;
    public $categoria;
    public $codneg;
    public $pais;
    public $cod_impuesto;
    public $impuesto;
    public $vida_util;
    public $tipoprod_sap;
    public $codlin_sap;
    public $codmar_sap;
    public $coduni_sap;
    public $cosprom;
    public $fecing;
    public $afecto;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->codpro = isset($registro->codpro) ? $registro->codpro : null;
            $this->despro = isset($registro->despro) ? $registro->despro : null;
            $this->tipo_prod = isset($registro->tipo_prod) ? $registro->tipo_prod : null;
            $this->desprocorta = isset($registro->desprocorta) ? $registro->desprocorta : null;
            $this->desprolarga = isset($registro->desprolarga) ? $registro->desprolarga : null;
            $this->codjer = isset($registro->codjer) ? $registro->codjer : null;
            $this->codlin = isset($registro->codlin) ? $registro->codlin : null;
            $this->deslin = isset($registro->deslin) ? $registro->deslin : null;
            $this->codsec = isset($registro->codsec) ? $registro->codsec : null;
            $this->dessec = isset($registro->dessec) ? $registro->dessec : null;
            $this->codrub = isset($registro->codrub) ? $registro->codrub : null;
            $this->desrub = isset($registro->desrub) ? $registro->desrub : null;
            $this->codsubgru = isset($registro->codsubgru) ? $registro->codsubgru : null;
            $this->dessubgru = isset($registro->dessubgru) ? $registro->dessubgru : null;
            $this->codgru = isset($registro->codgru) ? $registro->codgru : null;
            $this->desgru = isset($registro->desgru) ? $registro->desgru : null;
            $this->codmar = isset($registro->codmar) ? $registro->codmar : null;
            $this->desmar = isset($registro->desmar) ? $registro->desmar : null;
            $this->peso = isset($registro->peso) ? $registro->peso : null;
            $this->peso_neto = isset($registro->peso_neto) ? $registro->peso_neto : null;
            $this->uni_peso = isset($registro->uni_peso) ? $registro->uni_peso : null;
            $this->volumen = isset($registro->volumen) ? $registro->volumen : null;
            $this->uni_volumen = isset($registro->uni_volumen) ? $registro->uni_volumen : null;
            $this->coduni = isset($registro->coduni) ? $registro->coduni : null;
            $this->uni_embalaje = isset($registro->uni_embalaje) ? $registro->uni_embalaje : null;
            $this->den_conv_emb = isset($registro->den_conv_emb) ? $registro->den_conv_emb : null;
            $this->embalaje = isset($registro->embalaje) ? $registro->embalaje : null;
            $this->peso_neto_emb = isset($registro->peso_neto_emb) ? $registro->peso_neto_emb : null;
            $this->peso_emb = isset($registro->peso_emb) ? $registro->peso_emb : null;
            $this->volumen_emb = isset($registro->volumen_emb) ? $registro->volumen_emb : null;
            $this->uni_peso_emb = isset($registro->uni_peso_emb) ? $registro->uni_peso_emb : null;
            $this->uni_volumen_emb = isset($registro->uni_volumen_emb) ? $registro->uni_volumen_emb : null;
            $this->volumen_sub_emb = isset($registro->volumen_sub_emb) ? $registro->volumen_sub_emb : null;
            $this->uni_peso_sub_emb = isset($registro->uni_peso_sub_emb) ? $registro->uni_peso_sub_emb : null;
            $this->uni_volumen_sub_emb = isset($registro->uni_volumen_sub_emb) ? $registro->uni_volumen_sub_emb : null;
            $this->uni_sub_emb = isset($registro->uni_sub_emb) ? $registro->uni_sub_emb : null;
            $this->den_conv_sub_emb = isset($registro->den_conv_sub_emb) ? $registro->den_conv_sub_emb : null;
            $this->sub_embalaje = isset($registro->sub_embalaje) ? $registro->sub_embalaje : null;
            $this->peso_neto_sub_emb = isset($registro->peso_neto_sub_emb) ? $registro->peso_neto_sub_emb : null;
            $this->peso_bruto_sub_emb = isset($registro->peso_bruto_sub_emb) ? $registro->peso_bruto_sub_emb : null;
            $this->codsap = isset($registro->codsap) ? $registro->codsap : null;
            $this->codigo_uni = isset($registro->codigo_uni) ? $registro->codigo_uni : null;
            $this->tipoprod = isset($registro->tipoprod) ? $registro->tipoprod : null;
            $this->indsbr = isset($registro->indsbr) ? $registro->indsbr : null;
            $this->tiprot = isset($registro->tiprot) ? $registro->tiprot : null;
            $this->marca_propia = isset($registro->marca_propia) ? $registro->marca_propia : null;
            $this->importado = isset($registro->importado) ? $registro->importado : null;
            $this->categoria = isset($registro->categoria) ? $registro->categoria : null;
            $this->codneg = isset($registro->codneg) ? $registro->codneg : null;
            $this->pais = isset($registro->pais) ? $registro->pais : null;
            $this->cod_impuesto = isset($registro->cod_impuesto) ? $registro->cod_impuesto : null;
            $this->impuesto = isset($registro->impuesto) ? $registro->impuesto : null;
            $this->vida_util = isset($registro->vida_util) ? $registro->vida_util : null;
            $this->tipoprod_sap = isset($registro->tipoprod_sap) ? $registro->tipoprod_sap : null;
            $this->codlin_sap = isset($registro->codlin_sap) ? $registro->codlin_sap : null;
            $this->codmar_sap = isset($registro->codmar_sap) ? $registro->codmar_sap : null;
            $this->coduni_sap = isset($registro->coduni_sap) ? $registro->coduni_sap : null;
            $this->cosprom = isset($registro->cosprom) ? $registro->cosprom : null;
            $this->fecing = isset($registro->fecing) ? $registro->fecing : null;
            $this->afecto = isset($registro->afecto) ? $registro->afecto : null;
        }
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getDespro()
    {
        return $this->despro;
    }

    public function setDespro($despro)
    {
        $this->despro = $despro;
    }

    public function getTipoProd()
    {
        return $this->tipo_prod;
    }

    public function setTipoProd($tipo_prod)
    {
        $this->tipo_prod = $tipo_prod;
    }

    public function getDesprocorta()
    {
        return $this->desprocorta;
    }

    public function setDesprocorta($desprocorta)
    {
        $this->desprocorta = $desprocorta;
    }

    public function getDesprolarga()
    {
        return $this->desprolarga;
    }

    public function setDesprolarga($desprolarga)
    {
        $this->desprolarga = $desprolarga;
    }

    public function getCodjer()
    {
        return $this->codjer;
    }

    public function setCodjer($codjer)
    {
        $this->codjer = $codjer;
    }

    public function getCodlin()
    {
        return $this->codlin;
    }

    public function setCodlin($codlin)
    {
        $this->codlin = $codlin;
    }

    public function getDeslin()
    {
        return $this->deslin;
    }

    public function setDeslin($deslin)
    {
        $this->deslin = $deslin;
    }

    public function getCodsec()
    {
        return $this->codsec;
    }

    public function setCodsec($codsec)
    {
        $this->codsec = $codsec;
    }

    public function getDessec()
    {
        return $this->dessec;
    }

    public function setDessec($dessec)
    {
        $this->dessec = $dessec;
    }

    public function getCodrub()
    {
        return $this->codrub;
    }

    public function setCodrub($codrub)
    {
        $this->codrub = $codrub;
    }

    public function getDesrub()
    {
        return $this->desrub;
    }

    public function setDesrub($desrub)
    {
        $this->desrub = $desrub;
    }

    public function getCodsubgru()
    {
        return $this->codsubgru;
    }

    public function setCodsubgru($codsubgru)
    {
        $this->codsubgru = $codsubgru;
    }

    public function getDessubgru()
    {
        return $this->dessubgru;
    }

    public function setDessubgru($dessubgru)
    {
        $this->dessubgru = $dessubgru;
    }

    public function getCodgru()
    {
        return $this->codgru;
    }

    public function setCodgru($codgru)
    {
        $this->codgru = $codgru;
    }

    public function getDesgru()
    {
        return $this->desgru;
    }

    public function setDesgru($desgru)
    {
        $this->desgru = $desgru;
    }

    public function getCodmar()
    {
        return $this->codmar;
    }

    public function setCodmar($codmar)
    {
        $this->codmar = $codmar;
    }

    public function getDesmar()
    {
        return $this->desmar;
    }

    public function setDesmar($desmar)
    {
        $this->desmar = $desmar;
    }

    public function getPeso()
    {
        return $this->peso;
    }

    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    public function getPesoNeto()
    {
        return $this->peso_neto;
    }

    public function setPesoNeto($peso_neto)
    {
        $this->peso_neto = $peso_neto;
    }

    public function getUniPeso()
    {
        return $this->uni_peso;
    }

    public function setUniPeso($uni_peso)
    {
        $this->uni_peso = $uni_peso;
    }

    public function getVolumen()
    {
        return $this->volumen;
    }

    public function setVolumen($volumen)
    {
        $this->volumen = $volumen;
    }

    public function getUniVolumen()
    {
        return $this->uni_volumen;
    }

    public function setUniVolumen($uni_volumen)
    {
        $this->uni_volumen = $uni_volumen;
    }

    public function getCoduni()
    {
        return $this->coduni;
    }

    public function setCoduni($coduni)
    {
        $this->coduni = $coduni;
    }

    public function getUniEmbalaje()
    {
        return $this->uni_embalaje;
    }

    public function setUniEmbalaje($uni_embalaje)
    {
        $this->uni_embalaje = $uni_embalaje;
    }

    public function getDenConvEmb()
    {
        return $this->den_conv_emb;
    }

    public function setDenConvEmb($den_conv_emb)
    {
        $this->den_conv_emb = $den_conv_emb;
    }

    public function getEmbalaje()
    {
        return $this->embalaje;
    }

    public function setEmbalaje($embalaje)
    {
        $this->embalaje = $embalaje;
    }

    public function getPesoNetoEmb()
    {
        return $this->peso_neto_emb;
    }

    public function setPesoNetoEmb($peso_neto_emb)
    {
        $this->peso_neto_emb = $peso_neto_emb;
    }

    public function getPesoEmb()
    {
        return $this->peso_emb;
    }

    public function setPesoEmb($peso_emb)
    {
        $this->peso_emb = $peso_emb;
    }

    public function getVolumenEmb()
    {
        return $this->volumen_emb;
    }

    public function setVolumenEmb($volumen_emb)
    {
        $this->volumen_emb = $volumen_emb;
    }

    public function getUniPesoEmb()
    {
        return $this->uni_peso_emb;
    }

    public function setUniPesoEmb($uni_peso_emb)
    {
        $this->uni_peso_emb = $uni_peso_emb;
    }

    public function getUniVolumenEmb()
    {
        return $this->uni_volumen_emb;
    }

    public function setUniVolumenEmb($uni_volumen_emb)
    {
        $this->uni_volumen_emb = $uni_volumen_emb;
    }

    public function getVolumenSubEmb()
    {
        return $this->volumen_sub_emb;
    }

    public function setVolumenSubEmb($volumen_sub_emb)
    {
        $this->volumen_sub_emb = $volumen_sub_emb;
    }

    public function getUniPesoSubEmb()
    {
        return $this->uni_peso_sub_emb;
    }

    public function setUniPesoSubEmb($uni_peso_sub_emb)
    {
        $this->uni_peso_sub_emb = $uni_peso_sub_emb;
    }

    public function getUniVolumenSubEmb()
    {
        return $this->uni_volumen_sub_emb;
    }

    public function setUniVolumenSubEmb($uni_volumen_sub_emb)
    {
        $this->uni_volumen_sub_emb = $uni_volumen_sub_emb;
    }

    public function getUniSubEmb()
    {
        return $this->uni_sub_emb;
    }

    public function setUniSubEmb($uni_sub_emb)
    {
        $this->uni_sub_emb = $uni_sub_emb;
    }

    public function getDenConvSubEmb()
    {
        return $this->den_conv_sub_emb;
    }

    public function setDenConvSubEmb($den_conv_sub_emb)
    {
        $this->den_conv_sub_emb = $den_conv_sub_emb;
    }

    public function getSubEmbalaje()
    {
        return $this->sub_embalaje;
    }

    public function setSubEmbalaje($sub_embalaje)
    {
        $this->sub_embalaje = $sub_embalaje;
    }

    public function getPesoNetoSubEmb()
    {
        return $this->peso_neto_sub_emb;
    }

    public function setPesoNetoSubEmb($peso_neto_sub_emb)
    {
        $this->peso_neto_sub_emb = $peso_neto_sub_emb;
    }

    public function getPesoBrutoSubEmb()
    {
        return $this->peso_bruto_sub_emb;
    }

    public function setPesoBrutoSubEmb($peso_bruto_sub_emb)
    {
        $this->peso_bruto_sub_emb = $peso_bruto_sub_emb;
    }

    public function getCodsap()
    {
        return $this->codsap;
    }

    public function setCodsap($codsap)
    {
        $this->codsap = $codsap;
    }

    public function getCodigoUni()
    {
        return $this->codigo_uni;
    }

    public function setCodigoUni($codigo_uni)
    {
        $this->codigo_uni = $codigo_uni;
    }


    public function getIndsbr()
    {
        return $this->indsbr;
    }

    public function setIndsbr($indsbr)
    {
        $this->indsbr = $indsbr;
    }

    public function getTiprot()
    {
        return $this->tiprot;
    }

    public function setTiprot($tiprot)
    {
        $this->tiprot = $tiprot;
    }

    public function getMarcaPropia()
    {
        return $this->marca_propia;
    }

    public function setMarcaPropia($marca_propia)
    {
        $this->marca_propia = $marca_propia;
    }

    public function getImportado()
    {
        return $this->importado;
    }

    public function setImportado($importado)
    {
        $this->importado = $importado;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getCodneg()
    {
        return $this->codneg;
    }

    public function setCodneg($codneg)
    {
        $this->codneg = $codneg;
    }

    public function getPais()
    {
        return $this->pais;
    }

    public function setPais($pais)
    {
        $this->pais = $pais;
    }

    public function getCodImpuesto()
    {
        return $this->cod_impuesto;
    }

    public function setCodImpuesto($cod_impuesto)
    {
        $this->cod_impuesto = $cod_impuesto;
    }

    public function getImpuesto()
    {
        return $this->impuesto;
    }

    public function setImpuesto($impuesto)
    {
        $this->impuesto = $impuesto;
    }

    public function getVidaUtil()
    {
        return $this->vida_util;
    }

    public function setVidaUtil($vida_util)
    {
        $this->vida_util = $vida_util;
    }

    public function getTipoprodSap()
    {
        return $this->tipoprod_sap;
    }

    public function setTipoprodSap($tipoprod_sap)
    {
        $this->tipoprod_sap = $tipoprod_sap;
    }

    public function getCodlinSap()
    {
        return $this->codlin_sap;
    }

    public function setCodlinSap($codlin_sap)
    {
        $this->codlin_sap = $codlin_sap;
    }

    public function getCodmarSap()
    {
        return $this->codmar_sap;
    }

    public function setCodmarSap($codmar_sap)
    {
        $this->codmar_sap = $codmar_sap;
    }

    public function getCoduniSap()
    {
        return $this->coduni_sap;
    }

    public function setCoduniSap($coduni_sap)
    {
        $this->coduni_sap = $coduni_sap;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getFecing()
    {
        return $this->fecing;
    }

    public function setFecing($fecing)
    {
        $this->fecing = $fecing;
    }

    public function getAfecto()
    {
        return $this->afecto;
    }

    public function setAfecto($afecto)
    {
        $this->afecto = $afecto;
    }
}