<?php

namespace App\PDO\Oracle\DMVentas;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

class DmVentasPDO extends Model
{
    public static function saveAnotacion($inputs, $codusu, $rutcli)
    {
        $gestion = $inputs['anotacion'];
        if (empty($inputs['observacion'])) {
            $observacion = "Sin Observacion por usuario";
        } else {
            $observacion = $inputs['observacion'];
        }
        if (!array_key_exists('prox_accion', $inputs)) {
            $proxima = "";
        } else {
            $proxima = $inputs['prox_accion'];
        }
        if (!array_key_exists('fecha', $inputs)) {
            $pendiente = "0000-00-00";
        } else {
            $pendiente = $inputs['fecha'];
        }
        if (!array_key_exists('contactos', $inputs)) {
            $contactable = "";
        } else {
            $contactable = $inputs['contactos'];
        }
        if (!array_key_exists('numcot', $inputs)) {
            $cotizacion = "0";
        } else {
            $cotizacion = $inputs['numcot'];
        }
        if (!array_key_exists('motivo_coti', $inputs)) {
            $perdi = "";
        } else {
            $perdi = $inputs['motivo_coti'];
        }
        if (!array_key_exists('perdi_contra', $inputs)) {
            $perdi_con = "";
        } else {
            $perdi_con = $inputs['perdi_contra'];
        }
        if (!array_key_exists('rut_rel', $inputs)) {
            $rutrel = "";
        } else {
            $rutrel = $inputs['rut_rel'];
        }
        if (!array_key_exists('linea', $inputs)) {
            $linea_op = "";
        } else {
            $linea_op = $inputs['linea'];
        }
        if (!array_key_exists('monto', $inputs)) {
            $monto_linea = "";
        } else {
            $monto_linea = $inputs['monto'];
        }
        if ($pendiente == '0000-00-00') {
            $sql = "insert into tm_cliente_hst
            (
                fechas,horass,userid,rutcli,observ,
                prxacc,accion,contactable,cotizacion,perdi,perdi_con,rut_rel,linea,monto_linea
            )
            values
            (
                trunc(sysdate),to_char(sysdate, 'HH24:MI:SS'),'" . $codusu . "','" . $rutcli . "','" . $observacion . "',
                '" . $proxima . "','" . $gestion . "','" . $contactable . "','" . $cotizacion . "','" . $perdi . "','" . $perdi_con . "','" . $rutrel . "','" . $linea_op . "','" . $monto_linea . "'
            )
            ";
        } else {
            $sql = "insert into tm_cliente_hst
            (
                fechas,horass,userid,rutcli,observ,feclla,
                prxacc,accion,contactable,cotizacion,perdi,perdi_con,rut_rel,linea,monto_linea
            )
            values
            (
                trunc(sysdate),to_char(sysdate, 'HH24:MI:SS'),'" . $codusu . "','" . $rutcli . "','" . $observacion . "',to_date('" . $pendiente . "','yyyy-mm-dd'),
                '" . $proxima . "','" . $gestion . "','" . $contactable . "','" . $cotizacion . "','" . $perdi . "','" . $perdi_con . "','" . $rutrel . "','" . $linea_op . "','" . $monto_linea . "'
            )
            ";
        }
        DB::connection('oracle_dmventas')->beginTransaction();
        try {
            DB::connection('oracle_dmventas')->statement($sql);
            DB::connection('oracle_dmventas')->commit();
            return true;
        } catch (\Exception $e) {
            DB::connection('oracle_dmventas')->rollback();
            return false;
        }
    }
}