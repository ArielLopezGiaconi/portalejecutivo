<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 13/08/2018
 * Time: 11:03
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\MaProduct;
use \DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ProductosController;

class MaProductPDO extends Model
{
    public static function productosExisten($str_prods)
    {
        $regex = "/^(?:[\'][aA-zZ]{1,2}\d{5,6}[\']+,)*[\'][aA-zZ]{1,2}\d{5,6}[\']+$/";

        preg_match($regex, trim($str_prods), $match);

        // if match array is empty (no match), return false
        if (empty($match)) {
            return false;
        }

        $sql = "SELECT COUNT(*) AS existe"
            . " FROM ma_product"
            . " WHERE codpro in ($match[0])";

        $resultado = DB::connection('oracle_dmventas')->select($sql);

        // if the resultant count isn't equal to the substrings quantity return false
        return ($resultado != null && $resultado[0]->existe == count(explode(",", $str_prods)))
            ? true : false;
    }

    public static function existsProductByCod($in_codpro)
    {
        $sql = "SELECT COUNT(*) AS existe FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) >= 1 && $resultado[0]->existe > 0)
            ? true : false;
    }

    public static function sapGetMaterialProdByCod($in_codpro)
    {
        $sql = "SELECT codsap FROM ma_product WHERE codpro = :cod_prod";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro
        ]);

        return (count($resultado) > 0) ? $resultado[0]->codsap : null;
    }

    public static function getProductByCod($in_codpro, $first_row = true, $in_codemp = 3)
    {
        $sql = "SELECT a.* FROM ma_product a WHERE a.codpro = :cod_prod";
        if ($in_codemp == 3) {
            $resultado = DB::connection('oracle_dmventas')->select($sql, [
                'cod_prod' => $in_codpro
            ]);
        } else {
            $sql .= " AND EXISTS ( "
                . " SELECT 1 FROM ma_empprod b"
                . " WHERE b.codpro = a.codpro"
                . " AND b.codemp = :cod_emp"
                . " )";

            $resultado = DB::connection('oracle_unificado')->select($sql, [
                'cod_prod' => $in_codpro,
                'cod_emp' => $in_codemp
            ]);
        }

        return MaProductPDO::returnProductFormat($resultado, true);
    }

    public static function getProductByMaterial($in_material)
    {
        $sql = "SELECT * FROM ma_product WHERE codsap = :cod_material";
        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_material' => intval($in_material)
        ]);

        return MaProductPDO::returnProductFormat($resultado, true);
    }

    public static function getAllSapProducts()
    {
        $sql = "SELECT * FROM ma_product WHERE codsap IS NOT NULL";
        $resultado = DB::connection('oracle_dmventas')->select($sql);
        return MaProductPDO::returnProductFormat($resultado);
    }

    private static function returnProductFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $bodega = new MaProduct($registro);
            if ($first_row) {
                return $bodega;
            }
            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }

    //PROYECTO ACTUAL, YO ELIMINARIA EL RESTO PERO TU PREFIERES GUARDAR BASURA
    public static function getAllProducts($search_string, $type_search, $fulltext)
    {
        if (!$fulltext) {
            $like = ''.$search_string.'%';
        } else {
            $like = '%'.$search_string.'%';
        }
        $sql = "select a.codpro, a.despro, a.codsap from ma_product a"
            . " where a.estpro in (1,11)"
            . " and a.codsap is not null and a.codneg in (0,1)"
            . " and " . $type_search . " like '".$like."' and a.tipoprod = 1 "
            . " and exists (select 1 from re_canprod b where b.codemp = 3 and b.codpro = a.codpro) ";
        $resultado = DB::connection('oracle_dmventas')->select($sql);
        return $resultado;
    }

    public static function getProductByType($rutcli, $codpro, $tipo, $codemp = 3)
    {
        $sql = "SELECT CODPRO, CODSAP, DESPRO, "
            . "DM_VENTAS.GET_PRECIOCOSTOFINAL(" . $rutcli . ", CODPRO, '', 'P', " . $codemp . ") AS PRECIO, "
            . "DM_VENTAS.GET_PRECIOCOSTOFINAL(" . $rutcli . ", CODPRO, '', 'C', " . $codemp . ") AS COSTO, "
            . "GETCOSTOCOMERCIAL(" . $codemp . ",CODPRO) AS COSTOCOMERCIAL, "
            . "SAP_GET_STOCK(" . $codemp . ",SAP_GET_BODCODNEG(CODNEG),CODPRO," . $codemp . ") AS STOCK, "
            . "DECODE(PEDSTO, 'S',INDSBR, PEDSTO) AS CONDICION, "
            . "SAP_GET_CATEGORIA_PRODUCTO(" . $codemp . ", CODPRO) as categoria, "
            . "decode(SAP_GET_MARCA(codpro), 'SIN MARCA', GETMARCA(codpro), SAP_GET_MARCA(codpro)) as marca, "
            . "SAP_GET_LINEA_PROD(codpro) as linea "
            . "FROM MA_PRODUCT WHERE " . $tipo . " = '" . $codpro . "'";
        $resultado = DB::connection('oracle_dmventas')->select($sql);
        $productData = array();
        foreach ($resultado as $result) {
            $controlador = new ProductosController();
            $productData['codpro'] = $result->codpro;
            $productData['codsap'] = $result->codsap;
            $productData['despro'] = $result->despro;
            $productData['precio'] = (int)$result->precio;
            $productData['costo'] = (int)$result->costo;
            $productData['costocomercial'] = (int)$result->costocomercial;
            //$productData['stock'] = (int)$result->stock;
            $productData['stock'] = $controlador->getStockSap($codpro);
            $productData['contribucion'] = $result->precio - $result->costo;
            try {
                $productData['margen'] = number_format((($result->precio - $result->costo) / $result->precio) * 100, 1);
            }catch (\Exception $exception) {
                $productData['margen'] = 0;
            }
            $productData['condicion'] = $result->condicion;
            $productData['categoria'] = $result->categoria;
            $productData['marca'] = $result->marca;
            $productData['linea'] = $result->linea;
        }
        return $productData;
    }
}
