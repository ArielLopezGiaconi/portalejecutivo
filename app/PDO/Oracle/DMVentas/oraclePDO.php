<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 22/06/2018
 * Time: 10:05
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\MaUsuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\PDO\Lib\ResponseFormatt;
use Illuminate\Support\Facades\Config;
use Exception;

class oraclePDO extends Model
{

    public static function cambioClave($tipo, $claveOld, $claveNew, $rutusu, $codusu)
    {
        try {         
            //return '0';
            if($tipo == 2){
                $sql = "select (decode(clave1,'" . $claveOld . "',1,0) + 
                                decode(clave2,'" . $claveOld . "',1,0) + 
                                decode(clave3,'" . $claveOld . "',1,0)) numero from re_clavsis
                        where rutusu = " . $rutusu;
                
                $resultado = DB::connection('oracle_dmventas')->select($sql);
                
                if ($resultado[0]->numero == 0){
                    return '0';
                }else{
                    return '1';
                }
            }elseif($tipo == 3){
                $sql = "select decode(clave1,'" . $claveOld . "',1,0) clave1, 
                            decode(clave2,'" . $claveOld . "',1,0) clave2,
                            decode(clave3,'" . $claveOld . "',1,0) clave3 from re_clavsis
                        where rutusu = " . $rutusu;

                $resultado = DB::connection('oracle_dmventas')->select($sql);

                $sql = "select codval, desval, codref from de_dominio
                        where coddom = 220
                          and codval = 1
                        order by codval";

                $vencimiento = DB::connection('oracle_dmventas')->select($sql);

                $sql = "select codval, desval, codref from de_dominio
                where coddom = 220
                  and codval = 2
                order by codval";

                $tolerancia = DB::connection('oracle_dmventas')->select($sql);

                if($resultado[0]->clave1 == 1){
                    $sql = "update re_clavsis set fechas = trunc(sysdate+" . $vencimiento[0]->codref . "), 
                            clave2 = '" . $claveNew . "', 
                            fecvta = trunc(sysdate+" . $vencimiento[0]->codref . "+" . $tolerancia[0]->codref . ")
                            where rutusu = " . $rutusu;
                    
                    DB::connection('oracle_dmventas')->statement($sql);

                }elseif($resultado[0]->clave2 == 1){
                    $sql = "update re_clavsis set fechas = trunc(sysdate+" . $vencimiento[0]->codref . "), 
                    clave3 = '" . $claveNew . "', 
                    fecvta = trunc(sysdate+" . $vencimiento[0]->codref . "+" . $tolerancia[0]->codref . ")
                    where rutusu = " . $rutusu;
            
                    DB::connection('oracle_dmventas')->statement($sql);
                    
                }elseif($resultado[0]->clave3 == 1){
                    $sql = "update re_clavsis set fechas = trunc(sysdate+" . $vencimiento[0]->codref . "), 
                    clave1 = '" . $claveNew . "', 
                    fecvta = trunc(sysdate+" . $vencimiento[0]->codref . "+" . $tolerancia[0]->codref . ")
                    where rutusu = " . $rutusu;
            
                    DB::connection('oracle_dmventas')->statement($sql);

                }else{

                    $sql = "update re_clavsis set fechas = trunc(sysdate+" . $vencimiento[0]->codref . "), 
                    clave1 = '" . $claveNew . "', 
                    fecvta = trunc(sysdate+" . $vencimiento[0]->codref . "+" . $tolerancia[0]->codref . ")
                    where rutusu = " . $rutusu;
            
                    DB::connection('oracle_dmventas')->statement($sql);

                }
            }elseif($tipo == 5){
                config::set('database.connections.oracle_ejecutivo.database', $codusu);
                config::set('database.connections.oracle_ejecutivo.username', $codusu);
                config::set('database.connections.oracle_ejecutivo.password', $claveOld);

                $sql = "update ma_usuario set clave = '" . $claveNew . "' where rutusu = " . $rutusu;
                
                DB::connection('oracle_dmventas')->statement($sql);

                $sql = "alter user " . $codusu . " identified by " . $claveNew;
                
                DB::connection('oracle_ejecutivo')->statement($sql);
            }
        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getDocumentosRelacionados($numnvt)
    {
        try {            
            $sql = "select a.*, b.docaso, b.totnet from
                    (select a.numnvt, a.numgui, a.totnet total_guia, c.numfac, c.totnet total_factura from
                    (select a.numnvt, b.numgui, b.totnet, b.codemp from re_notagui a, en_guiades b 
                    where a.codemp = 3
                    and a.numnvt = " . $numnvt . "
                    and a.numgui = b.numgui
                    and a.codemp = b.codemp) a, re_guiafac b, en_factura c
                    where a.numgui = b.numgui
                    and a.codemp = b.codemp
                    and b.codemp = c.codemp
                    and b.numfac = c.numfac
                    union all
                    select a.numnvt, 0 guia, 0 total_guia, b.numfac, b.totnet total_factura from re_notafac a, en_factura b
                    where a.codemp = 3
                    and a.numnvt = " . $numnvt . "
                    and a.numfac = b.numfac) a, en_notavta b
                    where a.numnvt = b.numnvt
                      and b.codemp = 3";
           
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasRut($rut, $fechaDesde, $fechaHasta, $status)
    {
        try {
                $sql = "select
                nota_de_venta,
                orden_de_compra,
                fecha_emision,
                fecha_bodega,
                rut,
                cencos,
                razon_social,
                segmento,
                neto,
                margen,
                iva,
                total,
                estado,
                ejecutivo
                from (
                select
                nota_de_venta,
                decode(orden_de_compra, null, ' ',orden_de_compra) orden_de_compra,
                fecha_emision,
                decode(fecha_bodega, null, ' ',fecha_bodega) fecha_bodega,
                rut,
                cencos,
                razon_social,
                segmento,
                neto,
                margen,
                iva,
                total,
                case
                    when estado is not null then estado
                    when codpro is not null then 'Con genera pendiente'
                    when tipo_pend is not null and estcred_sap='C' then 'Pendiente Credito'
                    when tipo_pend is not null then 'Pendiente'
                    else 'Generada'
                end as estado,
                ejecutivo
                from
                (
                    select
                    a.numnvt as nota_de_venta,
                    a.docaso as orden_de_compra,
                    a.fecha_creac as fecha_emision,
                    a.rutcli as rut,
                    a.cencos,
                    d.fecha_bodega as fecha_bodega,
                    a.margen,
                    b.razons as razon_social,
                    sap_get_segmento_cliente(b.codemp,b.rutcli) as segmento,
                    a.totnet as neto,
                    a.totiva as iva,
                    a.totgen as total,
                    a.estcred_sap,
                    case
                    when
                        sap_get_fecha_rendicion(3,a.numnvt) is null
                    then null
                    when 
                        decode(sap_get_fecha_rendicion(3,a.numnvt),'01/01/1990','0', null) is null
                    then 'Rendida'
                    when
                        decode(sap_get_ultima_notificacion(3,a.numnvt),'01/01/1990','0', null) is null
                    then 'Notificada'
                    when
                        decode(sap_get_fecharuta_real(a.numnvt),'01/01/1990','0', null) is null
                    then 'Despachada'
                    else null
                    end as estado,
                    c.userid as ejecutivo
                    from en_notavta a
                    left join lg_ennota d
                    on a.numnvt=d.numnvt
                    ,en_cliente b,ma_usuario c
                    where a.codven=c.rutusu
                    and a.rutcli=b.rutcli
                    and b.codemp=3
                    and a.rutcli=" . $rut . "
                    and a.codest<>5
                    and a.fecha_creac between to_date('" . $fechaDesde . " 00:00:00','yyyy/mm/dd HH24:mi:ss') and to_date('" . $fechaHasta . " 23:59:59','yyyy/mm/dd HH24:mi:ss')
                ) z
                left join
                (
                    select
                    a.numnvt,b.codpro
                    from en_notavta a,de_notavta b
                    where a.numnvt=b.numnvt
                    and a.codest<>5
                    and b.codpro='W102030'
                    and a.rutcli=" . $rut . "
                    and a.fecha_creac between to_date('" . $fechaDesde . " 00:00:00','yyyy/mm/dd HH24:mi:ss') and to_date('" . $fechaHasta . " 23:59:59','yyyy/mm/dd HH24:mi:ss')
                ) y
                on z.nota_de_venta=y.numnvt
                left join
                (
                    select
                    a.numnvt,'PENDIENTE' tipo_pend
                    from en_notapen a, en_notavta b
                    Where a.estado=0                
                    and b.codest<>5
                    and b.rutcli=" . $rut . "
                    and b.fecha_creac between to_date('" . $fechaDesde . " 00:00:00','yyyy/mm/dd HH24:mi:ss') and to_date('" . $fechaHasta . " 23:59:59','yyyy/mm/dd HH24:mi:ss')                    
                    and b.numnvt=a.numnvt
                    and not exists (select 1 from de_notavta b where b.codemp=a.codemp and b.numnvt=a.numnvt and  b.codpro='W102030')
                    and not exists (select 1 from lg_ennota b where b.numnvt=a.numnvt)
                ) x
                on z.nota_de_venta=x.numnvt)";
                if ($status <> 'Todas'){
                    $sql = $sql . "where estado = '" . $status . "'";
                };
                
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCotizacionesRut($rut, $fechaDesde, $fechaHasta, $status)
    {
        try {            
            $sql = "select
            a.numcot as cotizacion,
            a.fecemi as fecha_emision,
            a.fecven as fecha_vencimiento,
            a.rutcli as rut,
            a.cencos,
            b.razons as razon_social,
            sap_get_segmento_cliente(b.codemp,b.rutcli) as segmento,
            '$' ||trim(to_char( totnet,'999,999,999')) as monto_cotizado,
            '$' ||trim(to_char(totiva,'999,999,999')) as iva,
            '$' ||trim(to_char(totgen,'999,999,999')) as total,
            case
                when fecven>=trunc(sysdate) then 'Activa'
                else 'Vencida'
            end as estado,
            c.userid as ejecutivo
            from en_cotizac a,en_cliente b,ma_usuario c
            where a.codven=c.rutusu
            and a.rutcli=b.rutcli
            and b.codemp=3
            and a.rutcli=" . $rut . "
            and a.fecemi between to_date('" . $fechaDesde . " 00:00:00','yyyy/mm/dd HH24:mi:ss') and to_date('" . $fechaHasta . " 23:59:59','yyyy/mm/dd HH24:mi:ss')";
            if($status == 'Activa'){
                $sql = $sql . "and fecven>=trunc(sysdate)";
            }elseif ($status == 'Vencida'){
                $sql = $sql . "and fecven<trunc(sysdate)";
            }elseif ($status == 'Efectiva'){
                $sql = $sql . "and fecven<trunc('01/03/1994')";
            }
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasSinDocumentar($ejecutivo)
    {
        try {
            
            $sql = "select
            nota_de_venta,
            orden_de_compra,
            fecha_emision,
            fecha_bodega,
            rut,
            cencos,
            razon_social,
            segmento,
            neto,
            margen,
            iva,
            total,
            estado,
            ejecutivo,
            estcred_sap,
            ESTBOD
            from (
            select
            nota_de_venta,
            decode(orden_de_compra, null, ' ',orden_de_compra) orden_de_compra,
            fecha_emision,
            fecha_bodega,
            rut,
            cencos,
            razon_social,
            segmento,
            neto,
            margen,
            iva,
            total,
            decode(estcred_sap, 'C', 'PENDIENTE', '') estcred_sap,
            decode(ESTBOD, 'P', 'PENDIENTE', '') ESTBOD,
            case
                when estado is not null then estado
                when codpro is not null then 'Con genera pendiente'
                when tipo_pend is not null and estcred_sap='C' then 'Pendiente Credito'
                when tipo_pend is not null then 'Pendiente'
                else 'Generada'
            end as estado,
            ejecutivo
            from
            (
                select
                a.numnvt as nota_de_venta,
                a.docaso as orden_de_compra,
                a.fecha_creac as fecha_emision,
                a.rutcli as rut,
                a.cencos,
                d.fecha_bodega as fecha_bodega,
                a.margen,
                b.razons as razon_social,
                sap_get_segmento_cliente(b.codemp,b.rutcli) as segmento,
                a.totnet as neto,
                a.totiva as iva,
                a.totgen as total,
                a.estcred_sap,
                a.ESTBOD,
                case
                when
                    sap_get_fecha_rendicion(3,a.numnvt) is null
                then null
                when 
                    decode(sap_get_fecha_rendicion(3,a.numnvt),'01/01/1990','0', null) is null
                then 'Rendida'
                when
                    decode(sap_get_ultima_notificacion(3,a.numnvt),'01/01/1990','0', null) is null
                then 'Notificada'
                when
                    decode(sap_get_fecharuta_real(a.numnvt),'01/01/1990','0', null) is null
                then 'Despachada'
                else null
                end as estado,
                c.userid as ejecutivo
                from en_notavta a
                left join lg_ennota d
                on a.numnvt=d.numnvt
                ,en_cliente b,ma_usuario c
                where a.codven=c.rutusu
                and a.rutcli=b.rutcli
                and b.codemp=3
                and a.codest<>5
                and a.codven = " . $ejecutivo . "
                and not exists ( select 1 from re_notafac z where z.codemp = a.codemp and z.numnvt = a.numnvt
                     union all
                     select 1 from re_notagui z2 where z2.codemp = a.codemp and z2.numnvt = a.numnvt)
            ) z
            left join
            (
                select
                a.numnvt,b.codpro
                from en_notavta a,de_notavta b
                where a.numnvt=b.numnvt
                and a.codest<>5
                and b.codpro='W102030'
                and a.codven = " . $ejecutivo . "
                and a.codemp = 3
                and a.codemp = b.codemp
            ) y
            on z.nota_de_venta=y.numnvt
            left join
            (
                select
                a.numnvt,'PENDIENTE' tipo_pend
                from en_notapen a, en_notavta b
                Where a.estado=0                
                and b.codest<>5                    
                and b.numnvt=a.numnvt
                and not exists (select 1 from de_notavta x where x.codemp=a.codemp and x.numnvt=a.numnvt and  x.codpro='W102030')
                and not exists (select 1 from lg_ennota y where y.numnvt=a.numnvt)
                and b.codven = " . $ejecutivo . "
                and a.codemp = 3
                and a.codemp = b.codemp
                and not exists ( select 1 from re_notafac z where z.codemp = a.codemp and z.numnvt = a.numnvt
                                union all
                                select 1 from re_notagui z2 where z2.codemp = a.codemp and z2.numnvt = a.numnvt)
            ) x
            on z.nota_de_venta=x.numnvt)
            where fecha_bodega is null";
            
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasSinDocumentarQ($ejecutivo)
    {
        try {
            
            $sql = "select count(nota_de_venta) qnotas from (select
            nota_de_venta,
            orden_de_compra,
            fecha_emision,
            fecha_bodega,
            rut,
            cencos,
            razon_social,
            segmento,
            neto,
            margen,
            iva,
            total,
            estado,
            ejecutivo,
            estcred_sap,
            ESTBOD
            from (
            select
            nota_de_venta,
            decode(orden_de_compra, null, ' ',orden_de_compra) orden_de_compra,
            fecha_emision,
            fecha_bodega,
            rut,
            cencos,
            razon_social,
            segmento,
            neto,
            margen,
            iva,
            total,
            decode(estcred_sap, 'C', 'PENDIENTE', '') estcred_sap,
            decode(ESTBOD, 'P', 'PENDIENTE', '') ESTBOD,
            case
                when estado is not null then estado
                when codpro is not null then 'Con genera pendiente'
                when tipo_pend is not null and estcred_sap='C' then 'Pendiente Credito'
                when tipo_pend is not null then 'Pendiente'
                else 'Generada'
            end as estado,
            ejecutivo
            from
            (
                select
                a.numnvt as nota_de_venta,
                a.docaso as orden_de_compra,
                a.fecha_creac as fecha_emision,
                a.rutcli as rut,
                a.cencos,
                d.fecha_bodega as fecha_bodega,
                a.margen,
                b.razons as razon_social,
                sap_get_segmento_cliente(b.codemp,b.rutcli) as segmento,
                a.totnet as neto,
                a.totiva as iva,
                a.totgen as total,
                a.estcred_sap,
                a.ESTBOD,
                case
                when
                    sap_get_fecha_rendicion(3,a.numnvt) is null
                then null
                when 
                    decode(sap_get_fecha_rendicion(3,a.numnvt),'01/01/1990','0', null) is null
                then 'Rendida'
                when
                    decode(sap_get_ultima_notificacion(3,a.numnvt),'01/01/1990','0', null) is null
                then 'Notificada'
                when
                    decode(sap_get_fecharuta_real(a.numnvt),'01/01/1990','0', null) is null
                then 'Despachada'
                else null
                end as estado,
                c.userid as ejecutivo
                from en_notavta a
                left join lg_ennota d
                on a.numnvt=d.numnvt
                ,en_cliente b,ma_usuario c
                where a.codven=c.rutusu
                and a.rutcli=b.rutcli
                and b.codemp=3
                and a.codest<>5
                and a.codven = " . $ejecutivo . "
                and not exists ( select 1 from re_notafac z where z.codemp = a.codemp and z.numnvt = a.numnvt
                     union all
                     select 1 from re_notagui z2 where z2.codemp = a.codemp and z2.numnvt = a.numnvt)
            ) z
            left join
            (
                select
                a.numnvt,b.codpro
                from en_notavta a,de_notavta b
                where a.numnvt=b.numnvt
                and a.codest<>5
                and b.codpro='W102030'
                and a.codven = " . $ejecutivo . "
                and a.codemp = 3
                and a.codemp = b.codemp
            ) y
            on z.nota_de_venta=y.numnvt
            left join
            (
                select
                a.numnvt,'PENDIENTE' tipo_pend
                from en_notapen a, en_notavta b
                Where a.estado=0                
                and b.codest<>5                    
                and b.numnvt=a.numnvt
                and not exists (select 1 from de_notavta b where b.codemp=a.codemp and b.numnvt=a.numnvt and  b.codpro='W102030')
                and not exists (select 1 from lg_ennota b where b.numnvt=a.numnvt)
                and b.codven = " . $ejecutivo . "
                and a.codemp = 3
                and a.codemp = b.codemp
                and not exists ( select 1 from re_notafac z where z.codemp = a.codemp and z.numnvt = a.numnvt
                     union all
                     select 1 from re_notagui z2 where z2.codemp = a.codemp and z2.numnvt = a.numnvt)
            ) x
            on z.nota_de_venta=x.numnvt)
            where fecha_bodega is null)";
            
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCobrador($rutcli, $cencos)
    {
        try {
            
            $sql = "select b.rutusu, b.nombre || ' ' || b.apepat || ' '  || b.apemat as nombre, b.codusu from de_cliente a, ma_usuario b
            where a.codemp = 3 
              and a.rutcli = " . $rutcli . "
              and a.cencos = " . $cencos . "
              and a.codcob = b.rutusu
              and a.codemp = b.codemp";
            
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCentrosCostos($rutcli)
    {
        try {
            
            $sql = "select id_cliente_sap, cencos codigo, 'SECUNDARIO' as cencos, descco as descripcion, 
                    direcc, sap_get_dominio(6,plapag) plazo_pago, sap_get_dominio(5,forpag) forma_pago, 
                    contac, concob cobranza, sap_get_dominio(2,comuna) comuna, 
                    sap_get_dominio(1,ciudad) ciudad, carcon cargo, fono01 telefono, numfax celular, 
                    telcob, emails, mail_cob from de_cliente a
                    where rutcli = " . $rutcli . "
                    and codemp = 3
                    and exists (select 1 from en_cliente x where x.rutcli = a.rutcli and x.codemp = a.codemp and x.cencos = a.cencos)
                    union all
                    select id_cliente_sap, cencos codigo, 'SECUNDARIO' as cencos, descco as descripcion,
                            direcc, sap_get_dominio(6,plapag) plazo_pago, sap_get_dominio(5,forpag) forma_pago, 
                            contac, concob cobranza, sap_get_dominio(2,comuna) comuna, 
                            sap_get_dominio(1,ciudad) ciudad, carcon cargo, fono01 telefono, numfax celular, 
                            telcob, emails, mail_cob from de_cliente b
                    where rutcli = " . $rutcli . "
                    and codemp = 3
                    and not exists (select 1 from en_cliente x where x.rutcli = b.rutcli and x.codemp = b.codemp and x.cencos = b.cencos)";
            
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasWebHist($ruts, $fecha, $rutcli)
    {
        try {
           
            $sql = "select a.numord, decode(a.numnvt, null, 'SIN NOTA', a.numnvt) numnvt, a.fecord, a.rutcli, SAP_GET_RAZONSOCIAL(3, a.rutcli) razon, a.cencos, GETCLASIFICACION_CLIENTE(3, A.RUTCLI) segmento, a.facnom, 
                    sap_get_dominio(705,a.tipweb) canal, sap_get_dominio(150,a.clavta) venta, totnet,  
                    a.tipdoc, a.pagada, nvl(a.observ,'SIN OBSERVACION') as observ
            from we_notavta a, de_cliente b
            where a.rutcli = " . $rutcli . "
            and a.rutcli = b.rutcli
            and a.fecord >= sysdate-10
            and a.cencos = b.cencos
            and b.rutven " . $ruts . "
            and b.codemp = 3
            group by a.numnvt, a.numord, a.fecord, a.rutcli, a.cencos, a.facnom, a.tipweb, a.clavta, totnet, a.tipdoc, a.pagada, a.observ";

            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasWebQ($ruts, $fecha)
    {
        try {
           
            /*$sql = "select count(numero) as numero from (
                select count(a.numord) as numero
                from we_notavta a, de_cliente b
                where a.numnvt is null
                and a.rutcli = b.rutcli
                and a.codest = 0
                and a.fecord >= to_Date('" . $fecha . "','yyyy/mm/dd')
                and b.rutven " . $ruts . "
                and a.cencos = b.cencos
                group by a.numord, a.fecord, a.rutcli, a.cencos, a.facnom, a.tipweb, a.clavta, totnet, a.tipdoc, a.pagada, a.observ)";*/
			$sql = "select count(numero) as numero from (
                select count(a.numord) as numero
                from we_notavta a, de_cliente b, wd_notavta c
                where a.numnvt is null
                and a.rutcli = b.rutcli
                and a.codest = 0
				and a.fecord>=sysdate-90
                and b.codemp = 3
                and b.rutven " . $ruts . "
                and a.cencos = b.cencos
                and a.numord = c.numord
                group by a.numord, a.fecord, a.rutcli, a.cencos, a.facnom, a.tipweb, a.clavta, totnet, a.tipdoc, a.pagada, a.observ)";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function Convenio($rutcli)
    {
        try {
           
            $sql = "select b.numcot, b.codpro, sap_get_Descripcion(b.codpro) descripcion, sap_get_marca(b.codpro) marca, b.precio, a.fecemi, a.fecven from en_conveni a, de_conveni b
            where rutcli = " . $rutcli . "
                and sysdate between a.fecemi and a.fecven
                and a.numcot = b.numcot";

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getReporteCobranza($rutcli)
    {
        try {
            $sql = "Select id_cliente_sap,
            a.rutcli,
            c.razons,
            b.doc_referencia factura,
            a.cencos,
            a.fecemi,
            a.fecven,
            a.totgen,
            decode(CON_PROMESA_PAGO,'X','Si','No') promesa,
            sap_get_doctos_asociados(b.doc_referencia) doc_asociados
            from en_factura a, ma_partidas_abiertas b, en_Cliente c  where cod_cliente =  c.id_cliente_Sap
            and a.numfac = to_number(b.doc_referencia)
            and c.rutcli  = a.rutcli
            and c.rutcli  = " . $rutcli . "
            and  c.codemp = 3
            and b.clase_doc in('DR','YZ')
            order by a.fecven desc";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getReporteDeudaVencida($rutcli)
    {
        try {
            $sql = "Select id_cliente_sap,
            a.rutcli,
            c.razons,
            b.doc_referencia factura,
            a.cencos,
            to_char(a.fecemi) as fecemi,
            to_char(a.fecven) as fecven,
            a.totgen,
            decode(CON_PROMESA_PAGO,'X','Si','No') promesa,
            sap_get_doctos_asociados(b.doc_referencia) doc_asociados
            from en_factura a, ma_partidas_abiertas b, en_Cliente c  where cod_cliente =  c.id_cliente_Sap
            and a.numfac = to_number(b.doc_referencia)
            and c.rutcli  = a.rutcli
            and c.rutcli = " . $rutcli . "
            and  c.codemp = 3
            and fecven < sysdate
            and b.clase_doc in('DR','YZ')
            order by fecemi desc";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getReporteDeudaVencidaTotal($rutcli)
    {
        try {
            $sql = "Select sum(a.totgen) total
            from en_factura a, ma_partidas_abiertas b, en_Cliente c  where cod_cliente =  c.id_cliente_Sap
            and a.numfac = to_number(b.doc_referencia)
            and c.rutcli  = a.rutcli
            and c.rutcli = " . $rutcli . "
            and  c.codemp = 3
            and fecven < sysdate
            and b.clase_doc in('DR','YZ')";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getReporteCobranzaDatos($rutcli)
    {
        try {
            // $sql = "select c.nombre || ' ' || c.apepat || ' '  || c.apemat cobrador, mail01 mail_cobrador, decode(nvl(anexos,0), 0, 'Sin anexo', '22 385' || anexos) anexo_cobrador, 
            // a.concob cobranza, a.telcob, a.mail_cob, a.carcon cargo            
            // from de_cliente a, en_cliente b, ma_usuario c
            // where a.rutcli = " . $rutcli . "
            // and a.codemp = 3
            // and a.rutcli = b.rutcli
            // and a.cencos = b.cencos
            // and a.codemp = b.codemp
            // and a.codcob = c.rutusu
            // and b.codemp = c.codemp";
            $sql = "select c.nombre || ' ' || c.apepat || ' '  || c.apemat cobrador,
                            mail01 mail_cobrador, 
                            decode(nvl(anexos,0), 0, 'Sin anexo', '22 385' || anexos) anexo_cobrador, 
                            a.concob cobranza, 
                            a.telcob, 
                            a.mail_cob, 
                            a.carcon cargo,
                            sap_get_dominio(2,292) comuna,
                            sap_get_dominio(1,130) ciudad,
                            d.dirdes direccion,
                            b.razons,
                            a.rutcli
                    from de_cliente a, en_cliente b, ma_usuario c, re_ddescli d
                    where a.rutcli = " . $rutcli . "
                    and a.codemp = 3
                    and a.rutcli = b.rutcli
                    and a.cencos = b.cencos
                    and a.codemp = b.codemp
                    and a.codcob = c.rutusu
                    and b.codemp = c.codemp
                    and b.rutcli = d.rutcli
                    and a.dirdes = d.coddir";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getCabeceraReportesCorp($tipo, $rutcli, $mes)
    {
        try {
           
            if ($tipo == 1) {
                $sql = "select rutcli, razons, b.nombre || ' ' || b.apepat as nombre, b.mail01 mail, sysdate fecha, '" . $mes . "' mes from en_cliente a, ma_usuario b
                    where a.rutcli = " . $rutcli . "
                      and a.codven = b.rutusu";
                      
                $resultado = DB::connection('oracle_dmventas')->select($sql);
            }else{
                $sql = "select id rutcli, dirigido razons, ejecutivo nombre, mail, now() fecha, '" . $mes . "' mes from holding_ids
                        where id = " . $rutcli;
                      
                $resultado = DB::connection('mysql_portal_ejecutivo')->select($sql);
            }
            
            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function grabaObserv($rutcli, $observacion, $cencos)
    {
        try {
           
            $sql = "update de_cliente set observ = '" . $observacion . "' where rutcli = " . $rutcli . " and cencos = " . $cencos . " and codemp = 3";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->statement($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function observacionCliente($rutcli)
    {
        try {
           
            $sql = "select a.rutcli, b.observ, a.cencos 
            from en_cliente a, de_cliente b 
            where a.rutcli = " . $rutcli . " 
            and a.rutcli = b.rutcli 
            and a.cencos = b.cencos 
            and a.codemp = 3 
            and a.codemp = b.codemp";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function validaConvenio($rutcli)
    {
        try {
           
            $sql = "select a.numcot, a.fecemi, a.fecven from en_conveni a
            where rutcli = " . $rutcli . "
                and sysdate between a.fecemi and a.fecven";

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getRelaciones($rutcli)
    {
        try {
           
            $sql = "select b.rutcli, b.razons, BI_GET_EJECUTIVO_VENTA(codven) ejecutivo from re_emprela a, en_cliente b
            where numrel = (select numrel from re_emprela where rutcli = " . $rutcli . ")
              and a.rutcli = b.rutcli";

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

            // $datos = array();
            // $i = 0;

            // for ($i = 0; $i <= sizeof($resultado); $i++) {
            //     $sql = "select																																						
            //     clasificacion,																																			
            //     if(dias_restantes<=0,'Disponible para traspaso', dias_restantes) as dias	
            //     from portal_ej_new_gestion_cartera																																		
            //     where rutcli=" . $resultado[$i]->rutcli;
                
            //     $respuesta = DB::connection('mysql_portal_ejecutivo')->select($sql);
                
            //     $datos[$i]['rutcli'] = $resultado[$i]->rutcli;
            //     $datos[$i]['razons'] = $resultado[$i]->razons;
            //     $datos[$i]['ejecutivo'] = $resultado[$i]->ejecutivo;
            //     $datos[$i]['clasificacion'] = $respuesta[0]->clasificacion;
            //     $datos[$i]['dias'] = $respuesta[0]->dias;
            // }

            // return $datos;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getContactos($rutcli)
    {
        try {
           
            $sql = "select
            rutcli,cencos,
            'Contacto Comercial' as tipo_contacto,
            nvl(fono01,'0') as fono,nvl(numfax,'0') as celular_o_fax,contac,nvl(carcon,'SIN INFORMAR') as carcon,emails as mail
            from de_cliente
            where codemp=3
            and rutcli=" . $rutcli . "
            union all
            select
            rutcli,cencos,
            'Contacto Cobranza' as tipo_contacto,
            nvl(telcob,'0') as fono,nvl(faxcob,'0') as celular_o_fax,concob,nvl(carcon,'SIN INFORMAR') as carcon,mail_cob as mail
            from de_cliente
            where codemp=3
            and rutcli=" . $rutcli;

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getGestiones($rutcli)
    {
        try {
           
            $sql = "select
            documento,tipo,estado,de,a,por,rutcli,folio,to_char(fecha,'dd-mm-yyyy HH24:mi') as fecha,valorizado, 
            decode(observacion,null,' ',observacion) as observacion, activa,
            trunc(sysdate)-trunc(fecha) as q_dias
            from
            (
            select
            documento,tipo,estado,de,a,por,rutcli,folio,fecha,valorizado,
            lower(observacion) as observacion, activa
            from
            (
            select
            numcot documento,
            'Cotizacion' as tipo,a.codest as estado,
            b.userid as de,' - ' a,'-' as por,
            a.rutcli,a.numcot as folio,a.fecemi as fecha,a.totnet as valorizado,
            a.observ as observacion,
            case
            when fecven-trunc(sysdate)>=0 then 'Activa'
            else 'Vencida'
            end as activa
            from en_cotizac a,ma_usuario b
            where a.codven=b.rutusu
            and a.fecemi between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
            and a.rutcli=" . $rutcli . "
            and a.codusu not in ('16460269','5822011')
            union all
            select
            numnvt documento,
            'Nota de Venta' as tipo,a.codest as estado,
            b.userid as de,' - ' a,'' as por,
            a.rutcli,a.numnvt as folio,a.fecha_creac as fecha,a.totnet as valorizado,
            a.observ as observacion,
            '' as activa
            from en_notavta a,ma_usuario b
            where a.codven=b.rutusu
            and a.fecha_creac between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
            and a.rutcli=" . $rutcli . "
            and a.codest<>5
            union all
            select
            0 documento,
            'Cambio de ejecutivo' as tipo,a.codest*1 as estado,
            b.userid as de,c.userid a,d.userid as por,
            a.rutcli,0 as folio,a.fechas as fecha,0 as valorizado,
            a.observacion as observacion,
            '' as activa
            from lg_camven a,ma_usuario b,ma_usuario c,ma_usuario d
            where a.rutnue=c.rutusu
            and a.rutven=b.rutusu
            and a.rutusu=d.rutusu
            and a.fechas between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
            and a.rutcli=" . $rutcli . "
            union all
            select
            0 documento,
            'Anotacion comercial' as tipo,0 as estado,
            a.userid as de,' - ' as  a,'' as por,
            a.rutcli,0 as folio,
            to_date(to_char(fechas,'dd/mm/yyyy') || ' ' || horass,'dd/mm/yyyy hh24:mi:ss') as fecha,0 as valorizado,
            a.observ as observacion,
            '' as activa
            from tm_cliente_hst a
            where a.fechas between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
            and a.rutcli=" . $rutcli . "
            union all
            select
            b.numcot documento,
            'Anulacion solicitud Convenio' as tipo,a.codest as estado,
            a.usr_anula as de,' - ' as  a,'' as por,
            b.rutcli,a.numcot as folio,
            a.fec_anula as fecha,a.monto as valorizado,
            a.observacion as observacion,
            '' as activa
            from en_convenio_wf a,en_cotizac b
            where a.fec_anula between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
            and b.rutcli=" . $rutcli . "
            and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Solicitud Convenio' as tipo,a.codest as estado,
                a.usr_creac as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fecsol as fecha,a.monto as valorizado,
                a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fecsol between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Asignacion Convenio' as tipo,a.codest as estado,
                a.usr_asignado as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_asignado as fecha,a.monto as valorizado,
                a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fec_asignado between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Propuesta Convenio' as tipo,a.codest as estado,
                a.usr_excel as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_excel as fecha,a.monto as valorizado,
                a.observacion as observacion,
                '' as activa
                from  en_convenio_wf a,en_cotizac b
                where a.fec_excel between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Aprobacion Convenio' as tipo,a.codest as estado,
                a.usr_aprobado as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_aprobado as fecha,a.monto as valorizado,
                a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fec_aprobado between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Cierre Convenio' as tipo,a.codest as estado,
                a.usr_cierre as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_cierre as fecha,a.monto as valorizado,
                a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fec_cierre between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Rechazo Convenio' as tipo,a.codest as estado,
                a.usr_rechazo as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_rechazo as fecha,a.monto as valorizado,
                 a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fec_rechazo between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
                union all
                select
                b.numcot documento,
                'Sin respuesta Convenio' as tipo,a.codest as estado,
                a.usr_sinrespuesta as de,' - ' as  a,'' as por,
                b.rutcli,a.numcot as folio,
                a.fec_sinrespuesta as fecha,a.monto as valorizado,
                 a.observacion as observacion,
                '' as activa
                from en_convenio_wf a,en_cotizac b
                where a.fec_sinrespuesta between add_months(to_date('01/' ||to_char(trunc(sysdate),'mm/yyyy'),'dd/mm/yyyy'),-24) and last_day(sysdate)
                and b.rutcli=" . $rutcli . "
                and a.numcot=b.numcot
                and a.codemp=b.codemp
            )
            order by fecha desc
            )
            where rownum<=20";

            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getNotasWeb($ruts, $fecha)
    {
        try {
           
            /*$sql = "select a.numord, a.fecord, a.rutcli, SAP_GET_RAZONSOCIAL(3, a.rutcli) razon, a.cencos, GETCLASIFICACION_CLIENTE(3, A.RUTCLI) segmento, a.facnom, 
                    sap_get_dominio(705,a.tipweb) canal, sap_get_dominio(150,a.clavta) venta, totnet,  
                    a.tipdoc, a.pagada, nvl(a.observ,'SIN OBSERVACION') as observ
            from we_notavta a, de_cliente b
            where a.numnvt is null
            and a.rutcli = b.rutcli
            and a.codest = 0
            and a.fecord >= to_Date('" . $fecha . "','yyyy/mm/dd')
            and b.rutven " . $ruts . "
            and a.cencos = b.cencos
            group by a.numord, a.fecord, a.rutcli, a.cencos, a.facnom, a.tipweb, a.clavta, totnet, a.tipdoc, a.pagada, a.observ";*/
			
			$sql = "select a.numord, a.fecord, a.rutcli, SAP_GET_RAZONSOCIAL(3, a.rutcli) razon, a.cencos, GETCLASIFICACION_CLIENTE(3, A.RUTCLI) segmento, a.facnom, 
                    sap_get_dominio(705,a.tipweb) canal, sap_get_dominio(150,a.clavta) venta, sum(c.canpro *c.prelis) totnet, 
                    a.tipdoc, a.pagada, nvl(a.observ,'SIN OBSERVACION') as observ
            from we_notavta a, de_cliente b, wd_notavta c 
            where a.numnvt is null
            and a.rutcli = b.rutcli
            and a.codest = 0
			and a.fecord>=sysdate-90
            and a.codemp = 3
            and b.rutven " . $ruts . "
            and a.codemp = b.codemp
            and a.cencos = b.cencos
            and a.numord = c.numord
            group by a.numord, a.fecord, a.rutcli, a.cencos, a.facnom, a.tipweb, a.clavta, totnet, a.tipdoc, a.pagada, a.observ";
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function RegistroAccion($fecha, $codusu, $observ, $numord, $rutcli)
    {
        try {

            $sql = "insert into tm_cliente_hst
            (fechas, userid, rutcli, observ, accion)
            values(to_date('" . $fecha . "','dd/mm/yyyy'), '" . $codusu . "', " . $rutcli . ", '" . $observ . "', 'Eliminación orden web " . $numord . "')";
            
            //return $sql;
            
            $resultado = DB::connection('oracle_dmventas')->statement($sql);

            return "ok";

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function EliminaNotaWeb($codusu, $observ, $numord, $rutcli)
    {
        try {
            //return ($codusu . $observ . $numord . $rutcli);
            $sql = "update we_notavta set codest = 1 where numord = " . $numord;
            //return $sql;
            $resultado = DB::connection('oracle_dmventas')->statement($sql);

            return "ok";

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getFechasAcciones($codusu, $fecha)
    {
        try {
            return $fecha;
            $sql = "select
            distinct a.feclla as fecha_accion_agendada
            from tm_cliente_hst a,en_cliente b
            where a.rutcli=b.rutcli 
            and b.codemp=3 
            and trunc(a.feclla) >= '" . $fecha . "' 
            and a.userid='" . $codusu . "'
            ORDER BY A.FECLLA DESC";
            return $sql;
            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getIdCliente($rut, $cencos)
    {
        try {
            $sql = "select id_cliente_sap from de_cliente where codemp = 3 and rutcli = " . $rut . " and cencos = " . $cencos;

            //return $sql;

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado[0]->id_cliente_sap;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function ActualizaMa_partidas_abiertas($datos, $id_cliente)
    {
        try {
            $sql = "Delete ma_partidas_abiertas where cod_cliente = " . $id_cliente;

            DB::connection('oracle_dmventas')->statement($sql);
            
            foreach ($datos as $item) {
                $sql = "Insert into ma_partidas_abiertas(COD_CLIENTE, COD_SOCIEDAD, DOC_FINANCIERO, CLASE_DOC, DOC_REFERENCIA, COD_SOLICITANTE, IMPORTE_MONEDA_LOC, FECHA_DOC, CONDICION_PAGO, DIAS_PLAZO, FECHA_VENCIMIENTO, EJERCICIO, CON_PROMESA_PAGO, FECHA_VENC_PROMESA_PAGO) values(" .
                        $id_cliente . "," . $item['COD_SOCIEDAD'] . "," . $item['DOC_FINANCIERO'] . ",'" . $item['CLASE_DOC'] . "','" . $item['DOC_REFERENCIA'] . "'," . $item['COD_SOLICITANTE'] . "," . $item['IMPORTE_MONEDA_LOC'] . ",'" . $item['FECHA_DOC'] . "'," .
                        "'" . $item['CONDICION_PAGO'] . "'," . $item['DIAS_PLAZO'] . ",'" . $item['FECHA_VENCIMIENTO'] . "'," . $item['EJERCICIO'] . ",";
                
                if ($item['CON_PROMESA_PAGO'] = "null") {
                    $sql = $sql . " Null ,";
                }else {
                    $sql = $sql . "'" . $item['CON_PROMESA_PAGO'] . "',";
                }

                if ($item['FECHA_VENC_PROMESA_PAGO'] = "null") {
                    $sql = $sql . "Null )";
                }else {
                    $sql = $sql . "'" . $item['FECHA_VENC_PROMESA_PAGO'] . "')";
                }

                DB::connection('oracle_dmventas')->statement($sql);
            }

            return 'ok';

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getAccionesQ($codusu, $fecha)
    {
        try {
            $sql = "select
            count(a.rutcli) cantidad
            from tm_cliente_hst a,en_cliente b
            where a.rutcli=b.rutcli 
            and b.codemp=3 
            and trunc(a.feclla)=to_date('" . $fecha . "','yyyy/mm/dd')
            and a.userid " . $codusu;

            //return $sql;

            $resultado = DB::connection('oracle_dmventasQA')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function GestionMes($rutusu, $codcnl, $tipo)
    {
        try {
            $sql = "select
            a.codven,b.userid,b.codcnl,
            tlmkt as q_cotizaciones_tlmkt,
            tlmkt_web as q_cotizaciones_tlmkt_web,
            auto_tlmkt_web as q_cotizaciones_focos,
            case
                when tlmkt_web + tlmkt>0 then round((tlmkt_web/(tlmkt+tlmkt_web))*100,2) 
                else 0
            end as porcentaje_cotizaciones,
            n_tlmkt as q_notas_tlmkt,
            n_tlmkt_web as q_notas_tlmkt_web,
            case
                when n_tlmkt_web + n_tlmkt>0 then round((n_tlmkt_web/(n_tlmkt+n_tlmkt_web))*100,2) 
                else 0
            end as porcentaje_notas
            from
            (
                select
                codven,
                nvl(
                count
                (
                    case
                        when tipo='c' and modulo is null then numcot
                        else null
                    end
                ),0) as tlmkt,
                nvl(
                count
                (
                    case
                        when tipo='c' and modulo='TLMK WEB' then numcot
                        else null
                    end
                ),0) as tlmkt_web,
                nvl(
                count
                (
                    case
                        when tipo='a' and modulo='TLMK WEB' then numcot
                        else null
                    end
                ),0) as auto_tlmkt_web,
                nvl(
                count
                (
                    case
                        when tipo='n' and modulo is null then numcot
                        else null
                    end
                ),0) as n_tlmkt,
                nvl(
                count
                (
                    case
                        when tipo='n' and modulo='TLMK WEB' then numcot
                        else null
                    end
                ),0) as n_tlmkt_web
                from
                (
                    select
                    'c' as tipo,codven,numcot,modulo,totnet
                    from en_cotizac
                    where fecemi between to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy') and sysdate
                    union all
                    select
                    'n' as tipo,codven,numnvt as numcot,modulo,totnet
                    from en_notavta
                    where fecha_creac between to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy') and sysdate
                    and codest<>5
                    union all
                    select
                    'a' as tipo,b.rutusu as codven,a.cotizacion*1 as numcot,'TLMK WEB' as modulo,0 as totnet
                    from tm_cliente_hst a,ma_usuario b
                    where a.userid=b.userid 
                    and fechas between to_date('01/'||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy') and sysdate
                    and codacc=55
                ) a
                group by codven 
            ) a,ma_usuario b
            where a.codven=b.rutusu ";
            if ($tipo == 0){
                $sql = $sql . "and a.codven = " . $rutusu . " ";
            }
            $sql = $sql . "and codcnl=" . $codcnl . "
            order by tlmkt+tlmkt_web+n_tlmkt+n_tlmkt_web desc";

            //return $sql;

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getAcciones($codusu, $fecha, $activo, $permanente)
    {
        try {
            $sql = "select
            b.rutcli,b.razons,sap_get_segmento_cliente(b.codemp,b.rutcli) as segmento,
            a.feclla as fecha_accion_agendada,
            a.prxacc as accion_agendada_para_hoy,
            a.fechas as fecha_ultima_accion,
            a.accion as accion_realizada,
            decode(a.observ,null,' ',a.observ) as observacion
            from tm_cliente_hst a,en_cliente b
            where a.rutcli=b.rutcli
              and b.codemp=3 
              and trunc(a.feclla)>=to_date('" . $fecha . " 00:00:00','yyyy/mm/dd hh24:mi:ss')
              and a.userid " . $codusu . " ";

            if ($activo == 1) {
                $sql = $sql . "and activo = 1 ";
            }else{
                if($activo == 2) {
                    $sql = $sql . "and activo is null ";
                }
            } 

            if ($permanente == 1) {
                $sql = $sql . "and permanente = 1 ";
            }else{
                if($permanente == 2) {
                    $sql = $sql . "and permanente is null ";
                }
            } 

            //return $sql;

            $resultado = DB::connection('oracle_dmventasQA')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }
    
    public static function getStatusNota($nota)
    {
        try {
            $sql = "select nvl((select numnvt from lg_ennota where numnvt = " . $nota . "),0) paso1,
            nvl((select numnvt from lg_ennota where numnvt = " . $nota . " and traspasado_dcms = 'S' and con_posicion = 'S'),0) paso2,
            nvl((select numnvt from re_en_notavta_cajas where numnvt = " . $nota . " and con_recepcion_despacho = 'S' group by numnvt), 0) paso3,
            nvl((select numnvt from re_en_notavta_cajas where numnvt = " . $nota . " and con_auditoria_jaula = 'S' group by numnvt), 0) paso4,
            nvl((select SAP_GET_NUMDOCTOS(codemp, numnvt) from en_notavta where numnvt = " . $nota . "), 0) paso5,
            (select CASE trazabilidad_pedido(" . $nota . ", 1)
                WHEN 'Entregada a Operador Logistico' THEN 'Entregada a Operador Logistico'
                WHEN 'En preparación' THEN '0'
                WHEN 'En ruta' THEN 'paso6'
                WHEN 'Entregada' THEN 'Entregada'
                WHEN 'Entregado parcial' THEN 'Entregado parcial'
                WHEN 'Pospuesto' THEN 'paso6'
                WHEN 'Entrega fallida' THEN 'Entrega fallida'
                END as paso67 from dual) paso67,
             nvl((select numnvt from en_notavta where numnvt = " . $nota . " and estcred_sap = 'C'), 0) pen_credito,
             nvl((select numnvt from en_notavta where numnvt = " . $nota . " and ESTBOD = 'P'), 0) pen_stock from dual";

            //return $sql;

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

    public static function getInfoStatus($nota, $tipo)
    {
        try {

            switch ($tipo) {
                case 1:
                    $sql = "select numnvt, nvl(mensaje_sap,'') mensaje_sap, nvl(msg_libera_sap,'') msg_libera_sap, 
                            nvl(fecha_libera_sap,'') fecha_libera_sap, fecemi, fecha_creac, rutcli from en_notavta where numnvt = " . $nota;
                    break;
                case 2:
                    $sql = "select numnvt, to_char(fecha_traspasado_dcms,'dd-mm-yyyy hh24:mi:ss') fecha_preparacion, 
                           to_char(fecha_bodega,'dd-mm-yyyy hh24:mi:ss') fecha_bodega 
                            from lg_ennota where numnvt = " . $nota;
                    break;
                case 3:
                    $sql = "select " . $nota . " numnvt, numhoj, fechas fecha_ruta, patente, trazabilidad_pedido(" . $nota . ", 5) operador from en_rutades 
                    where numhoj in(select numhoj from de_rutades where numnvt = " . $nota . ")";
                    break;
                case 4:
                    $sql = "select count(*) from eventos_transportes_ext where numnvt = " . $nota . " and status = 'SUCCESS'";

                    $resultado = DB::connection('oracle_dmventas')->select($sql);

                    if (empty($resultado)){
                        $sql = "select 'Entregada' status,
                        decode(proveedor,1,'Rounting',2,'Omega',3,'Fedex','Otro') proveedor, 
                        proveedor id_proveedor,
                        nvl(commentt,'SIN COMENTARIO') comentarios, 
                        nvl(url_photos,'0') foto,
                        url_photos,
                        to_char(create_at_ws,'dd/mm/yyyy hh24:mi:ss') fecha, 
                        mobile_device,
                        patente
                        from eventos_transportes_ext where numnvt = " . $nota . " and status = 'SUCCESS'";
                    }else{
                        $sql = "select SAP_GET_STATUSFEDEX(STATUS) status,
                        decode(proveedor,1,'Rounting',2,'Omega',3,'Fedex',4,'Proceso interno bodega','Otro') proveedor,
                        proveedor id_proveedor,
                        nvl(commentt,'SIN COMENTARIO') comentario,
                        nvl(url_photos,'0') foto,
                        url_photos,
                        to_char(create_at_ws,'dd/mm/yyyy hh24:mi:ss') fecha,
                        mobile_device,
                        patente 
                        from eventos_transportes_ext 
                        where numnvt = " . $nota . " 
                        and create_at_ws = (select max(create_at_ws) from eventos_transportes_ext where numnvt = " . $nota . " and status <> 'ARRIVAL')";
                    }
                    break;
            }

            // return $sql;

            $resultado = DB::connection('oracle_dmventas')->select($sql);

            return $resultado;

        } catch (\Exception $e) {
            throw new Exception("Error al consultar a la base de datos\n Error: " . $e->getMessage());
            return false;
        }
    }

}