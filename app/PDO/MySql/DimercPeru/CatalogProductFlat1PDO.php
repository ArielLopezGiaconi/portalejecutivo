<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 12:31
 */

namespace App\PDO\MySql\DimercPeru;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class CatalogProductFlat1PDO extends Model
{
    public static function getAllSku() {
        $sql = "SELECT sku FROM dimerc.catalog_product_flat_1 "
            . " WHERE sku LIKE 'PE_%' "
            // . " AND custom_stock_status IS NULL"
        ;

        $resultado = DB::connection('peru_mysql_magento')->select($sql);

        $collection = new Collection();
        foreach ($resultado as $registro) {
            $collection->push(str_replace('PE_', '', ''.$registro->sku));
        }

        return $collection;
    }
}