<?php


#region Ejecutivo
use Illuminate\Support\Facades\Storage;
use App\PDO\Oracle\DMVentas\MaUsuarioPDO;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Input;
use JonnyW\PhantomJs\Client;

Route::get('portada', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.portada');
    } else {
        return redirect()->to('login');
    }
});

Route::get('cotizaciones/{valor?}', function ($valor='') {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.cotizaciones', ["valor"=>$valor]);

    } else {
        return redirect()->to('login');
    }
});  

// Route::name('print')->get('/imprimir', 'FichaClienteController@imprimir');

//Route::post('getCuartacopia', 'CarteraController@loadDocumentAction'); 
Route::name('print3')->get('imprimirCartola/{datos?}', 'GobiernoController@getDatosReportesCorp');
Route::name('print2')->get('getCuartacopia/{valor?}', 'CarteraController@loadDocumentAction');
Route::name('print')->get('imprimir/{valor?}', 'FichaClienteController@imprimir');

Route::get('notasWeb', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.notasWeb');
    } else {
        return redirect()->to('login');
    }
});

Route::get('GestionMes', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.GestionMes');
    } else {
        return redirect()->to('login');
    }
});

Route::get('Indicadores', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.Indicadores');
    } else {
        return redirect()->to('login');
    }
});

Route::get('ReporteProductividad', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.ReporteProductividad');
    } else {
        return redirect()->to('login');
    }
});

Route::get('estadisticasDia', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.estadisticasDia');
    } else {
        return redirect()->to('login');
    }
});

Route::get('estadisticasMes', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.estadisticasMes');
    } else {
        return redirect()->to('login');
    }
});

Route::get('carteraMes', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.carteraMes');
    } else {
        return redirect()->to('login');
    }
});

Route::get('SeguimientoNota', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.SeguimientoNota');
    } else {
        return redirect()->to('login');
    }
});

Route::get('anotacionesEjecutivo', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.anotacionesEjecutivo');
    } else {
        return redirect()->to('login');
    }
});
Route::get('ingresosWeb', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.IngresosWeb');
    } else {
        return redirect()->to('login');
    }
});

Route::get('focos', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.focos');
    } else {
        return redirect()->to('login');
    }
});

Route::get('acciones', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.accionesAgendadas');
    } else {
        return redirect()->to('login');
    }
});

Route::get('chat', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.chat');
    } else {
        return redirect()->to('login');
    }
});

Route::get('comisiones', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.comisiones');
    } else {
        return redirect()->to('login');
    }
});

Route::get('cuarentenas', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.ventayCuarentena');
    } else {
        return redirect()->to('login');
    }
});

Route::get('ranking', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.ranking');
    } else {
        return redirect()->to('login');
    }
});

Route::get('campanas', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.campanas');
    } else {
        return redirect()->to('login');
    }
});

Route::get('briefing', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.briefing');
    } else {
        return redirect()->to('login');
    }
});
#endregion

#region Cliente
Route::get('fichacliente/{rutcli?}', function ($rutcli='') {
    if (Session::get('auth')) {
        return view('back.pages.cliente.fichacliente', ["rutcli"=>$rutcli]);

    } else {
        return redirect()->to('login');
    }
});

Route::get('reporteCobranza/{rutcli?}', function ($rutcli='') {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.reporteCobranza', ["rutcli"=>$rutcli]);

    } else {
        return redirect()->to('login');
    }
});

Route::get('visitascliente', function () {
    if (Session::get('auth')) {
        return view('back.pages.cliente.visitascliente');
    } else {
        return redirect()->to('login');
    }
});

    #region FichaCliente
    Route::get('generacotizacion/{rutcli?}/{previouspage?}/{numcot?}', function($rutcli='', $previouspage='', $numcot=''){
		$derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        if (Session::get('auth')) {
            return view('back.pages.cliente.fichacliente.generacotizacion-v2', ["rutcli"=>$rutcli, "numcot"=>$numcot, "from_otra_coti"=>$numcot, "previouspage"=>$previouspage,
																				"derechos" => $derechos]);
        } else {
            return redirect()->to('login');
        }
    });

    Route::get('listacotizacion/{rutcli?}/{previouspage?}', function($rutcli='', $previouspage=''){
        $derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        if (Session::get('auth')) {
            return view('back.pages.cliente.fichacliente.listacotizacion', ["rutcli"=>$rutcli, "previouspage"=>$previouspage, "derechos" => $derechos]);
        } else {
            return redirect()->to('login');
        }
    });
    
    Route::get('editacotizacion/{rutcli?}/{numcot?}/{previouspage?}', function($rutcli='', $numcot='', $previouspage=''){
        $derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        if (Session::get('auth')) {
            return view('back.pages.cliente.fichacliente.generacotizacion-v2', ["rutcli"=>$rutcli, "numcot"=>$numcot, "previouspage"=>$previouspage, "derechos" => $derechos]);
        } else {
            return redirect()->to('login');
        }
    });
    
	Route::get('editanotaventa/{rutcli?}/{numnvt?}/{previouspage?}', function($rutcli='', $numnvt='', $previouspage=''){
        $derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        if (Session::get('auth')) {
            return view('back.pages.cliente.fichacliente.generacotizacion-v2', ["rutcli"=>$rutcli, "numnvt"=>$numnvt, "previouspage"=>$previouspage, "derechos" => $derechos]);
        } else {
            return redirect()->to('login');
        }
    });

	Route::get('autocoti/{rutcli?}/{previouspage?}/', function($rutcli='', $previouspage=''){
        $derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
        if (Session::get('auth')) {
			$derechos = MaUsuarioPDO::getDerechos(Session::get('apikey'));
            return view('back.pages.cliente.fichacliente.generacotizacion-v2', ["rutcli"=>$rutcli, "autocoti"=>$rutcli, "previouspage"=>$previouspage,
																				"derechos" => $derechos]);
        } else {
            return redirect()->to('login');
        }
    });

    
    #endregion

#endregion


#region Controllers

#region PortadaController
Route::post('getventaejecutivo', 'PortadaController@getVentaEjecutivo');
//Route::post('getventacartera', 'PortadaController@getVentaCartera');
Route::post('getmetaejecutivo', 'PortadaController@getMetaEjecutivo');
Route::post('getrankingejecutivo', 'PortadaController@getRankingEjecutivo');
Route::post('getportada', 'PortadaController@getMetaMensualEjecutivo');
Route::post('pricing/ObtieneDatos', 'PortadaController@ObtieneDatos'); 
Route::post('pricing/bitacora', 'PortadaController@registro');
Route::post('pricing/GestionMes', 'PortadaController@GestionMes');

//Route::post('manual', 'PortadaController@getmanual');
Route::get('/manual', function () {
    return Response::download(storage_path() . "/Manual.pdf");
});

Route::post('pricing/portada', 'PortadaController@getPotencial');
Route::post('pricing/getventacartera', 'PortadaController@getVentaCartera');
Route::post('pricing/resumen', 'PortadaController@getResumenCartera');
Route::post('pricing/resumenTotales', 'PortadaController@getResumenTotales');
Route::post('pricing/meta', 'PortadaController@getMeta');
Route::post('pricing/dinero', 'PortadaController@getDinero');
#endregion

#region CarteraController
Route::post('getcartera', 'CarteraController@getCarteraEjecutivo');
#endregion

Route::post('getfocosCartera', 'CarteraController@getfocosCartera'); 
Route::post('pricing/Focos', 'CarteraController@getfocosKarim');
Route::post('pricing/getFechasAcciones', 'CarteraController@getFechasAcciones'); 
Route::post('pricing/acciones', 'CarteraController@getAcciones');
Route::post('pricing/accionesQ', 'CarteraController@getAccionesQ');
Route::post('pricing/cartera', 'CarteraController@getCartera'); 
Route::post('pricing/getDocumentos', 'CarteraController@getDocumentosRelacionados');
Route::post('getRut', 'CarteraController@getRut');

Route::get('notas/{valor?}', function ($valor='') {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.NotasDeVentas.notas', ["valor"=>$valor]);
    } else {
        return redirect()->to('login');
    }
});

Route::get('notasPedientes/{valor?}', function ($valor='') {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.NotasDeVentas.notasSinDocumentar', ["valor"=>$valor]);
    } else {
        return redirect()->to('login');
    }
});

Route::get('cartera/{valor?}', function ($valor='') {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.cartera', ["valor"=>$valor]);

    } else {
        return redirect()->to('login');
    }
});

#region RankingController
Route::post('getrankinggrupo', 'RankingController@getRankingGrupo');
#endregion

#region IndicadoresController
Route::post('indicadores/estadisticasDia', 'indicadoresController@getEstadisticasDia');
Route::post('indicadores/estadisticasDiaTotales', 'indicadoresController@getEstadisticasDiaTotales');
//
Route::post('indicadores/estadisticasMes', 'indicadoresController@getEstadisticasMes');
Route::post('indicadores/estadisticasMesTotales', 'indicadoresController@getEstadisticasMesTotales');
//
Route::post('indicadores/carteraMes', 'indicadoresController@getCarteraMes');
Route::post('indicadores/carteraMesTotales', 'indicadoresController@getCarteraMesTotales');
//
Route::post('indicadores/seguimientoNota', 'indicadoresController@getseguimientoNota');
Route::post('indicadores/seguimientoNotaTotales', 'indicadoresController@getseguimientoNotaTotales');

Route::post('indicadores/dashBoard', 'indicadoresController@getIndicadores');
#end region

#region FichaClienteController
Route::get('EnviaCartolaCorp', 'FichaClienteController@EnviarCartolaCorp');
Route::post('pricing/reporteCobranza', 'FichaClienteController@getReporteCobranza');
Route::post('pricing/reporteCobranzaDatos', 'FichaClienteController@getReporteCobranzaDatos');
Route::post('getdetalleclienteoracle', 'FichaClienteController@getDetalleFichaClienteOracle');
Route::post('getDatosCliente', 'FichaClienteController@getDatosCliente'); 
Route::post('getGestiones', 'FichaClienteController@getGestiones');
Route::post('validaConvenio', 'FichaClienteController@validaConvenio');
Route::post('observacionCliente', 'FichaClienteController@observacionCliente');
Route::post('grabaObserv', 'FichaClienteController@grabaObserv');
Route::post('getContactos', 'FichaClienteController@getContactos'); 
Route::post('getRelaciones', 'FichaClienteController@getRelaciones'); 
Route::post('getfocosCliente', 'FichaClienteController@getfocosCliente');
Route::post('getdetalleclientemysql', 'FichaClienteController@getDetalleFichaClienteMysql');
Route::post('getcontactoanotaciones/{rutcli?}', 'FichaClienteController@getContactosAnotaciones');
Route::post('guardaranotaciones/{rutcli?}','FichaClienteController@saveAnotaciones');
Route::post('getgraficoficha', 'FichaClienteController@getGraficoFicha');
Route::get('descargaConvenio/{rutcli}', 'FichaClienteController@descargaConvenio');
Route::post('getCredito', 'FichaClienteController@getCredito');
Route::post('getCredito2', 'FichaClienteController@getCredito2');
Route::post('getCencos', 'FichaClienteController@getCentrosCostos');
Route::post('getCobrador', 'FichaClienteController@getCobrador');
Route::post('pricing/cotizaciones', 'CotizacionController@getCotizaciones'); 
#endregion

#region CotizacionController
Route::get('cotizacionpdf', function(){
    if(Session::get('auth')){
        return view('back.pages.cliente.fichacliente.cotizacionpdf');
    } else {
        return redirect()->to('login');
    }
});

Route::post('generarpdfcotizacion', 'CotizacionController@generarPdfCotizacion');
Route::post('getMails', 'CotizacionController@getContactosAnotaciones');
Route::post('generarpdfcotizacionandsave', 'CotizacionController@generarPdfCotizacionAndSave');
Route::post('pricing/ingresosWeb', 'CotizacionController@ingresosWeb');
Route::post('pricing/comisionesCuadro1', 'CotizacionController@getComisiones1');
Route::post('pricing/comisionesCuadro2', 'CotizacionController@getComisiones2');
Route::post('pricing/comisionPortal', 'CotizacionController@getComisionesPortal');
Route::post('pricing/getPeriodosComisiones', 'CotizacionController@getPeriodosComisiones');
Route::post('pricing/cotizaciones', 'CotizacionController@getCotizaciones'); 
Route::post('pricing/cotizacionesRut', 'CotizacionController@getCotizacionesRut');
Route::post('pricing/notas', 'CotizacionController@getNotas');
Route::post('pricing/notasRut', 'CotizacionController@getNotasRut');
Route::post('pricing/getNotasWeb', 'CotizacionController@getNotasWeb');
Route::post('pricing/getNotasSinDocumentar', 'CotizacionController@getNotasSinDocumentar');
Route::post('pricing/getNotasSinDocumentarQ', 'CotizacionController@getNotasSinDocumentarQ');
Route::post('pricing/getNotasWebQ', 'CotizacionController@getNotasWebQ');
Route::post('pricing/getNotasWebHist', 'CotizacionController@getNotasWebHist');
Route::post('pricing/ingresosWebQ', 'CotizacionController@ingresosWebQ');
Route::post('pricing/EliminaNotaWeb', 'CotizacionController@EliminaNotaWeb');
Route::post('getStatusNota', 'CotizacionController@getStatusNota');
Route::post('getInfoNota1', 'CotizacionController@getInfoEstadoNota1');
Route::post('getInfoNota2', 'CotizacionController@getInfoEstadoNota2');
Route::post('getInfoNota3', 'CotizacionController@getInfoEstadoNota3');
Route::post('getInfoNota4', 'CotizacionController@getInfoEstadoNota4');
Route::post('getInfoEstadoNota', 'CotizacionController@ConsultaApi');
Route::post('ventaCovidComuna', 'CotizacionController@getCovidComuna');

Route::get('exportpdf/{numcoti}', function($numcoti){

    $pdf = PDF::loadView('back.pages.cliente.fichacliente.cotizacionpdf');
    return $pdf->download('cotizacion_'.$numcoti.'.pdf');
});

Route::get('download-pdf/{numcoti}', 'CotizacionController@descargaPdfCotizacion');
Route::get('download-pdf-simple/{numcot}', 'CotizacionController@getPdfCotizacionSimple');
Route::get('download-excel-simple/{numcot}', 'CotizacionController@getExcelCotizacionSimple');

Route::get('guardarpdf/{numcoti}', function($numcoti){
    $pdf = PDF::loadView('back.pages.cliente.fichacliente.cotizacionpdf');
    $nombrearchivo = 'cotizacion_'.$numcoti.'.pdf';
    $r1 = Storage::disk('pdf')->put($nombrearchivo, $pdf);
    $pdf->save(storage_path().'/archivos/pdf/'.$nombrearchivo);

    $rutaarchivo = storage_path('archivos/pdf/');
    return $rutaarchivo;
});

Route::post('getlistadocotizaciones', 'CotizacionController@getListadoCotizaciones');

Route::get('downloadtemplate', function(){
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'codigo/sku');
    $sheet->setCellValue('B1', 'precio');
    $sheet->setCellValue('C1', 'cantidad');

    $writer = new Xlsx($spreadsheet);

    $response =  new StreamedResponse(
        function () use ($writer) {
            $writer->save('php://output');
        }
    );
    $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $response->headers->set('Content-Disposition', 'attachment;filename="plantilla_cotizacion.xlsx"');
    $response->headers->set('Cache-Control','max-age=0');
    return $response;
});

Route::post('importexcelcotizacion', 'CotizacionController@importExcelCotizacion');
Route::post('sendmailcotizacion', 'CotizacionController@sendMailCotizacion');
Route::post('actualizar-mail', 'CotizacionController@updateMail');
#endregion

#region CampañasController
Route::post('getcampanavtames', 'CampanasController@getCampanaVtaMes');
Route::post('getcampanaclientecartera', 'CampanasController@getCampanaClienteCartera');
#endregion

//region GestionesBackController
Route::post('getDerechos', 'GestionesBackController@getDerechos');
//Route::post('descarga/comisiones', 'GestionesBackController@descargaComisiones');
Route::get('descarga/comisiones/{fecha}', 'GestionesBackController@descargaComisiones');
Route::post('decode', 'GestionesBackController@decode');
Route::post('subirImagen', 'GestionesBackController@subirImagen');
Route::post('ReporteProductividad', 'GestionesBackController@getReporteProductividad');
Route::post('sendmailcotizacion', 'CotizacionController@sendMailCotizacion');
Route::post('pricing/cambioClave', 'GestionesBackController@CambioClave');
#endregion

//region GobiernoController
Route::get('GobiernoDatos', 'GobiernoController@getDatosReportesCorp'); 

Route::get('Gobierno', function () {
    if (Session::get('auth')) {
        return view('back.pages.ejecutivo.ReportesGobierno.ReporteG1');
    } else {
        return redirect()->to('login');
    }
});

#endregion

// Route::get('GobiernoPrueba', function(){
//     if(Session::get('auth')){
//         return view('back.pages.FormatoPDF.Cartola'
//         , [
//             'datos1' => [
//                 ['linea' => 'TISSUE',
//                 'visits' => 6000000],
//                 ['linea' => 'CAFETERÍA',
//                 'visits' => 6000000],
//                 ['linea' => 'TECNOLOGÍA',
//                 'visits' => 6000000],
//                 ['linea' => 'SEGURIDAD',
//                 'visits' => 6000000],
//                 ['linea' => 'ASEO',
//                 'visits' => 6000000],
//                 ['linea' => 'PAPEL FOTOCOPIA',
//                 'visits' => 6000000],
//                 ['linea' => 'LIBRERÍA',
//                 'visits' => 6000000]
//             ]
//         ]);
//     } else {
//         return redirect()->to('login');
//     }
// });
