<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 22/06/2018
 * Time: 10:21
 */

namespace App\PDO\MySql\MarketingDigital;

use App\Entities\MySql\Cerberus\MdmMigracionGrupo;
use App\Entities\MySql\Cerberus\MdmMigracionSupervisor;
use App\Entities\MySql\Cerberus\MdmMigracionUsuario;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class MdmMigracionWebPDO extends Model
{
    public static function getMonthlyReportByUser($in_month, $in_year = 2018, $in_userid)
    {
        $sql = 'SELECT supervisor, grupo_venta, rut, ejecutivo, '
            . ' mes, anio, n_tlmk, n_web, n_nc,'
            . ' p_tlmk, p_web, '
            . ' intervalo_ventas, intervalo_pweb'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number '
            . ' AND anio = :year_number '
            . ' AND ejecutivo = :user_id ';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
            'user_id' => $in_userid
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getMonthlyReportBySalesGroup($in_month, $in_year = 2018, $in_codcnl)
    {
        $sql = 'SELECT supervisor, grupo_venta, rut, ejecutivo, '
            . ' mes, anio, n_tlmk, n_web, n_nc,'
            . ' p_tlmk, p_web, '
            . ' intervalo_ventas, intervalo_pweb'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number '
            . ' AND anio = :year_number '
            . ' AND grupo_venta = :canal_code ';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
            'canal_code' => $in_codcnl
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getMonthlyReport($in_month, $in_year = 2018) {
        $sql = 'SELECT supervisor, grupo_venta, rut, ejecutivo, '
            . ' mes, anio, n_tlmk, n_web, n_nc,'
            . ' p_tlmk, p_web, '
            . ' intervalo_ventas, intervalo_pweb'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number '
            . ' AND anio = :year_number ';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getSalesGroupMonthlyReport($in_month, $in_year = 2018)
    {
        $sql = 'SELECT supervisor, grupo_venta, mes, anio,'
            . ' sum(n_tlmk) as n_tlmk, sum(n_web) as n_web'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number '
            . ' AND anio = :year_number '
            . ' GROUP BY supervisor, grupo_venta, mes, anio';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnGrupoMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getSalesGroupMonthlyReportBySalesGroup($in_month, $in_year = 2018, $in_codcnl)
    {
        $sql = 'SELECT supervisor, grupo_venta, mes, anio,'
            . ' sum(n_tlmk) as n_tlmk, sum(n_web) as n_web'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number'
            . ' AND anio = :year_number '
            . ' AND grupo_venta = :sales_group'
            . ' GROUP BY supervisor, grupo_venta, mes, anio';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
            'sales_group' => $in_codcnl
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnGrupoMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getSpecificSupervisorMonthlyReport($in_month, $in_year = 2018, $in_superuserid)
    {
        $sql = 'SELECT supervisor, mes, anio,'
            . ' sum(n_tlmk) as n_tlmk, sum(n_web) as n_web'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number'
            . ' AND anio = :year_number '
            . ' AND supervisor = :supervisor_id'
            . ' GROUP BY supervisor, mes, anio';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
            'supervisor_id' => $in_superuserid
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnSupervisorMigracionFormat($resultado);
        return $arrayReturn;
    }

    public static function getSupervisorsMonthlyReport($in_month, $in_year = 2018)
    {
        $sql = 'SELECT supervisor, mes, anio,'
            . ' sum(n_tlmk) as n_tlmk, sum(n_web) as n_web'
            . ' FROM mdm_migracion_web'
            . ' WHERE mes = :month_number'
            . ' AND anio = :year_number '
            . ' GROUP BY supervisor, mes, anio';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'month_number' => $in_month,
            'year_number' => $in_year,
        ]);

        $arrayReturn = MdmMigracionWebPDO::returnSupervisorMigracionFormat($resultado);
        return $arrayReturn;
    }

    /* NUEVOS REPORTES  */
    public static function obtainPreviousMonthlyMajorReportByUser($in_userid, $in_currentmonth = null, $in_currentyear = null)
    {
        // Si no viene especificado, traer mes actual por defecto
        if (!$in_currentmonth) {
            $in_currentmonth = Carbon::now()->month;
            $in_currentyear = Carbon::now()->year;
        }

        // Traer último mes con mayor intervalo de ventas, y con mayor intervalo de participacion web
        $sql = "SELECT mes, anio, (n_tlmk + n_web - n_nc) as venta_total, p_web, intervalo_ventas, intervalo_pweb "
            . " FROM mdm_migracion_web "
            . " WHERE ejecutivo = :ejecutivo "
            . " AND CONCAT(anio, '-', mes) <> :filtro_fecha "
            . " AND CURRENT_DATE() > DATE(CONCAT(anio, '-', mes, '-', '01'))"
            . " ORDER BY intervalo_ventas desc, intervalo_pweb desc "
            . " LIMIT 1";

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'ejecutivo' => $in_userid,
            'filtro_fecha' => $in_currentyear . '-' . $in_currentmonth
        ]);

        $arrayReturn = null;
        if (count($resultado) > 0) {
            $arrayReturn = array(
                'mes' => (int)$resultado[0]->mes,
                'anio' => (int)$resultado[0]->anio,
                'p_web' => (int)$resultado[0]->p_web,
                'n_total' => (int)$resultado[0]->venta_total,
                'intervalo_ventas' => (int)$resultado[0]->intervalo_ventas,
                'intervalo_pweb' => (int)$resultado[0]->intervalo_pweb
            );
        } else {
            $arrayReturn = array(
                'mes' => 0,
                'anio' => 0,
                'venta_total' => 0,
                'p_web' => 0,
                'intervalo_ventas' => 9,
                'intervalo_pweb' => 9
            );
        }

        return $arrayReturn;
    }

    /*Reportes finales*/
    public static function obtainAllDataCampaing($in_valor = '', $in_filtro = 'ejecutivo')
    {
        $sql = 'SELECT '
            . ' (SELECT (jul.n_tlmk + jul.n_web - jul.n_nc) as suma	FROM mdm_migracion_web jul WHERE jul.mes = 07 AND jul.anio = 2018 AND jul.ejecutivo = a.ejecutivo) AS N_JUL,'
            . ' (SELECT (ago.n_tlmk + ago.n_web - ago.n_nc) as suma	FROM mdm_migracion_web ago WHERE ago.mes = 08 AND ago.anio = 2018 AND ago.ejecutivo = a.ejecutivo) AS N_AGO,'
            . ' (SELECT (sep.n_tlmk + sep.n_web - sep.n_nc) as suma	FROM mdm_migracion_web sep WHERE sep.mes = 09 AND sep.anio = 2018 AND sep.ejecutivo = a.ejecutivo) AS N_SEP,'
            . ' (SELECT (oct.n_tlmk + oct.n_web - oct.n_nc) as suma	FROM mdm_migracion_web oct WHERE oct.mes = 10 AND oct.anio = 2018 AND oct.ejecutivo = a.ejecutivo) AS N_OCT,'
            . ' (SELECT (nov.n_tlmk + nov.n_web - nov.n_nc) as suma	FROM mdm_migracion_web nov WHERE nov.mes = 11 AND nov.anio = 2018 AND nov.ejecutivo = a.ejecutivo) AS N_NOV,'
            . ' (SELECT (dic.n_tlmk + dic.n_web - dic.n_nc) as suma	FROM mdm_migracion_web dic WHERE dic.mes = 12 AND dic.anio = 2018 AND dic.ejecutivo = a.ejecutivo) AS N_DIC,'

            . ' (SELECT AVG(jul.p_web) as promedio		FROM mdm_migracion_web jul WHERE jul.mes = 07 AND jul.anio = 2018 AND jul.ejecutivo = a.ejecutivo) AS P_JUL,'
            . ' (SELECT AVG(ago.p_web) as promedio		FROM mdm_migracion_web ago WHERE ago.mes = 08 AND ago.anio = 2018 AND ago.ejecutivo = a.ejecutivo) AS P_AGO,'
            . ' (SELECT AVG(sep.p_web) as promedio		FROM mdm_migracion_web sep WHERE sep.mes = 09 AND sep.anio = 2018 AND sep.ejecutivo = a.ejecutivo) AS P_SEP,'
            . ' (SELECT AVG(oct.p_web) as promedio		FROM mdm_migracion_web oct WHERE oct.mes = 10 AND oct.anio = 2018 AND oct.ejecutivo = a.ejecutivo) AS P_OCT,'
            . ' (SELECT AVG(nov.p_web) as promedio		FROM mdm_migracion_web nov WHERE nov.mes = 11 AND nov.anio = 2018 AND nov.ejecutivo = a.ejecutivo) AS P_NOV,'
            . ' (SELECT AVG(dic.p_web) as promedio		FROM mdm_migracion_web dic WHERE dic.mes = 12 AND dic.anio = 2018 AND dic.ejecutivo = a.ejecutivo) AS P_DIC,'

            . ' (SELECT jul.intervalo_ventas as int_v	FROM mdm_migracion_web jul WHERE jul.mes = 07 AND jul.anio = 2018 AND jul.ejecutivo = a.ejecutivo) AS IV_JUL,'
            . ' (SELECT ago.intervalo_ventas as int_v	FROM mdm_migracion_web ago WHERE ago.mes = 08 AND ago.anio = 2018 AND ago.ejecutivo = a.ejecutivo) AS IV_AGO,'
            . ' (SELECT sep.intervalo_ventas as int_v	FROM mdm_migracion_web sep WHERE sep.mes = 09 AND sep.anio = 2018 AND sep.ejecutivo = a.ejecutivo) AS IV_SEP,'
            . ' (SELECT oct.intervalo_ventas as int_v	FROM mdm_migracion_web oct WHERE oct.mes = 10 AND oct.anio = 2018 AND oct.ejecutivo = a.ejecutivo) AS IV_OCT,'
            . ' (SELECT nov.intervalo_ventas as int_v	FROM mdm_migracion_web nov WHERE nov.mes = 11 AND nov.anio = 2018 AND nov.ejecutivo = a.ejecutivo) AS IV_NOV,'
            . ' (SELECT dic.intervalo_ventas as int_v	FROM mdm_migracion_web dic WHERE dic.mes = 12 AND dic.anio = 2018 AND dic.ejecutivo = a.ejecutivo) AS IV_DIC,'

            . ' (SELECT jul.intervalo_pweb as int_p     FROM mdm_migracion_web jul WHERE jul.mes = 07 AND jul.anio = 2018 AND jul.ejecutivo = a.ejecutivo) AS IP_JUL,'
            . ' (SELECT ago.intervalo_pweb as int_p     FROM mdm_migracion_web ago WHERE ago.mes = 08 AND ago.anio = 2018 AND ago.ejecutivo = a.ejecutivo) AS IP_AGO,'
            . ' (SELECT sep.intervalo_pweb as int_p     FROM mdm_migracion_web sep WHERE sep.mes = 09 AND sep.anio = 2018 AND sep.ejecutivo = a.ejecutivo) AS IP_SEP,'
            . ' (SELECT oct.intervalo_pweb as int_p     FROM mdm_migracion_web oct WHERE oct.mes = 10 AND oct.anio = 2018 AND oct.ejecutivo = a.ejecutivo) AS IP_OCT,'
            . ' (SELECT nov.intervalo_pweb as int_p     FROM mdm_migracion_web nov WHERE nov.mes = 11 AND nov.anio = 2018 AND nov.ejecutivo = a.ejecutivo) AS IP_NOV,'
            . ' (SELECT dic.intervalo_pweb as int_p     FROM mdm_migracion_web dic WHERE dic.mes = 12 AND dic.anio = 2018 AND dic.ejecutivo = a.ejecutivo) AS IP_DIC,'

            . ' a.ejecutivo'
            . ' FROM mdm_migracion_web a ';
        switch ($in_filtro) {
            case 'ejecutivo':
                $sql .= ' WHERE ejecutivo = :valor_filtro';
                break;
            case 'grupo-venta':
                $sql .= ' WHERE grupo_venta = :valor_filtro';
                break;
            case 'supervisor':
                $sql .= ' WHERE supervisor = :valor_filtro';
                break;
            default:
                break;

        }
        $sql .= ' GROUP BY a.ejecutivo';

        $resultado = DB::connection('mysql_cerberus_marketingdigital')->select($sql, [
            'valor_filtro' => $in_valor
        ]);

        if($resultado == null || count($resultado) <= 0) {
            return null;
        }

        /*foreach ($resultado as $key => $value) {
            $resultado[$key];
        }*/

        return $resultado;
    }

    public static function calculoBonoPorIntervalo($anterior_intervaloventas, $anterior_intervalopweb, $actual_intervaloventas, $actual_intervalopweb, $mes_actual = null, $anio_actual = null)
    {
        // Inicializar meses y años
        $mes_actual = ($mes_actual == null) ? Carbon::now()->month : (int)$mes_actual;
        $anio_actual = ($anio_actual == null) ? Carbon::now()->year : (int)$anio_actual;
        $bono_final = 0;

        // Preguntar si se mantiene intervalo de ventas (o aumenta),
        // Y además preguntar si el intervalo de participación web es mayor al anterior
        if ($actual_intervaloventas >= $anterior_intervaloventas && $actual_intervalopweb > $anterior_intervalopweb) {
            // Preguntar, según el intervalo de ventas, cuanto aumentó el intervalo de participación web
            switch ($actual_intervaloventas) {
                case 1: // Intervalo de ventas N°01 (< 10M)
                    if ($actual_intervalopweb >= 6 && $anterior_intervalopweb < 6) {
                        $bono_final += 25000;
                    }
                    if ($actual_intervalopweb >= 5 && $anterior_intervalopweb < 5) {
                        $bono_final += 20000;
                    }
                    if ($actual_intervalopweb >= 4 && $anterior_intervalopweb < 4) {
                        $bono_final += 20000;
                    }
                    if ($actual_intervalopweb >= 3 && $anterior_intervalopweb < 3) {
                        $bono_final += 20000;
                    }
                    break;
                case 2: // Intervalo de ventas N°02 (10M - 25M)
                    if ($actual_intervalopweb >= 6 && $anterior_intervalopweb < 6) {
                        $bono_final += 65000;
                    }
                    if ($actual_intervalopweb >= 5 && $anterior_intervalopweb < 5) {
                        $bono_final += 50000;
                    }
                    if ($actual_intervalopweb >= 4 && $anterior_intervalopweb < 4) {
                        $bono_final += 50000;
                    }
                    if ($actual_intervalopweb >= 3 && $anterior_intervalopweb < 3) {
                        $bono_final += 50000;
                    }
                    break;
                case 3: // Intervalo de ventas N°03 (25M - 50M)
                    if ($actual_intervalopweb >= 6) {
                        $bono_final += 95000;
                    }
                    if ($actual_intervalopweb >= 5 && $anterior_intervalopweb < 5) {
                        $bono_final += 60000;
                    }
                    if ($actual_intervalopweb >= 4 && $anterior_intervalopweb < 4) {
                        $bono_final += 60000;
                    }
                    if ($actual_intervalopweb >= 3 && $anterior_intervalopweb < 3) {
                        $bono_final += 60000;
                    }
                    break;
                case 4: // Intervalo de ventas N°04 (> 50M)
                    if ($actual_intervalopweb >= 6) {
                        $bono_final += 105000;
                    }
                    if ($actual_intervalopweb >= 5 && $anterior_intervalopweb < 5) {
                        $bono_final += 70000;
                    }
                    if ($actual_intervalopweb >= 4 && $anterior_intervalopweb < 4) {
                        $bono_final += 70000;
                    }
                    if ($actual_intervalopweb >= 3 && $anterior_intervalopweb < 3) {
                        $bono_final += 70000;
                    }
                    break;
                default:
                    $bono_final += 0;
            }
        }
        // Si el ejecutivo mantiene el intervalo de ventas en [6], en comparación al mes anterior,
        // Y además el intervalo de ventas se mantiene o aumenta, se da bono.
        // IMPORTANTE: Solo tendrá bono en los meses de Agosto/2018 Y Diciembre/2018
        else if ($anio_actual == 2018 && $anterior_intervalopweb == 6 && $actual_intervalopweb == 6 && $actual_intervaloventas >= $anterior_intervaloventas) {
            switch ($mes_actual) {
                case 8:
                    /******* AGOSTO/2018 *********/
                    switch ($actual_intervaloventas) {
                        case 2:
                            $bono_final += 50000;
                            break;
                        case 3:
                            $bono_final += 60000;
                            break;
                        case 4:
                            $bono_final += 70000;
                            break;
                        default:
                            $bono_final += 0;
                            break;
                    }
                    break;
                case 12:
                    /******* DICIEMBRE/2018 *********/
                    switch ($actual_intervaloventas) {
                        case 2:
                            $bono_final += 65000;
                            break;
                        case 3:
                            $bono_final += 95000;
                            break;
                        case 4:
                            $bono_final += 105000;
                            break;
                        default:
                            $bono_final += 0;
                            break;
                    }
                    break;
                default:
                    $bono_final += 0;
                    break;
            }
        }

        return $bono_final;
    }

    /* Clases mutadoras */
    private static function returnMigracionFormat($registros)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $migracion = new MdmMigracionUsuario($registro);
            $arrayReturn[] = $migracion;
        }

        return $arrayReturn;
    }

    private static function returnGrupoMigracionFormat($registros)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $migracion = new MdmMigracionGrupo($registro);
            $arrayReturn[] = $migracion;
        }

        return $arrayReturn;
    }

    private static function returnSupervisorMigracionFormat($registros)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $migracion = new MdmMigracionSupervisor($registro);
            $arrayReturn[] = $migracion;
        }

        return $arrayReturn;
    }
}
