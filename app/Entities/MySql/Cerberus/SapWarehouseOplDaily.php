<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 28/09/2018
 * Time: 9:38
 */


namespace App\Entities\MySql\Cerberus;

class SapWarehouseOplDaily implements \JsonSerializable
{
    private $id;
    private $sap_material;
    private $codpro;
    private $sap_centro;
    private $sap_almacen;
    private $stock;
    private $stock_compr;
    private $costo_prom;
    private $fecha_registro;

    public function __construct($registro = null)
    {
        if($registro != null) {
            $this->setId($registro->id);
            $this->setSapMaterial($registro->sap_material);
            $this->setCodpro($registro->codpro);
            $this->setSapCentro($registro->sap_centro);
            $this->setSapAlmacen($registro->sap_almacen);
            $this->setStock($registro->stock);
            $this->setStockCompr($registro->stock_compr);
            $this->setCostoProm($registro->costo_prom);
            $this->setFechaRegistro($registro->fecha_registro);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getSapMaterial()
    {
        return $this->sap_material;
    }

    public function setSapMaterial($sap_material)
    {
        $this->sap_material = $sap_material;
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getSapCentro()
    {
        return $this->sap_centro;
    }

    public function setSapCentro($sap_centro)
    {
        $this->sap_centro = $sap_centro;
    }

    public function getSapAlmacen()
    {
        return $this->sap_almacen;
    }

    public function setSapAlmacen($sap_almacen)
    {
        $this->sap_almacen = $sap_almacen;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    public function getStockCompr()
    {
        return $this->stock_compr;
    }

    public function setStockCompr($stock_compr)
    {
        $this->stock_compr = $stock_compr;
    }

    public function getCostoProm()
    {
        return $this->costo_prom;
    }

    public function setCostoProm($costo_prom)
    {
        $this->costo_prom = $costo_prom;
    }

    public function getFechaRegistro()
    {
        return $this->fecha_registro;
    }

    public function setFechaRegistro($fecha_registro)
    {
        $this->fecha_registro = $fecha_registro;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'sap_material' => $this->getSapMaterial(),
            'codpro' => $this->getCodpro(),
            'sap_centro' => $this->getSapCentro(),
            'sap_almacen' => $this->getSapAlmacen(),
            'stock' => $this->getStock(),
            'stock_compr' => $this->getStockCompr(),
            'costo_prom' => $this->getCostoProm(),
            'fecha_registro' => $this->getFechaRegistro(),
        ];
    }
}