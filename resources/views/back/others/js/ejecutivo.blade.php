<script>
    var meses = [];
    meses['01'] ='Enero';
    meses['02'] = 'Febrero';
    meses['03'] = 'Marzo';
    meses['04'] = 'Abril';
    meses['05'] = 'Mayo';
    meses['06'] = 'Junio';
    meses['07'] = 'Julio';
    meses['08'] = 'Agosto';
    meses['09'] = 'Septiembre';
    meses['10'] = 'Octubre';
    meses['11'] = 'Noviembre';
    meses['12'] = 'Diciembre';

    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    // /* inicio:Portada */
    // function getVentaEjecutivo() {
    //     var token = '{{ csrf_token() }}';
    //     var parametros = {
    //         _token: token
    //     };
    //     $.ajax({
    //         url: '{{ url('getventaejecutivo') }}',
    //         data: parametros,
    //         type: 'POST',
    //         cache: false,
    //         datatype: 'json',
    //         async: true,
    //         success: function (json) {
    //             if (json.code == 200) {
    //                 //console.log(json.response);
    //                 makeResumenVtaEjecutivo(json.response[0]);
    //             }
    //         }
    //     });
    // }

    // function makeResumenVtaEjecutivo(obj) {
    //     var pxAnt='ej_ant_';
    //     var pxAct='ej_act_';

    //     //Año anterior
    //     var mesAntEne = '$ ' + formatMoney(obj.ENE_1, 0, ',', '.');
    //     var mesAntFeb = '$ ' + formatMoney(obj.FEB_1, 0, ',', '.');
    //     var mesAntMar = '$ ' + formatMoney(obj.MAR_1, 0, ',', '.');
    //     var mesAntAbr = '$ ' + formatMoney(obj.ABR_1, 0, ',', '.');
    //     var mesAntMay = '$ ' + formatMoney(obj.MAY_1, 0, ',', '.');
    //     var mesAntJun = '$ ' + formatMoney(obj.JUN_1, 0, ',', '.');
    //     var mesAntJul = '$ ' + formatMoney(obj.JUL_1, 0, ',', '.');
    //     var mesAntAgo = '$ ' + formatMoney(obj.AGO_1, 0, ',', '.');
    //     var mesAntSep = '$ ' + formatMoney(obj.SEP_1, 0, ',', '.');
    //     var mesAntOct = '$ ' + formatMoney(obj.OCT_1, 0, ',', '.');
    //     var mesAntNov = '$ ' + formatMoney(obj.NOV_1, 0, ',', '.');
    //     var mesAntDic = '$ ' + formatMoney(obj.DIC_1, 0, ',', '.');
    //     document.getElementsByName(pxAnt + '1')[0].innerHTML = mesAntEne;
    //     document.getElementsByName(pxAnt + '2')[0].innerHTML = mesAntFeb;
    //     document.getElementsByName(pxAnt + '3')[0].innerHTML = mesAntMar;
    //     document.getElementsByName(pxAnt + '4')[0].innerHTML = mesAntAbr;
    //     document.getElementsByName(pxAnt + '5')[0].innerHTML = mesAntMay;
    //     document.getElementsByName(pxAnt + '6')[0].innerHTML = mesAntJun;
    //     document.getElementsByName(pxAnt + '7')[0].innerHTML = mesAntJul;
    //     document.getElementsByName(pxAnt + '8')[0].innerHTML = mesAntAgo;
    //     document.getElementsByName(pxAnt + '9')[0].innerHTML = mesAntSep;
    //     document.getElementsByName(pxAnt + '10')[0].innerHTML = mesAntOct;
    //     document.getElementsByName(pxAnt + '11')[0].innerHTML = mesAntNov;
    //     document.getElementsByName(pxAnt + '12')[0].innerHTML = mesAntDic;

    //     //Año actual
    //     var mesActEne = '$ ' + formatMoney(obj.ENE, 0, ',', '.');
    //     var mesActFeb = '$ ' + formatMoney(obj.FEB, 0, ',', '.');
    //     var mesActMar = '$ ' + formatMoney(obj.MAR, 0, ',', '.');
    //     var mesActAbr = '$ ' + formatMoney(obj.ABR, 0, ',', '.');
    //     var mesActMay = '$ ' + formatMoney(obj.MAY, 0, ',', '.');
    //     var mesActJun = '$ ' + formatMoney(obj.JUN, 0, ',', '.');
    //     var mesActJul = '$ ' + formatMoney(obj.JUL, 0, ',', '.');
    //     var mesActAgo = '$ ' + formatMoney(obj.AGO, 0, ',', '.');
    //     var mesActSep = '$ ' + formatMoney(obj.SEP, 0, ',', '.');
    //     var mesActOct = '$ ' + formatMoney(obj.OCT, 0, ',', '.');
    //     var mesActNov = '$ ' + formatMoney(obj.NOV, 0, ',', '.');
    //     var mesActDic = '$ ' + formatMoney(obj.DIC, 0, ',', '.');
    //     document.getElementsByName(pxAct + '1')[0].innerHTML = mesActEne;
    //     document.getElementsByName(pxAct + '2')[0].innerHTML = mesActFeb;
    //     document.getElementsByName(pxAct + '3')[0].innerHTML = mesActMar;
    //     document.getElementsByName(pxAct + '4')[0].innerHTML = mesActAbr;
    //     document.getElementsByName(pxAct + '5')[0].innerHTML = mesActMay;
    //     document.getElementsByName(pxAct + '6')[0].innerHTML = mesActJun;
    //     document.getElementsByName(pxAct + '7')[0].innerHTML = mesActJul;
    //     document.getElementsByName(pxAct + '8')[0].innerHTML = mesActAgo;
    //     document.getElementsByName(pxAct + '9')[0].innerHTML = mesActSep;
    //     document.getElementsByName(pxAct + '10')[0].innerHTML = mesActOct;
    //     document.getElementsByName(pxAct + '11')[0].innerHTML = mesActNov;
    //     document.getElementsByName(pxAct + '12')[0].innerHTML = mesActDic;

    // }

    // function getVentaCartera() {
    //     var token = '{{ csrf_token() }}';
    //     var parametros = {
    //         _token: token
    //     };
    //     $.ajax({
    //         url: '{{ url('getventacartera') }}',
    //         data: parametros,
    //         type: 'POST',
    //         cache: false,
    //         datatype: 'json',
    //         async: true,
    //         success: function (json) {
    //             //console.log(json);
    //             if (json.code == 200) {
    //                 makeResumenVtaCartera(json.response[0]);
    //             }
    //         }
    //     });
    // }

    // function makeResumenVtaCartera(obj) {

    //     var pxAnt='ca_ant_';
    //     var pxAct='ca_act_';
        
    //     //Año anterior
    //     var mesAntEne = '$ ' + formatMoney(obj.ENE_1, 0, ',', '.');
    //     var mesAntFeb = '$ ' + formatMoney(obj.FEB_1, 0, ',', '.');
    //     var mesAntMar = '$ ' + formatMoney(obj.MAR_1, 0, ',', '.');
    //     var mesAntAbr = '$ ' + formatMoney(obj.ABR_1, 0, ',', '.');
    //     var mesAntMay = '$ ' + formatMoney(obj.MAY_1, 0, ',', '.');
    //     var mesAntJun = '$ ' + formatMoney(obj.JUN_1, 0, ',', '.');
    //     var mesAntJul = '$ ' + formatMoney(obj.JUL_1, 0, ',', '.');
    //     var mesAntAgo = '$ ' + formatMoney(obj.AGO_1, 0, ',', '.');
    //     var mesAntSep = '$ ' + formatMoney(obj.SEP_1, 0, ',', '.');
    //     var mesAntOct = '$ ' + formatMoney(obj.OCT_1, 0, ',', '.');
    //     var mesAntNov = '$ ' + formatMoney(obj.NOV_1, 0, ',', '.');
    //     var mesAntDic = '$ ' + formatMoney(obj.DIC_1, 0, ',', '.');
    //     document.getElementsByName(pxAnt + '1')[0].innerHTML = mesAntEne;
    //     document.getElementsByName(pxAnt + '2')[0].innerHTML = mesAntFeb;
    //     document.getElementsByName(pxAnt + '3')[0].innerHTML = mesAntMar;
    //     document.getElementsByName(pxAnt + '4')[0].innerHTML = mesAntAbr;
    //     document.getElementsByName(pxAnt + '5')[0].innerHTML = mesAntMay;
    //     document.getElementsByName(pxAnt + '6')[0].innerHTML = mesAntJun;
    //     document.getElementsByName(pxAnt + '7')[0].innerHTML = mesAntJul;
    //     document.getElementsByName(pxAnt + '8')[0].innerHTML = mesAntAgo;
    //     document.getElementsByName(pxAnt + '9')[0].innerHTML = mesAntSep;
    //     document.getElementsByName(pxAnt + '10')[0].innerHTML = mesAntOct;
    //     document.getElementsByName(pxAnt + '11')[0].innerHTML = mesAntNov;
    //     document.getElementsByName(pxAnt + '12')[0].innerHTML = mesAntDic;

    //     //Año actual
    //     var mesActEne = '$ ' + formatMoney(obj.ENE, 0, ',', '.');
    //     var mesActFeb = '$ ' + formatMoney(obj.FEB, 0, ',', '.');
    //     var mesActMar = '$ ' + formatMoney(obj.MAR, 0, ',', '.');
    //     var mesActAbr = '$ ' + formatMoney(obj.ABR, 0, ',', '.');
    //     var mesActMay = '$ ' + formatMoney(obj.MAY, 0, ',', '.');
    //     var mesActJun = '$ ' + formatMoney(obj.JUN, 0, ',', '.');
    //     var mesActJul = '$ ' + formatMoney(obj.JUL, 0, ',', '.');
    //     var mesActAgo = '$ ' + formatMoney(obj.AGO, 0, ',', '.');
    //     var mesActSep = '$ ' + formatMoney(obj.SEP, 0, ',', '.');
    //     var mesActOct = '$ ' + formatMoney(obj.OCT, 0, ',', '.');
    //     var mesActNov = '$ ' + formatMoney(obj.NOV, 0, ',', '.');
    //     var mesActDic = '$ ' + formatMoney(obj.DIC, 0, ',', '.');
    //     document.getElementsByName(pxAct + '1')[0].innerHTML = mesActEne;
    //     document.getElementsByName(pxAct + '2')[0].innerHTML = mesActFeb;
    //     document.getElementsByName(pxAct + '3')[0].innerHTML = mesActMar;
    //     document.getElementsByName(pxAct + '4')[0].innerHTML = mesActAbr;
    //     document.getElementsByName(pxAct + '5')[0].innerHTML = mesActMay;
    //     document.getElementsByName(pxAct + '6')[0].innerHTML = mesActJun;
    //     document.getElementsByName(pxAct + '7')[0].innerHTML = mesActJul;
    //     document.getElementsByName(pxAct + '8')[0].innerHTML = mesActAgo;
    //     document.getElementsByName(pxAct + '9')[0].innerHTML = mesActSep;
    //     document.getElementsByName(pxAct + '10')[0].innerHTML = mesActOct;
    //     document.getElementsByName(pxAct + '11')[0].innerHTML = mesActNov;
    //     document.getElementsByName(pxAct + '12')[0].innerHTML = mesActDic;

    // }

    function getMetaEjecutivo() {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token
        };
        $.ajax({
            url: '{{ url('/getmetaejecutivo') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {

                    var meta = formatMoney(json.response[0].meta, 0, ',', '.');
                    var mesActual = '{{ date('m') }}';  
                    mesActual = parseInt(mesActual);
                    var venta = document.getElementsByName('ej_act_' + mesActual)[0].innerHTML.substr(2, 20).trim();
                    console.log('meta: ' + meta);
                    var cumplimiento = meta==0?0:Math.round((parseInt(venta) * 100)/parseInt(meta));

                    document.getElementsByName('tx_meta')[0].innerHTML = meta;
                    document.getElementsByName('tx_venta')[0].innerHTML = venta;
                    document.getElementsByName('tx_cumplimiento')[0].innerHTML = cumplimiento;
                    document.getElementsByName('barra_cumplimiento')[0].style = "width:" + cumplimiento + "%";
                }
            }
        });
    }

    function getRankingEjecutivo() {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token
        };
        $.ajax({
            url: '{{ url('getrankingejecutivo') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    //console.log(json.response);
                    document.getElementsByName('tx_ranking')[0].innerHTML = json.response[0].ranking;
                    document.getElementsByName('tx_rnk_mes_anio')[0].innerHTML = meses[json.response[0].mes] + ' ' + json.response[0].anio;
                }
            }
        });
    }
    /* fin:Portada */

    /* inicio:Ranking */
    function getRankingGrupo() {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token
        };
        $.ajax({
            url: '{{ url('getrankinggrupo') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    console.log(json.response);
                    makeRankingGrupo(json.response);
                    //document.getElementsByName('tx_ranking')[0].innerHTML = json.response[0].ranking;
                    //document.getElementsByName('tx_rnk_mes_anio')[0].innerHTML = meses[json.response[0].mes] + ' ' + json.response[0].anio;
                }
            }
        });
    }

    function makeRankingGrupo(obj) {
        var grupo = obj[0].concepto;
        var mesanio = meses[obj[0].mes] + ' ' + obj[0].anio;
        var rankingejecutivo = '0';
        var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';

        var rows_table = '';
        var row = '';

        var td_ranking_p1 = '';
        var td_ranking_p2 = '';
        var class_tr = '';

        for(var o in obj)
        {
            if(parseInt(obj[o].ranking) > 0) {
                if (obj[o].ranking == '1') {
                    td_ranking_p1 = '<td class="table-success text-center middle"><i class="fa fa-hashtag m-r-5"></i><strong>';
                    td_ranking_p2 = '</strong><i class="ion-trophy text-warning m-l-5"></i></td>';
                } else {
                    td_ranking_p1 = '<td class="table-success text-center"><i class="fa fa-hashtag m-r-5"></i><strong>';
                    td_ranking_p2 = '</strong></td>';
                }

                if (obj[o].es_el == "1") {
                    class_tr = ' class="table-warning text-inverse"';
                    rankingejecutivo = obj[o].ranking;
                } else {
                    class_tr = '';
                }

                row = '<tr' + class_tr + '>\n' +
                    '      <th scope="row">' + obj[o].ejecutivo + '</th>\n' +
                    '      <td class="text-center">' + obj[o].equipo + '</td>\n' +
                    '      <td class="text-center">' + obj[o].factor + '</td>\n' +
                    td_ranking_p1 + obj[o].ranking + td_ranking_p2 +
                    '      <td class="text-center">' + obj[o].Rank_Cumplimiento_Meta + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Crecimiento + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Fidelizacion + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Penetracion + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Web + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Ticket_Nuevos + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Venta_Clientes_Nuevos + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Balance + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Part_Mer + '</td>\n' +
                    '      <td class="text-center">' + obj[o].Rank_Atendidos + '</td>\n' +
                    '  </tr>\n';

                rows_table = rows_table + row;
            }
        }

        document.getElementsByName('tb_ranking_tbody')[0].innerHTML = rows_table;

        //Resumen
        document.getElementsByName('tx_grupo_res')[0].innerHTML = grupo;
        document.getElementsByName('tx_mes_anio_res')[0].innerHTML = mesanio;
        document.getElementsByName('tx_ranking_res')[0].innerHTML = rankingejecutivo;
        document.getElementsByName('tx_userid_res')[0].innerHTML = ejecutivo;
    }
    /* fin:Ranking */

</script>
