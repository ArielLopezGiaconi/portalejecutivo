@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="ion-document-text text-inverse f-50"></i>
                        </div>
                        <div class="col-sm-6">
                            <h3>Cotizaciones</h3>
                            <span class="f-16">Detalle Cotizaciones | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li id="despliegue"><i id="icono" class="feather minimize-card icon-minus"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block" style="">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-3 form-group ">
                                    <div class="card-sub">
                                        <div class="card-block">
                                            <label>Selección de Cartera</label>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12"> 
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" checked id="cartera1" name="cartera" value="1">
                                                        <label class="form-check-label" for="cartera1">Solo mi Cartera</label>
                                                    </div>
                                                
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera2" name="cartera" value="2">
                                                        <label class="form-check-label" for="cartera2">Cartera Partners</label>
                                                    </div>

                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="cartera3" name="cartera" value="3">
                                                        <label class="form-check-label" for="cartera3">Ambas Carteras</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 form-group">
                                    <div class="row">
                                        <div class="col col-md-4">
                                            <label>Rut de Cliente</label>
                                            <input class="form-control" name="rut" id="rut" type='number' required/>    
                                        </div>
                                        <div class="col col-md-8">
                                            <br>
                                            <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar1()">Buscar Rut</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-8">
                                            <label>Razon Social</label>
                                            <input class="form-control" name="razons" id="razons" type='string' onkeyup="this.value = this.value.toUpperCase();" required/>  
                                        </div>
                                        <div class="col col-md-4">
                                            <br>
                                            <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar2()">Buscar Razon</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>

                </div>

                <div class="row" style="margin-left: 0px;">
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                        {{-- <input name="check" class="pull-right" id="check-fechaProximaAccion" type="checkbox"/> --}}
                        <label>Fecha desde</label>
                        <div class='input-group date' id='fecha'>
                            <input class="form-control" name="fechaDesde" id="fechaDesde" type='date'/>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                        {{-- <input name="check" class="pull-right" id="check-fechaProximaAccion" type="checkbox"/> --}}
                        <label>Fecha Hasta</label>
                        <div class='input-group date' id='fecha'>
                            <input class="form-control" name="fechaHasta" id="fechaHasta" type='date'/>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                        <label>Estado</label>
                        <select name="status" id="status" class="form-control" required>
                            <option value="Todas">Todas</option>
                            <option value="Activa">Activa</option>
                            <option value="Efectiva">Efectiva</option>
                            <option value="Vencida">Vencida</option>
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <br>
                        <button id="Filtrar" class="btn btn-danger pull-right" onclick="Buscar3()">Buscar por Fecha</button>
                    </div>
                </div>
            </div>
            <div class="card-block">
                <div class="modal fade" id="modal-send-email" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 id="tituloMail">Enviar cotización por email</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-send-email">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="card-block">
                                    <div class="form-group row">
                                        <p>Ingrese correo al que desea enviar la cotización seleccionada:</p>
                                        <label class="col-sm-5 col-form-label">Ingrese destinatario:</label>
                                        <div class="col-sm-7">
                                            <input list="lista" type="text" class="form-control form-control-sm form-bg-warning" style="width: 100%;" id="tx_correo_destinatario" placeholder="Correo electrónico" autocomplete="off" required>
                                            <datalist id="lista">
                                            </datalist>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Comentario opcional:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" cols="5" class="form-control" id="tx_comentario_ejecutivo" placeholder="Comentario opcional" autocomplete="off"></textarea>
                                            <span class="messages popover-valid"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-5"></label>
                                        <div class="col-sm-7">
                                            <input type="hidden" value="" id="h_numcot">
                                            <button type="button" class="btn btn-primary btn-mini m-b-0" onclick="generarArchivoAdjunto();">Enviar cotización</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="CC" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <h2 class="modal-title card-block text-center">(C.C) Centro de Costo</h2>
                            <div class="card-block text-center">
                                <h3 class="modal-title">Centro de Costo que realiza la cotización y compra</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ElSegmento" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <h2 class="modal-title card-block text-center">Segmento</h2>
                            <div class="card-block text-center">
                                <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <div id="detalle" onkeyup="Formato()"></div>
                    <div class="spinner" id="spinner" style="display: none;">
                        <center>
                            <div class="bolitas">
                                <div class="dot1"></div>
                                <div class="dot2"></div>
                            </div>
                            <div class="cargando">Cargando datos...</div>
                        </center>
                    </div>
                </div>
            </div>

@include('back.others.js.cotizac')
@include('back.others.js.listacotizaciones')
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> 
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "ion-document-text";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Cotizaciones";
            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            getDerechos();
            
            if (date.getMonth() == 0) {
                primerDia = new Date(date.getFullYear()-1, 11, 1);
            }else {
                primerDia = new Date(date.getFullYear(), date.getMonth()-1, 1);
            }

            document.getElementById("fechaDesde").value = primerDia.toISOString().substr(0, 10);
            document.getElementById("fechaHasta").value = ultimoDia.toISOString().substr(0, 10);

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            
            var value = '{{ $valor }}';

            if (value < 10) {
                if(value == '1'){
                    document.getElementById('status').value = "Activa";
                }else{
                    if(value == '2'){
                        document.getElementById('status').value = "Efectiva";
                    }else{
                        if(value == '3'){
                            document.getElementById('status').value = "Vencida";
                        }else{
                            document.getElementById('status').value = "Todas";
                        }
                    }
                }
                Buscar(0);
            }else {
                BuscarRut(value);
            }
           
            document.getElementById('icono').click();

            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        $('#cartera1').click(function(){
            Buscar3();
        });

        $('#cartera2').click(function(){
            Buscar3();
        });
        
        $('#cartera3').click(function(){
            Buscar3();
        });

        function Buscar1() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=29;
            var oldvalue='';
            var newvalue=document.getElementById("rut").value;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
        }

        function Buscar2() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=31;
            var oldvalue='';
            var newvalue=document.getElementById("razons").value;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            Buscar(2);
        }

        function Buscar3() {
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=32;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var value = '{{ $valor }}';
            if(value === ""){
                Buscar(0);
            }else{
                BuscarRut(value);
            }
        }

        function Orden(){
            setTimeout('document.getElementById("fecha_Creacion").click()',200);
            setTimeout('document.getElementById("fecha_Creacion").click()',200);
            setTimeout('document.getElementById("monto").click()',200);
            setTimeout('document.getElementById("monto").click()',200);
            Formato();
            // setTimeout('document.getElementById("vencimiento").click()',200);
            // setTimeout('document.getElementById("vencimiento").click()',200);
            setTimeout('document.getElementById("fecha_Creacion").click()',200);
            setTimeout('document.getElementById("fecha_Creacion").click()',200);
        }

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenotas");
            var rows = table.getElementsByTagName("tr");
            tr = table.getElementsByTagName("tr"); 

            if (rows.length > 2) {
                for (i = 1; i < rows.length; i++) {
                    td = tr[i].getElementsByTagName("td")[8];
                    $(td).addClass('text-right');
                    td = tr[i].getElementsByTagName("td")[9];
                    $(td).addClass('text-right');
                    td = tr[i].getElementsByTagName("td")[10];
                    $(td).addClass('text-right');
                    row = table.rows[i];
                    if (row.cells[8].innerHTML.substring(0,1) != '$'){
                        row.cells[8].innerHTML = '$' + number_format(row.cells[8].innerHTML,0);
                        row.cells[9].innerHTML = '$' + number_format(row.cells[9].innerHTML,0);
                        row.cells[10].innerHTML = '$' + number_format(row.cells[10].innerHTML,0);
                    }
                };
            }
        }

        function FichaCliente(){
            var table = document.getElementById("detallenotas");
            var rows = table.getElementsByTagName("tr");
            var value = '{{ $valor }}';
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=33;
            var oldvalue='';
            var newvalue='';
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                if (value > 0) {
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue=cells[4].innerHTML;
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        location.href = window.location.href.replace('/cotizaciones/' + value ,'') + "/fichacliente/" + cells[4].innerHTML;
                    };
                }else{
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue=cells[4].innerHTML;
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        location.href = window.location.href.replace('/cotizaciones' + value ,'') + "/fichacliente/" + cells[4].innerHTML;
                    };
                }
                
            };
        }
        
        function BuscarRut(elrut) {
            var table = '<table id="detallenotas" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th onclick="Formato()" >Acción</th>\n' +
                '                            <th onclick="Formato()" >N° Cotizacion</th>\n' +
                '                            <th onclick="Formato()" id="fecha_Creacion">Fecha Creación</th>\n' +
                '                            <th onclick="Formato()" id="vencimiento">Fecha Vencimiento</th>\n' +
                '                            <th onclick="Formato()" >Rut</th>\n' +
                '                            <th onclick="Formato()" >C.C.\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#CC"></i></th>\n' + 
                '                            <th onclick="Formato()" >Razon Social</th>\n' +
                '                            <th onclick="Formato()" >Segmento\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th>\n' + 
                '                            <th onclick="Formato()" id="monto">Monto Cotizado</th>\n' +
                '                            <th onclick="Formato()" >Iva</th>\n' +
                '                            <th onclick="Formato()" >Total</th>\n' +
                '                            <th onclick="Formato()" >Estado</th>\n' +
                '                            <th onclick="Formato()" >Ejecutivo</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var token = '{{ csrf_token() }}';
            var fechaDesdeV = document.getElementById("fechaDesde").value;
            var fechaHastaV = document.getElementById("fechaHasta").value;
            var statusV = document.getElementById("status").value;
            var parametros = {
                _token: token,
                rut: elrut,
                fechaDesde: fechaDesdeV,
                fechaHasta: fechaHastaV,
                status: statusV
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/cotizacionesRut')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenotas").DataTable({
                            data: json.response,
                            columns: [
                                {
                                    data: "cotizacion", render: function (data, type, row, meta) {
                                        var boton = '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Ficha Cliente" onclick="FichaCliente()"><span\n' +
                                                    '       class="icofont icofont-ui-user f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Exporta PDF" onclick="exportCotizacionPdfB('+ data +');"><span\n' +
                                                    '       class="fa fa-download f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-warning waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Enviar por correo" data-toggle="modal" data-target="#modal-send-email" onclick="LevantaContactos('+ data +');"><span\n' +
                                                    '       class="fa fa-envelope f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-delete-button btn btn-inverse waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Guardar o Editar cotización vigente" onclick="editarCotizac('+ data +');"><span\n' +
                                                    '       class="fa fa-pencil f-18"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-delete-button btn btn-primary waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Generar cotización a partir de ésta" onclick="generarCotizac('+ data +');"><span\n' +
                                                    '       class="fa fa-magic f-18"></span>\n' +
                                                    '   </button>\n' +
                                                    '</div>\n';
                                        return boton;
                                    },
                                    className: 'botones_cotizaciones'
                                },
                                {data: "cotizacion",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "fecha_emision",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "fecha_vencimiento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data.substr(0,10) + '</span>\n';}}, 
                                {data: "rut",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "razon_social",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "segmento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "monto_cotizado",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "iva",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "total",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "estado",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "ejecutivo",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            searching: true,
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });

                        $("#spinner").hide();
                        Orden();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    //console.log(e.message);
                }
            });
        }

        function exportCotizacionPdfB(data){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=34;
            var oldvalue='';
            var newvalue=data;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            exportCotizacionPdf(data);
        }

        function Buscar(tipoV) {
            var table = '<table id="detallenotas" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th onclick="Formato()" >Acción</th>\n' +
                '                            <th onclick="Formato()" >N° Cotizacion</th>\n' +
                '                            <th onclick="Formato()" id="fecha_Creacion">Fecha Creación</th>\n' +
                '                            <th onclick="Formato()" id="vencimiento">Fecha Vencimiento</th>\n' +
                '                            <th onclick="Formato()" >Rut</th>\n' +
                '                            <th onclick="Formato()" >C.C.\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#CC"></i></th>\n' + 
                '                            <th onclick="Formato()" >Razon Social</th>\n' +
                '                            <th onclick="Formato()" >Segmento\n' +
                '                               <i class="fa fa-info-circle" data-toggle="modal" data-target="#ElSegmento"></i></th>\n' + 
                '                            <th onclick="Formato()" id="monto">Monto Cotizado</th>\n' +
                '                            <th onclick="Formato()" >Iva</th>\n' +
                '                            <th onclick="Formato()" >Total</th>\n' +
                '                            <th onclick="Formato()" >Estado</th>\n' +
                '                            <th onclick="Formato()" >Ejecutivo</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;

            var token = '{{ csrf_token() }}';
            if (tipoV == 1){
                var busquedaV = document.getElementById("rut").value;    
            }else {
                if (tipoV == 2) {
                    var busquedaV = document.getElementById("razons").value;    
                }else {
                    var busquedaV = 'null';
                }
            }

            var fechaDesdeV = document.getElementById("fechaDesde").value;
            var fechaHastaV = document.getElementById("fechaHasta").value;
            var carteraV = $('input:radio[name=cartera]:checked').val();
            var statusV = document.getElementById("status").value;

            var parametros = {
                _token: token,
                busqueda: busquedaV,
                fechaDesde: fechaDesdeV,
                fechaHasta: fechaHastaV,
                cartera: carteraV,
                tipo: tipoV,
                status: statusV
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/pricing/cotizaciones')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json.response);
                    if (Object.keys(json).length > 0) {
                        var datatable = $("#detallenotas").DataTable({
                            data: json.response,
                            columns: [
                                {
                                    data: "numcot", render: function (data, type, row, meta) {
                                        var boton = '<div class="btn-group btn-group-customt" style="float: none;">\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Ficha Cliente" onclick="FichaCliente()"><span\n' +
                                                    '       class="icofont icofont-ui-user f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-success waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Exporta PDF" onclick="exportCotizacionPdfB('+ data +');"><span\n' +
                                                    '       class="fa fa-download f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-edit-button btn btn-warning waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Enviar por correo" data-toggle="modal" data-target="#modal-send-email" onclick="LevantaContactos('+ data +');"><span\n' +
                                                    '       class="fa fa-envelope f-16"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-delete-button btn btn-inverse waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Guardar o Editar cotización vigente" onclick="editarCotizac('+ data +');"><span\n' +
                                                    '       class="fa fa-pencil f-18"></span>\n' +
                                                    '   </button>\n' +
                                                    '   <button type="button"\n' +
                                                    '       class="tabledit-delete-button btn btn-primary waves-effect waves-light"\n' +
                                                    '       style="float: none;margin: 0px;width:25px!important;height:25px!important;"\n' +
                                                    '       title="Generar cotización a partir de ésta" onclick="generarCotizac('+ data +');"><span\n' +
                                                    '       class="fa fa-magic f-18"></span>\n' +
                                                    '   </button>\n' +
                                                    '</div>\n';
                                        return boton;
                                    },
                                    className: 'botones_cotizaciones'
                                },
                                {data: "numcot",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "fecemi",
                                    render: function (data, type, row, meta) {
                                    return data;}}, 
                                {data: "fecven",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "rutcli",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "cencos",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-right">' + data + '</span>\n';}}, 
                                {data: "razons",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "segmento",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "monto_cotizado",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "iva",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "total",
                                    render: function (data, type, row, meta) {
                                    return data;}},
                                {data: "estado",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}, 
                                {data: "ejecutivo",
                                    render: function (data, type, row, meta) {
                                    return '<span class="pull-left">' + data + '</span>\n';}}
                            ],
                            "scrollX": true,
                            "scrollY": 400,
                            "iDisplayLength": 100,
                            "ColumnsDef": [
                                { "sWidth": "300px", "aTargets": [ 9 ] }
                            ],
                            "fixedHeader": true,
                            dom: 'Bfrtip',
                            searching: true,
                            buttons: [
                                // { extend:'excelHtml5', text: 'Descargar tabla'}
                            ]
                            ,
                            language: {
                                buttons: {
                                    copyTitle: 'Copiado en el portapapeles',
                                    copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                    copySuccess: {
                                        _: '%d filas copiadas al portapapeles',
                                        1: '1 fila copiada al portapapeles'
                                    }
                                },
                                "lengthMenu": "Mostrando _MENU_ registros por página",
                                "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                "infoFiltered": "(filtrado de _MAX_ registros)",
                                "search": "Buscar:",
                                "paginate": {
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                        });

                        $("#spinner").hide();
                        Orden();
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                }
            });
        }

        function editarCotizac(numcot){
            var table = document.getElementById("detallenotas");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=36;
            var oldvalue='';
            var newvalue=numcot;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                var value = '{{ $valor }}';
                //console.log(value);
                if (value > 0) {
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue = cells[1].innerHTML
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        //location.href = window.location.href.replace('/notas/' + value ,'') + "/editanotaventa/" + cells[4].innerHTML + "/" + cells[1].innerHTML;
                        location.href = "{{ url('editacotizacion') }}/" + cells[4].innerHTML +'/'+ numcot + '/listacotizaciones';
                    };
                }else{
                    row.onclick = function(){
                        var cells = this.getElementsByTagName("td");
                        newvalue = cells[1].innerHTML;
                        registro(userid, moduleid, actionid, oldvalue, newvalue);
                        //location.href = window.location.href.replace('/notas' + value ,'') + "/editanotaventa/" + cells[4].innerHTML + "/" + cells[1].innerHTML;
                        location.href = "{{ url('editacotizacion') }}/" + cells[4].innerHTML +'/'+ numcot + '/listacotizaciones';
                    };
                }
            };
        }
        
        function generarCotizac(numcot){
            var table = document.getElementById("detallenotas");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=37;
            var oldvalue='';
            var newvalue=numcot;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                generarCotizacion(numcot, cells[4].innerHTML);
                            };
            };
        }

        function LevantaContactos(numcot){
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=15;
            var actionid=35;
            var oldvalue='';
            var newvalue='';
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            var table = document.getElementById("detallenotas");
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                                var cells = this.getElementsByTagName("td");
                                Contactos(cells[4].innerHTML);
                                invocaSendMailCotizacion(numcot);
                            };
            };
        }

        function Contactos(elrut) {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token,
                rutcli: elrut
            };
            $.ajax({
                data: parametros,
                url: '{{url('/getMails')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        var text = "";
                        for (i = 0; i < json.response.length; i++) {
                            text += '<option value="' + json.response[i].contacto + '">' + json.response[i].contacto + ' - C.C ' + json.response[i].cencos + '</option>'; 
                        }
                        document.getElementById("lista").innerHTML = text;
                        document.getElementById("tituloMail").innerText = "Enviar cotización por email al rut: " + elrut;
                        document.getElementById("tx_correo_destinatario").value = "";
                    } else {
                        alert("Sin información");
                        $("#spinner").hide();
                    }
                }
                , error: function (e) {
                    //console.log(e.message);
                }
            });
        }

    </script>
@endsection