<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 22/06/2018
 * Time: 10:33
 */

namespace App\Entities\MySql\Cerberus;

use Carbon\Carbon;


class MdmMigracionUsuario implements \JsonSerializable
{
    private $supervisor;
    private $grupo_venta;
    private $rut;
    private $ejecutivo;
    private $mes;
    private $anio;
    private $n_tlmk;
    private $n_web;
    private $n_nc;
    private $p_tlmk;
    private $p_web;
    private $intervalo_ventas;
    private $intervalo_pweb;

    public function __construct($registro = null)
    {
        if ($registro != null) {
            $this->setSupervisor($registro->supervisor);
            $this->setGrupoVenta($registro->grupo_venta);
            $this->setRut($registro->rut);
            $this->setEjecutivo($registro->ejecutivo);
            $this->setMes($registro->mes);
            $this->setAnio($registro->anio);
            $this->setNTlmk($registro->n_tlmk);
            $this->setNWeb($registro->n_web);
            $this->setNNc($registro->n_nc);
            $this->setPTlmk($registro->p_tlmk);
            $this->setPWeb($registro->p_web);
            $this->setIntervaloVentas($registro->intervalo_ventas);
            $this->setIntervaloPweb($registro->intervalo_pweb);
        }
    }

    public function getSupervisor()
    {
        return $this->supervisor;
    }

    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;
    }

    public function getGrupoVenta()
    {
        return $this->grupo_venta;
    }

    public function setGrupoVenta($grupo_venta)
    {
        $this->grupo_venta = $grupo_venta;
    }

    public function getRut()
    {
        return $this->rut;
    }

    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    public function getEjecutivo()
    {
        return $this->ejecutivo;
    }

    public function setEjecutivo($ejecutivo)
    {
        $this->ejecutivo = $ejecutivo;
    }

    public function getMes()
    {
        return (int)$this->mes;
    }

    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    public function getAnio()
    {
        return (int)$this->anio;
    }

    public function setAnio($anio)
    {
        $this->anio = $anio;
    }

    public function getNTlmk()
    {
        return (int)$this->n_tlmk;
    }

    public function setNTlmk($n_tlmk)
    {
        $this->n_tlmk = $n_tlmk;
    }

    public function getNWeb()
    {
        return (int)$this->n_web;
    }

    public function setNWeb($n_web)
    {
        $this->n_web = $n_web;
    }

    public function getNNc()
    {
        return (int)$this->n_nc;
    }

    public function setNNc($n_nc)
    {
        $this->n_nc = $n_nc;
    }

    public function getPTlmk()
    {
        $porc = (int)$this->p_tlmk;
        return ($porc <= 0) ? 0 : ($porc >= 100) ? 100 : $porc;
    }

    public function setPTlmk($p_tlmk)
    {
        $this->p_tlmk = $p_tlmk;
    }

    public function getPWeb()
    {
        $porc = (int)$this->p_web;
        return ($porc <= 0) ? 0 : ($porc >= 100) ? 100 : $porc;
    }

    public function setPWeb($p_web)
    {
        $this->p_web = $p_web;
    }

    public function getIntervaloVentas()
    {
        return (int)$this->intervalo_ventas;
    }

    public function setIntervaloVentas($intervalo_ventas)
    {
        $this->intervalo_ventas = $intervalo_ventas;
    }

    public function getIntervaloPweb()
    {
        return (int)$this->intervalo_pweb;
    }

    public function setIntervaloPweb($intervalo_pweb)
    {
        $this->intervalo_pweb = $intervalo_pweb;
    }

    public function getTotalVentas()
    {
        return (int)$this->n_tlmk + (int)$this->n_web - (int)$this->n_nc;
    }

    public function getProyeccionMonto() {
        $total = $this->getTotalVentas();
        return $this->calculoProyeccion($total);
    }

    public function getProyeccionIntervaloVentas()
    {
        $proyeccion = $this->getProyeccionMonto();
        /*return ($proyeccion > 50000000) ?
            4 :
            ($proyeccion <= 50000000 && $proyeccion > 25000000) ?
                3 :
                ($proyeccion <= 25000000 && $proyeccion > 10000000) ?
                    2 : 1;*/
        if ($proyeccion == null || $proyeccion <= 10000000) {
            return 1;
        }
        else if($proyeccion <= 25000000 && $proyeccion > 10000000) {
            return 2;
        }
        else if($proyeccion <= 50000000 && $proyeccion > 25000000) {
            return 3;
        }
        else if($proyeccion > 50000000) {
            return 4;
        }

        return 0;
    }

    public function jsonSerialize()
    {
        return [
            /*'supervisor' => $this->getSupervisor(),
            'grupo_venta' => $this->getGrupoVenta(),
            'rut' => $this->getRut(),
            'ejecutivo' => $this->getEjecutivo(),*/

            'mes' => $this->getMes(),
            'anio' => $this->getAnio(),

            'n_tlmk' => $this->getNTlmk(),
            'n_web' => $this->getNWeb(),
            'n_nc' => $this->getNNc(),

            'p_tlmk' => (int)$this->getPTlmk(),
            'p_web' => (int)$this->getPWeb(),

            'intervalo_ventas' => $this->getIntervaloVentas(),
            'intervalo_pweb' => $this->getIntervaloPweb(),

            'n_total' => $this->getTotalVentas(),
            'n_total_proyeccion' => $this->getProyeccionMonto(),
            'intervalo_ventas_proyeccion' => $this->getProyeccionIntervaloVentas(),

            'cantidad_dias_habiles' => $this->diasHabilesTotal(Carbon::now()),
            'cantidad_dias_habiles_progreso' => $this->diasHabilesProgreso(Carbon::now()),
            'cantidad_dias_habiles_restantes' => $this->diasHabilesTotal(Carbon::now()) - $this->diasHabilesProgreso(Carbon::now()),
        ];
    }

    private function calculoProyeccion($actual_monto)
    {
        // 1.- Obtener inicio mes, fin de mes, fecha actual
        $hoy = Carbon::now();

        // 2.- Calcular la cantidad de días hábiles en el mes
        $dias_habiles_mes_total = $this->diasHabilesTotal($hoy);

        // 3.- Calcular la cantidad de días hábiles pasados hasta el momento
        $dias_habiles_mes_progreso = $this->diasHabilesProgreso($hoy);

        // 4.- Retornar monto proyección
        return intval(round($actual_monto * $dias_habiles_mes_total / $dias_habiles_mes_progreso, 0));
    }

    private function diasHabilesTotal(Carbon $fecha_actual)
    {
        $mes_inicio = Carbon::create($fecha_actual->year, $fecha_actual->month, 1);
        $mes_fin = Carbon::create($fecha_actual->year, $fecha_actual->month, $fecha_actual->daysInMonth)->addDay();
        $diff =  $mes_inicio->diffInDaysFiltered(function (Carbon $date) {
            return !$date->isWeekend();
        }, $mes_fin);

        return $diff;
    }

    private function diasHabilesProgreso(Carbon $fecha_actual) {
        $mes_inicio = Carbon::create($fecha_actual->year, $fecha_actual->month, 1);
        $diff =  $mes_inicio->diffInDaysFiltered(function (Carbon $date) {
            return !$date->isWeekend();
        }, $fecha_actual->addDay());

        return $diff;
    }
}
