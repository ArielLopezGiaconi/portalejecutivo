<script>
    var meses = [];
    meses['01'] = 'Enero';
    meses['02'] = 'Febrero';
    meses['03'] = 'Marzo';
    meses['04'] = 'Abril';
    meses['05'] = 'Mayo';
    meses['06'] = 'Junio';
    meses['07'] = 'Julio';
    meses['08'] = 'Agosto';
    meses['09'] = 'Septiembre';
    meses['10'] = 'Octubre';
    meses['11'] = 'Noviembre';
    meses['12'] = 'Diciembre';

    var ejecutivo = '{{ Session::get('usuario')->get('userid') }}';
    var moduleid_global = 6;

    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    function redirecturl(id) {
        switch (id) {
            case 'btn_gencoti':
                // var rutcli = $('#tx_rut_cliente').val();

                var rutcli = '{{ $rutcli }}';
                invocaRegistroBitacora('generarcotizacion');
                window.location = "{{ url('generacotizacion') }}/" + rutcli + "/fichacliente";

                break;
            case 'btn_listcoti':

                var rutcli = '{{ $rutcli }}';
                invocaRegistroBitacora('vercotizaciones');
                window.location = "{{ url('listacotizacion') }}/" + rutcli + "/fichacliente";

                break;
            case 'btn_fichacliente':
                window.location = "{{ url('fichacliente') }}";
                break;

        }
    }

    /* inicio:Ficha */
    function buscafichacliente() {
        var rutcli = $('#tx_rut_cliente').val();
        compruebaExisteRut(rutcli);
    }
    
    function compruebaExisteRut(rutcli) {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            url: '{{ url('getdetalleclienteoracle') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {

                    window.location = "{{ url('fichacliente') }}/" + rutcli;
                }
                else{
                    $('#tx_rut_cliente').val('');

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json;

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut - buscador', code, text);
            }
        });
    }
    function returnLastRut(){
        $('#btn_buscar_cliente').click();
        document.getElementById('fr_lk_rut').className = 'nav-link';
        document.getElementById('fr_lk_razons').className = 'nav-link active';
        document.getElementById('find-rut').className='tab-pane';
        document.getElementById('find-razons').className='tab-pane active';
    }
    function buscaclienterazons(){
        $('#btn_buscar_cliente').click();
        $('#btn_find_razons').click();
        findClientForRazons();

    }
    function findClientForRazons(){
        var valor_buscado = document.getElementById('tx_razons_cliente').value;
        valor_buscado = valor_buscado.toString().toUpperCase();

        var parametros={
            vendedor: ejecutivo,
            razon_social: valor_buscado
        };

        $.ajax({
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
            },
            data: parametros,
            url: '{{ env("URL_API") }}/api/clientes/buscar-rsocial',
            type: 'POST',
            // cache: true,
            datatype: 'json',
            // beforeSend: function () {
            //     document.getElementById("pre_loader").className = "loader animation-start";
            // },
            success: function (json) {

                if (json.code == 200) {
                    //console.log(json);
                    var data = json.response;
                    makeDataTableClientes(data);

                } else {

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json.response;
                    // console.log(json);

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                }
                // document.getElementById("pre_loader").className = "loader animation-start d-none";
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'buscar cliente por razon social', code, text);
            }
        })
    }
    function makeDataTableClientes(data){

        if(data!=null) {
            document.getElementById('fr_btn_ok').className = 'btn btn-primary btn-mini waves-effect hidden';
            document.getElementById('fr_btn_close').className = 'btn btn-default btn-mini waves-effect';
            document.getElementById('fr_btn_select').className = 'btn btn-primary btn-mini waves-effect waves-light';

            var rows = '<tbody>\n';
            data.forEach(function (cli) {

                rows = rows + '<tr>\n' +
                    '                                                                            <td><input type="radio" name="radio_rutcli" value="' + cli.rutcli + '"></td>\n' +
                    '                                                                            <td>' + cli.rutcli + '</td>\n' +
                    '                                                                            <td>' + cli.razons + '</td>\n' +
                    '                                                                        </tr>\n';
            });
            rows = rows + '</tbody>\n';
            // console.log(rows);

            var content = '<table id="tb_clientes_razons" class="table table-xs fixed_header f-12" >\n' +
                '                                                                    <thead>\n' +
                '                                                                    <tr>\n' +
                '                                                                        <th>Seleccionar</th>\n' +
                '                                                                        <th>Rut</th>\n' +
                '                                                                        <th>Razón social</th>\n' +
                '                                                                    </tr>\n' +
                '                                                                    </thead>\n' +
                rows +
                '                                                                </table>';

            document.getElementById('div_tb_clientes_razons').innerHTML = content;
        }
        else{
            document.getElementById('fr_btn_ok').className = 'btn btn-primary btn-mini waves-effect';
            document.getElementById('fr_btn_close').className = 'btn btn-default btn-mini waves-effect hidden';
            document.getElementById('fr_btn_select').className = 'btn btn-primary btn-mini waves-effect waves-light hidden';

            var mensaje = '<p>La búsqueda no arrojó ningún resultado.</p>';
            document.getElementById('div_tb_clientes_razons').innerHTML = mensaje;
        }
    }
    function selectClientFromTableResult(){
        // var table = document.getElementById('tb_clientes_razons');
        var radios = document.getElementsByName('radio_rutcli');
        // var inputs = table.getElementsByTagName('input');
        var rut_selected='';
        radios.forEach(function(ra){
            var checked = ra.checked;
            if(checked){
                rut_selected = ra.value;
            }

        });

        window.location = "{{ url('fichacliente') }}/" + rut_selected;
    }
    
    function initializeFichaCliente(rutcli) {
        getInfoClienteOracle(rutcli);
    }

    function getInfoClienteOracle(rutcli) {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            url: '{{ url('getdetalleclienteoracle') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    //makeFichaCliente('oracle', json.response[0], 0);
                    getInfoClienteMysql(json.response[0].potencial, rutcli);
                }
                else{
                    var btns_acciones = document.getElementsByName('btns_acciones');
                    btns_acciones.forEach(function(btn){
                        btn.className = "btn btn-disabled btn-grd-inverse btn-sm btn-round btn-block disabled";
                    });

                    var nFrom = "top";
                    var nAlign = "center";
                    var nIcons = "fa fa-times";
                    var nType = "danger";
                    var nAnimIn = "animated bounceInRight";
                    var nAnimOut = "animated bounceOutRight";
                    var title = "";
                    var message = json;

                    notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);

                    returnLastRut();
                }
            },
            error: function(e){
                var code = e.status;
                var text = e.statusText;
                registroError(ejecutivo, moduleid_global, 'obtener info cliente oracle por rut', code, text);
            }
        });
    }

    function getInfoClienteMysql(p, rutcli) {
        var token = '{{ csrf_token() }}';
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            url: '{{ url('getdetalleclientemysql') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    //makeFichaCliente('mysql', json.response[0], p);
                }
            }
        });
    }

    function makeFichaCliente(tipo_db, obj, p) {
        //console.log(obj);

        if (tipo_db == 'oracle') {
            var razonsocial_complete = obj.razons;
            var razonsocial_short = razonsocial_complete.substring(0, 34);

            /* Datos resumen */
            //document.getElementById('tx_name_client').innerHTML = razonsocial_short;
            document.getElementById('tx_name_client').title = razonsocial_complete;
            document.getElementById('tx_name_udn').innerHTML = obj.negocio;
            document.getElementById('tx_potencial').innerHTML = '$' + formatMoney(obj.potencial, 0, ',', '.');

            /* Variables paneles */
            var cl_rutempresa = obj.rutcli + '-' + obj.digcli;
            var cl_negocio = obj.negocio;
            var cl_segmento = obj.segmento;
            var cl_relacion = obj.relacion;
            var cl_tipo_contribuyente = '-';
            var cl_tramo_facturacion = '-';
            var cl_q_trabajadores = '-';
            var cl_giro_dimerc = '-';

            var ac_ejecutivo = obj.nombre.toLocaleLowerCase() + ' ' + obj.apepat.toLocaleLowerCase();
            var ac_equipo = obj.equipo;
            var ac_dias_cartera = obj.dias_cartera;

            var ic_observacion = obj.observacion;
            var ic_estado = obj.estado;
            var ic_forma_pago = obj.pago;
            var ic_linea_credito = formatMoney(obj.linea, 0, ',', '.');
            var ic_disponible = formatMoney(obj.disponible, 0, ',', '.');

            /* PANELES */
            /* Información cliente */
            var pnl_info_cliente = '<div class="card social-network" style="width:600px;">' +
                '<div class="card-block">' +
                '<div class="row">' +
                '<div class="col-sm-5">' +
                '<div class="row">' +
                '<div class="col-5">' +
                '<p class="text-muted m-b-5">Rut :</p>' +
                '</div>' +
                '<div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_rut">' + cl_rutempresa + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-5"><p class="text-muted m-b-5">Negocio :</p>' +
                '</div>' +
                '<div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_udn">' + cl_negocio + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-5"><p class="text-muted m-b-5">Segmento :</p>' +
                '</div>' +
                '<div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_segmento">' + cl_segmento + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-5"><p class="text-muted m-b-5">Relación :</p>' +
                '</div>' +
                '<div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_relacion">' + cl_relacion + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-7">' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Tipo contribuyente (SII) :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_tipocont">' + cl_tipo_contribuyente + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Tramo facturación (SII) :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_tramo">' + cl_tramo_facturacion + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row"><div class="col-6"><p class="text-muted m-b-5">Q Trabajadores (SII) :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_q">' + cl_q_trabajadores + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Giro Dimerc :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_giro">' + cl_giro_dimerc + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            /* Administra la cuenta */
            var pnl_admin_cuenta = '<div class="card social-network" style="width:300px;">' +
                '<div class="card-block">' +
                '<div class="row">' +
                '<div class="col-sm-12">' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Ejecutivo :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" style="text-transform:capitalize;" id="tb_admincuenta_ejecutivo">' + ac_ejecutivo + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Equipo :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_admincuenta_equipo">' + ac_equipo + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Días en cartera :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_admincuenta_dias_cartera">' + ac_dias_cartera + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            /* Información credito */
            var pnl_info_credito = '<div class="card social-network" style="width:300px;">' +
                '<div class="card-block">' +
                '<div class="row">' +
                '<div class="col-sm-12">' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Observación :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infocred_observacion">' + ic_observacion + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Estado :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infocred_estado">' + ic_estado + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Forma de pago :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infocred_formapago">' + ic_forma_pago + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Línea de crédito :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infocred_linea">$' + ic_linea_credito + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6"><p class="text-muted m-b-5">Disponible :</p>' +
                '</div>' +
                '<div class="col-6"><p class="m-b-5 f-w-400" id="tb_infocred_disponible">$' + ic_disponible + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            document.getElementById('btn_info_cliente').setAttribute("data-content", pnl_info_cliente);
            document.getElementById('btn_admin_cuenta').setAttribute("data-content", pnl_admin_cuenta);
            document.getElementById('btn_info_credito').setAttribute("data-content", pnl_info_credito);
            
        } else if (tipo_db == 'mysql') {

            var prom_vta = obj.prom_vta;
            var prom_cont = obj.prom_cont;
            var prom_margen = (prom_cont / prom_vta) * 100;

            /* Datos resumen */
            document.getElementById('tx_venta').innerHTML = '$' + formatMoney(prom_vta, 0, ',', '.');
            document.getElementById('tx_margen').innerHTML = prom_margen.toFixed(2).toString().replace('.', ',') + ' %';

            var brecha = p - prom_vta;
            document.getElementById('tx_pot_brecha').innerHTML = formatMoney(brecha, 0, ',', '.');
        }
    }

    /* fin:Ficha */

    /* inicio:GeneraCotizacion */
    function initializeGeneraCotizacion(rutcli) {
        getInfoClienteOracleForCotizacion(rutcli);
        //makeTablaCotizacion();

    }

    function pantallaCompleta(vista) {
        if (vista == 'fichacliente') {
                    {{--var sc = '{{ Session::get('searchclient') }}';--}}
            var sc = '{{ $rutcli }}';

            // console.log(sc);

            if (sc == '') {
                $('#btn_buscar_cliente').click();
                document.getElementById('p_body').className = 'd-none';
                document.getElementById('name_client').innerHTML = 'No ha seleccionado ningún cliente';
            } else {
                $('#mobile-collapse').click();
                document.getElementById('p_body').className = 'd-block';
                initializeFichaCliente(sc);
                console.log($('#potencial').val());
            }
        }
        
        if (vista == 'generacoti') {
            $('#mobile-collapse').click();
            $('#mobile-collapse').click(function () {
                var boton = document.getElementById('mobile-collapse');
                var icono = boton.getElementsByTagName('i')[0];

                if (icono.className == 'feather icon-menu icon-toggle-right') {
                    document.getElementById('tbl_cotizacion').setAttribute('style', "height: 260px; overflow: hidden; width: 1050px;");
                    document.getElementById('tbl_cotizacion').setAttribute('data-originalstyle', "height: 260px; overflow: hidden; width: 1050px;");
                    document.getElementsByClassName('wtHolder')[0].setAttribute('style', "position: relative; height: 260px; width: 1050px;");
                    document.getElementsByClassName('wtHider')[0].setAttribute('style', "width: 1030px; height: 533px;");
                } else if (icono.className == 'feather icon-menu icon-toggle-left') {
                    document.getElementById('tbl_cotizacion').setAttribute('style', "height: 260px; overflow: hidden; width: 1300px;");
                    document.getElementById('tbl_cotizacion').setAttribute('data-originalstyle', "height: 260px; overflow: hidden; width: 1300px;");
                    document.getElementsByClassName('wtHolder')[0].setAttribute('style', "position: relative; height: 260px; width: 1300px;");
                    document.getElementsByClassName('wtHider')[0].setAttribute('style', "width: 1283px; height: 533px;");
                }
            });
            document.getElementById('hot-display-license-info').style = "display: none;";
        }
    }

    function getInfoClienteOracleForCotizacion(rutcli) {
        var token = '{{ csrf_token() }}';
        {{--var rutcli = '{{ $rutcli }}';--}}

        console.log('rutcli: ' + rutcli);
        var parametros = {
            _token: token,
            rutcli: rutcli
        };
        $.ajax({
            url: '{{ url('getdetalleclienteoracle') }}',
            data: parametros,
            type: 'POST',
            cache: false,
            datatype: 'json',
            async: true,
            success: function (json) {
                if (json.code == 200) {
                    // console.log('imprimir: '+json);
                    makeInfoClienteCotizacion('oracle', json.response[0]);
                }
            }
        });
    }

    function makeInfoClienteCotizacion(tipo_db, obj) {
        //console.log(obj);
        if (tipo_db == 'oracle') {
            document.getElementById('tx_nameclient').innerHTML = obj.razons;

            /* Variables paneles */
            var cl_rutempresa = obj.rutcli + '-' + obj.digcli;
            var cl_negocio = obj.negocio;
            var cl_segmento = obj.segmento;
            var cl_relacion = obj.relacion;
            var cl_centro_costo = '-';
            var cl_forma_pago = '-';
            var cl_plazo_pago = '-';
            var cl_estado_cliente = '-';

            var datatitle = '<span>Información cliente</span>';

            var datacontent = '<div class="row" style="width:600px;">\n' +
                '                <div class="col-sm-7">\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Rut :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_rut">' + cl_rutempresa + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Negocio :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_udn">' + cl_negocio + '</p>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Segmento :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_segmento">' + cl_segmento + '</p>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-5"><p class="text-muted m-b-5">Relación :</p></div>\n' +
                '                        <div class="col-7"><p class="m-b-5 f-w-400" id="tb_infoclient_relacion">' + cl_relacion + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-sm-5">\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Centro costo :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_tipocont">' + cl_centro_costo + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Forma pago :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_tramo">' + cl_forma_pago + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Plazo pago :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_q">' + cl_plazo_pago + '</p></div>\n' +
                '                    </div>\n' +
                '                    <div class="row">\n' +
                '                        <div class="col-6"><p class="text-muted m-b-5">Estado cliente :</p></div>\n' +
                '                        <div class="col-6"><p class="m-b-5 f-w-400" id="tb_infoclient_giro">' + cl_estado_cliente + '</p></div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            document.getElementById('btn_infoclient').setAttribute('data-original-title', datatitle);
            document.getElementById('btn_infoclient').setAttribute('data-content', datacontent);
        }
    }

    function makeTablaCotizacion() {
        var dataObject = [
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            },
            {
                "Codigo": "",
                "Descripcion": "",
                "Marca": "",
                "Precio": "",
                "Cantidad": "",
                "Total": "",
                "Margen": "",
                "Comision": "",
                "Condicion": "",
                "Stock": ""
            }
        ];
        //var hotElementContainer = hotElement.parentNode;

        var configuraciones = {
            data: dataObject,
            columns: [
                {data: "Codigo", type: "text"},
                {data: "Descripcion", type: "text"},
                {data: "Marca", type: "text", readOnly: true},
                {data: "Precio", type: "numeric", format: '$0,0'},
                {data: "Cantidad", type: "numeric", format: '0,0'},
                {data: "Total", type: "numeric", format: '$0,0', readOnly: true},
                {data: "Margen", type: "text", readOnly: true},
                {data: "Comision", type: "text", readOnly: true},
                {data: "Condicion", type: "text", readOnly: true},
                {data: "Stock", type: "numeric", format: '0,0', readOnly: true}
            ],
            stretchH: 'all',
            width: 1300,
            autoWrapRow: true,
            height: 260,
            maxRows: 1000,
            rowHeaders: true,
            colHeaders: [
                'Código',
                'Descripción',
                'Marca',
                'Precio',
                'Cantidad',
                'Total',
                'Margen',
                'Comisión',
                'Condición S/P',
                'Stock'
            ],
            columnSorting: {
                indicator: true
            },
            contextMenu: true,
            manualRowMove: true,
            filters: true,
            dropdownMenu: true,
            beforeChange: function (changes, source) {
                for (var i = changes.length - 1; i >= 0; i--) {
                    if (changes[i][1] == 'Codigo') {
                        changes[i][3] = changes[i][3].toUpperCase();
                        //console.log(changes[i][3]+'*******************');
                    }
                }
            },
            afterChange: function (changes, source) {
                if (source != 'loadData') {
                    for (var i = changes.length - 1; i >= 0; i--) {
                        var row = changes[i][0];

                        var codpro = "";
                        var despro = "";
                        var marca = "";
                        var prepro = "";
                        var marpro = "";
                        var stopro = "";
                        var compro = 1;
                        var conpro = "";
                        var costo = "";

                        if (changes[i][1] == 'Codigo') {

                            var rutcli = '{{ $rutcli }}';
                            var codpro = elemento[3].toUpperCase();
                            var tipbus = 'CODPRO';

                            var parametros = {
                                rutcli: rutcli,
                                cod_producto: codpro,
                                tipo_busqueda: tipbus
                            };

                            ($.ajax({
                                xhrFields: {
                                    withCredentials: true
                                },
                                headers: {
                                    'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'
                                },
                                data: parametros,
                                url: '{{ url('../../portalejecutivo_apis/public/api/productos/producto-especifico') }}',
                                type: 'POST',
                                // cache: true,
                                datatype: 'json',
                                // async: true,
                                beforeSend: function () {
                                    document.getElementById("pre_loader").className = "loader animation-start";
                                },
                                success: function (json) {

                                    if (json.code == 200 && json.response.stock > 0) {
                                        codpro = json.response.codpro;
                                        despro = json.response.despro;
                                        marca = json.response.marca;
                                        prepro = json.response.precio;
                                        marpro = json.response.margen;
                                        stopro = json.response.stock;
                                        compro = json.response.categoria;
                                        conpro = json.response.condicion;
                                        costo = json.response.costo;

                                        //tblCotizacion.setDataAtCell(row, 1, despro);
                                        //tblCotizacion.setDataAtCell(row, 2, marca);
                                        // tblCotizacion.setDataAtCell(row, 0, codpro);
                                        // tblCotizacion.setDataAtCell(row, 1, despro);
                                        // tblCotizacion.setDataAtCell(row, 2, marca);
                                        // tblCotizacion.setDataAtCell(row, 3, prepro);
                                        // tblCotizacion.setDataAtCell(row, 4, '1');
                                        // tblCotizacion.setDataAtCell(row, 5, prepro);
                                        // tblCotizacion.setDataAtCell(row, 6, marpro + '%');
                                        // tblCotizacion.setDataAtCell(row, 7, compro);
                                        // tblCotizacion.setDataAtCell(row, 8, conpro);
                                        // tblCotizacion.setDataAtCell(row, 9, stopro);

                                        //console.log(json.response);

                                    }
                                    document.getElementById("pre_loader").className = "loader animation-start d-none";
                                }
                            }));
                        }
                    }
                }
            }
        };

        var elemento = document.getElementById('tbl_cotizacion');
        var tblCotizacion = new Handsontable(elemento, configuraciones);

        {{--tblCotizacion.addHook('afterChange', function(registroModificado, acciones) {--}}
        {{--    if(acciones != 'loadData'){--}}
        {{--        registroModificado.forEach(function(elemento){--}}
        {{--            var fila = tblCotizacion.getData()[elemento[0]];--}}
        {{--            //console.log(fila);--}}
        {{--            //console.log(elemento);--}}

        {{--            var row = elemento[0];--}}

        {{--            var codpro = "";--}}
        {{--            var despro = "";--}}
        {{--            var marca = "";--}}
        {{--            var prepro = "";--}}
        {{--            var marpro = "";--}}
        {{--            var stopro = "";--}}
        {{--            var compro = 1;--}}
        {{--            var conpro = "";--}}
        {{--            var costo = "";--}}

        {{--            if(elemento[1] == 'Codigo'){--}}
        {{--                var rutcli = '{{ Session::get('searchclient') }}';--}}
        {{--                var codpro = elemento[3].toUpperCase();--}}
        {{--                var tipbus = 'CODPRO';--}}
        {{--                --}}
        {{--                var parametros = {--}}
        {{--                    rutcli: rutcli,--}}
        {{--                    cod_producto: codpro,--}}
        {{--                    tipo_busqueda: tipbus--}}
        {{--                };--}}
        {{--                ($.ajax({--}}
        {{--                    xhrFields: {--}}
        {{--                        withCredentials: true--}}
        {{--                    },--}}
        {{--                    headers: {--}}
        {{--                        'Authorization': 'Basic ' + 'inteligenciadimerc:iNt3l1g3nt3'--}}
        {{--                    },--}}
        {{--                    data:parametros,--}}
        {{--                    url:'{{ url('../../portalejecutivo_apis/public/api/productos/producto-especifico') }}',--}}
        {{--                    type: 'POST',--}}
        {{--                    // cache: true,--}}
        {{--                    datatype: 'json',--}}
        {{--                    // async: true,--}}
        {{--                    beforeSend: function () {--}}
        {{--                        document.getElementById("pre_loader").className = "loader animation-start";--}}
        {{--                    },--}}
        {{--                    success: function (json) {--}}

        {{--                        if (json.code == 200 && json.response.stock > 0) {--}}
        {{--                            codpro = json.response.codpro;--}}
        {{--                            despro = json.response.despro;--}}
        {{--                            marca = json.response.marca;--}}
        {{--                            prepro = json.response.precio;--}}
        {{--                            marpro = json.response.margen;--}}
        {{--                            stopro = json.response.stock;--}}
        {{--                            compro = json.response.categoria;--}}
        {{--                            conpro = json.response.condicion;--}}
        {{--                            costo = json.response.costo;--}}

        {{--                            tblCotizacion.setDataAtCell(row, 1, despro);--}}
        {{--                            tblCotizacion.setDataAtCell(row, 2, marca);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 0, codpro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 1, despro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 2, marca);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 3, prepro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 4, '1');--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 5, prepro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 6, marpro + '%');--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 7, compro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 8, conpro);--}}
        {{--                            // tblCotizacion.setDataAtCell(row, 9, stopro);--}}

        {{--                            //console.log(json.response);--}}

        {{--                        }--}}
        {{--                        document.getElementById("pre_loader").className = "loader animation-start d-none";--}}
        {{--                    }--}}
        {{--                }));--}}
        {{--                console.log(fila.Codigo);--}}
        {{--                //fila.Descripcion.setValue(despro);--}}

        {{--                //$('#tbl_cotizacion').handsontable('setDataAtCell', row, 0, codpro, false);--}}


        {{--            }--}}
        {{--            // else if(elemento[1] == 'descripcion'){--}}
        {{--            //--}}
        {{--            // }--}}
        {{--        });--}}
        {{--    }--}}
        {{--});--}}

        tblCotizacion.render();
    }

    function opcionesAnotaciones(option) {
        var divOpciones = document.getElementById('opciones_anotaciones');
        if (option.selectedIndex == 0) {
            divOpciones.innerHTML = '';
        } else {
            if (option.selectedIndex == 1) { //No esta no contesta
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control"\n' +
                    '                                            onchange="proxAcc(this)" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Volver a llamar">Volver a llamar</option>\n' +
                    '                                        <option value="Sacar registro, datos no corresponden">Sacar registro, datos no\n' +
                    '                                            corresponden\n' +
                    '                                        </option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row" id="fecha_prox_accion">\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control" placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
            } else if (option.selectedIndex == 2) { //Se envia carta de presentación
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Contacte a</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="contactos" id="loscontactos" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona contacto</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Volver a llamar">Volver a llamar</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="date" name="fecha" id="fecha" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
                var token = '{{ csrf_token() }}';
                var parametros = {
                    _token: token
                };
                $.ajax({
                    data: parametros,
                    url: '{{ url('getcontactoanotaciones') }}/' + '{{$rutcli}}',
                    type: 'POST',
                    cache: true,
                    datatype: 'json',
                    async: true,
                    beforeSend: function () {
                        var selectobject = document.getElementById("loscontactos");
                        selectobject.disabled = true;
                        for (var i = 1; i < selectobject.length; i++) {
                            selectobject.remove(i);
                        }
                    },
                    success: function (json) {
                        if (json.code == 200) {
                            var selectobject = document.getElementById("loscontactos");
                            selectobject.disabled = false;
                            for (i in json.response) {
                                var option = document.createElement("option");
                                option.text = json.response[i].contacto;
                                option.value = json.response[i].contacto;
                                var select = document.getElementById("loscontactos");
                                select.appendChild(option);
                            }
                        }
                    },
                    error: function(e){
                        var code = e.status;
                        var text = e.statusText;
                        registroError(ejecutivo, moduleid_global, 'anotaciones', code, text);
                    }
                });
            } else if (option.selectedIndex == 3) { //se contacta a cliente
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Contacte a</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="contactos" id="loscontactos" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona contacto</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" onchange="proxAcc_2(this)" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Cliente indica que compra por otro Rut">Cliente indica que compra por otro Rut</option>\n' +
                    '                                        <option value="Sacar registro cliente no quiere trabajar con Dimerc">Sacar registro cliente no quiere trabajar con Dimerc</option>\n' +
                    '                                        <option value="Sacar Rut no rentable">Sacar Rut no rentable</option>\n' +
                    '                                        <option value="Sin requerimiento volver a llamar">Sin requerimiento volver a llamar</option>\n' +
                    '                                        <option value="Revisión de precios">Revisión de precios</option>\n' +
                    '                                        <option value="Se gestionara convenio">Se gestionara convenio</option>\n' +
                    '                                        <option value="Venta comprometida">Venta comprometida</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row" id="prox_accion_opt">\n' +
                    '                            </div>' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
                var token = '{{ csrf_token() }}';
                var parametros = {
                    _token: token
                };
                $.ajax({
                    data: parametros,
                    url: '{{ url('getcontactoanotaciones') }}/' + '{{$rutcli}}',
                    type: 'POST',
                    cache: true,
                    datatype: 'json',
                    async: true,
                    beforeSend: function () {
                        var selectobject = document.getElementById("loscontactos");
                        selectobject.disabled = true;
                        for (var i = 1; i < selectobject.length; i++) {
                            selectobject.remove(i);
                        }
                    },
                    success: function (json) {
                        console.log(json);
                        if (json.code == 200) {
                            var selectobject = document.getElementById("loscontactos");
                            selectobject.disabled = false;
                            for (i in json.response) {
                                var option = document.createElement("option");
                                option.text = json.response[i].contacto;
                                option.value = json.response[i].contacto;
                                var select = document.getElementById("loscontactos");
                                select.appendChild(option);
                            }
                        }
                    }
                });
            } else if (option.selectedIndex == 4 || option.selectedIndex == 5 || option.selectedIndex == 7) { //se envia oferta //se envia oferta cajas //se envia propuesta de precios
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Contacte a</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="contactos" id="loscontactos" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona contacto</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Volver a llamar">Volver a llamar</option>\n' +
                    '                                        <option value="Sin requerimiento, volver a llamar">Sin requerimiento, volver a llamar</option>\n' +
                    '                                        <option value="Revisión de precios">Revisión de precios</option>\n' +
                    '                                        <option value="Se gestionara convenio">Se gestionara convenio</option>\n' +
                    '                                        <option value="Venta comprometida">Venta comprometida</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="date" name="fecha" id="fecha" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
                var token = '{{ csrf_token() }}';
                var parametros = {
                    _token: token
                };
                $.ajax({
                    data: parametros,
                    url: '{{ url('getcontactoanotaciones') }}/' + '{{$rutcli}}',
                    type: 'POST',
                    cache: true,
                    datatype: 'json',
                    async: true,
                    beforeSend: function () {
                        var selectobject = document.getElementById("loscontactos");
                        selectobject.disabled = true;
                        for (var i = 1; i < selectobject.length; i++) {
                            selectobject.remove(i);
                        }
                    },
                    success: function (json) {
                        if (json.code == 200) {
                            var selectobject = document.getElementById("loscontactos");
                            selectobject.disabled = false;
                            for (i in json.response) {
                                var option = document.createElement("option");
                                option.text = json.response[i].contacto;
                                option.value = json.response[i].contacto;
                                var select = document.getElementById("loscontactos");
                                select.appendChild(option);
                            }
                        }
                    }
                });
            } else if (option.selectedIndex == 6) { //Cambie lista de precios
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Volver a llamar">Volver a llamar</option>\n' +
                    '                                        <option value="Sin requerimiento, volver a llamar">Sin requerimiento, volver a llamar</option>\n' +
                    '                                        <option value="Revisión de precios">Revisión de precios</option>\n' +
                    '                                        <option value="Se gestionara convenio">Se gestionara convenio</option>\n' +
                    '                                        <option value="Venta comprometida">Venta comprometida</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="date" name="fecha" id="fecha" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
            } else if (option.selectedIndex == 8) {
                var data = '<div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Volver a llamar">Volver a llamar</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Compromiso de pago">Compromiso de pago</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="date" name = "fecha" id = "fecha" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
            } else if (option.selectedIndex == 9) {
                var data =
                    '                                <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Cotización N°</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="number" name = "numcot" id="numcot" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Perdí la cotización por</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="motivo_coti" id="motivo_coti" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Bloqueado por credito">Bloqueado por credito</option>\n' +
                    '                                        <option value="Bloqueado por seguro">Bloqueado por seguro</option>\n' +
                    '                                        <option value="Falta de presupuesto">Falta de presupuesto</option>\n' +
                    '                                        <option value="Perdi por precio">Perdi por precio</option>\n' +
                    '                                        <option value="Perdi por servicio">Perdi por servicio</option>\n' +
                    '                                        <option value="Perdi por stock">Perdi por stock</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Perdí con</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="perdi_contra" id="perdi_contra" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Prisa">Prisa</option>\n' +
                    '                                        <option value="Ricardo Rodríguez">Ricardo Rodríguez</option>\n' +
                    '                                        <option value="Abatte">Abatte</option>\n' +
                    '                                        <option value="Edapi">Edapi</option>\n' +
                    '                                        <option value="Lapiz Lopez">Lapiz Lopez</option>\n' +
                    '                                        <option value="Dimeiggs">Dimeiggs</option>\n' +
                    '                                        <option value="Pentacrom">Pentacrom</option>\n' +
                    '                                        <option value="No sé">No sé</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                                </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" onchange="proxAcc_3(this)" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Sacar registro cliente no quiere trabajar con Dimerc">Sacar registro cliente no quiere trabajar con Dimerc</option>\n' +
                    '                                        <option value="Sacar Rut no rentable">Sacar Rut no rentable</option>\n' +
                    '                                        <option value="Revisión de precios">Revisión de precios</option>\n' +
                    '                                        <option value="Se gestionara convenio">Se gestionara convenio</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row" id="prox_accion_opt">\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
            } else if (option.selectedIndex == 10) {
                var data =
                    '                                <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Linea</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="linea" id="linea" class="form-control" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Libreria">Libreria</option>\n' +
                    '                                        <option value="Tissue">Tissue</option>\n' +
                    '                                        <option value="Tecnología">Tecnología</option>\n' +
                    '                                        <option value="Papel Fotocopia">Papel Fotocopia</option>\n' +
                    '                                        <option value="Cafeteria y menaje">Cafeteria y Menaje</option>\n' +
                    '                                        <option value="Aseo, higiene y otros">Aseo, higiene y otros</option>\n' +
                    '                                        <option value="Seguridad">Seguridad</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                                </div>\n' +
                    '                                <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Monto corregido</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <input type="number" name = "monto" id="monto" class="form-control" required>\n' +
                    '                                </div>\n' +
                    '                                </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Próxima acción</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <select name="prox_accion" id="prox_accion" class="form-control" onchange="proxAcc_3(this)" required>\n' +
                    '                                        <option value="">Selecciona una opción</option>\n' +
                    '                                        <option value="Sacar registro cliente no quiere trabajar con Dimerc">Sacar registro cliente no quiere trabajar con Dimerc</option>\n' +
                    '                                        <option value="Sacar Rut no rentable">Sacar Rut no rentable</option>\n' +
                    '                                        <option value="Revisión de precios">Revisión de precios</option>\n' +
                    '                                        <option value="Se gestionara convenio">Se gestionara convenio</option>\n' +
                    '                                        <option value="Cotización comprometida">Cotización comprometida</option>\n' +
                    '                                        <option value="Se agenda reunión">Se agenda reunión</option>\n' +
                    '                                        <option value="Se agenda visita">Se agenda visita</option>\n' +
                    '                                        <option value="Se agenda visita Kam">Se agenda visita Kam</option>\n' +
                    '                                    </select>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row" id="prox_accion_opt">\n' +
                    '                            </div>\n' +
                    '                            <div class="form-group row">\n' +
                    '                                <label class="col-sm-2 col-form-label">Observaciones</label>\n' +
                    '                                <div class="col-sm-10">\n' +
                    '                                    <textarea name="observacion" id="observacion" rows="5" cols="5" class="form-control"\n' +
                    '                                              placeholder="Observación libre" required></textarea>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                divOpciones.innerHTML = data;
            } else {
                divOpciones.innerHTML = '';
            }
        }
    }
    function proxAcc(option) {
        var divCalendar = document.getElementById('fecha_prox_accion');
        if (option.value == 'Volver a llamar') {
            divCalendar.innerHTML = '' +
                '<label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                '<div class="col-sm-10">\n' +
                '<input name="fecha" id="fecha" type="date" class="form-control" required>\n' +
                '</div>';
        } else {
            divCalendar.innerHTML = '';
        }
    }
    function proxAcc_2(option) {
        var divOpcion = document.getElementById('prox_accion_opt');
        if (option.selectedIndex == 0 || option.selectedIndex == 2 || option.selectedIndex == 3) {
            divOpcion.innerHTML = '';
        } else if (option.selectedIndex == 1) {
            divOpcion.innerHTML = '' +
                '<label class="col-sm-2 col-form-label">Indicar Rut Relacionado (Sin puntos ni dígito verificador)</label>\n' +
                '<div class="col-sm-10">\n' +
                '<input type="number" name="rut_rel" id="rut_rel" class="form-control" maxlength="8" required>\n' +
                '</div>';
        } else {
            divOpcion.innerHTML = '' +
                '<label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                '<div class="col-sm-10">\n' +
                '<input name="fecha" id="fecha" type="date" class="form-control" required>\n' +
                '</div>';
        }
    }
    function proxAcc_3(option) {
        var divOpcion = document.getElementById('prox_accion_opt');
        if (option.selectedIndex == 0 || option.selectedIndex == 1) {
            divOpcion.innerHTML = '';
        } else {
            divOpcion.innerHTML = '' +
                '<label class="col-sm-2 col-form-label">Fecha próxima acción</label>\n' +
                '<div class="col-sm-10">\n' +
                '<input name="fecha" id="fecha" type="date" class="form-control" required>\n' +
                '</div>';
        }
    }
    function validaFormAnotacion() {
        alert("ok");
        return false;
    }

    function invocaRegistroBitacora(action, oldvalue='', newvalue='', updaterecord=''){
        /*
        Module: fichacliente -> 6
        Action: clic en registro anotaciones	 -> 7
                clic en ver cotizaciones	 -> 8
                clic en generar cotizacion	 -> 9

        */
        var action_num=0;
        switch (action) {
            case "registroanotaciones":
                action_num=7;
                break;
            case "vercotizaciones":
                action_num=8;
                break;
            case "generarcotizacion":
                action_num=9;
                break;

        }
        var userid='{{ Session::get('usuario')->get('codusu') }}';
        var moduleid=6;
        var actionid=action_num;
        registro(userid, moduleid, actionid, oldvalue, newvalue, updaterecord);
    }

</script>