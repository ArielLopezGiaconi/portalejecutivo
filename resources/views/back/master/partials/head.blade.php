<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="#">
<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
<meta name="author" content="#">

<title>@yield('titulo', 'Portal del ejecutivo')</title>

<script src="https://code.jquery.com/jquery-3.2.1.js"></script>

<!-- Favicon icon -->
<link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon">
<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Abel&display=swap" rel="stylesheet"> 
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Fira+Sans+Condensed&display=swap" rel="stylesheet"> 
<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/css/bootstrap.min.css') }}">

<!-- feather Awesome -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/simple-line-icons/css/simple-line-icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/feather/css/feather.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/ion-icon/css/ionicons.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/icofont/css/icofont.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/themify-icons/themify-icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/material-design/css/material-design-iconic-font.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/toolbar/jquery.toolbar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/toolbar/custom-toolbar.css') }}">

<!-- notify js Fremwork -->
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components\pnotify\css\pnotify.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components\pnotify\css\pnotify.brighttheme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components\pnotify\css\pnotify.buttons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components\pnotify\css\pnotify.history.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components\pnotify\css\pnotify.mobile.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets\pages\pnotify\notify.css') }}">

{{--<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/css/sweetalert.css') }}">--}}
{{--<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">--}}

{{--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/css/handsontable.full.min.css') }}">--}}

<!-- Data Table Css -->
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

<link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/linearicons.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/notification/notification.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/animate.css/css/animate.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('assets\css\css_chat.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets\css\barraPedido.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets\css\tabla_fija.css') }}">
<link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">

<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.mCustomScrollbar.css') }}">
{{-- <link href="{{asset('/flickity/flickity.css')}}" rel="stylesheet" /> --}}
{{-- <link rel="stylesheet" href="{{ asset('bower_components/c3/css/c3.css') }}" type="text/css" media="all"> --}}

{{-- Prueba de tabla columnas fijas --}}
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.1/css/fixedColumns.dataTables.min.css" />

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link rel="stylesheet" href="assets/css/style.css" />
<link href="assets/css/sticky-footer-navbar.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/script.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.js" integrity="sha512-opXrgVcTHsEVdBUZqTPlW9S8+99hNbaHmXtAdXXc61OUU6gOII5ku/PzZFqexHXc3hnK8IrJKHo+T7O4GRIJcw==" crossorigin="anonymous"></script>