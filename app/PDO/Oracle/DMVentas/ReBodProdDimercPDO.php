<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 11:56
 */

namespace App\PDO\Oracle\DMVentas;

use App\Entities\Oracle\DMVentas\EnCliente;
use App\Entities\Oracle\DMVentas\ReBodProdDimerc;
use App\PDO\MySql\Magento\EmailCobranzaPDO;
use App\PDO\MySql\Magento\SalesFlatOrderPDO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Entities\Oracle\DMVentas\NotaVenta;
// use Illuminate\Support\Facades\DB;

class ReBodProdDimercPDO extends Model
{
    public static function getStockProduct($first_row = false, $in_codpro, $in_codbod, $in_codemp)
    {
        $sql = "SELECT * FROM re_bodprod_dimerc "
            . " WHERE codpro = :cod_prod"
            . " AND codbod = :cod_bod"
            . " AND codemp = :cod_emp";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_prod' => $in_codpro,
            'cod_bod' => $in_codbod,
            'cod_emp' => $in_codemp,
        ]);

        return ReBodProdDimercPDO::returnStockFormat($resultado, $first_row);
    }

    public static function getCorporateStock($in_codpro, $in_rutcliente)
    {
        $sql = "SELECT CASE WHEN (stocks-stkcom) > 0 THEN (stocks-stkcom) ELSE 0 END AS stock FROM re_bodprod_dimerc"
            . " WHERE codpro = :cod_pro AND codbod = GETRUTCONCESION_NEW(3, :rut_cliente, 0)";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_pro' => $in_codpro,
            'rut_cliente' => $in_rutcliente,
        ]);

        return ($resultado && count($resultado) > 0) ? $resultado[0]->stock : 0;
    }

    public static function getAllStockFulfillment()
    {
        $sql = "SELECT b.codpro, b.codsap, c.sap_almacen, c.sap_centro, a.stocks FROM "
            . " re_bodprod_dimerc a"
            . " INNER JOIN ma_product b ON a.codpro = b.codpro"
            . " INNER JOIN ma_bodegas c ON a.codbod = c.codbod AND c.codemp = a.codemp"
            . " WHERE a.codbod = :cod_bod_fullfilment"
            . " AND b.codneg = :cod_neg_fulfillment"
            . " AND b.codsap IS NOT NULL"
            . " AND c.sap_almacen IS NOT NULL"
            . " AND c.sap_centro IS NOT NULL"
            . " ORDER BY a.stocks DESC "// . " ORDER BY codsap asc"
        ;

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_bod_fullfilment' => 114,
            'cod_neg_fulfillment' => 1,
        ]);

        return $resultado;
    }

    public static function getAllPriceFulfillment()
    {
        $sql = "SELECT a.codpro, a.codsap, a.costo as precio, GET_RUTPROVEEDOR(a.codpro) as proveedor FROM " // b.precio
            . " ma_product a"
            . " INNER JOIN re_canprod b ON a.codpro = b.codpro"
            . " WHERE a.codneg = :cod_neg_fulfillment"
            . " AND b.codcnl = :pricelist_fulfillment"
            . " AND codsap IS NOT NULL";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_neg_fulfillment' => 1,
            'pricelist_fulfillment' => 710
        ]);

        return $resultado;
    }

    public static function getSpecificPriceFulfillment($codpro)
    {
        $sql = "SELECT a.codpro, a.codsap, a.costo as precio, GET_RUTPROVEEDOR(a.codpro) as proveedor FROM " // b.precio
            . " ma_product a"
            . " INNER JOIN re_canprod b ON a.codpro = b.codpro"
            . " WHERE a.codneg = :cod_neg_fulfillment"
            . " AND b.codcnl = :pricelist_fulfillment"
            . " AND codsap IS NOT NULL"
            . " AND a.codpro = :codpro";

        $resultado = DB::connection('oracle_dmventas')->select($sql, [
            'cod_neg_fulfillment' => 1,
            'pricelist_fulfillment' => 710,
            'codpro' => $codpro
        ]);

        return ($resultado && count($resultado) > 0) ? $resultado[0] : null;
    }

    public static function refreshStockProduct(array $products)
    {
        try {
            // DB::connection('oracle_dmventas')->beginTransaction();
            foreach ($products as $product) {
                if ($product['dimerc_codpro'] != null && trim($product['dimerc_codpro']) <> "") {
                    $jaja = $product;
                    // Formula 3
                    $sql = "MERGE INTO re_bodprod_dimerc a"
                        . " USING(SELECT :cod_emp AS codemp, :cod_bod AS codbod, :cod_pro AS codpro, "
                        . " :stock_fisico AS stocks, :stock_compr AS stkcom, :stock_disp AS stkdis FROM dual) src"
                        . " ON(a.codemp = src.codemp AND a.codbod = src.codbod AND a.codpro = src.codpro)"
                        . " WHEN MATCHED THEN"
                        . " UPDATE SET a.stocks = src.stocks, a.stkcom = src.stkcom, a.stkdis = src.stkdis"
                        . " WHEN NOT MATCHED THEN"
                        . " INSERT(codemp, codbod, codpro, stocks, stkcom, stkdis)"
                        . " VALUES(src.codemp, src.codbod, src.codpro, src.stocks, src.stkcom, src.stkdis)";

                    DB::connection('oracle_dmventas')->statement($sql, [
                        'stock_fisico' => $product['sap_stkfis'],
                        'stock_compr' => $product['sap_stkcom'],
                        'stock_disp' => $product['sap_stkdis'],
                        'cod_pro' => $product['dimerc_codpro'],
                        'cod_bod' => $product['dimerc_codbod'],
                        'cod_emp' => 3,
                    ]);
                }
            }

            // DB::connection('oracle_dmventas')->commit();
            return true;
        } catch (\Exception $e) {
            // DB::connection('oracle_dmventas')->rollBack();
            return false;
        }
    }

    public static function refreshStockProductFromSapTransfer($in_codemp = 3)
    {
        $conexion = ($in_codemp == 3) ? 'oracle_dmventas' : 'oracle_unificado';
        $table = ($in_codemp == 3 ) ? 're_bodprod_dimerc' : 're_bodprod';
        $table_fbo = ($in_codemp == 3 ) ? 'sap_fbo_tf_stock@ora92' : 'sap_fbo_tf_stock';
        //try {
            // INSERT INTO RE_BODPROD (_DIMERC)
            $sql = "MERGE INTO "
                . " ". $table ." a"
                . " USING("
                . "     SELECT codemp, codbod, codpro, stkfis, stkcom, stkdis FROM " . $table_fbo . " WHERE estado = 0 AND codemp = :cod_emp"
                . " ) tf"
                . " ON(a.codemp = tf.codemp AND a.codbod = tf.codbod AND a.codpro = tf.codpro)"
                . " WHEN MATCHED THEN"
                . " UPDATE SET a.stocks = tf.stkfis, a.stkcom = tf.stkcom, a.stkdis = tf.stkdis"
                . " WHEN NOT MATCHED THEN"
                . " INSERT(codemp, codbod, codpro, stocks, stkcom, stkdis)"
                . " VALUES(tf.codemp, tf.codbod, tf.codpro, tf.stkfis, tf.stkcom, tf.stkdis)"
            ;

            // EXECUTE IN SPECIFIC DATABASE
            DB::connection($conexion)->statement($sql, [
                'cod_emp' => $in_codemp
            ]);

            // REFRESH STATUS
            $sql = "UPDATE sap_fbo_tf_stock SET estado = 1, FECHA_PROCE = SYSDATE WHERE codemp = :cod_emp";
            DB::connection('oracle_unificado')->statement($sql, [
                'cod_emp' => $in_codemp
            ]);

            return true;
        //} catch (\Exception $e) {
            // DB::connection('oracle_dmventas')->rollBack();
            return false;
        //}
    }

    private static function returnStockFormat($registros, $first_row = false)
    {
        $arrayReturn = null;
        foreach ($registros as $registro) {
            $bodega = new ReBodProdDimerc($registro);
            if ($first_row) {
                return $bodega;
            }

            $arrayReturn[] = $bodega;
        }

        return $arrayReturn;
    }
}