<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 05/09/2018
 * Time: 13:45
 */


namespace App\Entities\SapWs\Request;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;
use Illuminate\Support\Collection;

class MtMm024ReqOut extends ReqOut
{
    private $material;
    private $cod_proveedor;
    private $cod_org_compra;
    private $precio_compra;

    public function __construct()
    {
        $this->setHeaderHosts((env('DATABASE_ENV') == 'prod') ? env('WS_SAP_PROD') : env('WS_SAP_DEV'));
        $this->setHeaderUrl($this->getHeaderHosts().'/XISOAPAdapter/MessageServlet');
        $this->setHeaderMethod('POST');
        $this->setHeaderUrlfull(
            $this->getHeaderUrl()
            . '?senderParty='
            . '&senderService=BC_VERACORE'
            . '&receiverParty='
            . '&receiverService='
            . '&interface=SI_MM024_SYN_OUT'
            . '&interfaceNamespace=urn:swap:veracore:s4h:info_precio_material:MM024');

        $this->materiales_stocks = null;
        /* Formato de Materiales: ['material' => '', 'stock' => ''] */
    }

    public function getMaterial()
    {
        // 18 Characters
        return substr('000000000000000000' . $this->material, -18);
    }

    public function setMaterial($material)
    {
        $this->material = $material;
    }

    public function getCodProveedor()
    {
        return $this->cod_proveedor;
    }

    public function setCodProveedor($cod_proveedor)
    {
        $this->cod_proveedor = $cod_proveedor;
    }

    public function getCodOrgCompra()
    {
        return $this->cod_org_compra;
    }

    public function setCodOrgCompra($cod_org_compra)
    {
        $this->cod_org_compra = $cod_org_compra;
    }

    public function getPrecioCompra()
    {
        return $this->precio_compra;
    }

    public function setPrecioCompra($precio_compra)
    {
        $this->precio_compra = $precio_compra;
    }

    public function getRequestXml()
    {
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:swap:veracore:s4h:info_precio_material:MM024">'
            . '<soapenv:Header/> '
            . '<soapenv:Body> '
            . '<urn:MT_MM024_REQ_OUT> '
            . '<COD_PROVEEDOR>' . $this->getCodProveedor() . '</COD_PROVEEDOR> '
            . '<COD_MATERIAL>' . $this->getMaterial() . '</COD_MATERIAL> '
            . '<COD_ORG_COMPRA>'. $this->getCodOrgCompra() .'</COD_ORG_COMPRA> '
            . '<PRECIO_COMPRA>'. $this->getPrecioCompra() .'</PRECIO_COMPRA> '
            . '</urn:MT_MM024_REQ_OUT> '
            . '</soapenv:Body> '
            . '</soapenv:Envelope>';

        return $xml;
    }

    /*public function getRequestXmlMultiple(array $in_data)
    {
        $xml ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'
            . ' xmlns:urn="urn:swap:veracore:s4h:stock_veracore:MM025">'
            . ' <soapenv:Header/>'
            . ' <soapenv:Body>'
            . ' <urn:MT_MM025_REQ_OUT>';

        foreach ($in_data as $registro) {
            $xml.=' <T_STOCK>'
                . ' <MATNR>' . $registro -> codsap . '</MATNR>'  // MATERIAL: [FF455740]
                . ' <WERKS>' . $registro -> sap_centro . '</WERKS>' // CENTRO LOGÍSTICO SAP: [Bodega 114]
                . ' <LGORT>' . $registro -> sap_almacen . '</LGORT>' // ALMACEN FULFILLMENT:  [Bodega 114]
                . ' <LBKUM>' . $registro -> stocks . '</LBKUM>' // STOCK
                . ' </T_STOCK>';
        }

        $xml.=' </urn:MT_MM025_REQ_OUT>'
            . ' </soapenv:Body>'
            . ' </soapenv:Envelope>';

        return $xml;
    }*/
}