<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 03/09/2018
 * Time: 12:31
 */

namespace App\PDO\MySql\Dimerc;

use App\Entities\Oracle\Holding\MaProduct;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Collection;

class CatalogProductEntityPDO extends Model
{
    public static function getAllSkuChile()
    {
        $sql = "SELECT a.sku as codpro, b.value as codsap FROM catalog_product_entity a"
            . " INNER JOIN catalog_product_entity_varchar b"
            . " ON a.entity_id = b.entity_id AND b.attribute_id = 2028"
            . " WHERE sku LIKE 'CL_%'"
            . " AND b.value IS NOT NULL"
            . " ORDER BY codsap ASC";

        $resultado = DB::connection('mysql_magentodimerc')->select($sql);

        $arrayReturn = null;
        foreach ($resultado as $registro) {
            $producto = new MaProduct();
            $producto->setCodsap($registro->codsap);
            $producto->setCodpro($registro->codpro);
            $arrayReturn[] = $producto;
        }

        return $arrayReturn;
    }
}