@extends('back.master.masterpage')
@section('contenido')

    <style>
        .botones_cartera {
            white-space: nowrap;
            width: 1%;
            padding: 3px 10px 3px 10px !important;
        }
    </style>
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-table text-inverse f-46"></i> 
                        </div>
                        <div class="col-sm-6">
                            <h3 id="titulo" >Indicadores</h3>
                            <span class="f-16">Indicadores | <i class="zmdi zmdi-account f-22 middle m-r-5" 
                                                             style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                    </div>
                    <div class="card-header-right">
                    </div>
                </div>
                <div class="card-block" style="">
                </div>
            </div>

            <div id="ElSegmento" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <h2 class="modal-title card-block text-center">Segmento</h2>
                        <div class="card-block text-center">
                            <h3 class="modal-title">Segmento del cliente en sistema, en relacion a su potencial e historial de compra</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
           
            <div class="card-block">
                <div class="row" id="draggablePanelList">
                    <div id="estadisticasDia" class="col-lg-12 col-xl-2">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-panel text-inverse f-46"></i>
                                <a target="_blank" href="{{ url('estadisticasDia') }}"><h4 class="card-title">Estadisticas del día</h4></a>
                            </div>
                        </div>
                    </div>
                    <div id="estadisticasMes" class="col-lg-12 col-xl-2">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="fa fa-bar-chart-o text-inverse f-46"></i>
                                <a target="_blank" href="{{ url('estadisticasMes') }}"><h4 class="card-title">Estadisticas del mes</h4></a>
                            </div>
                        </div>
                    </div>
                    <div id="carteraMes" class="col-lg-12 col-xl-2">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-wallet text-inverse f-46"></i>
                                <a target="_blank" href="{{ url('carteraMes') }}"><h4 class="card-title">Cartera en el mes</h4></a>
                            </div>
                        </div>
                    </div>
                    <div id="seguimientoNota" class="col-lg-12 col-xl-2">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="icon-notebook text-inverse f-46"></i>
                                <a target="_blank" href="{{ url('SeguimientoNota') }}"><h4 class="card-title">Seguimiento nota </h4></a>
                            </div>
                        </div>
                    </div>
                    <div id="anotacionesEjecutivo" class="col-lg-12 col-xl-2">
                        <div class="card-sub">
                            <div class="card-block">
                                <i class="ti-notepad text-inverse f-46"></i>
                                <a target="_blank" href="{{ url('anotacionesEjecutivo') }}"><h4 class="card-title">Anotaciones x ejecutivo</h4></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide col-lg-12" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="card" >
                            <div class="card-body">
                                <h4 class="x_title m-b-30">
                                    <img src="{{asset('/images/dim.png')}}" style="vertical-align: bottom; width: 20px; height: 20px;">&nbsp;Total 
                                    <span id="count_total" style="color: #73879C !important;"></span>
                                </h4>
                                <div class="row tile_count">
                                    {{-- <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <h3><strong>¡Información!</strong> hoy a las 18:00 hrs se realizaran mantenciones a los servidores por lo que podría no estar disponible los servicios.</h3>
                                    </div> --}}
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-usd"></i> 
                                            <strong>Venta Total</strong>
                                        </span>
                                        <div class="count f-36" id="ventadia_all"></div>
                                        <span class="count_bottom" id="meta_venta_all"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-usd"></i> 
                                            <strong>Venta Boleta</strong>
                                        </span>
                                        <div class="count f-36" id="bole_all"></div>
                                        <span class="count_bottom"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-usd"></i> 
                                            <strong>Venta Antofagasta</strong>
                                        </span>
                                        <div class="count f-36" id="anto_all"></div>
                                        <span class="count_bottom"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-usd"></i> 
                                            <strong>Venta Cajas</strong>
                                        </span>
                                        <div class="count f-36" id="cajas_all"></div>
                                        <span class="count_bottom"></span>
                                    </div>
                                </div>
                                <div class="row tile_count">
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-percent"></i> 
                                            <strong>Participación Ecommerce</strong>
                                        </span>
                                        <div class="count f-36" id="participacionecommerce_all"></div>
                                        <span class="count_bottom"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-percent"></i> 
                                            <strong>Participación Convenios</strong>
                                        </span>
                                        <div class="count f-36" id="participacionconvenios_all"></div>
                                        <span class="count_bottom"></span>
                                    </div>
                                </div>
                                {{-- <div class="row tile_count">
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-line-chart"></i> 
                                            <strong>Margen Frontal</strong>
                                        </span>
                                        <div class="count f-36" id="margenfrontal_all"></div>
                                        <span class="count_bottom" id="meta_mf_all"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-percent"></i> 
                                            <strong>CM</strong>
                                        </span>
                                        <div class="count f-36" id="cm_all"></div>
                                        <span class="count_bottom" id="meta_cm_all"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-percent"></i> 
                                            <strong>MP</strong>
                                        </span>
                                        <div class="count f-36" id="marcapropia_all"></div>
                                        <span class="count_bottom" id="meta_mp_all"></span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count m-b-30">
                                        <span class="count_top f-18">
                                            <i class="fa fa-line-chart"></i> 
                                            <strong>Comercial</strong>
                                        </span>
                                        <div class="count f-36" id="margenglobal_all"></div>
                                        <span class="count_bottom" id="meta_mg_all"></span>
                                    </div>
                                </div> --}}
                                {{-- <div class="table-responsive">
                                    <table class="table table-striped small">
                                        <thead>
                                            <tr class="f-16">
                                                <th>Linea</th>
                                                <th>Venta</th>
                                                <th>Margen Frontal</th>
                                                <th>% CM</th>
                                                <th>% MP</th>
                                                <th>Comercial</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">ASEO, HIGIENE Y OTROS</th>
                                                <td id="venta_aseo_all"></td>
                                                <td id="margen_aseo_all"></td>
                                                <td id="cm_aseo_all"></td>
                                                <td id="mp_aseo_all"></td>
                                                <td id="comercial_aseo_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">CAFETERIA Y MENAJE</th>
                                                <td id="venta_cafeteria_all"></td>
                                                <td id="margen_cafeteria_all"></td>
                                                <td id="cm_cafeteria_all"></td>
                                                <td id="mp_cafeteria_all"></td>
                                                <td id="comercial_cafeteria_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">CAJAS</th>
                                                <td id="venta_cajas_all"></td>
                                                <td id="margen_cajas_all"></td>
                                                <td id="cm_cajas_all"></td>
                                                <td id="mp_cajas_all"></td>
                                                <td id="comercial_cajas_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">LIBRERIA</th>
                                                <td id="venta_libreria_all"></td>
                                                <td id="margen_libreria_all"></td>
                                                <td id="cm_libreria_all"></td>
                                                <td id="mp_libreria_all"></td>
                                                <td id="comercial_libreria_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">OTRAS LINEAS Y FULFILLMENT</th>
                                                <td id="venta_nd_all"></td>
                                                <td id="margen_nd_all"></td>
                                                <td id="cm_nd_all"></td>
                                                <td id="mp_nd_all"></td>
                                                <td id="comercial_nd_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">PAPEL FOTOCOPIA</th>
                                                <td id="venta_papelfotocopia_all"></td>
                                                <td id="margen_papelfotocopia_all"></td>
                                                <td id="cm_papelfotocopia_all"></td>
                                                <td id="mp_papelfotocopia_all"></td>
                                                <td id="comercial_papelfotocopia_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">SEGURIDAD</th>
                                                <td id="venta_seguridad_all"></td>
                                                <td id="margen_seguridad_all"></td>
                                                <td id="cm_seguridad_all"></td>
                                                <td id="mp_seguridad_all"></td>
                                                <td id="comercial_seguridad_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">TECNOLOGIA</th>
                                                <td id="venta_tecnologia_all"></td>
                                                <td id="margen_tecnologia_all"></td>
                                                <td id="cm_tecnologia_all"></td>
                                                <td id="mp_tecnologia_all"></td>
                                                <td id="comercial_tecnologia_all"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">TISSUE</th>
                                                <td id="venta_tissue_all"></td>
                                                <td id="margen_tissue_all"></td>
                                                <td id="cm_tissue_all"></td>
                                                <td id="mp_tissue_all"></td>
                                                <td id="comercial_tissue_all"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            
            <div class="card">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="small">Mirada del día <?php echo date('d/m/Y')?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content carousel js-flickity"
                     data-flickity='{ "setGallerySize": false, "resize": false, "prevNextButtons": false, "wrapAround": true, "autoPlay": 10000 }'>
                    
                </div>
            </div>
                <div class="clearfix"></div>
                <div class="x_content">
        
                </div>
            </div>
            </div>
@endsection
@section('custom-includes')
    <!-- Editable-table js -->
    {{-- <script type="text/javascript" src="{{ asset('assets/pages/edit-table/jquery.tabledit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/edit-table/editable.js') }}"></script> --}}
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-hand-rock-o";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Acciones Agendadas";

            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
                        
            getDerechos();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
            //perfil();
            loadData();
        });

        function perfil() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            //console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('getDerechos')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                success: function (json) {
                    //console.log(json);
                    
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

        function loadData() {
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            $.ajax({
                data: parametros,
                url: '{{url('indicadores/dashBoard')}}', 
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    document.getElementById("ventadia_all").innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    document.getElementById('participacionecommerce_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    document.getElementById('participacionconvenios_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('ffds_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    document.getElementById('bole_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('tech_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    document.getElementById('anto_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    document.getElementById('cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenfrontal_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cm_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('marcapropia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenglobal_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";

                    // document.getElementById("ventadia_ge").innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionecommerce_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionconvenios_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('bole_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('anto_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenfrontal_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cm_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('marcapropia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenglobal_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";

                    // document.getElementById("ventadia_pyme").innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionecommerce_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionconvenios_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('bole_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('anto_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenfrontal_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cm_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('marcapropia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenglobal_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";

                    // document.getElementById("ventadia_corp").innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionecommerce_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionconvenios_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('bole_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('anto_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenfrontal_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cm_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('marcapropia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenglobal_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";

                    // document.getElementById("ventadia_gobierno").innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionecommerce_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('participacionconvenios_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('bole_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('anto_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenfrontal_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('cm_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('marcapropia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";
                    // document.getElementById('margenglobal_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"50\" width=\"50\" />";

                    // document.getElementById('venta_aseo_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_aseo_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_aseo_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_aseo_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_aseo_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cafeteria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cafeteria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cafeteria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cafeteria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cafeteria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cajas_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tecnologia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tecnologia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tecnologia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tecnologia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tecnologia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_libreria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_libreria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_libreria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_libreria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_libreria_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_nd_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_nd_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_nd_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_nd_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_nd_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_papelfotocopia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_papelfotocopia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_papelfotocopia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_papelfotocopia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_papelfotocopia_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_seguridad_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_seguridad_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_seguridad_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_seguridad_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_seguridad_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tissue_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tissue_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tissue_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tissue_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tissue_all').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_aseo_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_aseo_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_aseo_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_aseo_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_aseo_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cafeteria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cafeteria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cafeteria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cafeteria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cafeteria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cajas_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tecnologia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tecnologia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tecnologia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tecnologia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tecnologia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_libreria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_libreria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_libreria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_libreria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_libreria_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_nd_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_nd_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_nd_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_nd_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_nd_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_papelfotocopia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_papelfotocopia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_papelfotocopia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_papelfotocopia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_papelfotocopia_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_seguridad_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_seguridad_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_seguridad_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_seguridad_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_seguridad_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tissue_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tissue_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tissue_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tissue_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tissue_ge').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_aseo_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_aseo_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_aseo_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_aseo_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_aseo_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cafeteria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cafeteria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cafeteria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cafeteria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cafeteria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cajas_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tecnologia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tecnologia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tecnologia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tecnologia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tecnologia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_libreria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_libreria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_libreria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_libreria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_libreria_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_nd_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_nd_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_nd_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_nd_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_nd_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_papelfotocopia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_papelfotocopia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_papelfotocopia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_papelfotocopia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_papelfotocopia_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_seguridad_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_seguridad_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_seguridad_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_seguridad_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_seguridad_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tissue_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tissue_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tissue_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tissue_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tissue_pyme').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_aseo_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_aseo_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_aseo_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_aseo_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_aseo_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cafeteria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cafeteria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cafeteria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cafeteria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cafeteria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cajas_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tecnologia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tecnologia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tecnologia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tecnologia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tecnologia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_libreria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_libreria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_libreria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_libreria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_libreria_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_nd_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_nd_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_nd_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_nd_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_nd_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_papelfotocopia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_papelfotocopia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_papelfotocopia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_papelfotocopia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_papelfotocopia_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_seguridad_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_seguridad_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_seguridad_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_seguridad_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_seguridad_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tissue_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tissue_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tissue_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tissue_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tissue_corp').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_aseo_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_aseo_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_aseo_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_aseo_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_aseo_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cafeteria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cafeteria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cafeteria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cafeteria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cafeteria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_cajas_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tecnologia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tecnologia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tecnologia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tecnologia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tecnologia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_libreria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_libreria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_libreria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_libreria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_libreria_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_nd_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_nd_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_nd_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_nd_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_nd_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_papelfotocopia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_papelfotocopia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_papelfotocopia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_papelfotocopia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_papelfotocopia_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_seguridad_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_seguridad_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_seguridad_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_seguridad_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_seguridad_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                    // document.getElementById('venta_tissue_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('margen_tissue_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('cm_tissue_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('mp_tissue_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";
                    // document.getElementById('comercial_tissue_gobierno').innerHTML = "<img src=\"{{asset('/images/Spinner-1s-50px.gif')}}\" height=\"25\" width=\"25\" />";

                },
                success: function (json) {
                    if (Object.keys(json).length > 0) {
                        document.getElementById("ventadia_all").innerHTML = json.ventaDia_all;
                        document.getElementById('participacionecommerce_all').innerHTML = json.participacionEcommerce_all;
                        document.getElementById('participacionconvenios_all').innerHTML = json.participacionConvenio_all;
                        // document.getElementById('ffds_all').innerHTML = json.ffds_all;
                        document.getElementById('bole_all').innerHTML = json.bole_all;
                        // document.getElementById('tech_all').innerHTML = json.tech_all;
                        document.getElementById('anto_all').innerHTML = json.anto_all;
                        document.getElementById('cajas_all').innerHTML = json.cajas_all;
                        // document.getElementById('margenfrontal_all').innerHTML = json.margenfrontal_all;
                        // document.getElementById('cm_all').innerHTML = json.cm_all;
                        // document.getElementById('marcapropia_all').innerHTML = json.marcapropia_all;
                        // document.getElementById('margenglobal_all').innerHTML = json.margenglobal_all;

                        /*metas*/
                        // document.getElementById("meta_venta_all").innerHTML = json.meta_venta_all;
                        // document.getElementById("meta_mf_all").innerHTML = json.meta_mf_all;
                        // document.getElementById("meta_cm_all").innerHTML = json.meta_cm_all;
                        // document.getElementById("meta_mp_all").innerHTML = json.meta_mp_all;
                        // document.getElementById("meta_mg_all").innerHTML = json.meta_mg_all;
                        /*metas*/

                        // document.getElementById("ventadia_ge").innerHTML = json.ventaDia_ge;
                        // document.getElementById('participacionecommerce_ge').innerHTML = json.participacionEcommerce_ge;
                        // document.getElementById('participacionconvenios_ge').innerHTML = json.participacionConvenio_ge;
                        // document.getElementById('ffds_ge').innerHTML = json.ffds_ge;
                        // document.getElementById('bole_ge').innerHTML = json.bole_ge;
                        // document.getElementById('anto_ge').innerHTML = json.anto_ge;
                        // document.getElementById('cajas_ge').innerHTML = json.cajas_ge;
                        // document.getElementById('margenfrontal_ge').innerHTML = json.margenfrontal_ge;
                        // document.getElementById('cm_ge').innerHTML = json.cm_ge;
                        // document.getElementById('marcapropia_ge').innerHTML = json.marcapropia_ge;
                        // document.getElementById('margenglobal_ge').innerHTML = json.margenglobal_ge;

                        /*metas*/
                        // document.getElementById("meta_venta_ge").innerHTML = json.meta_venta_ge;
                        // document.getElementById("meta_mf_ge").innerHTML = json.meta_mf_ge;
                        // document.getElementById("meta_cm_ge").innerHTML = json.meta_cm_ge;
                        // document.getElementById("meta_mp_ge").innerHTML = json.meta_mp_ge;
                        // document.getElementById("meta_mg_ge").innerHTML = json.meta_mg_ge;
                        /*metas*/

                        // document.getElementById("ventadia_pyme").innerHTML = json.ventaDia_pyme;
                        // document.getElementById('participacionecommerce_pyme').innerHTML = json.participacionEcommerce_pyme;
                        // document.getElementById('participacionconvenios_pyme').innerHTML = json.participacionConvenio_pyme;
                        // document.getElementById('ffds_pyme').innerHTML = json.ffds_pyme;
                        // document.getElementById('bole_pyme').innerHTML = json.bole_pyme;
                        // document.getElementById('anto_pyme').innerHTML = json.anto_pyme;
                        // document.getElementById('cajas_pyme').innerHTML = json.cajas_pyme;
                        // document.getElementById('margenfrontal_pyme').innerHTML = json.margenfrontal_pyme;
                        // document.getElementById('cm_pyme').innerHTML = json.cm_pyme;
                        // document.getElementById('marcapropia_pyme').innerHTML = json.marcapropia_pyme;
                        // document.getElementById('margenglobal_pyme').innerHTML = json.margenglobal_pyme;

                        /*metas*/
                        // document.getElementById("meta_venta_pyme").innerHTML = json.meta_venta_pyme;
                        // document.getElementById("meta_mf_pyme").innerHTML = json.meta_mf_pyme;
                        // document.getElementById("meta_cm_pyme").innerHTML = json.meta_cm_pyme;
                        // document.getElementById("meta_mp_pyme").innerHTML = json.meta_mp_pyme;
                        // document.getElementById("meta_mg_pyme").innerHTML = json.meta_mg_pyme;
                        /*metas*/

                        // document.getElementById("ventadia_corp").innerHTML = json.ventaDia_corp;
                        // document.getElementById('participacionecommerce_corp').innerHTML = json.participacionEcommerce_corp;
                        // document.getElementById('participacionconvenios_corp').innerHTML = json.participacionConvenio_corp;
                        // document.getElementById('ffds_corp').innerHTML = json.ffds_corp;
                        // document.getElementById('bole_corp').innerHTML = json.bole_corp;
                        // document.getElementById('anto_corp').innerHTML = json.anto_corp;
                        // document.getElementById('cajas_corp').innerHTML = json.cajas_corp;
                        // document.getElementById('margenfrontal_corp').innerHTML = json.margenfrontal_corp;
                        // document.getElementById('cm_corp').innerHTML = json.cm_corp;
                        // document.getElementById('marcapropia_corp').innerHTML = json.marcapropia_corp;
                        // document.getElementById('margenglobal_corp').innerHTML = json.margenglobal_corp;

                        /*metas*/
                        // document.getElementById("meta_venta_corp").innerHTML = json.meta_venta_corp;
                        // document.getElementById("meta_mf_corp").innerHTML = json.meta_mf_corp;
                        // document.getElementById("meta_cm_corp").innerHTML = json.meta_cm_corp;
                        // document.getElementById("meta_mp_corp").innerHTML = json.meta_mp_corp;
                        // document.getElementById("meta_mg_corp").innerHTML = json.meta_mg_corp;
                        /*metas*/

                        // document.getElementById("ventadia_gobierno").innerHTML = json.ventaDia_gobierno;
                        // document.getElementById('participacionecommerce_gobierno').innerHTML = json.participacionEcommerce_gobierno;
                        // document.getElementById('participacionconvenios_gobierno').innerHTML = json.participacionConvenio_gobierno;
                        // document.getElementById('ffds_gobierno').innerHTML = json.ffds_gobierno;
                        // document.getElementById('bole_gobierno').innerHTML = json.bole_gobierno;
                        // document.getElementById('anto_gobierno').innerHTML = json.anto_gobierno;
                        // document.getElementById('cajas_gobierno').innerHTML = json.cajas_gobierno;
                        // document.getElementById('margenfrontal_gobierno').innerHTML = json.margenfrontal_gobierno;
                        // document.getElementById('cm_gobierno').innerHTML = json.cm_gobierno;
                        // document.getElementById('marcapropia_gobierno').innerHTML = json.marcapropia_gobierno;
                        // document.getElementById('margenglobal_gobierno').innerHTML = json.margenglobal_gobierno;

                        /*metas*/
                        // document.getElementById("meta_venta_gobierno").innerHTML = json.meta_venta_gobierno;
                        // document.getElementById("meta_mf_gobierno").innerHTML = json.meta_mf_gobierno;
                        // document.getElementById("meta_cm_gobierno").innerHTML = json.meta_cm_gobierno;
                        // document.getElementById("meta_mp_gobierno").innerHTML = json.meta_mp_gobierno;
                        // document.getElementById("meta_mg_gobierno").innerHTML = json.meta_mg_gobierno;
                        /*metas*/

                        // document.getElementById('venta_aseo_all').innerHTML = json.venta_aseo_all;
                        // document.getElementById('margen_aseo_all').innerHTML = json.margen_aseo_all;
                        // document.getElementById('cm_aseo_all').innerHTML = json.cm_aseo_all;
                        // document.getElementById('mp_aseo_all').innerHTML = json.mp_aseo_all;
                        // document.getElementById('comercial_aseo_all').innerHTML = json.comercial_aseo_all;

                        // document.getElementById('venta_cafeteria_all').innerHTML = json.venta_cafeteria_all;
                        // document.getElementById('margen_cafeteria_all').innerHTML = json.margen_cafeteria_all;
                        // document.getElementById('cm_cafeteria_all').innerHTML = json.cm_cafeteria_all;
                        // document.getElementById('mp_cafeteria_all').innerHTML = json.mp_cafeteria_all;
                        // document.getElementById('comercial_cafeteria_all').innerHTML = json.comercial_cafeteria_all;

                        // document.getElementById('venta_cajas_all').innerHTML = json.venta_cajas_all;
                        // document.getElementById('margen_cajas_all').innerHTML = json.margen_cajas_all;
                        // document.getElementById('cm_cajas_all').innerHTML = json.cm_cajas_all;
                        // document.getElementById('mp_cajas_all').innerHTML = json.mp_cajas_all;
                        // document.getElementById('comercial_cajas_all').innerHTML = json.comercial_cajas_all;

                        // document.getElementById('venta_tecnologia_all').innerHTML = json.venta_tecnologia_all;
                        // document.getElementById('margen_tecnologia_all').innerHTML = json.margen_tecnologia_all;
                        // document.getElementById('cm_tecnologia_all').innerHTML = json.cm_tecnologia_all;
                        // document.getElementById('mp_tecnologia_all').innerHTML = json.mp_tecnologia_all;
                        // document.getElementById('comercial_tecnologia_all').innerHTML = json.comercial_tecnologia_all;

                        // document.getElementById('venta_libreria_all').innerHTML = json.venta_libreria_all;
                        // document.getElementById('margen_libreria_all').innerHTML = json.margen_libreria_all;
                        // document.getElementById('cm_libreria_all').innerHTML = json.cm_libreria_all;
                        // document.getElementById('mp_libreria_all').innerHTML = json.mp_libreria_all;
                        // document.getElementById('comercial_libreria_all').innerHTML = json.comercial_libreria_all;

                        // document.getElementById('venta_nd_all').innerHTML = json.venta_nd_all;
                        // document.getElementById('margen_nd_all').innerHTML = json.margen_nd_all;
                        // document.getElementById('cm_nd_all').innerHTML = json.cm_nd_all;
                        // document.getElementById('mp_nd_all').innerHTML = json.mp_nd_all;
                        // document.getElementById('comercial_nd_all').innerHTML = json.comercial_nd_all;

                        // document.getElementById('venta_papelfotocopia_all').innerHTML = json.venta_papelfotocopia_all;
                        // document.getElementById('margen_papelfotocopia_all').innerHTML = json.margen_papelfotocopia_all;
                        // document.getElementById('cm_papelfotocopia_all').innerHTML = json.cm_papelfotocopia_all;
                        // document.getElementById('mp_papelfotocopia_all').innerHTML = json.mp_papelfotocopia_all;
                        // document.getElementById('comercial_papelfotocopia_all').innerHTML = json.comercial_papelfotocopia_all;

                        // document.getElementById('venta_seguridad_all').innerHTML = json.venta_seguridad_all;
                        // document.getElementById('margen_seguridad_all').innerHTML = json.margen_seguridad_all;
                        // document.getElementById('cm_seguridad_all').innerHTML = json.cm_seguridad_all;
                        // document.getElementById('mp_seguridad_all').innerHTML = json.mp_seguridad_all;
                        // document.getElementById('comercial_seguridad_all').innerHTML = json.comercial_seguridad_all;

                        // document.getElementById('venta_tissue_all').innerHTML = json.venta_tissue_all;
                        // document.getElementById('margen_tissue_all').innerHTML = json.margen_tissue_all;
                        // document.getElementById('cm_tissue_all').innerHTML = json.cm_tissue_all;
                        // document.getElementById('mp_tissue_all').innerHTML = json.mp_tissue_all;
                        // document.getElementById('comercial_tissue_all').innerHTML = json.comercial_tissue_all;

                        // document.getElementById('venta_aseo_ge').innerHTML = json.venta_aseo_ge;
                        // document.getElementById('margen_aseo_ge').innerHTML = json.margen_aseo_ge;
                        // document.getElementById('cm_aseo_ge').innerHTML = json.cm_aseo_ge;
                        // document.getElementById('mp_aseo_ge').innerHTML = json.mp_aseo_ge;
                        // document.getElementById('comercial_aseo_ge').innerHTML = json.comercial_aseo_ge;

                        // document.getElementById('venta_cafeteria_ge').innerHTML = json.venta_cafeteria_ge;
                        // document.getElementById('margen_cafeteria_ge').innerHTML = json.margen_cafeteria_ge;
                        // document.getElementById('cm_cafeteria_ge').innerHTML = json.cm_cafeteria_ge;
                        // document.getElementById('mp_cafeteria_ge').innerHTML = json.mp_cafeteria_ge;
                        // document.getElementById('comercial_cafeteria_ge').innerHTML = json.comercial_cafeteria_ge;

                        // document.getElementById('venta_cajas_ge').innerHTML = json.venta_cajas_ge;
                        // document.getElementById('margen_cajas_ge').innerHTML = json.margen_cajas_ge;
                        // document.getElementById('cm_cajas_ge').innerHTML = json.cm_cajas_ge;
                        // document.getElementById('mp_cajas_ge').innerHTML = json.mp_cajas_ge;
                        // document.getElementById('comercial_cajas_ge').innerHTML = json.comercial_cajas_ge;

                        // document.getElementById('venta_tecnologia_ge').innerHTML = json.venta_tecnologia_ge;
                        // document.getElementById('margen_tecnologia_ge').innerHTML = json.margen_tecnologia_ge;
                        // document.getElementById('cm_tecnologia_ge').innerHTML = json.cm_tecnologia_ge;
                        // document.getElementById('mp_tecnologia_ge').innerHTML = json.mp_tecnologia_ge;
                        // document.getElementById('comercial_tecnologia_ge').innerHTML = json.comercial_tecnologia_ge;

                        // document.getElementById('venta_libreria_ge').innerHTML = json.venta_libreria_ge;
                        // document.getElementById('margen_libreria_ge').innerHTML = json.margen_libreria_ge;
                        // document.getElementById('cm_libreria_ge').innerHTML = json.cm_libreria_ge;
                        // document.getElementById('mp_libreria_ge').innerHTML = json.mp_libreria_ge;
                        // document.getElementById('comercial_libreria_ge').innerHTML = json.comercial_libreria_ge;

                        // document.getElementById('venta_nd_ge').innerHTML = json.venta_nd_ge;
                        // document.getElementById('margen_nd_ge').innerHTML = json.margen_nd_ge;
                        // document.getElementById('cm_nd_ge').innerHTML = json.cm_nd_ge;
                        // document.getElementById('mp_nd_ge').innerHTML = json.mp_nd_ge;
                        // document.getElementById('comercial_nd_ge').innerHTML = json.comercial_nd_ge;

                        // document.getElementById('venta_papelfotocopia_ge').innerHTML = json.venta_papelfotocopia_ge;
                        // document.getElementById('margen_papelfotocopia_ge').innerHTML = json.margen_papelfotocopia_ge;
                        // document.getElementById('cm_papelfotocopia_ge').innerHTML = json.cm_papelfotocopia_ge;
                        // document.getElementById('mp_papelfotocopia_ge').innerHTML = json.mp_papelfotocopia_ge;
                        // document.getElementById('comercial_papelfotocopia_ge').innerHTML = json.comercial_papelfotocopia_ge;

                        // document.getElementById('venta_seguridad_ge').innerHTML = json.venta_seguridad_ge;
                        // document.getElementById('margen_seguridad_ge').innerHTML = json.margen_seguridad_ge;
                        // document.getElementById('cm_seguridad_ge').innerHTML = json.cm_seguridad_ge;
                        // document.getElementById('mp_seguridad_ge').innerHTML = json.mp_seguridad_ge;
                        // document.getElementById('comercial_seguridad_ge').innerHTML = json.comercial_seguridad_ge;

                        // document.getElementById('venta_tissue_ge').innerHTML = json.venta_tissue_ge;
                        // document.getElementById('margen_tissue_ge').innerHTML = json.margen_tissue_ge;
                        // document.getElementById('cm_tissue_ge').innerHTML = json.cm_tissue_ge;
                        // document.getElementById('mp_tissue_ge').innerHTML = json.mp_tissue_ge;
                        // document.getElementById('comercial_tissue_ge').innerHTML = json.comercial_tissue_ge;

                        // document.getElementById('venta_aseo_pyme').innerHTML = json.venta_aseo_pyme;
                        // document.getElementById('margen_aseo_pyme').innerHTML = json.margen_aseo_pyme;
                        // document.getElementById('cm_aseo_pyme').innerHTML = json.cm_aseo_pyme;
                        // document.getElementById('mp_aseo_pyme').innerHTML = json.mp_aseo_pyme;
                        // document.getElementById('comercial_aseo_pyme').innerHTML = json.comercial_aseo_pyme;

                        // document.getElementById('venta_cafeteria_pyme').innerHTML = json.venta_cafeteria_pyme;
                        // document.getElementById('margen_cafeteria_pyme').innerHTML = json.margen_cafeteria_pyme;
                        // document.getElementById('cm_cafeteria_pyme').innerHTML = json.cm_cafeteria_pyme;
                        // document.getElementById('mp_cafeteria_pyme').innerHTML = json.mp_cafeteria_pyme;
                        // document.getElementById('comercial_cafeteria_pyme').innerHTML = json.comercial_cafeteria_pyme;

                        // document.getElementById('venta_cajas_pyme').innerHTML = json.venta_cajas_pyme;
                        // document.getElementById('margen_cajas_pyme').innerHTML = json.margen_cajas_pyme;
                        // document.getElementById('cm_cajas_pyme').innerHTML = json.cm_cajas_pyme;
                        // document.getElementById('mp_cajas_pyme').innerHTML = json.mp_cajas_pyme;
                        // document.getElementById('comercial_cajas_pyme').innerHTML = json.comercial_cajas_pyme;

                        // document.getElementById('venta_tecnologia_pyme').innerHTML = json.venta_tecnologia_pyme;
                        // document.getElementById('margen_tecnologia_pyme').innerHTML = json.margen_tecnologia_pyme;
                        // document.getElementById('cm_tecnologia_pyme').innerHTML = json.cm_tecnologia_pyme;
                        // document.getElementById('mp_tecnologia_pyme').innerHTML = json.mp_tecnologia_pyme;
                        // document.getElementById('comercial_tecnologia_pyme').innerHTML = json.comercial_tecnologia_pyme;

                        // document.getElementById('venta_libreria_pyme').innerHTML = json.venta_libreria_pyme;
                        // document.getElementById('margen_libreria_pyme').innerHTML = json.margen_libreria_pyme;
                        // document.getElementById('cm_libreria_pyme').innerHTML = json.cm_libreria_pyme;
                        // document.getElementById('mp_libreria_pyme').innerHTML = json.mp_libreria_pyme;
                        // document.getElementById('comercial_libreria_pyme').innerHTML = json.comercial_libreria_pyme;

                        // document.getElementById('venta_nd_pyme').innerHTML = json.venta_nd_pyme;
                        // document.getElementById('margen_nd_pyme').innerHTML = json.margen_nd_pyme;
                        // document.getElementById('cm_nd_pyme').innerHTML = json.cm_nd_pyme;
                        // document.getElementById('mp_nd_pyme').innerHTML = json.mp_nd_pyme;
                        // document.getElementById('comercial_nd_pyme').innerHTML = json.comercial_nd_pyme;

                        // document.getElementById('venta_papelfotocopia_pyme').innerHTML = json.venta_papelfotocopia_pyme;
                        // document.getElementById('margen_papelfotocopia_pyme').innerHTML = json.margen_papelfotocopia_pyme;
                        // document.getElementById('cm_papelfotocopia_pyme').innerHTML = json.cm_papelfotocopia_pyme;
                        // document.getElementById('mp_papelfotocopia_pyme').innerHTML = json.mp_papelfotocopia_pyme;
                        // document.getElementById('comercial_papelfotocopia_pyme').innerHTML = json.comercial_papelfotocopia_pyme;

                        // document.getElementById('venta_seguridad_pyme').innerHTML = json.venta_seguridad_pyme;
                        // document.getElementById('margen_seguridad_pyme').innerHTML = json.margen_seguridad_pyme;
                        // document.getElementById('cm_seguridad_pyme').innerHTML = json.cm_seguridad_pyme;
                        // document.getElementById('mp_seguridad_pyme').innerHTML = json.mp_seguridad_pyme;
                        // document.getElementById('comercial_seguridad_pyme').innerHTML = json.comercial_seguridad_pyme;

                        // document.getElementById('venta_tissue_pyme').innerHTML = json.venta_tissue_pyme;
                        // document.getElementById('margen_tissue_pyme').innerHTML = json.margen_tissue_pyme;
                        // document.getElementById('cm_tissue_pyme').innerHTML = json.cm_tissue_pyme;
                        // document.getElementById('mp_tissue_pyme').innerHTML = json.mp_tissue_pyme;
                        // document.getElementById('comercial_tissue_pyme').innerHTML = json.comercial_tissue_pyme;

                        // document.getElementById('venta_aseo_corp').innerHTML = json.venta_aseo_corp;
                        // document.getElementById('margen_aseo_corp').innerHTML = json.margen_aseo_corp;
                        // document.getElementById('cm_aseo_corp').innerHTML = json.cm_aseo_corp;
                        // document.getElementById('mp_aseo_corp').innerHTML = json.mp_aseo_corp;
                        // document.getElementById('comercial_aseo_corp').innerHTML = json.comercial_aseo_corp;

                        // document.getElementById('venta_cafeteria_corp').innerHTML = json.venta_cafeteria_corp;
                        // document.getElementById('margen_cafeteria_corp').innerHTML = json.margen_cafeteria_corp;
                        // document.getElementById('cm_cafeteria_corp').innerHTML = json.cm_cafeteria_corp;
                        // document.getElementById('mp_cafeteria_corp').innerHTML = json.mp_cafeteria_corp;
                        // document.getElementById('comercial_cafeteria_corp').innerHTML = json.comercial_cafeteria_corp;

                        // document.getElementById('venta_cajas_corp').innerHTML = json.venta_cajas_corp;
                        // document.getElementById('margen_cajas_corp').innerHTML = json.margen_cajas_corp;
                        // document.getElementById('cm_cajas_corp').innerHTML = json.cm_cajas_corp;
                        // document.getElementById('mp_cajas_corp').innerHTML = json.mp_cajas_corp;
                        // document.getElementById('comercial_cajas_corp').innerHTML = json.comercial_cajas_corp;

                        // document.getElementById('venta_tecnologia_corp').innerHTML = json.venta_tecnologia_corp;
                        // document.getElementById('margen_tecnologia_corp').innerHTML = json.margen_tecnologia_corp;
                        // document.getElementById('cm_tecnologia_corp').innerHTML = json.cm_tecnologia_corp;
                        // document.getElementById('mp_tecnologia_corp').innerHTML = json.mp_tecnologia_corp;
                        // document.getElementById('comercial_tecnologia_corp').innerHTML = json.comercial_tecnologia_corp;

                        // document.getElementById('venta_libreria_corp').innerHTML = json.venta_libreria_corp;
                        // document.getElementById('margen_libreria_corp').innerHTML = json.margen_libreria_corp;
                        // document.getElementById('cm_libreria_corp').innerHTML = json.cm_libreria_corp;
                        // document.getElementById('mp_libreria_corp').innerHTML = json.mp_libreria_corp;
                        // document.getElementById('comercial_libreria_corp').innerHTML = json.comercial_libreria_corp;

                        // document.getElementById('venta_nd_corp').innerHTML = json.venta_nd_corp;
                        // document.getElementById('margen_nd_corp').innerHTML = json.margen_nd_corp;
                        // document.getElementById('cm_nd_corp').innerHTML = json.cm_nd_corp;
                        // document.getElementById('mp_nd_corp').innerHTML = json.mp_nd_corp;
                        // document.getElementById('comercial_nd_corp').innerHTML = json.comercial_nd_corp;

                        // document.getElementById('venta_papelfotocopia_corp').innerHTML = json.venta_papelfotocopia_corp;
                        // document.getElementById('margen_papelfotocopia_corp').innerHTML = json.margen_papelfotocopia_corp;
                        // document.getElementById('cm_papelfotocopia_corp').innerHTML = json.cm_papelfotocopia_corp;
                        // document.getElementById('mp_papelfotocopia_corp').innerHTML = json.mp_papelfotocopia_corp;
                        // document.getElementById('comercial_papelfotocopia_corp').innerHTML = json.comercial_papelfotocopia_corp;

                        // document.getElementById('venta_seguridad_corp').innerHTML = json.venta_seguridad_corp;
                        // document.getElementById('margen_seguridad_corp').innerHTML = json.margen_seguridad_corp;
                        // document.getElementById('cm_seguridad_corp').innerHTML = json.cm_seguridad_corp;
                        // document.getElementById('mp_seguridad_corp').innerHTML = json.mp_seguridad_corp;
                        // document.getElementById('comercial_seguridad_corp').innerHTML = json.comercial_seguridad_corp;

                        // document.getElementById('venta_tissue_corp').innerHTML = json.venta_tissue_corp;
                        // document.getElementById('margen_tissue_corp').innerHTML = json.margen_tissue_corp;
                        // document.getElementById('cm_tissue_corp').innerHTML = json.cm_tissue_corp;
                        // document.getElementById('mp_tissue_corp').innerHTML = json.mp_tissue_corp;
                        // document.getElementById('comercial_tissue_corp').innerHTML = json.comercial_tissue_corp;

                        // document.getElementById('venta_aseo_gobierno').innerHTML = json.venta_aseo_gobierno;
                        // document.getElementById('margen_aseo_gobierno').innerHTML = json.margen_aseo_gobierno;
                        // document.getElementById('cm_aseo_gobierno').innerHTML = json.cm_aseo_gobierno;
                        // document.getElementById('mp_aseo_gobierno').innerHTML = json.mp_aseo_gobierno;
                        // document.getElementById('comercial_aseo_gobierno').innerHTML = json.comercial_aseo_gobierno;

                        // document.getElementById('venta_cafeteria_gobierno').innerHTML = json.venta_cafeteria_gobierno;
                        // document.getElementById('margen_cafeteria_gobierno').innerHTML = json.margen_cafeteria_gobierno;
                        // document.getElementById('cm_cafeteria_gobierno').innerHTML = json.cm_cafeteria_gobierno;
                        // document.getElementById('mp_cafeteria_gobierno').innerHTML = json.mp_cafeteria_gobierno;
                        // document.getElementById('comercial_cafeteria_gobierno').innerHTML = json.comercial_cafeteria_gobierno;

                        // document.getElementById('venta_cajas_gobierno').innerHTML = json.venta_cajas_gobierno;
                        // document.getElementById('margen_cajas_gobierno').innerHTML = json.margen_cajas_gobierno;
                        // document.getElementById('cm_cajas_gobierno').innerHTML = json.cm_cajas_gobierno;
                        // document.getElementById('mp_cajas_gobierno').innerHTML = json.mp_cajas_gobierno;
                        // document.getElementById('comercial_cajas_gobierno').innerHTML = json.comercial_cajas_gobierno;

                        // document.getElementById('venta_tecnologia_gobierno').innerHTML = json.venta_tecnologia_gobierno;
                        // document.getElementById('margen_tecnologia_gobierno').innerHTML = json.margen_tecnologia_gobierno;
                        // document.getElementById('cm_tecnologia_gobierno').innerHTML = json.cm_tecnologia_gobierno;
                        // document.getElementById('mp_tecnologia_gobierno').innerHTML = json.mp_tecnologia_gobierno;
                        // document.getElementById('comercial_tecnologia_gobierno').innerHTML = json.comercial_tecnologia_gobierno;

                        // document.getElementById('venta_libreria_gobierno').innerHTML = json.venta_libreria_gobierno;
                        // document.getElementById('margen_libreria_gobierno').innerHTML = json.margen_libreria_gobierno;
                        // document.getElementById('cm_libreria_gobierno').innerHTML = json.cm_libreria_gobierno;
                        // document.getElementById('mp_libreria_gobierno').innerHTML = json.mp_libreria_gobierno;
                        // document.getElementById('comercial_libreria_gobierno').innerHTML = json.comercial_libreria_gobierno;

                        // document.getElementById('venta_nd_gobierno').innerHTML = json.venta_nd_gobierno;
                        // document.getElementById('margen_nd_gobierno').innerHTML = json.margen_nd_gobierno;
                        // document.getElementById('cm_nd_gobierno').innerHTML = json.cm_nd_gobierno;
                        // document.getElementById('mp_nd_gobierno').innerHTML = json.mp_nd_gobierno;
                        // document.getElementById('comercial_nd_gobierno').innerHTML = json.comercial_nd_gobierno;

                        // document.getElementById('venta_papelfotocopia_gobierno').innerHTML = json.venta_papelfotocopia_gobierno;
                        // document.getElementById('margen_papelfotocopia_gobierno').innerHTML = json.margen_papelfotocopia_gobierno;
                        // document.getElementById('cm_papelfotocopia_gobierno').innerHTML = json.cm_papelfotocopia_gobierno;
                        // document.getElementById('mp_papelfotocopia_gobierno').innerHTML = json.mp_papelfotocopia_gobierno;
                        // document.getElementById('comercial_papelfotocopia_gobierno').innerHTML = json.comercial_papelfotocopia_gobierno;

                        // document.getElementById('venta_seguridad_gobierno').innerHTML = json.venta_seguridad_gobierno;
                        // document.getElementById('margen_seguridad_gobierno').innerHTML = json.margen_seguridad_gobierno;
                        // document.getElementById('cm_seguridad_gobierno').innerHTML = json.cm_seguridad_gobierno;
                        // document.getElementById('mp_seguridad_gobierno').innerHTML = json.mp_seguridad_gobierno;
                        // document.getElementById('comercial_seguridad_gobierno').innerHTML = json.comercial_seguridad_gobierno;

                        // document.getElementById('venta_tissue_gobierno').innerHTML = json.venta_tissue_gobierno;
                        // document.getElementById('margen_tissue_gobierno').innerHTML = json.margen_tissue_gobierno;
                        // document.getElementById('cm_tissue_gobierno').innerHTML = json.cm_tissue_gobierno;
                        // document.getElementById('mp_tissue_gobierno').innerHTML = json.mp_tissue_gobierno;
                        // document.getElementById('comercial_tissue_gobierno').innerHTML = json.comercial_tissue_gobierno;


                        //totales
                        document.getElementById('count_total').innerHTML = json.count_total;
                        // document.getElementById('count_ge').innerHTML = json.count_ge;
                        // document.getElementById('count_pyme').innerHTML = json.count_pyme;
                        // document.getElementById('count_corp').innerHTML = json.count_corp;
                        // document.getElementById('count_gob').innerHTML = json.count_gob;
                    }
                }
                , error: function (e) {
                    console.log(e.message);
                }
            });
        }

    </script>
@endsection