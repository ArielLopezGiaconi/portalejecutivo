<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 30/08/2018
 * Time: 12:09
 */

namespace App\PDO\WebServices;

use App\Entities\SapWs\Request\MtMm021ReqOut;
use App\Entities\SapWs\Request\MtMm024ReqOut;
use App\Entities\SapWs\Response\MtMm021ResOut;
use App\Entities\SapWs\Request\MtMm025ReqOut;
use App\Entities\SapWs\Response\MtMm025ResOut;
use \DB;
use Illuminate\Support\Collection;


class StockWS extends WS
{
    public function getStockSpecificProduct($in_material, $in_centro, $in_almacen)
    {
        $webService = new MtMm021ReqOut();
        $webService->setMaterial($in_material);
        $webService->setCentro($in_centro);
        $webService->setAlmacen($in_almacen);

        // dd($webService->getRequestXml());

        $response = $this->consumeWSCurl(
            $webService->getRequestXml(),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM021'
        );

        $responseXml = new MtMm021ResOut($response);
        return $responseXml;
    }

    // Colección:
    // -> material
    // -> almacen
    // -> centro
    public function getStockManyProducts(array $in_materiales, $in_centro, $in_almacen)
    {
        $webService = new MtMm021ReqOut();
        $webService->setMateriales($in_materiales);
        $webService->setCentro($in_centro);
        $webService->setAlmacen($in_almacen);

        $response = $this->consumeWSCurl(
            $webService->getRequestXmlMultipleMaterial(),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM021'
        );

        $responseXml = new MtMm021ResOut($response);
        return $responseXml;
    }

    public function getStockAllProducts($in_centro, $in_almacen)
    {
        $webService = new MtMm021ReqOut();
        $webService->setCentro($in_centro);
        $webService->setAlmacen($in_almacen);

        $response = $this->consumeWSCurl(
            $webService->getRequestXml(),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM021'
        );

        if (!$response) {
            throw new \Exception("No se pudo cargar el XML de SAP");
        }

        $responseXml = new MtMm021ResOut($response);
        return $responseXml;
    }

    public function setStockFromVeracore($in_material, $in_centro, $in_almacen, $in_stock)
    {
        $webService = new MtMm025ReqOut();
        $webService->setMaterial($in_material);
        $webService->setCentro($in_centro);
        $webService->setAlmacen($in_almacen);
        $webService->setStock($in_stock);

        $response = $this->consumeWSCurl(
            $webService->getRequestXml(),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM025'
        );

        $responseXml = new MtMm025ResOut($response);
        return $responseXml;
    }

    public function setAllStockFromVeracore(array $in_data)
    {
        $webService = new MtMm025ReqOut();
        $response = $this->consumeWSCurl(
            $webService->getRequestXmlMultiple($in_data),
            $webService->getHeaderUrlFull(),
            $webService->getHeaderMethod(),
            'MTMM025'
        );

        $responseXml = new MtMm025ResOut($response);
        return $responseXml;
    }
}