<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 10/08/2018
 * Time: 19:42
 */

namespace App\Entities\Oracle\DMVentas;


class QvControlMargen implements \JsonSerializable
{
    private $cantid;
    private $cencos;
    private $clieconvenio;
    private $codcom;
    private $codemp;
    private $codlis;
    private $codpro;
    private $codven;
    private $cosprom;
    private $costo;
    private $ctocte;
    private $exento;
    private $fecha;
    private $foliodoc;
    private $internet;
    private $linea;
    private $mpropia_hst;
    private $neto;
    private $nomreg;
    private $numcot;
    private $numnvt;
    private $ordweb;
    private $prodconvenio;
    private $rutcli;
    private $suc_destino;
    private $suc_ori;
    private $supervisor;
    private $tip_cos;
    private $tipo;
    private $tipweb;

    public function getCantid()
    {
        return $this->cantid;
    }

    public function setCantid($cantid)
    {
        $this->cantid = $cantid;
    }

    public function getCencos()
    {
        return $this->cencos;
    }

    public function setCencos($cencos)
    {
        $this->cencos = $cencos;
    }

    public function getClieconvenio()
    {
        return $this->clieconvenio;
    }

    public function setClieconvenio($clieconvenio)
    {
        $this->clieconvenio = $clieconvenio;
    }

    public function getCodcom()
    {
        return $this->codcom;
    }

    public function setCodcom($codcom)
    {
        $this->codcom = $codcom;
    }

    public function getCodemp()
    {
        return $this->codemp;
    }

    public function setCodemp($codemp)
    {
        $this->codemp = $codemp;
    }

    public function getCodlis()
    {
        return $this->codlis;
    }

    public function setCodlis($codlis)
    {
        $this->codlis = $codlis;
    }

    public function getCodpro()
    {
        return $this->codpro;
    }

    public function setCodpro($codpro)
    {
        $this->codpro = $codpro;
    }

    public function getCodven()
    {
        return $this->codven;
    }

    public function setCodven($codven)
    {
        $this->codven = $codven;
    }

    public function getCosprom()
    {
        return $this->cosprom;
    }

    public function setCosprom($cosprom)
    {
        $this->cosprom = $cosprom;
    }

    public function getCosto()
    {
        return $this->costo;
    }

    public function setCosto($costo)
    {
        $this->costo = $costo;
    }

    public function getCtocte()
    {
        return $this->ctocte;
    }

    public function setCtocte($ctocte)
    {
        $this->ctocte = $ctocte;
    }

    public function getExento()
    {
        return $this->exento;
    }

    public function setExento($exento)
    {
        $this->exento = $exento;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getFoliodoc()
    {
        return $this->foliodoc;
    }

    public function setFoliodoc($foliodoc)
    {
        $this->foliodoc = $foliodoc;
    }

    public function getInternet()
    {
        return $this->internet;
    }

    public function setInternet($internet)
    {
        $this->internet = $internet;
    }

    public function getLinea()
    {
        return $this->linea;
    }

    public function setLinea($linea)
    {
        $this->linea = $linea;
    }

    public function getMpropiaHst()
    {
        return $this->mpropia_hst;
    }

    public function setMpropiaHst($mpropia_hst)
    {
        $this->mpropia_hst = $mpropia_hst;
    }

    public function getNeto()
    {
        return $this->neto;
    }

    public function setNeto($neto)
    {
        $this->neto = $neto;
    }

    public function getNomreg()
    {
        return $this->nomreg;
    }

    public function setNomreg($nomreg)
    {
        $this->nomreg = $nomreg;
    }

    public function getNumcot()
    {
        return $this->numcot;
    }

    public function setNumcot($numcot)
    {
        $this->numcot = $numcot;
    }

    public function getNumnvt()
    {
        return $this->numnvt;
    }

    public function setNumnvt($numnvt)
    {
        $this->numnvt = $numnvt;
    }

    public function getOrdweb()
    {
        return $this->ordweb;
    }

    public function setOrdweb($ordweb)
    {
        $this->ordweb = $ordweb;
    }

    public function getProdconvenio()
    {
        return $this->prodconvenio;
    }

    public function setProdconvenio($prodconvenio)
    {
        $this->prodconvenio = $prodconvenio;
    }

    public function getRutcli()
    {
        return $this->rutcli;
    }

    public function setRutcli($rutcli)
    {
        $this->rutcli = $rutcli;
    }

    public function getSucDestino()
    {
        return $this->suc_destino;
    }

    public function setSucDestino($suc_destino)
    {
        $this->suc_destino = $suc_destino;
    }

    public function getSucOri()
    {
        return $this->suc_ori;
    }

    public function setSucOri($suc_ori)
    {
        $this->suc_ori = $suc_ori;
    }

    public function getSupervisor()
    {
        return $this->supervisor;
    }

    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;
    }

    public function getTipCos()
    {
        return $this->tip_cos;
    }

    public function setTipCos($tip_cos)
    {
        $this->tip_cos = $tip_cos;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function getTipweb()
    {
        return $this->tipweb;
    }

    public function setTipweb($tipweb)
    {
        $this->tipweb = $tipweb;
    }


    public function jsonSerialize()
    {
        return [
            'cantid' => $this->getCantid(),
            'cencos' => $this->getCencos(),
            'clieconvenio' => $this->getClieconvenio(),
            'codcom' => $this->getCodcom(),
            'codemp' => $this->getCodemp(),
            'codlis' => $this->getCodlis(),
            'codpro' => $this->getCodpro(),
            'codven' => $this->getCodven(),
            'cosprom' => $this->getCosprom(),
            'costo' => $this->getCosto(),
            'ctocte' => $this->getCtocte(),
            'exento' => $this->getExento(),
            'fecha' => $this->getFecha(),
            'foliodoc' => $this->getFoliodoc(),
            'internet' => $this->getInternet(),
            'linea' => $this->getLinea(),
            'mpropia_hst' => $this->getMpropiaHst(),
            'neto' => $this->getNeto(),
            'nomreg' => $this->getNomreg(),
            'numcot' => $this->getNumcot(),
            'numnvt' => $this->getNumnvt(),
            'ordweb' => $this->getOrdweb(),
            'prodconvenio' => $this->getProdconvenio(),
            'rutcli' => $this->getRutcli(),
            'suc_destino' => $this->getSucDestino(),
            'suc_ori' => $this->getSucOri(),
            'supervisor' => $this->getSupervisor(),
            'tip_cos' => $this->getTipCos(),
            'tipo' => $this->getTipo(),
            'tipweb' => $this->getTipweb()
        ];
    }
}