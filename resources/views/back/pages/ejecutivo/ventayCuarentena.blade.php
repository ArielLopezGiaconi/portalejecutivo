@extends('back.master.masterpage')
@section('contenido')

    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="card">
                <div class="card-header">
                    <div class="breadcrumb-header row">
                        <div class="col-sm-1">
                            <i class="fa fa-money text-inverse f-50"></i>  
                        </div>
                        <div class="col-sm-6">
                            <h3>Venta y cuarentenas por comuna</h3>
                            <span class="f-16">Detalle - Venta y cuarentenas por comuna | <i class="zmdi zmdi-account f-22 middle m-r-5" style="margin-top:-5px;"></i> <strong>{{ Session::get('nombre_ejecutivo') }}</strong></span>
                        </div>
                        <div class="col-sm-4">
                        </div> 
                    </div>
                </div> 

                <div class="card-block" style="">
                    <div class="row">
                        
                    </div>
                </div>
            </div>

            <div class="card-block">
                <div id="alerta" style="display: none" class="alert alert-danger background-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        {{-- <i class="icofont icofont-close-line-circled text-white"></i> --}}
                    </button>
                    <strong>No hay documentos con deuda a la fecha.</strong>
                </div>
                <div id="alerta2" style="display: none" class="alert alert-warning background-warning">
                    <button type="button" class="close" data-dismiss="alert">
                        {{-- <i class="icofont icofont-close-line-circled text-white"></i> --}}
                    </button>
                    <strong>Sin información.</strong>
                </div>
                <div class="table-responsive">
                    {{-- <h5 class="sub-title m-b-15">Ordenes Web</h5> --}}
                    <div id="detalle" onkeyup="Formato()"></div>
                </div>
            </div>

@endsection
@section('custom-includes')
    <script>

        $(document).ready(function () {
            var nFrom = "bottom";
            var nAlign = "right";
            var nIcons = "fa fa-cloud-download";
            var nType = "inverse";
            var nAnimIn = "animated bounceInRight";
            var nAnimOut = "animated bounceOutRight";
            var title = "";
            var message = "Ordenes Web";


            notify2(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut, title, message);
            
            getDerechos();
            
            Buscar();
            setInterval('pendientes()',5000);
            setInterval('IngresosWeb()',5000);
            setInterval('subirImagen()',2000);
            setInterval('NotasPendientes()',5000);
        });

        function FichaCliente(rut){
            var table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            var userid='{{ Session::get('usuario')->get('codusu') }}';
            var moduleid=16;
            var actionid=41;
            var oldvalue='';
            var newvalue=rut;
            registro(userid, moduleid, actionid, oldvalue, newvalue);
            for (i = 1; i < rows.length; i++) {
                row = table.rows[i];
                row.onclick = function(){
                    var cells = this.getElementsByTagName("td");
                    location.href = window.location.href.replace('/notasWeb','') + "/fichacliente/" + rut;
                };
            };
        }

        function Orden(){
            Formato();
        }

        function Formato(){
            var table, tr, td, i, text;
            table = document.getElementById("detallenota");
            var rows = table.getElementsByTagName("tr");
            tr = table.getElementsByTagName("tr");

            for (i = 0; i < tr.length; i++) {
                // td = tr[i].getElementsByTagName("td")[4];
                // $(td).addClass('text-right');
                // text = $(td).text().substring();
                // $(td).text(text.substr(0,10));
                // $(td).text(text.substr(8,2) + "-" + text.substr(5,2) + "-" + text.substr(0,4));

                td = tr[i].getElementsByTagName("td")[5];
                if ($(td).text().substring(0,1) != '$'){
                    $(td).text('$' + number_format( $(td).text()));
                    $(td).addClass('text-right');
                }

                td = tr[i].getElementsByTagName("td")[6];
                if ($(td).text().substring(0,1) != '$'){
                    $(td).text('$' + number_format( $(td).text()));
                    $(td).addClass('text-right');
                }

                td = tr[i].getElementsByTagName("td")[7];
                if ($(td).text().substring(0,1) != '$'){
                    $(td).text('$' + number_format( $(td).text()));
                    $(td).addClass('text-right');
                }

            }
        }

        function Buscar() {
            var table = '<table id="detallenota" class="table table-hover display nowrap table-bordered"\n' +
                '                           cellspacing="0" width="100%">\n' +
                '                        <thead>\n' +
                '                        <tr>\n' +
                '                            <th>Rut</th>\n' + 
                '                            <th>Ranzon Social</th>\n' +
                '                            <th>Negocio</th>\n' +
                '                            <th>Equipo</th>\n' +
                '                            <th>Ejecutivo</th>\n' + 
                '                            <th>Año 2019</th>\n' + 
                '                            <th>Año 2020</th>\n' + 
                '                            <th>Año 2021</th>\n' + 
                '                            <th>Paso</th>\n' +
                '                            <th>Estado</th>\n' +
                '                            <th>Giro Leo</th>\n' +
                '                            <th>Inserción</th>\n' +
                '                            <th>Comuna</th>\n' +
                '                            <th>Ciudad</th>\n' +
                '                            <th>Region</th>\n' +
                '                            <th>Numero</th>\n' +
                '                        </tr>\n' +
                '                        </thead>\n' +
                '                        <tbody>\n' +
                '                        </tbody>\n' +
                '                    </table>';
            document.getElementById("detalle").innerHTML = table;
            
            var token = '{{ csrf_token() }}';
            var parametros = {
                _token: token
            };
            console.log(parametros);
            $.ajax({
                data: parametros,
                url: '{{url('/ventaCovidComuna')}}',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    $("#spinner").show();
                },
                success: function (json) {
                    console.log(json);
                    if (json == false) {
                        document.getElementById("alerta2").style.display = "block";
                            setTimeout(function() {
                                document.getElementById("alerta2").style.display = "none"
                                }, 5000);
                    }else{
                        if (json.response.length > 0) {
                            var datatable = $("#detallenota").DataTable({
                                data: json.response,
                                columns: [
                                    {data: "rutcli",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data+ '</span>\n';}},
                                    {data: "razons",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data+ '</span>\n';}},
                                    {data: "negocio_eerr",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "equipo",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data + '</span>\n';}},
                                    {data: "ejecutivo",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "a_2019",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "a_2020",
                                        render: function (data, type, row, meta) {
                                        return data;}},
                                    {data: "a_2021",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "paso",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "estado",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "giro_leo",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "insercion",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "comuna",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "ciudad",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "region",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-left">' + data + '</span>\n';}},
                                    {data: "num",
                                        render: function (data, type, row, meta) {
                                        return '<span class="pull-right">' + data + '</span>\n';}}
                                ],
                                "scrollX": true,
                                "scrollY": 400,
                                "iDisplayLength": 100,
                                "ColumnsDef": [
                                    { "sWidth": "300px", "aTargets": [ 9 ] }
                                ],
                                "fixedHeader": true,
                                dom: 'Bfrtip',
                                buttons: [
                                    { extend:'excelHtml5', text: 'Descargar a excel'},
                                    // { extend:'pdfHtml5', text: 'Descargar a PDF'}
                                ]
                                ,
                                language: {
                                    buttons: {
                                        copyTitle: 'Copiado en el portapapeles',
                                        copyKeys: 'Press <i>ctrl</i> or <i>\u2318</i> + <i>C</i> to copy the table data<br>to your system clipboard.<br><br>To cancel, click this message or press escape.',
                                        copySuccess: {
                                            _: '%d filas copiadas al portapapeles',
                                            1: '1 fila copiada al portapapeles'
                                        }
                                    },
                                    "lengthMenu": "Mostrando _MENU_ registros por página",
                                    "info": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
                                    "infoFiltered": "(filtrado de _MAX_ registros)",
                                    "search": "Buscar:",
                                    "paginate": {
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    }
                                }
                            });
                            $("#spinner").hide();
                            Orden();
                        } else {
                            document.getElementById("alerta").style.display = "block";
                            setTimeout(function() {
                                document.getElementById("alerta").style.display = "none"
                                }, 5000);
                            $("#spinner").hide();
                        }
                    }
                }
                , error: function (e) {
                    console.log('Error:' + e.message);
                }
            });
        }

    </script>
@endsection