<?php


namespace App\Entities\Oracle\DMVentas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
// use Illuminate\Support\Facades\DB;
use DB;


class ReClavsis implements \JsonSerializable
{
    private $rutusu;
    private $fechas;
    private $clave1;
    private $clave2;
    private $clave3;
    private $clavta1;
    private $clavta2;
    private $clavta3;
    private $fecvta;

    public function __construct($registro = null)
    {
        if($registro) {
            $this->rutusu = isset($registro->rutusu) ? $registro->rutusu : null;
            $this->fechas = isset($registro->fechas) ? $registro->fechas : null;
            $this->clave1 = isset($registro->clave1) ? $registro->clave1 : null;
            $this->clave2 = isset($registro->clave2) ? $registro->clave2 : null;
            $this->clave3 = isset($registro->clave3) ? $registro->clave3 : null;
            $this->clavta1 = isset($registro->clavta1) ? $registro->clavta1 : null;
            $this->clavta2 = isset($registro->clavta2) ? $registro->clavta2 : null;
            $this->clavta3 = isset($registro->clavta3) ? $registro->clavta3 : null;
            $this->fecvta = isset($registro->fecvta) ? $registro->fecvta : null;
        }
    }


    public function getRutusu()
    {
        return $this->rutusu;
    }


    public function setRutusu($rutusu)
    {
        $this->rutusu = $rutusu;
    }


    public function getFechas()
    {
        return $this->fechas;
    }


    public function setFechas($fechas)
    {
        $this->fechas = $fechas;
    }


    public function getClave1()
    {
        return $this->clave1;
    }


    public function setClave1($clave1)
    {
        $this->clave1 = $clave1;
    }


    public function getClave2()
    {
        return $this->clave2;
    }


    public function setClave2($clave2)
    {
        $this->clave2 = $clave2;
    }


    public function getClave3()
    {
        return $this->clave3;
    }


    public function setClave3($clave3)
    {
        $this->clave3 = $clave3;
    }


    public function getClavta1()
    {
        return $this->clavta1;
    }


    public function setClavta1($clavta1)
    {
        $this->clavta1 = $clavta1;
    }


    public function getClavta2()
    {
        return $this->clavta2;
    }


    public function setClavta2($clavta2)
    {
        $this->clavta2 = $clavta2;
    }


    public function getClavta3()
    {
        return $this->clavta3;
    }


    public function setClavta3($clavta3)
    {
        $this->clavta3 = $clavta3;
    }


    public function getFecvta()
    {
        return $this->fecvta;
    }


    public function setFecvta($fecvta)
    {
        $this->fecvta = $fecvta;
    }

    public function jsonSerialize()
    {
        return[
            'rutusu' =>$this->rutusu,
            'fechas' =>$this->fechas,
            'clave1' =>$this->clave1,
            'clave2' =>$this->clave2,
            'clave3' =>$this->clave3,
            'clavta1' =>$this->clavta1,
            'clavta2' =>$this->clavta2,
            'clavta3' =>$this->clavta3,
            'fecvta' =>$this->fecvta,
        ];
    }


}