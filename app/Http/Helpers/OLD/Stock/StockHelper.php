<?php
/**
 * Created by PhpStorm.
 * User: web1
 * Date: 29/08/2018
 * Time: 13:38
 */

namespace App\Http\Helpers\Stock;

use App\Entities\Oracle\Holding\MaProduct;
use App\PDO\MySql\Dimerc\CatalogProductEntityPDO;
use App\PDO\MySql\OfimarketProduction\CatalogProductFlat2PDO;
use App\PDO\Oracle\Holding\MaEmpresaPDO;
use App\Helpers\GeneralHelper;
use App\Http\Controllers\Stock\StockController;
use App\PDO\Lib\ResponseFormatt;
use App\PDO\Oracle\DMVentas\MaBodegasPDO;
use App\PDO\Oracle\DMVentas\MaProductPDO;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


class StockHelper extends GeneralHelper
{
    public function getStockSpecificProdVault($in_codpro = null, $in_codbod = null, $in_codemp = 3)
    {
        // 1.- Validar Ingreso codpro
        if ($in_codpro == null || trim($in_codpro) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codpro del producto a consultar [1|codpro]");

            return $responseFormatt->returnToJson();
        }

        // 2.- Validar ingreso codbod
        if ($in_codbod == null || trim($in_codbod) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar [1|codpro]");

            return $responseFormatt->returnToJson();
        }

        // 3.- Validar que bodega sea número
        if (!intval($in_codbod)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(221)
                ->setResponse("codbod debe ser número.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Validar codemp
        if ($in_codemp == null || trim($in_codemp) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar [1|codpro]");

            return $responseFormatt->returnToJson();
        }

        // 5.- Validar que empresa sea número
        if (!is_int($in_codemp)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(221)
                ->setResponse("codbod debe ser número.");

            return $responseFormatt->returnToJson();
        }

        // 6.- Validar existencia producto Código Dimerc
        if (!MaProductPDO::existsProductByCod($in_codpro)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Producto no existe en la base de datos (" . $in_codpro . ")");

            return $responseFormatt->returnToJson();
        }

        // 7.- Validar existencia de empresa
        if (!MaEmpresaPDO::existsCompanyByCod($in_codemp)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Empresa no existe en la base de datos (" . $in_codemp . ")");

            return $responseFormatt->returnToJson();
        }

        // 8.- Validar existencia bodega
        if (!MaBodegasPDO::existsActiveWharehouseByCod($in_codbod, $in_codemp)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Bodega no existe en la base de datos (" . $in_codbod . ")");

            return $responseFormatt->returnToJson();
        }

        // 9.- Validar que exista stock del producto en bodega específica
        $controlador = new StockController();
        return $controlador->getStockSpecificProdVault($in_codpro, $in_codbod, $in_codemp);
    }

    public function getSpecificStockMagento($in_codpro = null)
    {
        // 1.- Validar Ingreso codpro
        if ($in_codpro == null || trim($in_codpro) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codpro del producto a consultar [1|codpro]");

            return $responseFormatt->returnToJson();
        }

        // 2.- Validar existencia producto Código Dimerc
        if (!MaProductPDO::existsProductByCod($in_codpro)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(223)
                ->setResponse("Producto no existe en la base de datos (" . $in_codpro . ")");

            return $responseFormatt->returnToJson();
        }

        // 3.- Validar que exista stock del producto en bodega específica
        $controlador = new StockController();
        return $controlador->getSpecificStockMagento($in_codpro);
    }

    public function getSpecificWarehouseStockDimercChile($in_codbod = null, $in_codemp = 3)
    {
        // 1.- Verificar que ingresó codbod
        if ($in_codbod == null || trim($in_codbod) == '') {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar.");

            return $responseFormatt->returnToJson();
        }

        // 2.- Verificar que exista empresa
        if (!MaEmpresaPDO::existsCompanyByCod($in_codemp)) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Empresa " . $in_codemp . " no existe en base de datos. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 3.- Verificar que bodega ingresada exista
        $bodega = MaBodegasPDO::getBodegasByCodbod($in_codbod, $in_codemp, true);
        if ($bodega == null) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $in_codemp . ") no existe en base de datos, o no está activa la bodega. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Verificar que bodega tiene toda la información SAP.
        if ($bodega->getSapAlmacen() == null || $bodega->getSapCentro() == null || $bodega->getSapSociedad() == null) {
            $responseFormatt = new ResponseFormatt();
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $in_codemp . ") no tiene toda la información SAP"
                    . " [almacen: " . $bodega->getSapAlmacen() . " | centro: " . $bodega->getSapCentro() . " | sociedad: " . $bodega->getSapSociedad() . "]."
                );

            return $responseFormatt->returnToJson();
        }

        $controlador = new StockController();
        return $controlador->getSpecificWarehouseStockDimercChile($bodega);
    }

    public function getSpecificStoreStockDimercChile($in_codbod = null, $in_codpro = null, Request $request)
    {
        $update = false;
        $responseFormatt = new ResponseFormatt();

        // 0.- Validate Action
        if ($request->input('action') && $request->input('action') == 'update') {
            $update = true;
        }

        // 1.- Verificar que ingresó codbod
        if ($in_codbod == null || trim($in_codbod) == '') {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar.");

            return $responseFormatt->returnToJson();
        }

        // 3.- Verificar que bodega ingresada exista
        $bodega = MaBodegasPDO::getBodegasByCodbod($in_codbod, 3, true);
        if ($bodega == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . 3 . ") no existe en base de datos, o no está activa la bodega. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Verificar que bodega tiene toda la información SAP.
        if ($bodega->getSapAlmacen() == null || $bodega->getSapCentro() == null || $bodega->getSapSociedad() == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . 3 . ") no tiene toda la información SAP"
                    . " [almacen: " . $bodega->getSapAlmacen() . " | centro: " . $bodega->getSapCentro() . " | sociedad: " . $bodega->getSapSociedad() . "]."
                );

            return $responseFormatt->returnToJson();
        }

        // 5.- Verificar si es que ingresó producto, ver que exista
        $producto = null;
        if ($in_codpro != null && trim($in_codpro) <> "") {
            $producto = MaProductPDO::getProductByCod($in_codpro, true);
            if ($producto == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . "No existe en la base de datos de Legado. Verifique");

                return $responseFormatt->returnToJson();
            } else if ($producto->getCodsap() == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . " si existe, pero no tiene código sap. Verifique");

                return $responseFormatt->returnToJson();
            }
        }

        $controlador = new StockController();
        return $controlador->getSpecificStoreStockDimercChile($bodega, $producto, $update);
    }

    public function getAllStockMagento()
    {
        $controlador = new StockController();
        return $controlador->getAllStockMagento();
    }

    public function getAllStockMagentoAdvanced()
    {
        $controlador = new StockController();
        return $controlador->getAllStockMagentoAdvanced();
    }

    public function getAllStockMagentoPeru()
    {
        $controlador = new StockController();
        return $controlador->getAllStockMagentoPeru();
    }

    public function OplRefreshData()
    {
        $controlador = new StockController();
        return $controlador->refreshWareHouseOpl();
    }


    /* API para Mario Moreno */
    // CODEMP = 3
    public function getSpecificStockStoreChile($in_empresa = null, $in_codbod = null, $in_codpro = null, Request $formulario)
    {
        $update = false;
        $responseFormatt = new ResponseFormatt();

        // 0.- Validate Action
        if ($formulario->input('action') && $formulario->input('action') == 'update') {
            $update = true;
        }

        // Verificar empresa
        if ($in_empresa == null || ($in_empresa != 'dimerc' && $in_empresa != 'pronobel' && $in_empresa != 'dimeiggs')) {
            $responseFormatt
                ->setCode(300)
                ->setResponse("Debe especificar la empresa especifica en la cual quiere hacer la carga [dimerc | dimeigss | pronobel]");

            return $responseFormatt->returnToJson();
        }

        $codemp = 0;
        switch ($in_empresa) {
            case 'dimerc':
                $codemp = 3;
                break;
            case 'dimeiggs':
                $codemp = 2;
                break;
            case 'pronobel':
                $codemp = 1;
                break;
            default:
                $codemp = 0;
                break;
        }
        $empresa = MaEmpresaPDO::getCompanyByCod($codemp, true);
        if (!$empresa) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("No se encuentra la bodega " . $codemp . " en HOLDING");

            return $responseFormatt->returnToJson();
        }

        // 1.- Verificar que ingresó codbod
        if ($in_codbod == null || trim($in_codbod) == '') {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar.");

            return $responseFormatt->returnToJson();
        }

        // 3.- Verificar que bodega ingresada exista
        $bodega = MaBodegasPDO::getBodegasByCodbod($in_codbod, $empresa->getCodemp(), true);
        if ($bodega == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $empresa->getCodemp() . ") no existe en base de datos, o no está activa la bodega. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Verificar que bodega tiene toda la información SAP.
        if ($bodega->getSapAlmacen() == null || $bodega->getSapCentro() == null || $bodega->getSapSociedad() == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $codemp . ") no tiene toda la información SAP"
                    . " [almacen: " . $bodega->getSapAlmacen() . " | centro: " . $bodega->getSapCentro() . " | sociedad: " . $bodega->getSapSociedad() . "]."
                );

            return $responseFormatt->returnToJson();
        }

        // 5.- Verificar si es que ingresó producto, ver que exista
        $producto = null;
        if ($in_codpro != null && trim($in_codpro) <> "") {
            $producto = MaProductPDO::getProductByCod($in_codpro, true, $bodega->getCodemp());
            if ($producto == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . " no existe en la base de datos (Empresa: " . $bodega->getCodemp() . "). Verifique");

                return $responseFormatt->returnToJson();
            } else if ($producto->getCodsap() == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . " si existe, pero no tiene código sap. Verifique");

                return $responseFormatt->returnToJson();
            }
        }

        $controlador = new StockController();
        return $controlador->getSpecificStockStoreChile($bodega, $producto, $update);
    }


    public function getSpecificStockStorePeru($in_codbod = null, $in_codpro = null, Request $formulario)
    {
        $update = false;
        $responseFormatt = new ResponseFormatt();

        // 0.- Validate Action
        if ($formulario->input('action') && $formulario->input('action') == 'update') {
            $update = true;
        }

        $codemp = 3;
        // 1.- Verificar que ingresó codbod
        if ($in_codbod == null || trim($in_codbod) == '') {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Debe ingresar codbod de la bodega a consultar.");

            return $responseFormatt->returnToJson();
        }

        // 3.- Verificar que bodega ingresada exista
        $bodega = \App\PDO\Oracle\DMVentasPeru\MaBodegasPDO::getBodegasByCodbod($in_codbod, $codemp, true);
        if ($bodega == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $codemp . ") no existe en base de datos, o no está activa la bodega. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Verificar que bodega tiene toda la información SAP.
        if ($bodega->getSapAlmacen() == null || $bodega->getSapCentro() == null || $bodega->getSapSociedad() == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $in_codbod . " (empresa " . $codemp . ") no tiene toda la información SAP"
                    . " [almacen: " . $bodega->getSapAlmacen() . " | centro: " . $bodega->getSapCentro() . " | sociedad: " . $bodega->getSapSociedad() . "]."
                );

            return $responseFormatt->returnToJson();
        }

        // 5.- Verificar si es que ingresó producto, ver que exista
        $producto = null;
        if ($in_codpro != null && trim($in_codpro) <> "") {
            $producto = \App\PDO\Oracle\DMVentasPeru\MaProductPDO::getProductByCod($in_codpro, true);
            if ($producto == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . " no existe en la base de datos (Empresa: " . $bodega->getCodemp() . "). Verifique");

                return $responseFormatt->returnToJson();
            } else if ($producto->getCodsap() == null) {
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Producto " . $in_codpro . " si existe, pero no tiene código sap. Verifique");

                return $responseFormatt->returnToJson();
            }
        }

        $controlador = new StockController();
        return $controlador->getSpecificStockStorePeru($bodega, $producto, $update);
    }

    public function getSpecificStockWebChile($in_empresa = null, Request $formulario)
    {
        $update = false;
        $responseFormatt = new ResponseFormatt();

        // 0.- Validate Action
        if ($formulario->input('action') && $formulario->input('action') == 'update') {
            $update = true;
        }

        // Verificar empresa
        if ($in_empresa == null || ($in_empresa != 'dimerc' && $in_empresa != 'pronobel' && $in_empresa != 'dimeiggs')) {
            return $responseFormatt
                ->setCode(300)
                ->setResponse("Debe especificar si hará carga rápida o lenta [dimerc | dimeigss | pronobel]")
                ->returnToJson();
        }

        $codemp = 0;
        switch ($in_empresa) {
            case 'dimerc':
                $codemp = 3;
                $codbod = 1;
                break;
            case 'dimeiggs':
                $codemp = 2;
                $codbod = 141;
                break;
            case 'pronobel':
                $codemp = 1;
                $codbod = 1;
                break;
            default:
                $responseFormatt
                    ->setCode(220)
                    ->setResponse("Empresa no válida [dimerc | dimeiggs | pronobel]");

                return $responseFormatt->returnToJson();
                break;
        }

        $empresa = MaEmpresaPDO::getCompanyByCod($codemp, true);
        if (!$empresa) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("No se encuentra la bodega " . $codemp . " en HOLDING");

            return $responseFormatt->returnToJson();
        }


        // 3.- Verificar que bodega ingresada exista
        $bodega = MaBodegasPDO::getBodegasByCodbod($codbod, $empresa->getCodemp(), true);
        if ($bodega == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $codbod . " (empresa " . $empresa->getCodemp() . ") no existe en base de datos, o no está activa la bodega. Verifique.");

            return $responseFormatt->returnToJson();
        }

        // 4.- Verificar que bodega tiene toda la información SAP.
        if ($bodega->getSapAlmacen() == null || $bodega->getSapCentro() == null || $bodega->getSapSociedad() == null) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("Bodega " . $codbod . " (empresa " . $codemp . ") no tiene toda la información SAP"
                    . " [almacen: " . $bodega->getSapAlmacen() . " | centro: " . $bodega->getSapCentro() . " | sociedad: " . $bodega->getSapSociedad() . "]."
                );

            return $responseFormatt->returnToJson();
        }

        // 5.- Obtener todos los productos de la web específica
        $arrayProductos = null;
        switch ($in_empresa) {
            case 'dimeiggs':
                $arrayProductos = CatalogProductFlat2PDO::getAllSku();
                break;
            case 'pronobel':
                $codemp = 1;
                $codbod = 1;
                break;
            case 'dimerc':
            default:
                $arrayProductos = CatalogProductEntityPDO::getAllSkuChile();
                break;

        }

        if (!$arrayProductos) {
            $responseFormatt
                ->setCode(220)
                ->setResponse("No se encontraron productos en Magento (" . $in_empresa . ")");

            return $responseFormatt->returnToJson();
        }
        $arrayProductos = new Collection($arrayProductos);

        // IR AL CONTROLADOR
        $controlador = new StockController();
        return $controlador->getSpecificStockWebChile($bodega, $arrayProductos, $update);
    }
}