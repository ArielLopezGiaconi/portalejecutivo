<!DOCTYPE html>
<html lang="es">

<head>
    @include('back.master.partials.head')
</head>
<body>
{{--<body ondragstart="return false" onselectstart="return false" oncontextmenu="return false">--}}
@include('back.master.partials.loader')
<div id="pcoded" class="pcoded iscollapsed">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                
                <div class="pcoded-content">
                    @yield('contenido')
                </div>
            </div>
        </div>
    </div>
</div>
@include('back.master.partials.footer-includes')
@yield('custom-includes')
</body>
</html>

